<?php
	$root = substr($_SERVER['DOCUMENT_ROOT'], 0, -1);
?>


<table style="width: 100%;" >
	<tr>
		<th>Guida all'accesso al sito utilizzando Mozilla Firefox</th>
	</tr>

	<tr><th><br /></th></tr>

	<tr>
		<td style="text-align: center;">
			Alla comparsa della finestra riportata sotto, fare click sulla voce "Sono consapevole dei rischi"<br />
			<img src="ff_connessione.png" alt="ff_connessione.png"
				style="border: 0px;" />
		</td>
	</tr>

	<tr><td><br /></td></tr>

	<tr>
		<td style="text-align: center;">
			Quindi fare click sulla voce "Aggiungi eccezione"<br />
			<img src="ff_connessione2.png" alt="ff_connessione2.png"
				style="border: 0px;" />
		</td>
	</tr>

	<tr><td><br /></td></tr>

	<tr>
		<td style="text-align: center;">
			Verr&agrave; visualizzata la finestra riportata sotto, fare click su "Acquisisci certificato":<br />
			<img src="ff_eccezione.png" alt="ff_eccezione.png"
				style="border: 0px;" />
		</td>
	</tr>

	<tr><td><br /></td></tr>

	<tr>
		<td style="text-align: center;">
			in fine fare click su "Conferma eccezione di sicurezza":<br />
			<img src="ff_conferma_eccezione.png" alt="ff_conferma_eccezione.png"
				style="border: 0px;" />
		</td>
	</tr>

	<tr><td><br /></td></tr>

	<tr>
		<td style="text-align: center;">
			adesso &egrave;possibile visualizzare il sito senza problemi.
		</td>
	</tr>

</table>

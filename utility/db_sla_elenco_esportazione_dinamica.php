<!DOCTYPE html>
<style type="text/css">
    body {
        font-size: 10pt;
        font-family: monospace;
    }
</style>
<?php

    require_once '../php/Database.php';
    $d = new Database("localhost", "gestore", "polipo", "db_sla");
    
    DEFINE( "DEST_TABLE", "tab_switch" );
    
    $Actions = array( 'alssqol' => 'tab_alssqol' );
    
    
    
    foreach ($Actions as $key => $value) 
    {
        // recupero l'id del menu
        $Menus = $d->GetRows("id_menu, titolo", "tab_menu", "act = '$key'", "", "", 1 );
        
//        echo "<pre> Menus";
//            print_r($Menus);
//        echo "</pre>";
        
//        INSERT INTO tab_switch ( id_menu, descrizione, tabella, id_tabella, nome_campo, etichetta, tipo_campo) 
//            VALUES ( '48', 'Anamnesi familiare', 'tab_anamnesi_familiare', 'id_anamnesi', 'sla', 'Sla', '');
        $temp_array = $d->Query( "describe $value", 1 );
        
//        echo "<pre> temp_array";
//            print_r($temp_array);
//        echo "</pre>";
        
        foreach ($temp_array as $fkey => $fvalue ) 
        {
            if ( ( $fvalue['Field'] == "id_generalita" ) 
                 || ( $fvalue['Field'] == "data_insert" )
                 || ( preg_match( "(id_.*)", $fvalue['Field'] ) ) 
               )
            {
                // salta quei campi
                continue;
            }
            
            $QString = "INSERT INTO " . DEST_TABLE;
            $QString .= "( id_menu, descrizione, tabella, id_tabella, ";
            $QString .= "nome_campo, etichetta, tipo_campo) ";
            $QString .= "VALUES ( " . $Menus[0]['id_menu'] . ", '" . $Menus[0]['titolo'] . "', ";
            $QString .= "'$value', '" . $temp_array[0]['Field'] . "', ";
            $QString .= "'" . $fvalue['Field'] . "', ";
            
            switch ( $fvalue['Field'] ) 
            {
                case "data":
                    $fvalue['Etichetta'] = "Data visita";
                    break;
                
                default:
                     $fvalue['Etichetta'] = ucfirst( str_replace( '_', ' ',  $fvalue['Field'] ) );
            }
            $QString .= "'" . $fvalue['Etichetta'] . "', ";
            
            switch ( $fvalue['Type'] )
            {                   
                case "tinyint(1)":
                    $QString .= "'sino'";
                    break;
                
                default:
                    $QString .= "''";
            }
            $QString .= " );";
                
            echo "$QString <br />";
        }
    }
    
    $Results = $d->GetRows( "*", "tab_switch", "nome_campo LIKE 'data as%' and etichetta != 'Data visita'", "", "", 1 );
       
    foreach ( $Results as $key => $value )
    {
      //update tab_switch set nome_campo = 'data as data_bmi' where nome_campo = 'data' and tabella = 'tab_pesoBMI'
      $QString = "UPDATE " . DEST_TABLE;
      $QString .= " SET nome_campo = '" . str_replace( ' ', '_', strtolower( $value['etichetta'] ) ) . "' ";
      $QString .= "WHERE id_switch = '" . $value['id_switch'] . "'; ";
      $QString .= "-- " . $value['tabella'];
              
      echo "$QString <br />";
    }
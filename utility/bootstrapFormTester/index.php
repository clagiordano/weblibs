<!doctype html>
<?php
/**
 * Copyright 2015 Claudio Giordano <claudio.giordano@autistici.org>
 */
$formDirPath = "../../forms/";
$dirList     = scandir($formDirPath);

require_once '../../php/DatabaseLegacy.php';

use clagiordano\weblibs\php\DatabaseLegacy;

$d = new DatabaseLegacy("localhost", "test", "test", "sample");
?>

<html>
    <head>
        <title>Utility</title>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <meta name="description" content="Utility" />
        <meta name="keywords" content="HTML,CSS,XML,JavaScript,MySQL,PHP" />
        <meta name="author" content="Claudio Giordano <claudio.giordano@autistici.org>" />

        <!-- Bootstrap -->
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/bootstrap-theme.css" rel="stylesheet">

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="js/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="js/bootstrap.min.js"></script>

        <nav class="navbar navbar-inverse">

        </nav>

        <div class="container-fluid">
            <div class="row">
                <div class="col-md-3">
                    <ul class="list-group">
                        <?php
                        foreach ($dirList as $element) {
                            if ((filetype($formDirPath . $element) == "file") && ($element != '.') && ($element != '..')) {
                                ?>
                                <li <?php
                                if ($element == filter_input(INPUT_GET,
                                                             'preview',
                                                             FILTER_SANITIZE_STRING)) {
                                    echo "class='list-group-item active'";
                                } else {
                                    echo "class='list-group-item'";
                                }
                                ?>>
                                    <a href="?preview=<?php echo $element; ?>">
        <?php echo $element; ?>
                                        <i class="glyphicon glyphicon-chevron-right pull-right"></i>
                                    </a>

                                </li>
                                <?php
                            }
                        }
                        ?>
                    </ul>
                </div>

                <div class="col-md-9">
                    <?php
                    if (filter_input(INPUT_GET, 'preview',
                                     FILTER_SANITIZE_STRING) != false) {
                        include $formDirPath . filter_input(INPUT_GET,
                                                            'preview',
                                                            FILTER_SANITIZE_STRING);
                    } else {
                        echo "<h2>Select a page to preview!</h2>";
                    }
                    ?>
                </div>
            </div>
        </div>


    </body>
</html>

<!doctype html>
<?php
/**
 * Copyright 2015 Claudio Giordano <claudio.giordano@autistici.org>
 */
$formDirPath = "../../forms/";
$dirList     = scandir($formDirPath);
?>

<html>
    <head>
        <title>GN Gioielli</title>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <meta name="description" content="Utility" />
        <meta name="keywords" content="HTML,CSS,XML,JavaScript,MySQL,PHP" />
        <meta name="author" content="Claudio Giordano <claudio.giordano@autistici.org>" />

        <!-- Bootstrap -->
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/bootstrap-theme.css" rel="stylesheet">

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="js/tests/vendor/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="js/bootstrap.min.js"></script>

        <?php
            include filter_input(INPUT_GET, 'page');
        ?>
    </body>
</html>

<?php
/*
 *      CleanMailBox.php
 *
 *      Copyright 2011 Claudio Giordano <claudio.giordano@autistici.org>
 *
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 *      MA 02110-1301, USA.
 */

	$root = substr($_SERVER['DOCUMENT_ROOT'], 0, -1);
	require_once("$root/Class/Mail.php");

	if (!empty($_POST))
	{
		$m = new Mail($_POST['host'], $_POST['user'], $_POST['pass'], $_POST['proto']);
	}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<head>
	<title>Clean MailBox</title>
	<meta http-equiv="content-type" content="text/html;charset=utf-8" />
	<link href="/css/template-chiaro.css" rel="StyleSheet" type="text/css" />
</head>
<body>

<div id="search-form">
	<form method="post" action="<?php echo $_SERVER['REQUEST_URI']; ?>">
		<table class="form" style="width: 100%;">
			<tr>
				<th colspan="8">User Data:</th>
			</tr>
			<tr>
				<td style="text-align: right;">
					<label>Protocol:</label>
				</td>
				<td>
					<select name="proto"
						onchange="Javascript: posta('0', '<?php echo $_SERVER['REQUEST_URI']; ?>'); void(0);">
						<option value="-">Select protocol</option>
						<option value="pop"
							<?php if ($_POST['proto'] == "pop") {
								echo "selected=\"selected\"";
							 } ?>>POP</option>
					</select>

					&nbsp;
					<label>HostName:</label>
					<input name="host" type="text"
						value="<?php if (isset($_POST['host'])) { echo $_POST['host']; } ?>" />

					&nbsp;
					<label>UserName:</label>
					<input name="user" type="text"
						value="<?php if (isset($_POST['user'])) { echo $_POST['user']; } ?>" />

					&nbsp;
					<label>Password:</label>
					<input name="pass" type="password"
						value="<?php if (isset($_POST['pass'])) { echo $_POST['pass']; } ?>" />

					<input name="submit" type="submit" value="submit" />
				</td>
			</tr>
		</table>
	</form>
</div>
</body>
</html>

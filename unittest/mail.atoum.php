<?php

require_once 'atoum.phar';
include_once '../php/Mail.php';

use \mageekguy\atoum;
use weblibs\php\Mail;

class TestMail extends atoum\test
{
    function testSendMail()
    {
        $m = new Mail();

        $m->setHostname("localhost");
        $m->setProtocol("smtp");
        $m->setUsername("claudio@localhost.localdomain");
        $m->addAttachment("testfile.tmp");

        $m->send();
    }
}
<?php

require_once './EmptyTest.php';

use clagiordano\weblibs\php\Response;

/**
 * Description of TestResponse
 *
 * @author Claudio Giordano <claudio.giordano@autistici.org>
 */
class TestResponse extends EmptyTest
{
    protected function getClassFileName()
    {
        return "Response.php";
    }

    public function testStart()
    {
        //xdebug_start_code_coverage();

        $this->TestData = [
            "https",
            "username",
            "password",
            "www.somewhere.com",
            243,
            "/this/that/here.php",
            "com",
            "?a=1&b=2",
            "anchor"
        ];

        $this->TestData2 = [
            0,
            "test",
            "aaa",
            [
                'a',
                'b',
                'c'
            ],
            [
                'a' => 'z',
                'b' => 'y',
                'c' => 'x'
            ],
        ];

        $this->Response = new Response($this->TestData);
        $this->Response2 = new Response();
    }

    function testBasicResponse()
    {
        $this->assertIsA($this->Response, 'clagiordano\weblibs\php\Response');

        $this->assertNull(@$this->Response->getResponse());
        $this->assertIsA($this->Response->getResponse(false), 'String');
    }

    function testSetResponse()
    {
        $this->Response->setResponse($this->TestData2);
        $this->assertIsA($this->Response, 'clagiordano\weblibs\php\Response');

        $this->assertNull(@$this->Response->getResponse());
        $this->assertIsA($this->Response->getResponse(false), 'String');
    }

    function testPostSetResponse()
    {
        $this->assertNull(@$this->Response2->getResponse());
        $this->Response2->setResponse($this->TestData2);
        $this->assertIsA($this->Response2, 'clagiordano\weblibs\php\Response');

        $this->assertNull(@$this->Response2->getResponse());
        $this->assertIsA($this->Response2->getResponse(false), 'String');
    }
}

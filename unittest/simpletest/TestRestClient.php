<?php

require_once './EmptyTest.php';

use clagiordano\weblibs\php\RestClient;

/**
 * Description of TestAuthentication
 *
 * @author Claudio Giordano <claudio.giordano@autistici.org>
 */
class TestRestClient extends EmptyTest
{
    private $classFilename = "RestClient.php";
    private $restclient = null;

    private $testURL = 'https://www.google.it';
    private $testToken = "sample";
    private $testParams = [
        'param1' => 1,
        'param2' => 'test'
    ];

    protected function getClassFileName()
    {
        return "RestClient.php";
    }

    public function testMinimalUsage()
    {
        $this->restclient = new RestClient();
        $this->assertIsA($this->restclient, 'clagiordano\weblibs\php\RestClient');
    }

    public function testBasicUsage()
    {
        $this->restclient = new RestClient();
        $this->assertIsA($this->restclient, 'clagiordano\weblibs\php\RestClient');

        $this->restclient->setRequestURL('https://testurl.com/params/');
        $this->restclient->doRequest();
    }

    public function testBasicUsageWithCookie()
    {
        $this->restclient = new RestClient();
        $this->assertIsA($this->restclient, 'clagiordano\weblibs\php\RestClient');

        $this->restclient->setUseLocalCookie(true);
        $this->restclient->setLocalCookieName("sampleCookie");

        $this->assertEqual(
            'sampleCookie',
            $this->restclient->getLocalCookieName()
        );

        $this->assertTrue(
            $this->restclient->isUseLocalCookie()
        );

        $this->restclient->setRequestURL('https://testurl.com/params/12343');
        $this->restclient->doRequest();
    }

    public function testInvalidUsage()
    {
        $this->restclient = new RestClient();
        $this->assertIsA($this->restclient, 'clagiordano\weblibs\php\RestClient');

        $this->expectException();
        $this->restclient->doRequest();
    }

    public function testAdvancedUsage()
    {
        $this->restclient = new RestClient();
        $this->assertIsA($this->restclient, 'clagiordano\weblibs\php\RestClient');

        $this->restclient->setRequestURL($this->testURL);
        $this->assertEqual(
            $this->restclient->getRequestURL(),
            $this->testURL
        );

        $this->restclient->setRequestHeaders(['testHeader' => 'test']);
        $this->assertEqual(
            $this->restclient->getRequestHeaders('testHeader'),
            'test'
        );

        $this->restclient->setRequestHeaders([]);
        $this->assertIsA($this->restclient->getRequestHeaders(), 'Array');

        $this->restclient->setAuthenticationToken($this->testToken);
        $this->assertEqual(
            $this->restclient->getAuthenticationToken(),
            $this->testToken
        );

        $this->restclient->setRequestParams($this->testParams);
        $this->assertEqual(
            $this->restclient->getRequestParams('param2'),
            'test'
        );

        $this->assertIsA($this->restclient->getRequestParams(), 'Array');
        print_r("RESULT");
        print_r($this->restclient->doRequest());
    }

    public function testUseBaseURL()
    {
        $this->restclient = new RestClient();
        $this->assertIsA($this->restclient, 'clagiordano\weblibs\php\RestClient');

        $this->restclient->setRequestBaseURL('https://testurl.com/');
        $this->assertEqual(
            'https://testurl.com/',
            $this->restclient->getRequestBaseUrl()
        );

        $this->restclient->setRequestURL('sampleRequest/');
        $this->restclient->doRequest(true);
    }

}

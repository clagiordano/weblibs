<?php
require_once __DIR__ . "/../../vendor/autoload.php";
require_once __DIR__ . "/../../vendor/simpletest/simpletest/autorun.php";
?>

<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <meta name="description" content="Utility" />
        <meta name="keywords" content="HTML,CSS,XML,JavaScript,MySQL,PHP" />
        <meta name="author" content="Claudio Giordano <claudio.giordano@autistici.org>" />

        <!-- Bootstrap -->
        <link href="../../utility/bootstrapFormTester/css/bootstrap.min.css" rel="stylesheet">
        <link href="../../utility/bootstrapFormTester/css/bootstrap-theme.css" rel="stylesheet">

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="../../utility/bootstrapFormTester/js/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="../../utility/bootstrapFormTester/js/bootstrap.min.js"></script>

        <?php

        /**
         * Base unit test made with simpletest framework
         *
         * @author Claudio Giordano <claudio.giordano@autistici.org>
         */
        abstract class EmptyTest extends UnitTestCase
        {
            private $fullCodeCoverageStatus = [];
            private $codeNotExecutable = [];
            private $codeExecuted      = [];
            private $codeNotExecuted   = [];
            private $codeNotExecutableCount = null;
            private $codeExecutedCount      = null;
            private $codeNotExecutedCount   = null;

            abstract protected function getClassFileName();

            /**
             *
             */
            public function __construct()
            {
                if (is_callable("xdebug_start_code_coverage") && $this->getClassFileName()) {
                    xdebug_stop_code_coverage();
                    xdebug_start_code_coverage(XDEBUG_CC_UNUSED | XDEBUG_CC_DEAD_CODE);
                } else {
                    echo __METHOD__ . ": Unsupported code coverage.";
                }
            }

            /**
             * @param $classFilename
             * @param bool|false $showNotExecuted
             */
            public function calculateCodeCoverage($classFilename,
                                                  $showNotExecuted = false)
            {
                $fileKey        = null;
                $coverageData   = xdebug_get_code_coverage();
                $listFileParsed = array_keys($coverageData);

                foreach ($listFileParsed as $file) {
                    if (preg_match("(^(.*)/$classFilename$)", $file)) {
                        $fileKey = $file;
                    }
                }

                if (is_null($fileKey)) {
                    echo "No data coverage found for filename '$classFilename'";
                    return;
                }

                $classCode = file($fileKey);
                foreach ($coverageData[$fileKey] as $lineNum => $lineStatus) {
                    /** Fix offset starting line */
                    if ($lineNum == 0) {
                        $lineNum = 1;
                    }

                    if ($lineStatus == "-1") {
                        $this->codeNotExecuted[$lineNum] = [
                            'code'   => $classCode[$lineNum - 1],
                            'status' => -1
                        ];
                    } elseif ($lineStatus == "1") {
                        $this->codeExecuted[$lineNum] = [
                            'code'   => $classCode[$lineNum - 1],
                            'status' => 1
                        ];
                    } else {
                        $this->codeNotExecutable[$lineNum] = [
                            'code'   => $classCode[$lineNum - 1],
                            'status' => -2
                        ];
                    }
                }

                $this->codeExecutedCount      = count($this->codeExecuted);
                $this->codeNotExecutableCount = count($this->codeNotExecutable);
                $this->codeNotExecutedCount   = count($this->codeNotExecuted);

                $this->codeExecutionPercent = round(
                        ($this->codeExecutedCount / ($this->codeNotExecutedCount + $this->codeExecutedCount)) * 100,
                        2
                );

                if ($this->codeExecutionPercent <= 35) {
                    $this->codeCoverageStatus = "danger";
                } elseif ($this->codeExecutionPercent > 35 && $this->codeExecutionPercent <= 70) {
                    $this->codeCoverageStatus = "warning";
                } else {
                    $this->codeCoverageStatus = "success";
                }
                ?>
                <div class="alert alert-<?php echo $this->codeCoverageStatus; ?>">
                    <?php echo $this->getClassFileName(); ?>
                    code coverage status: <?php echo $this->codeExecutedCount; ?> lines of
                    <?php echo ($this->codeNotExecutedCount + $this->codeExecutedCount); ?>
                    (<?php echo $this->codeExecutionPercent . "%"; ?>)
                </div>
                <?php
//        echo "<pre>" . print_r($this->codeExecuted, true) . "</pre>";
//
//        echo "Stats:  notExecutable: {$this->codeNotExecutableCount}\n";
//        echo "Stats:   codeExecuted: {$this->codeExecutedCount}\n";
//        echo "Stats:    notExecuted: {$this->codeNotExecutedCount}\n\n";

                $this->fullCodeCoverageStatus = (
                    $this->codeExecuted + $this->codeNotExecuted + $this->codeNotExecutable
                    );
                ksort($this->fullCodeCoverageStatus);

                if ($showNotExecuted && ($this->codeNotExecutedCount > 0)) {
                    ?>
                    <div class="container-fluid">
                        <ul class="list-group">
                            <?php
                            foreach ($this->fullCodeCoverageStatus as $lineNum => $codeStatus) {
                                if ($codeStatus['status'] == "-1") {
                                    ?>
                                    <li class="list-group-item col-md-1 pull-left alert-danger">
                                        <a href="#line<?php echo $lineNum; ?>"><?php echo $lineNum; ?></a>
                                    </li>
                                    <?php
                                }
                            }
                            ?>
                        </ul>

                        <code>
                            <table class="table table-hover">
                                <tr>
                                    <th>#</th><th>Code line</th>
                                </tr>
                                <?php foreach ($this->fullCodeCoverageStatus as $lineNum => $codeStatus) { ?>
                                <tr  <?php echo " id='line$lineNum' ";
                                    if ($codeStatus['status'] == "1") {
                                        echo "class='success'";
                                    } elseif ($codeStatus['status'] == "-1") {
                                        echo "class='danger'";
                                    }
                                    ?>>
                                        <td>
                                            <?php echo $lineNum; ?>
                                        </td>
                                        <td>
                                            <?php echo str_replace(' ',
                                                               '&nbsp;',
                                                                htmlspecialchars($codeStatus['code'])); ?>
                                        </td>
                                    </tr>
            <?php } ?>
                            </table>
                        </code>
                    </div>
                    <?php
                }
            }

            /**
             *
             */
            public function __destruct()
            {
                if (is_callable("xdebug_start_code_coverage") && $this->getClassFileName()) {
                    $this->calculateCodeCoverage($this->getClassFileName(), true);
                    xdebug_stop_code_coverage();
                } else {
                    echo __METHOD__ . ": Unsupported code coverage.";
                }
            }

        }

<?php

require_once './EmptyTest.php';

use clagiordano\weblibs\php\BrowserInfo;

/**
 * Description of TestAuthentication
 *
 * @author Claudio Giordano <claudio.giordano@autistici.org>
 */
class TestBrowserInfo extends EmptyTest
{
    private $browserInfo = null;
    private $userAgentsList = [];

    protected function getClassFileName()
    {
        return "BrowserInfo.php";
    }

    public function testBasicUsage()
    {
        $this->browserInfo = new BrowserInfo();
        $this->assertIsA($this->browserInfo, "clagiordano\weblibs\php\BrowserInfo");

        $this->assertIsA(
            $this->browserInfo->getBrowserInfo(),
            "clagiordano\weblibs\php\BrowserInfo"
        );

        $this->assertTrue(
            $this->browserInfo->identificationStatus()
        );
    }

    public function testBasicUsageWithBrowscap()
    {
        $this->browserInfo = new BrowserInfo(true);
        $this->assertIsA($this->browserInfo, "clagiordano\weblibs\php\BrowserInfo");

        $this->assertIsA(
            $this->browserInfo->getBrowserInfo(),
            "clagiordano\weblibs\php\BrowserInfo"
        );

        $this->assertTrue(
            $this->browserInfo->identificationStatus()
        );
    }

    public function testGetUserAgentsListFromFile()
    {
        $fileCheck = file_exists(__CLASS__ . "Data.txt");
        $this->assertTrue($fileCheck);

        $this->userAgentsList = file(__CLASS__ . "Data.txt");
        $this->assertIsA($this->userAgentsList, "array");
        $this->assertTrue(count($this->userAgentsList) > 0);
    }

    public function testMassTestUserAgentsWithoutBrowscap()
    {
        set_time_limit(180);
        $this->browserInfo = new BrowserInfo();
        $this->assertIsA($this->browserInfo, "clagiordano\weblibs\php\BrowserInfo");

        $originalUserAgent = filter_input(INPUT_SERVER, 'HTTP_USER_AGENT');
        $this->assertTrue($originalUserAgent);

        foreach ($this->userAgentsList as $userAgent) {
            $this->browserInfo->setUserAgentString($userAgent);

            $this->assertEqual(
                $this->browserInfo->getUserAgentString(),
                $userAgent
            );

            $this->browserInfo->getBrowserInfo();

            $this->assertTrue(
                $this->browserInfo->identificationStatus(),
                "Failed identification for string '$userAgent'"
            );
        }
    }

    public function testMassTestUserAgentsWithBrowscap()
    {
        set_time_limit(240);
        $this->browserInfo = new BrowserInfo(true);
        $this->assertIsA($this->browserInfo, "clagiordano\weblibs\php\BrowserInfo");

        $originalUserAgent = filter_input(INPUT_SERVER, 'HTTP_USER_AGENT');
        $this->assertTrue($originalUserAgent);

        foreach ($this->userAgentsList as $userAgent) {
            $this->browserInfo->setUserAgentString($userAgent);

            $this->assertEqual(
                $this->browserInfo->getUserAgentString(),
                $userAgent
            );

            $this->browserInfo->getBrowserInfo(true);

            $this->assertTrue(
                $this->browserInfo->identificationStatus(),
                "Failed identification for string '$userAgent'"
            );
        }
    }
}
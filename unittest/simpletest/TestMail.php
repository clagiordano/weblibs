<?php

require_once './EmptyTest.php';

use clagiordano\weblibs\php\Mail;

/**
 * Description of TestMail
 *
 * @author Claudio Giordano <claudio.giordano@autistici.org>
 */
class TestMail extends EmptyTest
{
    const HOSTNAME = "localhost";
    const PROTOCOL = "smtp";
    const USERNAME = "claudio@localhost.localdomain";
    const PASSWORD = "password";
    const SUBJECT  = "Test subject";
    const BODY     = "Test body.";

    protected function getClassFileName()
    {
        return "Mail.php";
    }

    private $testRecipients = [
        [
            'type'  => 'to',
            'label' => 'test recipient1',
            'email' => 'claudio@localhost.localdomain'
        ],
        [
            'type'  => 'cc',
            'label' => 'test recipient2',
            'email' => 'pippo@localhost.localdomain'
        ],
        [
            'type'  => 'bcc',
            'label' => 'test recipient3',
            'email' => 'claudio@localhost.localdomain'
        ]
    ];
    private $testSender = [
        'type'  => 'from',
        'label' => 'test sender',
        'email' => 'claudio.giordano@autistici.org'
    ];

    function testInvalidUsage()
    {
        $this->expectException('Exception');

        $m = new Mail();
        $this->assertIsA($m, 'clagiordano\weblibs\php\Mail');
        $m->addAddress();
        $m->send();
    }

    function testBasicSendMail()
    {
        $m = new Mail();
        $this->assertIsA($m, 'clagiordano\weblibs\php\Mail');

        $m->setHostname(self::HOSTNAME);
        $this->assertEqual($m->getHostname(), self::HOSTNAME);

        $m->setProtocol(self::PROTOCOL);
        $this->assertEqual($m->getProtocol(), self::PROTOCOL);

        $m->setUsername(self::USERNAME);
        $this->assertEqual($m->getUsername(), self::USERNAME);

        $m->setPassword(self::PASSWORD);
        $this->assertEqual($m->getPassword(), self::PASSWORD);

        $m->setSubject(self::SUBJECT);
        $this->assertEqual($m->getSubject(), self::SUBJECT);

        $m->setBody(self::BODY);
        $this->assertEqual($m->getBody(), self::BODY);

        $m->addAddress($this->testSender['type'], $this->testSender['label'],
                       $this->testSender['email']);

        $m->addAddress($this->testRecipients[0]['type'],
                       $this->testRecipients[0]['label'],
                       $this->testRecipients[0]['email']);

        $m->addAddress($this->testRecipients[1]['type'],
                       $this->testRecipients[1]['label'],
                       $this->testRecipients[1]['email']);

        $m->addAddress($this->testRecipients[2]['type'],
                       $this->testRecipients[2]['label'],
                       $this->testRecipients[2]['email']);

        $this->assertIsA($m->getHeaders(), 'Array');
        //$this->assertIsA($m->getAttachments(), 'Array');
        $this->assertEqual($m->getAddressess(), $this->testRecipients);

        $this->assertTrue($m->send());
    }

    function testSendMailWithAttach()
    {
        $m = new Mail();
        $this->assertIsA($m, 'clagiordano\weblibs\php\Mail');

        $m->setHostname(self::HOSTNAME);
        $this->assertEqual($m->getHostname(), self::HOSTNAME);

        $m->setProtocol(self::PROTOCOL);
        $this->assertEqual($m->getProtocol(), self::PROTOCOL);

        $m->setUsername(self::USERNAME);
        $this->assertEqual($m->getUsername(), self::USERNAME);

        $m->setSubject(self::SUBJECT);
        $this->assertEqual($m->getSubject(), self::SUBJECT);

        $m->setBody(self::BODY);
        $this->assertEqual($m->getBody(), self::BODY);

        $this->assertTrue($m->send());
    }

    function testReadMail()
    {
        $m = new Mail();
        $this->assertIsA($m, 'clagiordano\weblibs\php\Mail');
        $m->read();
    }



}

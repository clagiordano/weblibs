<?php

require_once __DIR__ . "/../../vendor/autoload.php";
require_once __DIR__ . "/../../vendor/simpletest/simpletest/autorun.php";

/**
 * Description of AllTests
 *
 * @author Claudio Giordano <claudio.giordano@autistici.org>
 */
class AllTests extends TestSuite
{

    function __construct()
    {
        parent::__construct();

        foreach (glob(__DIR__ . "/Test*.php") as $testFile) {
            if (strpos($testFile, "Data.php") === false) {
                $this->addFile($testFile);
            }
        }
    }

}

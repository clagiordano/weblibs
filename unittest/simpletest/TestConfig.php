<?php

require_once './EmptyTest.php';

use clagiordano\weblibs\php\Config;

/**
 * Description of TestAuthentication
 *
 * @author Claudio Giordano <claudio.giordano@autistici.org>
 */
class TestConfig extends EmptyTest
{
    private $config = null;

    protected function getClassFileName()
    {
        return "Config.php";
    }

    public function testStart()
    {
        $this->config = new Config();
        $this->assertIsA($this->config, "clagiordano\weblibs\php\Config");
    }

    public function testBasicUsage()
    {
        $this->config = new Config();
        $this->assertIsA($this->config, "clagiordano\weblibs\php\Config");

        $this->config->loadConfig("TestConfigData.php");
        $this->assertTrue(
            $this->config->getValue('app')
        );
    }

    public function testFastUsage()
    {
        $this->config = new Config("TestConfigData.php");
        $this->assertIsA($this->config, "clagiordano\weblibs\php\Config");

        $this->assertTrue(
            $this->config->getValue('app')
        );
    }

    public function testFastInvalidKey()
    {
        $this->config = new Config("TestConfigData.php");
        $this->assertIsA($this->config, "clagiordano\weblibs\php\Config");

        $this->assertFalse(
            $this->config->getValue('invalidKey')
        );
    }

    public function testFastInvalidKeyWithDefault()
    {
        $this->config = new Config("TestConfigData.php");
        $this->assertIsA($this->config, "clagiordano\weblibs\php\Config");

        $this->assertEqual(
            $this->config->getValue('invalidKey', 'defaultValue'),
            'defaultValue'
        );
    }

    public function testFastNestedConfig()
    {
        $this->config = new Config("TestConfigData.php");
        $this->assertIsA($this->config, "clagiordano\weblibs\php\Config");

        $this->assertTrue(
            $this->config->getValue('other.multi.deep.nested')
        );
    }

    public function testCheckExistConfig()
    {
        $this->config = new Config("TestConfigData.php");
        $this->assertIsA($this->config, "clagiordano\weblibs\php\Config");

        $this->assertTrue(
            $this->config->existValue('other.multi.deep.nested')
        );
    }

    public function testCheckNotExistConfig()
    {
        $this->config = new Config("TestConfigData.php");
        $this->assertIsA($this->config, "clagiordano\weblibs\php\Config");

        $this->assertFalse(
            $this->config->existValue('invalid.config.path')
        );
    }

    public function testSetValue()
    {
        $this->config->setValue('other.multi.deep.nested', __FUNCTION__);

        $this->assertEqual(
            $this->config->getValue('other.multi.deep.nested'),
            __FUNCTION__
        );
    }

    public function testFailedSaveConfig()
    {
        $this->expectException();
        $this->config->saveConfigFile();
    }

    public function testSuccessSaveConfigOnTempAndReload()
    {
        $this->config->setValue('other.multi.deep.nested', "SUPERNESTED");
        $this->config->saveConfigFile("/tmp/testconfig.php", true);

        $this->assertEqual(
            $this->config->getValue('other.multi.deep.nested'),
            "SUPERNESTED"
        );
    }
}
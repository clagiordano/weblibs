<?php

require_once './EmptyTest.php';

use clagiordano\weblibs\php\Database;
use clagiordano\weblibs\php\Authentication;

/**
 * Description of TestAuthentication
 *
 * @author Claudio Giordano <claudio.giordano@autistici.org>
 */
class TestAuthentication extends EmptyTest
{
    private $authentication = null;
    private $database       = null;

    protected function getClassFileName()
    {
        return "Authentication.php";
    }

    public function testStart()
    {
        $this->database = new Database("localhost", "test", "test", "sample");
        $this->assertIsA($this->database, "clagiordano\weblibs\php\Database");

        $this->authentication = new Authentication($this->database, __CLASS__);
        $this->assertIsA($this->authentication, "clagiordano\weblibs\php\Authentication");
    }

    public function testCheckSession()
    {
        $this->assertFalse(
            $this->authentication->checkSession()
        );

        $this->assertFalse(
            $this->authentication->getUser()
        );
    }

    public function testFailedLogin()
    {
        $this->assertFalse(
            $this->authentication->logIn("invaliduser", "invalidpass")
        );

        $this->assertFalse(
            $this->authentication->getUser()
        );
    }

    public function testCheckSessionPostFail()
    {
        $this->assertFalse(
            $this->authentication->checkSession()
        );

        $this->assertFalse(
            $this->authentication->getUser()
        );
    }

    public function testSuccessLogin()
    {
        $this->assertTrue(
            $this->authentication->logIn("sample", "sample")
        );

        $this->assertTrue(
            $_SESSION[__CLASS__ . Authentication::SESSION_SUFFIX],
            'Missing session data object'
        );

        $this->assertIsA(
            unserialize($_SESSION[__CLASS__ . Authentication::SESSION_SUFFIX]),
            "clagiordano\weblibs\php\User"
        );
    }

    public function testCheckSessionPostSuccess()
    {
        $this->assertTrue(
            $this->authentication->checkSession()
        );

        $this->assertIsA(
            $this->authentication->getUser(),
            "clagiordano\weblibs\php\User"
        );

        $this->assertIsA(
            $this->authentication->getUser()->getUserName(),
            "string"
        );
    }

    public function testGetUser()
    {
        $this->assertIsA(
            $this->authentication->getUser(),
            "clagiordano\weblibs\php\User"
        );

        $this->assertIsA(
            $this->authentication->getUser()->getUserName(),
            "string"
        );
    }

    public function testSuccessLogout()
    {
        $this->authentication->logOut();

        $this->assertFalse(
            $_SESSION[__CLASS__ . Authentication::SESSION_SUFFIX],
            'Session data already present'
        );
    }

    public function testCheckSessionPostLogOut()
    {
        $this->assertFalse(
            $this->authentication->checkSession()
        );

        $this->assertFalse(
            $this->authentication->getUser()
        );
    }
}

<?php

require_once './EmptyTest.php';

use clagiordano\weblibs\php\User;

/**
 * Description of TestAuthentication
 *
 * @author Claudio Giordano <claudio.giordano@autistici.org>
 */
class TestUser extends EmptyTest
{
    private $user = null;

    protected function getClassFileName()
    {
        return "User.php";
    }

    public function testStart()
    {
        $this->user = new User("sample", "sample");
        $this->assertIsA($this->user, "clagiordano\weblibs\php\User");

        $this->user->printR();
    }
}
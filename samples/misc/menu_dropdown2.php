<link href="menu_dropdown2.css" rel="StyleSheet" type="text/css" />

<ul id="HDropdown-orange-classic">
	<li>
		<a href="#" title="Horizontal Css Menu">Horizontal Css Menu</a>
		<ul>
			<li><a href="#">Sub Menu 1</a></li>
			<li><a href="#">Sub Menu 1</a></li>
			<li><a href="#">Sub Menu 1</a></li>
			<li><a href="#">Sub Menu 1</a></li>
		</ul>
	</li>

	<li>
		<a href="#" title="Css Menu">Css Menu</a>
		<ul>
			<li><a href="#">Sub Menu 2</a></li>
			<li><a href="#">Sub Menu 2</a></li>
			<li><a href="#">Sub Menu 2</a></li>
			<li><a href="#">Sub Menu 2</a></li>
		</ul>
	</li>

	<li>
		<a href="#" title="Only Css Menu">Only Css Menu</a>
		<ul>
			<li><a href="#">Sub Menu 3</a></li>
			<li><a href="#">Sub Menu 3</a></li>
			<li><a href="#">Sub Menu 3</a></li>
			<li><a href="#">Sub Menu 3</a></li>
		</ul>
	</li>

	<li>
		<a href="#" title="Css Menu">Css Menu</a>
	</li>
</ul>

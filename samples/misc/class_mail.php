<?php

require "../php/Mail.php";

use weblibs\php\Mail;

$m = new Mail();

$m->setHostname("localhost");
$m->setProtocol("smtp");
$m->setUsername("claudio@localhost.localdomain");
$m->addAttachment("testfile.tmp");

$m->send();

var_dump($m);

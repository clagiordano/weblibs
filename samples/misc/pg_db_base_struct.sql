CREATE TABLE tab_config (
  id_config serial NOT NULL,
  opt varchar(20) NOT NULL,
  val varchar(20) NOT NULL
)

CREATE TABLE tab_menu (
  id_menu serial NOT NULL,
  link varchar(200) NOT NULL,
  titolo varchar(30) NOT NULL,
  perm varchar(30) NOT NULL DEFAULT 1,
  tip varchar(50) NOT NULL,
  titolo_pag varchar(50) NOT NULL,
  gruppo varchar(16) NOT NULL,
  act varchar(30) NOT NULL,
  page varchar(100) NOT NULL,
)

CREATE TABLE tab_session (
  id_session bigserial NOT NULL,
  session_id char(32) NOT NULL,
  id_user smallint NOT NULL,
  data timestamp NOT NULL,
  ip inet NOT NULL,
  action text NOT NULL,
  user_agent text NOT NULL
)

<?php

	/*
	 *  creazione automatica struttura tabella per domande multiple
	 * indicizzata con numero di domande
	 */

	$NOME_TAB 	 		= "tab_conv_frsbe_total";
	$PRIMARY_KEY 		= "id_conv_frsbe_total";


	$valori = Array("points","u_18_39_12_famy","u_18_39_sup12_famy","u_40_59_12_famy","u_40_59_sup12_famy","u_60_12_famy","u_60_sup12_famy","d_18_39_12_famy","d_18_39_sup12_famy","d_40_59_12_famy","d_40_59_sup12_famy","d_60_12_famy","d_60_sup12_famy","u_18_39_12_self","u_18_39_sup12_self","u_40_59_12_self","u_40_59_sup12_self","u_60_12_self","u_60_sup12_self","d_18_39_12_self","d_18_39_sup12_self","d_40_59_12_self","d_40_59_sup12_self","d_60_12_self","d_60_sup12_self");

	$struct_tab = "CREATE TABLE `$NOME_TAB` (
		`$PRIMARY_KEY` int(11) NOT NULL AUTO_INCREMENT, ";

	foreach ($valori as $key => $field)
	{
		$struct_tab .= "`$field` varchar(5) NOT NULL, ";
	}


	$struct_tab .= "PRIMARY KEY (`$PRIMARY_KEY`) ) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='latin1_swedish_ci'";

	echo $struct_tab . ";\n";
?>

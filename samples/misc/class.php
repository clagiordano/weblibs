<?php error_reporting(E_ALL); ini_set('display_errors', 1); session_start(); ?>
<!doctype html>
<html lang="it">
<!--
/*
 *      Gestione
 *
 *      Copyright 2010 Claudio Giordano <claudio.giordano@autistici.org>
 *
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 3 of the License, or
 *      (at your option) any later version.
 *
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 *      MA 02110-1301, USA.
 */
!-->
	<head>
		<meta charset="utf-8">
	</head>

	<body>

		<?php
			$root = substr($_SERVER['DOCUMENT_ROOT'], 0, -1);
			require_once("$root/Class/Database.php");

			echo "Debug => Test Class Page <br />";

			$d = new Database("localhost", "gestore", "polipo", "gestione");
			$d->GetRows("*", "tab_utenti", "", "", "1 limit 10", 1);

			$d->DrawControl("text", "prova", "controllo di prova");
			//~ $d->ReportHeader = "th,Data:,left|th,User:|th,Tipo:|th,Ip:|th,Action:";
			//~ $d->ReportFields = "td,data|td,user|td,tipo|td,ip|td,action";
		?>

		<form method="post" action="<?php echo $_SERVER['REQUEST_URI']; ?>">
			<table style="width: 100%;" class="dettaglio">
				<?php //$d->DrawReport(); ?>
			</table>
		</form>

	<?php echo "Debug => fine pagina <br />"; ?>

	</body>
</html>

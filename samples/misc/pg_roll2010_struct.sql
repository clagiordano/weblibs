CREATE TABLE tab_config (
  id_config serial NOT NULL,
  opt varchar(20) NOT NULL,
  val varchar(20) NOT NULL,
  PRIMARY KEY (id_config)
);

CREATE TABLE `tab_dett_requests` (
  `id_dettaglio` bigserial NOT NULL AUTO_INCREMENT,
  `id_request` int(11) NOT NULL,
  `id_macchina` int(11) NOT NULL,
  PRIMARY KEY (`id_dettaglio`)
);

CREATE TABLE `tab_ditte` (
  `id_ditta` int(11) NOT NULL AUTO_INCREMENT,
  `rag_soc` varchar(50) NOT NULL,
  PRIMARY KEY (`id_ditta`)
);

CREATE TABLE `tab_filiali` (
  `id_filiale` int(11) NOT NULL AUTO_INCREMENT,
  `cod_fil` varchar(11) NOT NULL,
  `den` varchar(100) NOT NULL,
  `ind` varchar(100) NOT NULL,
  `cap` varchar(5) NOT NULL,
  `citta` varchar(50) NOT NULL,
  `prov` varchar(2) NOT NULL,
  `reg` varchar(30) NOT NULL,
  `rol` varchar(80) NOT NULL,
  `tel` varchar(25) NOT NULL,
  `cell` varchar(25) NOT NULL,
  `mail` varchar(70) NOT NULL,
  `utenti` int(3) NOT NULL,
  `forn` varchar(30) NOT NULL,
  `variaz` varchar(20) NOT NULL,
  `dispo` varchar(30) NOT NULL,
  `id_stato` int(11) NOT NULL DEFAULT '5',
  `id_tecnico` int(11) NOT NULL DEFAULT '4',
  `data_pianif` date NOT NULL,
  `note_fil` longtext NOT NULL,
  `id_competenza` int(11) NOT NULL DEFAULT '5',
  `nome_srv` varchar(30) NOT NULL,
  PRIMARY KEY (`id_filiale`)
);

CREATE TABLE `tab_macchine` (
  `id_macchina` int(11) NOT NULL AUTO_INCREMENT,
  `provenienza` varchar(50) NOT NULL,
  `ddt_in` varchar(100) NOT NULL,
  `data_in` date NOT NULL,
  `data_st` date NOT NULL,
  `data_out` date NOT NULL,
  `allestimento` varchar(100) NOT NULL,
  `note_acc` longtext NOT NULL,
  `id_tipo_mch` int(11) NOT NULL,
  `cod_art` varchar(50) NOT NULL,
  `marca` varchar(50) NOT NULL,
  `modello` varchar(50) NOT NULL,
  `class_hw` varchar(50) NOT NULL,
  `ck_vis` varchar(50) NOT NULL,
  `ck_funz` varchar(50) NOT NULL,
  `serial` varchar(50) NOT NULL,
  `asset` varchar(50) NOT NULL,
  `pallet` varchar(50) NOT NULL,
  `id_magazzino` int(11) NOT NULL,
  `mac` varchar(20) NOT NULL,
  `hostname` varchar(30) NOT NULL,
  `ip` varchar(20) NOT NULL,
  `srv_code` varchar(30) NOT NULL,
  `id_stato_mch` int(11) NOT NULL DEFAULT '1',
  `dest_fil` varchar(100) NOT NULL,
  PRIMARY KEY (`id_macchina`)
);

CREATE TABLE `tab_magazzino` (
  `id_magazzino` int(11) NOT NULL AUTO_INCREMENT,
  `descr` varchar(50) NOT NULL,
  PRIMARY KEY (`id_magazzino`)
);

CREATE TABLE `tab_marche` (
  `tab_marche_id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`tab_marche_id`)
);

CREATE TABLE `tab_menu` (
  `id_menu` int(11) NOT NULL AUTO_INCREMENT,
  `link` varchar(200) NOT NULL,
  `titolo` varchar(50) NOT NULL,
  `perm` varchar(30) NOT NULL DEFAULT '',
  `tip` varchar(50) NOT NULL,
  `titolo_pag` varchar(50) NOT NULL,
  `gruppo` varchar(20) NOT NULL,
  `act` varchar(30) NOT NULL,
  `page` varchar(100) NOT NULL,
  PRIMARY KEY (`id_menu`)
);

CREATE TABLE `tab_modelli` (
  `tab_modelli_id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`tab_modelli_id`)
);

CREATE TABLE `tab_request` (
  `id_request` int(11) NOT NULL AUTO_INCREMENT,
  `id_filiale` int(11) NOT NULL DEFAULT '518',
  `request` varchar(100) NOT NULL DEFAULT '-',
  `data_request` date NOT NULL,
  `data_att` date NOT NULL,
  `report_cl` varchar(100) NOT NULL DEFAULT '-',
  `i_lett` varchar(200) NOT NULL DEFAULT '-',
  `qi_lett` int(2) NOT NULL,
  `i_prt` varchar(200) NOT NULL DEFAULT '-',
  `qi_prt` int(3) NOT NULL,
  `i_server` varchar(50) NOT NULL DEFAULT '-',
  `qi_server` int(3) NOT NULL,
  `sp_srv_new` int(3) NOT NULL,
  `sp_pdl` int(3) NOT NULL,
  `sp_las` int(3) NOT NULL,
  `sp_mon` int(3) NOT NULL,
  `sp_bar` int(3) NOT NULL,
  `sp_lett` int(3) NOT NULL,
  `sp_prt_lan` int(3) NOT NULL,
  `sp_altro_hw` int(3) NOT NULL,
  `st_pdl` int(3) NOT NULL,
  `st_srv` int(3) NOT NULL,
  `sos_pdl` int(3) NOT NULL,
  `sos_srv` int(3) NOT NULL,
  `sos_las` int(3) NOT NULL,
  `sos_lett` int(3) NOT NULL,
  `sos_bar` int(3) NOT NULL,
  `sos_mon` int(3) NOT NULL,
  `sos_prt_lan` int(3) NOT NULL,
  `ric_prt_lan` int(3) NOT NULL,
  `ric_multi` int(3) NOT NULL,
  `dis_pdl` int(3) NOT NULL,
  `dis_mon` int(3) NOT NULL,
  `dis_srv` int(3) NOT NULL,
  `dis_let` int(3) NOT NULL,
  `dis_cas` int(3) NOT NULL,
  `dis_fax` int(3) NOT NULL,
  `dis_bar` int(3) NOT NULL,
  `dis_las` int(3) NOT NULL,
  `dis_altro_hw` int(3) NOT NULL,
  `rit_pdl` int(3) NOT NULL,
  `rit_mon` int(3) NOT NULL,
  `rit_srv` int(3) NOT NULL,
  `rit_let` int(3) NOT NULL,
  `rit_cas` int(3) NOT NULL,
  `rit_las` int(3) NOT NULL,
  `rit_prt_lan` int(3) NOT NULL,
  `rit_bar` int(3) NOT NULL,
  `rit_fax` int(3) NOT NULL,
  `rit_altro_hw` int(3) NOT NULL,
  `nomi_prt_lan` varchar(200) NOT NULL DEFAULT '-',
  `nomi_multi` varchar(200) NOT NULL DEFAULT '-',
  `nome_srv_new` varchar(50) NOT NULL DEFAULT '-',
  `note_req` longtext NOT NULL,
  `i_mon` varchar(200) NOT NULL DEFAULT '-',
  `qi_mon` int(3) NOT NULL,
  `req_integ` varchar(100) NOT NULL,
  `note_integ` longtext NOT NULL,
  PRIMARY KEY (`id_request`)
);

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="it" lang="it">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<title>RollOut</title>
		<link href="/css/layoutmtop.css" rel="StyleSheet" type="text/css" />
		<link href="/css/templatechiaro.css" rel="StyleSheet" type="text/css" />
	</head>

	<body>
		<div id="container">
			<div id="header">
				<?php DrawHeader("Claudio Giordano", "Amministratore", "Layout di test"); ?>
			</div>

				<div id="menu">
					menu
				</div>

				<div id="body">
					body
				</div>

				<div id="footer">
					footer
				</div>
			</div>

		<div id="menu-voci"></div>
		<div id="popup"><!-- popup container !--></div>
		<div id="lsearch"><!-- lsearch container !--></div>


	</body>
</html>

package HelpAdde;
#: Version: 0.1
#: Description: -
#: Author: Claudio Giordano <claudio.giordano@autistici.org>

$::world->alias("elist", '/HelpAdde::PrintEvocazioni');
$::world->alias("nlist", '/HelpAdde::PrintNonMorti');

sub PrintEvocazioni
{
    $::world->echonl(::colorize("&Y+ ------------------------------ Lista Evocazioni ------------------------------ +"));
    $::world->echonl(::colorize("&Y+   Liv   Nome Evocazione         Classe            PF                           +"));
    $::world->echonl(::colorize("&Y+ ------------------------------------------------------------------------------ +"));
    $::world->echonl(::colorize("    &G[1] &w- &YAquila Gigante        &w- &CGuerriero       &w- &R62"));
    $::world->echonl(::colorize("    &G[1] &w- &YHobgoblin             &w- &CBarbaro         &w- &R94"));
    $::world->echonl(::colorize("    &G[1] &w- &YPiccolo Coboldo       &w- &CRanger          &w- &R"));
    $::world->echonl(::colorize("    &G[1] &w- &YRagno Rosso           &w- &CGuerriero       &w- &R64"));
    $::world->echonl(::colorize("    &G[1] &w- &YScorpione Mostruoso   &w- &CGuerriero       &w- &R"));
    $::world->echonl('');

    $::world->echonl(::colorize("    &G[2] &w- &YAzer                  &w- &CGuerriero       &w- &R177"));
    $::world->echonl(::colorize("    &G[2] &w- &YCubo Gelatinoso       &w- &CBarbaro         &w- &R"));
    $::world->echonl(::colorize("    &G[2] &w- &YDretch                &w- &CGuerriero       &w- &R181"));
    $::world->echonl(::colorize("    &G[2] &w- &YDriade                &w- &CChierico        &w- &R"));
    $::world->echonl(::colorize("    &G[2] &w- &YSerpente Mostruoso    &w- &CGuerriero       &w- &R"));
    $::world->echonl('');

    $::world->echonl(::colorize("    &G[3] &w- &YArpia                 &w- &CGuerriero       &w- &R"));
    $::world->echonl(::colorize("    &G[3] &w- &YBelva Distorcente     &w- &CGuerriero       &w- &R291"));
    $::world->echonl(::colorize("    &G[3] &w- &YEttin                 &w- &CBarbaro         &w- &R500"));
    $::world->echonl(::colorize("    &G[3] &w- &YLeone Crudele         &w- &CGuerriero       &w- &R288"));
    $::world->echonl(::colorize("    &G[3] &w- &YTroll                 &w- &CBarbaro         &w- &R577"));
    $::world->echonl('');

    $::world->echonl(::colorize("    &G[4] &w- &YGigante delle Colline &w- &CGuerriero       &w- &R423"));
    $::world->echonl(::colorize("    &G[4] &w- &YGigantesco Ogre       &w- &CBarbaro         &w- &R695"));
    $::world->echonl(::colorize("    &G[4] &w- &YLamia                 &w- &CGuerriero       &w- &R401"));
    $::world->echonl(::colorize("    &G[4] &w- &YRagno Fase            &w- &CGuerriero       &w- &R411"));
    $::world->echonl(::colorize("    &G[4] &w- &YStrega Annis          &w- &CMago            &w- &R208"));
    $::world->echonl('');

    $::world->echonl(::colorize("    &G[5] &w- &YGgante delle Pietre   &w- &CGuerriero       &w- &R550"));
    $::world->echonl(::colorize("    &G[5] &w- &YIdra			       			 &w- &CGuerriero       &w- &R528"));
    $::world->echonl(::colorize("    &G[5] &w- &YNalfeshnee        	   &w- &CGuerriero       &w- &R528"));
    $::world->echonl(::colorize("    &G[5] &w- &YOgre Magi		       	 &w- &CMago       	 	 &w- &R268"));
    $::world->echonl(::colorize("    &G[5] &w- &YSlaad			       		 &w- &CGuerriero       &w- &R528"));
    $::world->echonl(::colorize("&Y+ ------------------------------------------------------------------------------ +"));
}

sub PrintNonMorti
{
	$::world->echonl(::colorize("&Y+ ------------------------------- Lista NonMorti ------------------------------- +"));
	$::world->echonl(::colorize("&Y+   Liv   Nome Evocazione         Classe            PF                           +"));
	$::world->echonl(::colorize("&Y+ ------------------------------------------------------------------------------ +"));
	$::world->echonl(::colorize("    &G[ 3] &w- &YScheletro       			 &w- &C?       	 &w- &R"));
	$::world->echonl(::colorize("    &G[ 6] &w- &YZombi             		 &w- &C?         &w- &R"));
	$::world->echonl(::colorize("    &G[10] &w- &YGhoul       					 &w- &C?         &w- &R"));
	$::world->echonl(::colorize("    &G[13] &w- &YGolem       					 &w- &C?         &w- &R"));
	$::world->echonl(::colorize("    &G[16] &w- &YOmbra       					 &w- &C?         &w- &R"));
	$::world->echonl(::colorize("    &G[19] &w- &YWight       					 &w- &C?         &w- &R"));
	$::world->echonl(::colorize("    &G[21] &w- &YGhast       					 &w- &C?         &w- &R"));
	$::world->echonl(::colorize("    &G[24] &w- &YWraith       					 &w- &C?         &w- &R"));
	$::world->echonl(::colorize("    &G[27] &w- &YMummia-rossa       		 &w- &C?         &w- &R"));
	$::world->echonl(::colorize("    &G[30] &w- &YSpettro       		 		 &w- &C?         &w- &R"));
	$::world->echonl(::colorize("    &G[32] &w- &YScheletro-mago       	 &w- &C?         &w- &R"));
	$::world->echonl(::colorize("    &G[37] &w- &YMohrg       	 		     &w- &C?         &w- &R"));
	$::world->echonl(::colorize("    &G[40] &w- &YBanshee       	 		   &w- &C?         &w- &R128"));
	$::world->echonl('');
}

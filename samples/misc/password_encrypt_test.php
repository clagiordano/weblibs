<?php

    define("TEST_PASSWORD", "password");
    define("TEST_HASH", "$2y$10$4ewFPDpLp8M1JoNEZQ3lTeMq4a/9iU8.FpDxQSIXqvH2ZBqNFHG4a");

    function TimerCheck( $Start, $print=false )
    {
        $Diff = ( microtime(true) - $Start );

        if( $print === false )
        {
            return ( $Diff );
        }
        else {
            printf("Time: %fs<br />", $Diff );
        }
    }

    echo "<pre>";

    echo "Esempi di crittaggio password\n\n";

    $start = microtime(true);
    //printf( "[DEBUG][%10ds] MD5: '%s' => '%s'\n", TimerCheck( $start ), TEST_PASSWORD,  );
    printf( '[DEBUG]                      MD5: \'%s\' => \'%s\'                             - ', TEST_PASSWORD, md5( TEST_PASSWORD ) );
    TimerCheck( $start, true );

    $start = microtime(true);
    //printf( "[DEBUG][%10ds] MD5: '%s' => '%s'\n", TimerCheck( $start ), TEST_PASSWORD,  );
    printf( '[DEBUG]                     SHA1: \'%s\' => \'%s\'                     - ', TEST_PASSWORD, sha1( TEST_PASSWORD ) );
    TimerCheck( $start, true );

    $start = microtime(true);
    //printf( "[DEBUG][%10ds] MD5: '%s' => '%s'\n", TimerCheck( $start ), TEST_PASSWORD,  );
    printf( '[DEBUG] phash default PHP >= 5.5: \'%s\' => \'%s\' - ', TEST_PASSWORD, password_hash( TEST_PASSWORD, PASSWORD_DEFAULT ) );
    TimerCheck( $start, true );

    $start = microtime(true);
    //printf( "[DEBUG][%10ds] MD5: '%s' => '%s'\n", TimerCheck( $start ), TEST_PASSWORD,  );
    printf( '[DEBUG]  phash bcrypt PHP >= 5.5: \'%s\' => \'%s\' - ', TEST_PASSWORD, password_hash( TEST_PASSWORD, PASSWORD_BCRYPT ) );
    TimerCheck( $start, true );

    echo "\n\n";

    for( $i = 1; $i <= 10; $i++ )
    {
        $start = microtime(true);
        //printf( "[DEBUG][%10ds] MD5: '%s' => '%s'\n", TimerCheck( $start ), TEST_PASSWORD,  );
        printf( '[DEBUG]  phash bcrypt PHP >= 5.5: \'%s\' => \'%s\' - ', TEST_PASSWORD, password_hash( TEST_PASSWORD, PASSWORD_BCRYPT ) );
        TimerCheck( $start, true );
    }

    echo "\n\n";

    $start = microtime(true);
    if ( password_verify( TEST_PASSWORD, TEST_HASH ) )
    {
        echo "Password is valid! - ";
    } else {
        echo "Invalid password. - ";
    }
    TimerCheck( $start, true );

    echo "</pre>";

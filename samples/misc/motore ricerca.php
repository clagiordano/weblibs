<?php session_start(); error_reporting(E_ALL); ?>
<!doctype html>
<html lang="it">
<!--
/*
 *      prova
 *
 *      Copyright 2011 Claudio Giordano <claudio.giordano@autistici.org>
 *
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 3 of the License, or
 *      (at your option) any later version.
 *
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 *      MA 02110-1301, USA.
 */
!-->
	<head>
		<meta charset="utf-8">

		<title>Gestione</title>
		<meta name="description" content="Gestione" />
		<meta name="keywords" content="HTML,CSS,XML,JavaScript,MySQL,PHP" />
		<meta name="author" content="Claudio Giordano <claudio.giordano@autistici.org>" />

		<link href="/css/layoutmtop.css" rel="StyleSheet" type="text/css" />
		<link href="/css/templatechiaro.css" rel="StyleSheet" type="text/css" />
		<link href="/css/CalendarControl.css" rel="StyleSheet" type="text/css" />
		<link rel="icon" href="/Images/Favicons/fav_rm.ico" />
		<script type="text/javascript" src="/Function/javascript.js"></script>
		<script type="text/javascript" src="/Function/CalendarControl.js"></script>
		<script type="text/javascript" src="/Function/Ajax_Base.js"></script>
		<script type="text/javascript" src="/Class/Js/Forms.js"></script>
	</head>

	<body>
		<form action="http://www.google.com/search" method="get">
		 <label>Google: <input type="search" name="q"></label> <input type="submit" value="Search...">
		</form>
		<form action="http://www.bing.com/search" method="get">
		 <label>Bing: <input type="search" name="q"></label> <input type="submit" value="Search...">
		</form>

	</body>

	</html>

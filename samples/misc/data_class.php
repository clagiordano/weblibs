<!DOCTYPE html>
<head>
  <meta charset="utf-8">
</head>
<?php

    ini_set('display_startup_errors',1);
    ini_set('display_errors',1);
    error_reporting(-1);
    require_once '../php/DateTime.php';
    
    use weblibs\php\DateTime;

    echo "1: '" . ( new DateTime( "25/01/1984" ) )->inverti(  ) . "'<br />";
    echo "2: '" . ( new DateTime( "1984-01-25" ) )->inverti(  ) . "'<br />";
    echo "3: '" . ( new DateTime( "1984-01-25 02:03:00" ) )->inverti(  ) . "'<br />";
    echo "4: '" . ( new DateTime( "25-01-1984" ) )->inverti(  ) . "'<br />";

    $DataTest = new DateTime( "1984-01-25 02:03:00" );
    echo "TEST: '" . $DataTest->inverti() . "'<br />";
<?php

?>

<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>

<script type="text/javascript">

  function codeAddress() 
  {
		var geocoder = new google.maps.Geocoder();
    var address = document.getElementById("address").value;
    
    geocoder.geocode( { 'address': address}, function(results, status) {
      if (status == google.maps.GeocoderStatus.OK) {
	      //alert("coordinate estratte " + results);
	      //var [latitudine,longitudine] = results[0].geometry.location;
	      var MsgString = "<b>Verifica riuscita.</b>\n\n"
	    	  	+ "  Indirizzo: " + results[0].formatted_address + "\n"
	    			+ "   Location: " + results[0].geometry.location + "\n"
		      	+ " Latitudine: " + results[0].geometry.location.lat() + "\n"
		      	+ "Longitudine: " + results[0].geometry.location.lng() + "\n";

	      alert(MsgString);

	      document.getElementById('res_indirizzo').value = results[0].formatted_address;
	      document.getElementById('res_lat').value = results[0].geometry.location.lat();
	      document.getElementById('res_lng').value = results[0].geometry.location.lng();
	      
      } else {
        alert("Geocode was not successful for the following reason: " + status);
      }
    });
  }
</script>

<body>
  <div>
    <input id="address" type="text" value="verona">
    <input type="button" value="Encode" onclick="codeAddress()">
    
    <br>
    <br />
    Risultato:<br />
    <input id="res_indirizzo" type="text" value=""><br />
    <input id="res_lat" type="text" value=""><br />
    <input id="res_lng" type="text" value=""><br />
  </div>
</body>
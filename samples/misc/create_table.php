<?php

	/*
	 *  creazione automatica struttura tabella per domande multiple
	 * indicizzata con numero di domande
	 */

	$NOME_TAB 	 		= "tab_alssqol";
	$PRIMARY_KEY 		= "id_alssqol";
	$INIZIO_DOMANDE = "1";
	$NUM_DOMANDE 		= "10";

	$struct_tab = "CREATE TABLE `$NOME_TAB` (
		`$PRIMARY_KEY` int(11) NOT NULL AUTO_INCREMENT,
		`data` date NOT NULL,
		`data_insert` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP, ";

	for ($i = $INIZIO_DOMANDE; $i <= $NUM_DOMANDE; $i++)
	{
		$struct_tab .= "`col_$i` varchar(30) NOT NULL, ";
	}

	$struct_tab .= "PRIMARY KEY (`$PRIMARY_KEY`) ) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='latin1_swedish_ci'";

	echo $struct_tab . ";\n";
  
  echo "<br /><br />";
  // creazione automatica campi addizionali per la tabella sopra indicata:
  $struct_tab = "";
  for ($i = $INIZIO_DOMANDE; $i <= $NUM_DOMANDE; $i++)
	{
		$struct_tab .= "`col_$i_corr` tinyint(2) NOT NULL, ";
	}
  
  echo $struct_tab . ";\n";
?>

ALTER TABLE `tab_alssqol` ADD `d1_corr` INT( 2 ) NOT NULL ,
ADD `d2_corr` INT( 2 ) NOT NULL ,
ADD `d3_corr` INT( 2 ) NOT NULL ,
ADD `d4_corr` INT( 2 ) NOT NULL ,
ADD `d5_corr` INT( 2 ) NOT NULL ,
ADD `d6_corr` INT( 2 ) NOT NULL ,
ADD `d7_corr` INT( 2 ) NOT NULL ,
ADD `d8_corr` INT( 2 ) NOT NULL ,
ADD `d9_corr` INT( 2 ) NOT NULL ,
ADD `d10_corr` INT( 2 ) NOT NULL ,
ADD `d17_corr` INT( 2 ) NOT NULL ,
ADD `d22_corr` INT( 2 ) NOT NULL ,
ADD `d24_corr` INT( 2 ) NOT NULL ,
ADD `d25_corr` INT( 2 ) NOT NULL ,
ADD `d28_corr` INT( 2 ) NOT NULL ,
ADD `d31_corr` INT( 2 ) NOT NULL ,
ADD `d32_corr` INT( 2 ) NOT NULL ,
ADD `d34_corr` INT( 2 ) NOT NULL ,
ADD `subtot_col1` INT( 5 ) NOT NULL ,
ADD `subtot_col2` INT( 5 ) NOT NULL ,
ADD `avgtot_test` INT( 5 ) NOT NULL ,
ADD `avgnegative` INT( 5 ) NOT NULL ,
ADD `avginteraction` INT( 5 ) NOT NULL ,
ADD `avgintimacy` INT( 5 ) NOT NULL ,
ADD `avgreligiosity` INT( 5 ) NOT NULL ,
ADD `avgphisical` INT( 5 ) NOT NULL 



ALTER TABLE `tab_alssqol` CHANGE `avgtot_test` `avgtot_test` FLOAT( 5, 2 ) NOT NULL ,
CHANGE `avgnegative` `avgnegative` FLOAT( 5, 2 ) NOT NULL ,
CHANGE `avginteraction` `avginteraction` FLOAT( 5, 2 ) NOT NULL ,
CHANGE `avgintimacy` `avgintimacy` FLOAT( 5, 2 ) NOT NULL ,
CHANGE `avgreligiosity` `avgreligiosity` FLOAT( 5, 2 ) NOT NULL ,
CHANGE `avgphisical` `avgphisical` FLOAT( 5, 2 ) NOT NULL ,
CHANGE `avgbulbar` `avgbulbar` FLOAT( 5, 2 ) NOT NULL 

rename
ALTER TABLE `tab_alssqol` CHANGE `avgphisical` `avgphysical` FLOAT( 5, 2 ) NOT NULL 
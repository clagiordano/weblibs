#!/usr/local/bin/perl
#############################################################
##                                                         ##
## Procmail RC Webmin                                      ##
## Copyright (c) 2003 The University of Vermont            ##
## Written: June 19, 2001                                  ##
## Updated: March 3, 2003                                  ##
## By James Flemer <jflemer@alum.rpi.edu>                  ##
##                 <jflemer@uvm.edu>                       ##
##    Aaron Hawley <ashawley@uvm.edu> (MarkUp/Style)       ##
##                                                         ##
## Please see the LICENSE file for licensing requirements. ##
##                                                         ##
#############################################################
# $Id: procbuilder.pl,v 1.45 2003/03/03 22:59:55 jflemer Exp $
#############################################################
my $rcsid = '$Id: procbuilder.pl,v 1.45 2003/03/03 22:59:55 jflemer Exp $';
my $VERSION =  '$Revision: 1.45 $';
   $VERSION =~ s/.*:\s*([0-9.]*).*/$1/;
#############################################################
use strict;
use CGI;
$CGI::POST_MAX=1024 * 100;  # max 100K posts
$CGI::DISABLE_UPLOADS = 1;  # no uploads
#############################################################

#############################################################
## Configuration [GLOBALS]                                 ##
#############################################################
## Set to true to enable UVM's config.
my $UVM       = 0;
my $UVMtest   = 0;

## Client's username.
my $user      =  $ENV{'REMOTE_USER'};

## Location of user's resources
my $rc_file   = "/home/$user/.procmailrc";
my $log_file  = "/home/$user/.procmail.log";
my $mail_dir  = "/home/$user/mail";

## Location of this script. (eg. "/cgi-bin/procbuilder.pl")
my $self      = $ENV{'SCRIPT_NAME'};

## Base URL for this script.
my $base      =  "http://$ENV{'SERVER_NAME'}$self";
   $base      =~ s/(.*\/).*/$1/o;

## Title for generated pages (tags stripped in <title>...</title>).
my $title     = 'Procmail <acronym title="resource configuration">RC</acronym> <abbr title="Web-Based Interface for System Administration">Webmin</abbr>';

## Location of CSS stylesheet.
my $style     = 'procstyle.css';

## Location of HTML help file.
my $help      = 'prochelp.html';

## Webmaster Email address.
my $email     = "webmaster@$ENV{'SERVER_NAME'}";

## Author
my $author    = 'James Flemer';

## UVM Specific Config
if ($UVM) {
	$user     =  $ENV{'REMOTE_USER'};
	$user     =~ s/^(.)(.)/$1\/$2\/$1$2/o;
	$rc_file  =  "/.../uvm.edu/fs/mailhome/$user/.procmailrc";
	$log_file =  "/.../uvm.edu/fs/mailhome/$user/procmail.log";
	$mail_dir =  "/.../uvm.edu/fs/mailhome/$user/mail";
	$self     =  $ENV{'SCRIPT_NAME'};
	$base     =  "https://$ENV{'SERVER_NAME'}$self";
	$base     =~  s/(.*\/).*/$1/o;
	$title    =  'UVM Email Filtering';
	$style    =  '/account/procstyle.css';
	$help     =  '/account/prochelp.html';
	$email    =  'root@uvm.edu';
	$author   =  'the <a href="http://www.uvm.edu/">University of Vermont</a>';

	if($UVMtest) {
		$base  =~ s/https/http/o;
		$style =~ s/account/~jflemer\/proc/o;
		$help  =~ s/account/~jflemer\/proc/o;
	}
}

#############################################################
## Globals                                                 ##
#############################################################
local *RCFH;  # Filehandle for RC file
my $rc_md5;   # MD5 of rc_file;
my $level;    # User level [ default: "Basic" ]
my $page;     # Current page [ default: "Main Menu" ]
my $status;   # Status String.
my $errstr;   # Error String.
my %recipe;   # "Current" recipe.

              # The %recipe has the following fields:
			  #  o "a"           Single interpreted action
			  #  o "action"      Action of recipe
			  #  o "all"         Full recipe verbatim
			  #  o "c"           Single interpreted condition
			  #  o "conditions"  Array of conditions
			  #  o "custom"      True if custom recipe
			  #  o "desc"        String description line(s)
			  #  o "flags"       String of flags
			  #  o "lock"        True if lock on
			  #  o "lockfile"    Explicit local lockfile
			  #  o "regex"       True if regex recipe
			  #  o "type"        Recipe type

#############################################################
## Procmail RC Functions                                   ##
#############################################################

#######
## Get the MD5 checksum of the $rc_file.
sub ChecksumRc {
	$_ = `/usr/local/bin/md5 $rc_file` or return undef;
	s/.* = (.*)\n/$1/;
	$rc_md5 = $_;
	$rc_md5;
} # end sub ChecksumRc

#######
## Open the $rc_file, return it's checksum.
sub OpenRc {
	if ( ! -e $rc_file ) {
		my $umask = umask(077);
		open( RCFH, ">$rc_file" ) or return undef;
		print RCFH "## $rc_file   (created ". localtime() .")\n";
		print RCFH <<EOF;
#PATH=/usr/bin:/usr/ucb:/bin:/usr/local/bin
MAILDIR=\$HOME/mail
DEFAULT=\$MAIL
LOGFILE=\$HOME/procmail.log
LOGABSTRACT=all
#LOGABSTRACT=no
#VERBOSE=yes
VERBOSE=no
SHELL=/bin/sh

EOF
		#flush( RCFH );
		close( RCFH );
		umask($umask);
	}
	open( RCFH, "+<$rc_file" ) or return undef;
	ChecksumRc($rc_file);
	$rc_md5;
} # end sub OpenRc

#######
## Close the $rc_file.
sub CloseRc {
	#flush(RCFH);
	close( RCFH );
} # end sub CloseRc

#######
## Read the next recipe into %recipe.
sub NextRecipe {
	my $desc        = '';
	my $type        = '';
	my $regex       = 0;

	while($_ = <RCFH>) {
		chop;

		# environment variable
		if      ( m/^\s*([A-Z]+)=([^#]*)#?.*$/o )      {
			$desc  = '';
			$type  = '';
			$regex = 0;

		# WEBPROCRC directive
		} elsif ( m/^\s*#%WP:\s*(.*)$/o )       {
			my $cmd;
			my $args;
			($cmd, $args) = split( /\s+/, $1, 2 );

			if($cmd eq 'BEGIN') {
				$desc  = '';
				$type  = '';
				$regex = 0;

			} elsif ($cmd eq 'END') {
				$desc  = '';
				$type  = '';
				$regex = 0;

			} elsif ($cmd eq 'DESC' ) {
				if ( ! ( $args =~ m/^\s*$/o ) ) {
					$desc .= " $args";
				}

			} elsif ($cmd eq 'TYPE' ) {
				$type  = $args;

			} elsif ($cmd eq 'REGEX' ) {
				$regex++;

			} else {
				# unknown directive, ignore
			}

		# comment
		} elsif ( m/^\s*#(.*)$/o )           {

		# begin recipe
		} elsif ( m/^\s*:([0-9])\s*([^#]*)#?.*$/o )       {
			my $flags = $2;    # recipe flags
			my $lock;          # 1 if locking on for this
			my $locallockfile; # lockfile
			my $action;        # action
			my $custom;        # custom
			my @conditions;    # list of conditions
			my $c;             # condition we care about
			my $a;             # action we care about

			$custom = 0;
			$lock   = ( $flags =~ m/:/o );
			($flags, $locallockfile) = split( /\s*:\s*/o, $flags, 2 );
			if ( $type =~ m/custom/io ) { $custom++; }
			if ( $flags =~ m/[^\sD]/o ) { $custom++; }

			RECIPE:
			while ( $_ = <RCFH> ) {
				chop;

				if ( m/^\s*#(.*)$/o ) {
					# comment

				} elsif ( m/^\s*{/o ) { # }
					# nested recipes ...
					my $nestlev = 1;
					$nestlev -= m/\}/;
					$custom++;

					push ( @conditions, $_ );
					while( $nestlev > 0 && ( $_ = <RCFH> ) ) {
						chop;

						if    ( m/^\s*\{.*\}/o ) { }
						elsif ( m/^\s*{/o ) { $nestlev++; }
						elsif ( m/^\s*}/o ) { $nestlev--; }
						push ( @conditions, $_ );
					}
				} elsif ( m/^\s*\*\s*(.*)$/o ) {
					# condition
					(my $tmp = $1) =~ s/\s+$//o;
					push ( @conditions, "* $tmp" );
				} else {
					last RECIPE;
				}
			} # end RECIPE

			($action = $_) =~ s/#.*$//o;
			$custom += abs $#conditions;

			$c = '';
			$a = '';

			my $detected_type = '';
			if ( $action =~ m/^\s*(\/dev\/null)/o || $action =~ m/^\s*(\$HOME\/mail\/blocked-email)/o ) {
				$detected_type  = 'b';
				$a = $1;
			} elsif ( $action =~ m/^\s*!\s*(.*)$/o ) {
				$detected_type  = 'w';
				$a = $1;
			} elsif ( $action =~ m/^\s*(\w.*)/io ) {
				$detected_type  = 'f';
				$a = $1;
			} else {
				$detected_type  = 'x';
				$a = $action;
				$custom++;
			}

			if     ( $conditions[0] =~ m/^\* \^Subject:\.\*(.*)/io ) {
				$detected_type .= 's';
			} elsif( $conditions[0] =~ m/^\* \^TO_(.*)\(\$\|\[>, \]\)/o ) {
				$detected_type .= 'l';
			} elsif( $conditions[0] =~ m/^\* \^\(From\|Reply-To\):\.\*\[ <\](.*)\(\$\|\[> \]\)/io ) {
				$detected_type .= 'e';
			} else {
				$detected_type .= 'x'; # unknown/custom
				$custom++;
			}
			$c = $1;

			if ( $type eq '' ) {
				$type = $detected_type;
			}
			if ( $type eq 'bl' ) { # no such rule
				$custom++;
			}
			if ( $custom > 1 ) {
				$type = 'custom';
			}

			if( $custom < 1 ) {
				if ( $regex < 1 ) {
					if ( $c =~ m/[^\\][+?({|})\$\[\]]/o ) {
						$regex++;
					}
				}

				if( $regex < 1 ) {
					$c  =~ s/\.\*/*/g;
					$c  =~ s/\\([.+?({|})\$\[\]\\])/$1/g;
				}
			}

			my $recipe;
			if($lock) {
				$recipe = ":0$flags:$locallockfile";
			} else {
				$recipe = ":0$flags";
			}
			if (scalar @conditions) {
				$recipe .= "\n" . join("\n", @conditions);
			}
			if($action ne '') {
				$recipe .= "\n$action";
			}

			$desc =~ s/^\s*//o;

			%recipe = ();
			$recipe{"a"}          = $a;
			$recipe{"all"}        = $recipe;
			$recipe{"custom"}     = ($custom > 0) ? 1 : 0;
			$recipe{"regex"}      = ($regex  > 0) ? 1 : 0;
			$recipe{"type"}       = $type;
			$recipe{"desc"}       = $desc;
			$recipe{"flags"}      = $flags;
			$recipe{"lock"}       = $lock ? 1 : 0;
			$recipe{"lockfile"}   = $locallockfile;
			$recipe{"conditions"} = \@conditions;
			$recipe{"c"}          = $c;
			$recipe{"action"}     = $action;

			return 1;
		} # end if ... recipe
	}
	return 0;
} # end sub NextRecipe

#######
## Replace the Nth recipe in $rc_file with %recipe.
##  ReplaceRecipe N;
sub ReplaceRecipe {
	my $dst  = shift;

	if ( $dst eq undef || $dst < 1 ) {
		$status = 'Invalid recipe number.';
		return 0;
	}

	# seek to beginning of rc
	seek( RCFH, 0, 0 );

	# read in @arr and @s
	my @arr   = ();
	my @s     = ();
	my @t     = ();
	my $p     = 0;
	my $n     = 1;
	LOOPA: while ( $_ = <RCFH> ) {
		chop;
		if ( $n == $dst ) {
			# read the dst into @t instead of @arr
			if ( m/^\s*$/o ) {
				if    ( $#t > 0 ) {
					push( @t, $_ );
				} else            {
					push( @arr, $_ );
				}
			} elsif ( m/^\s*#%WP:\s*(DESC|REGEX|TYPE)/o ) {
				push( @t, $_ );
			} elsif ( m/^\s*:[0-9]/o ) {
				$n++;
				@s = @t;
				@t = ();
				push( @s, $_ );
				while ( $_ = <RCFH> ) {
					chop;
					push( @s, $_ );
					if ( m/^\s*[*#]/o ) {
						# no-op
					} elsif ( m/^\s*\{/o ) { # }
						my $nest = 1;
						while ( $nest > 0 && ($_ = <RCFH>) ) {
							chop;
							push( @s, $_ );
							if    ( m/^\s*\{/o ) { $nest++; }
							elsif ( m/^\s*\}/o ) { $nest--; }
						}
					} else {
						$p = $#arr;
						next LOOPA;
					}
				}
				$p = $#arr;
			} else {
				for( my $z = 0; $z < $#t; $z++ ) {
					push( @arr, $t[$z] );
				}
				push( @arr, $_ );
				@t = ();
			}
			next LOOPA;
		} # end if $n == $src

		push( @arr, $_ );
		if ( m/^\s*:[0-9]/o ) {
			$n++;
			LOOPB: while ( $_ = <RCFH> ) {
				chop;
				push( @arr, $_ );
				if ( m/^\s*[*#]/o ) {
					# no-op
				} elsif ( m/^\s*\{/o ) { # }
					my $nest = 1;
					while ( $nest > 0 && ($_ = <RCFH>) ) {
						chop;
						push( @arr, $_ );
						if    ( m/^\s*\{/o ) { $nest++; }
						elsif ( m/^\s*\}/o ) { $nest--; }
					}
				} else {
					last LOOPB;
				}
			} # end LOOPB
		}
	} # end while / LOOPA

	# now we have the dst recipe in @s, and the rest of the rc in @arr.
	# insert new recipe at $p

	# remove whitespace around $p
	my $q = $p;
	while ( $p > 0      && ( $arr[$p] =~ m/^\s*$/o ) ) { $p--; }
	while ( $q <= $#arr && ( $arr[$q] =~ m/^\s*$/o ) ) { $q++; }

	@s = ('', RecipeToArray(), '' );

	# rewind file and write out new one
	seek(RCFH, 0, 0);
	truncate(RCFH, 0);
	print RCFH join("\n", @arr[0 .. $p], @s, @arr[$q .. $#arr] ), "\n";
	#flush(RCFH);
	seek(RCFH, 0, 0);

	$status = 'Recipe changed.';
	1;
} # end sub ReplaceRecipe

#######
## Remove the Nth recipe in $rc_file.
##  RemoveRecipe N;
sub RemoveRecipe {
	my $dst  = shift;

	if ( $dst eq undef || $dst < 1 ) {
		$status = 'Invalid recipe number.';
		return 0;
	}

	# seek to beginning of rc
	seek( RCFH, 0, 0 );

	# read in @arr and @s
	my @arr   = ();
	my @s     = ();
	my @t     = ();
	my $p     = 0;
	my $n     = 1;
	LOOPA: while ( $_ = <RCFH> ) {
		chop;
		if ( $n == $dst ) {
			# read the dst into @t instead of @arr
			if ( m/^\s*$/o ) {
				if    ( $#t > 0 ) {
					push( @t, $_ );
				} else            {
					push( @arr, $_ );
				}
			} elsif ( m/^\s*#%WP:\s*(DESC|REGEX|TYPE)/o ) {
				push( @t, $_ );
			} elsif ( m/^\s*:[0-9]/o ) {
				$n++;
				@s = @t;
				@t = ();
				push( @s, $_ );
				while ( $_ = <RCFH> ) {
					chop;
					push( @s, $_ );
					if ( m/^\s*[*#]/o ) {
						# no-op
					} elsif ( m/^\s*\{/o ) { # }
						my $nest = 1;
						while ( $nest > 0 && ($_ = <RCFH>) ) {
							chop;
							push( @s, $_ );
							if    ( m/^\s*\{/o ) { $nest++; }
							elsif ( m/^\s*\}/o ) { $nest--; }
						}
					} else {
						$p = $#arr;
						next LOOPA;
					}
				}
				$p = $#arr;
			} else {
				for( my $z = 0; $z < $#t; $z++ ) {
					push( @arr, $t[$z] );
				}
				push( @arr, $_ );
				@t = ();
			}
			next LOOPA;
		} # end if $n == $src

		push( @arr, $_ );
		if ( m/^\s*:[0-9]/o ) {
			$n++;
			LOOPB: while ( $_ = <RCFH> ) {
				chop;
				push( @arr, $_ );
				if ( m/^\s*[*#]/o ) {
					# no-op
				} elsif ( m/^\s*\{/o ) { # }
					my $nest = 1;
					while ( $nest > 0 && ($_ = <RCFH>) ) {
						chop;
						push( @arr, $_ );
						if    ( m/^\s*\{/o ) { $nest++; }
						elsif ( m/^\s*\}/o ) { $nest--; }
					}
				} else {
					last LOOPB;
				}
			} # end LOOPB
		}
	} # end while / LOOPA

	# now we have the dst recipe in @s, and the rest of the rc in @arr.
	# remove whitespace around $p
	my $q = $p;
	while ( $p > 0      && ( $arr[$p] =~ m/^\s*$/o ) ) { $p--; }
	while ( $q <= $#arr && ( $arr[$q] =~ m/^\s*$/o ) ) { $q++; }

	@s = ('');

	# rewind file and write out new one
	seek(RCFH, 0, 0);
	truncate(RCFH, 0);
	print RCFH join("\n", @arr[0 .. $p], @s, @arr[$q .. $#arr]), "\n";
	#flush(RCFH);
	seek(RCFH, 0, 0);

	$status = 'Recipe removed.';
	1;
} # end sub RemoveRecipe

#######
## Count the number of recipes.
sub CountRecipes {
	my $num = 0;
	RefreshRc();

	while ( NextRecipe() ) { $num++; }
	RefreshRc();
	%recipe = ();

	$num;
} # end sub CountRecipes

#######
## Add the %recipe into $rc_file at the end.
sub AddRecipe {

	# seek to beginning of rc
	seek( RCFH, 0, 0 );

	# read in @arr and @s
	my @arr   = ();
	while ( $_ = <RCFH> ) {
		chop;
		push ( @arr, $_ );
	}
	my $p = $#arr;

	# remove whitespace around $p
	while ( $p > 0 && ( $arr[$p] =~ m/^\s*$/o ) ) { $p--; }

	my @s = ('', RecipeToArray() );

	# rewind file and write out new one
	seek(RCFH, 0, 0);
	truncate(RCFH, 0);
	print RCFH join("\n", @arr[0 .. $p], @s), "\n";
	#flush(RCFH);
	seek(RCFH, 0, 0);

	$status = qq|Recipe "$recipe{'desc'}" added.|;
	1;
} # end AddRecipe

#######
## Insert the %recipe into $rc_file before [after]
## the Nth recipe.
##  InsertRecipe N [, ("before"|"after") ];
sub InsertRecipe {
	my $dst  = shift;
	my $loc  = shift;

	# seek to beginning of rc
	seek( RCFH, 0, 0 );

	# read in @arr and @s
	my @arr   = ();
	my @s     = ();
	my @t     = ();
	my $p     = 0;
	my $n     = 1;
	LOOPA: while ( $_ = <RCFH> ) {
		chop;
		if ( $n == $dst ) {
			# read the dst into @t instead of @arr
			if ( m/^\s*$/o ) {
				if    ( $#t > 0 ) {
					push( @t, $_ );
				} else            {
					push( @arr, $_ );
				}
			} elsif ( m/^\s*#%WP:\s*(DESC|REGEX|TYPE)/o ) {
				push( @t, $_ );
			} elsif ( m/^\s*:[0-9]/o ) {
				$n++;
				@s = @t;
				@t = ();
				push( @s, $_ );
				while ( $_ = <RCFH> ) {
					chop;
					push( @s, $_ );
					if ( m/^\s*[*#]/o ) {
						# no-op
					} elsif ( m/^\s*\{/o ) { # }
						my $nest = 1;
						while ( $nest > 0 && ($_ = <RCFH>) ) {
							chop;
							push( @s, $_ );
							if    ( m/^\s*\{/o ) { $nest++; }
							elsif ( m/^\s*\}/o ) { $nest--; }
						}
					} else {
						$p = $#arr;
						next LOOPA;
					}
				}
				$p = $#arr;
			} else {
				for( my $z = 0; $z < $#t; $z++ ) {
					push( @arr, $t[$z] );
				}
				push ( @arr, $_ );
				@t = ();
			}
			next LOOPA;
		} # end if $n == $src

		push( @arr, $_ );
		if ( m/^\s*:[0-9]/o ) {
			$n++;
			LOOPB: while ( $_ = <RCFH> ) {
				chop;
				push( @arr, $_ );
				if ( m/^\s*[*#]/o ) {
					# no-op
				} elsif ( m/^\s*\{/o ) { # }
					my $nest = 1;
					while ( $nest > 0 && ($_ = <RCFH>) ) {
						chop;
						push( @arr, $_ );
						if    ( m/^\s*\{/o ) { $nest++; }
						elsif ( m/^\s*\}/o ) { $nest--; }
					}
				} else {
					last LOOPB;
				}
			} # end LOOPB
		}
	} # end while / LOOPA

	# now we have the dst recipe in @s, and the rest of the rc in @arr.
	# insert new recipe at $p

	# remove whitespace around $p
	my $q = $p;
	while ( $p > 0      && ( $arr[$p] =~ m/^\s*$/o ) ) { $p--; }
	while ( $q <= $#arr && ( $arr[$q] =~ m/^\s*$/o ) ) { $q++; }

	if ( $loc ne undef && $loc eq "before" ) {
		@s = ( '', RecipeToArray(), '', @s, '' );
	} else {
		@s = ( '', @s, '', RecipeToArray(), '');
	}

	# rewind file and write out new one
	seek(RCFH, 0, 0);
	truncate(RCFH, 0);
	print RCFH join("\n", @arr[0 .. $p], @s, @arr[$q .. $#arr]), "\n";
	#flush(RCFH);
	seek(RCFH, 0, 0);

	$status = 'Recipe added.';
	1;
} # end sub InsertRecipe

#######
## Move the recipe A to B
##  MoveRecipe A, B;
sub MoveRecipe {
	my $src  = shift;
	my $dst  = shift;

	if ( $src == $dst ) {
		$status = 'Recipe moved.';
		return 1;
	}

	# seek to beginning of rc
	seek( RCFH, 0, 0 );

	# read in @arr and @s
	my @arr   = ();
	my @s     = ();
	my @t     = ();
	my $p     = 0;
	my $n     = 1;
	LOOPA: while ( $_ = <RCFH> ) {
		chop;
		if ( $n == $src ) {
			# read the src into @t instead of @arr
			if ( m/^\s*$/o ) {
				if    ( $#t > 0 ) {
					push( @t, $_ );
				} else            {
					push( @arr, $_ );
				}
			} elsif ( m/^\s*#%WP:\s*(DESC|REGEX|TYPE)/o ) {
				push( @t, $_ );
			} elsif ( m/^\s*:[0-9]/o ) {
				$n++;
				@s = @t;
				@t = ();
				push( @s, $_ );
				while ( $_ = <RCFH> ) {
					chop;
					push( @s, $_ );
					if ( m/^\s*[*#]/o ) {
						# no-op
					} elsif ( m/^\s*\{/o ) { # }
						my $nest = 1;
						while ( $nest > 0 && ($_ = <RCFH>) ) {
							chop;
							push( @s, $_ );
							if    ( m/^\s*\{/o ) { $nest++; }
							elsif ( m/^\s*\}/o ) { $nest--; }
						}
					} else {
						$p = $#arr;
						next LOOPA;
					}
				}
				$p = $#arr;
			} else {
				for( my $z = 0; $z < $#t; $z++ ) {
					push( @arr, $t[$z] );
				}
				push( @arr, $_ );
				@t = ();
			}
			next LOOPA;
		} # end if $n == $src

		push( @arr, $_ );
		if ( m/^\s*:[0-9]/o ) {
			$n++;
			LOOPB: while ( $_ = <RCFH> ) {
				chop;
				push( @arr, $_ );
				if ( m/^\s*[*#]/o ) {
					# no-op
				} elsif ( m/^\s*\{/o ) { # }
					my $nest = 1;
					while ( $nest > 0 && ($_ = <RCFH>) ) {
						chop;
						push( @arr, $_ );
						if    ( m/^\s*\{/o ) { $nest++; }
						elsif ( m/^\s*\}/o ) { $nest--; }
					}
				} else {
					last LOOPB;
				}
			} # end LOOPB
		}
	} # end while / LOOPA

	# now we have the source in @s, and the rest of the rc in @arr.

	# scan @arr for where to insert
	$n = 1;
	my $p = 0;
	my $t = 0;
	LOOPC: while ( ($p+$t) <= $#arr ) {
		$_ = $arr[$p+$t];
		if ( $n == $dst ) {
			if ( m/^\s*$/o ) {
				if   ( $t > 0 ) { $t++; }
				else            { $p++; }
			} elsif ( m/^\s*#%WP:\s*(DESC|REGEX|TYPE)/o ) {
				$t++;
			} elsif ( m/^\s*:[0-9]/o ) {
				$n++;
				$t = 0;
				$p--;
				last LOOPC;
			} else {
				$p += $t + 1;
				$t  = 0;
			}
			next LOOPC;
		}

		$p++;

		if ( m/^\s*:[0-9]/o ) {
			$n++;
			while ( $p <= $#arr ) {
				$_ = $arr[$p];
				$p++;
				if ( m/^\s*[*#]/o ) {
					# no-op
				} elsif ( m/^\s*\{/o ) { # }
					my $nest = 1;
					while ( $nest > 0 && $p <= $#arr ) {
						$_ = $arr[$p];
						$p++;
						if    ( m/^\s*\{/o ) { $nest++; }
						elsif ( m/^\s*\}/o ) { $nest--; }
					}
				} else {
					next LOOPC;
				}
			} # end while
		} # end match :0
	} # end LOOPC

	# insert @s at $p

	# rewind file and write out new one
	seek(RCFH, 0, 0);
	truncate(RCFH, 0);
	print RCFH join("\n", @arr[0 .. $p], @s, @arr[$p .. $#arr]), "\n";
	#flush(RCFH);
	seek(RCFH, 0, 0);

	$status = 'Recipe moved.';
	1;
} # end sub MoveRecipe

#######
## Rewind $rc_file. Next NextRecipe() will retrieve the
## 1st recipe.
sub RefreshRc {
	#flush(RCFH);
	seek(RCFH, 0, 0);
} # end sub RefreshRc

#######
## Return entire rc file in an array.
sub FetchFullRc {
	my @arr = ();

	#flush(RCFH);

	seek(RCFH, 0, 0);
	while ( <RCFH> ) { chop; push @arr, $_; }
	seek(RCFH, 0, 0);

	@arr;
} # end sub FetchFullRc

#######
## Write entire rc file from an array.
sub WriteFullRc {
	my @arr = shift;

	# rewind file and write out new one
	seek(RCFH, 0, 0);
	truncate(RCFH, 0);
	print RCFH join("\n", @arr), "\n";
	#flush(RCFH);
	seek(RCFH, 0, 0);

	$status = 'Procmail RC written.';
	1;
} # end sub WriteFullRc

#######
## Return %recipe as an array of lines.
## Suitable for writing to .procmailrc.
sub RecipeToArray {
	my @r = ();
	if ( ! ( $recipe{'desc'} =~ m/^\s*$/ ) ) { push ( @r, "#%WP: DESC $recipe{'desc'}"); }
	if($recipe{"type"} ne '') {
		push ( @r, "#%WP: TYPE $recipe{'type'}" );
	}
	if($recipe{"regex"} > 0) {
		push ( @r, "#%WP: REGEX true" );
	}
	if($recipe{"custom"} || $recipe{"type"} eq 'custom') {
		push ( @r, $recipe{"all"} );
	} else {
		if($recipe{"lock"}) {
			push ( @r, ":0$recipe{'flags'}:$recipe{'lockfile'}" );
		} else {
			push ( @r, ":0$recipe{'flags'}" );
		}
		foreach my $c (@{$recipe{"conditions"}}) {
			push ( @r, $c );
		}
		push ( @r, $recipe{'action'} );
	}

	@r;
} # end sub RecipeToArray

#############################################################
## CGI Functions                                           ##
#############################################################

#######
## Return an array of the title in HTML.
sub TitleMarkUp {
	my @arr = ();
	push @arr, qq|<h1>$title</h1>|;
	push @arr,  q|<p class="rcfileblock"><small class="localtime">|. localtime(). q|</small><br />|;
	push @arr, qq|<small class="check-sum">$rc_md5</small></p>|;

	if ( $errstr ne "" ) {
		push @arr, qq|<p class="error">$errstr</p>|;
		$errstr = "";
	}

	@arr;
} # end sub TitleMarkUp

#######
## Return an array of the menu in HTML.
## Aaron's Auto Menu Code
sub MenuMarkUp {
	my $extra = shift;

	my @arr = ();

	if ($extra eq undef) {
		$extra = '';
	} else {
		$extra = "&amp;$extra";
	}

	my $str = '';
	$str .= qq|<p class="menu">[ |;
	if ($level ne 'Basic') {
		$str .= qq|<a href="$self?page=$page&amp;level=b$extra">Basic View</a>|;
	}
	else {
		$str .= '<strong>Basic View</strong>';
	}
	$str .= ' | ';
	if ($level ne 'Advanced') {
		$str .= qq|<a href="$self?page=$page&amp;level=a$extra">Advanced View</a>|;
	}
	else {
		$str .= '<strong>Advanced View</strong>';
	}
	$str .= qq! | <a href="$self">Main Menu</a> | <a href="$help#$page" target="prochelp">Help</a> ]</p>!;

	push @arr, $str;

	@arr;
} # end sub MenuMarkUp

#######
## Return an array with an HTML table with the RC file.
sub RcPage {

	RefreshRc();
	my @arr = ();

	push @arr, TitleMarkUp(), MenuMarkUp();

	my $num = 0;

	RECIPE:	while( NextRecipe() ) {
		$num++;

		my $type = $recipe{"type"};
		my $act;
		my $con;
		my $dst;

		if    ( $type eq 'be' ) { $act = 'Block';   $con = 'Email';   $dst = ''; }
		elsif ( $type eq 'bs' ) { $act = 'Block';   $con = 'Subject'; $dst = ''; }
		elsif ( $type eq 'fe' ) { $act = 'Filter';  $con = 'Email';   $dst = 'Folder'; }
		elsif ( $type eq 'fs' ) { $act = 'Filter';  $con = 'Subject'; $dst = 'Folder'; }
		elsif ( $type eq 'fl' ) { $act = 'Filter';  $con = 'List';    $dst = 'Folder'; }
		elsif ( $type eq 'we' ) { $act = 'Forward'; $con = 'Email';   $dst = 'Forward To'; }
		elsif ( $type eq 'ws' ) { $act = 'Forward'; $con = 'Subject'; $dst = 'Forward To'; }
		elsif ( $type eq 'wl' ) { $act = 'Forward'; $con = 'List';    $dst = 'Forward To'; }
		else                    { $act = 'Custom';  $con = '';        $dst = ''; }

		push @arr, '';
		push @arr, qq|<h3 class="recipe-name">Recipe #$num: $act $con</h3>|;
		if ( $level eq "Basic" ) {
			if ( $recipe{"custom"} ) {
				push @arr, qq|<p class="warning">This is a Custom recipe. To edit it, switch to the <a href="$self?page=view&amp;level=a">Advanced View</a>.</p>|;
				push @arr, qq|<hr />|;
				next RECIPE;
			}
			if ( $recipe{"regex"}  ) {
				push @arr, qq|<p class="warning">This recipe uses Regular Expressions. To edit it, switch to the <a href="$self?page=view&amp;level=a">Advanced View</a>.</p>|;
				push @arr, qq|<hr />|;
				next RECIPE;
  			}
		}

		push @arr, qq|<form method="post" action="$self" id="recipe-$num">|;
		push @arr, qq|<p>|;
		push @arr, qq|<input type="hidden" name="checksum" value="$rc_md5" />|;
		push @arr, qq|<input type="hidden" name="number"   value="$num" />|;
		push @arr, qq|<input type="hidden" name="type"     value="$type" />|;
		if ( $level ne "Basic" ) {
			push @arr, qq|<input type="hidden" name="level"    value="$level" />|;
		}
		push @arr, qq|<strong class="description">Description</strong> <input type="text" name="desc" size="50" value="$recipe{'desc'}" /><br />|;

		if ( $recipe{"custom"} ) {
			my $rows = $#{$recipe{"conditions"}} + 3;
			push @arr, CGI::textarea(-cols=>'60', -rows=>$rows, -name=>'custom', -wrap=>'off', -default=>$recipe{'all'}, -override=>1);
			push @arr, "<br />";
		} else {
			my $case  = ($recipe{"flags"} =~ m/D/o) ? ' checked="checked"' : '';
			my $regex = ($recipe{"regex"})          ? ' checked="checked"' : '';
			my $lock  = ($recipe{"lock"})           ? ' checked="checked"' : '';
			my $lockfile = $recipe{"lockfile"};

			push @arr, qq|<strong class="option">Case Matters</strong> <input name="case" type="checkbox" value="true"$case /><br />|;
			if ( $level ne "Basic" ) {
				push @arr, qq|<strong class="option">Regex</strong> <input name="regex" type="checkbox" value="true"$regex /><br />|;
				push @arr, qq|<strong class="option">Lock</strong> <input name="lock" type="checkbox" value="true"$lock /> File: <input type="text" name="lockfile" size="30" value="$lockfile" /><br />|;
			} else {
				if ( $recipe{"regex"} ) {
					push @arr,  q|<input type="hidden" name="regex"    value="true" />|;
				}
				if ( $recipe{"lock"} ) {
					push @arr,  q|<input type="hidden" name="lock"     value="true" />|;
					push @arr, qq|<input type="hidden" name="lockfile" value="$lockfile" />|;
				}
			}

			push @arr, qq|<strong class="condition">$con</strong> <input name="con" type="text" size="40" value="$recipe{"c"}" /><br />|;

			if ( $dst ne '' ) {
				push @arr, qq|<strong class="operation">$dst</strong> <input name="dst" type="text" size="40" value="$recipe{"a"}" /><br />|;
			}
		}
		push @arr,  q|<input type="submit" name="replace" value="  Save  " /><input type="submit" name="remove" value=" Remove " />|;
  	push @arr,  q|</p>|;
  	push @arr,  q|</form>|;
    push @arr,  q|<hr />|;
	} # end RECIPEs



	if ( $num < 1 ) {
		push @arr, qq|<p class="error">Your Procmail <acronym title="resource configuration">RC</acronym> is empty! Go back to the <a href="$self">Main Menu</a>, or <a href="$self?page=add">Add A New Recipe</a>.</p>|
	}

	push @arr, MenuMarkUp();

	@arr;
} # end sub rcpage

#######
## Return an array with all of users folders.
sub LookupFolders {
	my $cwd = chdir($mail_dir);
	my $str = `/bin/find . -type f`;
	$str =~ s/^\.\///mgo;
	my @arr = sort(split(/\n/, $str));
	@arr;
} # end sub LookupFolders

#######
## Return an array with a HTML table to create new recipes.
sub CreatePage {
	my @arr  = ();

	my $folders = qq|<option value="" selected="true"></option>\n|;
	foreach my $folder (LookupFolders()) {
		$folders .= qq|<option value="$folder">$folder</option>\n|;
	}
	$folders .= qq|<option value="">New Folder --&gt;</option>\n|;

	push @arr, TitleMarkUp(), MenuMarkUp();
	push @arr,  q|<hr />|;

#push @arr, <<EOF;
#<p>Type of recipe you want:</p>
#<ul class="submenu">
# <li><strong><a href="$self?$ENV{"QUERY_STRING"}#block">Block</a></strong>
#  <ul>
#   <li><a href="$self?$ENV{"QUERY_STRING"}#blockaddress">by Address</a></li>
#   <li><a href="$self?$ENV{"QUERY_STRING"}#blocksubject">by Subject</a></li>
#  </ul>
# </li>
# <li><strong><a href="$self?$ENV{"QUERY_STRING"}#filter">Filter to Folder</a></strong>
#  <ul>
#   <li><a href="$self?$ENV{"QUERY_STRING"}#filteraddress">by Address</a></li>
#   <li><a href="$self?$ENV{"QUERY_STRING"}#filterlistaddress">by List Address</a></li>
#   <li><a href="$self?$ENV{"QUERY_STRING"}#filtersubject">by Subject</a></li>
#  </ul>
# </li>
# <li><strong><a href="$self?$ENV{"QUERY_STRING"}#forward">Forward Message</a></strong>
#  <ul>
#   <li><a href="$self?$ENV{"QUERY_STRING"}#forwardaddress">by Address</a></li>
#   <li><a href="$self?$ENV{"QUERY_STRING"}#forwardlistaddress">by List Address</a></li>
#   <li><a href="$self?$ENV{"QUERY_STRING"}#forwardsubject">by Subject</a></li>
#  </ul>
# </li>
#</ul>
#<hr />
#EOF;

	push @arr, <<EOF;
<h3><a name="filter"></a>Folder Filtering Rules (Recipes)</h3>
<h4 class="recipe-name"><a name="filteraddress"></a>Filter Email by &quot;From:&quot; Address</h4>
<form method="post" action="$self" id="form-fe">
<p class="recipe">
<input type="hidden" name="type" value="fe" />
<strong class="condition">&quot;From:&quot; Address To Match:</strong> <input type="text" size="30" name="con" /><br />
<strong class="operation">Folder To Save To:</strong> <select name="dst">$folders</select><input type="text" size="20" name="new" /><input type="submit" name="create" value="Filter" /><br />
<small class="description">This recipe will save any mail to a folder matching the specified &quot;From:&quot; email address.</small>
</p>
</form>

<h4 class="recipe-name"><a name="filterlistaddress"></a>Filter Email by &quot;To:&quot; or List Address</h4>
<form method="post" action="$self" id="form-fl">
<p class="recipe">
<input type="hidden" name="type" value="fl" />
<strong class="condition">&quot;To:&quot; or List Address To Match:</strong> <input type="text" size="30" name="con" /><br />
<strong class="operation">Folder To Save To:</strong> <select name="dst">$folders</select><input type="text" size="20" name="new" /><input type="submit" name="create" value="Filter" /><br />
<small class="description">This recipe will save any mail to a folder sent to the specified &quot;To:&quot; or List email address.</small>
</p>
</form>

<h4 class="recipe-name"><a name="filtersubject"></a>Filter Email by Subject</h4>
<form method="post" action="$self" id="form-fs">
<p class="recipe">
<input type="hidden" name="type" value="fs" />
<strong class="condition">Subject To Match:</strong> <input type="text" size="30" name="con" /><br />
<strong class="operation">Folder To Save To:</strong> <select name="dst">$folders</select><input type="text" size="20" name="new" /><input type="submit" name="create" value="Filter" /><br />
<small class="description">This recipe will save any mail to a folder matching the specified subject.</small>
</p>
</form>

<hr />

<h3><a name="block"></a>Block Rules (Recipes)</h3>
<h4 class="recipe-name"><a name="blockaddress"></a>Block Email by &quot;From:&quot; Address</h4>
<form method="post" action="$self" id="form-be">
<p class="recipe">
<input type="hidden" name="type" value="be" />
<strong class="condition">&quot;From:&quot; Address To Block:</strong> <input type="text" size="30" name="con" /><input type="submit" name="create" value="Block" /><br />
<small class="description">This recipe will <strong>delete</strong> any mail received matching the specified &quot;From:&quot; email address.</small>
</p>
</form>

<h4 class="recipe-name"><a name="blocksubject"></a>Block Email by Subject</h4>
<form method="post" action="$self" id="form-bs">
<p class="recipe">
<input type="hidden" name="type" value="bs" />
<strong class="condition">Subject To Block:</strong> <input type="text" size="30" name="con" /><input type="submit" name="create" value="Block" /><br />
<small class="description">This recipe will <strong>delete</strong> any mail received matching the specified subject.</small>
</p>
</form>

<hr />

<h3><a name="forward"></a>Forwarding Rules (Recipes)</h3>
<h4 class="recipe-name"><a name="forwardaddress"></a>Forward Email by &quot;From:&quot; Address</h4>
<form method="post" action="$self" id="form-we">
<p class="recipe">
<input type="hidden" name="type" value="we" />
<strong class="condition">&quot;From:&quot; Address To Match:</strong> <input type="text" size="30" name="con" /><br />
<strong class="operation">Address To Forward To:</strong> <input type="text" size="30" name="dst" /><input type="submit" name="create" value="Forward" /><br />
<small class="description">This recipe will forward any mail matching the specified &quot;From:&quot; email address.</small>
</p>
</form>

<h4 class="recipe-name"><a name="forwardlistaddress"></a>Forward Email by &quot;To:&quot; or List Address</h4>
<form method="post" action="$self" id="form-wl">
<p class="recipe">
<input type="hidden" name="type" value="wl" />
<strong class="condition">&quot;To:&quot; or List Address To Match:</strong> <input type="text" size="30" name="con" /><br />
<strong class="operation">Address To Forward To:</strong> <input type="text" size="30" name="dst" /><input type="submit" name="create" value="Forward" /><br />
<small class="description">This recipe will forward any mail sent to the specified &quot;To:&quot; or List email address.</small>
</p>
</form>

<h4 class="recipe-name"><a name="forwardsubject"></a>Forward Email by Subject</h4>
<form method="post" action="$self" id="form-ws">
<p class="recipe">
<input type="hidden" name="type" value="ws" />
<strong class="condition">Subject To Match:</strong> <input type="text" size="30" name="con" /><br />
<strong class="operation">Address To Forward To:</strong> <input type="text" size="30" name="dst" /><input type="submit" name="create" value="Forward" /><br />
<small class="description">This recipe will forward any mail matching the specified subject.</small>
</p>
</form>

<hr />

EOF
	if ( $level ne "Basic" ) {
		push @arr, <<EOF;
<h4 class="recipe-name"><a name="custom"></a>Custom Rule (<em>For Advanced Users</em>)</h4>
<form method="post" action="$self" id="form-custom">
<p>
<input type="hidden" name="type" value="custom" />
<textarea cols="60" rows="6" name="custom" wrap="off"></textarea><br />
<input type="submit" name="create" value="Custom" />
</p>
</form>
EOF
	}

	push @arr, MenuMarkUp();
	@arr;
} # end sub CreatePage

#######
## Return an array with a HTML table to edit rc by hand.
sub ManualPage {
	my @arr = ();

	push @arr, TitleMarkUp();
	push @arr, qq|<form method="post" action="$self" id="fullrc">|;
	push @arr,  q|<p>|;
	push @arr, qq|<input type="hidden" name="checksum" value="$rc_md5" />|;
	push @arr, CGI::textarea(-cols=>'80', -rows=>'24', -name=>'full', -default=>join ("\n", FetchFullRc()), -override=>1);
	push @arr, "<br />\n";
	push @arr,  q|<input type="submit" name="saveall" value="Save" /><input type="submit" name="reset" value="Cancel" />|;
	push @arr,  q|</p>|;
	push @arr,  q|</form>|;

	@arr;
} # end sub ManualPage

#######
## Return an array with a the log file.
## Show the last N lines. [Default: N=10]
##  LogPage N;
sub LogPage {
	my @arr = ();
	my $num = shift;

	if ($num eq undef) {
		$num = 10;
	}

	my $l = $level;
	$l =~ s/^(.)(.*)/\l$1/;

	push @arr, TitleMarkUp(), MenuMarkUp("n=$num");
	push @arr, qq!<p class="menu">[ <a href="$self?page=$page&amp;level=$l&amp;n=10">Last 10 Lines</a> | <a href="$self?page=$page&amp;level=$l&amp;n=100">Last 100 Lines</a> | <a href="$self?page=$page&amp;level=$l&amp;n=500">Last 500 Lines</a> | <a href="$self?page=$page&amp;level=$l&amp;n=0">Entire Log</a> ]</p>!;
	push @arr,  q|<pre class="log">|;

	my @log = ();
	if ($num > 0) {
		@log = split(/\n/, `tail -$num $log_file`);
	} else {
		@log = split(/\n/, `cat $log_file`);
	}

	for(my $j = 0; $j<$#log; $j++) {
		#$_ = CGI::escapeHTML($log[$j]);
		$_ = $log[$j];
		s/&/&amp;/go;
		s/"/&quot;/go;
		s/</&lt;/go;
		s/>/&gt;/go;
		if ( m/^(From) ([^@]+@[^ ]+)  (.*)$/o ) {
			my $str = qq|<div class="logentry"><span class="keyword">$1</span> <span class="email">$2</span>  <span class="date">$3</span><br />|;
			$_ = $log[++$j];
			s|^ (\w+): (.*)$|&nbsp;<span class="keyword">$1</span>: <span class="subject">$2</span><br />|;
			$str .= $_;
			$_ = $log[++$j];
			s|^  (\w+): (.*) +(\d+)$|&nbsp;&nbsp;<span class="keyword">$1</span>: <span class="folder">$2</span> <span class="bytes">$3</span></div>|;
			$str .= $_;
			push @arr, $str;
		} elsif ( s/^procmail://o ) {
			my $quot    = "&quot;";
			my $notquot = "&([^q]|q[^u]|qu[^o]|quo[^t]|quot[^;])";
			s/($quot([^&]|$notquot)*$quot)/<span class="quote">$1<\/span>/go;
			if ($level eq 'Advanced') {
				push @arr, qq|<span class="singlelogentry"><span class="keyword">procmail</span>:$_</span>|;
			}
		} else {
			if ($level eq 'Advanced') {
				push @arr, qq|<span class="rawlogentry">$_</span>|;
			}
		}
	}

	push @arr,  q|</pre>|;
	push @arr, MenuMarkUp("n=$num");
	@arr;
} # end sub LogPage

#######
## Return an array with status page.
sub ConfirmPage {
	my @arr = ();
	my $qstr = shift;
	$qstr =~ s/&?confirm=[^&]*//io;

	#push @arr, TitleMarkUp();

	push @arr, qq|<form method="GET">|;
	if ($errstr ne "") {
		push @arr, qq|<p class="error">$errstr</p>|;
		push @arr,  q|<p><input type="button" value="Close" onclick="javascript:window.close();" /></p>|;
	} else {
		push @arr, qq|<p class="status">Are you sure you want to permanently $recipe{'desc'}?</p>|;
		push @arr, qq|<p><input type="button" value="Yes" onclick="javascript:window.location='$self?$qstr';" /> <input type="button" value="No" onclick="javascript:window.close();" /></p>|;
	}
	push @arr,  q|</form>|;

	@arr;
} # end sub ConfirmPage

#######
## Return an array with status page.
sub StatusPage {
	my @arr = ();

	#push @arr, TitleMarkUp();

	push @arr,  q|<form method="GET">|;
	if ($errstr ne "") {
		push @arr, qq|<p class="error">$errstr</p>|;
	} else {
		push @arr, qq|<p class="status">$status</p>|;
	}
	push @arr, qq|<p><input type="button" value="Close" onclick="javascript:window.close();" /></p>|;
	push @arr,  q|</form>|;

	@arr;
} # end sub StatusPage

#######
## Return an array with main page.
sub MainPage {
	my @arr = ();

	push @arr, TitleMarkUp();

	push @arr,  q|<ul class="menu">|;
	push @arr, qq|<li class="menu"><a href="$self?page=view"  >View / Edit / Remove Recipes</a></li>|;
	push @arr, qq|<li class="menu"><a href="$self?page=add"   >Add A New Recipe</a></li>|;
	push @arr, qq|<li class="menu"><a href="$self?page=order" >Change The Order Of Recipes</a> <em class="note"><noscript lang="javascript"><p>Requires JavaScript!</p></noscript></em></li>|;
	push @arr, qq|<li class="menu"><a href="$self?page=manual">Edit Procmail <acronym title="resource configuration">RC</acronym> By Hand</a> <em class="note">(Advanced Users Only)</em></li>|;
	push @arr, qq|<li class="menu"><a href="$self?page=log"   >View your Procmail log file</a></li>|;
	push @arr, qq|<li class="menu"><a href="$help" target="prochelp">Help Overview</a></li>|;
	push @arr,  q|</ul>|;

	@arr;
} # end sub MainPage

#######
## Return an array with page to change recipe order.
sub OrderPage {
	my $max = CountRecipes();

	my @arr = ();

	push @arr, TitleMarkUp(), MenuMarkUp();
	push @arr,  q|<noscript>|;
	push @arr,  q|<p class="error">Sorry, you must have JavaScript enabled for this page to work.</p>|;
	push @arr,  q|</noscript>|;
	push @arr, qq|<form method="post" action="$self" id="order" name="order">|;
	push @arr,  q|<p>|;
	push @arr, qq|<input type="hidden" name="checksum" value="$rc_md5" />|;
	push @arr,  q|<input type="hidden" name="move"     value="true" />|;
	push @arr,  q|<input type="hidden" name="src"      value="" />|;
	push @arr,  q|<input type="hidden" name="dst"      value="" />|;
	if ( $level ne "Basic" ) {
		push @arr, qq|<input type="hidden" name="level"    value="$level" />|;
	}

	my $num = 0;

	RECIPE:	while( NextRecipe() ) {
		$num++;

		my $type = $recipe{"type"};

		my $desc = $recipe{"desc"};
		if ( $desc =~ m/^\s*$/o ) {
			my $act;
			my $dst;
			my $a    = $recipe{'a'};
			my $c    = $recipe{'c'};

			if    ( $type eq 'be' ) { $act = 'Block Email From ';     $dst = '';    $a = ''; }
			elsif ( $type eq 'bs' ) { $act = 'Block Subject ';        $dst = '';    $a = ''; }
			elsif ( $type eq 'fe' ) { $act = 'Filter Email From ';    $dst = ' To Folder '; }
			elsif ( $type eq 'fs' ) { $act = 'Filter Subject ';       $dst = ' To Folder '; }
			elsif ( $type eq 'fl' ) { $act = 'Filter List Address ';  $dst = ' To Folder '; }
			elsif ( $type eq 'we' ) { $act = 'Forward Email From ';   $dst = ' To '; }
			elsif ( $type eq 'ws' ) { $act = 'Forward Subject ';      $dst = ' To '; }
			elsif ( $type eq 'wl' ) { $act = 'Forward List Address '; $dst = ' To '; }
			else                    { $act = 'Custom Recipe';         $dst = '';    $a = ''; $c = ''; }

			$desc = "$act$c$dst$a";
		} else {
			$desc = "$desc";
		}

		my $select = '';
		for ( my $i = 0; $i < $max; ) {
			$i++;
			if( $i == $num ) {
				$select .= qq|<option value="$i" selected="selected">$i</option>\n|;
			} else {
				$select .= qq|<option value="$i">$i</option>\n|;
			}
		}

		my $box = qq|<select name="dst$num" size="1" onchange="javascript:document.order.src.value=$num;document.order.dst.value=this.form.dst$num.options[this.form.dst$num.selectedIndex].value;this.form.submit();">\n|;

		push @arr, qq|$box$select</select> $desc<br />|;
	} # end RECIPEs

	push @arr,  q|</p>|;
	if ( $num < 1 ) {
		push @arr, qq|<p class="error">Your Procmail <acronym title="resource configuration">RC</acronym> is empty! Go back to the <a href="$self">Main Menu</a>, or <a href="$self?page=add">Add A New Recipe</a>.</p>|
	}
	push @arr,  q|</form>|;

	push @arr, MenuMarkUp();

	@arr;
} # end sub OrderPage

#######
## Read CGI Query and build %recipe from it.
sub QueryToRecipe {
	my $q = shift;  # CGI query object
	my $m = shift;  # method

	my $type  = $q->param('type');   # type
	my $desc  = $q->param('desc');   # description

	%recipe = ();

	$recipe{"type"}  = $type;
	$recipe{"desc"}  = $desc;

	if ( $type ne "custom" ) {
		my $con      = $q->param('con');      # condition
		my $dst      = $q->param('dst');      # destination (action)
		my $new      = $q->param('new');      # new destination (action)
		my $regex    = $q->param('regex');    # true if uses regex
		my $case     = $q->param('case');     # true if case sensitive
		my $lock     = $q->param('lock');     # true if uses lockfile
		my $lockfile = $q->param('lockfile'); # lockfile name

		if ( $con eq undef || $con =~ m/^\s*$/o ) {
			%recipe = ();
			$errstr = 'Error! You entered an invalid recipe. Please try again.';
			return 0;
		}

		$recipe{"c"}        = $con;
		$recipe{"lock"}     = ($lock eq 'true') ? 1 : 0;
		$recipe{"lockfile"} = $lockfile;
		$recipe{"action"}   = "";

		if ( $case  ne undef ) { $recipe{"flags"} = 'D'; }

		if ( $regex ne undef && $regex eq 'true') {
			$recipe{"regex"} = 1;
		} else {
			$con  =~ s/([.+?({|})\$\[\]\\])/\\$1/g;
			$con  =~ s/\*/.*/g;
		}

		if      ( $type =~ m/^b/o ) {
			$recipe{"action"}   = '$HOME/mail/blocked-email';
			$recipe{"lock"}     = 1;
			$recipe{"lockfile"} = undef;
		} elsif ( $type =~ m/^w/o ) {
			if ( $dst ne undef ) {
				$recipe{"action"} = "! $dst";
			}
		} elsif ( $type =~ m/^f/o ) {
			if ( $dst eq undef && $new ne undef ) {
				open(TMP, ">>$mail_dir/$new"); syswrite(TMP, "touch", 0); close(TMP);
				$dst = $new;
			}
			$recipe{"action"} = $dst;
			if ( $m eq 'create' ) { $recipe{"lock"} = 1; }
		}
		$recipe{"a"}        = $dst;

		if($recipe{"action"} eq "") {
			%recipe = ();
			$errstr = 'Error! You entered an invalid recipe. Please try again.';
			return 0;
		}

		if      ( $type =~ m/^.e/o ) {
			$recipe{"conditions"} = [ "* ^(From|Reply-To):.*[ <]".$con."(\$|[> ])" ];
		} elsif ( $type =~ m/^.s/o ) {
			$recipe{"conditions"} = [ "* ^Subject:.*$con" ];
		} elsif ( $type =~ m/^.l/o ) {
			$recipe{"conditions"} = [ "* ^TO_".$con."(\$|[>, ])" ];
		}

		my $recipe;
		if($recipe{"lock"}) {
			$recipe = ":0$recipe{'flags'}:$recipe{'lockfile'}\n";
		} else {
			$recipe = ":0$recipe{'flags'}\n";
		}
		$recipe .= join("\n", @{$recipe{"conditions"}});
		if($recipe{"action"} ne '') {
			$recipe .= "\n$recipe{'action'}";
		}

		$recipe{"all"} = $recipe;

	} else {

		my $recipe = $q->param('custom');
		$recipe{"all"} = $recipe;
		$recipe{"all"} =~ s/\r//og;

	}

	if ( $desc =~ m/^\s*$/o ) {
		my $act;
		my $dst;
		my $a    = $recipe{'a'};
		my $c    = $recipe{'c'};

		if    ( $type eq 'be' ) { $act = 'Block Email From ';     $dst = '';    $a = ''; }
		elsif ( $type eq 'bs' ) { $act = 'Block Subject ';        $dst = '';    $a = ''; }
		elsif ( $type eq 'fe' ) { $act = 'Filter Email From ';    $dst = ' To Folder '; }
		elsif ( $type eq 'fs' ) { $act = 'Filter Subject ';       $dst = ' To Folder '; }
		elsif ( $type eq 'fl' ) { $act = 'Filter List Address ';  $dst = ' To Folder '; }
		elsif ( $type eq 'we' ) { $act = 'Forward Email From ';   $dst = ' To '; }
		elsif ( $type eq 'ws' ) { $act = 'Forward Subject ';      $dst = ' To '; }
		elsif ( $type eq 'wl' ) { $act = 'Forward List Address '; $dst = ' To '; }
		else                    { $act = 'Custom Recipe';         $dst = '';    $a = ''; $c = ''; }

		$desc = "$act$c$dst$a";
	}
	$recipe{"desc"} = $desc;

	return 1;
} # end sub QueryToRecipe

#############################################################
## CGI Main                                                ##
#############################################################

umask(077);
$errstr = "";
$status = "";
my $md5err = <<EOF;
Error! Checksums do not match.
Your Procmail <acronym title="resource configuration">RC</acronym> file has been modified since this page was loaded.
Please try again.
If this error persists, please contact <a href="mailto:$email">$email</a>
EOF

# import query
my $q = new CGI;

if ( $q->param('level') ne '' ) {
	$level = $q->param('level');
}

# check the detail level [default: "Basic"]
if    ( $level eq undef   ) { $level = "Basic";        }
elsif ( $level =~ m/^a/oi ) { $level = "Advanced";     }
#	elsif ( $level =~ m/^i/oi ) { $level = "Intermediate"; }
else                        { $level = "Basic";        }

my $method = '';
my @submit = qw/remove create replace move saveall/;
foreach my $s (@submit) {
	if ( $q->param($s) ne undef ) {
		$method = $s;
	}
}

my @content      = ();

my $doctype      = qq|<!-- ?xml version="1.0"? -->\n<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"\n\t"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">|;
my $stylelink    = qq|<link href="$style" rel="styleSheet" type="text/css" title="procstyle" />|;

my $tmptitle     = $title;
   $tmptitle     =~ s/<[^>]+>//go;
push(@content, $doctype, '<html>', '<head>', "<title>$tmptitle</title>", "<base href=\"$base\" />", $stylelink, '</head>', '<body>');

$page       = $q->param('page');
my $datamd5 = $q->param('checksum');
my $currmd5 = OpenRc($rc_file);
CASE: {
	if      ( $method eq 'remove'  ) {
		my $num = $q->param('number');

		if ( $datamd5 ne $currmd5 ) {
			$errstr = $md5err;
		} else {
			RemoveRecipe($num);
			$page = 'view';
		}
		last CASE;

	} elsif ( $method eq 'create'  ) {
		%recipe = ();
		if ( QueryToRecipe($q, 'create') ) {
			if ( $q->param('confirm') ne undef && $q->param('confirm') ) {
				$page = 'confirm';
			} else {
				AddRecipe();
				$page = '';
			}
		} else {
			$page = '';
		}
		last CASE;

	} elsif ( $method eq 'replace' ) {
		my $num = $q->param('number');

		if ( $datamd5 ne $currmd5 ) {
			$errstr = $md5err;
		} else {
			%recipe = ();
			if ( QueryToRecipe($q, 'replace') ) {
				ReplaceRecipe($num);
				$page = 'view';
			} else {
				$page = '';
			}
		}
		last CASE;

	} elsif ( $method eq 'move' ) {
		if ( $datamd5 ne $currmd5 ) {
			$errstr = $md5err;
		} else {
			my $src = $q->param('src');
			my $dst = $q->param('dst');
			MoveRecipe($src, $dst);
			$page = 'order';
		}
		last CASE;

	} elsif ( $method eq 'saveall' ) {
		if ( $datamd5 ne $currmd5 ) {
			$errstr = $md5err;
		} else {
			my @arr = ();
			$arr[0] = $q->param('full');
			$arr[0] =~ s/\r//og;
			WriteFullRc(@arr);
			$page = '';
		}
		last CASE;

	}
} # end CASE
CloseRc();


if ( $q->param('single') ne undef && $page ne 'confirm' ) {
	push @content, StatusPage();
} else {
	OpenRc();
	if    ( $page eq undef     ) { push @content, MainPage();   }
	elsif ( $page eq 'view'    ) { push @content, RcPage();     }
	elsif ( $page eq 'add'     ) { push @content, CreatePage(); }
	elsif ( $page eq 'order'   ) { push @content, OrderPage();  }
	elsif ( $page eq 'manual'  ) { push @content, ManualPage(); }
	elsif ( $page eq 'confirm' ) { push @content, ConfirmPage($q->query_string()); }
	elsif ( $page eq 'log'     ) { push @content, LogPage($q->param('n')); }
	else                         { push @content, MainPage();   }
	CloseRc();
}

my $credits = <<EOF;
<hr />
<p class="credits"><a href="http://www.procmail.org/">Procmail</a> RC Webmin ($VERSION) by $author<br />
<!-- $rcsid -->
Written in <a href="http://www.perl.org/">Perl</a> ($]) using <a href="http://stein.cshl.org/WWW/software/CGI/">CGI.pm</a> ($CGI::VERSION)<br />
Markup is <a href="http://www.w3.org/TR/xhtml1">XHTML 1.0</a> and Style is <a href="http://www.w3.org/TR/REC-CSS2/">CSS2</a></p>
EOF

push (@content, $credits, '</body>', '</html>');

# print header, content;
print $q->header(), join "\n", @content;

__END__

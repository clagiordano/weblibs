<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<HTML>
<HEAD>
	<LINK rel="SHORTCUT ICON" href="./Img/shortcut2.png">
   	<META http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
   	<TITLE>Farabundo Index</TITLE>
   	<STYLE>
		BODY {
			font : bold 9pt Times;
		}
		INPUT {
			font : 8pt Verdana;
		}
		LAB {
			font : 8pt Verdana;
		}
		TH {
			font-family: Times;
			font-size: 18pt;
			color: red;
		}
		.sottotabelle TH {
			font-size: 12pt;
			font-family: Times;
			height: 30px;
			vertical-align: top;
		}
	</STYLE>
</HEAD> 
<BODY>
	<TABLE align="center" width="100%" border="0" cellpadding="0" cellspacing="0">
		<TH colspan="2">Frente Farabundo Mart&#236; Para la Liberaci&#242;n Nacional</TH>
		<TR><TD colspan="2">&nbsp;</TD></TR>
			<TR>
				<TD width="150" align="center" valign="top">
					<TABLE width="100%" height="100%" border="0" cellspacing="0">
						<TR><TD><A href="./Test"><IMG src="./Img/logo_mini.png" border="1"></A></TD></TR>
						<TR><TD>&nbsp;</TD></TR>
						
						<TR><TD><HR align="center" width="100%" size="2" color="Black"></TD></TR>
						<TR><TD align="center">Progetti:</TD></TR>
						<TR><TD><HR align="center" width="100%" size="2" color="Black"></TD></TR>
<!-- 						<TR><TD>&nbsp;</TD></TR> -->
						<TR><TD align="center"><A href="./Randibe/">RanDiBe</A></TD></TR>
						<TR><TD align="center"><A href="./Accomando/">Accomando</A></TD></TR>
						<TR><TD align="center"><A href="./StudioTusa/temp/">StudioTusa (temp)</A></TD></TR>
						<TR><TD align="center"><A href="./Riserve/">Riserve</A></TD></TR>
						<TR><TD align="center"><A href="./Ingegneri/">Ingegneri</A></TD></TR>
						<TR><TD align="center"><A href="./Peppe/">Peppe-Web</A></TD></TR>
						<TR><TD align="center"><A href="./Gestionale/">Gestionale</A></TD></TR>
						<TR><TD>&nbsp;</TD></TR>
						
						<TR><TD><HR align="center" width="100%" size="2" color="Black"></TD></TR>
						<TR><TD align="center">Amministrazione:</TD></TR>
						<TR><TD><HR align="center" width="100%" size="2" color="Black"></TD></TR>
						<TR>
							<TD align="center">
								<A href="./Amministrazione/phpmyadmin/">phpMyAdmin</A><BR>
								<A href="./info.php" target="_blank">phpinfo</A>
							</TD>
						</TR>
<!-- 						<TR><TD><HR align="center" width="100%" size="2" color="Black"></TD></TR> -->
						<TR><TD align="center"><A href="./post_install.php"><IMG border="0" src="./Img/synaptic.png" title="Software per la post installazione"><br>Software per la post installazione.</A></TD></TR>
						<TR><TD><HR align="center" width="100%" size="2" color="Black"></TD></TR>
					</TABLE>
				</TD>
				<TD align="right" rowspan="3"><IMG src="./Img/brigada.gif"></TD>
			</TR>
	</TABLE>
</BODY>
</HTML>

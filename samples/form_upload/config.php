<?php
$dir_upload=""; // DIRECTORY DOVE EFFETTUARE L'UPLOAD
$chmod_dir=0700; // PERMESSI DELLA DIRECTORY
$debug=0; // 1=STAMPA ALCUNE SEMPLICI INFORMAZIONI DI DEBUG, 0=NON STAMPARE
$sovrascrivi_file=0; // 1=SOVRASCRIVI I FILES SE ESISTENTI, 0=NON SOVRASCRIVERE I FILES
$dim_massima=500; // DIMENSIONE MASSIMA UPLOAD IN KB
$check_tipi=1; //1=CONTROLLA IL TIPO DI FILE, 0=NON CONTROLLARE
$tipi_permessi=array( //ARRAY PER ALCUNI TIPI DI FILE
	"text/plain",
	"text/html",
	"text/xml",
	"image/jpeg",
	"image/gif",
	"image/png",
	"video/mpeg",
	"audio/midi",
	"application/x-zip-compressed",
	"application/vnd.ms-excel",
	"application/x-msdos-program",
	"application/octet-stream"
);
?>

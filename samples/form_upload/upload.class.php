<?php
// file upload.class.php
//
// Marco Barcaro
// 27/12/2004
// Testato con Apache 1.3.29, PHP 4.3.10, su winXP
// Testato con Apache 1.3.26, PHP 4.1.2, su Debian

$dir_upload=(substr($dir_upload,-1) != "/" && $dir_upload != "") ? $dir_upload."/" : $dir_upload;
define("DIR_UPLOAD",$dir_upload);
define("CHMOD_DIR",$chmod_dir);
define("DEBUG",$debug);
define("SOVRASCRIVI_FILE",$sovrascrivi_file);
define("DIM_MASSIMA",$dim_massima*1024);
define("CHECK_TIPI",$check_tipi);
define("TIPI_PERMESSI",implode("|",$tipi_permessi));

class upload {
	function upload(){
	}

	function caricafile(){
		//CODICE PER L'UPLOAD CON VARI CONTROLLI
		if (count($_FILES) > 0){
			$numero_file= count($_FILES['file']['tmp_name']);
			for($i=0;$i<$numero_file;$i++){
				if($_FILES['file']['size'][$i] == 0){
					echo "L'UPLOAD DEL FILE <strong>{$_FILES['file']['name'][$i]}</strong> NON E' ANDATO A BUON FINE!<br />\n";
					unset( $_FILES['file']['name'][$i]);
					unset( $_FILES['file']['type'][$i]);
					unset( $_FILES['file']['size'][$i]);
					unset( $_FILES['file']['error'][$i]);
					unset( $_FILES['file']['tmp_name'][$i]);
				}
			}
			$numero_file=count( $_FILES['file']['tmp_name']);
			echo "Hai caricato $numero_file file(s)";
			echo "<br /><br />\n";
			foreach($_FILES['file']['name'] as $chiave=>$valore){
				if (DEBUG == 1){
					echo "Nome file: <strong>".$_FILES['file']['name'][$chiave]."</strong><br />\n";
					echo "Tipo file: <strong>".$_FILES['file']['type'][$chiave]."</strong><br />\n";
					echo "Dimensione: <strong>".$_FILES['file']['size'][$chiave]." byte</strong><br />\n";
					echo "Nome temporaneo: <strong>".$_FILES['file']['tmp_name'][$chiave]."</strong><br />\n";
				}
				if (is_uploaded_file( $_FILES['file']['tmp_name'][$chiave])){
					if ($_FILES['file']['size'][$chiave] <= DIM_MASSIMA){
						if(CHECK_TIPI == 0 || (CHECK_TIPI == 1 && in_array( $_FILES['file']['type'][$chiave], explode("|",TIPI_PERMESSI)))){
							if(!is_dir(DIR_UPLOAD) && DIR_UPLOAD != ""){
								if( !@mkdir(DIR_UPLOAD,CHMOD_DIR))
									die("ERRORE NELLA CREAZIONE DELLA DIRECTORY <strong>".DIR_UPLOAD."</strong>");
							}
							if(!file_exists(DIR_UPLOAD.$_FILES['file']['name'][$chiave]) || SOVRASCRIVI_FILE == 1){
								if (@move_uploaded_file( $_FILES['file']['tmp_name'][$chiave], DIR_UPLOAD.$_FILES['file']['name'][$chiave]))
									echo "FILE <strong>{$_FILES['file']['name'][$chiave]}</strong> TRASFERITO!";
								else
									die("ERRORE NEL TRASFERIMENTO DEL FILE <strong>".$_FILES['file']['name'][$chiave]."</strong>");
							} else
								echo ("IL FILE <strong>".$_FILES['file']['name'][$chiave]."</strong> E' ESISTENTE!");
						} else
							echo ("IL TIPO DI FILE <strong>".$_FILES['file']['type'][$chiave]."</strong> NON E' CONSENTITO!");
					} else
						echo ("LA DIMENSIONE DEL FILE <strong>".$_FILES['file']['type'][$chiave]."</strong> NON E' CONSENTITA!");
				} else
					die("ERRORE NEL TRASFERIMENTO DEL FILE <strong>".$_FILES['file']['name'][$chiave]."</strong>");
				echo "<hr />\n";
			}
		}
	}

	function mostraform(){
		//FORM PER EFFETTUARE L'UPLOAD
		echo "<html>
		<head>
		<script type=\"text/javascript\" src=\"esterno.js\"></script>
		</head>
		<body>
		<form action=\"{$_SERVER['PHP_SELF']}\" method=\"POST\" name=\"modulo\" enctype=\"multipart/form-data\">
		<div id=\"attachment\" style=\"display:none\">
			 <input id=\"file\" name=\"file\" type=\"file\" size=\"55\" />
			 <a href=\"#\" onclick=\"javascript:removeFile(this.parentNode.parentNode,this.parentNode);\"> Rimuovi</a>
		</div>
		<div id=\"attachments\">
			<br/><a id=\"addupload\" href=\"javascript:addUpload('file')\">Aggiungi file</a><br/><br/>
			<input name=\"file[]\" type=\"file\" size=\"55\" />
			<span id=\"attachmentmarker\"></span>
		</div>
		<input type=\"hidden\" name=\"MAX_FILE_SIZE\" value=\"".DIM_MASSIMA."\" />
		<input type=\"submit\" value=\"invia\" />
		</form>
		</body>
		</html>\n";
	}
}
?>

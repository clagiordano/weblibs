<?php
error_reporting(E_ALL);
require_once("config.php");
require_once("upload.class.php");

// ISTANZIA LA CLASSE
$upload=new upload();
// SE L'ARRAY $_FILES CONTIENE QUALCOSA ESEGUI IL CODICE PER CARICARE I FILE
if (count($_FILES) > 0)
	$upload->caricafile();
// ALTRIMENTI MOSTRA IL FORM PER L'UPLOAD
else
	$upload->mostraform();
?>

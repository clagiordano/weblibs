<?php
	$root = substr($_SERVER["DOCUMENT_ROOT"], 0, -1);
	require_once("$root/Class/Database.php");
	require_once("$root/Function/DataTime.php");
	require_once("$root/Class/psxlsgen.php");

    $d = new Database("localhost", "gestore", "polipo", "gestione");
	$r = $d->GetRows("*", "view_call", "", "", "data_att limit 50", 1);

	$myxls = new PhpSimpleXlsGen();
	$myxls->totalcol = 8;

	$myxls->InsertText("N.Call");
	$myxls->InsertText("Data Chisura");
	$myxls->InsertText("Cliente");
	$myxls->InsertText("CITY");
	$myxls->InsertText("Tecnico");
	$myxls->InsertText("Tipo Call");
	$myxls->InsertText("Note");
	$myxls->InsertText("Economico");

	$tot_economico = 0;
	foreach ($r as $key => $field)
	{
		if ($field['n_pdl'] > "1")
		{
			$economico = floatval($field['econ']) * floatval($field['n_pdl']);
			$compenso = floatval($field['prz']) * floatval($field['n_pdl']);
		}else{
			$economico = floatval($field['econ']);
			$compenso = floatval($field['prz']);
		}

		$myxls->InsertText($field['n_call']);
		$myxls->InsertText(Inverti_Data($field['data_att']));
		$myxls->InsertText($field['cliente']);
		$myxls->InsertText($field['Comune'] . " (" . $field['Targa'] . ")");
		$myxls->InsertText($field['tecnico']);
		$myxls->InsertText($field['tipo']);
		$myxls->InsertText($field['note_call']);
		$myxls->InsertNumber(sprintf("%.2f", $economico));

		$tot_economico += $economico;
	}


	$myxls->InsertText("Totale: ");
	$myxls->InsertText(str_replace(".", ",", sprintf("%.2f", $tot_economico+15.58)));

	$myxls->SendFile();



?>

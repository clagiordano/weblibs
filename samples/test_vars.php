<?php

echo "<pre>";

function fTestVars1($arg) 
{
    echo __METHOD__ . ": arg: $arg\n";
    
    fTestVars2($arg);
    
    echo __METHOD__ . ": arg: $arg\n";
}

function fTestVars2(&$arg) 
{
    echo __METHOD__ . ": arg: $arg\n";
    
    $arg = str_replace("test", "TEST", $arg);
    
    echo __METHOD__ . ": arg: $arg\n";
}

$TestVar = "test var for test_vars.php\n\n";
echo $TestVar;

fTestVars1($TestVar);


echo "</pre>";

<style>
    body {
        margin: 0px 0px 0px 0px;
        background-color : #E3E5CF;
        color: black;
        font-family: sans-serif;
        font-size : 9pt;
    }
    .err {
        font-size : 9pt;
        font-weight : bold;
        color: red;
    }

    .ok {
        font-size : 9pt;
        font-weight : bold;
        color: green;
    }
</style>
<?php
require_once("../php/Database_4.6.32.php");
require_once("../php/Report.php");

echo "Debug => Test Class Page <br />";

$d = new Database("localhost", "xxx", "xxx", "xxx");
$r = new Report();

$Campi     = $d->GetRows("u.user, t.tipo, s.stato, s.id_stato",
                         "tab_utenti u, tab_tipi t, tab_stato_utente s",
                         "u.tipo = t.id_tipo AND s.id_stato = u.id_stato", "",
                         "user", 0);
?>

<table style="width: 100%;" border="1">
    <?php
    // Header:
    $r->Fields = array("Utente:", "Tipo:", "Stato:|center");
    $r->RenderCell();

    // Results:
    foreach ($Campi as $key => $field) {
        if ($field['id_stato'] == "2") {
            $r->RowStyle = " class=\"err\"";
        } else {
            $r->RowStyle = " class=\"ok\"";
        }

        $r->Fields  = $field;
        $r->Exclude = array('pass' => '', 'id_user' => '', 'id_stato' => '');
        $r->Debug   = 0;
        $r->RenderCell();
    }
    ?>
</table>

<?php echo "Debug => fine pagina <br />"; ?>


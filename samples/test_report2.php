<?php

require "../php/Report.php";
require "../php/ReportNode.php";
require "../php/Database.php";
require "../php/Logger.php";
require "../php/User.php";
require "../php/BrowserInfo.php";

use weblibs\php\Report;
use weblibs\php\ReportNode;
use weblibs\php\Database;

$d          = new Database("localhost", "test", "sample", "db_gestione_gn");
$d->GetRows("*", "tab_comuni", "", "", "Prov, Comune");

$r = new Report();
$r->setReportId("comuni");

foreach ($d->Results as $result) {
    $n = new ReportNode([
        'NodeId'      => $vCat['id_cat'],
        'title'       => "<h2>{$vCat['cat']}</h2>",
        'description' => "<p>{$vCat['note']}</p>"
    ]);

    $r->addNode($n);
}

$r->show();

<html>
    <head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<title>Ajax Test</title>
		<script type="text/javascript" src="../Function/Ajax_Base.js"></script>

        <script type="text/javascript">
            function checkForm()
            {
                xmlhttp = CreateAjaxRequestObject()
                
                xmlhttp.onreadystatechange=function()
                {
                    if(xmlhttp.readyState==4)
                    {
                      document.myForm.time.value=xmlhttp.responseText;
                    }
                }

                xmlhttp.open("GET","time.php",true);
                xmlhttp.send(null);

            }
        </script>

    </head>
    
    <body>

    <form name="myForm">
        Name: <input type="text" name="username" onkeyup="checkForm();" />
        Time: <input type="text" name="time" />
    </form>


</body>
</html>
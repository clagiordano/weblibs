<?php
	$root = substr($_SERVER["DOCUMENT_ROOT"], 0, -1);
	require_once("$root/Class/Database.php");
	require_once("$root/Function/DataTime.php");
	require_once("$root/Class/XlsExport.php");

    $d = new Database("localhost", "gestore", "polipo", "gestione");
	$r = $d->GetRows("*", "view_call", "", "", "data_att limit 50", 1);

    // Send Header
    header("Pragma: public");
    header("Expires: 0");
    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
    header("Content-Type: application/force-download");
    header("Content-Type: application/octet-stream");
    header("Content-Type: application/download");
    header("Content-Disposition: attachment;filename=file.xls ");
    header("Content-Transfer-Encoding: binary ");

    // XLS Data Cell
    xlsBOF();

	xlsWriteLabel(0, 0, "N.Call");
	xlsWriteLabel(0, 1, "Data Chisura");
	xlsWriteLabel(0, 2, "Cliente");
	xlsWriteLabel(0, 3, "CITY");
	xlsWriteLabel(0, 4, "Tecnico");
	xlsWriteLabel(0, 5, "Tipo Call");
	xlsWriteLabel(0, 6, "Note");
	xlsWriteLabel(0, 7, "Economico");

	$xlsRow = 1;
	$tot_economico = 0;
	foreach ($r as $key => $field)
	{
		if ($field['n_pdl'] > "1")
		{
			$economico = floatval($field['econ']) * floatval($field['n_pdl']);
			$compenso = floatval($field['prz']) * floatval($field['n_pdl']);
		}else{
			$economico = floatval($field['econ']);
			$compenso = floatval($field['prz']);
		}

		xlsWriteLabel ($xlsRow, 0, $field['n_call']);
		xlsWriteLabel ($xlsRow, 1, Inverti_Data($field['data_att']));
		xlsWriteLabel ($xlsRow, 2, $field['cliente']);
		xlsWriteLabel ($xlsRow, 3, $field['Comune'] . " (" . $field['Targa'] . ")");
		xlsWriteLabel ($xlsRow, 4, $field['tecnico']);
		xlsWriteLabel ($xlsRow, 5, $field['tipo']);
		xlsWriteLabel ($xlsRow, 6,  $field['note_call']);
		xlsWriteNumber ($xlsRow, 7, sprintf("%.2f", $economico));

		$tot_economico += $economico;
		$xlsRow++;
    }

	xlsWriteLabel ($xlsRow, 0, "Totale: ");
	xlsWriteNumber ($xlsRow, 1, str_replace(".", ",", sprintf("%.2f", $tot_economico)));

	xlsWriteLabel ($xlsRow+1, 0, "Aaaaaa: ");
	xlsWriteNumber ($xlsRow+1, 1, "579,33");

	xlsWriteLabel ($xlsRow+2, 0, "float con virgola: ");
	xlsWriteNumber ($xlsRow+2, 1, sprintf("%,2f", $tot_economico));

    xlsEOF();
    exit();
?>

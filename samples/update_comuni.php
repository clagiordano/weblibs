<?php session_start(); ?>
<!doctype html>
<html lang="it">
<!--
/*
 *      Gestione
 *
 *      Copyright 2012 Claudio Giordano <claudio.giordano@autistici.org>
 *
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 3 of the License, or
 *      (at your option) any later version.
 *
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 *      MA 02110-1301, USA.
 */
!-->
	<head>
		<meta charset="utf-8">
	</head>

	<body>

		<?php
			$root = substr($_SERVER['DOCUMENT_ROOT'], 0, -1);
			require_once("$root/Class/Database.php");

			$d = new Database("localhost", "gestore", "polipo", "gestione");
			$Results = $d->GetRows("*", "tab_comuni", "", "", "", 1);

			//~ echo "<pre> Results";
				//~ print_r($Results);
			//~ echo "</pre>";

			foreach ($Results as $key => $field)
			{
				//~ echo "[Debug]: KEY: $key <br />";
				$Valori = $field;
				$Valori['Comune'] = ucfirst(strtolower($field['Comune']));
				$Valori['Prov'] = ucfirst(strtolower($field['Prov']));

				//~ echo "<pre>";
					//~ print_r($Valori);
				//~ echo "</pre>";
				$Status = $d->UpdateRow ($Valori, "", "tab_comuni", "ID = '" . $Valori['ID'] . "'");
				echo "[Debug][" . $Valori['ID'] . "]: $Status <br />";
			}

		?>

	</body>
</html>

<?php
/*
 *      test.php
 *
 *      Copyright 2008 Claudio Giordano <claudio.giordano@autistici.org>
 */
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

    <head>
        <title>senza nome</title>
        <meta http-equiv="content-type" content="text/html;charset=utf-8" />
        <meta name="generator" content="Geany 0.15" />
    </head>

    <body>

        <?php
        echo "Valore sessione attuale: " . session_cache_expire() . " minuti.<br />";
        // Setto la durata della sessione a 15 minuti, da richiamare prima di session_start():
        session_cache_expire(15);
        echo "Valore sessione attuale: " . session_cache_expire() . " minuti.<br />";
        ?>

    </body>
</html>

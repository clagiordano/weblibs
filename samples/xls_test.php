<?php

function exportFile() {
		$sql = $this->selectPart . " " . $this->fromPart;
		$sql .= " where a.EmployeeId = cv.EmployeeId" .
				" and cv.CertificateId = c.CodeId and c.TypeId = 1" .
				" and a.NationalId = d.CodeId and d.TypeId = 7" .
				" and a.CountryId = e.CodeId and e.TypeId = 6" .
				" and cv.DesireLocationId = f.CodeId and f.TypeId = 5" .
				" and ";
		$sql .= $this->wherePart;
		$clsDbConnect = new clsDbConnect();
		$result = $clsDbConnect->exeQuery($sql);
		$file_type = "vnd.ms-excel; charset=utf-8";
		$file_ending = "xls";
//		header("Content-Type: text/html; charset=utf-8");
		header("Content-Type: application/vnd.ms-xls; charset=utf-8");
		header("Content-Disposition: inline; filename=employeeList.$file_ending");
		header("Pragma: no-cache");
		header("Expires: 0");
		$sep = "\t";

		$clsCV = new clsCV($this->Language);
		$clsEmployee = new clsEmployee($this->Language);
		print utf8_decode($clsEmployee->getEmailText())."\t";
		print utf8_decode($clsEmployee->getSexText())."\t";
		print utf8_decode($clsEmployee->getNameText())."\t";
		print utf8_decode($clsEmployee->getTelephoneText())."\t";
		print utf8_decode($clsCV->getCertificateIdText())."\t";
		print utf8_decode($clsEmployee->getNationalIdText())."\t";
		print utf8_decode($clsEmployee->getCountryIdText())."\t";
		print utf8_decode($clsCV->getYearsOfExperienceText())."\t";
		print utf8_decode($clsCV->getEducationText())."\t";
		print utf8_decode($clsCV->getExperienceText())."\t";
		print utf8_decode($clsCV->getDesireJobText())."\t";
		print utf8_decode($clsCV->getDesireLocationIdText())."\t";
		print utf8_decode($clsCV->getSalaryText())."\t";;

		print ("\n");
		while ($row = mysql_fetch_row($result)) {
			$schema_insert = "";
			for ($j = 0; $j < mysql_num_fields($result); $j++) {
				if (!isset ($row[$j])) {
					$schema_insert .= "NULL" . $sep;
				}
				elseif ($row[$j] != "") {
					$schema_insert .= utf8_decode($row[$j]) . $sep;
				} else {
					$schema_insert .= "" . $sep;
				}
			}
			$schema_insert = str_replace($sep . "$", "", $schema_insert);
			$schema_insert = preg_replace("/\r\n|\n\r|\n|\r/", " ", $schema_insert);
			$schema_insert .= "\t";
			print (trim($schema_insert));
			print "\n";
		}
	}
?>

<?php
	$root = substr($_SERVER["DOCUMENT_ROOT"], 0, -1);
	require_once("$root/Class/Database.php");
	require_once("$root/Function/DataTime.php");
	require_once("$root/Class/XlsExport.php");

    $d = new Database("localhost", "gestore", "polipo", "gestione");
	$r = $d->GetRows("*", "view_call", "", "", "data_att limit 50", 1);

    // Send Header
    header('Content-type: application/vnd.ms-excel');
    header("Content-Type: application/force-download");
	header("Content-Type: application/octet-stream");
	header("Content-Type: application/download");
    header("Content-Disposition: attachment;filename=PROVA.xls");

	echo "<table style=\"width: 100%;\">";
	echo "<tr>
		<td>N.Call</td>
		<td>Data Chisura</td>
		<td>Cliente</td>
		<td>CITY</td>
		<td>Tecnico</td>
		<td>Tipo Call</td>
		<td>Note</td>
		<td>Economico</td>
	</tr>";

	foreach ($r as $key => $field)
	{
		if ($field['n_pdl'] > "1")
		{
			$economico = floatval($field['econ']) * floatval($field['n_pdl']);
			$compenso = floatval($field['prz']) * floatval($field['n_pdl']);
		}else{
			$economico = floatval($field['econ']);
			$compenso = floatval($field['prz']);
		}

		echo "<tr>";
			echo "<td>" . $field['n_call'] . "</td>";
			echo "<td>" . Inverti_Data($field['data_att']) . "</td>";
			echo "<td>" . $field['cliente'] . "</td>";
			echo "<td>" . $field['Comune'] . " (" . $field['Targa'] . ")" . "</td>";
			echo "<td>" . $field['tecnico'] . "</td>";
			echo "<td>" . $field['tipo'] . "</td>";
			echo "<td>" . $field['note_call'] . "</td>";
			echo "<td>" . str_replace(".", ",", sprintf("%.2f", $economico)) . "</td>";
		echo "<tr>";

		//$tot_economico += $economico;
	}

	echo "</table>";

?>

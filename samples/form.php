<?php

error_reporting(E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);

require "../php/Form.php";
require "../php/FormControl.php";

use weblibs\php\Form;

echo "<pre> POST: ";
print_r($_POST);
echo "</pre>";

$f = new Form();
$f->setName("sampleForm");
//$d->DrawControl("text", "ditta_rag_soc", "Ragione Sociale", 
//					"\w+", $_POST, null, "38.5em", "200"); 

/**
 * @param string $Type
 * @param string $Name
 * @param string $Label
 * @param string $Pattern
 * @param string $Value
 * @param string $CheckValue
 * @param string $Size
 * @param string $MaxLenght
 * @param string $JavaScript
 * @param array $DataArray
 * @param string $ID
 * @param string $Field
 * @param string $Style
 * @param string $RowHeight
 */
$f->addText([
    'Name'       => "sampleControl",
    'Label'      => 'Sample Control',
    'Pattern'    => '\w+',
    'Value'      => filter_input(INPUT_POST, 'sampleControl'),
    'CheckValue' => null,
    'MaxLenght'  => 200,
    'Size'       => "38.5em",
]);
$f->show();

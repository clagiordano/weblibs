<?php

error_reporting(E_ALL);
ini_set("display_errors", 1);
ini_set("display_startup_errors", 1);

echo "APC Info";

echo "<pre>";
//var_dump(apc_add("test", 1234) );

//apc_exists("test");

if (is_callable("apc_cache_info")) {
    apc_cache_info();
} else {
    throw new \Exception("Function apc_cache_info is not callable.");
}


echo "</pre>";

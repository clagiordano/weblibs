<?php

require "../php/Report.php";
require "../php/ReportNode.php";
require "../php/Database.php";
require "../php/Logger.php";
require "../php/User.php";
require "../php/BrowserInfo.php";

use weblibs\php\Report;
use weblibs\php\ReportNode;
use weblibs\php\Database;

$d          = new Database("localhost", "test", "sample", "db_gestione_gn");
$d->GetRows("*", "tab_cat_merce", "", "", "cat");
$categories = $d->Results;

$r = new Report();
$r->setReportId("TestReport");

//$n = new ReportNode([
//    'Id'          => $i,
//    'title'       => 'Titolo ' . $i,
//    'description' => 'Descrizione di prova' . $i
//        ]);
//
//$r->addNode($n);

foreach ($categories as $kCat => $vCat) {
    $d->getRows("count(id_prodotto) as tot", "tab_prodotti",
                "id_cat = '" . $vCat['id_cat'] . "'");

    $n = new ReportNode([
        'NodeId'      => $vCat['id_cat'],
        'title'       => "<h2>{$vCat['cat']}</h2>",
        'description' => "<p>{$vCat['note']}</p>"
    ]);

    $r->addNode($n);
}

$r->show();

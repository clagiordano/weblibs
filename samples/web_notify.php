<?php session_start(); error_reporting(E_ALL); ?>
<!doctype html>
<html lang="it">
<!--
/*
 *      TEST WEB NOTIFY
 *
 *      Copyright 2012 Claudio Giordano <claudio.giordano@autistici.org>
 *
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 3 of the License, or
 *      (at your option) any later version.
 *
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 *      MA 02110-1301, USA.
 */
!-->
	<head>
		<meta charset="utf-8">
		<title>TEST WEB NOTIFY</title>
	</head>

	<body>
		<script type="text/javascript">
			// verifica supporto
			if (window.webkitNotifications)
			{
				console.log("Notifications are supported!");
			}
			else {
				console.log("Notifications are not supported for this Browser/OS version yet.");
			}

			// creazione oggetto
			function createNotificationInstance(options)
			{
				if (options.notificationType == 'simple')
				{
					console.log("simple notification");
					return window.webkitNotifications.createNotification(
							'mail.png', 'Notification Title', 'Notification content...');
				} else if (options.notificationType == 'html') {
					console.log("html notification");
					return window.webkitNotifications.createHTMLNotification('http://someurl.com');
				}
			}

			document.querySelector('#show_button').addEventListener('click', function() {
			if (window.webkitNotifications.checkPermission() == 0) { // 0 is PERMISSION_ALLOWED
				// function defined in step 2
				createNotificationInstance({ notificationType: 'html' });
			} else {
				window.webkitNotifications.requestPermission();
			}
		}, false);

			//var notification = new Notification("mail.png", "New Email Received");
			//notification.onshow = function() { setTimeout(notification.cancel(), 15000); }
			//notification.show();
		</script>
	</body>
</html>

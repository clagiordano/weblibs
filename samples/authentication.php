<?php
session_start();

error_reporting(E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);

echo "<pre> PRE SESSION: ";
print_r($_SESSION);
echo "</pre>";

function __autoload($className)
{
    $className = ltrim($className, '\\');
    $fileName  = '';
    $namespace = '';
    if ($lastNsPos = strrpos($className, '\\')) {
        $namespace = substr($className, 0, $lastNsPos);
        $className = substr($className, $lastNsPos + 1);
        $fileName  = str_replace('\\', DIRECTORY_SEPARATOR, $namespace) . DIRECTORY_SEPARATOR;
    }
    $fileName .= str_replace('_', DIRECTORY_SEPARATOR, $className) . '.php';

    require "../../$fileName";
}

use weblibs\php\DateTime;
use weblibs\php\Database;
use weblibs\php\Authentication;
use weblibs\php\Form;

echo "<pre>";
echo "Verifica meccanismo di autenticazione:\n\n";

$date = new DateTime(time());
$d    = new Database("localhost", "test", "sample", "db_anathema");
$a    = new Authentication($d, "testAuth");


$d->getRows("*", "tab_config", "opt = 'enabled'", true);
$StatoDB = $d->Results[0]['val'];
print_r($d->Results);
// Pulisco il campo user:
if (filter_input(INPUT_POST, 'username') != "") {
    //echo "effettuo il login\n\n";
    $a->logIn(filter_input(INPUT_POST, 'username'),
                           filter_input(INPUT_POST, 'password'), "testAuth", "");
}

//echo "DB STATUS: $StatoDB\n\n";
//    echo "POST\n";
//    print_r($_POST);
//    
//    echo "\nGET\n";
//    print_r($_GET);
//    
//    echo "\nSESSION\n";
//    print_r($_SESSION);
//$Results = $d->GetRows( "*", "tab_utenti", "user LIKE 'eteral'", "", "", 0 );
//    echo "test User object\n";
//    $U = new User( "username", "password", 1, 3, "description", 0, 2 );
//    echo "TEST SALVATAGGIO\n";
//    $d->saveRow($_POST, array('salva' => ''), "tab_utenti", "user", true);
//echo "</pre>";
//$U->printR();
?>

<form method="post" action="<?php echo filter_input(INPUT_SERVER, 'REQUEST_URI'); ?>">
    <div style="width: 100%;">

        <?php if ($StatoDB == 0) { ?>
            <div style="text-align: center;">
                <input name="username" type="text" tabindex="1" required
                       placeholder="Username" style="width: 20em;"/>
            </div>

            <div style="text-align: center;">
                <input name="password" type="password" tabindex="2" required
                       placeholder="Password" style="width: 20em;" />
            </div>

            <div style="text-align: center;">
                <input name="salva" type="submit" value="Accedi" tabindex="3"
                       style="width: 20em;" />
            </div>
        <?php } else { ?>
            <div>
                <label class="err">
                    Accesso temporaneamente disabilitato per manutenzione.
                </label>
            </div>
        <?php } ?>

        <div <?php
        if ($a->ErrorsOccurred === true) {
            echo "class=\"err\"";
        } else {
            echo "class=\"ok\"";
        }
        ?> >
                <?php
                if (isset($a->ErrorsOccurred)) {
                    echo $a->ResultStatus;
                }
                ?>
        </div>

    </div>
</form>

<?php
$date->timerStop("End");
echo "<pre> POST SESSION: ";
print_r($_SESSION);
echo "</pre>";

drop view if exists view_anagrafica;
create view view_anagrafica as (select
	ta.tipo,
	a.*,
	p.pagamento,
	p.costo,
	c.Comune,
	pro.Targa

from
	tab_anagrafica a, 
	tab_tipo_anag ta,
	tab_mod_pagamento p,
	tab_comuni c,
	tab_province pro

where
	a.id_tipo_anagrafica = ta.id_anag
and
	a.id_pagamento = p.id_pagamento
and
	a.id_citta = c.ID
and
	a.id_prov = pro.ID);

drop view if exists view_prodotti;
create view view_prodotti as (
select
	p.id_prodotto,
	p.cod_forn,
	p.cod_int,
	p.marca,
	p.modello,
	p.prz_acq,
	p.prz_ve,
	p.riordino,
	p.ordinati,
	p.note,
	p.id_cat,
	p.id_tipo,
	p.ck_com,
	p.barc_forn,

	c.cat,
	u.um,
	d.dep,
	t.desc_art,
	a.ragione_sociale,
	i.*
from
	tab_prodotti p,
	tab_cat_merce c,
	tab_um u,
	tab_depositi d,
	tab_tipo_art t,
	tab_anagrafica a,
	tab_iva i
where
	p.id_cat = c.id_cat
and
	p.id_um = u.id_um
and
	p.id_dep = d.id_dep
and
	p.id_tipo = t.id_art
and
	p.id_forn = a.id_anag
and
	p.id_iva = i.id_iva);

drop view if exists view_macchine_assegnate;
create view view_macchine_assegnate as (select 
	m.*,
	d.id_request,
	r.request,
	r.data_pianif,
	r.cod_fil,
	r.den
from
	view_macchine m,
	tab_dett_requests d,
	view_requests r

where 
	m.id_macchina = d.id_macchina
and
	d.id_request = r.id_request);
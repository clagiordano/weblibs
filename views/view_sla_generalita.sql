CREATE OR REPLACE VIEW view_generalita as (select
	gen.*,
	prov.Targa,
	comu.Comune,
	stato.stato_civile,
	istr.istruzione,
	istr.valore,
	istr.check_frsbe,
	forma.forma_clinica
from
	tab_generalita gen,
	tab_province prov,
	tab_comuni comu,
	tab_stato_civile stato,
	tab_istruzione istr,
	tab_forma_clinica forma
where
	gen.id_prov = prov.ID
and
	gen.id_citta = comu.ID
and
	gen.id_istruzione = istr.id_istruzione
and
	gen.id_forma_clinica = forma.id_forma_clinica
and
	gen.id_stato_civile = stato.id_stato_civile);

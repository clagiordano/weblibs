drop view if exists view_dvk;
create view view_dvk as (select
	r.id_request,
	r.request,
	r.data_pianif,
	r.den as filiale,
	r.nome_srv,

	m.id_macchina,
	m.data_in,
	m.provenienza,
	m.marca,
	m.modello,
	m.serial,
	m.asset,
	m.mac,
	m.tipo_mch,
	m.id_stato_mch,
	m.stato_mch,
	m.color

from
	view_requests r,
	tab_dett_requests dr,
	view_macchine m

where
	r.id_request = dr.id_request
and
	dr.id_macchina = m.id_macchina
and
	m.tipo_mch = 'Server'
and
	m.data_in != r.data_pianif);

--group by request

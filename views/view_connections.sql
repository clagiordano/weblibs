CREATE OR REPLACE VIEW view_connections as (
	select
		s.action,
		s.data,
		s.ip,
		s.platform,
		s.browser,
		s.version,
		s.arch,
		u.user,
		t.tipo

	from
		tab_utenti u,
		tab_tipi t,
		tab_session s

	where
		u.tipo = t.id_tipo
	and
		u.id_user = s.id_user);

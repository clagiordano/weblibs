use db_rollout_2010;

drop view if exists view_connections;
drop view if exists view_dvk;
drop view if exists view_macchine;
drop view if exists view_macchine_assegnate;
drop view if exists view_pianificazione;
drop view if exists view_requests;
drop view if exists view_tot_ritiri;
drop view if exists view_tot_ritirato;

drop table if exists view_connections;
drop table if exists view_dvk;
drop table if exists view_macchine;
drop table if exists view_macchine_assegnate;
drop table if exists view_pianificazione;
drop table if exists view_requests;
drop table if exists view_tot_ritiri;
drop table if exists view_tot_ritirato;

create view view_connections as (select
		s.action,
		s.data,
		s.ip,
		s.platform,
		s.browser,
		s.version,
		s.arch,
		u.user,
		t.tipo

	from
		tab_utenti u,
		tab_tipi t,
		tab_session s

	where
		u.tipo = t.id_tipo
	and
		u.id_user = s.id_user);

create view view_pianificazione as (select
	f.id_filiale as id_filiale_f,
	f.cod_fil,
	f.den,
	f.ind,
	f.cap,
	f.citta,
	f.prov,
	f.rol,
	f.tel,
	f.cell,
	f.mail,
	f.utenti,
	f.forn,
	f.variaz,
	f.dispo,
	f.data_pianif,
	f.note_fil,
	f.id_stato,
	f.id_tecnico,
	f.nome_srv,
	s.stato_call,
	s.color,
	concat(t.nome, " ", t.cognome) as tecnico,
	r.*,
	d.*

from
	tab_filiali f,
	tab_stato_call s,
	tab_tecnici t,
	tab_request r,
	tab_ditte d

where
	f.id_stato = s.id_stato_call
and
	f.id_tecnico = t.id_tecnico
and
	(f.id_filiale = r.id_filiale)
and
	f.id_competenza = d.id_ditta);

create view view_requests as (select
	r.*,
	f.cod_fil,
	f.den,
	f.ind,
	concat(f.citta, " (", f.prov, ")") as citta,
	f.utenti,
	f.id_stato,
	f.data_pianif,
	f.rol,
	f.mail,
	f.tel,
	f.cell,
	f.nome_srv,

	s.stato_call,
	s.color

from
	tab_request r,
	tab_filiali f,
	tab_stato_call s
where
	(r.id_filiale = f.id_filiale)
and
	f.id_stato = s.id_stato_call);

create view view_macchine as (select
	m.*,
	t.tipo_mch,
	s.stato_mch,
	s.color,
	mag.descr as magazzino

from
	tab_macchine m,
	tab_tipi_mch t,
	tab_stato_mch s,
	tab_magazzino mag

where
	m.id_tipo_mch = t.id_tipo_mch
and
	m.id_stato_mch = s.id_stato_mch
and
	mag.id_magazzino = m.id_magazzino);

create view view_dvk as (select
	r.id_request,
	r.request,
	r.data_pianif,
	r.den as filiale,
	r.nome_srv,

	m.id_macchina,
	m.data_in,
	m.provenienza,
	m.marca,
	m.modello,
	m.serial,
	m.asset,
	m.mac,
	m.tipo_mch,
	m.id_stato_mch,
	m.stato_mch,
	m.color

from
	view_requests r,
	tab_dett_requests dr,
	view_macchine m

where
	r.id_request = dr.id_request
and
	dr.id_macchina = m.id_macchina
and
	m.tipo_mch = 'Server'
and
	m.data_in != r.data_pianif);

create view view_tot_ritiri as (select
	sum(r.rit_srv) as rit_srv,
	sum(r.rit_pdl) as rit_pdl,
	sum(r.rit_mon) as rit_mon,
	sum(r.rit_let) as rit_let,
	sum(r.rit_cas) as rit_cas,
	sum(r.rit_las) as rit_las,
	sum(r.rit_prt_lan) as rit_prt_lan,
	sum(r.rit_bar) as rit_bar,
	sum(r.rit_fax) as rit_fax,
	sum(r.rit_altro_hw) as rit_altro_hw
from
	view_requests r
where
	data_pianif <= now());

create view view_macchine_assegnate as (select
	m.*,
	d.id_request,
	r.request,
	r.data_pianif,
	r.cod_fil,
	r.den
from
	view_macchine m,
	tab_dett_requests d,
	view_requests r

where
	m.id_macchina = d.id_macchina
and
	d.id_request = r.id_request);

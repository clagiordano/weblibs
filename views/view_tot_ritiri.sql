drop view if exists view_tot_ritiri;
create view view_tot_ritiri as (select
	sum(r.rit_srv) as rit_srv,
	sum(r.rit_pdl) as rit_pdl,
	sum(r.rit_mon) as rit_mon,
	sum(r.rit_let) as rit_let,
	sum(r.rit_cas) as rit_cas,
	sum(r.rit_las) as rit_las,
	sum(r.rit_prt_lan) as rit_prt_lan,
	sum(r.rit_bar) as rit_bar,
	sum(r.rit_fax) as rit_fax,
	sum(r.rit_altro_hw) as rit_altro_hw
from
	view_requests r
where
	data_pianif <= now());

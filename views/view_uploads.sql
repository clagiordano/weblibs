
create or replace view view_uploads as (select
	concat(g.cognome, ' ', g.nome) as generalita,
	m.titolo_pag,
	u.*
from
	tab_generalita g,
	tab_menu m,
	tab_uploads u
where
	g.id_generalita = u.id_generalita
and
	m.id_menu = u.id_menu);

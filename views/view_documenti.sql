CREATE OR REPLACE VIEW view_documenti as
(
	select
		d.id_documento,
		d.cod_documento,
		d.data_doc,
		d.imponibile,
		d.iva,
		d.totale,
		d.spese,
		d.data_pag,
		d.note,
		d.id_tipo,
		d.note as note_doc,
		d.ck_pag,
		d.data_scad,

		a.ragione_sociale,
		a.indirizzo,
		a.civ,
		a.cap,
		a.stato,
		a.id_citta,
		a.id_prov,
		a.piva,
		a.telefono1,
        a.telefono2,
        a.telefono3,
        a.fax1,
        a.fax2,
        a.fax3,
		m.pagamento,

		t.tipo_doc,

		c.Comune,
		p.Targa

	from
		tab_documenti d,
		tab_anagrafica a,
		tab_tipi_doc t,
		tab_mod_pagamento m,
		tab_comuni c,
		tab_province p

	where
		d.id_anagrafica = a.id_anag
	and
		d.id_tipo = t.id_tipo_doc
	and
		a.id_pagamento = m.id_pagamento
	and
		a.id_citta = c.ID
	and
		a.id_prov = p.ID

	order by d.data_doc
)

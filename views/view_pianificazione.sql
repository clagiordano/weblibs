drop view if exists view_pianificazione;
create view view_pianificazione as (select
	f.id_filiale as id_filiale_f,
	f.cod_fil,
	f.den,
	f.ind,
	f.cap,
	f.citta,
	f.prov,
	f.rol,
	f.tel,
	f.cell,
	f.mail,
	f.utenti,
	f.forn,
	f.variaz,
	f.dispo,
	f.data_pianif,
	f.note_fil,
	f.id_stato,
	f.id_tecnico,
	f.nome_srv,
	s.stato_call,
	s.color,
	concat(t.nome, " ", t.cognome) as tecnico,
	r.*,
	d.*

from
	tab_filiali f,
	tab_stato_call s,
	tab_tecnici t,
	tab_request r,
	tab_ditte d

where
	f.id_stato = s.id_stato_call
and
	f.id_tecnico = t.id_tecnico
and
	(f.id_filiale = r.id_filiale)
and
	f.id_competenza = d.id_ditta);

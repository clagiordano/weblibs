drop view if exists view_tot_ritirato;
create view view_tot_ritirato as (select
	tipo_mch, 
	count(tipo_mch) as tot
from 
	tab_dett_requests d,
	view_macchine m
where
	d.id_macchina = m.id_macchina
and
	provenienza = 'DA FILIALE'

group by 
	tipo_mch)
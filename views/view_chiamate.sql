drop view view_call;

create view view_call as (select
	tab_chiamate.id_chiamata,
	tab_chiamate.n_call,
	tab_chiamate.n_pdl,
	tab_chiamate.ref,
	tab_chiamate.tel,
	tab_chiamate.den,
	tab_chiamate.ind,
	tab_chiamate.data_att,
	tab_chiamate.citta,
	tab_chiamate.note as note_call,
	tab_chiamate.id_tecnico,
	tab_chiamate.id_stato_call,
	tab_chiamate.econ,
	tab_chiamate.id_citta,

	tab_attivita.*,

	tab_stato_call.stato_call,
	concat(tab_tecnici.nome, " ", tab_tecnici.cognome) as tecnico,
	tab_cat_att.*,

	tab_province.Targa,
	tab_province.ID as id_prov,

	tab_comuni.Comune,
	tab_comuni.Cap,
	tab_comuni.ID as id_com

from
	tab_chiamate,
	tab_attivita,
	tab_stato_call,
	tab_tecnici,
	tab_cat_att,
	tab_province,
	tab_comuni

where
	tab_chiamate.id_tipo_call = tab_attivita.id_attivita
and
	tab_chiamate.id_tecnico = tab_tecnici.id_tecnico
and
	tab_chiamate.id_stato_call = tab_stato_call.id_stato_call
and
	tab_attivita.id_cat = tab_cat_att.id_cat_att
and
	tab_province.ID = tab_comuni.ID_Prov
and
	tab_chiamate.id_citta = tab_comuni.ID);

drop view if exists view_requests;
create view view_requests as (select
	r.*,
	f.cod_fil,
	f.den,
	f.ind,
	concat(f.citta, " (", f.prov, ")") as citta,
	f.utenti,
	f.id_stato,
	f.data_pianif,
	f.rol,
	f.mail,
	f.tel,
	f.cell,
	f.nome_srv,

	s.stato_call,
	s.color

from
	tab_request r,
	tab_filiali f,
	tab_stato_call s
where
	(r.id_filiale = f.id_filiale)
and
	f.id_stato = s.id_stato_call);

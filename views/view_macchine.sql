drop view if exists view_macchine;
create view view_macchine as (select
	m.*,
	t.tipo_mch,
	s.stato_mch,
	s.color,
	mag.descr as magazzino

from
	tab_macchine m,
	tab_tipi_mch t,
	tab_stato_mch s,
	tab_magazzino mag

where
	m.id_tipo_mch = t.id_tipo_mch
and
	m.id_stato_mch = s.id_stato_mch
and
	mag.id_magazzino = m.id_magazzino);

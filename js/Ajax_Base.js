/*
 *      Javascript.js
 *
 *      Copyright 2010 Claudio Giordano <claudio.giordano@autistici.org>
 *
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 3 of the License, or
 *      (at your option) any later version.
 *
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 *      MA 02110-1301, USA.
 */


function CreateAjaxRequestObject()
{
	var xmlhttp;

	if (window.XMLHttpRequest) {
		// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp = new XMLHttpRequest();
	} else if (window.ActiveXObject) {
		// code for IE6, IE5
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	} else {
		alert("Your browser does not support XMLHTTP!");
	}

	return xmlhttp;
}

function ShowObject(object, target)
{
	var coord = GetObjCoord(object);
	var x = coord['x']-1;
	var y = coord['y']-3;
	MoveObj(target, x, y, 'block');
}

function ShowPopupDiv(URLStr, left, top, width, height, target, args)
{

	xmlhttp = CreateAjaxRequestObject();
	t = document.getElementById(target);
	//~ c = document.getElementById('popup_frame');

	t.style.left = left;
	t.style.top = top;
	t.style.width = width;
	t.style.height = height;

	//~ t.style.display = 'block';
	//~ alert(URLStr);
	//~ c.src = URLStr;

	xmlhttp.onreadystatechange = function()
	{
		if (xmlhttp.readyState === 4) {
			t.innerHTML = xmlhttp.responseText;
			t.style.display = 'block';
		}
	};

	if (args !== "") {
		xmlhttp.open('POST', URLStr, true);
		//Send the proper header information along with the request
		xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		xmlhttp.setRequestHeader("Content-length", args.length);
		xmlhttp.setRequestHeader("Connection", "close");

		xmlhttp.send(args);
	} else {
		xmlhttp.open('GET', URLStr, true);
		xmlhttp.send(null);
	}
}

function DragObject(target)
{
    ;
}

function ShowCoord(event, target)
{
	t = document.getElementById(target);
	t.innerHTML = "X: " + event.screenX + "<br />Y: " + event.screenY;

}

function polipo(src, target)
{
	var val = document.getElementsByName(src)[0].value;
	var t = document.getElementsByName(target);

	switch (target) {
		case "cat_merce":
			break;
	}
}

function LiveSearch (object, target, url)
{
	//~ alert("object: " + object + "\n target: " + target + "\n url: " + url);
	xmlhttp = CreateAjaxRequestObject();
	IdTarget = document.getElementById(target);
	document.getElementById(target).style.width = object.style.width;

	xmlhttp.onreadystatechange = function()
	{
		//~ alert("xmlhttp.readyState: " + xmlhttp.readyState + "\n\nRESPONSE:\n" + xmlhttp.responseText);
		if(xmlhttp.readyState === 4) {
			IdTarget.innerHTML = xmlhttp.responseText;
			var coord = GetObjCoord(object);
			var x = coord['x'];
			var y = coord['y'];
			MoveObj(target, x, y, 'block');
		}

		r = document.getElementById('runscript');
		//~ alert(r);
		if (r) {
			eval(r.innerHTML);
		}
	};

	xmlhttp.open("GET", url, true);
	xmlhttp.send(null);

	//~ document.getElementById(target).style.min-height = "auto";
	//~ document.getElementById(target).style.height = "auto";
	//~ document.getElementById(target).style.width="auto";
}

function ShowFormDiv (URLStr, left, top, width, height, target)
{
	if (document.getElementById(target)) {
		t = document.getElementById(target);
	} else {
		t = parent.document.getElementById(target);
	}

	t.style.left = left;
	t.style.top = top;

	if (isNaN(width)) {
		// se non e' un numero settalo automatico:
		t.style.width = 'auto';
	} else {
		// altrimenti setta il valore in pixel:
		t.style.width = (parseInt(width) + 3) + 'px';
	}

	if (isNaN(height)) {
		// se non e' un numero settalo automatico:
		t.style.height = 'auto';
	} else {
		// altrimenti setta il valore in pixel:
		t.style.height = (parseInt(height) + 3) + 'px';
	}

	t.style.display = 'block';
	t.innerHTML = '<iframe id="objpopup" width="' + width + ';" height="'
		+ height + ';" type="text/html" src="' + URLStr + '" frameBorder="0" scrolling="yes"></iframe>';
}

function ClosePopup()
{
	t = window.parent.document.getElementById('popup');
	//~ alert(t);

	//t = parent.document.getElementById('popup');
	//~ alert(t);

	//t = parent.document.getElementById('popup');
	//t = top.document.getElementById('popup');
	//alert(top.window.document.getElementById('popup'));
	//alert(document.getElementById('popup'));

	if (t) {
		t.style.display = 'none';
	} else {
		//~ alert(window.frames.parent.document.getElementById('popup'));
		//~ document.all["popup"].style.display = 'none';
		//~ alert(document.all);
		//~ for (id in document.all)
		//~ {
			//~ alert(document.all[id].id);
		//~ }
		//~ alert(parentNode);
		//~ this.popup.style.display = 'none';
	}
}

/**
 * Esegue una chiamata ajax
 * @param {type} requestUrl
 * @returns {xmlhttp.responseText|Boolean} */
function getData(requestUrl)
{
    var responseData = {};
    var xmlHttp = CreateAjaxRequestObject();

    xmlHttp.open("GET", requestUrl, true);
	xmlHttp.send(null);

    xmlHttp.onreadystatechange = function()
	{
        /* if ((xmlhttp.readyState === 4) && (xmlhttp.status === 200)) {
            responseData = xmlhttp.responseText;
        }*/
        responseData.requestInfo = xmlHttp;
        responseData.requestData = xmlHttp.responseText;
    };

    return responseData;
}
/*
 *      Javascript.js
 *
 *      Copyright 2010 Claudio Giordano <claudio.giordano@autistici.org>
 *
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 3 of the License, or
 *      (at your option) any later version.
 *
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 *      MA 02110-1301, USA.
 */

function posta (IdFrm,url)
{
	document.forms[IdFrm].action=url;
	document.forms[IdFrm].submit();
}

function conferma_posta (IdFrm, mesg, url, PrecUrl)
{
	var agree = confirm(mesg);

	if (agree)
	{
		document.forms[IdFrm].action = url;
		document.forms[IdFrm].submit();
	}else{
		window.document.location.href = PrecUrl;
	}
}

function conferma(mesg, url, PrecUrl)
{
	var agree=confirm(mesg);
	if (agree)
			posta(url);
	else
		window.document.location.href = PrecUrl;
}

function conferma_salva(IdFrm, mesg, url)
{
	var agree = confirm (mesg);

	if (agree)
	{
		posta (IdFrm, url);
	}
}

function go_conf(mesg,url,PrecUrl)
{
	var agree=confirm(mesg);
	if (agree)
		window.document.location.href=url;
	else
		window.document.location.href=PrecUrl;
}

function go_conf2(mesg,url)
{
	var agree=confirm(mesg);
	if (agree)
		window.document.location.href=url;
}

function go(url)
{
	window.document.location.href=url;
}

var popUpWin=0;
function popUpWindow(PopName, URLStr, left, top, width, height)
{
	if(popUpWin)
	{
		if(!popUpWin.closed)
			popUpWin.close();
	}
	option='"toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,';
	option+='resizable=yes,copyhistory=yes,';
	option+='width='+width+',height='+height+',left='+left+', top='+top+',screenX='+left+',screenY='+top+'"';
	popUpWin=window.open(URLStr, PopName, option);
}

function SelectLocalita (prov, comune, cap)
{
	var f=window.opener.window.document.forms[0];
	if (confirm("Confermi la selezione della località?"))
	{
		f.prov.value = prov;
		f.citta.value = comune;
		f.cap.value = cap;
		window.close();
	}
}

function setText(TargetId, Str)
{
	document.getElementById(TargetId).value = Str;
}

function GetObjCoord(obj)
{
	//~ uso: var [x,y] = GetObjCoord(obj);
	var curleft = curtop = 0;
	if (obj.offsetParent)
	{
		do {
			curleft += obj.offsetLeft - 0.5;
			curtop += obj.offsetTop;
			} while ((obj = obj.offsetParent));

		//~ array associativo:
		var coord = { "x": curleft, "y": curtop };
		//~ return [curleft, curtop];
		return coord;
	}else{
        return false;
    }
}

function MoveObj(IdObj, x, y, status)
{
	object = document.getElementById(IdObj);
	object.style.top = (y + 22) + "px"; //~ altezza di un rigo standard
	object.style.left = (x + 3) + "px";
	object.style.display = status;
}

function showResult(obj, array, TargetId)
{
	var target = document.getElementById(TargetId);
	var table = "";
	var coord = "";
	var x = 0;
	var y = 0;

	if (obj)
	{
		//~ var [x,y] = GetObjCoord(obj);
		coord = GetObjCoord(obj);
		x = coord['x'];
		y = coord['y'];
	}
	var index = -1;
	var flag_find = false;

	if (obj.value != "")
	{
		var str = obj.value;
		var pattern = new RegExp ("/*" + obj.value + "+", "i");
		table = '<table id="rsearch" style="width: 100%;">';
		for (index in Sottoconti)
		{
			if (pattern.test(Sottoconti[index]))
			{
				str = Sottoconti[index];
				table += '<tr><td>';
				table += '<a href="javascript: setText(\'' + obj.name + '\', \'';
				table += str + '\'); void(0);" title="Click per selezionare la voce">';
				table += str + '</a>';
				table += '</td></tr>';
				flag_find = true;
			}
		}
	}else{
		MoveObj('lsearch', x, y, 'none');
	}
	table += '</table>';

	if (flag_find)
	{
		MoveObj('lsearch', x, y, 'block');
		target.innerHTML = table;
	}else{
		MoveObj('lsearch', x, y, 'none');
	}
}

function SubmitEnter(field, event)
{
	if (event.keyCode == 13)
	{
		field.form.submit();
		return false;
	}else{
        return true;
    }
}

function ShowFrameDiv (URLStr, left, top, width, height, target, frame)
{
	//in disuso
	t = document.getElementById(target);
	f = document.getElementById(frame);

	t.style.left = left;
	t.style.top = top;
	t.style.width = width;
	t.style.height = height;

	f.src = URLStr;
	t.style.display = 'block';
}

function SelectAll(Name, Action)
{
  var selects = document.getElementsByName(Name);
	for (index in selects)
  {
		if (selects[index].type == "checkbox")
		{
			switch (Action)
			{
				case "comm":
				{
					selects[index].checked = !(selects[index].checked);
					break;
				}

				case "sel":
				{
					selects[index].checked = true;
					break;
				}

				case "unsel":
				{
					selects[index].checked = false;
					break;
				}
			}
		}
  }
}

function CheckSelected(Name)
{
	var Selected = false;
	var selects = document.getElementsByName(Name);
	for (index in selects)
  {
		if ((selects[index].type == "checkbox")
			&& (selects[index].checked))
		{
			Selected = true;
		}
  }

  return Selected;
}

function ConvertKey (KeyFrom, KeyTo)
{
	// cross-browser happy
	//var code = (event.charCode) ? event.charCode : event.keyCode;
	//~ var key = window.event.keyCode;

	if (window.event.keyCode == KeyFrom)
	{
		window.event.keyCode = KeyTo;
		window.event.charCode = KeyTo;
		window.event.keyCode.which = '-';
	}

  return window.event.keyCode;
}

function SendOutput(target, url)
{
	xmlhttp = CreateAjaxRequestObject();

	if (document.getElementById(target))
	{
		t = document.getElementById(target);
	}else{
		t = parent.document.getElementById(target);
	}

	//~ alert(t);

	xmlhttp.onreadystatechange = function()
	{
		//~ alert("state: " + xmlhttp.readyState + ", status " + xmlhttp.status);
		//~ t.innerHTML = "state: " + xmlhttp.readyState + ", status " + xmlhttp.status + "\n\n" + xmlhttp.responseText;
		if ((xmlhttp.readyState == 4) && (xmlhttp.status == 200))
		{
			t.innerHTML = "state: " + xmlhttp.readyState + ", status " + xmlhttp.status + "\n\n" + xmlhttp.responseText;
			//~ var out = xmlhttp.responseText;
			//~ alert("richiesta completata");

			//~ t.innerHTML = "pprrrrrrrrrrrr";
			//~ alert("response:\n" + out);
			//~ parent.document.getElementById('dati_salvati').innerHTML = xmlhttp.responseText;
		}
	};

	xmlhttp.open("GET", url, true);
	xmlhttp.send();
}

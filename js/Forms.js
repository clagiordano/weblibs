/*
 *      Forms.js
 *
 *      Copyright 2011 Claudio Giordano <claudio.giordano@autistici.org>
 *
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 3 of the License, or
 *      (at your option) any later version.
 *
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 *      MA 02110-1301, USA.
 */

String.prototype.trim = function() 
{
    return this.replace(/^\s+|\s+$/g, "");
};

function ValidateField (object, pattern)
{
	/*
	 * 27/01/2012   Claudio Giordano  <claudio.giordano@autistici.org>
	 * Disattivata correzione automatica slash in quanto su IE8 venivano
	 * corretti involontariamente anche gli slash delle date con ovvi problemi.
	 */

	//~ Drop slashes
	//~ var re 			 = new RegExp ("(\\\\|/)", "g");
	//~ object.value = object.value.replace(re, '');

	//~ Convert ' to -
	//~ var re 			 = new RegExp ("'", "g");
	//~ object.value = object.value.replace(re, '-');

	var re 			 = new RegExp (pattern);
	var match 	 = re.test(object.value);

	//~ alert("match:\n Name: " + object.name + "\nMatched: " + match
	//~	+ "\n Pattern: '" + pattern + "'\n Value: '" + object.value + "'");

	if (match == true)
	{
		object.style.border = "";
		return true;
	}else{
		//~ se contiene errori coloralo di rosso
		object.style.border = "3px solid red";
		//~ object.style.background = "red";
		return false;
	}

	//~ if (object.value == "")
	//~ {
		//~ object.style.background = "";
	//~ }
}

function DrawMenu (object, target, MenuGroup)
{
	MenuOut = '<table style="width: 100%;" class="menu">';

	for (id in MenuArray)
	{
		if (MenuArray[id]['gruppo'] == MenuGroup)
		{
			MenuOut += '<tr><td><a ';
			MenuOut += 'href="' + MenuArray[id]['link'] + '" ';
			MenuOut += 'title="' + MenuArray[id]['tip'] + '">';
			MenuOut += MenuArray[id]['titolo'];
			MenuOut += '</a></td></tr>';
		}
	}

	MenuOut += '</table>';
	document.getElementById(target).innerHTML = MenuOut;
	ShowObject(object, target);
}

function Expand (object, image, defaultH, ImgPath1, ImgPath2)
{
	object = document.getElementById(object);
	image = document.getElementById(image);

	if (object.style.height == "auto")
	{
		object.style.height = defaultH;
		image.src = ImgPath1;
	}else{
		object.style.height = "auto";
		image.src = ImgPath2;
	}
}

function VerificaSalvaPost(IdFrm, mesg, url)
{
	//~ alert("Form ID: " + IdFrm + "\nMessage: "
		//~ + mesg + "\nUrlStri: " + url);

	var Agree = confirm (mesg);
	var Status = true;
	var Elems = document.forms[IdFrm].elements;

	if (Agree)
	{
		//~ var str = "";
		//~ for (var i = 0; i < Elems.length; i++)
		//~ {
			//~ str += "Type: '" + Elems[i].type + "',  ";
			//~ str += "Name: '" + Elems[i].name + "'\n";
			//~ str += "Value: '" + Elems[i].value + "'  ";
			//~ str += "Pattern: '" + Elems[i].pattern + "'  ";
			//~ str += "required: '" + Elems[i].required + "'  ";
			//~ str += "\n\n";
		//~ }
		//~ alert(str);
		
		var j = 0;
		for (j = 0; j < Elems.length; j++)
		{
			if ((Elems[j].pattern != undefined
				&& (Elems[j].pattern != null)
				&& (Elems[j].pattern != "")
				&& Elems[j].required) || ((Elems[j].type == "select-one") && Elems[j].required))
			{
				if (Elems[j].pattern == undefined)
				{
					Elems[j].pattern = "[^-]";
				}

				//~ alert("Valido il campo: (" + j + ") " + Elems[j].name
					//~ + ", pattern: '" + Elems[j].pattern + "'");

				//~ Verifico  il campo
				var SingleStatus = ValidateField(Elems[j], Elems[j].pattern);

				if (SingleStatus == false)
				{
					Status = false;
				}
			}
		}

		//~ al termine della validazione se ci sono errori alert
		if (Status == false)
		{
			alert("Attenzione, alcuni campi presentano errori di compilazione,\n"
				+ "e' necessario correggerli prima di poter proseguire.");
		}else{
			//~ altrimenti posta
			document.forms[IdFrm].action = url;
			document.forms[IdFrm].submit();
		}
	}
}

function getMultiSelect( InputName, Location ) 
{
    selects = window.parent.document.getElementsByName( InputName );
    multi_mod = "";
    for (index in selects)
    {
        if (selects[index].checked)
        {
            //alert(selects[index].value);
            // converto gli id dei selezionati in una stringa da passare
            // alla stessa pagina:
            multi_mod = multi_mod + selects[index].value + ";";
        }
    }

    // eseguo il redirect passando alla pagina gli id dei selezionati:
    document.location.href = Location + multi_mod;
}
CREATE TABLE `tab_fatture` (
  `id_fattura` int(11) NOT NULL auto_increment,
  `cod_fattura` varchar(30) NOT NULL,
  `id_anag` int(11) NOT NULL,
  `data` date NOT NULL,
  `imponibile` float NOT NULL,
  `totale` float NOT NULL,
  `spese` float NOT NULL,
  `id_tipo` int(11) NOT NULL,
  `id_causale` int(11) NOT NULL,
  `id_pagamento` int(11) NOT NULL
  PRIMARY KEY  (`id_fattura`)
)

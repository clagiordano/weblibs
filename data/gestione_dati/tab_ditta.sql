-- phpMyAdmin SQL Dump
-- version 2.11.6
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generato il: 01 Nov, 2008 at 12:20 AM
-- Versione MySQL: 5.0.51
-- Versione PHP: 5.2.6

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `pg_gestione`
--

-- --------------------------------------------------------

--
-- Struttura della tabella `tab_ditta`
--

CREATE TABLE `tab_ditta` (
  `id_ditta` int(11) NOT NULL auto_increment,
  `rag_soc` varchar(50) NOT NULL,
  `p_iva` char(11) NOT NULL,
  `cf` char(16) NOT NULL,
  `cognome` varchar(50) NOT NULL,
  `indirizzo` varchar(100) NOT NULL,
  `telefono` varchar(20) NOT NULL default '',
  `mail` varchar(70) NOT NULL,
  `r_vendite` varchar(50) NOT NULL,
  `cap` char(5) NOT NULL,
  `prov` char(2) NOT NULL,
  `fax` varchar(20) NOT NULL default '',
  `stato` varchar(50) NOT NULL,
  `nome` varchar(50) NOT NULL,
  `citta` varchar(50) NOT NULL,
  `civico` varchar(10) NOT NULL,
  `cellulare` varchar(20) NOT NULL,
  PRIMARY KEY  (`id_ditta`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COMMENT='latin1_swedish_ci' AUTO_INCREMENT=3 ;

--
-- Dump dei dati per la tabella `tab_ditta`
--

INSERT INTO `tab_ditta` (`id_ditta`, `rag_soc`, `p_iva`, `cf`, `cognome`, `indirizzo`, `telefono`, `mail`, `r_vendite`, `cap`, `prov`, `fax`, `stato`, `nome`, `citta`, `civico`, `cellulare`) VALUES
(1, 'Rm Di Giorgio Mazzola', '04988260826', '', 'Mazzola', 'Via Salvatore Puglisi', '091', 'info@erremmeweb.com', '', '90100', 'Pa', '091', 'Italia', 'Giorgio', 'Palermo', '45', '328');

-- MySQL dump 10.13  Distrib 5.5.24, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: gestione
-- ------------------------------------------------------
-- Server version	5.5.24-3

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tab_config`
--

DROP TABLE IF EXISTS `tab_config`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tab_config` (
  `id_config` int(11) NOT NULL AUTO_INCREMENT,
  `opt` varchar(30) NOT NULL,
  `val` varchar(200) NOT NULL,
  PRIMARY KEY (`id_config`)
) ENGINE=MyISAM AUTO_INCREMENT=19 DEFAULT CHARSET=latin1 COMMENT='latin1_swedish_ci';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tab_config`
--

LOCK TABLES `tab_config` WRITE;
/*!40000 ALTER TABLE `tab_config` DISABLE KEYS */;
INSERT INTO `tab_config` VALUES (1,'enabled','0'),(2,'ditta_rag_soc','RM di Giorgio Mazzola'),(3,'ditta_piva','4988260826'),(4,'ditta_indirizzo','Via Salvatore Puglisi, 45'),(5,'ditta_telefono','091 9773179'),(6,'ditta_cognome','Mazzola'),(7,'ditta_nome','Giorgio'),(8,'ditta_mail','info@erremmeweb.com'),(9,'ditta_responsabile','Giorgio Mazzola'),(10,'ditta_cap','90100'),(11,'ditta_id_prov','82'),(12,'ditta_id_citta','7327'),(13,'ditta_stato','Italia'),(14,'ditta_fax','091 326462'),(15,'ditta_cellulare','3701170481'),(16,'ditta_logo','img/logo.jpg'),(17,'ditta_layout_doc','1'),(18,'ditta_iban','IT14S0501804600000000116130');
/*!40000 ALTER TABLE `tab_config` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2012-06-11 17:29:00

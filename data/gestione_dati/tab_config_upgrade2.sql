/*
-- Query: SELECT * FROM gestione.tab_config where opt like 'ditta_doc%'
LIMIT 0, 1000

-- Date: 2012-06-21 16:49
*/
INSERT INTO `tab_config` (`id_config`,`opt`,`val`) VALUES (19,'ditta_doc_show_logo','0');
INSERT INTO `tab_config` (`id_config`,`opt`,`val`) VALUES (20,'ditta_doc_cod_forn','1');
INSERT INTO `tab_config` (`id_config`,`opt`,`val`) VALUES (21,'ditta_doc_cod_int','0');
INSERT INTO `tab_config` (`id_config`,`opt`,`val`) VALUES (22,'ditta_doc_descr','0');
INSERT INTO `tab_config` (`id_config`,`opt`,`val`) VALUES (23,'ditta_doc_qta','0');
INSERT INTO `tab_config` (`id_config`,`opt`,`val`) VALUES (24,'ditta_doc_prz','0');
INSERT INTO `tab_config` (`id_config`,`opt`,`val`) VALUES (25,'ditta_doc_sco','0');
INSERT INTO `tab_config` (`id_config`,`opt`,`val`) VALUES (26,'ditta_doc_importo','0');
INSERT INTO `tab_config` (`id_config`,`opt`,`val`) VALUES (27,'ditta_doc_iva','0');
INSERT INTO `tab_config` (`id_config`,`opt`,`val`) VALUES (28,'ditta_doc_note_art','1');

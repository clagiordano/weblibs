<?php

namespace clagiordano\weblibs\php;

/**
 * Class to manage date and time string, format conversion and timing.
 *
 * @version  1.2
 * @author   Claudio Giordano <claudio.giordano@autistici.org>
 * @since    2014-04-06
 * @category Tools
 */
class Conversion
{

    /**
     * Vecchia funzione di conversione dimensione file in disuso,
     * sostituita da convertSize.
     *
     * @deprecated since version 1.0
     * @param  int $bytes
     * @return string
     */
    public function convertBytes($bytes)
    {
        $size = $bytes / 1024;
        if ($size < 1024) {
            $size = number_format($size, 2);
            $size .= ' KB';
        } else {
            if ($size / 1024 < 1024) {
                $size = number_format($size / 1024, 2);
                $size .= ' MB';
            } elseif ($size / 1024 / 1024 < 1024) {
                $size = number_format($size / 1024 / 1024, 2);
                $size .= ' GB';
            }
        }

        return $size;
    }

    /**
     * Funzione che converte un numero in dimensioni di byte corrispondenti.
     *
     * @param  int $size
     * @return string
     */
    public function convertSize($size)
    {
        $filesizename = array(" Bytes", " KB", " MB", " GB", " TB", " PB", " EB", " ZB", " YB");

        return $size ? round(
            $size / pow(1024, ($i = floor(log($size, 1024)))),
            2
        ) . $filesizename[$i] : '0 Bytes';
    }

    /**
     * Funzione che spezza una stringa e la riunisce con il codice
     * di ritorno a capo <br />
     *
     * @param string $separatore
     * @param string $stringa
     * @return string
     */
    public function addBR($separatore, $stringa)
    {
        $tmp_array = explode($separatore, $stringa);
        for ($index = 0; $index <= count($tmp_array) - 1; $index++) {
            if ($index != count($tmp_array) - 1) {
                $result = $result . $tmp_array[$index] . $separatore . "<br />";
            } else {
                $result = $result . $tmp_array[$index];
            }
        }

        return $result;
    }

    /**
     * Funzione che sostituisce agli spazi nella ricerca con "%"
     *
     * @param  string $string
     * @return string
     */
    public function expandSearch($string)
    {
        $temp_val = explode(" ", $string);
        $string   = join("%", $temp_val);

        return $string;
    }

    /**
     * Funzione che suddivide una stringa in singoli caratteri
     *
     * @param  string $string
     * @return array
     */
    public function splitString($string)
    {
        $len    = strlen($string) - 1;
        $result = array();
        for ($i = 0; $i <= $len; $i++) {
            array_push($result, substr($string, $i, 1));
        }

        return $result;
    }

    /**
     * Funzione che cerca una stringa di testo, tramite altre stringhe
     * obsoleta grazie alle regexp
     *
     * @deprecated since version 0.1
     *
     * @param  string $stringa
     * @param  int $inizio
     * @param  int $fine
     * @param  int $offset
     * @param  int $foffset
     * @return string
     */
    public function findStr($stringa, $inizio, $fine, $offset, $foffset)
    {
        $v_inizio = strpos($stringa, $inizio);
        if ($v_inizio !== false) {
            $v_inizio += strlen($inizio) - $offset;
            $v_fine  = strpos($stringa, $fine) - $foffset;
            $trovato = ucfirst(substr($stringa, $v_inizio, $v_fine - $v_inizio));
        } else {
            $trovato = "error";
        }

        return $trovato;
    }

    /**
     * Funzione che aggiunge il carattere di espansione % per le query
     *
     * @param  string $val
     * @param  string $modo
     * @return string
     */
    public function modoRicerca($val, $modo = "tutto")
    {
        switch ($modo) {
            case "esatto":
                $iniz = "";
                $fine = "";
                break;

            case "inizio":
                $iniz = "";
                $fine = "%";
                break;

            case "fine":
                $iniz = "%";
                $fine = "";
                break;

            case "tutto":
                $iniz = "%";
                $fine = "%";
                break;
        }

        $valore = $iniz . $val . $fine;

        return $valore;
    }

    /**
     * Split string argument with $Separator and reassemble it with $Glue.
     *
     * @param  string $Text String to split
     * @param  string $Separator String for split string
     * @param  string $Glue String for join splitted string
     * @return string $string    Return string
     */
    public function multiString($Text, $Separator, $Glue)
    {
        $tmp_array = explode($Separator, $Text);
        $string    = "";

        foreach ($tmp_array as $key => $field) {
            if ($key != count($tmp_array) - 1) {
                $string .= trim($field) . $Glue;
            } else {
                $string .= trim($field);
            }
        }

        return $string;
    }

    /**
     * Return throught regexp the first N words of the string.
     *
     * @param  string $string String to truncate
     * @param  int $numWords Number for words to get
     * @param  string $dots String for helision
     * @return string
     */
    public function getFirstWords($string, $numWords = 5, $dots = ' ...')
    {
        $match = array();
        preg_match("/^(\W*\w+){0,$numWords}/", $string, $match);

        return $match[0] . $dots;
    }
}

<?php

namespace clagiordano\weblibs\php;

/**
 * Classe per la verifica della visilità di un media.
 *
 * @author Claudio Giordano <claudio.giordano@wyscout.com>
 */
final class Visibility
{
    const MEDIA_CHECK_FIELDS = "id, userId, cerebrumGroupId, visibility";

    private $isPrivate       = false;     // flag che indica se il media è privato o no
    private $isVisible       = false;     // flag che indica se il media è visibile o no
    private $statusMessage   = "";        // messaggio in caso di errore
    private $mediaData       = array();   // array con i dati del media da validare
    private $response        = array();   // array di risposta.
    private $mediaId         = 0;
    private $mediaType       = "";
    private $userId          = 0;
    private $cerebrumGroupId = 0;
    private $subGroupId      = 0;

    /**
     * @param array $mixed
     * @param array $mixed['obj']:             required - Tipo di media da validare (mediaType).
     * @param array $mixed['objId']:           required - Id del media da validare (mediaId).
     * @param array $mixed['userId']:          optional - Id dell'utente del quale verificare la visibilità.
     * @param array $mixed['cerebrumGroupId']: optional - Id del gruppo dell'utente del quale verificare la visibilità.
     * @param array $mixed['subGroupId']:      optional - Id del sottogruppo dell'utente del quale verificare la visibilità.
     */
    public function __construct(array $mixed = array())
    {
        // Verifica che il mediaId sia un intero valido, in caso contrario esce.
        if (!($this->mediaId = filter_var($mixed['objId'], FILTER_VALIDATE_INT))) {
            exit("[" . __METHOD__ . "] Invalid objId '" . $mixed['objId'] . "'.");
        }

        // Eseguo la validazione del campo mediaType
        if (!($this->mediaType = filter_var(
            $mixed['obj'],
            FILTER_SANITIZE_STRING
        )) || (!defined('media_type::' . $mixed['obj']))) {
            exit("[" . __METHOD__ . "] Invalid obj '" . $mixed['obj'] . "'.");
        }

        // Verifica che il userId sia un intero valido, in caso contrario utilizza quello dell'utente.
        if (!($this->userId = filter_var($mixed['userId'], FILTER_VALIDATE_INT))) {
            $this->userId = my_id();
        }

        // Verifica che il cerebrumGroupId sia un intero valido, in caso contrario utilizza quello dell'utente.
        if (!($this->cerebrumGroupId = filter_var(
            $mixed['cerebrumGroupId'],
            FILTER_VALIDATE_INT
        ))) {
            $this->cerebrumGroupId = network::getMyGroupId();
        }

        // Verifica che il subGroupId sia un intero valido, in caso contrario utilizza quello dell'utente.
        if (!($this->subGroupId = filter_var(
            $mixed['subGroupId'],
            FILTER_VALIDATE_INT
        ))) {
            $this->subGroupId = network::getMyNetworkSubgroups();
        }

        return $this;
    }

    /**
     * Metodo che restituisce TRUE/FALSE in caso il media sia rispettivamente
     * visibile/non visibile, in funzione dei parametri forniti.
     *
     * @param string $key la chiave dell'array
     * @return mixed $retVal
     */
    public function is_visible($key = null)
    {
        $retVal = null;
        $this->getMediaInfo();

        // preparo l'array di risposta per la visibilità
        $this->response = array('bool'      => $this->isVisible,
            'isprivate' => $this->isPrivate,
            'msg'       => $this->statusMessage
        );

        if (is_null($key)) {
            $retVal = $this->response;
        } else {
            $retVal = $this->response[$key];
        }

        return $retVal;
    }

    public function print_r()
    {
        printf("<pre>");
        printf(
            "[DEBUG][%s]:      is_private: %d<br />",
            __CLASS__,
            $this->isPrivate
        );
        printf(
            "[DEBUG][%s]:   statusMessage: '%s'<br />",
            __CLASS__,
            $this->statusMessage
        );
        printf(
            "[DEBUG][%s]:       mediaData: <pre>%s</pre><br />",
            __CLASS__,
            print_r($this->mediaData, true)
        );

        printf(
            "[DEBUG][%s]:         mediaId: %d<br />",
            __CLASS__,
            $this->mediaId
        );
        printf(
            "[DEBUG][%s]:       mediaType: '%s'<br />",
            __CLASS__,
            $this->mediaType
        );
        printf(
            "[DEBUG][%s]:          userId: %d<br />",
            __CLASS__,
            $this->userId
        );
        printf(
            "[DEBUG][%s]: cerebrumGroupId: %d<br />",
            __CLASS__,
            $this->cerebrumGroupId
        );
        printf(
            "[DEBUG][%s]:      subGroupId: %d<br />",
            __CLASS__,
            $this->subGroupId
        );
        printf("</pre>");

        return $this;
    }

    /**
     * Metodo che estrae le informazioni del madia dal db e le passa al metodo di validazione
     */
    private function getMediaInfo()
    {
        $qMediaInfo = "SELECT " . self::MEDIA_CHECK_FIELDS . " FROM `"
                . constant('media_type::' . $this->mediaType) . "` WHERE id  = " . $this->mediaId;
        //pr($qMediaInfo);

        $query                         = new aeq($qMediaInfo, true);
        $this->mediaData               = $query->fetch();
        //pr($this->mediaData);
        // converto la stringa visibility in array
        $this->mediaData['visibility'] = explode(
            ",",
            $this->mediaData['visibility']
        );

        $this->checkVisibility();
        unset($query);

        return $this;
    }

    /**
     * Metodo di validazione dei permessi di visibilità del media.
     */
    private function checkVisibility()
    {

        if ($this->mediaData['userId'] > 0) {
            // se il media ha un userId è privato in quanto ha un proprietario
            $this->isPrivate = true;

            // verifica se l'utente ha la capability indicata per visualizzare il match
            if (can('view_all')) {
                if ($this->cerebrumGroupId == $this->mediaData['cerebrumGroupId']) {
                    $this->isVisible = true;
                } else {
                    $this->isVisible     = true;
                    $this->statusMessage = 'MEDIA_CONTENT_NOT_IN_GROUP';
                }
            } else {
                if ($this->userId == $this->mediaData['userId']) {
                    $this->isPrivate = true;
                    $this->isVisible = true;
                } elseif (($this->cerebrumGroupId == $this->mediaData['cerebrumGroupId']) && (in_array(
                    $this->mediaData['visibility'],
                    $this->subGroupId
                ))) {
                    $this->isVisible = true;
                } else {
                    $this->isVisible     = false;
                    $this->statusMessage = 'MEDIA_CONTENT_NOT_AVAILABLE';
                }
            }
        } else {
            // se non è privato il media è di proprietà Wyscout quindi visibile a tutti (pubblico)
            $this->isPrivate = false;
            $this->isVisible = true;
        }

        return $this;
    }
}

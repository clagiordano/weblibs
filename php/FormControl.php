<?php

namespace clagiordano\weblibs\php;

/**
 * @version  0.5
 * @author   Claudio Giordano <claudio.giordano@autistici.org>
 * @since    2015-04-07
 * @category Layout
 * @package  weblibs
 */
class FormControl
{
    private $Params         = null;
    private $MarkupHtml     = null;
    private $MarkupProperty = null;
    private $Type           = null;
    private $Name           = null;
    private $Label          = null;
    private $Pattern        = null;
    private $Value          = null;
    private $CheckValue     = null;
    private $Size           = null;
    private $MaxLenght      = null;
    private $Javascript     = null;
    private $Data           = null;
    private $ID             = null;
    private $Field          = null;
    private $Style          = null;
    private $RowHeight      = null;
    private $Placeholder    = null;

    public function __construct(array $mixed = [])
    {
        $this->Params = $mixed;

        foreach ($this->Params as $Key => $Value) {
            $this->$Key = $Value;
            
            if (is_callable("self::set$Key")) {
                $Setter = "set$Key";
                echo "setter set$Key callable<br>";
                $this->$Setter($Value);
            }
        }

        return $this;
    }

    public function render()
    {
        $this->renderProperty();

//        switch ($this->Type) {
//            case 'text':
                $this->MarkupHtml .= '<input' . $this->MarkupProperty;
//                break;
//        }

        $this->MarkupHtml .= "/>";

        return $this->MarkupHtml;
    }

    private function renderProperty()
    {

        if ($this->Name) {
            $this->MarkupProperty .= ' name="' . $this->Name . '" ';
        } else {
            throw new \Exception("Error, invalid control name.");
        }
        
        if ($this->Placeholder) {
            $this->MarkupProperty .= ' placeholder="' . $this->Placeholder . '" ';
        }
        
        if ($this->Style) {
            $this->MarkupProperty .= ' style="' . $this->Style . '" ';
        }
    }
    
    /**
     *
     * @return type
     */
    function getType()
    {
        return $this->Type;
    }

    /**
     *
     * @param string $Type
     * @return \weblibs\php\FormControl
     * @throws \Exception
     */
    function setType($Type)
    {
        $this->Type = filter_var($Type, FILTER_SANITIZE_STRING);
        
        if ($this->Type) {
            $this->MarkupProperty .= ' type="' . $this->Type . '" ';
        } else {
            throw new \Exception("Error, invalid control type.");
        }
        
        return $this;
    }

    /**
     *
     * @return type
     */
    function getMaxLenght()
    {
        return $this->MaxLenght;
    }

    /**
     *
     * @param int $MaxLenght
     * @return \weblibs\php\FormControl
     */
    function setMaxLenght($MaxLenght)
    {
        $this->MaxLenght = filter_var($MaxLenght, FILTER_VALIDATE_INT);
        
        if ($this->MaxLenght) {
            $this->MarkupProperty .= ' maxlength="' . $this->MaxLenght . '" ';
        }
        
        $this->MaxLenght = $MaxLenght;
        return $this;
    }

    /**
     *
     * @return type
     */
    function getLabel()
    {
        return $this->Label;
    }

    /**
     *
     * @param string $Label
     * @return \weblibs\php\FormControl
     */
    function setLabel($Label)
    {
        $this->Label = filter_var($Label, FILTER_SANITIZE_STRING);
        
        if ($this->Label && !$this->Placeholder) {
            $this->Placeholder = $this->Label;
        }
        
        if ($this->Label) {
            $this->MarkupProperty .= ' title="' . $this->Label . '" ';
        }

        return $this;
    }

    /**
     *
     * @return type
     */
    function getPattern()
    {
        return $this->Pattern;
    }

    /**
     *
     * @param string $Pattern
     * @return \weblibs\php\FormControl
     */
    function setPattern($Pattern)
    {
        $this->Pattern = filter_var($Pattern, FILTER_SANITIZE_STRING);
        
        if ($this->Pattern) {
            $this->MarkupProperty = ' required pattern="' . $this->Pattern . '" ';
        }
        
        return $this;
    }

    /**
     *
     * @return type
     */
    function getValue()
    {
        return $this->Value;
    }

    /**
     *
     * @return type
     */
    function getCheckValue()
    {
        return $this->CheckValue;
    }

    /**
     *
     * @return type
     */
    function getSize()
    {
        return $this->Size;
    }

    function setValue($Value)
    {
        $this->Value = $Value;
        return $this;
    }

    function setCheckValue($CheckValue)
    {
        $this->CheckValue = $CheckValue;
        return $this;
    }

    function setSize($Size)
    {
        $this->Size = $Size;
        return $this;
    }

        
    /**
     *
     * @param type $Type
     * @param type $Name
     * @param type $Label
     * @param type $Pattern
     * @param string $Value
     * @param type $CheckValue
     * @param type $Size
     * @param type $MaxLenght
     * @param type $JavaScript
     * @param array $DataArray
     * @param type $ID
     * @param type $Field
     * @param type $Style
     * @param type $RowHeight
     */
    public function drawControl(
        $Type,
        $Name,
        $Label = "",
        $Pattern = "",
        $Value = "",
        $CheckValue = "",
        $Size = "10em;",
        $MaxLenght = "10",
        $JavaScript = "",
        $DataArray = "",
        $ID = "",
        $Field = "",
        $Style = "",
        $RowHeight = ""
    ) {

        if (trim($Size) == "") {
            exit("Errore, il valore '$Size' di Size non &egrave; valido, esco. <br />");
        }

        if (!isset($Value)) {
            $Value = "";
        }

        if (!isset($DataArray)) {
            $DataArray = [];
        }


        switch ($Type) {
            case 'text':
                ?>
                <input name="<?php echo $Name; ?>"
                       type="text"
                        <?php echo " " . $Placeholder . " " . $Pattern; ?>
                       title="<?php echo $Label; ?>"
                        <?php echo $MaxLenght; ?>
                       style="width: <?php echo $Size; ?>;"
                       value="<?php
                        if (isset($Value[$Name])) {
                            echo stripslashes($Value[$Name]);
                        }
                        ?>"
                        <?php echo " " . $JavaScript . " "; ?>
                       />

                <?php
                break;
            case 'radio':
            case 'checkbox':
                ?>
                <input name="<?php echo $Name; ?>"
                       type="<?php echo $Type; ?>"
                        <?php echo $Pattern; ?>
                       value="<?php echo $CheckValue; ?>"
                       title="<?php echo $Label; ?>"
                        <?php
                        if (isset($Value[$Name]) && ($Value[$Name] == $CheckValue)) {
                            echo "checked=\"checked\"";
                        }
                        ?> />
                        <?php
                        echo $Label;
                break;

            case 'select':
            case 'selectall':
                ?>
                <select name="<?php echo $Name; ?>"
                        title="<?php echo $Label; ?>"
                        style="width: <?php echo $Size; ?>;"
                        <?php echo " " . $JavaScript . " "; ?> >

                    <?php if ($Type == "select") {
?>
                        <option value="-"><?php echo $Label; ?></option>
                        <option value="-"></option>
                    <?php
} else {
?>
                        <option value="%">Nessun filtro</option>
                        <option value="%"></option>
                        <?php
}

foreach ($DataArray as $key => $field) {
    ?>
    <option value="<?php echo $field[$ID]; ?>"
            <?php
            if (isset($Value[$Name]) && ($Value[$Name] == $field[$ID])) {
                echo "selected=\"selected\"";
            }
            ?> >
                <?php echo $field[$Field]; ?>
                        </option>
                    <?php
} ?>
                </select>
                <?php
                break;

            case 'textarea':
                ?>
                <textarea name="<?php echo $Name; ?>" <?php echo " " . $Placeholder . " " . $Pattern . " " . $JavaScript . " "; ?> title="<?php echo $Label; ?>" style="width: <?php echo $Size; ?>; height: 3em;"><?php
                if (isset($Value[$Name])) {
                    echo stripslashes($Value[$Name]);
                }
                    ?></textarea>
                <?php
                break;

            case 'hidden':
                ?>
                <input name="<?php echo $Name; ?>" 
                       type="hidden"
                       value="<?php
                        if (isset($Value[$Name])) {
                            echo stripslashes($Value[$Name]);
                        }
                        ?>"
                       />
                <?php
                break;

            default:
                exit("Errore, tipo di controllo '$Type' non riconosciuto, esco. <br />");
        }
    }
}

<?php

namespace clagiordano\weblibs\php;

/**
 * Class to get information from user agent string and return them
 * in an object.
 *
 * @version  2.1.3
 * @author   Claudio Giordano <claudio.giordano@autistici.org>
 * @since    2014-04-04
 * @category Information
 * @package  weblibs
 *
 */
class BrowserInfo
{
    /** @var string $Platform System platform name */
    public $Platform = "Unknown";
    /** @var string $Architecture System platform architecture (32/64bit) */
    public $Architecture = "Unknown";
    /** @var string $Browser Browser name */
    public $Browser = "Unknown";
    /** @var int $Version Browser version */
    public $Version = "Unknown";
    /** @var string $RemoteAddress System remote ip address */
    public $RemoteAddress = "0.0.0.0";
    /** @var string $UserAgentString Server HTTP_USER_AGENT */
    private $UserAgentString = "";
    /** @var string $BrowserMatched Browser name matched form user agent */
    private $BrowserMatched = "";

    /**
     * @constructor
     * @param  boolean $useBrowscap
     * @return \weblibs\php\BrowserInfo
     */
    public function __construct($useBrowscap = false)
    {
        $this->UserAgentString = str_replace(
            '\n',
            '',
            filter_input(INPUT_SERVER, 'HTTP_USER_AGENT')
        );
        $this->getBrowserInfo($useBrowscap);

        return $this;
    }

    /**
     * Retrive information from native method get_browser with php_browscap.ini
     * or detect them with this class functions from $SERVER['HTTP_USER_AGENT']
     * and return an object BrowserInfo, with al properties setted.
     *
     * @param  boolean $useBrowscap
     * @return \weblibs\php\BrowserInfo
     */
    public function getBrowserInfo($useBrowscap = false)
    {
        if ($useBrowscap && ini_get('browscap')) {
            // In this case use get_browser with php_browscap.ini method:
            $Info = get_browser(null, true);
            $this->Platform = $Info['platform'];
            $this->Browser = $Info['browser'];
            $this->Version = $Info['version'];
        } else {
            // In this case start detection with regexp:
            $this->Platform = $this->detectPlatform();
            $this->Browser = $this->detectBrowser();
            $this->Version = $this->detectVersion();
        }

        // Common methods for both cases
        $this->Architecture = $this->detectArchitecture();
        $this->RemoteAddress = filter_input(
            INPUT_SERVER,
            'REMOTE_ADDR',
            FILTER_VALIDATE_IP
        );

        return $this;
    }

    /**
     * @return string
     */
    private function detectPlatform()
    {
        $platform = "Unknown";

        if (preg_match('/linux/i', $this->UserAgentString)) {
            $platform = 'Linux';
        } elseif (preg_match('/macintosh|mac os x/i', $this->UserAgentString)) {
            $platform = 'Mac';
        } elseif (preg_match('/windows|win32/i', $this->UserAgentString)) {
            $platform = 'Windows';
        } elseif (preg_match('/android/i', $this->UserAgentString)) {
            $platform = 'Android';
        } elseif (preg_match('/series40|series\ 60/i', $this->UserAgentString)) {
            $platform = 'SymbianOS';
        }

        /*
         * TODO Other agent:
         * Googlebot/version
         * Googlebot-Mobile/version
         * msnbot/version
         * Ezooms/version  (bot gmail)
         */

        return $platform;
    }

    /**
     * @return string
     */
    private function detectBrowser()
    {
        $browser = "Unknown";

        if (preg_match('/MSIE/i', $this->UserAgentString) && !preg_match(
            '/Opera/i',
            $this->UserAgentString
        )
        ) {
            $browser = 'Internet Explorer';
            $this->BrowserMatched = "MSIE";
        } elseif (preg_match('/Firefox/i', $this->UserAgentString)) {
            $browser = 'Mozilla Firefox';
            $this->BrowserMatched = "Firefox";
        } elseif (preg_match('/Chrome/i', $this->UserAgentString)) {
            $browser = 'Google Chrome';
            $this->BrowserMatched = "Chrome";
        } elseif (preg_match('/Safari/i', $this->UserAgentString)) {
            $browser = 'Apple Safari';
            $this->BrowserMatched = "Safari";
        } elseif (preg_match('/Opera/i', $this->UserAgentString)) {
            $browser = 'Opera';
            $this->BrowserMatched = "Opera";
        } elseif (preg_match('/Netscape/i', $this->UserAgentString)) {
            $browser = 'Netscape';
            $this->BrowserMatched = "Netscape";
        } elseif (preg_match('/S40OviBrowser/i', $this->UserAgentString)) {
            $browser = 'Nokia proxy browser';
            $this->BrowserMatched = "S40OviBrowser";
        }

        return $browser;
    }

    /**
     * @return string
     */
    private function detectVersion()
    {
        $version = "Unknown";
        $matches = [];

        // finally get the correct version number
        $known = ['Version', $this->BrowserMatched, 'other'];
        $pattern = '#(?<browser>' . join('|', $known) . ')[/ ]+(?<version>[0-9.|a-zA-Z.]*)#';
        if (!preg_match_all($pattern, $this->UserAgentString, $matches)) {
            // we have no matching number just continue
        }

        // see how many we have
        if (count($matches['browser']) != 1) {
            // we will have two since we are not using 'other' argument yet
            // see if version is before or after the name
            if (strripos($this->UserAgentString, "Version") < strripos(
                $this->UserAgentString,
                $this->BrowserMatched
            )
            ) {
                $version = $matches['version'][0];
            } elseif (isset($matches['version'][1])) {
                $version = $matches['version'][1];
            }
        } else {
            $version = $matches['version'][0];
        }

        // check if we have a number
        if ($version == null || $version == "") {
            $version = "Unknown";
        }

        //print $pattern;
        return $version;
    }

    /**
     * @return string
     */
    private function detectArchitecture()
    {
        $architecture = "Unknown";

        if (preg_match('/i386|i486|i686/i', $this->UserAgentString)) {
            $architecture = '32bit';
        } elseif (preg_match('/X86_64|ia64/i', $this->UserAgentString)) {
            $architecture = '64bit';
        }

        return $architecture;
    }

    /**
     * @return bool
     */
    public function identificationStatus()
    {
        $identification = true;

        if ($this->Platform == "Unknown") {
            $identification = false;
        } /* else if ($this->Architecture == "Unknown") {
          $identification = false;
          } */ elseif ($this->Browser == "Unknown") {
            $identification = false;
        } elseif ($this->Version == "Unknown") {
            $identification = false;
        } elseif ($this->RemoteAddress == "0.0.0.0") {
            $identification = false;
        }

        return $identification;
    }

    /**
     * Print internal properties info
     */
    public function printR()
    {
        echo "<pre>";
        echo "userAgentString: " . $this->UserAgentString . "\n";
        echo "       Platform: " . $this->Platform . "\n";
        echo "   Architecture: " . $this->Architecture . "\n";
        echo "        Browser: " . $this->Browser . "\n";
        echo "        Version: " . $this->Version . "\n";
        echo "  RemoteAddress: " . $this->RemoteAddress . "\n";
        echo "</pre>";
    }

    /**
     *
     * @return type
     */
    function getUserAgentString()
    {
        return $this->UserAgentString;
    }

    /**
     *
     * @param type $UserAgentString
     * @return \clagiordano\weblibs\php\BrowserInfo
     */
    function setUserAgentString($UserAgentString)
    {
        $this->UserAgentString = $UserAgentString;

        return $this;
    }


}

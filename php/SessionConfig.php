<?php

namespace clagiordano\weblibs\php;

/**
 * Classe di configurazione per le sessioni
 *
 * @version  1.0.0
 * @author   Claudio Giordano <claudio.giordano@autistici.org>
 * @since    2014-11-08
 * @category Configuration
 */
class SessionConfig
{
    const SESSION_TABLE = "sessions";
    const ID            = "id_session"; // primary id autoincrement
    const SESSION_ID    = "session_id"; // php_session id
    const USER_ID       = "id_user";
    const IP            = "ip";
    const ACTION        = "action";
    const USER_AGENT    = "user_agent";
    const PLATFORM      = "platform";
    const BROWSER       = "browser";
    const VERSION       = "version";
    const DATA          = "data";
    const SESSION_TIMEOUT = 0;
}

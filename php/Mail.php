<?php

namespace clagiordano\weblibs\php;

/**
 * Class to send and read email with direct send command to socket.
 *
 * @version 1.6.2
 * @author Claudio Giordano <claudio.giordano@autistici.org>
 * @since 2010-01-01
 * @category Network
 */
class Mail
{
    const SOCKET_TIMEOUT           = 30;
    const NETWORK_BUFFER           = 512;
    const LINE_ENDINGS             = "\r\n";
    const HEADER_MAILER            = "PHP-Code";
    const HEADER_MIME_VERSION      = "1.0";
    const HEADER_CHARSET           = "iso-8859-1";
    const HEADER_TRANSFER_ENCODING = "7bit";

    private $HostName       = null;
    private $UserName       = null;
    private $SenderDomain   = null;
    private $Password       = null;
    private $Protocol       = null;
    private $Port           = null;
    private $ErrorNum       = null;
    private $ErrorStr       = null;
    private $MsgHeaders     = [];
    private $MsgBody        = null;
    private $MsgBoundary    = null;
    private $MsgAttachments = [];
    private $Subject        = null;
    private $Attachments    = [];
    private $From           = [];
    private $To             = [];
    private $ToString       = "";
    private $Cc             = [];
    private $CcString       = "";
    private $Bcc            = [];
    private $BccString      = "";
    private $Body           = null;
    private $Connection     = null;
    private $ServerResponse = [];

    /**
     *
     * @param string $hostname
     * @param string $username
     * @param string $password
     * @param string $protocol
     * @return \weblibs\php\Mail
     */
    public function __construct(
        $hostname = null,
        $username = null,
        $password = null,
        $protocol = null
    ) {
        if (!is_null($hostname)) {
            $this->setHostname($hostname);
        }

        if (!is_null($username)) {
            $this->setUsername($username);
        }

        if (!is_null($password)) {
            $this->setPassword($password);
        }

        if (!is_null($protocol)) {
            $this->setProtocol($protocol);
        }

        $this->MsgBoundary = md5(uniqid(time()));

        return $this;
    }

    /**
     * Set and a validate hostname.
     *
     * @param string $hostname
     * @return \weblibs\php\Mail
     * @throws \Exception
     */
    public function setHostname($hostname = null)
    {
        if (filter_var($hostname, FILTER_VALIDATE_IP) || filter_var(
            $hostname,
            FILTER_VALIDATE_URL
        ) || $hostname == "localhost") {
            $this->HostName = $hostname;
        } else {
            throw new \Exception(__METHOD__ . ': Invalid hostname');
        }

        return $this;
    }

    /**
     * Get property value
     *
     * @return string
     */
    public function getHostname()
    {
        return $this->HostName;
    }

    /**
     * Set and a validate username.
     *
     * @param string $username
     * @return \weblibs\php\Mail
     * @throws \Exception
     */
    public function setUsername($username = null)
    {
        if (filter_var($username, FILTER_VALIDATE_EMAIL)) {
            $this->UserName     = $username;
            $mailData           = explode("@", $this->UserName);
            $this->SenderDomain = $mailData[1];
        } else {
            throw new \Exception(__METHOD__ . ': Invalid username');
        }

        return $this;
    }

    /**
     * Get property value
     *
     * @return string
     */
    public function getUsername()
    {
        return $this->UserName;
    }

    /**
     * Set and a validate password.
     *
     * @param string $password
     * @return \weblibs\php\Mail
     * @throws \Exception
     */
    public function setPassword($password)
    {
        if (filter_var($password, FILTER_SANITIZE_STRING)) {
            $this->Password = filter_var($password, FILTER_SANITIZE_STRING);
        } else {
            throw new \Exception(__METHOD__ . ': Invalid password');
        }

        return $this;
    }

    /**
     * Get property value
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->Password;
    }

    /**
     * Set protocol and port
     *
     * @param string $protocol
     * @return \weblibs\php\Mail
     * @throws \Exception
     */
    public function setProtocol($protocol = null)
    {
        if (filter_var($protocol, FILTER_SANITIZE_STRING)) {
            $this->Protocol = filter_var($protocol, FILTER_SANITIZE_STRING);

            switch ($this->Protocol) {
                case "pop3":
                    $this->Port = 110;
                    break;

                case "pop3s":
                    $this->Port = 995;
                    break;

                case "smtp":
                    $this->Port = 25;
                    break;

                case "smtps":
                case "urd":
                    $this->Port = 465;
                    break;

                case "imap":
                    $this->Port = 143;
                    break;

                case "imaps":
                    $this->Port = 993;
                    break;

                default:
                    throw new \Exception(__METHOD__ . ': Invalid protocol');
            }
        } else {
            throw new \Exception(__METHOD__ . ': Invalid protocol');
        }

        return $this;
    }

    /**
     * Get property value
     *
     * @return string
     */
    public function getProtocol()
    {
        return $this->Protocol;
    }

    /**
     * Send command to open socket and return response.
     *
     * @param string $commandString
     * @return string
     */
    private function sendCommand($commandString, $waitResponse = true)
    {
        if ($this->Connection) {
            fwrite($this->Connection, $commandString . self::LINE_ENDINGS);

            $this->ServerResponse[] = sprintf(
                "[%s] SEND: '%s'",
                date('Y-m-d H:i:s'),
                htmlentities($commandString)
            );

            if ($waitResponse) {
                $Response               = fgets($this->Connection);
                echo "$Response<br />";
                $this->ServerResponse[] = sprintf(
                    "[%s] RESP: '%s'",
                    date('Y-m-d H:i:s'),
                    $Response
                );
            }
        } else {
            throw new \Exception(__METHOD__ . ': Invalid connection, connect to host first.');
        }

        return $this->ServerResponse;
    }

    /**
     * Connect to a server and return response
     *
     * @return string
     * @throws \Exception
     */
    private function connect()
    {
        if (!$this->Connection) {
            if (!is_null($this->HostName) && !is_null($this->Port)) {
                $this->Connection = fsockopen(
                    $this->HostName,
                    $this->Port,
                    $this->ErrorNum,
                    $this->ErrorStr,
                    self::SOCKET_TIMEOUT
                );
                if ($this->Connection) {
                    return $this->ServerResponse[] = fgets($this->Connection);
                } else {
                    throw new \Exception(__METHOD__ . ': Failed connection');
                }
            } else {
                throw new \Exception(__METHOD__ . ': Invalid hostname or port, check and retry!');
            }
        } else {
            throw new \Exception(__METHOD__ . ': Already connected!');
        }
    }

    /**
     * Disconnect from host and unset connection.
     *
     * @throws \Exception
     */
    private function disconnect()
    {
        if ($this->Connection) {
            switch ($this->Protocol) {
                case "smtp":
                case "smtps":
                    $this->sendCommand(self::LINE_ENDINGS . "." . self::LINE_ENDINGS . "QUIT");
                    break;

                default:
                    throw new \Exception(__METHOD__ . ': Unhandled protocol');
            }
            fclose($this->Connection);
            unset($this->Connection);
        } else {
            throw new \Exception(__METHOD__ . ': Already disconnected!');
        }
    }

    /**
     * Execute login and return response
     *
     * @return string
     * @throws \Exception
     */
    private function login()
    {
        if (!is_null($this->Connection)) {
            if (!is_null($this->UserName)) {
                switch ($this->Protocol) {
                    case "smtp":
                    case "smtps":
                        $this->smtpLogin();
                        break;

                    case "imap":
                    case "imaps":
                        $this->imapLogin();
                        break;

                    case "pop":
                    case "pops":
                    case "apop":
                        $this->popLogin();
                        break;

                    default:
                        throw new \Exception(__METHOD__ . ': Unhandled protocol');
                }

                return $this->ServerResponse;
            } else {
                throw new \Exception(__METHOD__ . ': Invalid username.');
            }
        } else {
            throw new \Exception(__METHOD__ . ': Invalid connection, connect to host first.');
        }
    }

    /**
     * Execute smtp login
     *
     * @throws \Exception
     */
    private function smtpLogin()
    {
        switch ($this->Protocol) {
            case "smtp":
                $this->sendCommand("HELO " . $this->SenderDomain);
                break;

            case "smtps":
                $this->sendCommand("EHLO " . $this->SenderDomain);
                break;

            default:
                throw new \Exception(__METHOD__ . ': Unhandled protocol');
        }

        $this->sendCommand("MAIL FROM: <" . $this->UserName . ">");
    }

    /**
     * Execute imap login
     *
     * @throws \Exception
     */
    private function imapLogin()
    {
        if (!is_null($this->Password)) {
            $this->sendCommand("USER " . $this->UserName);
            $this->sendCommand("PASS " . $this->Password);
        } else {
            throw new \Exception(__METHOD__ . ': Invalid password');
        }
    }

    /**
     * Execute pop login
     *
     * @throws \Exception
     */
    private function popLogin()
    {
        
    }

    /**
     * Preparate header for message to send
     *
     * @return \weblibs\php\Mail
     */
    private function prepareHeaders()
    {
        $this->MsgHeaders[] = 'X-Mailer: ' . self::HEADER_MAILER;
        $this->MsgHeaders[] = 'MIME-Version: ' . self::HEADER_MIME_VERSION;
        $this->MsgHeaders[] = 'Content-Type: multipart/mixed; boundary="' . $this->MsgBoundary . '"';
        $this->MsgHeaders[] = 'Content-Transfer-Encoding: ' . self::HEADER_TRANSFER_ENCODING;

        return $this;
    }

    /**
     * Preparate attachments for message to send
     *
     * @return \weblibs\php\Mail
     */
    private function prepareAttachments()
    {
        foreach ($this->Attachments as $attach) {
            $this->MsgAttachments[] = '--' . $this->MsgBoundary;
            $this->MsgAttachments[] = 'Content-Type: ' . $attach['mimeType'] . '; name="' . $attach['fileName'] . '"';
            $this->MsgAttachments[] = 'Content-Transfer-Encoding: base64';
            $this->MsgAttachments[] = 'Content-Disposition: attachment; filename="' . $attach['fileName'] . '"' . self::LINE_ENDINGS;
            $this->MsgAttachments[] = $attach['data'];
            $this->MsgAttachments[] = '--' . $this->MsgBoundary . '--' . self::LINE_ENDINGS;
        }

        return $this;
    }

    /**
     * Return selected property
     *
     * @return array with headers
     */
    public function getHeaders()
    {
        $this->prepareHeaders();

        return $this->MsgHeaders;
    }

    /**
     * Return selected property
     *
     * @return array with header attachments
     */
    public function getAttachments()
    {
        $this->prepareAttachments();

        return $this->MsgAttachments;
    }

    /**
     * Preparate body for message to send
     *
     * @return \weblibs\php\Mail
     */
    private function prepareBody()
    {
        $this->MsgBody[] = self::LINE_ENDINGS . '--' . $this->MsgBoundary;
        $this->MsgBody[] = 'Content-Type: text/plain; charset="iso-8859-1"';
        $this->MsgBody[] = 'Content-Transfer-Encoding: quoted-printable' . self::LINE_ENDINGS;
        $this->MsgBody[] = $this->Body;
        $this->MsgBody[] = '--' . $this->MsgBoundary . '--' . self::LINE_ENDINGS;

        return $this;
    }

    /**
     * Add recipient to specific list
     *
     * @param string $type  - recipient type [to, cc, bcc]
     * @param string $label - recipient label
     * @param string $email - recipient email
     * @return \weblibs\php\Mail
     * @throws \Exception
     */
    public function addAddress($type = null, $label = "", $email = null)
    {
        if (is_null($type)) {
            throw new \Exception(__METHOD__ . ': Invalid recipient type.');
        }

        if (is_null($email)) {
            throw new \Exception(__METHOD__ . ': Invalid recipient email.');
        }

        if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
            switch ($type) {
                case "from":
                    $this->From = ['label' => $label, 'email' => $email];
                    break;

                case "to":
                    $this->To[] = ['label' => $label, 'email' => $email];
                    $this->ToString .= $email . ", ";
                    break;

                case "cc":
                    $this->Cc[] = ['label' => $label, 'email' => $email];
                    $this->CcString .= $email . ", ";
                    break;

                case "bcc":
                case "ccn":
                    $this->Bcc[] = ['label' => $label, 'email' => $email];
                    $this->BccString .= $email . ", ";
                    break;

                default:
                    throw new \Exception(__METHOD__ . ': Invalid recipient type.');
            }
        } else {
            throw new \Exception(__METHOD__ . ': Invalid recipient email "' . $email . '"');
        }

        return $this;
    }

    /**
     * Return selected properties
     *
     * @return array
     */
    public function getFrom()
    {
        return $this->From;
    }

    /**
     * Return an array with all recipients list
     *
     * @return array recipient list
     */
    public function getAddressess()
    {
        foreach ($this->To as $recipient) {
            $recipient['type'] = "to";
            $recipientsList[]  = $recipient;
        }

        foreach ($this->Cc as $recipient) {
            $recipient['type'] = "cc";
            $recipientsList[]  = $recipient;
        }

        foreach ($this->Bcc as $recipient) {
            $recipient['type'] = "bcc";
            $recipientsList[]  = $recipient;
        }

        return $recipientsList;
    }

    /**
     * Add attachment from file path, convert and add to array
     *
     * @param string $filePath absolute path to file
     * @return \weblibs\php\Mail
     * @throws \Exception
     */
    public function addAttachment($filePath = null)
    {
        if (!is_null($filePath) && filter_var($filePath, FILTER_SANITIZE_STRING) && (file_exists(filter_var(
            $filePath,
            FILTER_SANITIZE_STRING
        )))) {
            $filePath = filter_var($filePath, FILTER_SANITIZE_STRING);
            $fInfo    = new \finfo(FILEINFO_MIME);

            $this->Attachments[] = [
                'fileName' => basename($filePath),
                'mimeType' => $fInfo->file($filePath),
                'data'     => chunk_split(base64_encode(file_get_contents($filePath)))
            ];
        } else {
            throw new \Exception(__METHOD__ . ': Invalid filePath for attachment');
        }

        return $this;
    }

    /**
     * Set and a validate $subject.
     *
     * @param string $subject
     * @return \weblibs\php\Mail
     * @throws \Exception
     */
    public function setSubject($subject)
    {
        if (filter_var($subject, FILTER_SANITIZE_STRING)) {
            $this->Subject = filter_var($subject, FILTER_SANITIZE_STRING);
        } else {
            throw new \Exception(__METHOD__ . ': Invalid subject');
        }

        return $this;
    }

    /**
     * Get property value
     *
     * @return string
     */
    public function getSubject()
    {
        return $this->Subject;
    }

    /**
     * Prepare and send email message
     *
     * @throws \Exception
     */
    public function send()
    {
        if (!is_null($this->HostName) && !is_null($this->UserName) && !is_null($this->Password) && !is_null($this->Port)) {
//            $this->prepareHeaders();
//            $this->prepareAttachments();
//            $this->prepareBody();
            $this->connect();
            $this->login();
            $this->sendRecipients();
            $this->sendData();
            $this->sendHeaders();
            $this->sendBody();
            $this->sendAttachments();
            $this->disconnect();
        } else {
            throw new \Exception('Missing parameters, cannot send message.');
        }
    }

    /**
     * TODO: Get count of message on the server
     */
    public function getMessageCount()
    {
        //        $this->TotMsg = explode(" ", $this->ServerResponse);
        //        $this->TotMsg = $this->TotMsg[1];
        //        echo "[Debug]: tot msg: " . $this->TotMsg . " <br />";
    }

    /**
     * Set and validate body
     *
     * @param type $body
     * @return \weblibs\php\Mail
     * @throws \Exception
     */
    public function setBody($body)
    {
        if (filter_var($body, FILTER_SANITIZE_STRING)) {
            $this->Body = filter_var($body, FILTER_SANITIZE_STRING);
        } else {
            throw new \Exception(__METHOD__ . ': Invalid body');
        }

        return $this;
    }

    /**
     * Get property value
     *
     * @return string
     */
    public function getBody()
    {
        return $this->Body;
    }

    /**
     * Send all recipient to server
     *
     * @return \weblibs\php\Mail
     */
    private function sendRecipients()
    {
        foreach ($this->getAddressess() as $recipient) {
            $this->sendCommand("RCPT TO: <" . $recipient['email'] . ">");
        }

        return $this;
    }

    /**
     * Send all data field to server
     *
     * @return \weblibs\php\Mail
     */
    private function sendData()
    {
        $this->sendCommand("DATA");
        $this->sendCommand("From: " . $this->UserName, false);

        if ($this->ToString != "") {
            $this->ToString = ereg_replace("/,\ $/", "", $this->ToString);
            $this->sendCommand("To: " . $this->ToString, false);
        }

        if ($this->CcString != "") {
            $this->CcString = ereg_replace("/,\ $/", "", $this->CcString);
            $this->sendCommand("Cc: " . $this->CcString, false);
        }

        if ($this->BccString != "") {
            $this->BccString = ereg_replace("/,\ $/", "", $this->BccString);
            $this->sendCommand("Bcc: " . $this->BccString, false);
        }

        $this->sendCommand("Received: from " . $this->UserName
                . " by " . $this->SenderDomain . "; " . date('r'), false);
        $this->sendCommand("Date: " . date('r'), false);
        $this->sendCommand("Subject: " . $this->Subject, false);

        return $this;
    }

    /**
     * Send all headers to server
     *
     * @return \weblibs\php\Mail
     */
    private function sendHeaders()
    {
        foreach ($this->getHeaders() as $header) {
            $this->sendCommand($header);
        }

        return $this;
    }

    /**
     * Send body to server
     *
     * @return \weblibs\php\Mail
     */
    private function sendBody()
    {
        $this->sendCommand($this->Body);

        return $this;
    }

    /**
     * Send all headers to server
     *
     * @return \weblibs\php\Mail
     */
    private function sendAttachments()
    {
        foreach ($this->getAttachments() as $attachRow) {
            $this->sendCommand($attachRow);
        }

        return $this;
    }

    /**
     * TODO
     */
    public function getMessageList()
    {

        /* $this->MsgList = array();

          fwrite($this->Connection, "LIST\r\n");
          $this->server_response = fgets($this->Connection);
          echo "[Debug]: LIST resp: " . $this->server_response . "<br />"; */

        //for ($i = 1; $i <= $this->TotMsg; $i++)
        for ($i = 1; $i <= 1; $i++) {
            fwrite($this->Connection, "TOP $i 0" . self::LINE_ENDINGS);
            $this->ServerResponse[] = fgets(
                $this->Connection,
                self::NETWORK_BUFFER
            );
            echo "[Debug]: TOP $i resp: " . $this->ServerResponse . "<br />";
        }


        /*
          //while (!feof($this->Connection))
          $i = 1;
          while ($i <= 10)
          {
          array_push($this->MsgList, $this->server_response);
          $i++;
          }
         */

        /*
          echo "<pre> MsgList ";
          print_r($this->MsgList);
          echo "</pre>";
         */
    }

    /**
     * Placeholder
     */
    public function read()
    {
        
    }
}

<?php

namespace clagiordano\weblibs\php;

/**
 * Description of Cart
 *
 * @author Claudio Giordano <claudio.giordano@autistici.org>
 */
class Cart
{
    /**
     * Informazioni sulla classe
     */
    const VERSIONE                  = '1.0';
    const DATA_CREAZIONE            = '2014-04-14';
    const DATA_ULTIMO_AGGIORNAMENTO = '2014-04-14';
    const AUTORE                    = 'Claudio Giordano';
    const MAIL_AUTORE               = 'claudio.giordano@autistici.org';

    /**
     * Costruttore
     */
    public function __construct()
    {

    }

    /**
     * Metodo che azzera l'array dei prodotti
     */
    public function emptyProducts()
    {

    }

    /**
     * Metodo che restituisce il numero totale dei prodotti presenti in carrello
     */
    public function getCount()
    {

    }

    /**
     * Metodo che restituisce il costo totale dei prodotti presenti in carrello
     */
    public function getTotal()
    {

    }

    /**
     * Metodo che aggiunge un prodotto al carrello
     */
    public function addProduct()
    {

    }

    /**
     * Metodo che rimuove un prodotto dal carrello
     */
    public function remProduct()
    {

    }
}

<?php

namespace clagiordano\weblibs\php;

use clagiordano\weblibs\php\Authentication;
use clagiordano\weblibs\php\BrowserInfo;

/**
 * User object class
 *
 * @version 1.2
 * @author Claudio Giordano <claudio.giordano@autistici.org>
 * @since 2014-04-03
 * @category Security
 */
class User
{
    private $userId;
    private $Username;
    private $Password;
    private $GroupID;
    //private $Group = new Group(); // TODO
    private $Enabled;
    private $StatusID;
    private $CityID;
    private $Description;
    private $SessionID;
    public $BrowserInfo; // new BrowserInfo()

    /**
     * Definizione tabella UTENTI e relativi campi
     *
     * CREATE TABLE `tab_utenti` (
      `id_user` int(11) NOT NULL AUTO_INCREMENT,
      `user` varchar(30) NOT NULL,
      `pass` char(32) NOT NULL DEFAULT '',
      `tipo` int(11) NOT NULL,
      `stato` int(1) NOT NULL DEFAULT '1',
      `id_stato` int(11) NOT NULL,
      `id_city` int(11) NOT NULL DEFAULT '1',
      `desc_user` varchar(150) NOT NULL,
      PRIMARY KEY (`id_user`)
      ) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=latin1 COMMENT='latin1_swedish_ci'
     */

    public function __construct(
        $Username,
        $Password,
        $GroupID = 1,
        $StatusID = 1,
        $Description = "",
        $Enabled = false,
        $CityID = 1
    ) {
        $this->setUserId(1);                    // ID 1 (admin) in fase di creazione
        $this->setUsername($Username);
        $this->setPassword($Password);
        $this->setGroupID($GroupID);
        $this->setStatusID($StatusID);
        $this->setDescription($Description);
        $this->setEnabled($Enabled);
        $this->setCityID($CityID);              // default 1

        if (session_id()) {
            // imposta l'id di sessione se presente
            $this->setSessionID(session_id());
        }

        // crea l'oggetto e lo associa all'utente
        $this->BrowserInfo = new BrowserInfo();
    }

    /**
     * Stampa tutte le informazioni dell'oggetto
     */
    public function printR()
    {
        echo "<pre>";
        echo "     UserID: " . $this->userId . "\n";
        echo "   Username: " . $this->Username . "\n";
        echo "   Password: " . $this->Password . "\n";
        echo "    GroupID: " . $this->GroupID . "\n";
        echo "    Enabled: " . $this->Enabled . "\n";
        echo "   StatusID: " . $this->StatusID . "\n";
        echo "     CityID: " . $this->CityID . "\n";
        echo "Description: " . $this->Description . "\n";
        echo "  SessionID: " . $this->SessionID . "\n";
        echo "</pre>";

        $this->BrowserInfo->PrintR();
    }

    /**
     * Imposta il campo dell'oggetto
     *
     * @param type $userId
     */
    public function setUserId($userId)
    {
        // validazione campo
        if (filter_var($userId, FILTER_VALIDATE_INT) && (strlen($userId) <= 11)) {
            $this->userId = $userId;
        } else {
            throw new \Exception("Il campo 'UserID' deve essere un numero non pi&ugrave; lunga di 11 caratteri");
        }
    }

    /**
     * Restituisce il campo dell'oggetto
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Imposta il campo dell'oggetto
     *
     * @param type $Username
     */
    public function setUsername($Username)
    {
        /**
         * Pulitura campo user:
         */
        $Username = htmlspecialchars($Username, ENT_QUOTES, 'UTF-8');

        /**
         * Validazione campo
         */
        if (is_string($Username) && (strlen($Username) <= 30) && (strlen(trim($Username)) > 0)) {
            $this->Username = $Username;
        } else {
            throw new \Exception("Il campo 'Username' deve essere una stringa non pi&ugrave; lunga di 30 caratteri");
        }
    }

    /**
     * Restituisce il campo dell'oggetto
     */
    public function getUsername()
    {
        return $this->Username;
    }

    /**
     * Imposta il campo dell'oggetto
     *
     * @param type $Password
     */
    public function setPassword($Password)
    {
        if (is_string($Password) && (strlen($Password) <= 60) && (strlen(trim($Password)) > 0)) {
            $this->Password = $Password;
        } else {
            throw new \Exception("Il campo 'Password' deve essere una stringa non pi&ugrave; lunga di 60 caratteri");
        }
    }

    /**
     * Restituisce il campo dell'oggetto
     */
    public function getPassword()
    {
        return $this->Password;
    }

    /**
     * Imposta il campo dell'oggetto
     *
     * @param type $GroupID
     */
    public function setGroupID($GroupID)
    {
        if (filter_var($GroupID, FILTER_VALIDATE_INT) && (strlen($GroupID) <= 11) && (strlen(trim($GroupID)) > 0)) {
            $this->GroupID = $GroupID;
        } else {
            throw new \Exception("Il campo 'GroupID' deve essere una numero non pi&ugrave; lungo di 11 caratteri");
        }
    }

    /**
     * Restituisce il campo dell'oggetto
     */
    public function getGroupID()
    {
        return $this->GroupID;
    }

    /**
     * Imposta il campo dell'oggetto
     *
     * Il campo è booleano, 1 per FALSE, 0 per TRUE
     *
     * @param type $value
     */
    public function setEnabled($value)
    {
        $this->Enabled = (boolean)$value;
    }

    /**
     * Restituisce il campo dell'oggetto
     */
    public function getEnabled()
    {
        return (boolean)$this->Enabled;
    }

    /**
     * Imposta il campo dell'oggetto
     *
     * @param type $StatusID
     */
    public function setStatusID($StatusID)
    {
        if (filter_var($StatusID, FILTER_VALIDATE_INT) && (strlen($StatusID) <= 11)) {
            $this->StatusID = $StatusID;
        } else {
            throw new \Exception("Il campo 'StatusID' deve essere una numero non pi&ugrave; lungo di 11 caratteri.");
        }
    }

    /**
     * Restituisce il campo dell'oggetto
     */
    public function getStatusID()
    {
        return $this->StatusID;
    }

    /**
     * Imposta il campo dell'oggetto
     *
     * @param type $CityID
     */
    public function setCityID($CityID)
    {
        if (filter_var($CityID, FILTER_VALIDATE_INT) && (strlen($CityID) <= 11)) {
            $this->CityID = $CityID;
        } else {
            throw new \Exception("Il campo 'CityID' deve essere una numero non pi&ugrave; lungo di 11 caratteri.");
        }
    }

    /**
     * Restituisce il campo dell'oggetto
     */
    public function getCityID()
    {
        return $this->CityID;
    }

    /**
     * Imposta il campo dell'oggetto
     *
     * @param type $Description
     */
    public function setDescription($Description)
    {
        if (is_string($Description) && (strlen($Description) <= 150)) {
            $this->Description = $Description;
        } else {
            throw new \Exception("Il campo 'Description' deve essere una stringa non pi&ugrave; lunga di 150 caratteri.");
        }
    }

    /**
     * Restituisce il campo dell'oggetto
     */
    public function getDescription()
    {
        return $this->Description;
    }

    /**
     * Imposta il campo dell'oggetto
     *
     * @param type $SessionID
     */
    public function setSessionID($SessionID)
    {
        if (is_string($SessionID) && (strlen($SessionID) <= 32)) {
            $this->SessionID = $SessionID;
        } else {
            throw new \Exception("Il campo 'SessionID' deve essere una stringa non pi&ugrave; lunga di 32 caratteri.");
        }
    }

    /**
     * Restituisce il campo dell'oggetto
     */
    public function getSessionID()
    {
        return $this->SessionID;
    }

    public function setAll(array $Values)
    {
        foreach ($Values as $key => $value) {
            switch ($key) {
                case AuthenticationConfig::TABLE_USERS_ID:
                    $this->setUserId((int)$value);
                    break;

                case AuthenticationConfig::TABLE_USERS_USER:
                    $this->setUsername($value);
                    break;

                case AuthenticationConfig::TABLE_USERS_PASSWORD:
                    $this->setPassword($value);
                    break;

                case AuthenticationConfig::TABLE_USERS_GROUP_ID:
                    $this->setGroupID($value);
                    break;

                case AuthenticationConfig::TABLE_USERS_ENABLED:
                    $this->setEnabled($value);
                    break;

                case AuthenticationConfig::TABLE_USERS_STATUS_ID:
                    $this->setStatusID($value);
                    break;

                case AuthenticationConfig::TABLE_USERS_CITY_ID:
                    $this->setCityID($value);
                    break;

                case AuthenticationConfig::TABLE_USERS_DESCRIPTION:
                    $this->setDescription($value);
                    break;
            }
        }
    }
}

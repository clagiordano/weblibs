<?php

namespace clagiordano\weblibs\php;

use PDO;

/**
 * Simple class for multi level log operations,
 * inspired from:
 *
 * https://github.com/php-fig/log
 *
 * @version 1.0.2
 * @author Claudio Giordano <claudio.giordano@autistici.org>
 * @since 2014-04-05
 * @category Information
 */
class Logger
{
    const EMERGENCY = 'emergency';
    const ALERT     = 'alert';
    const CRITICAL  = 'critical';
    const ERROR     = 'error';
    const WARNING   = 'warning';
    const NOTICE    = 'notice';
    const INFO      = 'info';
    const DEBUG     = 'debug';

    /**
     * Definizione tabella LOG e relativi campi
     *
     * CREATE TABLE `tab_session` (
      `id_session` int(11) NOT NULL AUTO_INCREMENT,
      `session_id` char(32) NOT NULL,
      `id_user` tinyint(4) NOT NULL,
      `data` datetime NOT NULL,
      `ip` varchar(20) NOT NULL,
      `action` text NOT NULL,
      `platform` varchar(20) NOT NULL,
      `browser` varchar(20) NOT NULL,
      `version` varchar(5) NOT NULL,
      `arch` char(2) NOT NULL,
      `level` varchar(20) NOT NULL,
      PRIMARY KEY (`id_session`)
      ) ENGINE=MyISAM DEFAULT CHARSET=latin1
     */
    const TABLE_LOG              = "tab_session";
    const TABLE_LOG_ID           = "id_session";
    const TABLE_LOG_LEVEL        = "level";
    const TABLE_LOG_SESSION_ID   = "session_id";
    const TABLE_LOG_USER_ID      = "id_user";
    //const TABLE_LOG_DATE            = "data"; convertito in timestamp sul db e quindi automatico
    const TABLE_LOG_IP           = "ip";
    const TABLE_LOG_ACTION       = "action";
    const TABLE_LOG_PLATFORM     = "platform";
    const TABLE_LOG_BROWSER      = "browser";
    const TABLE_LOG_VERSION      = "version";
    const TABLE_LOG_ARCHITECTURE = "arch";

    /**
     * @param PDO $Database
     * @param User $User
     */
    public function __construct(PDO $Database, User $User = null)
    {
        $this->Database = $Database;
        if ($User == null) {
            $this->User = new User("unknown", "wrong");
        } else {
            $this->User = $User;
        }
    }

    /**
     * System is unusable.
     *
     * @param string $message
     * @param array $context
     * @return null
     */
    public function emergency($message, array $context = [])
    {
        $this->interpolateString($message, $context);
        $this->saveLog(self::EMERGENCY, $message);
    }

    /**
     * Sostituisce i valori dell'array context all'interno del messaggio.
     * ( come un printf )
     * @param $message
     * @param array $context
     */
    private function interpolateString(&$message, array $context = [])
    {
        $replace = [];
        foreach ($context as $key => $val) {
            $replace['{' . $key . '}'] = $val;
        }

        $temp_message = strtr($message, $replace);
        $message = addslashes(stripslashes($temp_message));
    }

    /**
     * Funzione di salvataggio log su database.
     * TODO migrare nomi dei campi
     * TODO migrare accesso ai dati del browser nel nuovo formato
     *
     * @param string $level Log level
     * @param string $message Message string to save
     */
    private function saveLog($level, $message)
    {
        /**
         * Preparo la query:
         */
        $Query = "INSERT INTO " . self::TABLE_LOG . " (" . self::TABLE_LOG_LEVEL . ", ";
        $Query .= self::TABLE_LOG_SESSION_ID . ", " . self::TABLE_LOG_USER_ID . ", ";
        $Query .= self::TABLE_LOG_ACTION . ", " . self::TABLE_LOG_IP . ", ";
        $Query .= self::TABLE_LOG_PLATFORM . ", " . self::TABLE_LOG_BROWSER . ", ";
        $Query .= self::TABLE_LOG_VERSION . ", " . self::TABLE_LOG_ARCHITECTURE . ") VALUES (";

        $Query .= "'" . $level . "', '" . $this->User->getSessionID() . "', ";
        $Query .= "'" . $this->User->getUserId() . "', '" . $message . "', ";
        $Query .= "'" . $this->User->BrowserInfo->RemoteAddress . "', ";
        $Query .= "'" . $this->User->BrowserInfo->Platform . "', ";
        $Query .= "'" . $this->User->BrowserInfo->Browser . "', ";
        $Query .= "'" . $this->User->BrowserInfo->Version . "', ";
        $Query .= "'" . $this->User->BrowserInfo->Architecture . "')";

        // Valida ed aggiunge eventuali escape per l'esecuzione della query
        $cursor = $this->Database->prepare($Query);
        try {
            $cursor->execute();
        } catch (\PDOException $ex) {
            echo "Errore sulla query! <br />" . $ex->getMessage();
            exit();
        }
    }

    /**
     * Action must be taken immediately.
     *
     * Example: Entire website down, database unavailable, etc. This should
     * trigger the SMS alerts and wake you up.
     *
     * @param string $message
     * @param array $context
     * @return null
     */
    public function alert($message, array $context = [])
    {
        $this->interpolateString($message, $context);
        $this->saveLog(self::ALERT, $message);
    }

    /**
     * Critical conditions.
     *
     * Example: Application component unavailable, unexpected exception.
     *
     * @param string $message
     * @param array $context
     * @return null
     */
    public function critical($message, array $context = [])
    {
        $this->interpolateString($message, $context);
        $this->saveLog(self::CRITICAL, $message);
    }

    /**
     * Runtime errors that do not require immediate action but should typically
     * be logged and monitored.
     *
     * @param string $message
     * @param array $context
     * @return null
     */
    public function error($message, array $context = [])
    {
        $this->interpolateString($message, $context);
        $this->saveLog(self::ERROR, $message);
    }

    /**
     * Exceptional occurrences that are not errors.
     *
     * Example: Use of deprecated APIs, poor use of an API, undesirable things
     * that are not necessarily wrong.
     *
     * @param string $message
     * @param array $context
     * @return null
     */
    public function warning($message, array $context = [])
    {
        $this->interpolateString($message, $context);
        $this->saveLog(self::WARNING, $message);
    }

    /**
     * Normal but significant events.
     *
     * @param string $message
     * @param array $context
     * @return null
     */
    public function notice($message, array $context = [])
    {
        $this->interpolateString($message, $context);
        $this->saveLog(self::NOTICE, $message);
    }

    /**
     * Interesting events.
     *
     * Example: User logs in, SQL logs.
     *
     * @param string $message
     * @param array $context
     * @return null
     */
    public function info($message, array $context = [])
    {
        $this->interpolateString($message, $context);
        $this->saveLog(self::INFO, $message);
    }

    /**
     * Detailed debug information.
     *
     * @param string $message
     * @param array $context
     * @return null
     */
    public function debug($message, array $context = [])
    {
        $this->interpolateString($message, $context);
        $this->saveLog(self::DEBUG, $message);
    }

    /**
     * Logs with an arbitrary level.
     *
     * @param mixed $level
     * @param string $message
     * @param array $context
     * @return null
     */
    public function log($level, $message, array $context = [])
    {
        $this->interpolateString($message, $context);
        $this->saveLog($level, $message);
    }

    /**
     * Funzione di stampa messaggi di errore ed invio automatico mail
     * con segnalazione di bug
     *
     * TODO: Reimplementare i vari componenti.
     *
     * @param string $Command
     * @param string $Note
     */
//      public function printError($Command, $Note = "-")
//      {
    // includo il modulo di rete per l'invio della mail:
    // TODO require_once("Net.php");
//          $ln = "%0A";
//          $tab = "%20%20%20%20";
//          $this->ErrorMsgStyle = "padding: 10px; font-size: 9pt; font-weight: bold;"
//                                  . "color: red; border: 2px dashed red;"
//                                  . "width: 40%; height: auto;"
//                                  . "top: 10%; left: 10%; position: absolute;";
    //~ $this->ErrorMail = "Si e' verificato un errore sulla query:"
    //~ . "$ln $ln L'errore riporta: $ln $tab (" . mysql_errno() . ") " . mysql_error()
    //~ . "$ln $ln L'errore e' stato generato da: $ln $tab $Command"
    //~ . "$ln $ln Nota relativa all'errore: $ln $tab $Note"
    //~ . "$ln $ln Informazioni Aggiuntive: $ln"
    //~ . "$tab Get Request: " . str_replace("&", "%26", http_build_query(filter_input_array(INPUT_GET))) . "$ln"
    //~ . "$tab Post Request: " . str_replace("&", "%26", http_build_query(filter_input_array(INPUT_POST))) . "$ln";
//          $this->ErrorMail = "Si e' verificato un errore sulla query:"
//              . "\n\n L'errore riporta: \n\t (" . mysql_errno() . ") " . mysql_error()
//              . "\n\n L'errore e' stato generato da: \n\t $Command"
//              . "\n\n Nota relativa all'errore: \n\t $Note"
//              . "\n\n Informazioni Aggiuntive: \n"
//              . "\t Get Request: " . str_replace("&", "%26", http_build_query(filter_input_array(INPUT_GET))) . "\n"
//              . "\t Post Request: " . str_replace("&", "%26",
//                      http_build_query(filter_input_array(INPUT_POST))) . "\n";
//
//          $this->ErrorBox = '<div style="' . $this->ErrorMsgStyle . '">'
//              . "Si è verificato un errore sulla query:<br /><br />"
//
//              . "L'errore riporta: <br />"
//              . "&nbsp;&nbsp;&nbsp;&nbsp; (" . mysql_errno() . ") " . mysql_error() . "<br /><br />"
//
//              . "L'errore e' stato generato da: <br />"
//              . "&nbsp;&nbsp;&nbsp;&nbsp;$Command <br /><br />"
//
//              . "Nota relativa all'errore: <br />"
//              . "&nbsp;&nbsp;&nbsp;&nbsp;$Note <br /><br />"
//
//              . "Informazioni Aggiuntive: <br />"
//              . "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Get Request: " . http_build_query($_GET) . "<br /><br />"
//              . "&nbsp;&nbsp;&nbsp;&nbsp;Post Request: " . http_build_query($_POST) . "<br /><br />"

    /* . "<a href=\"mailto:" . DeveloperMailAddress . "?Subject=[BugReport]"
      . "&body=" . $this->ErrorMail
      . '" title="invia segnalazione bug">Invia la segnalazione dell\'errore.</a>' */
    // TODO
//          $MailData = array('from' => "[DataBase] <claudio.giordano@autistici.org>",
//                                                  'to' => 'Claudio Giordano <claudio.giordano@autistici.org>',
//                                       'subject' => "[DataBase][BugReport] $Command",
//                                              'body' => $this->ErrorMail);
    // TODO DirectSendMail2($MailData);
//          echo $this->ErrorBox;
//      }
}

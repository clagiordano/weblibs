<?php

namespace clagiordano\weblibs\php;

/**
 * Class to manage date and time string, format conversion and timing.
 *
 * @version  1.2
 * @author   Claudio Giordano <claudio.giordano@autistici.org>
 * @since    2010-04-03
 * @category DateTime
 */
class DateTime
{
    // Regexp constants for date/time formats:
    const REG_DATA_MYSQL = '([1-2][0-9][0-9][0-9]-[0-1][0-9]-[0-3][0-9])';
    const REG_DATA_ITA   = '([0-3][0-9]/[0-1][0-9]/[1-2][0-9][0-9][0-9])';
    const REG_DATA_TIME  = '([1-2][0-9][0-9][0-9]-[0-1][0-9]-[0-3][0-9]\ [0-2][0-9]:[0-6][0-9]:[0-6][0-9])';

    // const REG_TIME       = '([0-2][0-9]:[0-6][0-9]:[0-6][0-9])';

    private $timeStart  = 0;
    private $stringDate = "";

    /**
     * Make DateTime object with string $Date as params.
     *
     * @constructor
     * @param  string $Date
     * @return \weblibs\php\DateTime
     */
    public function __construct($Date = null)
    {
        if (isset($Date)) {
            $this->setDate($Date);
        }

        return $this;
    }

    /**
     * Set internal property
     * @param  string $Date
     * @return \weblibs\php\DateTime
     */
    public function setDate($Date)
    {
        if ((strlen($Date) < 8) || (strlen($Date) > 20)) {
            exit("Invalid date string format!");
        } else {
            // assegna l'argomento del cotruttore per tutte le elaborazioni.
            $this->stringDate = $Date;
        }

        return $this;
    }

    /**
     * Convert date format based on args, example from YYYY-MM-DD to DD-MM-YYYY
     * or reverse operation, this method can detect date format autonomously through
     * the use of regexp then the arguments are optional.
     *
     * @param  string $Separator
     * @param  string $Explode
     * @return string $retVal
     */
    public function invert($Separator = "/", $Explode = "-")
    {
        $retVal = "";

        // In this case the date is invalid then return a "-" string
        if (($this->stringDate == "0000-00-00") || ($this->stringDate == "00/00/0000")) {
            $retVal = "-";
        }

        // Try to detect format with regexp
        if (preg_match(self::REG_DATA_TIME, $this->stringDate)) {
            $retVal = $this->invertDateTime("-", "/");
        } elseif (preg_match(self::REG_DATA_ITA, $this->stringDate)) {
            $retVal = $this->invertDate("-", "/");
        } elseif (preg_match(self::REG_DATA_MYSQL, $this->stringDate)) {
            $retVal = $this->invertDate("/", "-");
        } else {
            // the regexp doesn't match, use the argument if provided
            if ((trim($Separator) != "") && (trim($Explode) != "")) {
                $retVal = $this->invertDate($Separator, $Explode);
            }
        }

        return $retVal;
    }

    /**
     * Converts a string to timestamp format
     *
     * @param  string $Separator
     * @param  string $Explode
     * @return string
     */
    private function invertDateTime($Separator, $Explode)
    {
        // Separa la stringa in 2 sottostringhe contenente date ed ora
        $DateTimeArray = explode(" ", $this->stringDate);

        // converto la date mediante la funzione classica:
        // riassegno la proprietà della classe per automatizzare la conversione
        // della parte interessata della date
        $this->stringDate = $DateTimeArray[0];
        $NewString        = $this->invert($Separator, $Explode);

        // alla nuova stringa concateno l'orario:
        $NewString .= " " . $DateTimeArray[1];

        return $NewString;
    }

    /**
     * Execute the real conversion and return a result string.
     *
     * @param  string $Separator
     * @param  string $Explode
     * @return string $NewString
     */
    private function invertDate($Separator, $Explode)
    {
        $NewString = "";

        // Split string through argument
        $DateArray = explode($Explode, $this->stringDate);

        // Passes through the array to reconstruct the string
        for ($IndexDate = count($DateArray) - 1; $IndexDate >= 0; $IndexDate--) {
            $NewString .= $DateArray[$IndexDate];
            if ($IndexDate != 0) {
                $NewString .= $Separator;
            }
        }

        return $NewString;
    }

    /**
     * Set time start as a chronometer
     *
     * @return \weblibs\php\DateTime
     */
    public function timerStart()
    {
        $this->timeStart = microtime(true);

        return $this;
    }

    /**
     * Stop time, calculate time diff and return, diff or message with diff.
     *
     * @param bool|string $Message
     * @return string
     */
    public function timerStop($Message = false)
    {
        $Diff            = (microtime(true) - $this->timeStart);
        $this->timeStart = 0;

        if ($Message === false) {
            $retVal = $Diff;
        } else {
            $retVal = sprintf("%s %fs", $Message, $Diff);
        }

        return $retVal;
    }
}

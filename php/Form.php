<?php

namespace clagiordano\weblibs\php;

use clagiordano\weblibs\php\FormControl;

/**
 * Form builder class
 *
 * @version  2.0
 * @author   Claudio Giordano <claudio.giordano@autistici.org>
 * @since    2014-04-04
 * @category Layout
 * @package  weblibs
 */
class Form
{
    private $MarkupHtml   = null;
    private $ListControls = [];
    private $Css          = null;
    private $Method       = null;
    private $Action       = null;
    private $Name         = null;

    /**
     *
     */
    public function __construct($Method = "post", $Action = null, $Name = null)
    {
        if (!is_null($Method)) {
            $this->setMethod($Method);
        }

        if (!is_null($Action)) {
            $this->setAction($Action);
        }

        if (!is_null($Name)) {
            $this->setName($Name);
        }

        return $this;
    }

    function getMethod()
    {
        return $this->Method;
    }

    function getAction()
    {
        return $this->Action;
    }

    function getName()
    {
        return $this->Name;
    }

    function setMethod($Method)
    {
        $this->Method = filter_var($Method, FILTER_SANITIZE_STRING);
        return $this;
    }

    function setAction($Action)
    {
        $this->Action = filter_var($Action, FILTER_SANITIZE_STRING);
        return $this;
    }

    function setName($Name)
    {
        $this->Name = filter_var($Name, FILTER_SANITIZE_STRING);

        return $this;
    }

    // text
    public function addText(array $params = [])
    {
        $params['Type']       = "text";
        $Control              = new FormControl($params);
        $this->ListControls[] = $Control;

        return $Control;
    }

    // checkbox + radio
    public function addCheckbox()
    {
        
    }

    // select + selectall
    public function addSelect()
    {
        
    }

    // text area
    public function addTextArea()
    {
        
    }

    // hidden
    public function addHidden()
    {
        
    }

    // button + submit
    public function addButton()
    {
        
    }

    /**
     *
     * @param boolean $returnmarkupHtml
     * @return string/null
     */
    public function show($returnmarkupHtml = false)
    {
        $this->render();

        if ($returnmarkupHtml) {
            return $this->MarkupHtml;
        } else {
            echo $this->MarkupHtml;
        }
    }

    private function render()
    {
        if (isset($this->Name)) {
            $this->MarkupHtml .= '<form name= "' . $this->Name . '" class="form">';
        } else {
            $this->MarkupHtml .= '<form class="form">';
        }

        $this->MarkupHtml .= "<div>";

        foreach ($this->ListControls as $Control) {
            $this->MarkupHtml .= $Control->render();
        }

        $this->MarkupHtml .= "</div>";
        $this->MarkupHtml .= "</form>";

        return $this;
    }

    public function showEditBar(
        $DescString,
        $Mod,
        $Links,
        $Action,
        $Span = 0,
        $CountDett = 0,
        $PopupLink = "#",
        $ArbLinks = "",
        $Indietro = ""
    ) {
        $ConfSalva = "Salvare i dati inseriti?";

        // Abolito con il passaggio alla struttura a div
        //~ if ($Span > 1)
        //~ {
        //~ $ColSpan = "colspan=\"" . $Span . "\"";
        //~ }else{
        //~ $ColSpan = "";
        //~ }

        echo "<div id=\"edit-bar\">";
        //~ echo "<label class=\"title\">$DescString</label>&nbsp;";
        echo "$DescString&nbsp;";

        if (trim($Mod) != "") {
            echo "<label class=\"err_status\">(In Modifica)&nbsp;</label>";
            $UrlSalva  = "?act=$Action&amp;tomod=" . $Mod . "&amp;salva=modifica";
            $UrlAction = "?act=$Action&amp;tomod=" . $Mod;
        } else {
            echo "<label class=\"err_status\">(Nuovo Inserimento)&nbsp;</label>";
            $UrlSalva  = "?act=$Action&amp;salva=salva";
            $UrlAction = "?act=$Action";
        }

        $ArraiLinks = explode(",", $Links);
        foreach ($ArraiLinks as $key => $link) {
            switch ($link) {
                case "new":
                    echo "&nbsp;<a onclick=\"Javascript: "
                    . "go_conf2('I dati non salvati verranno persi, continuare?', "
                    . "'?act=" . $Action . "&amp;reset=0'); void(0);\">"
                    . "<img src=\"" . IMG_PATH . "Links/new.png\" alt=\"new\" title=\"Nuovo\" />"
                    . "Nuovo"
                    . "</a>";
                    break;

                case "oldsave":
                    echo "&nbsp;<a onclick=\"Javascript: conferma_salva('0', '"
                    . $ConfSalva . "', '" . $UrlSalva . "');\" >"
                    . "<img src=\"" . IMG_PATH . "Links/save.png\" alt=\"save\" title=\"Salva\" />"
                    . "Salva"
                    . "</a>";
                    break;

                case "save":
                    echo "&nbsp;<a onclick=\"Javascript: VerificaSalvaPost('0', '"
                    . $ConfSalva . "', '" . $UrlSalva . "');\" >"
                    . "<img src=\"" . IMG_PATH . "Links/save.png\" alt=\"save\" title=\"Salva\" />"
                    . "Salva"
                    . "</a>";
                    break;

                case "add":
                    echo "&nbsp;<a onclick=\"Javascript: popUpWindow('" . $Action . "', '$PopupLink";
                    if (trim($Mod) != "") {
                        echo "?tomod=$Mod";
                    }

                    echo "', '200px', '200px', '750px', '300px');\">"
                    . "<img src=\"" . IMG_PATH . "Links/add.png\" alt=\"add\" title=\"Aggiungi\" />"
                    . "Aggiungi";
                    echo "</a>";
                    break;

                case "clear":
                    echo "&nbsp;<a onclick=\"Javascript: go_conf2('Rimuovere tutte le voci dal dettaglio?', "
                    . "'?act=" . $Action . "&amp;svuota=0'); void(0);\">"
                    . "<img src=\"" . IMG_PATH . "Links/clear.png\" alt=\"clear\" />"
                    . "Voci ( <label class=\"err\" title=\"Totale voci dettaglio\">"
                    . $CountDett . "</label> )"
                    . "</a>";
                    break;

                case "blank":
                    echo "&nbsp;<a onclick=\"Javascript: go('" . $UrlAction . "&amp;blank=0');\" >"
                    . "<img src=\"" . IMG_PATH . "Links/new.png\" alt=\"new\" title=\"Nuovo\" />"
                    . "Nuovo"
                    . "</a>&nbsp;";
                    break;

                case "del":
                    break;
            }
        }

        if ($ArbLinks != "") {
            foreach ($ArbLinks as $key => $field) {
                echo $field['link'];
            }
        }

        echo "</div>";


        if (isset($this->SaveErrors) && ($this->SaveErrors != "")) {
            ?>
            <div style="border: 2px solid rgb(255, 0, 0); padding: 5px;" class="err">
                <img src="<?php echo IMG_PATH; ?>Links/cancel.png"
                     style="width: 14px; height: 14px; vertical-align: middle;"
                     alt="Errore" title="Errore" />
                        <?php echo $this->SaveErrors; ?>
            </div>
            <?php
        }


        if (isset($this->ResultStatus) && ($this->ResultStatus != "")) {
            ?>
            <div style="border: 2px solid rgb(0, 128, 0); padding: 5px;" class="ok">
                <img src="<?php echo IMG_PATH; ?>Links/apply.png"
                     style="width: 14px; height: 14px; vertical-align: middle;"
                     alt="OK" title="OK" />
                        <?php echo $this->ResultStatus; ?>
            </div>
            <?php
        }
    }
}

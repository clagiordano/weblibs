<?php

namespace clagiordano\weblibs\php;

/**
 * @version  1.3
 * @author   Claudio Giordano <claudio.giordano@autistici.org>
 * @since    2014-04-09
 * @category Layout
 * @package  weblibs
 */
class Layout
{

    public function __construct()
    {
        // TODO
    }

    /**
      TODO aggiungere funzione per il redirect anziche' nel login logout
      in modo da delegare alla pagina interessata l'operazione ed il check

      $Redirect = "home.php?act=welc"

      if ($Redirect != "") { ?>
      <label class="ok">Login effettuato, redirect in corso...</label>
      <script type="text/javascript">
      document.location.href = "<?php echo $Redirect; ?>";
      </script>
      <?php } ?>
      ?>

      <label class="ok">LogOut effettuato, redirect in corso...</label>

      <script type="text/javascript">
      document.location.href = '<?php echo $Redirect; ?>';
      </script>
      <?php
     */
    public function showWaitDiv()
    {
        ?>
        <div id="wait">
            <img src="<?php echo IMG_PATH; ?>Varie/wait.gif" alt="wait" />
            <br />
            <label>Aggiornamento pagina in corso, attendere...</label>
        </div>
        <?php
    }

    public function showCreditsBar($ShowTime = false)
    {
        global $start;
        ?>

        <a onclick="Javascript: window.open('http://gnu.it', '_blank'); void(0);">
            <img src="<?php echo IMG_PATH; ?>Software/gnubanner-2.png"
                 alt="GNU Project" title="Progetto GNU" />
        </a>

        <a onclick="Javascript: window.open('http://it.wikipedia.org/wiki/Linux', '_blank'); void(0);">
            <img src="<?php echo IMG_PATH; ?>Software/linux.gif"
                 alt="Linux" title="GNU/Linux" />
        </a>

        <a onclick="Javascript: window.open('http://www.debian.org', '_blank'); void(0);">
            <img src="<?php echo IMG_PATH; ?>Software/debian.png" alt="debian.org"
                 title="Debian - Debian è un sistema operativo (OS) libero (free) per il tuo computer." />
        </a>
        <a onclick="Javascript: window.open('http://www.apache.org', '_blank'); void(0);">
            <img src="<?php echo IMG_PATH; ?>Software/apache.png" alt="apache.org" title="Apache" />
        </a>
        <a onclick="Javascript: window.open('http://www-it.mysql.com/', '_blank'); void(0);">
            <img src="<?php echo IMG_PATH; ?>Software/mysql.png" alt="mysql.com" title="MySql" />
        </a>
        <a onclick="Javascript: window.open('http://it.php.net/', '_blank'); void(0);">
            <img src="<?php echo IMG_PATH; ?>Software/php.png" alt="php.net" title="Php" />
        </a>
        <a onclick="Javascript: window.open('http://www.mozillaitalia.it/archive/index.html#p1', '_blank'); void(0);">
            <img src="<?php echo IMG_PATH; ?>Software/firefox3.gif" alt="firefox.gif" title="Firefox"/>
        </a>
        <a onclick="Javascript: window.open('http://validator.w3.org/check?uri=referer', '_blank'); void(0);">
            <img src="<?php echo IMG_PATH; ?>Validator/valid-xhtml11.png"
                 alt="Valid XHTML 1.0 Strict"  />
        </a>
        <a onclick="Javascript: window.open('http://jigsaw.w3.org/css-validator/', '_blank'); void(0);">
            <img src="<?php echo IMG_PATH; ?>Validator/vcss.gif" alt="CSS Valido!" />
        </a>

        <?php if ($ShowTime == true) {
?>
            <label class="note">
                Pagina elaborata in <?php echo (time() - $start); ?> secondi.
            </label>
            <?php
}
    }

    public function drawClosePopup()
    {
        ?>
        <tr>
            <td style="text-align: right;" colspan="4">
                <a onclick="Javascript: ClosePopup(); void(0);" title="Chiudi Finestra">
                    [ <label class="err" style="cursor: pointer;">Chiudi</label> ]
                </a>
            </td>
        </tr>
        <?php
    }

    public function drawHeader($User, $Group, $PageTitle)
    {
        ?>
        <div id="header">
            <table style="width: 100%;" class="header">
                <tr>
                    <td style="text-align: right; width: 4em;">
                        <label>Utente:</label>
                    </td>

                    <td style="width: 25%;">
                        <a onclick="Javascript: ShowFormDiv('frm_passwd.php', '100px', '100px', '500px', '200px', 'popup'); void(0);"
                           title="Click per modificare i dati utente">
        <?php echo $User; ?>
                            <img src="<?php echo IMG_PATH; ?>Links/key.png" alt="key" />
                        </a>
                    </td>

                    <th rowspan="2">
                        <?php
                        if ($PageTitle != "") {
                            echo $PageTitle;
                        }
                        ?>
                    </th>

                </tr>
                <tr>
                    <td style="text-align: right;">
                        <label>Gruppo:</label>
                    </td>
                    <td>
                        <label class="ok">
        <?php echo $Group; ?>
                        </label>
                    </td>
                </tr>
            </table>
        </div>
        <?php
    }

    public function drawMenu($BasePage = "home.php", $SubVoci = true)
    {
        if (filter_input(INPUT_GET, 'gruppo') != "") {
            $_GET['gruppo'] = "Home";
        }

        list($GruppiMenu, $VociMenu) = $this->getVisibleMenu($_SESSION['id_tipo']);
        ?>

        <ul id="horizontalmenu">
            <li>
                <a href="?act=welc">
                    <img src="<?php echo IMG_PATH; ?>Links/home.png" alt="Home" title="Home" />
                    Home
                </a>
            </li>

            <?php foreach ($GruppiMenu as $key_group => $field_group) {
?>
                <li <?php
                if ($field_group['gruppo'] == $_GET['gruppo']) {
                    echo "style=\"background-color: #DFE6A1;\"";
                };
                ?>>

                    <a <?php
                    if ($SubVoci == false) {
                        echo " href=\"?gruppo=" . $field_group['gruppo'] . "\" ";
                    }
                    ?>
                        title="<?php echo $field_group['titolo_gruppo']; ?>">
                        <img src="<?php echo IMG_PATH; ?>Links/down.png" alt="Down" />
            <?php echo $field_group['titolo_gruppo']; ?>
                    </a>

                        <?php if ($SubVoci == true) {
?>
                        <ul>
                            <?php
                            foreach ($VociMenu as $key_voci => $field_voci) {
                                if ($field_group['gruppo'] == $field_voci['gruppo']) {
                                    ?>
                                    <li>
                                        <a href="<?php
                                        echo htmlspecialchars(
                                            $field_voci['link'],
                                            ENT_QUOTES,
                                            'UTF-8'
                                        );
                                        ?>">
                                                <?php
                                                echo htmlspecialchars(
                                                    $field_voci['titolo'],
                                                    ENT_QUOTES,
                                                    'UTF-8'
                                                );
                                                ?>
                                        </a>
                                    </li>
                                    <?php
                                }
                            }
                            ?>
                        </ul>
                <?php
} ?>
                </li>
        <?php
} ?>

            <li>
                <a href="logout.php">
                    <img src="<?php echo IMG_PATH; ?>Links/exit.png" alt="Esci" title="Esci" />
                    Esci
                </a>
            </li>
        </ul>

        <select id="horizontalmenumini" onchange="Javascript: go(this.value);" >
            <option value="home.php">Home</option>
                <?php foreach ($GruppiMenu as $key_group => $field_group) {
?>
                <optgroup label="<?php echo $field_group['titolo_gruppo']; ?>">
                    <?php
                    foreach ($VociMenu as $key_voci => $field_voci) {
                        if ($field_group['gruppo'] == $field_voci['gruppo']) {
                            ?>
                            <option value="<?php
                            echo htmlspecialchars(
                                $field_voci['link'],
                                ENT_QUOTES,
                                'UTF-8'
                            );
                            ?>"
                                    <?php
                                    if ($_GET['act'] == $field_voci['act']) {
                                        echo "selected=\"\selected\"";
                                    }
                                    ?> >
                                        <?php
                                        echo htmlspecialchars(
                                            $field_voci['titolo'],
                                            ENT_QUOTES,
                                            'UTF-8'
                                        );
                                        ?>
                            </option>
                            <?php
                        }
                    }
                    ?>
                </optgroup>
        <?php
} ?>
            <option value="logout.php">Esci</option>
        </select>
        <?php
    }

    public function drawMenuGroup(
        $Spacer = "&nbsp;&nbsp;&rsaquo;&rsaquo;",
        $ImageGroup = 0,
        $ImageLinks = 1
    ) {
        $GruppiMenu = $this->getRows(
            "distinct(gruppo), image_group",
            "tab_menu",
            "",
            "",
            "gruppo",
            false
        );

        //~ echo "<pre>";
        //~ print_r($GruppiMenu);
        //~ echo "</pre>";

        foreach ($GruppiMenu as $key_group => $field_group) {
            $VociMenu = $this->GetRows(
                "*",
                "tab_menu",
                "gruppo = '"
                . $field_group['gruppo'] . "'",
                "",
                "titolo",
                1
            );
            ?>

            <details>
                <summary>
                    <?php if ($ImageGroup == 0) {
?>
                        <img src="<?php echo $field_group['image_group']; ?>" alt="img" />
                        <?php
}
                    echo $field_group['gruppo'];
                    ?>
                </summary>

                <table style="width: 100%;">
                            <?php foreach ($VociMenu as $field) {
?>
                        <tr>
                            <td title="<?php echo $field['tip']; ?>" >
                                <?php if ($ImageLinks != 1) {
?>
                                    <img src="<?php echo $field['image_link'] ?>" alt="img" />
                                <?php
} ?>

                                    <?php echo $Spacer; ?>
                                <a href="<?php echo $field['link']; ?>">
                <?php echo $field['titolo']; ?>
                                </a>
                            </td>
                        </tr>
            <?php
} ?>
                </table>
            </details>
            <br />
            <?php
        }
    }
}

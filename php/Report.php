<?php

namespace clagiordano\weblibs\php;

use clagiordano\weblibs\php\ReportNode;

/**
 * Report class
 *
 * @version  1.2
 * @author   Claudio Giordano <claudio.giordano@autistici.org>
 * @since    2015-03-29
 * @category Image
 * @package  weblibs
 */
class Report
{
    private $ReportId   = null;
    private $MarkupHtml = null;
    private $ListNodes  = [];
    private $Css        = null;
    private $LimitRows  = 20;

    const CONTAINER_OPEN  = "<div>";
    const CONTAINER_CLOSE = "</div>";

    /**
     *
     * @return \weblibs\php\Report
     */
    public function __construct()
    {
        return $this;
    }

    /**
     * Return property
     *
     * @return string
     */
    function getReportId()
    {
        return $this->ReportId;
    }

    /**
     * Set property
     *
     * @param string $ReportId
     * @return \weblibs\php\Report
     */
    public function setReportId($ReportId = null)
    {
        if (isset($ReportId)) {
            $this->ReportId = filter_var($ReportId, FILTER_SANITIZE_STRING);
        }

        return $this;
    }

    /**
     * Return property
     *
     * @return string
     */
    function getCss()
    {
        return $this->Css;
    }

    /**
     * Set property
     *
     * @param string $Css
     * @return \weblibs\php\Report
     */
    function setCss($Css)
    {
        $this->Css = $Css;

        return $this;
    }

    /**
     * Get property
     *
     * @return int
     */
    function getLimitRows()
    {
        return $this->LimitRows;
    }

    /**
     * Set property
     *
     * @param int $LimitRows
     * @return \weblibs\php\Report
     */
    function setLimitRows($LimitRows)
    {
        $this->LimitRows = intval($LimitRows);

        return $this;
    }

    /**
     *
     * @param boolean $returnmarkupHtml
     * @return string/null
     */
    public function show($returnmarkupHtml = false)
    {
        $this->render();

        if ($returnmarkupHtml) {
            return $this->MarkupHtml;
        } else {
            echo $this->MarkupHtml;
        }
    }

    /**
     *
     * @return \weblibs\php\Report
     */
    private function render()
    {
        if (isset($this->ReportId)) {
            $this->MarkupHtml .= '<div id= "' . $this->ReportId . '" class="reportContainer">';
        } else {
            $this->MarkupHtml .= "<div>";
        }

        foreach ($this->ListNodes as $Node) {
            $this->MarkupHtml .= $Node->render();
        }

        $this->MarkupHtml .= "</div>";

        return $this;
    }

    /**
     * Add a ReportNode to Report
     *
     * @param ReportNode $Node
     * @param int $Order
     */
    public function addNode(ReportNode $Node = null, $Order = null)
    {
        if (!is_null($Node)) {
            if (!is_null($Order)) {
                $this->ListNodes[$Order] = $Node;
            } else {
                $this->ListNodes[] = $Node;
            }
        }

        return $this;
    }

    /**
     * Remove a ReportNode from Report
     *
     * @param int $NodeNum
     */
    public function removeNode($NodeNum = null)
    {
        if (!is_null($NodeNum)) {
            unset($this->ListNodes[$NodeNum]);
        } else {
            throw new \Exception(__METHOD__ . ': Invalid node number ' . $NodeNum);
        }
    }

    /**
     * Pagination TODO
     *
     * @param string $Page navigation
     * @param int $TotResults
     * @param int $PageLimit @deprecated limit rows for page
     * @param boolean $debug
     * @return string
     */
    private function pageSplit(
        $Page,
        $TotResults,
        $PageLimit = 20,
        $debug = false
    ) {
        $order = "";
        $start = 0;

        switch ($Page) {
            case "all":
                // no limit;
                break;

            case "first":
                $start = 0;
                $order .= " LIMIT $start, " . $PageLimit;
                break;

            case "last":
                $start = (intval($TotResults / $PageLimit) * $PageLimit);
                $order .= " LIMIT $start, " . $PageLimit;
                break;

            case "next":
                if (($_SESSION['start'] + $PageLimit) > $TotResults) {
                    $start = (intval($TotResults / $PageLimit) * $PageLimit);
                } else {
                    $start = ($_SESSION['start'] + $PageLimit);
                }
                $order .= " LIMIT $start, " . $PageLimit;
                break;

            case "prev":
                if (($_SESSION['start'] - $PageLimit) < 0) {
                    $start = 0;
                } else {
                    $start = ($_SESSION['start'] - $PageLimit);
                }
                $order .= " LIMIT $start, " . $PageLimit;
                break;
        }

        $_SESSION['start'] = $start;
        return $order;
    }

    // -------------- OLD -----------------------

    public function drawReport()
    {
        $this->StrOut = "";
        /*
          echo "[Debug][ShowReport] ReportHeader: " . $this->ReportHeader . " <br />";
          echo "[Debug][ShowReport] ReportFields: " . $this->ReportFields . " <br />";
         */

        // tipo,title,align|type,title,align..." :   "th,ID,right|th|field,align"

        /*
         * REPORT HEADER
         */
        $this->Fields = explode("|", $this->ReportHeader);
        $this->StrOut .= "<tr>";
        foreach ($this->Fields as $key => $field) {
            $this->Cell = explode(",", $field);

            if ($this->Cell[2] != "") {
                $this->Cell[2] = ' style="text-align: ' . $this->Cell[2] . ';" ';
            } else {
                $this->Cell[2] = ' style="text-align: left;" ';
            }

            $this->StrOut .= "<" . $this->Cell[0] . $this->Cell[2] . ">"
                    . $this->Cell[1] . "</" . $this->Cell[0] . ">";
        }
        $this->StrOut .= "</tr>";
        $this->StrOut .= "<tr><th colspan=\"" . count($this->Fields) . "\"><hr /></th></tr>";


        /*
         * REPORT RESULTS
         */
        $this->Fields = explode("|", $this->ReportFields);
        foreach ($this->Fields as $key => $field) {
            $this->Cell = explode(",", $field);
            if ($this->Cell[2] != "") {
                $this->Cell[2] = ' style="text-align: ' . $this->Cell[2] . ';" ';
            } else {
                $this->Cell[2] = ' style="text-align: left;" ';
            }
        }

        // Per ogni record
        foreach ($this->LastResultArray as $key => $data) {
            $this->StrOut .= "<tr>";

            // per ogni cella
            foreach ($this->Fields as $key => $field) {
                $this->Cell = explode(",", $field);
                if ($this->Cell[2] != "") {
                    $this->Cell[2] = ' style="text-align: ' . $this->Cell[2] . ';" ';
                } else {
                    $this->Cell[2] = ' style="text-align: left;" ';
                }

                if (preg_match("(" . RegDataTime . ")", $data[$this->Cell[1]])) {
                    // è una data nel formato mysql yyyy-mm-dd hh:ii:ss :
                    $data[$this->Cell[1]] = $this->Inverti_DataTime($data[$this->Cell[1]]);
                }

                if (preg_match("(" . RegDataMySql . ")", $data[$this->Cell[1]])) {
                    // è una data nel formato mysql yyyy-mm-dd:
                    $data[$this->Cell[1]] = $this->Inverti_Data($data[$this->Cell[1]]);
                }


                $this->StrOut .= "<" . $this->Cell[0] . $this->Cell[2] . ">"
                        . $data[$this->Cell[1]]
                        . "</" . $this->Cell[0] . ">";
            }

            $this->StrOut .= "</tr>";
        }

        $this->StrOut .= "<tr><th colspan=\"" . count($this->Fields) . "\"><hr /></th></tr>";

        echo $this->StrOut;
        return true;
    }

    public function showResultBar(
        $TotResults,
        $LinkPage,
        $PageLimit = 20,
        $ShowButton = true
    ) {
        ?>
        <tr>
            <th style="text-align: left;"
            <?php
            if ($ShowButton == false) {
                echo " colspan=\"2\"";
            }
            ?> >
                <!-- Inizio barra con risultati e pulsanti !-->
                <?php if (($TotResults != "") && ($TotResults > 0)) {
?>
                    <label>Risultati visualizzati: </label>

                    <label class="ok">
                        <?php
                        if ($_SESSION['start'] == 0) {
                            echo ($_SESSION['start'] + 1);
                        } else {
                            echo $_SESSION['start'];
                        }
                        ?>
                    </label>

                    &nbsp;-
                    <label class="ok">
                        <?php
                        if ((($_SESSION['start'] + $PageLimit) <= $TotResults) && ($_GET['page'] != "all")) {
                            echo ($_SESSION['start'] + $PageLimit);
                        } else {
                            echo $TotResults;
                        }
                        ?>
                    </label>

                    <label>&nbsp;su&nbsp;</label>
                    <label class="ok">
                        <?php echo $TotResults; ?>
                    </label>

                    <label>&nbsp;corrispondenti</label>

                    &nbsp;
                    <!--<a onclick="Javascript: window.open('frm_pdf_call.php?tipo=<?php //echo $_POST['rad_tipo'];    ?>', '_blank'); void(0);"
                        title="Esporta in Pdf" style="cursor: pointer;">
                            <img src="<?php //echo IMG_PATH;    ?>pdf.png" alt="Pdf" /> Esporta
                    </a>

                    &nbsp;!-->
                    <a onclick="Javascript: posta('0', '<?php echo $LinkPage; ?>&amp;page=first'); void(0);">
                        <img src="<?php echo IMG_PATH; ?>Links/first.png" alt="first" /> Inizio
                    </a>

                    &nbsp;
                    <a onclick="Javascript: posta('0', '<?php echo $LinkPage; ?>&amp;page=prev'); void(0);">
                        <img src="<?php echo IMG_PATH; ?>Links/prev.png" alt="prev" /> Prec
                    </a>

                    &nbsp;
                    <a onclick="Javascript: posta('0', '<?php echo $LinkPage; ?>&amp;page=next'); void(0);">
                        <img src="<?php echo IMG_PATH; ?>Links/next.png" alt="next" /> Succ
                    </a>

                    &nbsp;
                    <a onclick="Javascript: posta('0', '<?php echo $LinkPage; ?>&amp;page=last'); void(0);">
                        <img src="<?php echo IMG_PATH; ?>Links/last.png" alt="last" /> Fine
                    </a>

                    &nbsp;
                    <a onclick="Javascript: posta('0', '<?php echo $LinkPage; ?>&amp;page=all'); void(0);">
                        <img src="<?php echo IMG_PATH; ?>Links/all.png" alt="all" /> Tutti
                    </a>

                    <!--&nbsp;
                    <a onclick="Javascript: posta('0', '<?php //echo $LinkPage;    ?>&amp;page=first&amp;exp=csv'); void(0);">
                        <img src="<?php echo IMG_PATH; ?>Links/xls.png" alt="xls" />
                        Esporta csv
                    </a>

                    &nbsp;
                    <a onclick="Javascript: posta('0', '#'); void(0);">
                        <img src="<?php echo IMG_PATH; ?>Links/xls.png" alt="xls" />
                        Grafico dalla Ricerca
                    </a>!-->

                <?php
} elseif ($TotResults == 0) {
?>
                    <label class="err">Nessun risultato corrispondente.</label>
                <?php
} ?>
            </th>
            <!-- Fine barra con risultati e pulsanti !-->

            <?php if ($ShowButton == true) {
?>
                <th style="text-align: right;">
                    <a onclick="Javascript: posta('0', '<?php echo $LinkPage; ?>&amp;page=first'); void(0);"
                       style="cursor: pointer;" title="Avvia la ricerca">
                        <img src="<?php echo IMG_PATH; ?>Links/find.png" alt="Avvia la ricerca" />Avvia Ricerca
                    </a>

                    &nbsp;
                    <a onclick="Javascript: go('<?php echo $LinkPage; ?>&amp;page=first'); void(0);"
                       style="cursor: pointer;" title="Nuova Ricerca">
                        <img src="<?php echo IMG_PATH; ?>Links/new.png" alt="Nuova Ricerca" />Nuova Ricerca
                    </a>
                </th>
            <?php
} ?>
        </tr>
        <?php
    }

    var $Debug       = 1;
    var $Results     = array();
    var $Fields      = array();
    var $Exclude     = array();
    //var $CellStyle = array();
    var $RowStyle    = "";
    var $SplitReport = 1; // 0 to split results
    var $PageLimit   = 20;

    function RenderCell()
    {
        $StrOut = "<tr";
        if ($this->RowStyle != "") {
            $StrOut .= $this->RowStyle . ">";
        } else {
            $StrOut .= ">";
        }

        // string|align
        foreach ($this->Fields as $key => $field) {
            $StrOut .= "<td";

            if (!array_key_exists($key, $this->Exclude)) {
                if (strpos($field, "|")) {
                    $cell = explode("|", $field);
                    if (trim($cell[1]) != "") {
                        $StrOut .= ' style="text-align: ' . $cell[1] . '"';
                    }
                } else {
                    $cell[0] = $field;
                }
                $StrOut .= ">" . $cell[0] . "</td>";
            } else {
                if ($this->Debug != 1) {
                    echo "[Debug][Report]: Skipped key: $key <br />";
                }
            }
        }

        $StrOut .= "</tr>";

        print $StrOut;
    }
}

class Report2
{
    var $Fields   = array();
    var $Exclude  = array();
    //var $CellStyle = array();
    var $RowStyle = "";

    /* function __construct($Results, $Exclude=null, $SplitReport=1, $PageLimit=20, $Debug=1)
      {
      $this->Debug = $Debug;
      $this->Results = $Results;
      $this->Exclude = $Exclude;
      $this->SplitReport = $SplitReport;
      $this->PageLimit = $PageLimit;
      } */

    function Render(
        $Results,
        $Exclude = null,
        $SplitReport = 1,
        $PageLimit = 20,
        $Debug = 1
    ) {
        $StrOut = "";
        foreach ($Results as $key => $element) {
            $StrOut .= "<tr";

            $StrOut .= ">";

            foreach ($element as $ekey => $field) {
                if (!array_key_exists($ekey, $Exclude)) {
                    $StrOut .= "<td";

                    $StrOut .= ">" . $field . "</td>";
                } else {
                    if ($this->Debug != 1) {
                        echo "[Debug][Report]: Skipped key: $ekey <br />";
                    }
                }
            }
            $StrOut .= "</tr>";
        }

        print $StrOut;
    }

    function RenderCell()
    {
        $StrOut = "<tr";
        if ($this->RowStyle != "") {
            $StrOut .= $this->RowStyle . ">";
        } else {
            $StrOut .= ">";
        }

        // string|align
        foreach ($this->Fields as $key => $field) {
            $StrOut .= "<td";

            if (!array_key_exists($key, $this->Exclude)) {
                if (strpos($field, "|")) {
                    $cell = explode("|", $field);
                    if (trim($cell[1]) != "") {
                        $StrOut .= ' style="text-align: ' . $cell[1] . '"';
                    }
                } else {
                    $cell[0] = $field;
                }
                $StrOut .= ">" . $cell[0] . "</td>";
            } else {
                if ($this->Debug != 1) {
                    echo "[Debug][Report]: Skipped key: $key <br />";
                }
            }
        }

        $StrOut .= "</tr>";

        print $StrOut;
    }
}

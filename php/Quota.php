<?php

namespace clagiordano\weblibs\php;

/**
 * Classe per la gestione delle quote utente e lettura delle informazioni
 * ad essa relative.
 *
 * @author Claudio Giordano <claudio.giordano@wyscout.com>
 */
final class Quota
{
    // dimensiome media video post render LQ -> SD -> HD: 600, 1000, 1400 MB
    // test quota 20GB -> 21474836480 Byte
    /*
     * # info quota
     * - verifica la quota disponibile dell'utente ( quota assegnata )
     * - verifica lo spazio occupato attualmente dai video filtrando la tabella match
     *   i video di cui si è proprietari sommando le dimensioni delle varie versioni ( utilizzo quota )
     * - elenco dei media di cui si è priprietario ( contenuto quota ) (elenco video uploadati)
     * - calcolo spazio residuo
     *
     * # gestione quota
     * - rimozione versione specifica da db e disco previo controlli
     * - acquisto spazio aggiuntivo ed espansione quota ( modifica quota ) (solo admin)
     */
    const QUOTA_CAPABILITY_NAME    = 'platform_media_quota';  // tutti i parametri forniti al costruttore
    const QUOTA_FIELDS_CHECK       = "id as mediaId, userId, filesize_lq, filesize_sd, filesize_hd";        // Id dell'utente

    private $Params          = [];       // Object per la verifica
    private $userId          = 0;        // Id del gruppo
    private $Object          = "";        // Id del sotto gruppo
    private $cerebrumGroupId = 0;        // quota totale assegnata all'utente (mediante capability)
    private $subGroupId      = 0;        // totale utilizzo quota calcolato
    private $quotaTotal      = 0;        // quota restante disponibile per l'utente
    private $quotaUsed       = 0;  // lista dei media appartenenti al gruppo/utente
    private $quotaRemaining  = 0;
    private $mediaList       = [];

    // NOTA: se non  c'e' l'userid sulla tabella match è un contenuto pubblico wyscout

    /**
     *
     * @param array $mixed
     * @param int   $mixed['obj']              required obj per la verifica
     * @param int   $mixed['userId']           optional Id dell'utente
     * @param array $mixed['cerebrumGroupId']: optional - Id del gruppo dell'utente del quale verificare la visibilità.
     * @param array $mixed['subGroupId']:      optional - Id del sottogruppo dell'utente del quale verificare la visibilità.
     * @return \media_quota
     */
    public function __construct($mixed)
    {
        $this->Params = $mixed;

        if (isset($mixed['userId'])) {
            $this->setUserId($mixed['userId']);
        } else {
            $this->setUserId(my_id());
        }

        if (isset($mixed['obj'])) {
            $this->setObject($mixed['obj']);
        } else {
            throw new \Exception("[" . __METHOD__ . "] Invalid object ''.");
        }

        if (isset($mixed['cerebrumGroupId'])) {
            $this->setCerebrumGroupId($mixed['cerebrumGroupId']);
        } else {
            $this->setCerebrumGroupId();
        }

        if (isset($mixed['subGroupId'])) {
            $this->setSubGroupId($mixed['subGroupId']);
        } else {
            $this->setSubGroupId();
        }

        // inizializzo lo stato della quota
        $this->calculateQuotas();

        return $this;
    }

    /**
     * Ricalcola tutte le proprietà
     *
     * @param  boolean $percent - flag per far restituire il valore % al posto del totale.
     * @return \media_quota
     */
    private function calculateQuotas($percent)
    {
        $this->getQuotaTotal($percent);
        $this->getQuotaUsed($percent);
        $this->getQuotaRemaining($percent);

        return $this;
    }

    /**
     * Ricalcola e restituisce la proprietà indicata.
     *
     * @param  boolean $percent - flag per far restituire il valore % al posto del totale.
     * @return int totale quota assegnata all'utente in MB
     */
    public function getQuotaTotal($percent)
    {
        $this->setQuotaTotal($this->calculateQuotaTotal($percent));

        return $this->quotaTotal;
    }

    /**
     * Imposta la proprietà indicata.
     *
     * @param int $quotaTotal
     * @return \media_quota
     */
    public function setQuotaTotal($quotaTotal)
    {
        // Verifica che quotaTotal sia un intero valido, in caso contrario esce.
        if (($this->quotaTotal = filter_var($quotaTotal, FILTER_VALIDATE_INT)) === false) {
            throw new \Exception("[" . __METHOD__ . "] Invalid quotaTotal '" . $quotaTotal . "'.");
        }

        return $this;
    }

    /**
     * Legge la capability dell'utente, la verifica ed imposta la proprietà.
     */
    private function calculateQuotaTotal()
    {
        // Leggo il valore della capability
        if (!($quotaTotalVal = can(self::QUOTA_CAPABILITY_NAME))) {
            throw new \Exception("[" . __METHOD__ . "] User without capability '" . self::QUOTA_CAPABILITY_NAME . "'.");
        }
        return $quotaTotalVal;
    }

    /**
     * Calcola e restituisce la quota disco utilizzata dall'utente.
     *
     * @param  boolean $percent - flag per far restituire il valore % al posto del totale.
     * @return int totale quota utilizzata all'utente in MB
     */
    public function getQuotaUsed($percent = false)
    {
        $this->setQuotaUsed($this->calculateQuotaUsed($percent));

        if ($percent) {
            return (number_format(
                (($this->quotaUsed * 100) / $this->quotaTotal),
                2
            ));
        } else {
            return $this->quotaUsed;
        }
    }

    /**
     * Imposta la proprietà indicata.
     *
     * @param int $quotaUsed
     * @return \media_quota
     */
    public function setQuotaUsed($quotaUsed)
    {
        // Verifica che quotaTotal sia un intero valido, in caso contrario esce.
        if (($this->quotaUsed = filter_var($quotaUsed, FILTER_VALIDATE_INT)) === false) {
            throw new \Exception("[" . __METHOD__ . "] Invalid quotaUsed '" . $quotaUsed . "'.");
        }

        return $this;
    }

    /**
     * TODO
     * Calcola la quota utilizzata per l'utente indicato nel costruttore
     *
     * @param  boolean $percent - flag per far restituire il valore % al posto del totale.
     * @return int     $totalSize
     */
    private function calculateQuotaUsed($percent)
    {
        $this->getMediaList();
        $totalSize = 0;

        // sommo le dimensioni delle versioni
        foreach ($this->mediaList as $key => $value) {
            $this->mediaList[$key]['totalSize'] = ($value['filesize_lq'] + $value['filesize_sd'] + $value['filesize_hd']);

            $totalSize += $this->mediaList[$key]['totalSize'];
        }

        // pr($this->mediaList);
        // pr("totalSize: $totalSize");

        return $totalSize;
    }

    public function getMediaList()
    {
        // Estraggo tutti i contenuti di proprietà del gruppo dell'utente indicato, prelevando sole le dimensioni.
        $qStringMedia = "SELECT " . self::QUOTA_FIELDS_CHECK
                . " FROM `" . constant('media_type::' . $this->Object)
                //. "` WHERE userId = '" . $this->userId . "'";
                . "` WHERE cerebrumGroupId = '" . $this->cerebrumGroupId . "'";

        $qMediaList      = new aeq($qStringMedia, true);
        $this->mediaList = [];

        while ($rowMedia = $qMediaList->fetch()) {
            $this->mediaList[] = $rowMedia;
        }
        unset($qMediaList);

        //pr($this->mediaList);

        return $this;
    }

    /**
     * Restituisce la proprietà indicata.
     *
     * @param  boolean $percent - flag per far restituire il valore % al posto del totale.
     * @return int totale quota restante all'utente in MB
     */
    public function getQuotaRemaining($percent = false)
    {
        /**
         * - calcola differenza fra quotaTotal e quotaUsed
         * - imposta il valore sulla proprietà
         * - restituisci la proprietà
         */
        $this->setQuotaRemaining($this->calculateQuotaRemaining($percent));

        if ($percent) {
            return (number_format(
                (($this->quotaRemaining * 100) / $this->quotaTotal),
                2
            ));
        } else {
            return $this->quotaRemaining;
        }
    }

    /**
     * Imposta la proprietà indicata.
     *
     * @param int $quotaRemaining
     * @return \media_quota
     */
    public function setQuotaRemaining($quotaRemaining)
    {
        // Verifica che quotaRemaining sia un intero valido, in caso contrario esce.
        if (($this->quotaRemaining = filter_var(
            $quotaRemaining,
            FILTER_VALIDATE_INT
        )) === false
        ) {
            throw new \Exception("[" . __METHOD__ . "] Invalid quotaRemaining '" . $quotaRemaining . "'.");
        }

        return $this;
    }

    /**
     * TODO
     * Calcola la quota rimanente per l'utente indicato nel costruttore
     *
     * @param  boolean $percent - flag per far restituire il valore % al posto del totale.
     * @return int     $Remaining
     */
    private function calculateQuotaRemaining($percent)
    {
        $Remaining = ($this->getQuotaTotal() - $this->getQuotaUsed());

        return $Remaining;
    }

    /**
     * Restituisce la proprietà indicata.
     *
     * @return int userId dell'utente
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Imposta la proprietà indicata.
     *
     * @param int $userId
     * @return \media_quota
     */
    public function setUserId($userId)
    {
        // Verifica che userId sia un intero valido, in caso contrario esce.
        if (($this->userId = filter_var($userId, FILTER_VALIDATE_INT)) === false) {
            throw new \Exception("[" . __METHOD__ . "] Invalid userId '" . $userId . "'.");
        }

        return $this;
    }

    /**
     * Restituisce la proprietà indicata.
     *
     * @return int userId dell'utente
     */
    public function getObject()
    {
        return $this->Object;
    }

    /**
     * Imposta la proprietà indicata.
     *
     * @param int $Object
     * @return \media_quota
     */
    public function setObject($Object)
    {
        if (!($this->Object = filter_var($Object, FILTER_SANITIZE_STRING))) {
            throw new \Exception("[" . __METHOD__ . "] Invalid Object '" . $Object . "'.");
        }

        return $this;
    }

    /**
     * Restituisce la proprietà indicata.
     *
     * @return int cerebrumGroupId dell'utente
     */
    public function getCerebrumGroupId()
    {
        return $this->cerebrumGroupId;
    }

    /**
     * Imposta la proprietà indicata.
     *
     * @param int $ID
     * @return \media_quota
     */
    public function setCerebrumGroupId($ID)
    {
        // Verifica che il cerebrumGroupId sia un intero valido, in caso contrario esce.
        if (($this->cerebrumGroupId = filter_var($ID, FILTER_VALIDATE_INT)) === false) {
            $this->cerebrumGroupId = network::getMyGroupId();
        }

        return $this;
    }

    /**
     * Restituisce la proprietà indicata.
     *
     * @return int subGroupID dell'utente
     */
    public function getSubGroupId()
    {
        return $this->subGroupId;
    }

    /**
     * Imposta la proprietà indicata.
     *
     * @param int $ID
     * @return \media_quota
     */
    public function setSubGroupId($ID)
    {
        // Verifica che userId sia un intero valido, in caso contrario esce.
        if (!($this->subGroupId = filter_var($ID, FILTER_VALIDATE_INT))) {
            $this->subGroupId = network::getMyNetworkSubGroups();
        }

        return $this;
    }

    /**
     * @override embedded function
     */
    public function print_r($Array = true)
    {
        echo "<pre>";

        if ($Array) {
            echo "         Params: " . print_r($this->Params, true) . "\n";
        }

        echo "         userId: " . $this->userId . "\n";
        echo "         Object: " . $this->Object . "\n";
        echo "cerebrumGroupId: " . $this->cerebrumGroupId . "\n";

        if ($Array) {
            echo "     subGroupId: " . print_r($this->subGroupId, true) . "\n";
        }

        echo "     quotaTotal: " . $this->quotaTotal . "\n";
        echo "      quotaUsed: " . $this->quotaUsed . " - ( " . $this->getQuotaUsed(true) . "% )\n";
        echo " quotaRemaining: " . $this->quotaRemaining . " - ( " . $this->getQuotaRemaining(true) . "% )\n";

        if ($Array) {
            echo "      mediaList: " . print_r($this->mediaList, true) . "\n";
        }

        echo "</pre>";
    }
}

<?php

namespace clagiordano\weblibs\php;

/**

 * Database.php
 * Version: 4.6.34
 *
 * Copyright 2010 - 2014 Claudio Giordano <claudio.giordano@autistici.org>
 * @deprecated since Database 5.0
 */
class DatabaseLegacy
{

    function __construct(
        $db_host,
        $db_user,
        $db_password,
        $db_name,
        $backend = "mysql"
    ) {
        define("DeveloperMailAddress", "claudio.giordano@autistici.org");
        define("RegDataMySql", "([1-2][0-9][0-9][0-9]-[0-1][0-9]-[0-3][0-9])");
        define("RegDataIt", "([0-3][0-9]/[0-1][0-9]/[1-2][0-9][0-9][0-9])");
        define("RegTime", "([0-2][0-9]:[0-6][0-9]:[0-6][0-9])");
        define(
            "RegDataTime",
            "([1-2][0-9][0-9][0-9]-[0-1][0-9]-[0-3][0-9] [0-2][0-9]:[0-6][0-9]:[0-6][0-9])"
        );

        $this->ReportHeader = "";
        $this->ReportFields = "";
        $this->BackEnd      = $backend;
        $this->ResultStatus = "";
        $this->LastID       = "";
        $this->SaveErrors   = "";

        switch ($backend) {
            case "mysql":
                if (!$this->db = mysqli_connect($db_host, $db_user, $db_password)) {
                    $this->StampaErr(
                        "mysql_connect",
                        "Errore durante la connessione al database,"
                        . " verificare i parametri."
                    );
                    exit();
                }

                if (!mysqli_select_db($this->db, $db_name)) {
                    $this->StampaErr(
                        "mysql_select_db",
                        "Errore durante la selezione del database,"
                        . " verificare i parametri."
                    );
                    exit();
                }
                break;

            case "postgresql":
                if (!$this->db = pg_connect("host=$db_host port=5432 dbname=$db_name user=$db_user password=$db_password")) {
                    echo "[Debug]: pg_connection_status: " . pg_connection_status($this->db) . " <br />";
                    $this->StampaErr(
                        "pg_connect",
                        "Errore durante la connessione
                            o selezione del database, verificare i parametri."
                    );
                    exit();
                }

                break;
        }

        return $this->db;
    }

    function SaveRow($Valori, $Esclusioni, $Tab, $debug = 1, $CkFields = "")
    {
        /*
         * Compongo la query con i dati provenienti dall'array del post:
         */

        $Status = array();
        $Unique = 0;

        $q = "INSERT INTO $Tab ";
        $k = "(";
        $f = ") values (";

        if ($Esclusioni != "") {
            foreach ($Valori as $key => $field) {
                if (!array_key_exists($key, $Esclusioni)) {
                    $k .= "$key, ";
                    $f .= "'" . addslashes(stripslashes($field)) . "', ";
                } else {
                    if ($debug != 1) {
                        echo "[Debug][SaveRow]: Escludo la key: $key dalla Query <br />";
                    }
                }
            }
        } else {
            foreach ($Valori as $key => $field) {
                $k .= "$key, ";
                $f .= "'" . addslashes(stripslashes($field)) . "', ";
            }
        }

        $f .= ");";
        $q .= $k . $f;
        $q = str_replace(", )", ")", $q);

        /*
         * Check Verifica Unicità valori:
         */
        $CkWere = "";
        if ($CkFields != "") {
            $CkArray = explode(",", $CkFields);
            $CkWere .= "(";
            foreach ($CkArray as $key => $field) {
                $CkWere .= "$field = '" . trim(addslashes(stripslashes($Valori[$field]))) . "' ";
                if ($key < count($CkArray) - 1) {
                    $CkWere .= "OR ";
                }
            }
            $CkWere .= ")";
            $UniqueRows = $this->GetRows("*", $Tab, $CkWere, "", "", $debug);
            $Unique += count($UniqueRows);
        }

        if ($debug != 1) {
            echo "[Debug]: CkWere: $CkWere <br />";
            echo "[Debug]: Unique: $Unique <br />";
            echo "[Debug][SaveRow]: $q <br />";
        }

        $this->SessionLog("[Debug][SaveRow]: $q");


        /*
         * Query completata.
         */

        if ($Unique == 0) {
            switch ($this->BackEnd) {
                case "mysql":
                    $r                  = mysqli_query($this->db, $q) or
                    die($this->StampaErr(
                        $q,
                        '[SaveRow] '
                    ));
                    $this->ResultStatus = "Dati inseriti correttamente.";
                    // Recupero l'id dell'ultimo elemento inserito:
                    $this->LastID       = mysql_insert_id();

                    return $this->ResultStatus;
                    break;

                case "postgresql":
                    $r = pg_query($this->db, $q) or
                    die($this->StampaErr(
                        $q,
                        '[SaveRow]'
                    ));
                    break;
            }
        } else {
            $this->SaveErrors = "Errore, uno dei campi non risulta univoco, salvataggio annullato.";
            return $this->SaveErrors;
        }
    }

    function UpdateRow($Valori, $Esclusioni, $Tab, $Where, $debug = 1)
    {
        /*
         * Compongo la query con i dati provenienti dall'array del post:
         */
        $q = "UPDATE $Tab SET ";

        if ($Esclusioni != "") {
            foreach ($Valori as $key => $field) {
                if (!array_key_exists($key, $Esclusioni)) {
                    $q .= "$key='" . addslashes(stripslashes($field)) . "', ";
                } else {
                    if ($debug != 1) {
                        echo "[Debug][UpdateRow]: Escludo la key: $key dalla Query <br />";
                    }
                }
            }
        } else {
            foreach ($Valori as $key => $field) {
                $q .= "$key='" . addslashes(stripslashes($field)) . "', ";
            }
        }

        $q .= "WHERE $Where ";
        $q = str_replace(", WHERE", " WHERE", $q);

        $this->SessionLog("[Debug][UpdateRow]: $q");

        if ($debug != 1) {
            echo "[Debug][UpdateRow]: $q <br />";
        }

        /*
         * Query completata.
         */

        $r                  = mysqli_query($this->db, $q) or die($this->StampaErr(
            $q,
            '[UpdateRow] '
        ));
        //$Status =  "Debug => Query Update: $q <br />";
        $this->ResultStatus = "Dati modificati correttamente.";
        // $Status = $q;
        return $this->ResultStatus;
    }

    function GetRows(
        $Fields = "*",
        $Tab,
        $Where = "",
        $Group = "",
        $Order = "",
        $debug = 1
    ) {
        $Results = array();
        //$this->ValidateQuery($Where);

        if (trim($Fields) == "") {
            $Fields = "*";
        }

        $q = "SELECT $Fields FROM $Tab";

        if ($Where != "") {
            //$q .= " WHERE " . addslashes(stripslashes($Where));
            $q .= " WHERE $Where";
        }

        if ($Group != "") {
            $q .= " GROUP BY $Group";
        }

        if ($Order != "") {
            $q .= " ORDER BY $Order";
        }

        switch ($this->BackEnd) {
            case "mysql":
                $r = mysqli_query($this->db, $q) or
                die($this->StampaErr(
                    $q,
                    '[GetRows]'
                ));
                $n = mysqli_affected_rows($this->db);

            while ($Row = mysqli_fetch_assoc($r)) {
                array_push($Results, $Row);
            }
                mysqli_free_result($r);
                break;

            case "postgresql":
                $r       = pg_query($this->db, $q) or
                die($this->StampaErr(
                    $q,
                    '[GetRows]'
                ));
                $n       = pg_num_rows($r);
                $Results = pg_fetch_all($r);
                pg_free_result($r);
                break;
        }

        if ($debug != 1) {
            echo "[Debug][GetRows]: $q <br /><br />";
        }

        $this->LastResultArray = $Results;
        return $Results;
    }

    function GetId($Fields = "*", $Tab, $Where, $debug = 1)
    {
        $q = "SELECT $Fields FROM $Tab WHERE $Where";

        switch ($this->BackEnd) {
            case "mysql":
                $r = mysqli_query($this->db, $q) or
                die($this->StampaErr(
                    $q,
                    '[GetId]'
                ));
                $n = mysql_affected_rows($this->db);

            while ($Row = mysql_fetch_assoc($r)) {
                array_push($Results, $Row);
            }
                mysql_free_result($r);
                break;

            case "postgresql":
                $r       = pg_query($this->db, $q) or
                die($this->StampaErr(
                    $q,
                    '[GetId]'
                ));
                $n       = pg_num_rows($r);
                $Results = pg_fetch_all($r);
                pg_free_result($r);
                break;
        }

        if ($debug != 1) {
            echo "[Debug][GetId]: $q <br />";
        }

        return $row;
    }

    function CheckInclude($Action, $DefaultPage, $DefaultTitle, $Group)
    {
        $Action = addslashes(stripslashes($Action));
        $Pages  = $this->GetRows(
            "*",
            "tab_menu",
            "act = '$Action' AND perm LIKE '%$Group%'",
            "",
            "",
            1
        );

        if (count($Pages) == 1) {
            $Page                   = $Pages[0]['page'];
            $_SESSION['titolo_pag'] = $Pages[0]['titolo_pag'];
        } else {
            $Page                   = $DefaultPage;
            $_SESSION['titolo_pag'] = $DefaultTitle;
        }

        $this->DrawHeader(
            $_SESSION['desc_user'],
            $_SESSION['tipo'],
            $_SESSION['titolo_pag']
        );

        return $Page;
    }

    function DeleteRows($Tab, $Where, $debug = 1)
    {
        $q = "DELETE FROM $Tab Where $Where";

        switch ($this->BackEnd) {
            case "mysql":
                $r = mysqli_query($this->db, $q) or
                die($this->StampaErr(
                    $q,
                    '[DeleteRows] '
                ));
                break;

            case "postgresql":
                $r = pg_query($this->db, $q) or
                die($this->StampaErr(
                    $q,
                    '[DeleteRows]'
                ));
                break;
        }

        $this->SessionLog("[Debug][DeleteRows]: $q");

        if ($debug != 1) {
            echo "[Debug][DeleteRows]: $q <br />";
        }
        return true;
    }

    function StampaErr($Command, $Note = "-")
    {
        // includo il modulo di rete per l'invio della mail:
//        require_once __DIR__ . "../function/Net.php";

        $ln                  = "%0A";
        $tab                 = "%20%20%20%20";
        $this->ErrorMsgStyle = "padding: 10px; font-size: 9pt; font-weight: bold;"
                . "color: red; border: 2px dashed red;"
                . "width: 40%; height: auto;"
                . "top: 10%; left: 10%; position: absolute;";

        //~ $this->ErrorMail = "Si e' verificato un errore sulla query:"
        //~ . "$ln $ln L'errore riporta: $ln $tab (" . mysql_errno() . ") " . mysql_error()
        //~ . "$ln $ln L'errore e' stato generato da: $ln $tab $Command"
        //~ . "$ln $ln Nota relativa all'errore: $ln $tab $Note"
        //~ . "$ln $ln Informazioni Aggiuntive: $ln"
        //~ . "$tab Get Request: " . str_replace("&", "%26", http_build_query($_GET)) . "$ln"
        //~ . "$tab Post Request: " . str_replace("&", "%26", http_build_query($_POST)) . "$ln";

        $this->ErrorMail = "Si e' verificato un errore sulla query:"
                . "\n\n L'errore riporta: \n\t (" . $this->db->errno . ") " . $this->db->error
                . "\n\n L'errore e' stato generato da: \n\t $Command"
                . "\n\n Nota relativa all'errore: \n\t $Note"
                . "\n\n Informazioni Aggiuntive: \n"
                . "\t Get Request: " . str_replace(
                    "&",
                    "%26",
                    http_build_query($_GET)
                ) . "\n"
                . "\t Post Request: " . str_replace(
                    "&",
                    "%26",
                    http_build_query($_POST)
                ) . "\n";

        $this->ErrorBox = '<div style="' . $this->ErrorMsgStyle . '">'
                . "Si è verificato un errore sulla query:<br /><br />"
                . "L'errore riporta: <br />"
                . "&nbsp;&nbsp;&nbsp;&nbsp; (" . $this->db->errno . ") " . $this->db->error . "<br /><br />"
                . "L'errore e' stato generato da: <br />"
                . "&nbsp;&nbsp;&nbsp;&nbsp;$Command <br /><br />"
                . "Nota relativa all'errore: <br />"
                . "&nbsp;&nbsp;&nbsp;&nbsp;$Note <br /><br />"
                . "Informazioni Aggiuntive: <br />"
                . "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Get Request: " . http_build_query($_GET) . "<br /><br />"
                . "&nbsp;&nbsp;&nbsp;&nbsp;Post Request: " . http_build_query($_POST) . "<br /><br />"

        /* . "<a href=\"mailto:" . DeveloperMailAddress . "?Subject=[BugReport]"
          . "&body=" . $this->ErrorMail
          . '" title="invia segnalazione bug">Invia la segnalazione dell\'errore.</a>' */;
        $MailData = array('from'    => "[DataBase] <claudio.giordano@autistici.org>",
            'to'      => 'Claudio Giordano <claudio.giordano@autistici.org>',
            'subject' => "[DataBase][BugReport] $Command",
            'body'    => $this->ErrorMail);

//        DirectSendMail2($MailData);
        echo $this->ErrorBox;
    }

    function SessionLog($LogString)
    {
        $q = "INSERT INTO tab_session (session_id, id_user, data, ip,
                action, platform, arch, browser, version) values ("
                . "'" . session_id() . "', '" . $_SESSION['id_user'] . "', '" . date('Y-m-d H:i:s')
                . "', '" . $_SERVER['REMOTE_ADDR'] . "', '" . addslashes(stripslashes($LogString))
                . "', '" . $_SESSION['platform'] . "', '" . $_SESSION['arch'] . "', '"
                . $_SESSION['browser'] . "', '" . $_SESSION['version'] . "')";

        switch ($this->BackEnd) {
            case "mysql":
                $r = $this->db->query($q) or
                die($this->StampaErr(
                    $q,
                    '[SessionLog] '
                ));
                break;

            case "postgresql":
                $r = pg_query($this->db, $q) or
                die($this->StampaErr(
                    $q,
                    '[SessionLog]'
                ));
                break;
        }
    }

    function ShowResultBar(
        $TotResults,
        $LinkPage,
        $PageLimit = 20,
        $ShowButton = true
    ) {
        ?>
        <tr>
            <th style="text-align: left;"
            <?php
            if ($ShowButton == false) {
                echo " colspan=\"2\"";
            }
            ?> >
                <!-- Inizio barra con risultati e pulsanti !-->
                <?php if (($TotResults != "") && ($TotResults > 0)) {
?>
                    <label>Risultati visualizzati: </label>

                    <label class="ok">
                        <?php
                        if ($_SESSION['start'] == 0) {
                            echo ($_SESSION['start'] + 1);
                        } else {
                            echo $_SESSION['start'];
                        }
                        ?>
                    </label>

                    &nbsp;-
                    <label class="ok">
                        <?php
                        if ((($_SESSION['start'] + $PageLimit) <= $TotResults) && ($_GET['page'] != "all")) {
                            echo ($_SESSION['start'] + $PageLimit);
                        } else {
                            echo $TotResults;
                        }
                        ?>
                    </label>

                    <label>&nbsp;su&nbsp;</label>
                    <label class="ok">
                        <?php echo $TotResults; ?>
                    </label>

                    <label>&nbsp;corrispondenti</label>

                    &nbsp;
                    <!--<a onclick="Javascript: window.open('frm_pdf_call.php?tipo=<?php //echo $_POST['rad_tipo'];         ?>', '_blank'); void(0);"
                        title="Esporta in Pdf" style="cursor: pointer;">
                            <img src="<?php //echo IMG_PATH;         ?>pdf.png" alt="Pdf" /> Esporta
                    </a>

                    &nbsp;!-->
                    <a onclick="Javascript: posta('0', '<?php echo $LinkPage; ?>&amp;page=first');
                            void(0);">
                        <img src="<?php echo IMG_PATH; ?>Links/first.png" alt="first" /> Inizio
                    </a>

                    &nbsp;
                    <a onclick="Javascript: posta('0', '<?php echo $LinkPage; ?>&amp;page=prev');
                            void(0);">
                        <img src="<?php echo IMG_PATH; ?>Links/prev.png" alt="prev" /> Prec
                    </a>

                    &nbsp;
                    <a onclick="Javascript: posta('0', '<?php echo $LinkPage; ?>&amp;page=next');
                            void(0);">
                        <img src="<?php echo IMG_PATH; ?>Links/next.png" alt="next" /> Succ
                    </a>

                    &nbsp;
                    <a onclick="Javascript: posta('0', '<?php echo $LinkPage; ?>&amp;page=last');
                            void(0);">
                        <img src="<?php echo IMG_PATH; ?>Links/last.png" alt="last" /> Fine
                    </a>

                    &nbsp;
                    <a onclick="Javascript: posta('0', '<?php echo $LinkPage; ?>&amp;page=all');
                            void(0);">
                        <img src="<?php echo IMG_PATH; ?>Links/all.png" alt="all" /> Tutti
                    </a>

                    <!--&nbsp;
                    <a onclick="Javascript: posta('0', '<?php //echo $LinkPage;        ?>&amp;page=first&amp;exp=csv'); void(0);">
                        <img src="<?php echo IMG_PATH; ?>Links/xls.png" alt="xls" />
                        Esporta csv
                    </a>

                    &nbsp;
                    <a onclick="Javascript: posta('0', '#'); void(0);">
                        <img src="<?php echo IMG_PATH; ?>Links/xls.png" alt="xls" />
                        Grafico dalla Ricerca
                    </a>!-->

                <?php
} elseif ($TotResults == 0) {
?>
                    <label class="err">Nessun risultato corrispondente.</label>
                <?php
} ?>
            </th>
            <!-- Fine barra con risultati e pulsanti !-->

            <?php if ($ShowButton == true) {
?>
                <th style="text-align: right;">
                    <a onclick="Javascript: posta('0', '<?php echo $LinkPage; ?>&amp;page=first');
                            void(0);"
                       style="cursor: pointer;" title="Avvia la ricerca">
                        <img src="<?php echo IMG_PATH; ?>Links/find.png" alt="Avvia la ricerca" />Avvia Ricerca
                    </a>

                    &nbsp;
                    <a onclick="Javascript: go('<?php echo $LinkPage; ?>&amp;page=first');
                            void(0);"
                       style="cursor: pointer;" title="Nuova Ricerca">
                        <img src="<?php echo IMG_PATH; ?>Links/new.png" alt="Nuova Ricerca" />Nuova Ricerca
                    </a>
                </th>
            <?php
} ?>
        </tr>
        <?php
    }

    function PageSplit($Page, $TotResults, $PageLimit = 20, $debug = 1)
    {
        $order = "";
        $start = 0;

        switch ($Page) {
            case "all":
                // no limit;
                break;

            case "first":
                $start = 0;
                $order .= " LIMIT $start, " . $PageLimit;
                break;

            case "last":
                $start = (intval($TotResults / $PageLimit) * $PageLimit);
                $order .= " LIMIT $start, " . $PageLimit;
                break;

            case "next":
                if (($_SESSION['start'] + $PageLimit) > $TotResults) {
                    $start = (intval($TotResults / $PageLimit) * $PageLimit);
                } else {
                    $start = ($_SESSION['start'] + $PageLimit);
                }
                $order .= " LIMIT $start, " . $PageLimit;
                break;

            case "prev":
                if (($_SESSION['start'] - $PageLimit) < 0) {
                    $start = 0;
                } else {
                    $start = ($_SESSION['start'] - $PageLimit);
                }
                $order .= " LIMIT $start, " . $PageLimit;
                break;
        }

        $_SESSION['start'] = $start;
        return $order;
    }

    function DrawReport()
    {
        $this->StrOut = "";
        /*
          echo "[Debug][ShowReport] ReportHeader: " . $this->ReportHeader . " <br />";
          echo "[Debug][ShowReport] ReportFields: " . $this->ReportFields . " <br />";
         */

        // tipo,title,align|type,title,align..." :   "th,ID,right|th|field,align"

        /*
         * REPORT HEADER
         */
        $this->Fields = explode("|", $this->ReportHeader);
        $this->StrOut .= "<tr>";
        foreach ($this->Fields as $key => $field) {
            $this->Cell = explode(",", $field);

            if ($this->Cell[2] != "") {
                $this->Cell[2] = ' style="text-align: ' . $this->Cell[2] . ';" ';
            } else {
                $this->Cell[2] = ' style="text-align: left;" ';
            }

            $this->StrOut .= "<" . $this->Cell[0] . $this->Cell[2] . ">"
                    . $this->Cell[1] . "</" . $this->Cell[0] . ">";
        }
        $this->StrOut .= "</tr>";
        $this->StrOut .= "<tr><th colspan=\"" . count($this->Fields) . "\"><hr /></th></tr>";


        /*
         * REPORT RESULTS
         */
        $this->Fields = explode("|", $this->ReportFields);
        foreach ($this->Fields as $key => $field) {
            $this->Cell = explode(",", $field);
            if ($this->Cell[2] != "") {
                $this->Cell[2] = ' style="text-align: ' . $this->Cell[2] . ';" ';
            } else {
                $this->Cell[2] = ' style="text-align: left;" ';
            }
        }

        // Per ogni record
        foreach ($this->LastResultArray as $key => $data) {
            $this->StrOut .= "<tr>";

            // per ogni cella
            foreach ($this->Fields as $key => $field) {
                $this->Cell = explode(",", $field);
                if ($this->Cell[2] != "") {
                    $this->Cell[2] = ' style="text-align: ' . $this->Cell[2] . ';" ';
                } else {
                    $this->Cell[2] = ' style="text-align: left;" ';
                }

                if (preg_match("(" . RegDataTime . ")", $data[$this->Cell[1]])) {
                    // è una data nel formato mysql yyyy-mm-dd hh:ii:ss :
                    $data[$this->Cell[1]] = $this->Inverti_DataTime($data[$this->Cell[1]]);
                }

                if (preg_match("(" . RegDataMySql . ")", $data[$this->Cell[1]])) {
                    // è una data nel formato mysql yyyy-mm-dd:
                    $data[$this->Cell[1]] = $this->Inverti_Data($data[$this->Cell[1]]);
                }


                $this->StrOut .= "<" . $this->Cell[0] . $this->Cell[2] . ">"
                        . $data[$this->Cell[1]]
                        . "</" . $this->Cell[0] . ">";
            }

            $this->StrOut .= "</tr>";
        }

        $this->StrOut .= "<tr><th colspan=\"" . count($this->Fields) . "\"><hr /></th></tr>";

        echo $this->StrOut;
        return true;
    }

    function ShowEditBar(
        $DescString,
        $Mod,
        $Links,
        $Action,
        $Span = 0,
        $CountDett = 0,
        $PopupLink = "#",
        $ArbLinks = "",
        $Indietro = ""
    ) {
        $ConfSalva = "Salvare i dati inseriti?";

        // Abolito con il passaggio alla struttura a div
        //~ if ($Span > 1)
        //~ {
        //~ $ColSpan = "colspan=\"" . $Span . "\"";
        //~ }else{
        //~ $ColSpan = "";
        //~ }

        echo "<div id=\"edit-bar\">";
        //~ echo "<label class=\"title\">$DescString</label>&nbsp;";
        echo "$DescString&nbsp;";

        if (trim($Mod) != "") {
            echo "<label class=\"err_status\">(In Modifica)&nbsp;</label>";
            $UrlSalva  = "?act=$Action&amp;tomod=" . $Mod . "&amp;salva=modifica";
            $UrlAction = "?act=$Action&amp;tomod=" . $Mod;
        } else {
            echo "<label class=\"err_status\">(Nuovo Inserimento)&nbsp;</label>";
            $UrlSalva  = "?act=$Action&amp;salva=salva";
            $UrlAction = "?act=$Action";
        }

        $ArraiLinks = explode(",", $Links);
        foreach ($ArraiLinks as $key => $link) {
            switch ($link) {
                case "new":
                    echo "&nbsp;<a onclick=\"Javascript: "
                    . "go_conf2('I dati non salvati verranno persi, continuare?', "
                    . "'?act=" . $Action . "&amp;reset=0'); void(0);\">"
                    . "<img src=\"" . IMG_PATH . "Links/new.png\" alt=\"new\" title=\"Nuovo\" />"
                    . "Nuovo"
                    . "</a>";
                    break;

                case "oldsave":
                    echo "&nbsp;<a onclick=\"Javascript: conferma_salva('0', '"
                    . $ConfSalva . "', '" . $UrlSalva . "');\" >"
                    . "<img src=\"" . IMG_PATH . "Links/save.png\" alt=\"save\" title=\"Salva\" />"
                    . "Salva"
                    . "</a>";
                    break;

                case "save":
                    echo "&nbsp;<a onclick=\"Javascript: VerificaSalvaPost('0', '"
                    . $ConfSalva . "', '" . $UrlSalva . "');\" >"
                    . "<img src=\"" . IMG_PATH . "Links/save.png\" alt=\"save\" title=\"Salva\" />"
                    . "Salva"
                    . "</a>";
                    break;

                case "add":
                    echo "&nbsp;<a onclick=\"Javascript: popUpWindow('" . $Action . "', '$PopupLink";
                    if (trim($Mod) != "") {
                        echo "?tomod=$Mod";
                    }

                    echo "', '200px', '200px', '750px', '300px');\">"
                    . "<img src=\"" . IMG_PATH . "Links/add.png\" alt=\"add\" title=\"Aggiungi\" />"
                    . "Aggiungi";
                    echo "</a>";
                    break;

                case "clear":
                    echo "&nbsp;<a onclick=\"Javascript: go_conf2('Rimuovere tutte le voci dal dettaglio?', "
                    . "'?act=" . $Action . "&amp;svuota=0'); void(0);\">"
                    . "<img src=\"" . IMG_PATH . "Links/clear.png\" alt=\"clear\" />"
                    . "Voci ( <label class=\"err\" title=\"Totale voci dettaglio\">"
                    . $CountDett . "</label> )"
                    . "</a>";
                    break;

                case "blank":
                    echo "&nbsp;<a onclick=\"Javascript: go('" . $UrlAction . "&amp;blank=0');\" >"
                    . "<img src=\"" . IMG_PATH . "Links/new.png\" alt=\"new\" title=\"Nuovo\" />"
                    . "Nuovo"
                    . "</a>&nbsp;";
                    break;

                case "del":
                    break;
            }
        }

        if ($ArbLinks != "") {
            foreach ($ArbLinks as $key => $field) {
                echo $field['link'];
            }
        }

        echo "</div>";


        if (isset($this->SaveErrors) && ($this->SaveErrors != "")) {
            ?>
            <div style="border: 2px solid red; padding: 5px;" class="err">
                <img src="<?php echo IMG_PATH; ?>Links/cancel.png"
                     style="width: 14px; height: 14px; vertical-align: middle;"
                     alt="Errore" title="Errore" />
                        <?php echo $this->SaveErrors; ?>
            </div>
            <?php
        }


        if (isset($this->ResultStatus) && ($this->ResultStatus != "")) {
            ?>
            <div style="border: 2px solid green; padding: 5px;" class="ok">
                <img src="<?php echo IMG_PATH; ?>Links/apply.png"
                     style="width: 14px; height: 14px; vertical-align: middle;"
                     alt="OK" title="OK" />
                        <?php echo $this->ResultStatus; ?>
            </div>
            <?php
        }
    }

    function Inverti_Data($data, $separator = "/", $explode = "-")
    {
        $data_inv = "";

        if (($data == "0000-00-00") || ($data == "00/00/0000")) {
            return "-";
        }

        if (preg_match(RegDataIt, $data)) {
            $separator = "-";
            $explode   = "/";
            //echo "[Debug]: $data: la data è nel formato it <br />";
        }

        if (preg_match(RegDataMySql, $data)) {
            $separator = "/";
            $explode   = "-";
            //echo "[Debug]: $data: la data è nel formato mysql <br />";
        }

        if (preg_match(RegDataTime, $data)) {
            //echo "[Debug]: $data: datatime nel formato mysql <br />";
        }

        $data_array = explode($explode, $data);
        for ($index_data = count($data_array) - 1; $index_data >= 0; $index_data--) {
            $data_inv = $data_inv . $data_array[$index_data];
            if ($index_data != 0) {
                $data_inv = $data_inv . $separator;
            }
        }

        return $data_inv;
    }

    function Inverti_DataTime($data)
    {
        $data_inv   = "";
        $datatime   = explode(" ", $data);
        $data_array = explode("-", $datatime[0]);

        for ($index_data = count($data_array) - 1; $index_data >= 0; $index_data--) {
            $data_inv = $data_inv . $data_array[$index_data];
            if ($index_data != 0) {
                $data_inv = $data_inv . "/";
            }
        }

        return $data_inv . " " . $datatime[1];
    }

    function DateTimeDiff(
        $data_str1,
        $data_str2,
        $format = "Y-m-d H:i:s",
        $unit = "h"
    ) {
        switch ($unit) {
            case "h":
                // hours
                $unit = 3600;
                break;
            case "m":
                // minute
                $unit = 60;
                break;
            case "s":
                // second
                $unit = 1;
                break;
            case "d":
                // day
                $unit = 86400;
                break;
            case "w":
                // week
                $unit = 604800;
                break;
            case "m":
                // week
                $unit = 31536000;
                break;
        }

        list($data1, $time1) = explode(' ', $data_str1);
        list($data2, $time2) = explode(' ', $data_str2);

        list($year1, $month1, $day1) = explode('-', $data1);
        list($year2, $month2, $day2) = explode('-', $data2);

        list($hour1, $min1, $sec1) = explode(':', $time1);
        list($hour2, $min2, $sec2) = explode(':', $time2);

        $res1 = mktime($hour1, $min1, $sec1, $month1, $day1, $year1);
        $res2 = mktime($hour2, $min2, $sec2, $month2, $day2, $year2);

        $diff = ($res2 - $res1) / $unit;
        $diff = round($diff, 2);
        //~ echo "Debug => diff: $diff<br />";

        return $diff;
    }

    function ShowWaitDiv()
    {
        ?>
        <div id="wait">
            <img src="<?php echo IMG_PATH; ?>Varie/wait.gif" alt="wait" />
            <br />
            <label>Aggiornamento pagina in corso, attendere...</label>
        </div>
        <?php
    }

    function ShowCreditsBar($ShowTime = false)
    {
        global $start;
        ?>

        <a onclick="Javascript: window.open('http://gnu.it', '_blank');
                void(0);">
            <img src="<?php echo IMG_PATH; ?>Software/gnubanner-2.png"
                 alt="GNU Project" title="Progetto GNU" />
        </a>

        <a onclick="Javascript: window.open('http://it.wikipedia.org/wiki/Linux', '_blank');
                void(0);">
            <img src="<?php echo IMG_PATH; ?>Software/linux.gif"
                 alt="Linux" title="GNU/Linux" />
        </a>

        <a onclick="Javascript: window.open('http://www.debian.org', '_blank');
                void(0);">
            <img src="<?php echo IMG_PATH; ?>Software/debian.png" alt="debian.org"
                 title="Debian - Debian è un sistema operativo (OS) libero (free) per il tuo computer." />
        </a>
        <a onclick="Javascript: window.open('http://www.apache.org', '_blank');
                void(0);">
            <img src="<?php echo IMG_PATH; ?>Software/apache.png" alt="apache.org" title="Apache" />
        </a>
        <a onclick="Javascript: window.open('http://www-it.mysql.com/', '_blank');
                void(0);">
            <img src="<?php echo IMG_PATH; ?>Software/mysql.png" alt="mysql.com" title="MySql" />
        </a>
        <a onclick="Javascript: window.open('http://it.php.net/', '_blank');
                void(0);">
            <img src="<?php echo IMG_PATH; ?>Software/php.png" alt="php.net" title="Php" />
        </a>
        <a onclick="Javascript: window.open('http://www.mozillaitalia.it/archive/index.html#p1', '_blank');
                void(0);">
            <img src="<?php echo IMG_PATH; ?>Software/firefox3.gif" alt="firefox.gif" title="Firefox"/>
        </a>
        <a onclick="Javascript: window.open('http://validator.w3.org/check?uri=referer', '_blank');
                void(0);">
            <img src="<?php echo IMG_PATH; ?>Validator/valid-xhtml11.png"
                 alt="Valid XHTML 1.0 Strict"  />
        </a>
        <a onclick="Javascript: window.open('http://jigsaw.w3.org/css-validator/', '_blank');
                void(0);">
            <img src="<?php echo IMG_PATH; ?>Validator/vcss.gif" alt="CSS Valido!" />
        </a>

        <?php if ($ShowTime == 1) {
?>
            <label class="note">
                Pagina elaborata in <?php echo (time() - $start); ?> secondi.
            </label>
            <?php
}
    }

    function DrawClosePopup()
    {
        ?>
        <tr>
            <td style="text-align: right;" colspan="4">
                <a onclick="Javascript: ClosePopup();
                        void(0);" title="Chiudi Finestra">
                    [ <label class="err" style="cursor: pointer;">Chiudi</label> ]
                </a>
            </td>
        </tr>
        <?php
    }

    function DrawHeader($User, $Group, $PageTitle)
    {
        ?>
        <div id="header">
            <table style="width: 100%;" class="header">
                <tr>
                    <td style="text-align: right; width: 4em;">
                        <label>Utente:</label>
                    </td>

                    <td style="width: 25%;">
                        <a onclick="Javascript: ShowFormDiv('frm_passwd.php', '100px', '100px', '500px', '200px', 'popup');
                                void(0);"
                           title="Click per modificare i dati utente">
                                <?php echo $User; ?>
                            <img src="<?php echo IMG_PATH; ?>Links/key.png" alt="key" />
                        </a>
                    </td>

                    <th rowspan="2">
                        <?php
                        if ($PageTitle != "") {
                            echo $PageTitle;
                        }
                        ?>
                    </th>

                </tr>
                <tr>
                    <td style="text-align: right;">
                        <label>Gruppo:</label>
                    </td>
                    <td>
                        <label class="ok">
                            <?php echo $Group; ?>
                        </label>
                    </td>
                </tr>
            </table>
        </div>
        <?php
    }

    function DrawControl(
        $Type,
        $Name,
        $Label = "",
        $Pattern = "",
        $Value = "",
        $CheckValue = "",
        $Size = "10em;",
        $MaxLenght = "10",
        $JavaScript = "",
        $DataArray = "",
        $ID = "",
        $Field = "",
        $Style = "",
        $RowHeight = ""
    ) {
        //~ echo "<pre>Value";
        //~ print_r($Value);
        //~ echo "</pre>";
        //~ echo "[Debug]: Name: $Name <br />";
        //~ echo "[Debug]: Value: $Value <br />";
        //~ echo "[Debug]: Value[$Name]: " . $Value[$Name] . "<br />";
        //~ echo "[Debug]: ID: $ID, Field: $Field <br />";

        if (!$Type) {
            echo "[Debug][DrawControl]: Errore, non e' stato fornito il tipo per il controllo, esco. <br />";
            return;
        }

        if (!$Name) {
            echo "[Debug][DrawControl]: Errore, non e' stato fornito il nome per il controllo, esco. <br />";
            return;
        }

        if ($Label != "") {
            $Placeholder = " placeholder=\"$Label\" ";
        }

        if ($Pattern != "") {
            $Pattern = " required pattern=\"$Pattern\" ";
        }

        if (is_numeric($MaxLenght)) {
            $MaxLenght = " maxlength=\"$MaxLenght\" ";
        } else {
            //~ echo "[Debug][DrawControl]: Errore, il valore '$MaxLenght' di MaxLenght non &egrave; valido, esco. <br />";
            $MaxLenght = "";
        }

        if (trim($Size) == "") {
            echo "[Debug][DrawControl]: Errore, il valore '$Size' di Size non &egrave; valido, esco. <br />";
            return;
        }

        if (!isset($Value)) {
            $Value = "";
        }

        if (!isset($DataArray)) {
            $DataArray = array();
        }


        switch ($Type) {
            case 'text':
                ?>
                <input name="<?php echo $Name; ?>"
                       type="text"
                        <?php echo " " . $Placeholder . " " . $Pattern; ?>
                       title="<?php echo $Label; ?>"
                        <?php echo $MaxLenght; ?>
                       style="width: <?php echo $Size; ?>;"
                       value="<?php
                        if (isset($Value[$Name])) {
                            echo stripslashes($Value[$Name]);
                        }
                        ?>"
                        <?php echo " " . $JavaScript . " "; ?>
                       />

                <?php
                break;
            case 'radio':
            case 'checkbox':
                ?>
                <input name="<?php echo $Name; ?>"
                       type="<?php echo $Type; ?>"
                        <?php echo $Pattern; ?>
                       value="<?php echo $CheckValue; ?>"
                       title="<?php echo $Label; ?>"
                        <?php
                        if (isset($Value[$Name]) && ($Value[$Name] == $CheckValue)) {
                            echo "checked=\"checked\"";
                        }
                        ?> />
                        <?php
                        echo $Label;
                break;

            case 'select':
            case 'selectall':
                ?>
                <select name="<?php echo $Name; ?>"
                        title="<?php echo $Label; ?>"
                        style="width: <?php echo $Size; ?>;"
                        <?php echo " " . $JavaScript . " "; ?> >

                    <?php if ($Type == "select") {
?>
                        <option value="-"><?php echo $Label; ?></option>
                        <option value="-"></option>
                    <?php
} else {
?>
                        <option value="%">Nessun filtro</option>
                        <option value="%"></option>
                        <?php
}

foreach ($DataArray as $key => $field) {
    ?>
    <option value="<?php echo $field[$ID]; ?>"
            <?php
            if (isset($Value[$Name]) && ($Value[$Name] == $field[$ID])) {
                echo "selected=\"selected\"";
            }
            ?> >
                <?php echo $field[$Field]; ?>
                        </option>
                    <?php
} ?>
                </select>
                <?php
                break;

            case 'textarea':
                ?>
                <textarea name="<?php echo $Name; ?>" <?php echo " " . $Placeholder . " " . $Pattern . " " . $JavaScript . " "; ?> title="<?php echo $Label; ?>" style="width: <?php echo $Size; ?>; height: 3em;"><?php
                if (isset($Value[$Name])) {
                    echo stripslashes($Value[$Name]);
                }
                    ?></textarea>
                <?php
                break;
            case 'hidden':
                ?>
                <input name="<?php echo $Name; ?>"
                       type="hidden"
                       value="<?php
                        if (isset($Value[$Name])) {
                            echo stripslashes($Value[$Name]);
                        }
                        ?>"
                       />
                        <?php
                break;

            default:
                echo "[Debug][DrawControl]: Errore, tipo di controllo '$Type' non riconosciuto, esco. <br />";
                return;
        }
    }

           /**
            * Funzione che restituisce i gruppi e le voci di menu visibili in base ai permessi:
            */
    function getVisibleMenu($Permission)
    {
        if (!isset($Permission)) {
            // se non e' stato specificato un permesso non restituire nulla.
            return array(null, null);
        }

        $GruppiMenu = $this->GetRows(
            "*",
            "tab_menu",
            "perm like '%$Permission%'",
            "gruppo",
            "ordine_gruppo, titolo_gruppo",
            1
        );
        //$VociMenu = $this->GetRows("*", "tab_menu",
        //    "gruppo = '" . $field_group['gruppo'] . "'", "", "ordine, titolo", 1);
        $VociMenu   = $this->GetRows(
            "*",
            "tab_menu",
            "perm like '%$Permission%'",
            "",
            "ordine, titolo",
            1
        );

        return array($GruppiMenu, $VociMenu);
    }

    function DrawMenu($BasePage = "home.php", $SubVoci = true)
    {
        if (!isset($_GET['gruppo'])) {
            $_GET['gruppo'] = "Home";
        }

        list( $GruppiMenu, $VociMenu ) = $this->getVisibleMenu($_SESSION['id_tipo']);
        ?>

        <ul id="horizontalmenu">
     <li>
         <a href="?act=welc">
             <img src="<?php echo IMG_PATH; ?>Links/home.png" alt="Home" title="Home" />
             Home
                </a>
            </li>

            <?php foreach ($GruppiMenu as $key_group => $field_group) {
?>
                <li <?php
                if ($field_group['gruppo'] == $_GET['gruppo']) {
                    echo "style=\"background-color: #DFE6A1;\"";
                }
                ?>>

                    <a <?php
                    if ($SubVoci == false) {
                        echo " href=\"$BasePage?gruppo=" . $field_group['gruppo'] . "\" ";
                    }
                    ?>
                        title="<?php echo $field_group['titolo_gruppo']; ?>">
                        <img src="<?php echo IMG_PATH; ?>Links/down.png" alt="Down" />
                        <?php echo $field_group['titolo_gruppo']; ?>
                    </a>

                    <?php if ($SubVoci == true) {
?>
                        <ul>
                            <?php
                            foreach ($VociMenu as $key_voci => $field_voci) {
                                if ($field_group['gruppo'] == $field_voci['gruppo']) {
                                    ?>
                                    <li>
                                        <a href="<?php
                                        echo htmlspecialchars(
                                            $field_voci['link'],
                                            ENT_QUOTES,
                                            'UTF-8'
                                        );
                                        ?>">
                                                <?php
                                                echo htmlspecialchars(
                                                    $field_voci['titolo'],
                                                    ENT_QUOTES,
                                                    'UTF-8'
                                                );
                                                ?>
                                        </a>
                                    </li>
                                    <?php
                                }
                            }
                            ?>
                        </ul>
                    <?php
} ?>
                </li>
            <?php
} ?>

            <li>
                <a href="logout.php">
                    <img src="<?php echo IMG_PATH; ?>Links/exit.png" alt="Esci" title="Esci" />
                    Esci
                </a>
            </li>
        </ul>

        <select id="horizontalmenumini" onchange="Javascript: go(this.value);" >
            <option value="home.php">Home</option>
            <?php foreach ($GruppiMenu as $key_group => $field_group) {
?>
                <optgroup label="<?php echo $field_group['titolo_gruppo']; ?>">
                    <?php
                    foreach ($VociMenu as $key_voci => $field_voci) {
                        if ($field_group['gruppo'] == $field_voci['gruppo']) {
                            ?>
                            <option value="<?php
                            echo htmlspecialchars(
                                $field_voci['link'],
                                ENT_QUOTES,
                                'UTF-8'
                            );
                            ?>"
                                    <?php
                                    if ($_GET['act'] == $field_voci['act']) {
                                        echo "selected=\"\selected\"";
                                    }
                                    ?> >
                                        <?php
                                        echo htmlspecialchars(
                                            $field_voci['titolo'],
                                            ENT_QUOTES,
                                            'UTF-8'
                                        );
                                        ?>
                            </option>
                            <?php
                        }
                    }
                    ?>
                </optgroup>
            <?php
} ?>
            <option value="logout.php">Esci</option>
        </select>
        <?php
    }

    function GetMax($table, $col, $where = '')
    {
        $Ar_Max = $this->GetRows("Max(" . $col . ") as mass", $table, $where);
        $return = $Ar_Max[0]['mass'] + 1;
        return $return;
    }

    function Login(
        $UserName,
        $Password,
        $SessionPrefix,
        $Redirect = "home.php?act=welc"
    ) {
        // Pulitura campo user:
        $UserName = htmlspecialchars($UserName, ENT_QUOTES, 'UTF-8');

        $UserAgent = get_browser(null, true);

        // Architecture 32 or 64 bit.
        if (($UserAgent['win32'] == "1") && ($UserAgent['win64'] == "")) {
            $UserAgent['arch'] = "32";
        } else {
            $UserAgent['arch'] = "64";
        }

        $_SESSION['platform'] = $UserAgent['platform'];
        $_SESSION['browser']  = $UserAgent['browser'];
        $_SESSION['version']  = $UserAgent['version'];
        $_SESSION['arch']     = $UserAgent['arch'];

        // Rimuovo dal db tutte le sessioni salvate piu' vecchie di 1 ora:
        /*
          $q_old_ses = "DELETE FROM tab_session WHERE data <= date_sub('"
          . date('Y-m-d H:i:s') . "', interval 1 HOUR);";
          $r_old_ses = mysqli_query($q_old_ses, $db) or die (StampaErr($q_old_ses));
         */

        $Where = "u.user = '$UserName' AND u.pass = '$Password'"
                . " AND u.tipo = t.id_tipo and u.stato = '0'";
        $Users = $this->GetRows(
            "*",
            "tab_utenti u, tab_tipi t",
            $Where,
            "",
            "",
            1
        );

        if (count($Users) == 1) {
            $_SESSION['id_user']                = $Users[0]['id_user'];
            $_SESSION[$SessionPrefix . '_user'] = $UserName;
            $_SESSION[$SessionPrefix . '_sess'] = session_id();
            $_SESSION['desc_user']              = $Users[0]['desc_user'];
            $_SESSION['id_tipo']                = $Users[0]['id_tipo'];
            $_SESSION['tipo']                   = $Users[0]['tipo'];
            $_SESSION['id_city']                = $Users[0]['id_city'];

            /* Inserisco nel db i dati sul login effettuato: */
            $Valori = array('session_id' => session_id(),
                'id_user'    => $Users[0]['id_user'],
                'data'       => date('Y-m-d H:i:s'),
                'ip'         => $_SERVER['REMOTE_ADDR'],
                'action'     => 'LogIn',
                'platform'   => $UserAgent['platform'],
                'browser'    => $UserAgent['browser'],
                'version'    => $UserAgent['version'],
                'arch'       => $UserAgent['arch']);

            $Status = $this->SaveRow($Valori, "", "tab_session");

            // Genero il menu per l'utente in base ai permessi:
            //~ $_SESSION['MenuArray'] = $this->GetRows("*", "tab_menu", "perm like '%"
            //~ . $_SESSION['id_tipo'] . "%'", "", "gruppo, titolo", 1);

            if ($Redirect != "") {
                ?>
                <label class="ok">Login effettuato, redirect in corso...</label>
                <script type="text/javascript">
                    document.location.href = "<?php echo $Redirect; ?>";
                </script>
            <?php
            } ?>
            <?php
        } else {
            $Result              = '<label class="err">Errore, nome utente o password errati.</label>';
            $_SESSION['id_user'] = 1;
            $this->SessionLog("Accesso fallito per l'utente '$UserName' con password '$Password'");
            return $Result;
        }
    }

    function Logout($SessionPrefix, $Redirect = "index.php")
    {
        /* Inserisco nel db i dati sul login effettuato: */
        $Valori = array('session_id' => session_id(),
            'id_user'    => $_SESSION['id_user'],
            'data'       => date('Y-m-d H:i:s'),
            'ip'         => $_SERVER['REMOTE_ADDR'],
            'action'     => 'LogOut',
            'platform'   => $_SESSION['platform'],
            'browser'    => $_SESSION['browser'],
            'version'    => $_SESSION['version'],
            'arch'       => $_SESSION['arch']);
        $Status = $this->SaveRow($Valori, "", "tab_session");

        unset($_SESSION[$SessionPrefix . "_user"]);
        unset($_SESSION[$SessionPrefix . "_sess"]);
        session_destroy();
        ?>

        <label class="ok">LogOut effettuato, redirect in corso...</label>

        <script type="text/javascript">
            document.location.href = '<?php echo $Redirect; ?>';
        </script>
        <?php
    }

    function ExpandSearch($string)
    {
        // Sostituisce agli spazi nella ricerca con "%"
        //~ $temp_val = explode(" ", $string);
        //~ $string = join("%", $temp_val);

        $string = str_replace(" ", "%", $string);

        return $string;
    }

    function SplitString($string)
    {
        $len    = strlen($string) - 1;
        $result = array();
        for ($i = 0; $i <= $len; $i++) {
            array_push($result, substr($string, $i, 1));
        }

        return $result;
    }

    function FindStr($stringa, $inizio, $fine, $offset, $foffset)
    {
        $v_inizio = strpos($stringa, $inizio);
        if ($v_inizio !== false) {
            $v_inizio += strlen($inizio) - $offset;
            $v_fine  = strpos($stringa, $fine) - $foffset;
            $trovato = ucfirst(substr($stringa, $v_inizio, $v_fine - $v_inizio));
        } else {
            $trovato = "error";
        }

        return $trovato;
    }

    function ModoRicerca($val, $modo = "tutto")
    {
        switch ($modo) {
            case "esatto":
                $iniz = "";
                $fine = "";
                break;
            case "inizio":
                $iniz = "";
                $fine = "%";
                break;
            case "fine":
                $iniz = "%";
                $fine = "";
                break;
            case "tutto":
                $iniz = "%";
                $fine = "%";
                break;
        }

        $valore = $iniz . $val . $fine;
        return $valore;
    }

    function CheckAuth($id, $string)
    {
        $perms = explode(";", $string);

        if (in_array($id, $perms)) {
            $flag[0] = "checked=\"checked\"";
            $flag[1] = 0;
        } else {
            $flag[1] = 1;
        }

        return $flag;
    }

    function ValidateQuery($String)
    {
        $return_string = "";

        echo "[Debug]: String: $String <br />";
        if (preg_match_all("('(.*)' )", $String, $Match)) {
            echo "<pre> MATCH ";
            print_r($Match);
            echo "</pre>";
        }
        /*
          preg_match
          (.*) = '(.*)' .*

          ((.*) (=|!=|like|LIKE|<>|<=|>=) '(.*)')
         */
    }

    function MultiString($Text, $Separator, $Glue)
    {
        $tmp_array = explode($Separator, $Text);

        $string = "";
        foreach ($tmp_array as $key => $field) {
            if ($key != count($tmp_array) - 1) {
                $string .= trim($field) . $Glue;
            } else {
                $string .= trim($field);
            }
        }

        return $string;
    }

    function FormBuilder($Fields)
    {
        /*
         * Struttura array label/placeholder,
         * nome(campo e controllo),
         * pattern+required,
         */
        ;
    }

    /*
     *
     * name: Query
     * @param
     * @return
     */

    function Query($QueryString, $debug = 1)
    {
        $Results = array();
        //~ $q = addslashes(stripslashes($QueryString));
        $q       = $QueryString;

        if ($debug != 1) {
            echo "[Debug][Query]: $q <br />";
        }

        switch ($this->BackEnd) {
            case "mysql":
                $r = mysqli_query($this->db, $q) or
                die($this->StampaErr(
                    $q,
                    '[Query]'
                ));
                $n = mysql_affected_rows($this->db);

            if (is_resource($r)) {
                $Results = mysql_fetch_assoc($r);
                mysql_free_result($r);
            }
                break;

            case "postgresql":
                $r = pg_query($this->db, $q) or
                die($this->StampaErr(
                    $q,
                    '[Query]'
                ));

            if ($r) {
                $n       = pg_num_rows($r);
                $Results = pg_fetch_all($r);
                pg_free_result($r);
            }
                break;
        }

        $this->SessionLog("[Debug][Query]: $q");
        //$this->LastResultArray = $Results;
        return $Results;
    }

    function DrawMenuGroup(
        $Spacer = "&nbsp;&nbsp;&rsaquo;&rsaquo;",
        $ImageGroup = 0,
        $ImageLinks = 1
    ) {
        $GruppiMenu = $this->GetRows(
            "distinct(gruppo), image_group",
            "tab_menu",
            "",
            "",
            "gruppo",
            1
        );

        //~ echo "<pre>";
        //~ print_r($GruppiMenu);
        //~ echo "</pre>";

        foreach ($GruppiMenu as $key_group => $field_group) {
            $VociMenu = $this->GetRows(
                "*",
                "tab_menu",
                "gruppo = '"
                . $field_group['gruppo'] . "'",
                "",
                "titolo",
                1
            );
            ?>

            <details>
                <summary>
                    <?php if ($ImageGroup == 0) {
?>
                        <img src="<?php echo $field_group['image_group']; ?>" alt="img" />
                        <?php
}
                    echo $field_group['gruppo'];
                    ?>
                </summary>

                <table style="width: 100%;">
                    <?php foreach ($VociMenu as $key => $field) {
?>
                        <tr>
                            <td title="<?php echo $field['tip']; ?>" >
                                <?php if ($ImageLinks != 1) {
?>
                                    <img src="<?php $field['image_link'] ?>" alt="img" />
                                <?php
} ?>

                                <?php echo $Spacer; ?>
                                <a href="<?php echo $field['link']; ?>">
                                    <?php echo $field['titolo']; ?>
                                </a>
                            </td>
                        </tr>
                    <?php
} ?>
                </table>
            </details>
            <br />
            <?php
        }
    }

    function CheckSession(
        $SessionPrefix,
        $ValidRedirect = "home.php?act=welc",
        $InvalidRedirect = "index.php",
        $OnlyCheck = false
    ) {
        /*
         *
         * name: CheckSession
         * @param: SessionPrefix, ValidRedirect, InvalidRedirect, OnlyCheck
         * @return: true or false and/or redirect
         */

        if (isset($_SESSION[$SessionPrefix . '_user']) && isset($_SESSION[$SessionPrefix . '_sess']) && ($_SESSION[$SessionPrefix . '_user'] != "") && ($_SESSION[$SessionPrefix . '_sess'] != "")) {
            //echo "Sessione valida redirect su: '$ValidRedirect'<br />";
            if ($OnlyCheck == false) {
                ;
            }
            return true;
        } else {
            //echo "Sessione NON valida, redirect su: '$InvalidRedirect'<br />";
            if ($OnlyCheck == false) {
                ?>
                <script type="text/javascript">
                    //alert("Sessione non valida redirect su <?php echo $InvalidRedirect; ?>");
                    document.location.href = '<?php echo $InvalidRedirect; ?>';
                </script>
                <?php
            }
            return false;
        }
    }

    function first_n_words($string, $int = 5, $dots = ' ...')
    {
        preg_match("/^(\W*\w+){0,$int}/", $string, $match);
        return $match[0] . $dots;
    }

    function file_size($size)
    {
        $filesizename = array(" Bytes", " KB", " MB", " GB", " TB", " PB", " EB", " ZB", " YB");
        return $size ? round(
            $size / pow(1024, ($i            = floor(log(
                $size,
                1024
            )))),
            2
        ) . $filesizename[$i] : '0 Bytes';
    }

    // End Class
}

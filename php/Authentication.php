<?php

namespace clagiordano\weblibs\php;

use clagiordano\weblibs\php\AuthenticationConfig;
use clagiordano\weblibs\php\User;
use clagiordano\weblibs\php\Logger;

/**
 * Classe per l'autenticazione, login, logout, inclusioni ed altre
 * funzioni di sicurezza.
 *
 * @version  1.2
 * @author   Claudio Giordano <claudio.giordano@autistici.org>
 * @since    2010
 * @category Security
 */
class Authentication
{
    private $loginStatus       = false;
    private $loginMessage      = null;
    private $dataBase          = null;
    private $sessionNamePrefix = null;
    private $authLogger        = null;
    private $sessionData       = null;

    const SESSION_SUFFIX = "_session";
    const PASSWORD_FIELD_FORMAT_INVALID = 0;
    const PASSWORD_FIELD_FORMAT_OLD = 1;
    const PASSWORD_FIELD_SIZE_OLD   = 32;
    const PASSWORD_FIELD_FORMAT_NEW = 2;
    const PASSWORD_FIELD_SIZE_NEW   = 60;

    /**
     *
     * @param \weblibs\php\Database $dataBase
     * @param type $sessionNamePrefix
     */
    public function __construct(Database $dataBase, $sessionNamePrefix)
    {
        /**
         * TODO implementare funzione di verifica struttura tabella ed
         * TODO eventuale creazione/alterazione delle tabelle
         */
        $this->dataBase          = $dataBase;
        $this->sessionNamePrefix = $sessionNamePrefix;
        $this->authLogger        = new Logger($this->dataBase->getDbConnection());
    }

    /**
     * TODO Funzione per la verifica e successiva inclusione di pagine esterne.
     *
     * @param type $Action
     * @param type $DefaultPage
     * @param type $DefaultTitle
     * @param type $Group
     */
//    public function includePage($Action, $DefaultPage, $DefaultTitle, $Group)
//    {
//        /**
//         * FIXME
//         */
//        // richiama checkInclude
//        $this->checkInclude($Action, $DefaultPage, $DefaultTitle, $Group);
//    }

    /**
     * TODO Verifica che sia consentita l'inclusione della pagina richiesta.
     */
//    private function checkInclude($Action, $DefaultPage, $DefaultTitle, $Group)
//    {
//        $Action = addslashes(stripslashes($Action));
//        $Pages  = $this->dataBase->getRows(
//                "*", "tab_menu", "act = '$Action' AND perm LIKE '%$Group%'", "",
//                "", false
//        );
//
//        if (count($Pages) == 1) {
//            $Page                   = $Pages[0]['page'];
//            $_SESSION['titolo_pag'] = $Pages[0]['titolo_pag'];
//        } else {
//            $Page                   = $DefaultPage;
//            $_SESSION['titolo_pag'] = $DefaultTitle;
//        }
//
//        /* FIXME
//          $this->DrawHeader($_SESSION['desc_user'], $_SESSION['tipo'],
//          $_SESSION['titolo_pag']); */
//
//        return $Page;
//    }

    /**
     * Effettua il login della sessione
     *
     * @param type $userName
     * @param type $passWord
     */
    public function logIn($userName, $passWord)
    {
        /**
         * Normalizzo il campo username e creo l'oggetto in base ai primi 2 campi
         */
        $User = new User(trim(strtolower($userName)), $passWord);

        /**
         * Check user and crypt method for password
         */
        $this->checkAuthentication($User, $passWord);

        if ($this->loginStatus === true) {
            // aggiungo i dati in sessione:
            $this->setSessionData($User);
        } else {
            $this->authLogger->notice(
                    "{function}: Accesso fallito per l'utente '{user}' con password '{pass}'",
                    array('function' => __METHOD__, 'user' => $userName, 'pass' => $passWord)
            );
        }

        return $this->loginStatus;
    }

    /**
     * Effettua il logout della sessione in base al prefisso di sessione
     * e pulisce la memoria
     */
    public function logOut()
    {
        /* Loggo l'azione */
        $userSession = unserialize($_SESSION[$this->sessionNamePrefix . self::SESSION_SUFFIX]);

        $this->authLogger->notice(
            "{function}: {user} ha eseguito il logout.",
            [
                'function' => __METHOD__,
                'user'     => $userSession->getUsername()
            ]
        );

        /**
         * Unset all session data and objects
         */
        unset($_SESSION[$this->sessionNamePrefix . self::SESSION_SUFFIX]);
        $this->loginStatus = false;

        if (session_id()) {
            /**
             * If isset a started session, destroy it
             */
            session_destroy();
        }
    }

    /**
     * TODO Verifica i permessi per la singola pagina per il gruppo
     *
     * @param type $ID
     * @param type $String
     * @return int
     */
//    private function checkAuth($ID, $String)
//    {
//        $perms = explode(";", $String);
//
//        if (in_array($ID, $perms)) {
//            $flag[0] = "checked=\"checked\"";
//            $flag[1] = 0;
//        } else {
//            $flag[1] = 1;
//        }
//
//        return $flag;
//    }

    /**
     * Validate user session
     *
     * @param type $sessionPrefix
     * @return boolean true or false and/or redirect
     */
    public function checkSession()
    {
        $checkStatus = false;

        if (isset($_SESSION[$this->sessionNamePrefix . self::SESSION_SUFFIX])) {
            /** There is a session, check if is a User object */
//            $checkStatus = is_a(
//                $_SESSION[$sessionPrefix . self::SESSION_SUFFIX],
//                'clagiordano\weblibs\php\User'
//            );

            if ($this->getSessionData()->getUserId() !== null) {
                $checkStatus = true;
            }
        }

        return $checkStatus;
    }

    /**
     * Validate user session and redirect
     *
     * @param type $validRedirect
     * @param type $invalidRedirect
     */
    public function checkSessionAndRedirect(
            $validRedirect = "home.php?act=welc",
            $invalidRedirect = "index.php")
    {
        if ($this->checkSession($this->sessionNamePrefix)) {
           $redirectToPage = $validRedirect;
        } else {
            $redirectToPage = $invalidRedirect;
        }

        ?>
            <script type="text/javascript">
                document.location.href = '<?php echo $redirectToPage; ?>';
            </script>
        <?php
    }

    /**
     * Funzione che verifica l'algoritmo di crittaggio della password
     * in base alla lunghezza della stessa e verifica se e' corretta.
     *
     * Restituisce true o false rispettivamente se è corretta o errata
     * @param User   $User
     * @param string $FormPassword
     * return
     */
    private function checkPasswordFormat(User $User, $FormPassword)
    {
        //echo "[DEBUG][" . __METHOD__ . "]: verifico il formato della pass '" . $User->getPassword() . "'<br />";

        /**
         * Se la password è maggiore di 32 caratteri allora è memorizzata
         * nel nuovo formato HASH BLOWFISH introdotto nella versione
         * 5 della classe Database e Auth 1.0, altrimenti e' nel vecchio formato
         * MD5 e deve essere convertita.
         */
        if (strlen($User->getPassword()) > self::PASSWORD_FIELD_SIZE_OLD) {
            /**
             * La password è già nel nuovo formato, eseguo solo la verifica.
             */
            if (password_verify($FormPassword, $User->getPassword())) {
                $this->loginStatus = true;
                // cripto la pass sull'oggetto utente
                $User->setPassword(password_hash(
                                $User->getPassword(), PASSWORD_BCRYPT
                ));
            }
        } elseif (strlen($User->getPassword()) == self::PASSWORD_FIELD_SIZE_OLD) {
            /**
             * La password è nel vecchio formato, eseguo la verifica e l'aggiornamento
             * della password salvata sul database ed eventualmente migro il campo
             * estendendolo.
             */
            if (md5($FormPassword) == $User->getPassword()) {
                //echo "[DEBUG][" . __METHOD__ . "]: La password MD5 corrisponde. <br />";
                $this->loginStatus = true;

                /**
                 * Check to migrate user password
                 */
                $this->migrateUserPassword($User,
                                           password_hash($FormPassword,
                                                         PASSWORD_BCRYPT));
            }
        }
    }

    /**
     * Validate table user's field
     *
     * @return int
     */
    private function validatePasswordFieldTable()
    {
        /**
         * Get table field information
         */
        $this->dataBase->runQuery(
                "SELECT
                CHARACTER_MAXIMUM_LENGTH AS fieldLength
            FROM
                information_schema.COLUMNS
            WHERE
                TABLE_SCHEMA = '{$this->dataBase->getDatabaseName()}'
            AND
               TABLE_NAME = '" . AuthenticationConfig::TABLE_USERS . "'
            AND
                COLUMN_NAME = '" . AuthenticationConfig::TABLE_USERS_PASSWORD . "'
            "
        );

        if ($this->dataBase->getResults()[0]['fieldLength'] == self::PASSWORD_FIELD_SIZE_OLD) {
            $validationStatus = self::PASSWORD_FIELD_FORMAT_OLD;
        } elseif ($this->dataBase->getResults()[0]['fieldLength'] > self::PASSWORD_FIELD_SIZE_OLD) {
            $validationStatus = self::PASSWORD_FIELD_FORMAT_NEW;
        } else {
            $validationStatus = self::PASSWORD_FIELD_FORMAT_INVALID;
        }

        return $validationStatus;
    }

    /**
     * Check user grant access
     *
     * TODO: this is a placeholder method
     */
    private function checkUserDbGrantAlter()
    {
        return false;
    }

    /**
     * Check and convert user's password field on users table for new format.
     * TODO: Pre check for alter command permission
     * @param User $User
     */
    private function migrateUserPassword(User $User, $newPassword)
    {
        $updatePassword = false;

        switch ($this->validatePasswordFieldTable()) {
            case self::PASSWORD_FIELD_FORMAT_INVALID:
            case self::PASSWORD_FIELD_FORMAT_OLD:
                if ($this->checkUserDbGrantAlter()) {
                    $this->dataBase->runQuery(
                            "ALTER TABLE `" . AuthenticationConfig::TABLE_USERS .
                            "` MODIFY COLUMN `" . AuthenticationConfig::TABLE_USERS_PASSWORD
                            . "` CHAR(" . self::PASSWORD_FIELD_SIZE_NEW . ") NOT NULL"
                    );

                    $updatePassword = true;
                }
                break;

            default:
                // Field already in new format
                $updatePassword = true;
                continue;
        }


        if ($updatePassword) {
            /**
             * Change encription for user password
             */
            $User->setPassword($newPassword);
            $this->dataBase->updateRow(
                    [
                AuthenticationConfig::TABLE_USERS_PASSWORD => $User->getPassword()
                    ], [], AuthenticationConfig::TABLE_USERS,
                    AuthenticationConfig::TABLE_USERS_ID . " = '" . $User->getUserId() . "'"
            );
        }
    }

    /**
     * Verifica presenza utente e algoritmo di crittaggio password
     */
    private function checkAuthentication(User $User, $FormPassword)
    {
        /**
         * Verifica la presenza dell'utente sul database, in base al solo username
         * e stato 0 attivo, 1 disattivato
         */
        $this->dataBase->getRows(
                "*", AuthenticationConfig::TABLE_USERS,
                AuthenticationConfig::TABLE_USERS_USER . " = '" . $User->getUsername() . "' AND stato = '1'"
        );

        if (count($this->dataBase->getResults()) == 1) {
            /**
             * Valid user, check its password,
             * init user object whith database data
             */
            $User->setAll($this->dataBase->Results[0]);

            // verifico l'algoritmo di crittaggio della password
            $this->checkPasswordFormat($User, $FormPassword);

            if ($this->loginStatus === true) {
                $this->loginMessage = AuthenticationConfig::AUTH_OK_LOGIN;
            }
        } else {
            $this->loginStatus  = false;
            $this->loginMessage = AuthenticationConfig::AUTH_ERROR_LOGIN;
        }
    }

    /**
     * TODO Funzione che restituisce i gruppi e le voci di menu visibili in base ai permessi:
     */
//    private function getVisibleMenu($Permission)
//    {
//        $GruppiMenu = null;
//        $VociMenu   = null;
//
//        if (isset($Permission)) {
//            $GruppiMenu = $this->getRows(
//                    "*", \AuthenticationConfig::TABLE_MENU,
//                    \AuthenticationConfig::TABLE_MENU_PERMISSION . " like '%$Permission%'",
//                    \AuthenticationConfig::TABLE_MENU_GROUP,
//                    \AuthenticationConfig::TABLE_MENU_GROUP_ORDER . ", "
//                    . \AuthenticationConfig::TABLE_MENU_GROUP_TITLE, false
//            );
//            //$VociMenu = $this->getRows("*", "tab_menu",
//            //    "gruppo = '" . $field_group['gruppo'] . "'", "", "ordine, titolo", 1);
//            $VociMenu   = $this->getRows(
//                    "*", \AuthenticationConfig::TABLE_MENU,
//                    \AuthenticationConfig::TABLE_MENU_PERMISSION . " like '%$Permission%'",
//                    "", "ordine, titolo", false
//            );
//        }
//
//        // se non e' stato specificato l'argomento restituisce 2 valori null
//        return array($GruppiMenu, $VociMenu);
//    }

    /**
     * Serialize and set sessionData into session
     *
     * @param User $sessionData
     * @return \clagiordano\weblibs\php\Authentication
     */
    private function setSessionData(User $sessionData)
    {
        $this->sessionData = serialize($sessionData);
        $_SESSION[$this->sessionNamePrefix . self::SESSION_SUFFIX] = $this->sessionData;

        return $this;
    }

    /**
     * Get get and unserialize User object from session
     *
     * @param type $sessionPrefix
     * @return type
     */
    private function getSessionData()
    {
        if (isset($_SESSION[$this->sessionNamePrefix . self::SESSION_SUFFIX])) {
            $this->sessionData = unserialize($_SESSION[$this->sessionNamePrefix . self::SESSION_SUFFIX]);
        } else {
            $this->sessionData = null;
        }

        return $this->sessionData;
    }

    /**
     * Return current user object from session if present.
     *
     * @return \User\null
     */
    public function getUser()
    {
        $userObject = false;

        if (!is_null($this->sessionNamePrefix)) {
            $userObject = $this->getSessionData();
        }

        return $userObject;
    }
}

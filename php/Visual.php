<?php

namespace clagiordano\weblibs\php;

/**
 *
 * @author Claudio Giordano <claudio.giordano@autistici.org>
 */
interface Visual
{
    public function render();
}

<?php

namespace clagiordano\weblibs\php;

/**
 * Classe per la creazione ed elaborazione di immagini con varie funzioni
 * di ritaglio, conversione e creazione tramite testo con esportazione in
 * formato base64.
 *
 * @version  1.3
 * @author   Claudio Giordano <claudio.giordano@autistici.org>
 * @since    2014-04-04
 * @category Image
 * @package  weblibs
 */
final class Image
{
    /**
     * Configurazione dimensioni immagini per l'avatar ( in pixel )
     */
    const IMG_THUMB_WIDTH  = 80;
    const IMG_THUMB_HEIGHT = 80;
    const IMG_FULL_WIDTH   = 250;
    const IMG_FULL_HEIGHT  = 250;

    /**
     * Configurazione dimensioni immagini per l'agency ( in pixel )
     */
    const IMG_AG_THUMB_WIDTH  = 120;
    const IMG_AG_THUMB_HEIGHT = 80;
    const IMG_AG_FULL_WIDTH   = 240;
    const IMG_AG_FULL_HEIGHT  = 160;

    /**
     * Configurazione dimensioni massime per l'immagine originale di backup
     * ( in pixel )
     */
    const IMG_ORIG_FULL_WIDTH  = 600;
    const IMG_ORIG_FULL_HEIGHT = 600;

    /** Formato immagine predefinito */
    const IMG_DEFAULT_MIME = "image/jpeg";

    /** Tabella di destinazione predefinita */
    const IMG_TABLE_NAME       = "user_thumb";
    const IMG_AG_TABLE_NAME    = "group_thumb";
    const IMG_SUBGR_TABLE_NAME = "subgroup_thumb";

    /** Array contenente i dati per il database in caso di update */
    var $dataArray        = [];
    var $skipUpdate       = false;
    var $transparentBG    = false;
    var $originalMimeType = "";

    public function __construct()
    {
        
    }

    /**
     * Metodo statico pubblico accessibile direttamente dall'esterno che
     * restituisce l'html dei pulsanti richiesti.
     *
     * @param array $params Parametri di configurazione generici per la creazione dei pulsanti.
     * @param array $buttons Array multidimensionale contenente i pulsanti richiesti e la loro configurazione.
     * @return string $outhtml Restituisce l'html risultante dalla creazione dei pulsanti.
     */
    final public static function create_image_editbuttons(
        array $params = [],
        array $buttons = []
    ) {

        if (!isset($buttons[0]['command']) || trim($buttons[0]['command']) == "") {
            exit("Il parametro command e' obbligatorio.");
        }

        $outhtml = "";

        //print_r($params);
        //print_r($buttons);
        /**
         * array('dialog_title' => b('MY_THUMB_EDIT'));
         *
         *
         * array('label' => b('EDIT'),
         *       'width' => $has_thumb ? '161px' : '100%',
         *       'command' => 'user_thumb',
         *       'title'=>b('MY_THUMB_EDIT')
         */
        /**
         * Preparo i pulsanti per la gestione dell'immagine
         */
        $buttonEdit = new aengine_button([
            'label'         => b('EDIT'),
            'width'         => isset($buttons[0]['width']) ? $buttons[0]['width'] . 'px' : '100%',
            'centered'      => true,
            'css'           => 'dark5',
            'outer_styles'  => ['position:absolute', 'bottom:0px', 'left:0px'],
            'command'       => 'commandEditButton',
            'commandparams' => [
                'command' => $buttons[0]['command'],
                'type'    => 'html',
                'owner'   => 'dialog',
                'params'  => [
                    'mode'                  => $params['mode'],
                    'subgroupId'            => isset($params['subgroupId']) ? $params['subgroupId'] : '',
                    'size'                  => 'xxl',
                    'position'              => 'auto',
                    'title'                 => $params['title'] ? b($params['title']) : b('MY_THUMB_EDIT'),
                    'close'                 => true,
                    'dialog_footer_options' => [
                        'css'     => 'dark dark2',
                        'buttons' => [
                            [
                                'label'         => b('CHANGE_IMAGE'),
                                'name'          => 'b_back_thumb',
                                'command'       => 'back_thumb',
                                'hidden'        => true,
                                'commandparams' => [
                                    'owner'  => $buttons[0]['owner'],
                                    'target' => $buttons[0]['target'],
                                    'params' => [
                                        'transition' => 'up@flip',
                                        'callback'   => [
                                            'type'    => "button",
                                            'target'  => "b_save_thumb",
                                            'command' => "hide",
                                            'params'  => [
                                                'transition' => 'prev',
                                                'callback'   => [
                                                    'type'    => "button",
                                                    'target'  => $buttons[0]['subtarget'],
                                                    'command' => "hide"
                                                ]
                                            ]
                                        ]
                                    ]
                                ]
                            ],
                            [
                                'label'         => b('SAVE'),
                                'type'          => 'green',
                                'name'          => 'b_save_thumb',
                                'command'       => 'save_thumb',
                                'hidden'        => true,
                                'command'       => 'uploadImageThumb',
                                'commandparams' => [
                                    'owner'  => 'welcome',
                                    'type'   => 'component',
                                    'params' => [
                                        'mode' => $params['mode']
                                    ]
                                ]
                            ]
                        ]
                    ]
                ]
            ]
        ]);
        $outhtml .= $buttonEdit->display();

        // Se esiste l'immagine allora genero anche il pulsante di eliminazione.
        if (isset($params['avatarImg']) && $params['avatarImg'] != "") {
            //print_r($params);
            $buttonRemove = new aengine_button([
                //'label'=>b('REMOVE'),
                'icon'          => 'ae-delete-trash-1',
                'width'         => '40px',
                'centered'      => true,
                'css'           => 'brd-left dark5',
                'outer_styles'  => ['position: absolute', 'bottom: 0px', 'left: ' . ($buttons[0]['width']) . 'px'],
                'command'       => 'buttonImageRemove',
                'commandparams' => [
                    'command' => $params['obj'] . '_' . $params['act'],
                    'target'  => isset($params['target']) ? $params['target'] : '',
                    'type'    => 'html',
                    'params'  => $buttons[1]['params']
                ]
            ]);
            $outhtml .= $buttonRemove->display();
        }

        return $outhtml;
    }

    /**
     * Metodo statico pubblico accessibile direttamente dall'esterno che
     * restituisce la lunghezza del lato scelto.
     *
     * @param mixed $Image (base64 | resource)
     * @param string $Mode modalità di scelta del lato dell'immagine ( min | max )
     *
     * @return int Lunghezza in pixel.
     */
    final public static function getSelectedSize($Image, $Mode = "max")
    {
        $imgData    = getimagesize($Image);
        $width      = $imgData[0];
        $height     = $imgData[1];
        $ReturnSize = null;

        switch ($Mode) {
            case 'max':
                if ($width > $height) {
                    $ReturnSize = $width;
                } else {
                    $ReturnSize = $height;
                }
                break;

            case 'min':
                if ($width < $height) {
                    $ReturnSize = $width;
                } else {
                    $ReturnSize = $height;
                }
                break;

            default:
                exit("Errore, modalita' selezione immagine '$Mode' non valida.");
        }

        return $ReturnSize;
    }

    /**
     * Funzione che crea l'oggetto immagine mediante il file immagine,
     * fornito tramite percorso e resituisce la risorsa puntata.
     *
     * @return ImagObject
     */
    public function create_from_file($FilePath, $verbose = false, $debug = false)
    {
        /**
         * Esegue la validazione e verifica della variabile.
         */
        $FilePath = filter_var($FilePath);
        if (!$FilePath) {
            exit("Errore, percorso immagine non valido. <br />");
        }

        if ($debug) {
            print_r($FilePath);
        }

        /**
         * Verifica il tipo di immagine per discriminare la funzione da utilizzare
         * mediante il tipo mime del file.
         */
        $file_info = new finfo(FILEINFO_MIME_TYPE);
        $mime_type = $file_info->buffer(file_get_contents($FilePath));

        if ($debug) {
            print_r($mime_type);
        }

        switch ($mime_type) {
            case "image/png":
                $this->originalMimeType = $mime_type;

                $ImageObject = imagecreatefrompng($FilePath);
                $this->check_transparent($ImageObject, $verbose);
                if ($this->transparentBG) {
                    // In caso di immagine originale con sfondo trasparente,
                    // crea lo stesso tipo di sfondo anche sulla nuova immagine
                    $transparent = imagecolorallocatealpha(
                        $ImageObject,
                        255,
                        255,
                        255,
                        127
                    );
                    //$white = imagecolorallocate($newImage, 255, 255, 255);
                    imagefill($ImageObject, 0, 0, $transparent);
                } else {
                    // se lo sfondo non e' trasparente usa il colore del primo pixel (0, 0)
                    $background = $this->get_first_px_color($ImageObject);
                    imagefill($ImageObject, 0, 0, $background);
                }
                break;

            case "image/jpeg":
                $this->originalMimeType = $mime_type;
                $ImageObject            = imagecreatefromjpeg($FilePath);
                break;

            case "image/gif":
                $this->originalMimeType = $mime_type;
                $ImageObject            = imagecreatefromgif($FilePath);
                break;

            default:
                exit("Errore, formato immagine non valido. <br />");
                break;
        }

        /**
         * Verifico se la creazione dell'oggetto immagine è avvenuta correttamente
         */
        if ($ImageObject !== false) {
            if ($verbose == true) {
                echo "Creazione immagine avvenuta correttamente. <br />";
            }
        } else {
            if ($verbose == true) {
                echo "errore durante la creazione dell'oggetto immagine. <br />";
            }
        }

        return $ImageObject;
    }

    /**
     * Funzione che verifica pixel per pixel un'immagine per verificare se
     * ha dei pixel trasparenti per identificare il canale alpha.
     * @param type $ImageObject
     * @return boolean
     */
    private function check_transparent($ImageObject, $verbose = false)
    {
        $width  = imagesx($ImageObject); // Get the width of the image
        $height = imagesy($ImageObject); // Get the height of the image
        // We run the image pixel by pixel and as soon as we find a transparent pixel we stop and return true.
        for ($i = 0; $i < $width; $i++) {
            for ($j = 0; $j < $height; $j++) {
                $rgba = imagecolorat($ImageObject, $i, $j);
                if (($rgba & 0x7F000000) >> 24) {
                    if ($verbose) {
                        echo __METHOD__ . ": Trovato pixel trasparente alle coordinate ($i, $j)<br />";
                    }
                    $this->transparentBG = true;
                    return;
                }
            }
        }

        // If we dont find any pixel the function will return false.
        if ($verbose) {
            echo __METHOD__ . ": L'immagine non presenta pixel trasparenti.<br />";
        }
        $this->transparentBG = false;
    }

    /**
     * Funzione che legge il colore del primo pixel alle coordinate 0,0
     * e restituisce un int contente il colore.
     *
     * @param resource $ImageObject
     * @return int color
     */
    private function get_first_px_color($ImageObject, $verbose = false)
    {
        $color = imagecolorat($ImageObject, 0, 0);
        if ($verbose) {
            echo __METHOD__ . ": Il pixel alle coordinate (0, 0) e': $color<br />";
        }
        return $color;
    }

    /**
     * Funzione che crea un oggetto immagine im base ai paramentri con una
     * stringa di testo
     *
     * @param type $String
     * @param type $bgColor
     * @param type $fgColor
     * @param type $Width
     * @param type $Height
     * @param type $verbose
     * @param type $debug
     *
     * @return im
     */
    public function create_from_string(
        $String,
        $bgColor,
        $fgColor,
        $Width,
        $Height,
        $FontSize = 10,
        $FontPath = "/usr/share/fonts/truetype/OpenSans-Regular.ttf",
        $verbose = false,
        $debug = false
    ) {
        $bgColor    = $this->RgbfromHex($bgColor);
        $fgColor    = $this->RgbfromHex($fgColor);
        $im         = imagecreatetruecolor($Width, $Height);
        $bg_color   = imagecolorallocate(
            $im,
            $bgColor[0],
            $bgColor[1],
            $bgColor[2]
        );
        imagefill($im, 0, 0, $bg_color);
        $text_color = imagecolorallocate(
            $im,
            $fgColor[0],
            $fgColor[1],
            $fgColor[2]
        );

        //imagestring($im, $FontSize, $coords['x'], $coords['y'],  $String, $text_color);
        $coords = $this->calculate_font_position(
            $im,
            $String,
            $FontPath,
            $FontSize
        );
        imagettftext(
            $im,
            $FontSize,
            0,
            $coords['x'],
            $coords['y'],
            $text_color,
            $FontPath,
            $String
        );

        return $im;
    }

    /**
     * Funzione che restituisce un array RGB in base alla stringa HEX #RRGGBB
     * @param type $hexValue
     * @return type
     */
    public function RgbfromHex($hexValue)
    {
        $colorArray = [0, 0, 0];

        if (strlen(trim($hexValue)) == 6) {
            $colorArray = [hexdec(substr($hexValue, 0, 2)), // R
                hexdec(substr($hexValue, 2, 2)), // G
                hexdec(substr($hexValue, 4, 2))  // B
            ];
        }

        return $colorArray;
    }

    /**
     * Funzione che calcola il posizionamento del testo all'interno dell'immagine
     * e restituisce le coordinateiniziali.
     *
     * @param resource  $Image
     * @param string    $String
     * @return array    (int, int) coordinate x,y
     */
    private function calculate_font_position(
        $Image,
        $String,
        $FontFile,
        $FontSize
    ) {
        if ($FontSize <= 5) {
            $fw = imagefontwidth($FontSize);    // width of a character
            $fh = imagefontheight($FontSize);   // height of a character

            $l  = strlen($String);              // number of characters
            $tw = $l * $fw;                     // text width
            $iw = imagesx($Image);              // image width
            $ih = imagesy($Image);              // image height

            $xpos = ($iw - $tw) / 2;
            $ypos = ($ih - $fh) / 2;
        } else {
            $box = imagettfbbox($FontSize, 0, $FontFile, $String);

            $xpos = $box[0] + (imagesx($Image) / 2) - ($box[4] / 2) - 2;
            $ypos = $box[1] + (imagesy($Image) / 2) - ($box[5] / 2) - 2;
        }

        return ['x' => $xpos, 'y' => $ypos];
    }

    /**
     * Funzione che esegue il ritaglio di un oggetto immagine in base agli
     * argomenti forniti e restituisce un nuovo oggetto immagine.
     *
     * @param type $ImageObject
     * @param type $SelectionX
     * @param type $SelectionY
     * @param type $SelectionWidth
     * @param type $SelectionHeight
     * @param type $verbose
     * @param type $debug
     * @return type
     */
    public function crop(
        $ImageObject,
        $SelectionX,
        $SelectionY,
        $SelectionWidth,
        $SelectionHeight,
        $verbose = false,
        $debug = false
    ) {
        if (!$ImageObject) {
            exit("Errore, l'oggetto immagine e' nullo. <br />");
        }

        if ($debug) {
            printf(
                "[DEBUG][%s]: resimage: %d, SelX: %d, SelY: %d, SelW: %d, SelH: %d <br />",
                __METHOD__,
                $ImageObject,
                $SelectionX,
                $SelectionY,
                $SelectionWidth,
                $SelectionHeight
            );
        }

        /**
         * Creo l'oggetto immagine vuoto per il ridimensionamento
         */
        if ($verbose) {
            echo "creo il nuovo oggetto immagine <br />";
        }
        $newImage = imagecreatetruecolor($SelectionWidth, $SelectionHeight);

        if ($this->transparentBG) {
            // In caso di immagine originale con sfondo trasparente,
            // crea lo stesso tipo di sfondo anche sulla nuova immagine
            $transparent = imagecolorallocatealpha($newImage, 255, 255, 255, 127);
            //$white = imagecolorallocate($newImage, 255, 255, 255);
            imagefill($newImage, 0, 0, $transparent);
        } else {
            // se lo sfondo non e' trasparente usa il colore del primo pixel (0, 0)
            $background = $this->get_first_px_color($newImage);
            imagefill($newImage, 0, 0, $background);
        }

        // bool imagecopyresized ( resource $dst_image , resource $src_image , int $dst_x , int $dst_y ,
        // int $src_x , int $src_y , int $dst_w , int $dst_h , int $src_w , int $src_h )
        if (imagecopyresized(
            $newImage,
            $ImageObject,
            0,
            0,
            $SelectionX,
            $SelectionY,
            $SelectionWidth,
            $SelectionHeight,
            $SelectionWidth,
            $SelectionHeight
        )) {
            if ($verbose) {
                echo "Ridimensionamento riuscito <br />";
            }
        } else {
            if ($verbose) {
                echo "Errore durante il ridimensionamento dell'immagine. <br />";
            }
        }

        return $newImage;
    }

    /**
     * Funzione che esegue il ridimensionamento massivamente su un array di record fornito
     */
    public function resize_massive(
        $ArrayData,
        $ResizeMethod = "fit",
        $updateDB = false,
        $verbose = false,
        $debug = false
    ) {
        if (count($ArrayData) < 1) {
            exit("Errore, l'array dei dati e' vuoto. <br />");
        }

        if ($verbose) {
            printf("Avvio resize massivo di %d record.<br/>", count($ArrayData));
        }

        $counter = 1;
        foreach ($ArrayData as $field) {
            // salvo l'immagine originale grande prima dell'update.
            $ImageOriginal    = $this->create_from_hash(
                $field['img'],
                $verbose,
                $debug
            );
            $newImageOriginal = $this->resize(
                $ImageOriginal,
                self::IMG_ORIG_FULL_WIDTH,
                self::IMG_ORIG_FULL_HEIGHT,
                "fit",
                $verbose,
                $debug
            );
            $field['orig']    = $this->to_base64($newImageOriginal, $verbose);

            if ($verbose) {
                printf("Record %d, modifica immagine originale.<br/>", $counter);
            }

            //print_r($field);
            $ImageObjectThumb = $this->create_from_hash(
                $field['thumb'],
                $verbose,
                $debug
            );
            $newImageThumb    = $this->resize(
                $ImageObjectThumb,
                self::IMG_THUMB_WIDTH,
                self::IMG_THUMB_HEIGHT,
                $ResizeMethod,
                $verbose,
                $debug
            );
            $field['thumb']   = $this->to_base64($newImageThumb, $verbose);

            if ($verbose) {
                printf("Record %d, modifica immagine thumb.<br/>", $counter);
            }

            $ImageObjectFull = $this->create_from_hash($field['img']);
            $newImageFull    = $this->resize(
                $ImageObjectFull,
                self::IMG_FULL_WIDTH,
                self::IMG_FULL_HEIGHT,
                $ResizeMethod,
                $verbose,
                $debug
            );
            $field['img']    = $this->to_base64($newImageFull, false);

            if ($verbose) {
                printf("Record %d, modifica immagine img.<br/>", $counter);
            }

            if ($updateDB && !$this->skipUpdate) {
                // eseguo l'update dei dati
                $this->update_hash_db($field, $verbose, $debug);
                if ($verbose) {
                    printf("Record %d, update dati sul database.<br/>", $counter);
                }
            } else {
                if ($verbose) {
                    printf(
                        "Record %d, update non necessario lo salto.<br/>",
                        $counter
                    );
                }
            }
            $counter++;
        }

        if ($verbose) {
            printf("Resize massivo completato correttamente.<br/>", $counter);
        }
    }

    /**
     * Funzione che crea l'oggetto immagine mediante la stringa encodata base64,
     * presente nell'array ImageData proveniente dalla funzione imageGetHashData
     * e resituisce la risorsa puntata.
     *
     * @return ImagObject
     */
    public function create_from_hash(
        $Base64Data,
        $verbose = false,
        $debug = false
    ) {
        $ImageData   = $this->getHashdata($Base64Data, $debug);
        $ImageObject = imagecreatefromstring(base64_decode($ImageData['string']));
        if ($debug) {
            printf(
                "[DEBUG][%s]: imagedata: %s<br />",
                __METHOD__,
                $ImageData['string']
            );
            echo "creo l'immagine: '" . $ImageData['string'] . "'<br />";
        }

        if ($debug) {
            printf(
                "[DEBUG][%s]: resimage: %d, dimensione reale: %dx%d<br />",
                __METHOD__,
                $ImageObject,
                imagesx($ImageObject),
                imagesy($ImageObject)
            );
        }

        /**
         * Verifico la creazione dell'oggetto immagine
         */
        if ($ImageObject !== false) {
            if ($verbose == true) {
                echo "Creazione immagine avvenuta correttamente. <br />";
            }
            $this->check_transparent($ImageObject, $verbose);
            if ($this->transparentBG) {
                // In caso di immagine originale con sfondo trasparente,
                // crea lo stesso tipo di sfondo anche sulla nuova immagine
                $transparent = imagecolorallocatealpha(
                    $ImageObject,
                    255,
                    255,
                    255,
                    127
                );
                //$white = imagecolorallocate($newImage, 255, 255, 255);
                imagefill($ImageObject, 0, 0, $transparent);
            } else {
                // se lo sfondo non e' trasparente usa il colore del primo pixel (0, 0)
                $background = $this->get_first_px_color($ImageObject);
                imagefill($ImageObject, 0, 0, $background);
            }
        } else {
            if ($verbose == true) {
                echo "errore durante la creazione dell'oggetto immagine. <br />";
            }
        }

        return $ImageObject;
    }

    /**
     * Funzione che separa i campi delle immagini encodate dalla stringa
     * "formato,encoding" presente sul database e restituisce un array di
     * dati con i campi separati.
     *
     * @return ImageData array
     */
    private function getHashdata($HashString, $debug = false)
    {
        $ImageData = [];

        if (preg_match("(^data:(.*);(\w+),(.*)$)", $HashString, $match)) {
            $ImageData['mimeinfo'] = $match[1];
            $ImageData['encoding'] = $match[2];
            $ImageData['string']   = $match[3];
        } else {
            if ($debug) {
                echo "no match!<br />";
            }
            $ImageData = [];
        }

        if ($debug) {
            print_r($ImageData);
        }

        return $ImageData;
    }

    /**
     * Funzione che ridimensiona l'immagine in base ai parametri scelti.
     * Esegue un ridimensionamento proporzionale e dinamico in base al lato più lungo.
     * Riposiziona la nuova immagine calcolando l'offset per centrarla.
     * Se updateDB == true converte la nuova immagine in base64 ed esegue direttamnte l'update,
     * altrimenti restituisce l'oggetto immagine.
     *
     * @param resource  $ImageObject    Oggetto immagine sul quale eseguire l'elaborazione.
     * @param int       $TargetWidth    Valore dell'altezza dell'immagine finale.
     * @param int       $TargetHeight   Valore della larghezza dell'immagine finale.
     * @param string    $ResizeMethod   Metodo di ridimensionamento dell'immagine.
     * @param boolean   $verbose        Flag che attiva la visualizzazione dei messaggi verbose.
     * @param boolean   $debug          Flag che attiva la visualizzazione dei messaggi di debug.
     * @return resource $ImageObject    Oggetto immagine trattato.
     */
    public function resize(
        $ImageObject,
        $TargetWidth,
        $TargetHeight,
        $ResizeMethod = "fit",
        $verbose = false,
        $debug = false
    ) {
        if (!$ImageObject) {
            exit("Errore, l'oggetto immagine e' nullo. <br />");
        }

        if ($debug) {
            printf(
                "[DEBUG][%s]: resimage: %d, TW: %d, TH: %d <br />",
                __METHOD__,
                $ImageObject,
                $TargetWidth,
                $TargetHeight
            );
        }

        // verifico se l'immagine necessita di ridimensionamento
        if ($this->check_size($ImageObject, $TargetWidth, $TargetHeight)) {
            /**
             * Creo l'oggetto immagine vuoto per il ridimensionamento
             */
            if ($verbose) {
                echo "creo il nuovo oggetto immagine <br />";
            }

            $newImage = imagecreatetruecolor($TargetWidth, $TargetHeight);

            if ($this->transparentBG) {
                // In caso di immagine originale con sfondo trasparente,
                // crea lo stesso tipo di sfondo anche sulla nuova immagine
                $transparent = imagecolorallocatealpha(
                    $newImage,
                    255,
                    255,
                    255,
                    127
                );
                //$white = imagecolorallocate($newImage, 255, 255, 255);
                imagefill($newImage, 0, 0, $transparent);
            } else {
                // se lo sfondo non e' trasparente usa il colore del primo pixel (0, 0)
                $background = $this->get_first_px_color($ImageObject);
                imagefill($ImageObject, 0, 0, $background);
            }

            /**
             * Calcolo il rapporto d'aspetto ed eseguo il calcolo per il ridimensionamento
             */
            $RatioOriginal = round(
                imagesx($ImageObject) / imagesy($ImageObject),
                2
            );
            list($OffsetX, $OffsetY, $TargetWidth, $TargetHeight) = $this->calculate_resize(
                $ImageObject,
                $RatioOriginal,
                $ResizeMethod,
                $TargetWidth,
                $TargetHeight,
                $verbose,
                $debug
            );

            /**
             * Copio e ridimensiono effettivamente l'immagine
             *
             * bool imagecopyresampled ( resource $dst_image , resource $src_image , int $dst_x , int $dst_y ,
             * int $src_x , int $src_y , int $dst_w , int $dst_h , int $src_w , int $src_h )
             *
             * bool imagecopyresized ( resource $dst_image , resource $src_image , int $dst_x , int $dst_y ,
             * int $src_x , int $src_y , int $dst_w , int $dst_h , int $src_w , int $src_h )
             */
            if (imagecopyresized(
                $newImage,
                $ImageObject,
                $OffsetX,
                $OffsetY,
                0,
                0,
                $TargetWidth,
                $TargetHeight,
                imagesx($ImageObject),
                imagesy($ImageObject)
            )) {
                if ($verbose) {
                    echo "Ridimensionamento riuscito <br />";
                }
            } else {
                if ($verbose) {
                    echo "Errore durante il ridimensionamento dell'immagine. <br />";
                }
            }
        }

        return $newImage;
    }

    /**
     * Funzione che estrae le informazioni relative all'oggetto immagine
     * e verifica se necessita di ridimensionamento.
     * @return boolean
     */
    private function check_size(
        $ImageObject,
        $TargetWidth,
        $TargetHeight,
        $verbose = false,
        $debug = false
    ) {
        if ($debug) {
            printf(
                "[DEBUG][%s]: resimage: %d, TW: %d, TH: %d <br />",
                __METHOD__,
                $ImageObject,
                $TargetWidth,
                $TargetHeight
            );
        }

        /**
         * Estraggo le informazioni dell'immagine creata
         */
        if ($verbose) {
            printf(
                "Dimensioni attuali immagine: %dx%d.<br />",
                imagesx($ImageObject),
                imagesy($ImageObject)
            );
        }

        /**
         * Verifica le dimensioni dell'immagine
         * se diverso da quella attesa crea l'immagine vuota
         * quindi ridimensiona l'immagine sulla nuova alle dimensioni corrette
         */
        if ((imagesx($ImageObject) != $TargetWidth) || (imagesy($ImageObject) != $TargetHeight)) {
            if ($verbose) {
                //echo "L'immagine eccede le dimensioni massime, deve essere ridimensionata. <br />";
                printf(
                    "La dimensione dell'immagine (%dx%d) non e' conforme alle dimensioni
                        richieste (%dx%d), deve essere ridimensionata. <br />",
                    imagesx($ImageObject),
                    imagesy($ImageObject),
                    $TargetWidth,
                    $TargetHeight
                );
            }
            $this->skipUpdate = false;
            return true;
        } else {
            if ($verbose) {
                echo "L'immagine non richiede un ridimensionamento salto l'operazione. <br />";
            }
            $this->skipUpdate = true;
            return false;
        }
    }

    /**
     * Funzione che calcola le nuove dimensioni dell'immagine in base a quelle reali,
     * per il ridimensionamento ed avvia il tipo di ridimensionamento richiesto.
     *
     * @param resource  $ImageObject
     * @param float     $RatioOriginal
     * @param string    $ResizeMethod [fit, crop]
     * @return array($OffsetX, $OffsetY, $TargetWidth, $TargetHeight);
     */
    private function calculate_resize(
        $ImageObject,
        $RatioOriginal,
        $ResizeMethod = "fit",
        $TargetWidth,
        $TargetHeight,
        $verbose = false,
        $debug = false
    ) {
        /**
         * Imposto l'offset di base per il posizionamento
         */
        $OffsetX              = 0;
        $OffsetY              = 0;
        $OriginalTargetWidth  = $TargetWidth;
        $OriginalTargetHeight = $TargetHeight;

        switch ($ResizeMethod) {
            case "fit":
                /**
                 * Calcolo il lato più lungo per il ridimensionamento
                 * e l'offset per il posizionamento dell'immagine
                 */
                if (imagesx($ImageObject) > imagesy($ImageObject)) {
                    $TargetHeight = round($TargetWidth * $RatioOriginal, 2);
                    if ($TargetHeight > $OriginalTargetHeight) {
                        $TargetHeight = round($TargetWidth / $RatioOriginal, 2);
                    }
                    $OffsetY = ($OriginalTargetHeight - $TargetHeight) / 2;

                    if ($verbose) {
                        echo "<pre>       X > Y: (ratio $RatioOriginal)<br />";
                        echo "TargetHeight: $TargetHeight<br />";
                        echo "      Offset: ($OffsetX, $OffsetY)</pre><br />";
                    }
                } else {
                    $TargetWidth = round($TargetHeight * $RatioOriginal, 2);
                    if ($TargetWidth > $OriginalTargetWidth) {
                        $TargetWidth = round($TargetHeight / $RatioOriginal, 2);
                    }
                    $OffsetX = ($OriginalTargetWidth - $TargetWidth) / 2;

                    if ($verbose) {
                        echo "<pre>      Y > X: (ratio $RatioOriginal)<br />";
                        echo "TargetWidth: $TargetWidth<br />";
                        echo "     Offset: ($OffsetX, $OffsetY)</pre><br />";
                    }
                }

                return [$OffsetX, $OffsetY, $TargetWidth, $TargetHeight];
                break;

            case "crop":
                /**
                 * Calcolo il lato più corto per il ritaglio centrato dell'immagine originale
                 */
                if (imagesx($ImageObject) < imagesy($ImageObject)) {
                    $TargetHeight = round($TargetWidth * $RatioOriginal, 2);
                    if ($TargetHeight < $OriginalTargetHeight) {
                        $TargetHeight = round($TargetWidth / $RatioOriginal, 2);
                    }
                    $OffsetY = ($OriginalTargetHeight - $TargetHeight) / 2;
                } else {
                    $TargetWidth = round($TargetHeight * $RatioOriginal, 2);
                    if ($TargetWidth < $OriginalTargetWidth) {
                        $TargetWidth = round($TargetHeight / $RatioOriginal, 2);
                    }
                    $OffsetX = ($OriginalTargetWidth - $TargetWidth) / 2;
                }

                if ($verbose) {
                    echo "<pre>";
                    echo "               Ratio: $RatioOriginal<br />";
                    echo "         TargetWidth: $TargetWidth<br />";
                    echo "        TargetHeight: $TargetHeight<br />";
                    echo " OriginalTargetWidth: $OriginalTargetWidth<br />";
                    echo "OriginalTargetHeight: $OriginalTargetHeight<br />";
                    echo "              Offset: ($OffsetX, $OffsetY)</pre><br />";
                }

                return [$OffsetX, $OffsetY, $TargetWidth, $TargetHeight];
                break;
        }
    }

    /**
     * Funzione che converte l'oggetto immagine in un hash base64
     * @return base64
     */
    public function to_base64($ImageObject, $ShowImage = false)
    {
        ob_start();
        imagejpeg($ImageObject);
        $data = ob_get_contents();
        ob_end_clean();
        //print_r($data);
        // Riassemblo la stringa da salvare sul DB
        if ($this->originalMimeType == "") {
            $base64 = "data:" . self::IMG_DEFAULT_MIME . ";base64," . base64_encode($data);
        } else {
            $base64 = "data:" . $this->originalMimeType . ";base64," . base64_encode($data);
        }
        //echo "new base64: $base64<br />";

        if ($ShowImage && ($data != "")) {
            echo "anteprima immagine ridimensionata: <br />";
            if ($this->originalMimeType == "") {
                echo "<img src='data:" . self::IMG_DEFAULT_MIME . ";base64," . base64_encode($data) . "'><br />";
            } else {
                echo "<img src='data:" . $this->originalMimeType . ";base64," . base64_encode($data) . "'><br />";
            }
        }

        return $base64;
    }

    /**
     * Funzione che esegue l'update del base64 sul database.
     */
    public function update_hash_db($ArrayData, $verbose = false, $debug = false)
    {
        /**
         * Esegue la validazione del campo id
         */
        if (!filter_var($ArrayData['id'], FILTER_VALIDATE_INT)) {
            exit("Errore, id non valido. <br />");
        }

        if ($debug) {
            print_r($ArrayData);
        }

        $query = new aeq();

        // verifico se si tratta di un'agenzia o di un utente:
        if (isset($ArrayData['userId'])) {
            $query->update(
                self::IMG_TABLE_NAME,
                ['userId' => $ArrayData['userId'],
                'img'    => strToDB($ArrayData['img']),
                'thumb'  => strToDB($ArrayData['thumb']),
                'orig'   => strToDB($ArrayData['orig'])],
                "id = " . $ArrayData['id'],
                true
            );
        } elseif (isset($ArrayData['cerebrumGroupId'])) {
            $query->update(
                self::IMG_AG_TABLE_NAME,
                ['cerebrumGroupId' => $ArrayData['cerebrumGroupId'],
                'img'             => strToDB($ArrayData['img']),
                'thumb'           => strToDB($ArrayData['thumb']),
                'orig'            => strToDB($ArrayData['orig'])],
                "id = " . $ArrayData['id'],
                true
            );
        }
        unset($query);
    }
}

<?php

namespace clagiordano\weblibs\php;

/**
 * Description of Documenti
 *
 * @version  0.2
 * @author   Claudio Giordano <claudio.giordano@autistici.org>
 * @since    2014-04-14
 * @category Image
 * @package  weblibs
 */
class Documenti
{

    /**
     * Costruttore
     */
    public function __construct()
    {
        
    }

    public function emptyProducts()
    {
        
    }

    public function getCount()
    {
        
    }

    public function getTotal()
    {
        
    }

    public function addProduct()
    {
        
    }

    public function remProduct()
    {
        
    }
}

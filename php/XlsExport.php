<?php

namespace clagiordano\weblibs\php;

/**
 * Class to export data to xls format and write a file and/or download them.
 *
 * @version  1.3
 * @author   Claudio Giordano <claudio.giordano@autistici.org>
 * @since    2010-01-01
 * @category Data Management
 *
 * @property string   $FileName     Output file name
 * @property resource FileHandler   Resource for file output
 * @property string   $Output       File content
 * @property string   $OutputMethod Output method type null/download
 * @property boolean  $WriteStatus  File write status
 */
class XlsExport
{
    /**
     * Internal properties
     */
    private $FileName = null;
    private $FileHandler = null;
    private $Output = "";
    private $OutputMethod = "download";
    private $WriteStatus = false;

    /**
     * @constructor
     * @param string $FileName
     * @param string $OutputMethod
     *
     * @return \weblibs\php\XlsExport
     */
    public function __construct($FileName, $OutputMethod = "download")
    {
        $this->FileName = $FileName;
        $this->OutputMethod = $OutputMethod;

        $this->xlsBOF();

        return $this;
    }

    /**
     * Create header of xls file
     *
     * @return \weblibs\php\XlsExport
     */
    public function xlsBOF()
    {
        $this->Output = pack('ssssss', 0x809, 0x8, 0x0, 0x10, 0x0, 0x0);

        return $this;
    }

    /**
     * Write a cell formatted to number at coord $Row, $Col
     *
     * @param int $Row Row number of the file
     * @param int $Col Column number of the file
     * @param int $Value Cell content value
     *
     * @return \weblibs\php\XlsExport
     */
    public function xlsWriteNumber($Row, $Col, $Value)
    {
        $this->Output .= pack('sssss', 0x203, 14, $Row, $Col, 0x0);
        $this->Output .= pack('d', $Value);

        return $this;
    }

    /**
     * Write a cell formatted to string at coord $Row, $Col
     *
     * @param int $Row Row number of the file
     * @param int $Col Column number of the file
     * @param string $Value Cell content value
     *
     * @return \weblibs\php\XlsExport
     */
    public function xlsWriteLabel($Row, $Col, $Value)
    {
        $Lenght = strlen($Value);
        $this->Output .= pack('ssssss', 0x204, 8 + $Lenght, $Row, $Col, 0x0, $Lenght);
        $this->Output .= $Value;

        return $this;
    }

    /**
     * Write a file on disk and, if reuired, download them.
     */
    public function writeFile()
    {
        $this->xlsEOF();

        $this->FileHandler = fopen($this->FileName, 'w+');
        if ($this->FileHandler === false) {
            throw new \Exception("[" . __METHOD__ . "]: Unable to open file '" . $this->FileName . "'");
        }

        if (fwrite($this->FileHandler, $this->Output)) {
            throw new \Exception("[" . __METHOD__ . "]: Unable to write file '" . $this->FileName . "'");
        } else {
            $this->WriteStatus = true;
        }

        fclose($this->FileHandler);

        if ($this->Method == "download") {
            $this->sendHeaders();
        }

        return $this->WriteStatus;
    }

    /**
     * Create the footer of xls file
     *
     * @return \weblibs\php\XlsExport
     */
    public function xlsEOF()
    {
        $this->Output .= pack('ss', 0x0A, 0x00);

        return $this;
    }

    /**
     * Method to send http header to force download as attachment
     */
    public function sendHeaders()
    {
        ob_end_clean();
        ini_set('zlib.output_compression', 'Off');

        header('Pragma: public');
        // Date in the past
        header("Expires: Sat, 26 Jul 1997 05:00:00 GMT");
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
        // HTTP/1.1
        header('Cache-Control: no-store, no-cache, must-revalidate');
        // HTTP/1.1
        header('Cache-Control: pre-check=0, post-check=0, max-age=0');
        header("Pragma: no-cache");
        header("Expires: 0");
        header('Content-Transfer-Encoding: none');
        // This should work for IE & Opera
        header('Content-Type: application/vnd.ms-excel;');
        // This should work for the rest
        header("Content-type: application/vnd.ms-excel");
        header('Content-Disposition: attachment; filename="'
            . basename($this->FileName) . '"');
        readfile($this->FileName);
        unlink($this->FileName);
    }
}

<?php

namespace clagiordano\weblibs\php;

/**
 * Classe astratta che descrive i metodi standard degli oggetti,
 * e ne automatizza le meccaniche di base.
 *
 * @author Claudio Giordano <claudio.giordano@wyscout.com>
 */
abstract class ObjectActions
{
    private $Params             = array();      /* array dei parametri forniti al costruttore. */
    private $Object             = "";           /* string dell'oggetto di riferimento. */
    private $Table              = "";           /* string della tabella di riferimento, eventualmente recuperata dal config. */
    private $TableColumns       = array();      /* Array contenente la struttura della tabella di riferimento. */
    private $ExcludeFields      = array("id");  /* array dei campi della tabella da escludere dalle operazioni di insert/update. */
    private $ObjectId           = null;         /* int Id principale dell'oggetto da usare in caso di update. */
    private $ForceInsert        = false;        /* boolean flag che forza l'inserimento anche in caso di Id presente. */
    private $TableFieldsAllowed = array();      /* array con l'elenco degli effettivi campi della tabella. */
    private $ConfigVars         = array();      /* array di configurazione globale. */
    private $FieldVars          = array();
    private $CommandVars        = array();
    private $OpResult           = false;        /* boolean flag che indica se l'operazione è andata a buon fine. */
    private $Results            = array();      /* array contenente i risultati di una query select */

    const TABLE_APC_PREFIX = "struct_columns_";    /* Prefisso chiavi APC per la struttura delle tabelle */

    /**
     * @param array   $mixed                     required - Array di parametri di configurazione per la classe.
     * @param string  $mixed['obj']:             required - Tipo di oggetto.
     * @param string  $mixed['table']:           optional - nome della tabella di destinazione, se non presente accede al config.
     * @param array   $mixed['excludefields']:   optional - forza l'inserimento del record sulla tabella.
     * @param int     $mixed['id']:              optional - Id dell'oggetto in caso di update/delete o min/max in caso di generazione automatica,
     *                                                      Nel secondo caso implica forceinsert.
     * @param boolean $mixed['forceinsert']:     optional - forza l'inserimento del record sulla tabella anche se già presente.
     */
    public function __construct(array $mixed = array())
    {
        $this->setParams($mixed);

        return $this;
    }

    /**
     * Metodo che imposta la proprietà indicata
     *
     * @param array $mixed
     * @return \object_actions
     */
    final protected function setParams(array $mixed = array())
    {
        // pr("DEBUG: " . __METHOD__ . " " . print_r($mixed, true));
        $this->Params = $mixed;

        if (isset($mixed['obj'])) {
            $this->setObject($mixed['obj']);
        } else {
            exit("[" . __METHOD__ . "] Invalid obj ''.");
        }

        if (gears_get_content_vars($this->Object)) {
            $this->setConfigVars(gears_get_content_vars($this->Object));
        } else {
            $this->setConfigVars(array('mysql_table' => ''));
        }

        if (isset($mixed['table'])) {
            $this->setTable($mixed['table']);
        } else {
            $this->setTable(null);
        }

        /**
         * Valido l'objId ed aggiungo i dati di creazione/aggiornamento predefinito se non presenti.
         */
        if (isset($mixed['id'])) {
            $this->setObjectId($mixed['id']);
        } else {
            $this->setObjectId();
        }

        /**
         * In caso sia stato fornito un array di campi da escludere,
         * unisco l'array predefinito con quello fornito.
         */
        if (isset($mixed['excludefields']) && is_array($mixed['excludefields'])) {
            $this->ExcludeFields = array_merge(
                $this->ExcludeFields,
                $mixed['excludefields']
            );
        }

        if (!isset($this->ConfigVars['commands'])) {
            $this->ConfigVars['commands'] = array();
        }

        return $this;
    }

    /**
     * Metodo che imposta la proprietà indicata
     *
     * @param string $String
     * @return \object_actions
     */
    final protected function setObject($String)
    {
        if (isset($String) && !($this->Object = filter_var(
            $String,
            FILTER_SANITIZE_STRING
        ))) {
            exit("[" . __METHOD__ . "] Invalid obj '" . $String . "'.");
        } elseif (!isset($String)) {
            exit("[" . __METHOD__ . "] Invalid obj ''.");
        }

        return $this;
    }

    /**
     * Metodo che restituisce la proprietà indicata
     *
     * @return \object_actions
     */
    final protected function getObject()
    {
        return $this->Object;
    }

    /**
     * Metodo che restituisce tutti i parametri se $key è nullo
     * o il singolo elemento corrispondente se esiste.
     *
     * @param string $key Chiave dell'elemento da recuperare
     * @return \object_actions
     */
    final protected function getParams($key = null)
    {
        if (is_null($key)) {
            $retVal = $this->Params;
        } elseif (!is_null($key) && isset($this->Params[$key])) {
            $retVal = $this->Params[$key];
        } else {
            exit("[" . __METHOD__ . "] Invalid key '$key'.");
        }

        return $retVal;
    }

    /**
     * Metodo che imposta la proprietà indicata
     *
     * @param array $mixed
     * @return \object_actions
     */
    final protected function setConfigVars(array $mixed = array())
    {
        $this->ConfigVars = $mixed;

        return $this;
    }

    /**
     * Metodo che imposta la proprietà indicata
     *
     * @param type $String
     * @return \object_actions
     */
    final protected function setTable($String = null)
    {
        if (isset($String) && !($this->Table = filter_var(
            $String,
            FILTER_SANITIZE_STRING
        ))) {
            exit("[" . __METHOD__ . "] Invalid table '" . $String . "'.");
        } elseif (!isset($String)) {
            // provo a leggere la tabella corrispondente dal config
            $this->Table = $this->ConfigVars['mysql_table'];

            if ($this->Table == "") {
                exit("[" . __METHOD__ . "] Invalid table ''.");
            }
        }

        return $this;
    }

    /**
     * Metodo che imposta la proprietà indicata
     *
     * @param int $Id
     * @return \object_actions
     */
    final protected function setObjectId($Id = null)
    {
        if (!is_null($Id)) {
            $this->ObjectId = filter_var($Id, FILTER_VALIDATE_INT);

            if (!isset($this->Params['creation'])) {
                $this->Params['creation'] = now('dbfull');
            }

            if (!isset($this->Params['creationBy'])) {
                $this->Params['creationBy'] = my_id();
            }
        } else {
            if (!isset($this->Params['modified'])) {
                $this->Params['modified'] = now('dbfull');
            }

            if (!isset($this->Params['modifiedBy'])) {
                $this->Params['modifiedBy'] = my_id();
            }
        }
        return $this;
    }

    /**
     * Metodo che estrae dalla tabella indicata la lista dei campi disponibili
     * per validare le operazioni sul db, ed esclude dalla lista determinati
     * campi se fortniti dall'utente.
     *
     * Nota: rifacimento della funzione globale gears_get_field_allowed($obj,$deleteKey='id')
     */
    final private function getTableFields()
    {
        // Verifico se è presente in APC la struttura della tabella attuale
        if (!ae()->get_memory(self::TABLE_APC_PREFIX . $this->Table)) {
            // I dati non sono presenti, rigenero l'informazione sull'APC
            $qColumns = "SHOW COLUMNS FROM `" . $this->Table . "`";
            $query    = new aeq($qColumns, true);

            while ($row = $query->fetch()) {
                $list[] = $row['Field'];
                array_push($this->TableColumns, $row);
            }

            // Memorizzo la struttura con chiave univoca in APC
            ae()->load_memory(
                self::TABLE_APC_PREFIX . $this->Table . "_field_list",
                $list,
                3600
            );
            ae()->load_memory(
                self::TABLE_APC_PREFIX . $this->Table,
                $this->TableColumns,
                3600
            );
        } else {
            // I dati sono presenti, li prendo da APC e non eseguo la query
            $list               = ae()->get_memory(self::TABLE_APC_PREFIX . $this->Table . "_field_list");
            $this->TableColumns = ae()->get_memory(self::TABLE_APC_PREFIX . $this->Table);
            //pr($this->TableColumns);
        }

        /**
         * Utilizza l'array ExcludeFields per escludere dalla lista
         * i campi corrispondenti mediante una ricerca.
         */
        foreach ($this->ExcludeFields as $keyToDelete) {
            $keyId = array_search($keyToDelete, $list);
            if (is_numeric($keyId)) {
                unset($list[$keyId]);
            }
        }
        $this->TableFieldsAllowed = $list;

        unset($list);
        unset($query);

        return $this;
    }

    /**
     * Metodo  che estrae dall'array dei parametri i valori e li associa
     * ai campi reali della tabella di destinazione.
     */
    final private function associateFields2Params()
    {
        foreach ($this->Params as $key => $value) {
            if (in_array($key, $this->TableFieldsAllowed)) {
                if (isset($this->ConfigVars['field_type'][$key])) {
                    $type = $this->ConfigVars['field_type'][$key];
                } elseif (isset($this->ConfigVars['type_default'])) {
                    $type = $this->ConfigVars['type_default'];
                } else {
                    $type = 'standard';
                }

                switch ($type) {
                    case 'datetime':
                    case 'date':
                        $this->FieldVars[$key] = gears_date_validate(
                            $value,
                            $type
                        );
                        break;

                    default:
                        $this->FieldVars[$key] = $value;
                }

                // delego al metodo la validazione dei campi/db
                $this->validateFields($key);
            }

            if (in_array($key, $this->ConfigVars['commands'])) {
                $this->CommandVars[$key] = $value;
            }
        }

        return $this;
    }

    /**
     * Metodo che verifica se i dati forniti per i campi sono coerenti
     * con il tipo di dato atteso dalla tabella sia per quanto riguarda il
     * tipo di valore fornito int/string ecc e anche per quanto riguarda la
     * lunghezza massima dei dati ospitabili dalla tabella.
     *
     * @param string $FieldName     required - Nome del campo sul quale inserire il valore e quindi da validare.
     * @return \aengine_object_actions
     */
    final private function validateFields($FieldName)
    {
        // Array search multidimensionale by id
        foreach ($this->TableColumns as $key => $val) {
            if ($val['Field'] === $FieldName) {
                //pr("type: " . $val['Type']);
                if (preg_match("((\w+)\(*(\d*)\)*(.*))", $val['Type'], $match)) {
                    $FieldType   = $match[1];
                    $FieldLenght = $match[2] > 0 ? $match[2] : false;
                } else {
                    exit("[DEBUG][" . __METHOD__ . "]: FieldType '" . $val['Type'] . "' fallita regexp.<br />");
                }

                // campo trovato, eseguo la validazione
                switch ($FieldType) {
                    case 'datetime':
                    case 'timestamp':
                    case 'date':
                    case 'blob':
                        // casi da saltare
                        break;

                    case 'int':
                    case 'bigint':
                    case 'tinyint':
                        $this->FieldVars[$FieldName] = filter_var(
                            $this->FieldVars[$FieldName],
                            FILTER_VALIDATE_INT
                        );
                        if ($FieldLenght && strlen($this->FieldVars[$FieldName]) > $FieldLenght) {
                            exit("[DEBUG][" . __METHOD__ . "]: Lunghezza campo '$FieldName' non valida, lunghezza attuale "
                                    . strlen($this->FieldVars[$FieldName]) . ", lunghezza massima $FieldLenght.<br />");
                        }
                        break;

                    case 'varchar':
                    case 'char':
                        $this->FieldVars[$FieldName] = filter_var(
                            $this->FieldVars[$FieldName],
                            FILTER_SANITIZE_STRING
                        );
                        if ($FieldLenght && strlen($this->FieldVars[$FieldName]) > $FieldLenght) {
                            exit("[DEBUG][" . __METHOD__ . "]: Lunghezza campo '$FieldName' non valida, lunghezza attuale "
                                    . strlen($this->FieldVars[$FieldName]) . ", lunghezza massima $FieldLenght.<br />");
                        }
                        break;

                    case 'text':    // campo testo, eseguo solo la validazione dato che non ha limite
                    case 'mediumtext':
                    case 'tinytext':
                        $this->FieldVars[$FieldName] = filter_var(
                            $this->FieldVars[$FieldName],
                            FILTER_SANITIZE_STRING
                        );
                        break;

                    default:
                        exit("[DEBUG][" . __METHOD__ . "]: FieldType '$FieldType' sconosciuto<br />");
                }
            }
        }

        return $this;
    }

    /**
     * Metodo generico che effettua l'azione prestabilita dell'oggetto sul database,
     * effettuando automaticamente in base al caso l'inserimento o l'update.
     */
    final public function execute()
    {
        $this->getTableFields();
        $this->associateFields2Params();

        /**
         * Se esiste il campo objId ma è una stringa verifica gli argomenti,
         * se corrispondono al formato atteso genera l'id (positivo o negativo)
         * e attiva il flag forceinsert.
         */
        if (isset($this->ObjectId) && ($this->Params['id'] == "min")) {
            $this->setNextMinId();
        } elseif (isset($this->ObjectId) && ($this->Params['id'] == "max")) {
            $this->setNextMaxId();
        }

        /**
         * Scelta operazione da intraprendere con i dati forniti
         */
        if ((!$this->ObjectId) || ($this->ObjectId && $this->ForceInsert)) {
            //pr("objid NON esiste o esiste ma c'e' il forceinsert!" );
            $this->insert();
        } elseif ($this->ObjectId && !$this->ForceInsert) {
            //pr("objid esiste e non c'e' il forceinsert!" );
            $this->update();
        }
    }

    /**
     * Metodo che estrae i dati di un oggetto in base all'id
     */
    final private function select()
    {
        $q     = "SELECT * FROM `" . $this->Table . "` WHERE id = ' " . $this->ObjectId . "'";
        $query = new aeq($q);
        $query->open();

        if ($query->get_num_rows() == 1) {
            $this->Results = $query->fetch();
        } else {
            while ($row = $query->fetch()) {
                array_push($this->Results, $row);
            }
        }
        unset($query);

        return $this;
    }

    /**
     * Metodo che estrae i dati di un oggetto in base all'id o ai parametri forniti:
     *
     * @param mixed $mixed Variabile contenente il dato da recuperare, puo' essere:
     *                     null  - viene restituito l'elemento indicato nel costruttore/setParams
     *                     int   - viene restituito l'elemento con l'id corrispondente a quello indicato
     *                     array - se si tratta di una singola coppia chiave => valore viene usata
     *                             la stessa come condizione WHERE
     *                     array - se presente piu' di un elemento viene usato l'intero array come
     *                             condizione WHERE IN (...)
     *
     * @return array contenente il resultset richiesto in base ai parametri.
     */
    final public function get($mixed = null)
    {
        $q = "SELECT * FROM `" . $this->Table . "` WHERE ";
        $q .= $this->compileCondition($mixed);

        $query = new aeq($q);
        $query->open();

        if ($query->get_num_rows() == 1) {
            $this->Results = $query->fetch();
        } else {
            while ($row = $query->fetch()) {
                $this->Results[] = $row;
            }
        }
        unset($query);

        return $this->Results;
    }

    /**
     * Compone una stringa per la condizione WHERE in base ai parametri indicati
     *
     * @see $this->get()
     * @param mixed $mixed
     *
     * @return string
     */
    final private function compileCondition($mixed = null)
    {
        $computedString = "";

        if (is_null($mixed) && filter_var($this->ObjectId, FILTER_VALIDATE_INT)) {
            $computedString .= "id = '" . $this->ObjectId . "'";
        } elseif (filter_var($mixed, FILTER_VALIDATE_INT)) {
            $computedString .= "id = '$mixed'";
        } elseif (is_array($mixed) && count($mixed) == 1) {
            $key = array_keys($mixed);
            $key = $key[0];
            $computedString .= "$key = '" . $mixed[$key] . "'";
        } elseif (is_array($mixed) && count($mixed) > 1) {
            $computedString .= "id IN(" . join(",", $mixed) . ")";
        } else {
            exit("[DEBUG][" . __METHOD__ . "]: Argomento '" . print_r(
                $mixed,
                true
            ) . "' non valido per il metodo.<br />");
        }

        //print "[DEBUG][" . __METHOD__ . "]: computedString: $computedString\n";

        return $computedString;
    }

    /**
     * Metodo che verifica se l'utente può eseguire l'update dei dati in oggetto.
     *
     * @return boolean
     */
    protected /* final */ function is_editable()
    {
        // TODO verifico la visibilità dell'oggetto
        $this->select();
        // if ...
    }

    /**
     * Metodo che richiama le funzioni di inserimento sul database in base alle proprietà.
     */
    final private function insert()
    {
        $query          = new aeq();
        $query->insert($this->Table, $this->FieldVars, true);
        $this->ObjectId = $query->lastId();
        unset($query);

        $this->OpResult = true;
        return true;
    }

    /**
     * Metodo che richiama le funzioni di update sul database in base alle proprietà.
     */
    final private function update()
    {
        /**
         * in questo caso acquisisci l'array e verifica quali campi sono stati effettivamente modificati,
         * rimuovendo dalla lista dei campi dell'update i campi non cambiati.
         */
        $this->select();
        $oldData = $this->FieldVars;

        foreach ($this->FieldVars as $key => $value) {
            if ($value == $this->Results[$key]) {
                //pr("il campo $key non e' cambiato, lo salto.");
                unset($this->FieldVars[$key]);
            }
        }

        $query = new aeq();
        $query->update(
            $this->Table,
            $this->FieldVars,
            "id = '" . $this->ObjectId . "'",
            true
        );
        unset($query);

        // reimposto i dati originali nell'array dopo l'update
        $this->FieldVars = $oldData;

        $this->OpResult = true;
        return true;
    }

    /**
     * Metodo che richiama le funzioni di eliminazione sul database in base alle proprietà.
     *
     * TODO da implementare successivamente.
     */
    final private function delete()
    {
        printf("[DEBUG][%s]: called method<br />", __METHOD__);
    }

    /**
     * Metodo che calcola il prossimo id negativo
     * eseguendo il lock della tabella e verificando l'id più basso -1.
     *
     * @param string $Field     optional - nome del campo sul quale eseguire la verifica (default id)
     * @return int id
     */
    final private function getNextMinId($Field = "id")
    {
        $q     = "SELECT (MIN($Field)-1)AS id FROM `" . $this->Table . "` LOCK IN SHARE MODE";
        $query = new aeq($q, true);
        $row   = $query->fetch();
        unset($query);

        return $row['id'];
    }

    /**
     * Metodo che calcola il prossimo id positivo
     * eseguendo il lock della tabella e verificando l'id più alto +1
     *
     * @param string $Field     optional - nome del campo sul quale eseguire la verifica (default id)
     * @return int id
     */
    final private function getNextMaxId($Field = "id")
    {
        $q     = "SELECT (MAX($Field)+1)AS id FROM `" . $this->Table . "` LOCK IN SHARE MODE";
        $query = new aeq($q, true);
        $row   = $query->fetch();
        unset($query);

        return $row['id'];
    }

    /**
     * Metodo che calcola il prossimo id negativo
     * eseguendo il lock della tabella e verificando l'id più basso -1.
     * e lo imposta sulla proprietà corrispondente.
     * Imposta inoltre in automatico il flag ForceInsert a true.
     *
     * @param string $Field     optional - nome del campo sul quale eseguire la verifica (default id)
     */
    final private function setNextMinId($Field = "id")
    {
        $this->ObjectId    = $this->getNextMinId($Field);
        $this->ForceInsert = true;

        return $this;
    }

    /**
     * Metodo che calcola il prossimo id positivo
     * eseguendo il lock della tabella e verificando l'id più alto +1
     * e lo imposta sulla proprietà corrispondente.
     * Imposta inoltre in automatico il flag ForceInsert a true.
     *
     * @param string $Field     optional - nome del campo sul quale eseguire la verifica (default id)
     */
    final private function setNextMaxId($Field = "id")
    {
        $this->ObjectId    = $this->getNextMaxId($Field);
        $this->ForceInsert = true;

        return $this;
    }

    // @override embedded function
    public function print_r()
    {
        printf("<pre>");
        printf(
            "[DEBUG][%s]:          Object: '%s'<br />",
            __CLASS__,
            $this->Object
        );
        printf(
            "[DEBUG][%s]:   ExcludeFields: '%s'<br />",
            __CLASS__,
            print_r($this->ExcludeFields, true)
        );
        printf(
            "[DEBUG][%s]:           Table: '%s'<br />",
            __CLASS__,
            $this->Table
        );
        printf(
            "[DEBUG][%s]:     ForceInsert: '%s'<br />",
            __CLASS__,
            $this->ForceInsert ? 'true' : 'false'
        );
        printf(
            "[DEBUG][%s]:        ObjectId: '%d'<br />",
            __CLASS__,
            $this->ObjectId
        );

        printf(
            "[DEBUG][%s]:          Params: %s<br />",
            __CLASS__,
            print_r($this->Params, true)
        );

        printf("</pre>");

        return $this;
    }

    /**
     * Metodo che restituisce la proprietà indicata
     *
     * @return null Metodo mantenuto per compatibilità generale
     */
    protected function get_vars()
    {
        return null;
    }

    /**
     * Metodo che imposta la proprietà indicata
     *
     * @return null Metodo mantenuto per compatibilità generale
     */
    protected function load_vars()
    {
        return null;
    }

    /**
     * Metodo che restituisce la proprietà indicata
     *
     * @return array Restituisce l'array con i parametri interni;
     */
    protected function get_params($key = null)
    {
        return $this->getParams($key);
    }

    /**
     * Metodo che imposta la proprietà indicata
     *
     * @return array;
     */
    /* protected */function load_params(array $mixed = array())
    {
        $this->setParams($mixed);
        return $this;
    }

    /**
     * Metodo che restituisce la proprietà indicata
     *
     * @return string Restituisce il nome dell'oggetto;
     */
    protected function get_obj()
    {
        return $this->getObject();
    }
}

<?php

namespace clagiordano\weblibs\php;

use clagiordano\weblibs\php\Database;

/**
 * Description of DatabaseManagement
 *
 * @author Claudio Giordano <claudio.giordano@autistici.org>
 */
class DatabaseManagement
{

    public function drawConnectionForm($controlPrefix = null)
    {
        ?>
        <div class="connectioncontainer">
            <div class="connectiontitle">Parametri connessione database server:</div>
            <div class="row">
                <div class="rowlabel">User:</div>
                <div class="rowcontrol">
                    <input name="<?php echo $controlPrefix; ?>_user" type="text"
                           value="<?php
                            if (filter_input(INPUT_POST, $controlPrefix . "_user")) {
                                echo filter_input(
                                    INPUT_POST,
                                    $controlPrefix . "_user"
                                );
                            }
                            ?>"/>
                </div>
            </div>

            <div class="row">
                <div class="rowlabel">Password:</div>
                <div class="rowcontrol">
                    <input name="<?php echo $controlPrefix; ?>_pass" type="password"
                           value="<?php
                            if (filter_input(INPUT_POST, $controlPrefix . "_pass")) {
                                echo filter_input(
                                    INPUT_POST,
                                    $controlPrefix . "_pass"
                                );
                            }
                            ?>" />
                </div>
            </div>

            <div class="row">
                <div class="rowlabel">Hostname:</div>
                <div class="rowcontrol">
                    <input name="<?php echo $controlPrefix; ?>_host" type="text"
                           value="<?php
                            if (filter_input(INPUT_POST, $controlPrefix . "_host")) {
                                echo filter_input(
                                    INPUT_POST,
                                    $controlPrefix . "_host"
                                );
                            }
                            ?>" />
                </div>
            </div>

            <div class="row">
                <div class="rowlabel">Porta:</div>
                <div class="rowcontrol">
                    <input name="<?php echo $controlPrefix; ?>_port" type="text"
                           value="<?php
                            if (filter_input(INPUT_POST, $controlPrefix . "_port")) {
                                echo filter_input(
                                    INPUT_POST,
                                    $controlPrefix . "_port"
                                );
                            }
                            ?>" />
                </div>
            </div>

            <div class="row">
                <div class="rowlabel">Selezione database:</td>
                    <div class="rowcontrol">
                        <select name="<?php echo $controlPrefix; ?>_name"
                                onchange="javascript: posta('1', '<?php
                                echo filter_input(INPUT_SERVER, 'REQUEST_URI');
                                ?>'); void(0);" >
                            <option value="-">-</option>
                            <?php if (isset($dba_list)) {
?>
                                <?php foreach ($dba_list as $key => $field) {
?>
                                    <option value="<?php echo $field['Database']; ?>"
                                    <?php
                                    if (filter_input(
                                        INPUT_POST,
                                        $controlPrefix . '_name'
                                    ) == $field['Database']) {
                                        echo "selected=\"selected\"";
                                    }
                                    ?> >
                                                <?php echo $field['Database']; ?>
                                    </option>
                                <?php
} ?>
                            <?php
} ?>
                        </select>
                    </div>
                </div>

                <div class="row">
                    <div class="rowlabel">
                        <input name="bt_anteprima" type="submit" value="Anteprima" />
                    </div>
                </div>
            </div>
            <?php
    }
}
    

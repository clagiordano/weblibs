<?php

namespace clagiordano\weblibs\php;

use PDO;

/**
 * Class to connect to a database and execute full CRUD operation and validation.
   *
 * Rewriting interface with database migration of the old functions
   * Mysql_ * in favor of the new class PDO.
 *
 * @version  5.6.4
 * @author   Claudio Giordano <claudio.giordano@autistici.org>
 * @since    2010-01-01
 * @category Data Management
 *
 * @property string              $Driver database driver backend type
 * @property array               $DriverOptions database driver backend options
 * @property boolean             $QueryStatus PDO execution status
 * @property string              $ResultStatus Success message
 * @property boolean             $ErrorsOccurred Error flag
 * @property int                 $LastInsertID Last inserted id
 * @property array               $Results array with query resultset
 * @property string              $Table Table destination name
 * @property array               $Fields array with fields
 * @property array               $TableColumns array with table structure.
 * @property array               $Values array with key value.
 * @property \weblibs\php\Logger $Logger Logger external object
 *
 * @property @deprecated array $ReportHeader property to move away
 * @property @deprecated array $ReportFields property to move away
 */
class Database
{
    // Enumerate query type
    const QUERY_SELECT           = 0;
    const QUERY_INSERT           = 1;
    const QUERY_UPDATE           = 2;
    const QUERY_DELETE           = 3;
    const TABLE_APC_PREFIX       = "struct_columns_";
    // Default messages for query status
    const QUERY_OK_SELECT        = "Dati letti correttamente.";
    const QUERY_OK_INSERT        = "Dati inseriti correttamente.";
    const QUERY_OK_UPDATE        = "Dati modificati correttamente.";
    const QUERY_OK_DELETE        = "Dati eliminati correttamente.";
    const QUERY_ERROR_NOT_UNIQUE = "Errore, uno dei campi non risulta univoco, salvataggio annullato.";
    const QUERY_ERROR_SELECT     = "Errore durante la lettura dei dati.";
    const QUERY_ERROR_INSERT     = "Errore durante l'inserimento dei dati.";
    const QUERY_ERROR_UPDATE     = "Errore durante l'aggiornamento dei dati.";
    const QUERY_ERROR_DELETE     = "Errore durante l'eliminazione dei dati.";
    // Misc message
    const ERROR_CREATE_OBJECT    = "Errore durante la creazione dell'oggetto database.";

    private $dbConnection   = null;
    private $Driver         = "";
    private $DriverOptions  = array();
    private $QueryStatus    = false;
    private $ResultStatus   = "";
    private $ErrorsOccurred = false;
    private $LastInsertID   = 0;
    public  $Results         = array();
    private $Table          = "";
    private $Fields         = array();
    private $TableColumns   = array();
    private $ReportHeader   = "";
    private $ReportFields   = "";
    private $Logger         = null;
    private $dbName         = null;

    function getDbConnection()
    {
        return $this->dbConnection;
    }

    /**
     * Costruttore di classe e creazione connessione
     * @param  string $db_host
     * @param  string $db_user
     * @param  string $db_password
     * @param  string $db_name
     * @param  string $driver
     * @param  string $db_charset
     * @return \weblibs\php\Database
     */
    public function __construct(
        $db_host, $db_user, $db_password, $db_name, $driver = "mysql",
        $db_charset = "utf8"
    )
    {
        // store driver PDO
        $this->dbName  = $db_name;
        $this->Driver        = $driver;
        // /*PDO::ATTR_PERSISTENT => true,*/   // persistent connection
        // PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION tratta ogni errore come eccezione
        $this->DriverOptions = array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION);

        /**
         * Try to connect to database
         */
        try {
            $this->dbConnection = new PDO(
                    "$driver:host=$db_host;dbname=$db_name;charset=$db_charset",
                    "$db_user", "$db_password", $this->DriverOptions
            );
            $this->Logger       = new Logger($this->dbConnection);

            return $this->dbConnection;
        } catch (\PDOException $ex) {
            // Error during database connection, check params.
            exit(self::ERROR_CREATE_OBJECT . "<br />" . $ex->getMessage());
        }
    }

    public function getDatabaseName()
    {
        return $this->dbName;
    }

    public function getResults()
    {
        return $this->Results;
    }

    /**
     * Funzione che prepara la stringa della query da eseguire
     *
     * @param int     $TipoQuery
     * @param string  $Table
     * @param array   $Valori
     * @param array   $Esclusioni
     * @param string  $Where
     * @param boolean $debug
     */
    private function preparateQuery(
        $TipoQuery, $Table = "", array $Valori = array(),
        array $Esclusioni = array(), $Where = "", $debug = false
        )
    {
        // Prepare query prefix
        switch ($TipoQuery) {
            case self::QUERY_INSERT: $String = "INSERT INTO $Table ";
                break;
            case self::QUERY_UPDATE: $String = "UPDATE $Table SET ";
                break;
            case self::QUERY_DELETE: $String = "DELETE FROM $Table ";
                break;
            default:
                $this->Logger->notice(
                        "Tipo query non valido!",
                        array('function' => __METHOD__, 'tipoquery' => $TipoQuery)
                );
                $String = ""; // Restituisce una stringa nulla in questo caso.
        }

        if ($debug == true) {
            //echo "[DEBUG][" . __METHOD__ . "]: String: '$String'<br />";
            $this->Logger->debug(
                    "{function}: String: '{string}'",
                    array('function' => __METHOD__, 'string' => $String)
            );
        }

        // aggiunge i campi ed i valori
        $String .= $this->assembleFields(
                $TipoQuery, $Valori, $Esclusioni, $debug
        );

        // aggiunge la condizione where se presente
        if (trim($Where) != "") {
            $String .= " WHERE $Where ";
        }

        if ($debug == true) {
            //echo "[DEBUG][" . __METHOD__ . "]: String: '$String'<br />";
            $this->Logger->debug(
                    "{function}: String: '{string}'",
                    array('function' => __METHOD__, 'string' => $String)
            );
        }

        return $String;
    }

    /**
     * Reassemble fields array and prepare query string
     * Funzione che riassembla l'array dei campi e prepara la stringa per la query
     *
     * @param  int   $TipoQuery
     * @param  array $Valori
     * @param  array $Esclusioni
     * @return mixed|string
     */
    private function assembleFields(
    $TipoQuery, array $Valori = array(), array $Esclusioni = array(),
    $debug = false
    )
    {
        // Rimuove i campi da escludere da quelli da usare
        $diffArray = array_diff_key($Valori, $Esclusioni);
        $String    = "";               // Stringa finale da restituire

        if ($TipoQuery == self::QUERY_INSERT) {
            $Keys   = "(";              // String with field name ( keys )
            $Fields = ") values (";     // string with value ( values )
        } else {
            $Keys   = "";               // String with field name ( keys )
            $Fields = "";               // Unused on update case
        }

        // Attraversa l'array per riassemblare i campi
        foreach ($diffArray as $key => $field) {
            if ($TipoQuery == self::QUERY_INSERT) {
                $Keys   .= "$key, ";
                $Fields .= "'" . addslashes(stripslashes($field)) . "', ";
            } elseif ($TipoQuery == self::QUERY_UPDATE) {
                $Keys .= "$key = '" . addslashes(stripslashes($field)) . "', ";
            }
        }

        // Ricompongo e rimuovo i caratteri non necessari
        $String .= str_replace(", )", ")", $Keys . $Fields);
        $String = preg_replace("/^(.*)(\,\ )$/", "$1", $String);
        if ($TipoQuery == self::QUERY_INSERT) {
            $String .= ")";
        }

        if ($debug == true) {
            //echo "[DEBUG][" . __METHOD__ . "]: String: '$String'<br />";
            $this->Logger->debug(
                    "{function}: String: '{string}'",
                    array('function' => __METHOD__, 'string' => $String)
            );
        }

        return $String;
    }

    /**
     * Funzione di esecuzione reale singola query come da argomento.
     *
     * @param  string  $QueryString
     * @param  boolean $debug
     * @return array   $Results
     */
    public function runQuery($QueryString, $debug = false)
    {
        if ($debug == true) {
            // echo "[Debug][" . __METHOD__ . "]: QueryString: $QueryString<br />";
            $this->Logger->debug(
                    "{function}: QueryString: {query}",
                    array('function' => __METHOD__, 'query' => $QueryString)
            );
        }

        /**
         * passo la stringa della query ricomposta a PDO per la validazione
         * ed esecuzione.
         */
        $cursor = $this->dbConnection->prepare($QueryString);
        try {
            // start transaction
            $this->dbConnection->beginTransaction();
            // execute the query and return a status
            $this->QueryStatus = $cursor->execute();
            // finally execute the query
            $this->QueryStatus = $this->dbConnection->commit();
            // return last inserted id
            $this->LastID      = $this->dbConnection->lastInsertId();
        } catch (\PDOException $ex) {
            // In case of error, log operation
            $this->Logger->error(
                    "{function}: Errore durante l'esecuzione della query: '{query}', {ex}, eseguo il rollback!",
                    array('function' => __METHOD__, 'query' => $QueryString, 'ex' => $ex->getMessage())
            );
            $this->dbConnection->rollback();
            // Return execution status to false
            $this->QueryStatus = false;
            //exit();
        }

        try {
            /**
             * Estraggo i risultati della query e popolo la proprietà
             */
            $this->Results = $cursor->FetchAll(PDO::FETCH_ASSOC);
        } catch (\PDOException $ex) {
            return;
        }
    }

    /**
     * Funzione per la verifica dell'unicità di valori per prevenire duplicati
     *
     * Restituisce il numero di risultati corrispondenti
     */
    private function checkUnique(
    $Table = "", array $Valori = array(), $CkFields = "", $debug = false
    )
    {
        $Unique = 0;    // flag / contatore di controllo per l'unicità
        $CkWere = "";   // Stringa Where

        if ($CkFields != "") {
            $CkArray = explode(",", $CkFields);
            $CkWere  .= "(";
            foreach ($CkArray as $key => $field) {
                $CkWere .= "$field = '" . trim(addslashes(stripslashes($Valori[$field]))) . "' ";
                if ($key < count($CkArray) - 1) {
                    $CkWere .= "OR ";
                }
            }
            $CkWere     .= ")";
            $UniqueRows = $this->runQuery(
                    "SELECT * FROM $Table WHERE $CkWere", $debug
            );
            $Unique     += count($UniqueRows);
        }

        return $Unique; // il numero di risultati
    }

    /**
     * Funzione di salvataggio dati su database
     *
     * @param  array|type  $Valori
     * @param  array|type  $Esclusioni
     * @param  string|type $Table
     * @param  string|type $CkFields
     * @param  bool|type   $debug
     * @return type
     */
    public function saveRow(
    array $Valori = array(), array $Esclusioni = array(), $Table = "",
    $CkFields = "", $debug = false
    )
    {
        /**
         * Preparo la query con i dati provenienti dagli argomenti
         */
        $QueryString = $this->preparateQuery(
                self::QUERY_INSERT, $Table, $Valori, $Esclusioni, null, $debug
        );

        /**
         * Verifico l'unicità dei campi
         */
        $Unique = $this->checkUnique($Table, $Valori, $CkFields, $debug);

        if ($debug == true) {
            // echo "[Debug][" . __METHOD__ . "]:    CkFields: $CkFields <br />";
            // echo "[Debug][" . __METHOD__ . "]:      Unique: $Unique <br />";
            // echo "[Debug][" . __METHOD__ . "]: QueryString: $QueryString <br />";
            $this->Logger->debug(
                    "{function}: Query: {query}, CkFields {CkFields}, Unique {Unique}",
                    array('function' => __METHOD__, 'CkFields' => $CkFields, 'Unique' => $Unique)
            );
        }

        if ($Unique == 0) {
            /**
             * In caso di 0 corrispondenze eseguo effettivamente la query per il salvataggio
             */
            $this->runQuery($QueryString, $debug);
            if ($this->QueryStatus == true) {
                $this->ResultStatus = self::QUERY_OK_INSERT;
            } else {
                $this->ResultStatus = self::QUERY_ERROR_INSERT;
            }
        } else {
            $this->ResultStatus = self::QUERY_ERROR_NOT_UNIQUE;
            $this->Logger->debug(
                    "{function}: " . self::QUERY_ERROR_NOT_UNIQUE . "Unique: {Unique}",
                    array('function' => __METHOD__, 'Unique' => $Unique)
            );
        }
    }

    /**
     * Funzione di aggiornamento dati su database
     *
     * @param  array|type  $Valori
     * @param  array|type  $Esclusioni
     * @param  string|type $Table
     * @param  string|type $Where
     * @param  bool|type   $debug
     * @return type
     */
    public function updateRow(
    array $Valori = array(), array $Esclusioni = array(), $Table = "",
    $Where = "", $debug = false
    )
    {
        /**
         * Preparo la query con i dati provenienti dagli argomenti
         */
        $QueryString = $this->preparateQuery(
                self::QUERY_UPDATE, $Table, $Valori, $Esclusioni, $Where, $debug
        );

        if ($debug == true) {
            // echo "[Debug][" . __METHOD__ . "]: QueryString: $QueryString <br />";
            $this->Logger->debug(
                    "{function}: {query}",
                    array('function' => __METHOD__, 'QueryString' => $QueryString)
            );
        }

        $this->runQuery($QueryString, $debug);
        // in caso di errore non viene eseguita questa parte
        if ($this->QueryStatus == true) {
            $this->ResultStatus = self::QUERY_OK_UPDATE;
        } else {
            $this->ResultStatus = self::QUERY_ERROR_UPDATE;
        }
    }

    /**
     * Funzione di selezione dati da database
     *
     * @param  string      $Fields
     * @param  string|type $Table
     * @param  string|type $Where
     * @param  string|type $Group
     * @param  string|type $Order
     * @param  bool|type   $debug
     * @return type
     */
    public function getRows(
    $Fields = "*", $Table = "", $Where = "", $Group = "", $Order = "",
    $debug = false
    )
    {
        // Pulisco il campo fields
        if (trim($Fields) == "") {
            $Fields = "*";
        }

        $QueryString = "SELECT $Fields FROM $Table";

        if (trim($Where) != "") {
            //$q .= " WHERE " . addslashes(stripslashes($Where));
            $QueryString .= " WHERE $Where";
        }

        if (trim($Group) != "") {
            $QueryString .= " GROUP BY $Group";
        }

        if (trim($Order) != "") {
            $QueryString .= " ORDER BY $Order";
        }

        if ($debug == true) {
            // echo "[Debug][" . __METHOD__ . "]: QueryString: $QueryString <br />";
            $this->Logger->debug(
                    "QueryString: $QueryString", array('function' => __METHOD__)
            );
        }

        $this->runQuery($QueryString, $debug);
        // in caso di errore non viene eseguita questa parte
        if ($this->QueryStatus == true) {
            $this->ResultStatus = self::QUERY_OK_SELECT;
        } else {
            $this->ResultStatus = self::QUERY_ERROR_SELECT;
        }
    }

    /**
     * Funzione di rimozione dati da database
     *
     * @param  type      $Table
     * @param  type      $Where
     * @param  bool|type $debug
     * @return boolean
     */
    public function deleteRows($Table, $Where, $debug = false)
    {
        $QueryString = "DELETE FROM $Table Where $Where";

        if ($debug == true) {
            // echo "[Debug][" . __METHOD__ . "]: QueryString: $QueryString <br />";
            $this->Logger->debug(
                    "QueryString: $QueryString", array('function' => __METHOD__)
            );
        }

        $this->runQuery($QueryString, $debug);
        // in caso di errore non viene eseguita questa parte
        if ($this->QueryStatus == true) {
            $this->ResultStatus = self::QUERY_OK_DELETE;
        } else {
            $this->ResultStatus = self::QUERY_ERROR_DELETE;
        }
    }

    // --- opzionali ---
    /**
     * Funzione quasi identica alla GetRow ma per singole query,
     * non viene ricostruita la listas degli argomenti ed è meno sicura.
     * Restituisce inoltre solo il primo risultato della query.
     *
     * Deprecata !?
     *
     * @param  type $Fields
     * @param  type $Table
     * @param  type $Where
     * @param  type $debug
     * @return type
     *
     * @deprecated ince version 5
     */
    //      public function getId($Fields = "*", $Table = "", $Where = "", $debug = false)
    //      {
    //          $QueryString = "SELECT $Fields FROM $Table WHERE $Where";
    //
    //            $this->runQuery($QueryString, $debug);
    //            // in caso di errore non viene eseguita questa parte
    //            if ($this->QueryStatus == true) {
    //                $this->ResultStatus = self::QUERY_OK_SELECT;
    //            } else {
    //                $this->ResultStatus = self::QUERY_ERROR_SELECT;
    //            }
    //      }

    /**
     * Funzione che restituisce il valore massimo + 1 in bae agli argomenti
     *
     * @param  type $Table
     * @param  type $Field
     * @param  type $Where
     * @return type
     *
     * @deprecated since version 5.6.2
     */
    //        public function getMax($Table, $Field, $Where = "", $debug = false)
    //        {
    //            $QueryString = "SELECT Max($Field) as NMax FROM $Table WHERE $Where";
    //
    //            $this->runQuery($QueryString, $debug);
    //            // in caso di errore non viene eseguita questa parte
    //            if ($this->QueryStatus == true) {
    //                $this->ResultStatus = self::QUERY_OK_SELECT;
    //            } else {
    //                $this->ResultStatus = self::QUERY_ERROR_SELECT;
    //            }
    //
    //            // restituisco il valore
    //            return $this->Results['NMax'] + 1;
    //        }
    // --- opzionali FINE ---

    /**
     * Metodo che estrae i dati di un oggetto in base all'id o ai parametri forniti:
     *
     * @param mixed $mixed Variabile contenente il dato da recuperare, puo' essere:
     *                     null  - viene restituito l'elemento indicato nel costruttore/setParams
     *                     int   - viene restituito l'elemento con l'id corrispondente a quello indicato
     *                     array - se si tratta di una singola coppia chiave => valore viene usata
     *                     la stessa come condizione WHERE
     *                     array - se presente piu' di un elemento viene usato l'intero array come
     *                     condizione WHERE IN (...)
     *
     * @return array contenente il resultset richiesto in base ai parametri.
     */
    final public function get($mixed = null)
    {
        $queryString = "SELECT * FROM `" . $this->Table . "` WHERE ";
        $queryString .= $this->compileCondition($mixed);

        $this->runQuery($queryString);

        return $this->Results;
    }

    /**
     * Compone una stringa per la condizione WHERE in base ai parametri indicati
     *
     * @see   $this->get()
     * @param mixed $mixed
     *
     * @return string
     */
    final private function compileCondition($mixed = null)
    {
        $computedString = "";

        if (is_null($mixed) && filter_var($this->ObjectId, FILTER_VALIDATE_INT)) {
            $computedString .= "id = '" . $this->ObjectId . "'";
        } elseif (filter_var($mixed, FILTER_VALIDATE_INT)) {
            $computedString .= "id = '$mixed'";
        } elseif (is_array($mixed) && count($mixed) == 1) {
            $key            = array_keys($mixed);
            $key            = $key[0];
            $computedString .= "$key = '" . $mixed[$key] . "'";
        } elseif (is_array($mixed) && count($mixed) > 1) {
            $computedString .= "id IN(" . join(",", $mixed) . ")";
        } else {
            exit("[DEBUG][" . __METHOD__ . "]: Argomento '" . print_r(
                            $mixed, true
                    ) . "' non valido per il metodo.<br />");
        }

        //print "[DEBUG][" . __METHOD__ . "]: computedString: $computedString\n";
        return $computedString;
    }

    /**
     * Metodo che calcola il prossimo id negativo
     * eseguendo il lock della tabella e verificando l'id più basso -1.
     *
     * @param  string $Field optional - nome del campo sul quale eseguire la verifica (default id)
     * @return int    id
     */
    final private function getNextMinId($Field = "id")
    {
        $queryString = "SELECT (MIN($Field)-1) AS value FROM `" . $this->Table . "` LOCK IN SHARE MODE";
        $this->runQuery($queryString);

        return $this->Results[0]['value'];
    }

    /**
     * Metodo che calcola il prossimo id positivo
     * eseguendo il lock della tabella e verificando l'id più alto +1
     *
     * @param  string $Field optional - nome del campo sul quale eseguire la verifica (default id)
     * @return int    id
     */
    final private function getNextMaxId($Field = "id")
    {
        $queryString = "SELECT (MAX($Field)-1) AS value FROM `" . $this->Table . "` LOCK IN SHARE MODE";
        $this->runQuery($queryString);

        return $this->Results[0]['value'];
    }

    /**
     * Metodo che calcola il prossimo id negativo
     * eseguendo il lock della tabella e verificando l'id più basso -1.
     * e lo imposta sulla proprietà corrispondente.
     * Imposta inoltre in automatico il flag ForceInsert a true.
     *
     * @param  string $Field optional - nome del campo sul quale eseguire la verifica (default id)
     * @return $this
     */
    final private function setNextMinId($Field = "id")
    {
        $this->ObjectId    = $this->getNextMinId($Field);
        $this->ForceInsert = true;

        return $this;
    }

    /**
     * Metodo che calcola il prossimo id positivo
     * eseguendo il lock della tabella e verificando l'id più alto +1
     * e lo imposta sulla proprietà corrispondente.
     * Imposta inoltre in automatico il flag ForceInsert a true.
     *
     * @param  string $Field optional - nome del campo sul quale eseguire la verifica (default id)
     * @return $this
     */
    final private function setNextMaxId($Field = "id")
    {
        $this->ObjectId    = $this->getNextMaxId($Field);
        $this->ForceInsert = true;

        return $this;
    }

    /**
     * Metodo che estrae dalla tabella indicata la lista dei campi disponibili
     * per validare le operazioni sul db, ed esclude dalla lista determinati
     * campi se fortniti dall'utente.
     *
     * Nota: rifacimento della funzione globale gears_get_field_allowed($obj,$deleteKey='id')
     */
    final private function getTableFields()
    {
        // Verifico se è presente in APC la struttura della tabella attuale
        if (!ae()->get_memory(self::TABLE_APC_PREFIX . $this->Table)) {
            // I dati non sono presenti, rigenero l'informazione sull'APC
            $this->TableColumns = $this->runQuery("SHOW COLUMNS FROM `" . $this->Table . "`");

            foreach ($this->TableColumns as $value) {
                $list[] = $value['Field'];
            }

            // Memorizzo la struttura con chiave univoca in APC
            ae()->load_memory(
                    self::TABLE_APC_PREFIX . $this->Table . "_field_list",
                    $list, 3600
            );
            ae()->load_memory(
                    self::TABLE_APC_PREFIX . $this->Table, $this->TableColumns,
                    3600
            );
        } else {
            // I dati sono presenti, li prendo da APC e non eseguo la query
            $list               = ae()->get_memory(self::TABLE_APC_PREFIX . $this->Table . "_field_list");
            $this->TableColumns = ae()->get_memory(self::TABLE_APC_PREFIX . $this->Table);
            //pr($this->TableColumns);
        }

        $this->TableFieldsAllowed = $list;
        unset($list);

        return $this;
    }

    /**
     * Metodo  che estrae dall'array dei parametri i valori e li associa
     * ai campi reali della tabella di destinazione.
     */
    final private function associateFields2Params()
    {
        foreach ($this->Params as $key => $value) {
            if (in_array($key, $this->TableFieldsAllowed)) {
                if (isset($this->ConfigVars['field_type'][$key])) {
                    $type = $this->ConfigVars['field_type'][$key];
                } elseif (isset($this->ConfigVars['type_default'])) {
                    $type = $this->ConfigVars['type_default'];
                } else {
                    $type = 'standard';
                }

                switch ($type) {
                    case 'datetime':
                    case 'date':
                        $this->FieldVars[$key] = gears_date_validate(
                                $value, $type
                        );
                        break;

                    default:
                        $this->FieldVars[$key] = $value;
                }

                // delego al metodo la validazione dei campi/db
                $this->validateFields($key);
            }

            if (in_array($key, $this->ConfigVars['commands'])) {
                $this->CommandVars[$key] = $value;
            }
        }

        return $this;
    }

    /**
     * Metodo che verifica se i dati forniti per i campi sono coerenti
     * con il tipo di dato atteso dalla tabella sia per quanto riguarda il
     * tipo di valore fornito int/string ecc e anche per quanto riguarda la
     * lunghezza massima dei dati ospitabili dalla tabella.
     *
     * @param  string $FieldName required - Nome del campo sul quale inserire il valore e quindi da validare.
     * @return \Database
     */
    final private function validateFields($FieldName)
    {
        // Array search multidimensionale by id
        foreach ($this->TableColumns as $key => $val) {
            if ($val['Field'] === $FieldName) {
                //pr("type: " . $val['Type']);
                if (preg_match("((\w+)\(*(\d*)\)*(.*))", $val['Type'], $match)) {
                    $FieldType   = $match[1];
                    $FieldLenght = $match[2] > 0 ? $match[2] : false;
                } else {
                    exit("[DEBUG][" . __METHOD__ . "]: FieldType '" . $val['Type'] . "' fallita regexp.<br />");
                }

                // campo trovato, eseguo la validazione
                switch ($FieldType) {
                    case 'datetime':
                    case 'timestamp':
                    case 'date':
                    case 'blob':
                        // casi da saltare
                        break;

                    case 'int':
                    case 'bigint':
                    case 'tinyint':
                        $this->FieldVars[$FieldName] = filter_var(
                                $this->FieldVars[$FieldName],
                                FILTER_VALIDATE_INT
                        );
                        if ($FieldLenght && strlen($this->FieldVars[$FieldName]) > $FieldLenght) {
                            exit("[DEBUG][" . __METHOD__ . "]: Lunghezza campo '$FieldName' non valida, lunghezza attuale "
                                    . strlen($this->FieldVars[$FieldName]) . ", lunghezza massima $FieldLenght.<br />");
                        }
                        break;

                    case 'varchar':
                    case 'char':
                        $this->FieldVars[$FieldName] = filter_var(
                                $this->FieldVars[$FieldName],
                                FILTER_SANITIZE_STRING
                        );
                        if ($FieldLenght && strlen($this->FieldVars[$FieldName]) > $FieldLenght) {
                            exit("[DEBUG][" . __METHOD__ . "]: Lunghezza campo '$FieldName' non valida, lunghezza attuale "
                                    . strlen($this->FieldVars[$FieldName]) . ", lunghezza massima $FieldLenght.<br />");
                        }
                        break;

                    case 'text':    // campo testo, eseguo solo la validazione dato che non ha limite
                    case 'mediumtext':
                    case 'tinytext':
                        $this->FieldVars[$FieldName] = filter_var(
                                $this->FieldVars[$FieldName],
                                FILTER_SANITIZE_STRING
                        );
                        break;

                    default:
                        exit("[DEBUG][" . __METHOD__ . "]: FieldType '$FieldType' sconosciuto<br />");
                }
            }
        }

        return $this;
    }

}

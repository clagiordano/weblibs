<?php

namespace clagiordano\weblibs\php;

/**
 * Class to perform simple and complex curl request in easy way.
 *
 * @version 1.0.1
 * @author Claudio Giordano <claudio.giordano@autistici.org>
 * @since 2015-09-01
 * @category Network
 */
class RestClient
{
    const CONNECTION_TIMEOUT = 30;
    const TIMEOUT = 200;

    private $requestURL = null;
    private $requestParams = [];
    private $authenticationToken = null;
    private $requestHeaders = [];
    private $useLocalCookie = false;
    private $localCookieName = null;
    private $requestBaseUrl = null;

    /**
     * @return null
     */
    public function getRequestBaseUrl()
    {
        return $this->requestBaseUrl;
    }

    /**
     * Imposta l'url base per le successive chiamate
     *
     * @param null $requestBaseUrl
     */
    public function setRequestBaseUrl($requestBaseUrl)
    {
        $this->requestBaseUrl = filter_var($requestBaseUrl, FILTER_SANITIZE_URL);
    }

    /**
     * @return boolean
     */
    public function isUseLocalCookie()
    {
        return (boolean)$this->useLocalCookie;
    }

    /**
     * @param boolean $useLocalCookie
     */
    public function setUseLocalCookie($useLocalCookie)
    {
        $this->useLocalCookie = $useLocalCookie;
    }

    /**
     * @param string $headerKey
     * @return mixed $results
     */
    public function getRequestHeaders($headerKey = null)
    {
        if (!is_null($headerKey)) {
            $results = isset($this->requestHeaders[$headerKey]) ? $this->requestHeaders[$headerKey] : null;
        } else {
            $results = $this->requestHeaders;
        }

        return $results;
    }

    /**
     * @param array $requestHeaders
     */
    public function setRequestHeaders(array $requestHeaders = [])
    {
        $this->requestHeaders = $requestHeaders;
    }

    /**
     * @return null
     */
    public function getAuthenticationToken()
    {
        return $this->authenticationToken;
    }

    /**
     * Imposta l'header corretto per l'autenticazione
     * in formato bearer es: Authorization: Bearer mytoken123
     *
     * @param string $authenticationToken
     */
    public function setAuthenticationToken($authenticationToken)
    {
        $this->authenticationToken = $authenticationToken;
        $this->requestHeaders['WWW-Authenticate'] = "Bearer {$authenticationToken}";
        //$this->requestHeaders['Authorization'] = "Bearer {$authenticationToken}";
    }

    /**
     * @param string $requestURL
     * @param array $requestParams
     */
    public function __construct($requestURL = null, array $requestParams = [])
    {
        $this->setRequestURL($requestURL);
        $this->setRequestParams($requestParams);
    }

    /**
     * @return null
     */
    public function getRequestURL()
    {
        return $this->requestURL;
    }

    /**
     * @param null $requestURL
     */
    public function setRequestURL($requestURL = null)
    {
        $this->requestURL = $requestURL;
    }

    /**
     * @param string $paramKey
     *
     * @return mixed $results
     */
    public function getRequestParams($paramKey = null)
    {
        if (!is_null($paramKey)) {
            $results = isset($this->requestParams[$paramKey]) ? $this->requestParams[$paramKey] : null;
        } else {
            $results = $this->requestParams;
        }

        return $results;
    }

    /**
     * @param array $requestParams
     */
    public function setRequestParams(array $requestParams = [])
    {
        $this->requestParams = $requestParams;
    }

    /**
     * Set local cookie name
     */
    public function setLocalCookieName($cookieName)
    {
        $this->localCookieName = filter_var($cookieName, FILTER_SANITIZE_STRING);
    }

    /**
     * Get local cookie name
     */
    public function getLocalCookieName()
    {
        return $this->localCookieName;
    }

    /**
     * Perform real curl request.
     *
     * @param boolean $debugMode
     */
    public function doRequest($debugMode = false)
    {
        $postVars = [];

        if (is_null($this->requestURL)) {
            throw new \Exception(__METHOD__ . ": Invalid requestURL '{$this->requestURL}'");
        }

        if (count($this->requestParams) > 0) {
            $postVars = http_build_query($this->requestParams);
        }

        $curlHandle = curl_init();

        curl_setopt($curlHandle, CURLOPT_URL, $this->requestBaseUrl . $this->requestURL);

        /**
         * Se forniti imposta gli header custom
         */
        if (count($this->requestHeaders) > 0) {
            // Preparo l'array degli header
            $requestHeaders = [];
            foreach ($this->requestHeaders as $hKey => $hValue) {
                $requestHeaders[] = "$hKey: $hValue";
            }

            $requestHeaders[] = "Content-Type: application/json";
            curl_setopt($curlHandle, CURLOPT_HTTPHEADER, $requestHeaders);
        }

        /**
         * Se si è scelto di usare l'autenticazione tramite cookie allora utilizza
         * quello dell'utente
         */
        if ($this->useLocalCookie) {
            $cookieString = "{$this->localCookieName}=" . filter_input(INPUT_COOKIE, $this->localCookieName);
            curl_setopt($curlHandle, CURLOPT_COOKIE, $cookieString);
        }

        if ($debugMode) {
            curl_setopt($curlHandle, CURLOPT_VERBOSE, true);
            curl_setopt($curlHandle, CURLOPT_HEADER, 1);
        }

        if (count($this->requestParams) > 0) {
            curl_setopt($curlHandle, CURLOPT_POST, count($this->requestParams));
            curl_setopt($curlHandle, CURLOPT_POSTFIELDS, $postVars);
        }

        curl_setopt($curlHandle, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curlHandle, CURLOPT_CONNECTTIMEOUT, self::CONNECTION_TIMEOUT);
        curl_setopt($curlHandle, CURLOPT_TIMEOUT, self::TIMEOUT);

        $requestResult = curl_exec($curlHandle);
        curl_close($curlHandle);

        return $requestResult;
    }
}

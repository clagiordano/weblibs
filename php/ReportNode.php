<?php

namespace clagiordano\weblibs\php;

/**
 * ReportNode
 *
 * @version  1.3
 * @author   Claudio Giordano <claudio.giordano@autistici.org>
 * @since    2015-03-29
 * @category Image
 * @package  weblibs
 */
class ReportNode
{
    private $NodeId     = null;
    private $MarkupHtml = null;

    /**
     * @param array $data
     */
    public function __construct(array $data = [])
    {
        foreach ($data as $key => $value) {
            $this->$key = $value;
        }

        return $this;
    }

    /**
     * Magic get method
     *
     * @param string $name
     * @return string
     */
    public function __get($name)
    {
        return $this->$name;
    }

    /**
     * Magic set method
     *
     * @param string $name
     * @param string $value
     * @throws \Exception
     */
    public function __set($name, $value)
    {
        if ($name == "nodeId") {
            throw new \Exception(__METHOD__ . ': Invalid property name "' . $name . '"');
        }

        if ($name == "markupHtml") {
            throw new \Exception(__METHOD__ . ': Invalid property name "' . $name . '"');
        }

        //$this->$Name = filter_var($Value, FILTER_SANITIZE_STRING, FILTER_FLAG_NO_ENCODE_QUOTES);
        $this->$name = $value;
    }

    /**
     * Render and return node as html
     *
     * return string markupHtml
     */
    public function render()
    {
        $this->MarkupHtml = "";

        if (isset($this->NodeId)) {
            $this->MarkupHtml .= '<div id= "Node' . $this->NodeId . '" class="reportNode">';
        } else {
            $this->MarkupHtml .= "<div>";
        }

        foreach ($this as $key => $value) {
            if ($key != "NodeId" && $key != "MarkupHtml") {
                $this->MarkupHtml .= "$value";
            }
        }

        $this->MarkupHtml .= "</div>";

        return $this->MarkupHtml;
    }
}

<?php

namespace clagiordano\weblibs\php;

/**
 * Interfaccia che descrive i metodi standard degli oggetti.
 *
 * @author Claudio Giordano <claudio.giordano@autistici.org>
 */
interface ActionInterface
{

    public function save(array $mixed = []);

    public function execute(array $mixed = []);
}

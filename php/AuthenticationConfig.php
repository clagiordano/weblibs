<?php

namespace clagiordano\weblibs\php;

/**
 * Classe di configurazione utenti
 *
 * @version  1.1
 * @author   Claudio Giordano <claudio.giordano@autistici.org>
 * @since    2014-11-08
 * @category Configuration
 */
class AuthenticationConfig
{
    /**
     * Configurazioni nomenclatura di sessione:
     */
    const SESSION_USER_SUFFIX = "_user";
    const SESSION_NAME_SUFFIX = "_sess";
    const SESSION_TIME_EXPIRE = 15; // Minutes
    // Messaggi vari
    const AUTH_OK_LOGIN       = "Login effettuato correttamente, redirect in corso...";
    const AUTH_ERROR_LOGIN    = "Errore, nome utente o password errati.";
    const AUTH_ERROR_NOUSER   = "Errore, utente non esistente o disattivato.";
    // Formati password
    const PASS_TYPE_MD5       = 0;    // vecchio formato md5
    const PASS_TYPE_BLOWFISH  = 1;    // nuovo formato blowfish

    /**
     * Configurazioni della classe relative al database:
     */
    const TABLE_USERS             = "tab_utenti";
    const TABLE_USERS_ID          = "id_user";
    const TABLE_USERS_USER        = "user";
    const TABLE_USERS_PASSWORD    = "pass";
    const TABLE_USERS_GROUP_ID    = "tipo";
    const TABLE_USERS_ENABLED     = "stato";
    const TABLE_USERS_STATUS_ID   = "id_stato";
    const TABLE_USERS_CITY_ID     = "id_city";     // utilizzato solo in alcuni casi
    const TABLE_USERS_DESCRIPTION = "desc_user";

    /**
     * Definizione tabella STATI UTENTE e relativi campi
     *
     * CREATE TABLE `tab_stato_utente` (
      `id_stato` int(11) NOT NULL AUTO_INCREMENT,
      `stato` varchar(20) NOT NULL,
      PRIMARY KEY (`id_stato`)
      )
     */
    const TABLE_USER_STATUS             = "tab_stato_utente";
    const TABLE_USER_STATUS_ID          = "id_stato";
    const TABLE_USER_STATUS_DESCRIPTION = "stato";

    /**
     * Definizione tabella GRUPPI e relativi campi
     *
     * CREATE TABLE `tab_tipi` (
      `id_tipo` int(11) NOT NULL AUTO_INCREMENT,
      `tipo` varchar(30) NOT NULL,
      PRIMARY KEY (`id_tipo`)
      ) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1 COMMENT='latin1_swedish_ci'
     */
    const TABLE_GROUPS       = "tab_tipi";
    const TABLE_GROUPS_ID    = "id_tipo";
    const TABLE_GROUPS_GROUP = "tipo";

    /**
     * Definizione tabella MENU e relativi campi
     *
     * CREATE TABLE `tab_menu` (
      `id_menu` int(11) NOT NULL AUTO_INCREMENT,
      `link` varchar(200) NOT NULL,
      `titolo` varchar(30) NOT NULL,
      `perm` varchar(30) NOT NULL DEFAULT '',
      `tip` varchar(50) NOT NULL,
      `titolo_pag` varchar(50) NOT NULL,
      `gruppo` varchar(20) NOT NULL,
      `act` varchar(30) NOT NULL,
      `page` varchar(100) NOT NULL,
      `titolo_gruppo` varchar(50) NOT NULL,
      `ordine` int(5) NOT NULL,
      `ordine_gruppo` varchar(45) DEFAULT NULL,
      PRIMARY KEY (`id_menu`)
      ) ENGINE=MyISAM AUTO_INCREMENT=47 DEFAULT CHARSET=latin1 COMMENT='latin1_swedish_ci'
     */
    const TABLE_MENU             = "tab_menu";
    const TABLE_MENU_ID          = "id_menu";
    const TABLE_MENU_LINK        = "link";
    const TABLE_MENU_TITLE       = "titolo";
    const TABLE_MENU_PERMISSION  = "perm";
    const TABLE_MENU_TIP         = "tip";
    const TABLE_MENU_PAGE_TITLE  = "titolo_pag";
    const TABLE_MENU_GROUP       = "gruppo";
    const TABLE_MENU_ACTION      = "act";
    const TABLE_MENU_PAGE        = "page";
    const TABLE_MENU_GROUP_TITLE = "titolo_gruppo";
    const TABLE_MENU_ORDER       = "ordine";
    const TABLE_MENU_GROUP_ORDER = "ordine_gruppo";

    /**
     * Definizione vista LOG
     *
     *  drop view if exists view_connections;
      create view view_connections as (select
      s.action,
      s.data,
      s.ip,
      s.platform,
      s.browser,
      s.version,
      s.arch,
      u.user,
      t.tipo

      from
      tab_utenti u,
      tab_tipi t,
      tab_session s

      where
      u.tipo = t.id_tipo
      and
      u.id_user = s.id_user);
     */
}

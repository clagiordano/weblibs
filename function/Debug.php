<?php

/*
 *      Debug.php
 *
 *      Copyright 2008 Claudio Giordano <claudio.giordano@autistici.org>
 *  Funzioni globali sostituite ed ottimizzate/potenziate dalla classe Logger
 */

define("DeveloperMailAddress", "claudio.giordano@autistici.org");

function StampaErr($Command, $Note = "-")
{
    $ln            = "%0A";
    $tab           = "%20%20%20%20";
    $ErrorMsgStyle = "padding: 10px; font-size: 9pt; font-weight: bold;"
            . "color: red; border: 2px dashed red;"
            . "width: 40%; height: auto;"
            . "top: 10%; left: 10%; position: absolute;";
    $ErrorMail     = "Si è verificato un errore sulla query:"
            . "$ln $ln L'errore riporta: $ln $tab (" . mysql_errno() . ") " . mysql_error()
            . "$ln $ln L'errore e' stato generato da: $ln $tab $Command"
            . "$ln $ln Nota relativa all'errore: $ln $tab $Note";

    $ErrorBox = '<div style="' . $ErrorMsgStyle . '">'
            . "Si è verificato un errore sulla query:<br /><br />"
            . "L'errore riporta: <br />"
            . "&nbsp;&nbsp;&nbsp;&nbsp; (" . mysql_errno() . ") " . mysql_error() . "<br /><br />"
            . "L'errore e' stato generato da: <br />"
            . "&nbsp;&nbsp;&nbsp;&nbsp;$Command <br /><br />"
            . "Nota relativa all'errore: <br />"
            . "&nbsp;&nbsp;&nbsp;&nbsp;$Note <br /><br />"
            . "<a href=\"mailto:" . DeveloperMailAddress . "?Subject=[BugReport]"
            . "&body=" . $ErrorMail
            . '" title="invia segnalazione bug">Invia la segnalazione dell\'errore.</a>';

    echo $ErrorBox;
}

function LogToSession($Messaggio)
{
    if (!isset($_SESSION['debug'])) {
        $_SESSION['debug'] = array();
    }
    $Ora  = date("H:i:s");
    $Data = date("d/m/Y");
    $Sec  = time();
    array_push($_SESSION['debug'], "[$Sec] $Messaggio");
}

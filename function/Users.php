<?php
/*
 *      Users.php
 *
 *      Copyright 2008 Claudio Giordano <claudio.giordano@autistici.org>
 *
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 3 of the License, or
 *      (at your option) any later version.
 *
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 *      MA 02110-1301, USA.
 * 
 *      @deprecated since Authentication 1.0
 */

function Login($username, $pass, $session_name, $db)
{
    // Rimuovo dal db tutte le sessioni salvate piu' vecchie di 1 ora:
    /*
      $q_old_ses = "DELETE FROM tab_session WHERE data <= date_sub('"
      . date('Y-m-d H:i:s') . "', interval 1 HOUR);";
      $r_old_ses = mysql_query($q_old_ses, $db) or die (StampaErr($q_old_ses));
     */

    $Users = GetRows("tab_utenti, tab_tipi",
                     "tab_utenti.user = '"
            . $username . "' AND tab_utenti.tipo = tab_tipi.id_tipo", "", $db);

    // Se esiste un utente con questo nome verifica se la password e' corretta:
    if (count($Users) == 1) {
        if (md5($pass) == $Users[0]['pass']) {
            $_SESSION['id_user']              = $Users[0]['id_user'];
            // Memorizzo in sessione l'utente che ha effettato il login:
            $_SESSION[$session_name . 'user'] = $username;
            // Memorizzo in sessione l'id di sessione corrente:
            $_SESSION[$session_name . 'sess'] = session_id();
            // Memorizzo in sessione la descrizione dell'utente se presente:
            $_SESSION['desc_user']            = $Users[0]['desc_user'];
            //Memorizzo in sessione il tipo di utente:
            $_SESSION['id_tipo']              = $Users[0]['id_tipo'];
            $_SESSION['tipo']                 = $Users[0]['tipo'];
            $_SESSION['id_city']              = $Users[0]['id_city'];

            /*
             * Inserisco nel db i dati sul login effettuato:
             */
            $Valori = array('session_id' => session_id(),
                'id_user'    => $Users[0]['id_user'],
                'data'       => date('Y-m-d H:i:s'),
                'ip'         => $_SERVER['REMOTE_ADDR'],
                'action'     => 'LogIn',
                'user_agent' => $_SERVER['HTTP_USER_AGENT']);
            $Status = SaveRow($Valori, $Esclusioni, "tab_session", $db);
            ?>

            <label class="ok">Login effettuato, redirect in corso...</label>
            <script type="text/javascript">
                document.location.href = "home.php?act=welc"
            </script>
            <?php
        } else {
            $RESULT = '<label class="err">La password è sbagliata.</label>';
        }
    } else {
        /* FIXME 20/07/2010  Claudio Giordano: Generalizzare il messaggio? */
        $RESULT = '<label class="err">L\'utente non esiste.</label>';
    }

    return $RESULT;
}

function Logout($session, $session_name, $db)
{
    $Valori = array('session_id' => session_id(),
        'id_user'    => $Users[0]['id_user'],
        'data'       => date('Y-m-d H:i:s'),
        'ip'         => $_SERVER['REMOTE_ADDR'],
        'action'     => 'LogOut',
        'user_agent' => $_SERVER['HTTP_USER_AGENT']);
    $Status = SaveRow($Valori, $Esclusioni, "tab_session", $db);

    $_SESSION[$session_name . "user"] = "";
    $_SESSION[$session_name . "sess"] = "";
    session_destroy();
}

function SaveUser($username, $password, $tipo, $stato, $db)
{
    if (ExistUser($username, $db) == 0) {
        $md5_pass   = md5($password);
        $q_add_user = "INSERT INTO tab_utenti (user, pass, tipo, stato) VALUES "
                . "('$username', '$md5_pass', '$tipo', '$stato')";
        $r_add_user = mysql_query($q_add_user, $db) or die(StampaErr($q_add_user));

        $msg = "<label class=\"ok\">Salvataggio effettuato correttamente.</label>";
        return $msg;
    } else {
        $msg = "<label class=\"err\">Errore, è già presente un utente con questo nome.</label>";
        return $msg;
    }
}

function ModUser($username, $password, $tipo, $stato, $db, $tomod)
{
    $md5_pass   = md5($password);
    $q_mod_user = "UPDATE tab_utenti SET user='$username', pass='$md5_pass', "
            . "tipo='$tipo', stato='$stato' WHERE id_user='$tomod'";
    $r_mod_user = mysql_query($q_mod_user, $db) or die(StampaErr($q_mod_user));

    $msg = "<label class=\"ok\">Dati modificati correttamente.</label>";
    return $msg;
}

function ExistUser($username, $db)
{
    $q_user = "SELECT user FROM tab_utenti WHERE user = '$username'";
    $r_user = mysql_query($q_user, $db) or die(StampaErr($q_user));
    $n_user = mysql_affected_rows($db);

    mysql_free_result($r_user);
    return $n_user;
}

function RemoveUser($id, $db)
{
    $q_del = "DELETE FROM tab_utenti WHERE id_user = '$id' AND user != 'admin'";
    $r_del = mysql_query($q_del, $db) or die(StampaErr($q_del));
}

function GetIdUser($username, $db)
{
    // Restituisce l'id dell'utente attualmente in sessione:
    $q   = "SELECT id_user, user FROM tab_utenti WHERE user = '$username'";
    $r   = mysql_query($q, $db) or die(StampaErr($q));
    $row = mysql_fetch_assoc($r);

    mysql_free_result($r);
    return $row['id_user'];
}

function CheckAuth($id, $string)
{
    $perms = explode(";", $string);

    if (in_array($id, $perms)) {
        $flag[0] = "checked=\"checked\"";
        $flag[1] = 0;
    } else {
        $flag[1] = 1;
    }

    return $flag;
}
?>

<?php

/*
 *      Fatturazione.php
 *
 *      Copyright 2008 Claudio Giordano <claudio.giordano@autistici.org>
 *
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 *      MA 02110-1301, USA.
 */

function GetTipiFatt($db)
{
    /*
     * Seleziona i tipi di fattura:
     */

    $q    = "SELECT * FROM tab_tipi_fatt ORDER BY id_tipo_fatt";
    $r    = mysql_query($q, $db)
            or die(StampaErr($q, "[GetTipiFatt] "));
    $Tipi = array();

    while ($row = mysql_fetch_assoc($r)) {
        array_push($Tipi, $row);
    }
    mysql_free_result($r);

    return $Tipi;
}

function GetCausaliFatt($Tipo, $db)
{
    /*
     * Seleziona  le causali:
     */

    $q       = "SELECT * FROM tab_causali WHERE tipo = '$Tipo' ORDER BY causale";
    $r       = mysql_query($q, $db) or die(StampaErr($q, "[GetTCausaliFatt] "));
    $Causali = array();

    while ($row = mysql_fetch_assoc($r)) {
        array_push($Causali, $row);
    }
    mysql_free_result($r);

    return $Causali;
}

function GetFornitori($db)
{
    $q = "SELECT id_anag, rag_soc FROM tab_anagrafica  WHERE id_tipo = '1' ORDER BY rag_soc";
    $r = mysql_query($q, $db) or die(StampaErr($q, "[GetFornitori]"));
    $n = mysql_affected_rows($db);

    $Fornitori = array();

    while ($row = mysql_fetch_assoc($r)) {
        array_push($Fornitori, $row);
    }
    mysql_free_result($r);

    return $Fornitori;
}

function GetProgressivoFattura($db)
{
    $q   = "SELECT id_fattura FROM tab_fatture order by id_fattura desc limit 1";
    $r   = mysql_query($q, $db) or die(StampaErr($q, "[GetProgressivoFattura]"));
    $row = mysql_fetch_assoc($r);

    $progressivo = ($row['id_fattura'] + 1) . date('/y');
    mysql_free_result($r);

    return $progressivo;
}

function GetDescCausale($id, $db)
{
    $q   = "SELECT * FROM tab_causali WHERE id_causale = '$id'";
    $r   = mysql_query($q, $db) or die(StampaErr($q));
    $row = mysql_fetch_assoc($r);

    $descr = $row['causale'];

    return $descr;
}

function GetModPagamento($IdPag, $db)
{
    $q         = "SELECT pagamento FROM tab_mod_pagamento WHERE id_pagamento = '$IdPag'";
    $r         = mysql_query($q, $db) or die(StampaErr($q, '[GetModPagamento]'));
    $row       = mysql_fetch_assoc($r);
    $Pagamento = $row['pagamento'];
    mysql_free_result($r);

    return $Pagamento;
}

function GetDescTipo($Tipo, $db)
{
    $q        = "SELECT tipo_fatt FROM tab_tipi_fatt WHERE tipo = '$Tipo'";
    $r        = mysql_query($q, $db) or die(StampaErr($q, '[GetDescTipo]'));
    $row      = mysql_fetch_assoc($r);
    $TipoFatt = $row['tipo_fatt'];
    mysql_free_result($r);

    return $TipoFatt;
}

?>

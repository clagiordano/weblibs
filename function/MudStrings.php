<?php

error_reporting(E_ALL);
/*
 *      MudStrings.php
 *
 *      Copyright 2008 Claudio Giordano <claudio.giordano@autistici.org>
 *
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 3 of the License, or
 *      (at your option) any later version.
 *
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 *      MA 02110-1301, USA.
 */

function ConvertiTipoOgg($stringa)
{
    /*
     * 	Converte il tipo dell'oggetto da inglese nell'ostat
     * 	a italiano sul db:
     *
     * TODO: Aggiungere un campo aggiuntivo della tab_tipo_ogg con i nomi in inglese?
     */
    switch ($stringa) {
        case "Armor":
            $tipo = "Armatura";
            break;
        case "Container":
            $tipo = "Contenitore";
            break;
        case "Drinkcon":
            $tipo = "drinkcon"; //### ?
            break;
        case "Food":
            $tipo = "Cibo";
            break;
        case "Herb":
            $tipo = "Erba";
            break;
        case "Key":
            $tipo = "Chiave";
            break;
        case 'Lever':
            $tipo = "Leva";
            break;
        case "Light":
            $tipo = "Luce";
            break;
        case "Money":
            $tipo = "Monete";
            break;
        case "Pill":
            $tipo = "Pastiglia";
            break;
        case "Potion":
            $tipo = "Pozione";
            break;
        case "Salve":
            $tipo = "salve"; //### richiamo?
            break;
        case "Scroll":
            $tipo = "Pergamena";
            break;
        case "Staff":
            $tipo = "Bastone";
            break;
        case "Switch":
            $tipo = "Interruttore";
            break;
        case "Trap":
            $tipo = "Trappola";
            break;
        case "Treasure":
            $tipo = "Tesoro";
            break;
        case "Wand":
            $tipo = "Bacchetta magica";
            break;
        case "Weapon":
            $tipo = "Arma";
            break;
        default:
            $tipo = "Tipo non riconosciuto: |$stringa|";
            break;
    }
    return $tipo;
}

function ConvertiSlot($stringa)
{
    /*
     * 	Converte lo slot dell'oggetto da inglese nell'ostat
     * 	a italiano sul db:
     *
     * TODO: Aggiungere un campo aggiuntivo della tab_slot con i nomi in inglese?
     */
    switch ($stringa) {
        case "Take ears":
            $tipo = "Orecchie";
            break;
        default:
            $tipo = "Slot non riconosciuto: |$stringa|";
            break;
    }
    return $slot;
}

function GetIdRep($stringa)
{
    global $d;
    $row = $d->GetRows("*", "tab_rep", "rep like '%$stringa%'", "", "", 1);

    return $row['id_rep'];
}

function GetIdTipo($stringa)
{
    global $d;
    $row_tipo = $d->GetRows("*", "tab_tipo_ogg", "tipo like '%$stringa%'", "",
                            "", 1);

    return $row_tipo[0]['id_tipo_ogg'];
}

function LeggiOstat($stringa)
{
    // Se  tipo != armatura non ha ac.


    $ident = array();

    $ident['vnum'] = FindStr($stringa, "Vnum: ", " Tipo:", 0, 1);

    $ident['tipo'] = FindStr($stringa, "Tipo: ", " Quantita:", 0, 1);
    $ident['tipo'] = ConvertiTipoOgg($ident['tipo']);
    $ident['tipo'] = GetIdTipo($ident['tipo']);

    $ident['nome'] = FindStr($stringa, "Descrizione corta: ",
                             "Descrizione lunga: ", 0, 1);

    $ident['slot'] = FindStr($stringa, "Flag Wear : ", "Flag extra: ", 0, 1);
    $ident['slot'] = ConvertiSlot($ident['slot']);

    $ident['prop'] = FindStr($stringa, "Flag extra: ", "Flag Magici: ", 0, 1);

    $ident['modi'] = FindStr($stringa, "Flag Magici: ", "Numero: ", 0, 1);
    $ident['modi'] .= FindStr($stringa, "Affetto ",
                              "Indicatore di magia intrinseca:", 8, 0);

    $ident['peso'] = FindStr($stringa, "Peso: ", "Costo: ", 0, 1);
    $temp_peso     = explode("/", $ident['peso']);
    $ident['peso'] = $temp_peso[0];

    $ident['vale'] = FindStr($stringa, "Costo: ", "Affitto: ", 0, 1);

    $ident['rent'] = FindStr($stringa, "Affitto: ", "Wear_loc: ", 0, 1);

    $ident['rep'] = FindStr($stringa, "Drop%: ", " Mult : ", 0, 1);
    $ident['rep'] = GetIdRep($ident['rep']);

    $ident['liv'] = FindStr($stringa, "Livello: ", "Condizione: ", 0, 1);

    //~ $ident['danno'] = "";

    $voggetto = FindStr($stringa, "Valori oggetto: ",
                        "Chiavi descrizione primaria: ", 0, 3);
    $voggetto = explode(" ", $voggetto);
    $indice   = 1;
    foreach ($voggetto as $valore) {
        $val_id         = "val$indice";
        $ident[$val_id] = $valore;
        $indice++;
    }

    $_SESSION['tomod'] = "";
    return $ident;
}

function SalvaOggetto($username, $post)
{
    include("config.inc.php");

    if (trim($post['TXT_NOME'] != "")) {
        $post['TXT_NOME'] = ucfirst(trim($post['TXT_NOME']));
        $verifica         = VerificaUnicita(addslashes($post['TXT_NOME']), $db);

        if ($verifica == 0) {
            //echo "Non ci sono duplicati <br />";
            $nome  = addslashes(ucfirst(trim($post['TXT_NOME'])));
            $tipo  = ucfirst(trim($post['OPT_TIPO']));
            $peso  = ucfirst(trim($post['TXT_PESO']));
            $val   = ucfirst(trim($post['TXT_VAL']));
            $rent  = ucfirst(trim($post['TXT_RENT']));
            $liv   = ucfirst(trim($post['OPT_LIV']));
            $danno = ucfirst(trim($post['TXT_DANNO']));
            $ac    = ucfirst(trim($post['TXT_AC']));
            $prop  = addslashes(ucfirst(trim($post['TXT_PROP'])));
            $mod   = addslashes(ucfirst(trim($post['TXT_MOD'])));
            $area  = ucfirst(trim($post['OPT_AREA']));
            $mob   = addslashes(ucfirst(trim($post['TXT_MOB'])));
            $rep   = ucfirst(trim($post['OPT_REP']));
            $slot  = ucfirst(trim($post['OPT_SLOT']));
            $vnum  = ucfirst(trim($post['vnum']));
            $val1  = ucfirst(trim($post['val1']));
            $val2  = ucfirst(trim($post['val2']));
            $val3  = ucfirst(trim($post['val3']));
            $val4  = ucfirst(trim($post['val4']));
            $val5  = ucfirst(trim($post['val5']));
            $val6  = ucfirst(trim($post['val6']));

            $id_user = GetIdUser($username, $db);
            $data    = date('Y-m-d H:i:s');

            $q_save = "INSERT INTO
							tab_oggetti (nome, id_tipo, peso, vale, rent, liv, danno,
								ac, prop, modi, id_rep, id_area, mob, id_user, data,
								id_slot, val1, val2, val3, val4, val5, val6, vnum)
								VALUES
									('$nome', '$tipo', '$peso', '$val', '$rent', '$liv',
										'$danno', '$ac', '$prop', '$mod', '$rep', '$area',
										'$mob', '$id_user', '$data', '$slot', '$val1',
										'$val2', '$val3', '$val4', '$val5', '$val6',
										'$vnum')";

            $r_save = mysql_query($q_save, $db) or die(StampaErr($q_save));
            $msg    = "<label class=\"ok\">L'oggetto &#232; stato salvato correttamente.</label>";
            SessionLog("[Debug][SalvaOggetto]: $q_save", $db);
            //echo $q_save."<BR>";
        } else {
            $msg = "<label class=\"err\">L'oggetto &#232; gi&#224; presente nel database, salvataggio annullato.</label>";
        }
    } else {
        $msg = "<label class=\"err\">In campo nome dell'oggetto non pu&#242; essere vuoto.</label>";
    }
    mysql_close($db);
    return $msg;
}

function ModOggetto($username, $post, $tomod)
{
    include("config.inc.php");

    if (trim($post['TXT_NOME'] != "")) {
        $nome  = addslashes(ucfirst(trim($post['TXT_NOME'])));
        $tipo  = ucfirst(trim($post['OPT_TIPO']));
        $peso  = ucfirst(trim($post['TXT_PESO']));
        $val   = ucfirst(trim($post['TXT_VAL']));
        $rent  = ucfirst(trim($post['TXT_RENT']));
        $liv   = ucfirst(trim($post['OPT_LIV']));
        $danno = addslashes(ucfirst(trim($post['TXT_DANNO'])));
        $ac    = ucfirst(trim($post['TXT_AC']));
        $prop  = addslashes(ucfirst(trim($post['TXT_PROP'])));
        $mod   = addslashes(ucfirst(trim($post['TXT_MOD'])));
        $area  = ucfirst(trim($post['OPT_AREA']));
        $mob   = addslashes(ucfirst(trim($post['TXT_MOB'])));
        $rep   = ucfirst(trim($post['OPT_REP']));
        $slot  = ucfirst(trim($post['OPT_SLOT']));
        $vnum  = ucfirst(trim($post['vnum']));
        $val1  = ucfirst(trim($post['val1']));
        $val2  = ucfirst(trim($post['val2']));
        $val3  = ucfirst(trim($post['val3']));
        $val4  = ucfirst(trim($post['val4']));
        $val5  = ucfirst(trim($post['val5']));
        $val6  = ucfirst(trim($post['val6']));

        $id_user = GetIdUser($username, $db);
        $data    = date('Y-m-d H:i:s');

        $q_save = "UPDATE
						tab_oggetti
						SET
							nome='$nome', id_tipo='$tipo', peso='$peso',
							vale='$val', rent='$rent', liv='$liv', danno='$danno',
							ac='$ac', prop='$prop', modi='$mod', id_rep='$rep',
							id_area='$area', mob='$mob', id_user='$id_user',
							data='$data', id_slot='$slot', val1='$val1', val2='$val2',
							val3='$val3', val4='$val4', val5='$val5', val6='$val6',
							vnum='$vnum'
						WHERE
							id_oggetto='$tomod'";
// 		echo $q_save;
        $r_save = mysql_query($q_save, $db) or die(StampaErr($q_save));
        $msg    = "<label class=\"ok\">L'oggetto &#232; stato modificato correttamente.</label>";
        SessionLog("[Debug][ModOggetto]: $q_save", $db);
    } else {
        $msg = "<label class=\"err\">In campo nome dell'oggetto non pu&#242; essere vuoto.</label>";
    }
    mysql_close($db);
    return $msg;
}

function VerificaUnicita($stringa, $db)
{
    /*
     * Verifica l'unicit� dell'oggetto cercato
     * e restituisce il numero di risultati corrispondenti:
     */
    $q = "SELECT nome FROM tab_oggetti WHERE nome = '$stringa'";
    $r = mysql_query($q, $db) or die(StampaErr($q));
    $n = mysql_affected_rows($db);

    return $n;
}

function VerificaUnicitaArea($stringa, $db)
{
    /*
     * Verifica l'unicit� dell'area cercata
     * e restituisce il numero di risultati corrispondenti:
     */
    $q = "SELECT area FROM tab_aree WHERE area = '$stringa'";
    $r = mysql_query($q, $db) or die(StampaErr($q));
    $n = mysql_affected_rows($db);

    return $n;
}

function SaveArea($area, $stringa, $db)
{
    if (VerificaUnicitaArea($area, $db) == 0) {
        $q_add_area = "INSERT INTO tab_aree (area, stringa) VALUES ('$area', '$stringa')";
        $r_add_area = mysql_query($q_add_area, $db) or die(StampaErr($q_add_area));

        $msg = "<label class=\"ok\">Area salvata correttamente.</label>";
        SessionLog("[Debug][SaveArea]: $q_add_area", $db);
        return $msg;
    } else {
        $msg = "<label class=\"err\">Esiste gi&#224; un'area con questo nome, salvataggio annullato.</label>";
        return $msg;
    }
}

function SalvaLivello($id_razza, $id_classe, $livello)
{
    include("config.inc.php");
    $id_user = GetIdUser($db);
    $data    = date('Y-m-d H:i:s');

    // Verifica unicita' livello:
    $q_ver = "select * from tab_livelli where id_razza='$id_razza' AND id_classe='$id_classe'";
    $r_ver = mysql_query($q_ver, $db) or die(StampaErr($q_ver));
    $n_ver = mysql_affected_rows($db);

    // Se non c'e' una coppia classe livello salvata aggiungila:
    if ($n_ver == 0) {
        $q_save = "insert into tab_livelli (id_razza, id_classe, livello, id_user, data) "
                . "values ('$id_razza', '$id_classe', '$livello', '$id_user', '$data')";
        $r_save = mysql_query($q_save, $db) or die(StampaErr($q_save));
        $msg    = "<label class=\"ok\">Livello salvato correttamente.</label>";
        SessionLog("[Debug][SalvaLivello]: $q_save", $db);
    } else {
        $msg = "<label class=\"err\">Livello gi&#224; presente, salvataggio annullato.</label>";
    }

    mysql_close($db);
    return $msg;
}

function CalcoloBonus($ObjectList)
{
    global $d;

    $Bonus            = array();
    $Bonus['ac']      = 0;
    $Bonus['pf']      = 0;
    $Bonus['mana']    = 0;
    $Bonus['mov']     = 0;
    $Bonus['pfreg']   = 0;
    $Bonus['manareg'] = 0;
    $Bonus['movreg']  = 0;
    $Bonus['spfail']  = 0;
    $Bonus['min']     = 0;
    $Bonus['hit']     = 0;
    $Bonus['dam']     = 0;
    $Bonus['bash']    = 0;
    $Bonus['par']     = 0;
    $Bonus['peso']    = 0;
    $Bonus['for']     = 0;
    $Bonus['int']     = 0;
    $Bonus['sag']     = 0;
    $Bonus['des']     = 0;
    $Bonus['cos']     = 0;
    $Bonus['car']     = 0;
    $Bonus['fort']    = 0;


    foreach ($ObjectList as $key => $field) {
        if ($field != "-") {
            $obj = $d->GetRows("*", "view_oggetti", "id_oggetto = '$field'");
            //echo "[Debug]: slot: " .  $obj[0]['slot'] . " <br />";
            //echo "[Debug]: nome: " . $obj[0]['nome'] . ", ac: " . $obj[0]['ac'] . " <br />";
            //echo "[Debug]: modi: " . $obj[0]['modi'] . " <br />";
            // Calcolo bonus AC:
            if (preg_match("(Corpo \(armatura\)|Testa|Vita)i", $obj[0]['slot'])) {
                //echo "[Debug]: l'ac e' moltiplicata per 3. <br />";
                $Bonus['ac'] += ($obj[0]['ac'] * 3);
            } elseif (preg_match("(Corpo \(mantello\)|Gambe|Braccia)i",
                                 $obj[0]['slot'])) {
                //echo "[Debug]: l'ac e' moltiplicata per 2. <br />";
                $Bonus['ac'] += ($obj[0]['ac'] * 2);
            } else {
                //echo "[Debug]: l'ac non e' moltiplicata <br />";
                $Bonus['ac'] += $obj[0]['ac'];
            }

            if (preg_match("(punti ferita di ((-|)\d+))i", $obj[0]['modi'],
                           $Match)) {
                $Bonus['pf'] += $Match[1];
            }

            if (preg_match("(il mana di ((-|)\d+))i", $obj[0]['modi'], $Match)) {
                $Bonus['mana'] += $Match[1];
            }

            if (preg_match("(movimento di ((-|)\d+))i", $obj[0]['modi'], $Match)) {
                $Bonus['mov'] += $Match[1];
            }

            if (preg_match("(regain dei pf di ((-|)\d+))i", $obj[0]['modi'],
                           $Match)) {
                $Bonus['pfreg'] += $Match[1];
            }

            if (preg_match("(regain del mana di ((-|)\d+))i", $obj[0]['modi'],
                           $Match)) {
                $Bonus['manareg'] += $Match[1];
            }

            if (preg_match("(regain del movimenti di ((-|)\d+))i",
                           $obj[0]['modi'], $Match)) {
                $Bonus['movreg'] += $Match[1];
            }

            if (preg_match("(spell fail di ((-|)\d+))i", $obj[0]['modi'], $Match)) {
                $Bonus['spfail'] += $Match[1];
            }

            if (preg_match("(minaccia di ((-|)\d+))i", $obj[0]['modi'], $Match)) {
                $Bonus['min'] += $Match[1];
            }

            if (preg_match("(tiro per colpire di ((-|)\d+))i", $obj[0]['modi'],
                           $Match)) {
                $Bonus['hit'] += $Match[1];
            }

            if (preg_match("(tiro al danno di ((-|)\d+))i", $obj[0]['modi'],
                           $Match)) {
                $Bonus['dam'] += $Match[1];
            }

            if (preg_match("(capacita' di caricare di ((-|)\d+))i",
                           $obj[0]['modi'], $Match)) {
                $Bonus['bash'] += $Match[1];
            }

            if (preg_match("(capacita' di parare di ((-|)\d+))i",
                           $obj[0]['modi'], $Match)) {
                $Bonus['par'] += $Match[1];
            }

            if (preg_match("(peso di ((-|)\d+))i", $obj[0]['modi'], $Match)) {
                $Bonus['peso'] += $Match[1];
            }

            // Stat:
            if (preg_match("(forza di (\d+))i", $obj[0]['modi'], $Match)) {
                $Bonus['for'] += $Match[1];
            }

            if (preg_match("(intelligenza di ((-|)\d+))i", $obj[0]['modi'],
                           $Match)) {
                $Bonus['int'] += $Match[1];
            }

            if (preg_match("(saggezza di ((-|)\d+))i", $obj[0]['modi'], $Match)) {
                $Bonus['sag'] += $Match[1];
            }

            if (preg_match("(destrezza di ((-|)\d+))i", $obj[0]['modi'], $Match)) {
                $Bonus['des'] += $Match[1];
            }

            if (preg_match("(costituzione di ((-|)\d+))i", $obj[0]['modi'],
                           $Match)) {
                $Bonus['cos'] += $Match[1];
            }

            if (preg_match("(carisma di ((-|)\d+))i", $obj[0]['modi'], $Match)) {
                $Bonus['car'] += $Match[1];
            }

            if (preg_match("(fortuna di ((-|)\d+))i", $obj[0]['modi'], $Match)) {
                $Bonus['fort'] += $Match[1];
            }
        }
    }

    /* TODO: 2010-11-07 Claudio Giordano Resi immu e susce */


    foreach ($Bonus as $key => $field) {
        if (($field > 0) && (($key != "spfail") || ($key != "min"))) {
            $Bonus[$key] = "<label class=\"ok\">$field</label>";
        }

        if (($field < 0) && (($key == "spfail") || ($key == "min"))) {
            $Bonus[$key] = "<label class=\"ok\">$field</label>";
        }

        if (($field < 0) && (($key != "spfail") && ($key != "min"))) {
            $Bonus[$key] = "<label class=\"err\">$field</label>";
        }
    }

    return $Bonus;
}

function CalcoloBonusRdA($ObjectList)
{
    global $d;

    $Bonus            = array();
    $Bonus['ac']      = 0;
    $Bonus['pf']      = 0;
    $Bonus['mana']    = 0;
    $Bonus['mov']     = 0;
    $Bonus['pfreg']   = 0;
    $Bonus['manareg'] = 0;
    $Bonus['movreg']  = 0;
    $Bonus['spfail']  = 0;
    $Bonus['min']     = 0;
    $Bonus['hit']     = 0;
    $Bonus['dam']     = 0;
    $Bonus['bash']    = 0;
    $Bonus['par']     = 0;
    $Bonus['peso']    = 0;
    $Bonus['for']     = 0;
    $Bonus['int']     = 0;
    $Bonus['sag']     = 0;
    $Bonus['des']     = 0;
    $Bonus['cos']     = 0;
    $Bonus['car']     = 0;
    $Bonus['fort']    = 0;


    foreach ($ObjectList as $key => $field) {
        if ($field != "-") {
            $obj = $d->GetRows("*", "view_oggetti", "id_oggetto = '$field'");
            //echo "[Debug]: Slot: " . $obj[0]['slot'] . " <br />";
            //echo "[Debug]: Nome: " . $obj[0]['nome'] . ", AC: " . $obj[0]['ac'] . " <br />";
            //echo "[Debug]: Modi: " . $obj[0]['modi'] . " <br />";
            // Calcolo bonus AC:
            if (preg_match("(la classe d'armatura di ((-|)\d+))i",
                           $obj[0]['modi'], $Match)) {
                $Bonus['bonus_ac'] += ($Match[1] * -1);
                //echo "[Debug]: Bonus AC: " . $Bonus['bonus_ac'] . " <br />";
            }

            if (preg_match("(Corpo \(armatura\)|Testa|Vita)i", $obj[0]['slot'])) {
                //echo "[Debug]: l'ac e' moltiplicata per 3. <br />";
                $Bonus['ac'] += (($obj[0]['ac'] + $Bonus['bonus_ac']) * 3);
            } elseif (preg_match("(Schiena \(mantello\)|Gambe|Braccia)i",
                                 $obj[0]['slot'])) {
                //echo "[Debug]: l'ac e' moltiplicata per 2. <br />";
                $Bonus['ac'] += (($obj[0]['ac'] + $Bonus['bonus_ac']) * 2);
            } else {
                //echo "[Debug]: l'ac non e' moltiplicata <br />";
                $Bonus['ac'] += ($obj[0]['ac'] + $Bonus['bonus_ac']);
            }

            if (preg_match("(punti ferita di ((-|)\d+))i", $obj[0]['modi'],
                           $Match)) {
                $Bonus['pf'] += $Match[1];
            }

            if (preg_match("(mana di ((-|)\d+))i", $obj[0]['modi'], $Match)) {
                $Bonus['mana'] += $Match[1];
            }

            if (preg_match("(movimento di ((-|)\d+))i", $obj[0]['modi'], $Match)) {
                $Bonus['mov'] += $Match[1];
            }

            if (preg_match("(rigenerazione dei pf di ((-|)\d+))i",
                           $obj[0]['modi'], $Match)) {
                $Bonus['pfreg'] += $Match[1];
            }

            if (preg_match("(rigenerazione del mana di ((-|)\d+))i",
                           $obj[0]['modi'], $Match)) {
                $Bonus['manareg'] += $Match[1];
            }

            if (preg_match("(rigenerazione del movimento di ((-|)\d+))i",
                           $obj[0]['modi'], $Match)) {
                $Bonus['movreg'] += $Match[1];
            }

            if (preg_match("(l'esecuzione degli incantesimi di ((-|)\d+))i",
                           $obj[0]['modi'], $Match)) {
                $Bonus['spfail'] += ($Match[1]);
            }

            if (preg_match("(tiro per colpire di ((-|)\d+))i", $obj[0]['modi'],
                           $Match)) {
                $Bonus['hit'] += $Match[1];
            }

            if (preg_match("(danno di ((-|)\d+))i", $obj[0]['modi'], $Match)) {
                $Bonus['dam'] += $Match[1];
            }

            if (preg_match("(caricare di ((-|)\d+))i", $obj[0]['modi'], $Match)) {
                $Bonus['bash'] += $Match[1];
            }

            if (preg_match("(parare di ((-|)\d+))i", $obj[0]['modi'], $Match)) {
                $Bonus['par'] += $Match[1];
            }

            if (preg_match("(peso di ((-|)\d+))i", $obj[0]['modi'], $Match)) {
                $Bonus['peso'] += $Match[1];
            }

            // Stat:
            if (preg_match("(forza di (\d+))i", $obj[0]['modi'], $Match)) {
                $Bonus['for'] += $Match[1];
            }

            if (preg_match("(intelligenza di ((-|)\d+))i", $obj[0]['modi'],
                           $Match)) {
                $Bonus['int'] += $Match[1];
            }

            if (preg_match("(saggezza di ((-|)\d+))i", $obj[0]['modi'], $Match)) {
                $Bonus['sag'] += $Match[1];
            }

            if (preg_match("(destrezza di ((-|)\d+))i", $obj[0]['modi'], $Match)) {
                $Bonus['des'] += $Match[1];
            }

            if (preg_match("(costituzione di ((-|)\d+))i", $obj[0]['modi'],
                           $Match)) {
                $Bonus['cos'] += $Match[1];
            }

            if (preg_match("(carisma di ((-|)\d+))i", $obj[0]['modi'], $Match)) {
                $Bonus['car'] += $Match[1];
            }

            if (preg_match("(fortuna di ((-|)\d+))i", $obj[0]['modi'], $Match)) {
                $Bonus['fort'] += $Match[1];
            }
        }
    }

    /* TODO: 2010-11-07 Claudio Giordano Resi immu e susce */


    foreach ($Bonus as $key => $field) {
        if (($field > 0) && (($key != "spfail") || ($key != "min"))) {
            $Bonus[$key] = "<label class=\"ok\">$field</label>";
        }

        if (($field < 0) && (($key == "spfail") || ($key == "min"))) {
            $Bonus[$key] = "<label class=\"ok\">$field</label>";
        }

        if (($field > 0) && (($key == "spfail") || ($key == "min"))) {
            $Bonus[$key] = "<label class=\"err\">$field</label>";
        }

        if (($field < 0) && (($key != "spfail") && ($key != "min"))) {
            $Bonus[$key] = "<label class=\"err\">$field</label>";
        }
    }

    return $Bonus;
}

function LeggiValutaMob($Text)
{
    $Valuta = array();

    preg_match("((.*) appartiene alla razza (.*)\.)", $Text, $Match);
    $Valuta['nome']  = ucfirst($Match[1]);
    $Valuta['razza'] = ucfirst($Match[2]);

    preg_match("(Ha all'incirca (.*) dadi vita\.)", $Text, $Match);
    $Valuta['dadi'] = $Match[1];

    preg_match("(Vale approssimativamente (.*) punti esperienza\.)", $Text,
               $Match);
    $Valuta['exp'] = $Match[1];

    preg_match("(Ha circa (.*) punti ferita\.)", $Text, $Match);
    $Valuta['pf'] = $Match[1];

    if (preg_match_all("(Con il potere della tua magia comprendi che e' sensibile (.*)\.)",
                       $Text, $Match)) {
        $Valuta['susce'] = join(",\n", $Match[1]);
    } else {
        $Valuta['susce'] = "Nessuna";
    }

    if (preg_match_all("(La tua magia rivela una resistenza (.*)\.)", $Text,
                       $Match)) {
        $Valuta['resi'] = join(",\n", $Match[1]);
    } else {
        $Valuta['resi'] = "Nessuna";
    }

    if (preg_match_all("(Qualche cosa lo rende immune (.*)\.)", $Text, $Match)) {
        $Valuta['immu'] = join(",\n", $Match[1]);
    } else {
        $Valuta['immu'] = "Nessuna";
    }

    preg_match("(Ha circa (.*) attacchi per round\.)", $Text, $Match);
    $Valuta['attacchi'] = $Match[1];

    preg_match("(Ora sai che ognuno dei suoi colpi potrebbe infliggere (.*) punti di danno)",
               $Text, $Match);
    $Valuta['danno'] = ucfirst($Match[1]);

    $_SESSION['tomod'] = "";
    return $Valuta;
}

function LeggiValutaMobRda($Text)
{
    global $d;
    $Valuta = array();

    if (preg_match("((.*) appartiene alla razza (.*)\.)", $Text, $Match)) {
        $Valuta['nome']  = ucfirst($Match[1]);
        $Valuta['razza'] = ucfirst($Match[2]);
    }

    if (preg_match("(Credi sia alto (.*), per un peso di (.*)\.)", $Text, $Match)) {
        $Valuta['altezza'] = $Match[1];
        $Valuta['peso']    = $Match[2];
    }

    if (preg_match("(Dovrebbe valere circa (.*) punti esperienza\.)", $Text,
                   $Match)) {
        $Valuta['exp'] = $Match[1];
    }

    if (preg_match("(Pensi possa combattere come (.*) di livello (.*)\.)",
                   $Text, $Match)) {
        $temp_result         = $d->GetRows("id_classe", "tab_classi",
                                           "classe = '"
                . $Match[1] . "'", "", "", 1);
        $Valuta['id_classe'] = $temp_result[0]['id_classe'];
        $Valuta['livello']   = $Match[2];
    }


    if (preg_match("(Probabilmente ha (.*) punti ferita\.)", $Text, $Match)) {
        $Valuta['pf'] = $Match[1];
    }

    if (preg_match_all("((.*) suscettibile a: (.*)\.)", $Text, $Match)) {
        $Valuta['susce'] = $Match[2][0];
    } else {
        $Valuta['susce'] = "Nessuna";
    }

    if (preg_match_all("((.*) e' resistente a: (.*)\.)", $Text, $Match)) {
        $Valuta['resi'] = $Match[2][0];
    } else {
        $Valuta['resi'] = "Nessuna";
    }

    if (preg_match_all("((.*) appare immune a: (.*)\.)", $Text, $Match)) {
        $Valuta['immu'] = $Match[2][0];
    } else {
        $Valuta['immu'] = "Nessuna";
    }

    if (preg_match("(Puo' realizzare circa (.*) attacc(.*) per round\.)", $Text,
                   $Match)) {
        $Valuta['attacchi'] = $Match[1];
    }

    if (preg_match("(Ogni attacco causa intorno a (.*) punti ferita.)", $Text,
                   $Match)) {
        $Valuta['danno'] = ucfirst($Match[1]);
    }

    $_SESSION['tomod'] = "";
    return $Valuta;
}

function LeggiIdentificazione($Text)
{
    $Identificazione = array();
    $TipiOggetto     = "(vestito|armatura leggera|armatura media|armatura pesante|bacchetta magica|tesoro|pozione|contenitore per liquidi|arma)";

    $Identificazione['ac']    = "-";
    $Identificazione['danno'] = "-";
    $Identificazione['prop']  = "-";
    $Identificazione['modi']  = "-";
    $Oro                      = 0;
    $Arg                      = 0;
    $Ram                      = 0;

    if (preg_match("(L'oggetto '(.*)' consiste in (.*)\.)", $Text, $Match)) {
        $Identificazione['nome'] = ucfirst($Match[1]);
        preg_match($TipiOggetto, $Match[2], $Match);
        $Identificazione['tipo'] = GetIdTipo($Match[0]);
    } else {
        preg_match("(Guardi (.*):)", $Text, $Match);
        $Identificazione['nome'] = ucfirst($Match[1]);

        preg_match("(E' (.*)\.)", $Text, $Match);
        preg_match($TipiOggetto, $Match[1], $Match);
        $Identificazione['id_tipo'] = GetIdTipo($Match[0]);
    }

    if (preg_match("(Il suo peso e' (.*) e vale)", $Text, $Match)) {
        $Identificazione['peso'] = $Match[1];
    }

    if (preg_match("(\d+ monete d'oro)", $Text, $Match)) {
        preg_match("(\d+)", $Match[0], $Match);
        $Oro = $Match[0];
    }

    if (preg_match("(\d+ monete d'argento)", $Text, $Match)) {
        preg_match("(\d+)", $Match[0], $Match);
        $Arg = $Match[0];
    }

    if (preg_match("(\d+ monete di rame)", $Text, $Match)) {
        preg_match("(\d+)", $Match[0], $Match);
        $Ram = $Match[0];
    }

    $Identificazione['vale'] = $Oro . $Arg . $Ram;

    preg_match("(Il costo di affitto e' (.*)\.)", $Text, $Match);
    $Identificazione['rent'] = $Match[1];

    if (preg_match("(L'AC e' (.*)\.)", $Text, $Match)) {
        $Identificazione['ac'] = $Match[1];
    }

    if (preg_match("(Il danno va (.*)\.)", $Text, $Match)) {
        $Identificazione['danno'] = ucfirst($Match[1]);
    }


    preg_match("(Il livello da cui puo' essere usato e' (.*)\.)", $Text, $Match);
    $Identificazione['liv'] = $Match[1];

    preg_match("(Proprieta' speciali: (.*)\nModifica)", $Text, $Match);
    $Identificazione['prop'] = $Match[1];

    preg_match_all("(Modifica (.*)\.)", $Text, $Match);
    $Identificazione['modi'] = join(".\n", $Match[1]);

    return $Identificazione;
}

function LeggiIdentificazioneRda($Text)
{
    global $d;

    $Identificazione = array();
    //$TipiOggetto = "(vestito|armatura leggera|armatura media|armatura pesante|bacchetta magica|tesoro|pozione|contenitore per liquidi|arma)";

    $Identificazione['ac']    = "-";
    $Identificazione['danno'] = "-";
    $Identificazione['prop']  = "-";
    $Identificazione['modi']  = "-";
    $Oro                      = 0;
    $Arg                      = 0;
    $Ram                      = 0;

    if (preg_match("(L'oggetto '(.*)' e' di tipo (.*)\.)", $Text, $Match)) {
        $Identificazione['nome']    = ucfirst($Match[1]);
        //preg_match($TipiOggetto, $Match[2], $Match);
        $Identificazione['id_tipo'] = GetIdTipo($Match[2]);
    }

    // FIXME: SLOT ''Equipaggiabile stretto in mano.''
    /* if (preg_match("")
      {
      ;
      } */
    if (preg_match("(Equipaggiabile (.*)\.)", $Text, $Match)) {
        $words                      = explode(" ", $Match[1]);
        $temp                       = $d->GetRows("*", "tab_slot",
                                                  "slot like '%" . $words[count($words) - 1] . "%'",
                                                                                "",
                                                                                "",
                                                                                1);
        $Identificazione['id_slot'] = $temp[0]['id_slot'];
    }

    if (preg_match("(Pesa circa (.*) ed ha un valore di (\d+) monete d'oro.)",
                   $Text, $Match)) {
        $Identificazione['peso'] = $Match[1];
        $Identificazione['vale'] = $Match[2];
    }


    /* Assente sulle idente di rda
      preg_match("(Il costo di affitto e' (.*)\.)", $Text, $Match);
      $Identificazione['rent'] = $Match[1];
     */

    if (preg_match("(La classe d'armatura vale (.*)\.)", $Text, $Match)) {
        $Identificazione['ac'] = $Match[1];
    }

    if (preg_match("(L'arma causa (.*) punti ferita, (.*) in media\.)", $Text,
                   $Match)) {
        $Identificazione['danno'] = ucfirst($Match[1]) . ", media " . $Match[2];
    }

    if (preg_match("(E' classificata come (.*)\.)", $Text, $Match)) {
        $words                            = explode(" ", $Match[1]);
        $temp                             = $d->GetRows("*", "tab_tipo_danni",
                                                        "tipo_danno like '%" . $words[count($words) - 1] . "%'",
                                                                                            "",
                                                                                            "",
                                                                                            1);
        $Identificazione['id_tipo_danno'] = $temp[0]['id_tipo_danno'];
    }

    preg_match("(Puo' essere utilizzato dal livello (\d+)\.)", $Text, $Match);
    $Identificazione['liv'] = $Match[1];

    preg_match("(Proprieta' speciali: (.*)\n)", $Text, $Match);
    $Identificazione['prop'] = $Match[1];

    preg_match_all("(Modifica (.*)\.)", $Text, $Match);
    $Identificazione['modi'] = join(".\n", $Match[1]);

    /*
      echo "<pre>";
      print_r($Identificazione);
      echo "</pre>";
     */

    return $Identificazione;
}

function LeggiIdentificazioneAnathema($Text)
{
    global $d;
    $Identificazione = array();

    //~OK Gimnir ti dice 'Informazioni su una cintura di pelle di troll:'
    //~NO Gimnir ti dice 'Keywords: una cintura'
    //~OK Gimnir ti dice 'Tipo: armor'
    //~OK Gimnir ti dice 'Questo oggetto e': none' // extra FLAG
    //~OK Gimnir ti dice 'Questo oggetto ha le seguenti capacita' magiche: none' // magic flag
    //~OK Gimnir ti dice 'Questo oggetto e' di livello: 7 e costa d'affitto: 0.'
    //~OK Gimnir ti dice che la classe armatura base dell'oggetto e' 0.
    //~OK Effetti:       pf regen  per   1.
    //~OK Gimnir ti dice 'Informazioni su una bisaccia ingioiellata:'
    //~NO Gimnir ti dice 'Keywords: bisaccia ingioiellata'
    //~OK Gimnir ti dice 'Tipo: armor'
    //~OK Gimnir ti dice 'Questo oggetto e': magico antineutrale anticombattenti organico'
    //~OK Gimnir ti dice 'Questo oggetto ha le seguenti capacita' magiche: none'
    //~OK Gimnir ti dice 'Questo oggetto e' di livello: 51 e costa d'affitto: 0.'
    //~OK Gimnir ti dice che la classe armatura base dell'oggetto e' 8.

    $Identificazione['ac']      = "-";
    $Identificazione['danno']   = "-";
    $Identificazione['prop']    = "-";
    $Identificazione['modi']    = "-";
    $Identificazione['id_slot'] = "21";

    if (preg_match("(Gimnir ti dice (.*))", $Text, $Match)) {
        if (preg_match("(Gimnir ti dice 'Informazioni su (.*):)", $Text, $Match)) {
            $Identificazione['nome'] = $Match[1];
        }

        if (preg_match("(Gimnir ti dice 'Tipo: (.*)')", $Text, $Match)) {
            $Identificazione['id_tipo'] = GetIdTipo(trim($Match[1]));
        }

        if (preg_match("(Gimnir ti dice 'Questo oggetto e': (.*)')", $Text,
                       $Match)) {
            $Identificazione['prop'] = $Match[1];
        }

        if (preg_match("(Gimnir ti dice 'Questo oggetto ha le seguenti capacita' magiche: (.*)')",
                       $Text, $Match)) {
            $Identificazione['magic_flag'] = $Match[1];
        }

        if (preg_match("(Gimnir ti dice 'Questo oggetto e' di livello: (.*) e costa d'affitto: (.*)\.')",
                       $Text, $Match)) {
            $Identificazione['liv']  = trim($Match[1]);
            $Identificazione['rent'] = trim($Match[2]);
        }

        if (preg_match("(Gimnir ti dice che la classe armatura base dell'oggetto e' (.*)\.)",
                       $Text, $Match)) {
            $Identificazione['ac'] = $Match[1];
        }
    } else {
        if (preg_match("(Oggetto: (.*)\n)", $Text, $Match)) {
            $Identificazione['nome'] = ucfirst($Match[1]);
        }

        if (preg_match("(Vnum: (.*)\n)", $Text, $Match)) {
            $Identificazione['vnum'] = $Match[1];
        }

        if (preg_match("(Tipo: (.*)\n)", $Text, $Match)) {
            $Identificazione['id_tipo'] = GetIdTipo(trim($Match[1]));
        }

        if (preg_match("(Extra Flag: (.*)\n)", $Text, $Match)) {
            $Identificazione['prop'] = $Match[1];
        }

        if (preg_match("(Flag Magici: (.*)\n)", $Text, $Match)) {
            $Identificazione['magic_flag'] = $Match[1];
        }

        if (preg_match("(Affitto: (.*) Livello: (.*))", $Text, $Match)) {
            $Identificazione['rent'] = trim($Match[1]);
            $Identificazione['liv']  = trim($Match[2]);
        }

        if (preg_match("(Classe armatura: (.*))", $Text, $Match)) {
            $Identificazione['ac'] = $Match[1];
        }
    }

    if (preg_match("(Si indossa : prendibile (.*)\n)", $Text, $Match)) {
        $temp_var                   = $d->GetRows("*", "tab_slot",
                                                  "slot LIKE '%"
                . trim($Match[1]) . "%'", "", "", 1);
        $Identificazione['id_slot'] = $temp_var[0]['id_slot'];
    }

    if (preg_match_all("(Effetti: (.*))", $Text, $Match)) {
        $Identificazione['modi'] = "";
        foreach ($Match[1] as $key => $field) {
            $Identificazione['modi'] .= trim(str_replace(".", "", $field)) . ".\n";
        }
        //~ $Identificazione['modi'] = join(".\n", $Match[1]);
    }

    if (preg_match("(Spazi per le rune: (.*)\n)", $Text, $Match)) {
        $Identificazione['spazi_rune'] = $Match[1];
    }

    if (preg_match("(Il danno va (.*)\.)", $Text, $Match)) {
        $Identificazione['danno'] = $Match[1];
    }

    if (preg_match("(Abilita' richiesta: (.*))", $Text, $Match)) {
        $Identificazione['skill_required'] = $Match[1];
    }

    if (preg_match("(Tipo di danno: (.*))", $Text, $Match)) {
        $temp_var                         = $d->GetRows("*", "tab_tipo_danni",
                                                        "tipo_danno LIKE '%"
                . trim($Match[1]) . "%'", "", "", 1);
        $Identificazione['id_tipo_danno'] = $temp_var[0]['id_tipo_danno'];
    }

    if (preg_match_all("(Lancia l'incantesimo: '(.*)')", $Text, $Match)) {
        //~ $Identificazione['modi'] = "";
        //~ foreach ($Match[1] as $key => $field)
        //~ {
        //~ $Identificazione['modi'] .= trim($field) . ".\n";
        //~ }
        $Identificazione['casta'] = join(".\n", $Match[1]);
    }

    //~ echo "<pre>";
    //~ print_r($Identificazione);
    //~ echo "</pre>";

    return $Identificazione;
}

function LogBeautiferRda($Text)
{
    $Log   = "";
    $Rows  = explode("\n", $Text);
    $index = 0;

    foreach ($Rows as $key => $field) {
        // GD PARTICOLARI:
        if (preg_match("(^\[(.*)\] dice al gruppo '\[@\] (.*) \[@\]'\.)",
                       $field, $Match)) {
            $field = "<span style=\"color: blue;\">[</span>"
                    . "<span style=\"color: white;\">" . $Match[1] . "</span>"
                    . "<span style=\"color: blue;\">] dice al gruppo '</span>"
                    . "<span style=\"color: grey;\">[</span>"
                    . "<span style=\"color: #800517;\">@</span>"
                    . "<span style=\"color: grey;\">]</span>"
                    . "<span style=\"color: #00FF00;\"> " . $Match[2] . " </span>"
                    . "<span style=\"color: grey;\">[</span>"
                    . "<span style=\"color: #800517;\">@</span>"
                    . "<span style=\"color: grey;\">]</span>"
                    . "<span style=\"color: blue;\">'.</span><br />";
        }

        if (preg_match("(^\[(.*)\] dice al gruppo '\[\*\] (.*) \[\*\]'\.)",
                       $field, $Match)) {
            $field = "<span style=\"color: blue;\">[</span>"
                    . "<span style=\"color: white;\">" . $Match[1] . "</span>"
                    . "<span style=\"color: blue;\">] dice al gruppo '</span>"
                    . "<span style=\"color: red;\">[</span>"
                    . "<span style=\"color: yellow;\">*</span>"
                    . "<span style=\"color: red;\">]</span>"
                    . "<span style=\"color: yellow;\"> " . $Match[2] . " </span>"
                    . "<span style=\"color: red;\">[</span>"
                    . "<span style=\"color: yellow;\">*</span>"
                    . "<span style=\"color: red;\">]</span>"
                    . "<span style=\"color: blue;\">'.</span><br />";
        }

        if (preg_match("(^Dici al gruppo '\[\*\] (.*) \[\*\]'\.)", $field,
                       $Match)) {
            $field = "<span style=\"color: blue;\">Dici al gruppo '</span>"
                    . "<span style=\"color: red;\">[</span>"
                    . "<span style=\"color: yellow;\">*</span>"
                    . "<span style=\"color: red;\">]</span>"
                    . "<span style=\"color: yellow;\"> " . $Match[1] . " </span>"
                    . "<span style=\"color: red;\">[</span>"
                    . "<span style=\"color: yellow;\">*</span>"
                    . "<span style=\"color: red;\">]</span>"
                    . "<span style=\"color: blue;\">'.</span><br />";
        }

        if (preg_match("(^Dici al gruppo '\[@\] (.*) \[@\]'\.)", $field, $Match)) {
            $field = "<span style=\"color: blue;\">Dici al gruppo '</span>"
                    . "<span style=\"color: grey;\">[</span>"
                    . "<span style=\"color: #800517;\">@</span>"
                    . "<span style=\"color: grey;\">]</span>"
                    . "<span style=\"color: #00FF00;\"> " . $Match[1] . " </span>"
                    . "<span style=\"color: grey;\">[</span>"
                    . "<span style=\"color: #800517;\">@</span>"
                    . "<span style=\"color: grey;\">]</span>"
                    . "<span style=\"color: blue;\">'.</span><br />";
        }

        if (preg_match("(^\[(.*)\] dice al gruppo '(.*)'.)", $field, $Match)) {
            $field = "<span style=\"color: blue;\">[</span>"
                    . "<span style=\"color: white;\">" . $Match[1] . "</span>"
                    . "<span style=\"color: blue;\">] dice al gruppo '</span>"
                    . "<span style=\"color: blue;\">" . $Match[2] . "'.</span><br />";
        }

        if (preg_match("(^\[(.*)\] dice '(.*)'.)", $field, $Match)) {
            $field = "<span style=\"color: white;\">[</span>"
                    . "<span style=\"color: purple;\">" . $Match[1] . "</span>"
                    . "<span style=\"color: white;\">] dice '</span>"
                    . "<span style=\"color: white;\">" . $Match[2] . "'.</span><br />";
        }

        if (preg_match("(^Dici al gruppo '(.*)'.)", $field, $Match)) {
            $field = "<span style=\"color: blue;\">Dici al gruppo '</span>"
                    . "<span style=\"color: blue;\">" . $Match[1] . "'.</span><br />";
        }



        if (preg_match("(^(.*) e' circondat(.*) dalle fiamme!)", $field, $Match)) {
            $field = "<span style=\"color: red;\">"
                    . $Match[0] . " </span><br />";
        }

        if (preg_match("(^(.*) e' circondato da un vortice di energia!)",
                       $field, $Match)) {
            $field = "<span style=\"color: blue;\">"
                    . $Match[0] . " </span><br />";
        }

        // Santificazione
        if (preg_match("(^(.*) e' circondat(.*) da un'aura di energia negativa!|"
                        . "^(.*) e' circondat(.*) da una barriera di ombra e luce!|"
                        . "^(.*) e' avvolt(.*) da un'aura bianca!)", $field,
                       $Match)) {
            $field = "<span style=\"color: white;\">"
                    . $Match[0] . " </span><br />";
        }

        if (preg_match("(^L'attacco di (.*) e' perfetto!|Il tuo attacco e' perfetto!)",
                       $field, $Match)) {
            $field = "<span style=\"color: purple;\">"
                    . $Match[0] . "</span><br />";
        }

        if (preg_match("(^(.*)raggio mentale(.*)|"
                        . "^(.*)raggio psichico(.*))", $field, $Match)) {
            $field = "<span style=\"color: purple;\">"
                    . $Match[0] . "</span>";
        }

        if (preg_match("(^Incantesimi:|^------------|"
                        . "^(.*) viene circondat(.*) da un'aura rosa\.)",
                       $field, $Match)) {
            $field = "<span style=\"color: magenta;\">"
                    . $Match[0] . "</span><br />";
        }

        if (preg_match("(^Incantesimo: (.*), per (.*) round.)", $field, $Match)) {
            $field = "<span style=\"color: magenta;\">Incantesimo: </span>"
                    . "<span style=\"color: white;\">" . $Match[1] . "</span>"
                    . "<span style=\"color: magenta;\">, per </span>"
                    . "<span style=\"color: white;\">" . $Match[2] . "</span>"
                    . " <span style=\"color: magenta;\">round</span><br />";
        }

        if (preg_match("(^(.*) colpisce (.*) in pieno (.*))", $field, $Match)) {
            $field = "<span style=\"color: grey;\">" . $Match[1]
                    . " colpisce " . $Match[2] . "</span>"
                    . "<span style=\"color: white;\"> in pieno </span>"
                    . "<span style=\"color: grey;\">" . $Match[3] . "</span>";
        }

        if (preg_match("(^(.*) ti colpisce in pieno (.*))", $field, $Match)) {
            $field = "<span style=\"color: grey;\">" . $Match[1]
                    . " ti colpisce</span>"
                    . "<span style=\"color: white;\"> in pieno </span>"
                    . "<span style=\"color: grey;\">" . $Match[2] . "</span>";
        }

        if (preg_match("(^Colpisci (.*) in pieno (.*))", $field, $Match)) {
            $field = "<span style=\"color: grey;\">Colpisci " . $Match[1]
                    . "<span style=\"color: white;\"> in pieno </span>"
                    . "<span style=\"color: grey;\">" . $Match[2] . "</span>";
        }

        if (preg_match("(^(.*) taglia (.*) in pieno (.*))", $field, $Match)) {
            $field = "<span style=\"color: grey;\">" . $Match[1]
                    . " taglia " . $Match[2] . "</span>"
                    . "<span style=\"color: white;\"> in pieno </span>"
                    . "<span style=\"color: grey;\">" . $Match[3] . "</span>";
        }

        if (preg_match("(^(.*) trafigge (.*) in pieno (.*))", $field, $Match)) {
            $field = "<span style=\"color: grey;\">" . $Match[1]
                    . " trafigge " . $Match[2] . "</span>"
                    . "<span style=\"color: white;\"> in pieno </span>"
                    . "<span style=\"color: grey;\">" . $Match[3] . "</span>";
        }


        if (preg_match("(^(.*) massacra (.*))", $field, $Match)) {
            $field = "<span style=\"color: grey;\">" . $Match[1] . "</span>"
                    . "<span style=\"color: white;\"> massacra </span>"
                    . "<span style=\"color: grey;\">" . $Match[2] . "</span>";
        }

        if (preg_match("(^(.*) maciulla (.*))", $field, $Match)) {
            $field = "<span style=\"color: grey;\">" . $Match[1] . "</span>"
                    . "<span style=\"color: white;\"> maciulla </span>"
                    . "<span style=\"color: grey;\">" . $Match[2] . "</span>";
        }

        if (preg_match("(^(.*) devasta (.*))", $field, $Match)) {
            $field = "<span style=\"color: grey;\">" . $Match[1] . "</span>"
                    . "<span style=\"color: white;\"> devasta </span>"
                    . "<span style=\"color: grey;\">" . $Match[2] . "</span>";
        }

        if (preg_match("(^(.*) squarta (.*))", $field, $Match)) {
            $field = "<span style=\"color: grey;\">" . $Match[1] . "</span>"
                    . "<span style=\"color: white;\"> squarta </span>"
                    . "<span style=\"color: grey;\">" . $Match[2] . "</span>";
        }

        if (preg_match("(^Squarti (.*))", $field, $Match)) {
            $field = "<span style=\"color: white;\">Squarti </span>"
                    . "<span style=\"color: grey;\">" . $Match[1] . "</span>";
        }

        if (preg_match("(^(.*) demolisce (.*))", $field, $Match)) {
            $field = "<span style=\"color: grey;\">" . $Match[1] . "</span>"
                    . "<label style=\"color: grey;\"> demolisce </label>"
                    . "<span style=\"color: grey;\">" . $Match[2] . "</span>";
        }

        if (preg_match("(^(.*) polverizza (.*))", $field, $Match)) {
            $field = "<span style=\"color: grey;\">" . $Match[1] . "</span>"
                    . "<span style=\"color: #800517;\"> polverizza </span>"
                    . "<span style=\"color: grey;\">" . $Match[2] . "</span>";
        }

        if (preg_match("(^(.*) annichilisce (.*))", $field, $Match)) {
            $field = "<span style=\"color: grey;\">" . $Match[1] . "</span>"
                    . "<span style=\"color: yellow;\"> annichilisce </span>"
                    . "<span style=\"color: grey;\">" . $Match[2] . "</span>";
        }

        if (preg_match("(^La carica di (.*)\.|^La tua carica manda (.*)\.|"
                        . "^(.*) la tua carica lo travolge.|^(.*) la tua carica lo centra(.*)\.|"
                        . "^Una pozza di sangue ristagna macabramente sul terreno\.|"
                        . "^(.*) viene spazzato via PER SEMPRE, travolto dalla carica (.*)\.|"
                        . "^La terra riassorbe una pozza di sangue\.)", $field,
                       $Match)) {
            $field = "<span style=\"color: #800517;\">"
                    . $Match[0] . "</span><br />";
        }

        if (preg_match("(^(.*) e' mort(.*)! R.I.P.)", $field, $Match)) {
            $field = "<span style=\"color: white;\">"
                    . $Match[1] . " e' mort" . $Match[2]
                    . "! </span><span style=\"color: yellow;\">R.I.P.</span><br />";
        }

        if (preg_match("(^(.*) quando senti l'urlo di morte di (.*)\.)", $field,
                       $Match)) {
            $field = "<span style=\"color: magenta;\">"
                    . $Match[1] . " quando senti l'urlo di morte di </span>"
                    . "<span style=\"color: white;\">" . $Match[2] . "</span>"
                    . "<span style=\"color: magenta;\">.</span><br />";
        }

        if (preg_match("(^(.*) si riprende dalla carica e si rialza.|"
                        . "^(.*) si ricompone un poco e lentamente si rialza\.|"
                        . "^Sprofondi a piu' non posso nel fango\.\.\.)",
                       $field, $Match)) {
            $field = "<span style=\"color: #7B8000;\">"
                    . $Match[0] . "</span><br />";
        }

        if (preg_match("(^Fini schegge di ghiaccio (.*))", $field, $Match)) {
            $field = "<span style=\"color: cyan;\">"
                    . $Match[0] . "</span>";
        }

        if (preg_match("(^(.*) restituisce il colpo sotto forma di fiamme!)",
                       $field, $Match)) {
            $field = "<span style=\"color: red;\">"
                    . $Match[0] . "</span><br />";
        }

        if (preg_match("(^(.*) dall'energia della tua barriera!|"
                        . "^(.*) l'energia gli si riversa contro)", $field,
                       $Match)) {
            $field = "<span style=\"color: blue;\">"
                    . $Match[0] . "</span><br />";
        }

        if (preg_match("(^(.*) restituisce il colpo in taglienti schegge di ghiaccio!)",
                       $field, $Match)) {
            $field = "<span style=\"color: cyan;\">"
                    . $Match[0] . "</span><br />";
        }

        if (preg_match("(^Lo scudo di (.*) cresce d'intensita' quando (.*) per poco non lo colpisce!|"
                        . "^Il tuo scudo cresce d'intensita' quando (.*) per poco non ti colpisce!)",
                       $field, $Match)) {
            $field = "<span style=\"color: red;\">"
                    . $Match[0] . "</span><br />";
        }

        if (preg_match("(^(.*) e' avvolto in un manto di ghiaccio!)", $field,
                       $Match)) {
            $field = "<span style=\"color: cyan;\">"
                    . $Match[0] . "</span><br />";
        }

        if (preg_match("(^(.*) sembra sdoppiato, ne vedi (.*)!|"
                        . "^(.*) e' ingannat(.*) dalle immagini di (.*)\.|"
                        . "^(.*) centra una delle immagini di (.*)\.|"
                        . "^(.*) l'ultima immagine (.*))", $field, $Match)) {
            $field = "<span style=\"color: #006D4F;\">"
                    . $Match[0] . "</span><br />";
        }

        if (preg_match("(^Con un gran fragore l'esplosione (.*)\.)", $field,
                       $Match)) {
            $field = "<span style=\"color: red;\">"
                    . $Match[0] . "</span><br />";
        }

        if (preg_match("(^(.*) una densa sfera di fuoco, WOOOOOM!)", $field,
                       $Match)) {
            $field = "<span style=\"color: grey;\">" . $Match[1]
                    . "una densa sfera di fuoco, </span>"
                    . "<span style=\"color: #800517;\">W</span>"
                    . "<span style=\"color: red;\">O</span>"
                    . "<span style=\"color: yellow;\">OOO</span>"
                    . "<span style=\"color: red;\">O</span>"
                    . "<span style=\"color: #800517;\">M!</span><br />";
        }

        if (preg_match("(^(.*) crolla addosso a (.*) lasciandolo stordito!)",
                       $field, $Match)) {
            $field = "<label style=\"color: grey;\">"
                    . $Match[0] . "</label><br />";
        }

        if (preg_match("(^Vorresti smettere di SANGUINARE cosi' tanto!)",
                       $field, $Match)) {
            $field = "<span style=\"color: grey;\">Vorresti smettere di </span>"
                    . "<span style=\"color: red;\">SANGUINARE</span>"
                    . "<span> cosi' tanto!</span><br />";
        }

        if (preg_match("(^Capo Gruppo: (.*) \[PF\] \[MN\] \[MV\] \[VS\] \[Mst\]"
                        . "                        \[Razza\])", $field, $Match)) {
            $field = "<span style=\"color: white;\">Capo Gruppo: " . $Match[1] . " </span>"
                    . "<span style=\"color: grey;\">[</span>"
                    . "<span style=\"color: white;\">PF</span>"
                    . "<span style=\"color: grey;\">] </span>"
                    . "<span style=\"color: grey;\">[</span>"
                    . "<span style=\"color: white;\">MN</span>"
                    . "<span style=\"color: grey;\">] </span>"
                    . "<span style=\"color: grey;\">[</span>"
                    . "<span style=\"color: white;\">MV</span>"
                    . "<span style=\"color: grey;\">] </span>"
                    . "<span style=\"color: grey;\">[</span>"
                    . "<span style=\"color: white;\">VS</span>"
                    . "<span style=\"color: grey;\">] </span>"
                    . "<span style=\"color: grey;\">[</span>"
                    . "<span style=\"color: white;\">Mst</span>"
                    . "<span style=\"color: grey;\">]                        </span>"
                    . "<span style=\"color: grey;\">[</span>"
                    . "<span style=\"color: white;\">Razza</span>"
                    . "<span style=\"color: grey;\">]</span><br />";
        }

        // Disintegrazione
        if (preg_match("(^Disintegri (.*)|^(.*) raggio disintegrante (.*))",
                       $field, $Match)) {
            $field = "<label style=\"color: #00FF00;\">"
                    . $Match[0] . "</label>";
        }

        if (preg_match("(^La carica energetica si propaga (.*))", $field, $Match)) {
            $field = "<span style=\"color: white;\">"
                    . $Match[0] . "</span>";
        }

        // Cast
        if (preg_match("(^(.*) pronuncia le parole, '(.*)'.)", $field, $Match)) {
            $field = "<span style=\"color: #0055FF;\">"
                    . $Match[1] . " pronuncia le parole, '</span>"
                    . "<span style=\"color: cyan;\">" . $Match[2] . "</span>"
                    . "<span style=\"color: blue;\">'.</span><br />";
        }

        // Titoli Room:
        if (preg_match("(^\ \ \ (.*)$)", $Rows[$index + 1], $temp_match) && (!preg_match("^$)",
                                                                                         $Rows[$index - 1],
                                                                                         $temp_match))) {
            $field = "<label style=\"color: cyan;\">" . $field . "</label>";
        }

        // Morte:
        if (preg_match("(^Sei morto! Peccato\.\.\.)", $field, $Match)) {
            $field = "<span style=\"color: red;\">"
                    . $Match[0] . "</span>";
        }

        $Log .= $field;
        $index++;
    }

    return $Log;
    /*
      echo "<pre> ROWS";
      print_r($Rows);
      echo "</pre>";
     */
}

function LogBeautiferAnathema($Text)
{
    $Log   = "";
    $Rows  = explode("\n", $Text);
    $index = 0;

    foreach ($Rows as $key => $field) {
        // titolo Room
        //La vetta tra le nuvole                                  *
        //~ if (preg_match("(^\ \ \ (.*)(\s+)(.*)$)", $field, $Match))
        //~ {
        //~ echo "<pre>";
        //~ print_r($Match);
        //~ echo "</pre>";
        //~ $field = "<span style=\"color: red;\">" . $Match[2] . "</span>";
        //~ }
        // GD PARTICOLARI:
        if (preg_match("(^\[(.*)\] dice al gruppo '\[@\] (.*) \[@\]'\.)",
                       $field, $Match)) {
            $field = "<span style=\"color: blue;\">[</span>"
                    . "<span style=\"color: white;\">" . $Match[1] . "</span>"
                    . "<span style=\"color: blue;\">] dice al gruppo '</span>"
                    . "<span style=\"color: grey;\">[</span>"
                    . "<span style=\"color: #800517;\">@</span>"
                    . "<span style=\"color: grey;\">]</span>"
                    . "<span style=\"color: #00FF00;\"> " . $Match[2] . " </span>"
                    . "<span style=\"color: grey;\">[</span>"
                    . "<span style=\"color: #800517;\">@</span>"
                    . "<span style=\"color: grey;\">]</span>"
                    . "<span style=\"color: blue;\">'.</span><br />";
        }

        if (preg_match("(^\[(.*)\] dice al gruppo '\[\*\] (.*) \[\*\]'\.)",
                       $field, $Match)) {
            $field = "<span style=\"color: blue;\">[</span>"
                    . "<span style=\"color: white;\">" . $Match[1] . "</span>"
                    . "<span style=\"color: blue;\">] dice al gruppo '</span>"
                    . "<span style=\"color: red;\">[</span>"
                    . "<span style=\"color: yellow;\">*</span>"
                    . "<span style=\"color: red;\">]</span>"
                    . "<span style=\"color: yellow;\"> " . $Match[2] . " </span>"
                    . "<span style=\"color: red;\">[</span>"
                    . "<span style=\"color: yellow;\">*</span>"
                    . "<span style=\"color: red;\">]</span>"
                    . "<span style=\"color: blue;\">'.</span><br />";
        }

        if (preg_match("(^Dici al gruppo '\[\*\] (.*) \[\*\]'\.)", $field,
                       $Match)) {
            $field = "<span style=\"color: blue;\">Dici al gruppo '</span>"
                    . "<span style=\"color: red;\">[</span>"
                    . "<span style=\"color: yellow;\">*</span>"
                    . "<span style=\"color: red;\">]</span>"
                    . "<span style=\"color: yellow;\"> " . $Match[1] . " </span>"
                    . "<span style=\"color: red;\">[</span>"
                    . "<span style=\"color: yellow;\">*</span>"
                    . "<span style=\"color: red;\">]</span>"
                    . "<span style=\"color: blue;\">'.</span><br />";
        }

        if (preg_match("(^Dici al gruppo '\[@\] (.*) \[@\]'\.)", $field, $Match)) {
            $field = "<span style=\"color: blue;\">Dici al gruppo '</span>"
                    . "<span style=\"color: grey;\">[</span>"
                    . "<span style=\"color: #800517;\">@</span>"
                    . "<span style=\"color: grey;\">]</span>"
                    . "<span style=\"color: #00FF00;\"> " . $Match[1] . " </span>"
                    . "<span style=\"color: grey;\">[</span>"
                    . "<span style=\"color: #800517;\">@</span>"
                    . "<span style=\"color: grey;\">]</span>"
                    . "<span style=\"color: blue;\">'.</span><br />";
        }

        // Canali vari:
        if (preg_match("(^\[(.*)\] dice al gruppo '(.*)'.)", $field, $Match)) {
            $field = "<span style=\"color: blue;\">[</span>"
                    . "<span style=\"color: white;\">" . $Match[1] . "</span>"
                    . "<span style=\"color: blue;\">] dice al gruppo '</span>"
                    . "<span style=\"color: blue;\">" . $Match[2] . "'.</span><br />";
        }

        if (preg_match("(^\[(.*)\] dice '(.*)'.)", $field, $Match)) {
            //~ $field = "<span style=\"color: white;\">[</span>"
            //~ . "<span style=\"color: purple;\">" . $Match[1] . "</span>"
            //~ . "<span style=\"color: white;\">] dice '</span>"
            //~ . "<span style=\"color: white;\">" . $Match[2] . "'.</span><br />";

            $field = "<span style=\"color: white;\">["
                    . $Match[1] . "] dice '</span>"
                    . "<span style=\"color: white;\">"
                    . $Match[2] . "'.</span><br />";
        }

        if (preg_match("(^Dici al gruppo '(.*)'.)", $field, $Match)) {
            $field = "<span style=\"color: blue;\">Dici al gruppo '</span>"
                    . "<span style=\"color: blue;\">" . $Match[1] . "'.</span><br />";
        }

        if (preg_match("(^Tu dici '(.*)')", $field, $Match)) {
            $field = "<span style=\"color: white;\">"
                    . $Match[0] . " </span><br />";
        }

        if (preg_match("(^\[(.*)\] dice nel chiacchiero: '(.*)')", $field,
                       $Match)) {
            $field = "<span style=\"color: #017F7F;\">"
                    . $Match[0] . " </span><br />";
        }

        // Scudoni
        if (preg_match("(^(.*) e' circondat(.*) dalle fiamme!)", $field, $Match)) {
            $field = "<span style=\"color: red;\">"
                    . $Match[0] . " </span><br />";
        }

        if (preg_match("(^(.*) ha un turbine di energia intorno al corpo.)",
                       $field, $Match)) {
            $field = "<span style=\"color: #017F7F;\">"
                    . $Match[0] . " </span><br />";
        }

        if (preg_match("(^(.*) ha un vortice di frammenti di ghiaccio che turbinano intorno al suo corpo.)",
                       $field, $Match)) {
            $field = "<span style=\"color: cyan;\">"
                    . $Match[0] . " </span><br />";
        }



        // Santificazione
        if (preg_match("(^(.*) ha un manto di tenebre intorno al corpo.|"
                        . "^(.*) e' circondat(.*) da una barriera di ombra e luce!|"
                        . "^(.*) ha un'aura di luce dorata intorno al corpo.)",
                       $field, $Match)) {
            $field = "<span style=\"color: white;\">"
                    . $Match[0] . " </span><br />";
        }


        if (preg_match("(^Incantesimo: (.*), per (.*) round.)", $field, $Match)) {
            $field = "<span style=\"color: magenta;\">Incantesimo: </span>"
                    . "<span style=\"color: white;\">" . $Match[1] . "</span>"
                    . "<span style=\"color: magenta;\">, per </span>"
                    . "<span style=\"color: white;\">" . $Match[2] . "</span>"
                    . " <span style=\"color: magenta;\">round</span><br />";
        }

        if (preg_match("(^(.*) colpisce (.*) in pieno (.*))", $field, $Match)) {
            $field = "<span style=\"color: grey;\">" . $Match[1]
                    . " colpisce " . $Match[2] . "</span>"
                    . "<span style=\"color: white;\"> in pieno </span>"
                    . "<span style=\"color: grey;\">" . $Match[3] . "</span>";
        }

        if (preg_match("(^(.*) ti colpisce in pieno (.*))", $field, $Match)) {
            $field = "<span style=\"color: grey;\">" . $Match[1]
                    . " ti colpisce</span>"
                    . "<span style=\"color: white;\"> in pieno </span>"
                    . "<span style=\"color: grey;\">" . $Match[2] . "</span>";
        }

        if (preg_match("(^Colpisci (.*) in pieno (.*))", $field, $Match)) {
            $field = "<span style=\"color: grey;\">Colpisci " . $Match[1]
                    . "<span style=\"color: white;\"> in pieno </span>"
                    . "<span style=\"color: grey;\">" . $Match[2] . "</span>";
        }

        if (preg_match("(^(.*) taglia (.*) in pieno (.*))", $field, $Match)) {
            $field = "<span style=\"color: grey;\">" . $Match[1]
                    . " taglia " . $Match[2] . "</span>"
                    . "<span style=\"color: white;\"> in pieno </span>"
                    . "<span style=\"color: grey;\">" . $Match[3] . "</span>";
        }

        if (preg_match("(^(.*) trafigge (.*) in pieno (.*))", $field, $Match)) {
            $field = "<span style=\"color: grey;\">" . $Match[1]
                    . " trafigge " . $Match[2] . "</span>"
                    . "<span style=\"color: white;\"> in pieno </span>"
                    . "<span style=\"color: grey;\">" . $Match[3] . "</span>";
        }


        if (preg_match("(^(.*) massacra (.*))", $field, $Match)) {
            $field = "<span style=\"color: grey;\">" . $Match[1] . "</span>"
                    . "<span style=\"color: white;\"> massacra </span>"
                    . "<span style=\"color: grey;\">" . $Match[2] . "</span>";
        }

        if (preg_match("(^(.*) maciulla (.*))", $field, $Match)) {
            $field = "<span style=\"color: grey;\">" . $Match[1] . "</span>"
                    . "<span style=\"color: white;\"> maciulla </span>"
                    . "<span style=\"color: grey;\">" . $Match[2] . "</span>";
        }

        if (preg_match("(^(.*) devasta (.*))", $field, $Match)) {
            $field = "<span style=\"color: grey;\">" . $Match[1] . "</span>"
                    . "<span style=\"color: white;\"> devasta </span>"
                    . "<span style=\"color: grey;\">" . $Match[2] . "</span>";
        }

        if (preg_match("(^(.*) squarta (.*))", $field, $Match)) {
            $field = "<span style=\"color: grey;\">" . $Match[1] . "</span>"
                    . "<span style=\"color: white;\"> squarta </span>"
                    . "<span style=\"color: grey;\">" . $Match[2] . "</span>";
        }

        if (preg_match("(^Squarti (.*))", $field, $Match)) {
            $field = "<span style=\"color: white;\">Squarti </span>"
                    . "<span style=\"color: grey;\">" . $Match[1] . "</span>";
        }

        if (preg_match("(^(.*) demolisce (.*))", $field, $Match)) {
            $field = "<span style=\"color: grey;\">" . $Match[1] . "</span>"
                    . "<label style=\"color: grey;\"> demolisce </label>"
                    . "<span style=\"color: grey;\">" . $Match[2] . "</span>";
        }

        if (preg_match("(^(.*) polverizza (.*))", $field, $Match)) {
            $field = "<span style=\"color: grey;\">" . $Match[1] . "</span>"
                    . "<span style=\"color: #800517;\"> polverizza </span>"
                    . "<span style=\"color: grey;\">" . $Match[2] . "</span>";
        }

        if (preg_match("(^(.*) annichilisce (.*))", $field, $Match)) {
            $field = "<span style=\"color: grey;\">" . $Match[1] . "</span>"
                    . "<span style=\"color: yellow;\"> annichilisce </span>"
                    . "<span style=\"color: grey;\">" . $Match[2] . "</span>";
        }

        if (preg_match("(^La carica di (.*)\.|^La tua carica manda (.*)\.|"
                        . "^(.*) la tua carica lo travolge.|^(.*) la tua carica lo centra(.*)\.|"
                        . "^Una pozza di sangue ristagna macabramente sul terreno\.|"
                        . "^(.*) viene spazzato via PER SEMPRE, travolto dalla carica (.*)\.|"
                        . "^La terra riassorbe una pozza di sangue\.)", $field,
                       $Match)) {
            $field = "<span style=\"color: #800517;\">"
                    . $Match[0] . "</span><br />";
        }

        if (preg_match("(^(.*) e' mort(.*)!)", $field, $Match)) {
            $field = "<span style=\"color: #A52A2A;\">"
                    . $Match[1] . " e' mort" . $Match[2]
                    . "! </span><br />";
        }

        if (preg_match("(^(.*) quando senti l'urlo di morte di (.*)\.)", $field,
                       $Match)) {
            $field = "<span style=\"color: magenta;\">"
                    . $Match[1] . " quando senti l'urlo di morte di </span>"
                    . "<span style=\"color: white;\">" . $Match[2] . "</span>"
                    . "<span style=\"color: magenta;\">.</span><br />";
        }

        if (preg_match("(^(.*) si riprende dalla carica e si rialza.|"
                        . "^(.*) si ricompone un poco e lentamente si rialza\.|"
                        . "^Sprofondi a piu' non posso nel fango\.\.\.)",
                       $field, $Match)) {
            $field = "<span style=\"color: #7B8000;\">"
                    . $Match[0] . "</span><br />";
        }

        if (preg_match("(^Il tuo muro di ghiaccio (.*) di (\w+)(.*))", $field,
                       $Match)) {
            $field = "<span style=\"color: cyan;\">"
                    . $Match[0] . "</span>";
        }

        if (preg_match("(^(\w+) viene (.*) dai frammenti di ghiaccio del tuo scudo!)",
                       $field, $Match)) {
            $field = "<span style=\"color: cyan;\">"
                    . $Match[0] . "</span><br />";
        }

        if (preg_match("(^(.*) restituisce il colpo sotto forma di fiamme!)",
                       $field, $Match)) {
            $field = "<span style=\"color: red;\">"
                    . $Match[0] . "</span><br />";
        }

        if (preg_match("(^(.*) dall'energia della tua barriera!|"
                        . "^(.*) l'energia gli si riversa contro)", $field,
                       $Match)) {
            $field = "<span style=\"color: blue;\">"
                    . $Match[0] . "</span><br />";
        }

        if (preg_match("(^(.*) restituisce il colpo in taglienti schegge di ghiaccio!)",
                       $field, $Match)) {
            $field = "<span style=\"color: cyan;\">"
                    . $Match[0] . "</span><br />";
        }

        if (preg_match("(^Lo scudo di (.*) cresce d'intensita' quando (.*) per poco non lo colpisce!|"
                        . "^Il tuo scudo cresce d'intensita' quando (.*) per poco non ti colpisce!)",
                       $field, $Match)) {
            $field = "<span style=\"color: red;\">"
                    . $Match[0] . "</span><br />";
        }

        if (preg_match("(^(.*) e' avvolto in un manto di ghiaccio!)", $field,
                       $Match)) {
            $field = "<span style=\"color: cyan;\">"
                    . $Match[0] . "</span><br />";
        }

        if (preg_match("(^(.*) sembra sdoppiato, ne vedi (.*)!|"
                        . "^(.*) e' ingannat(.*) dalle immagini di (.*)\.|"
                        . "^(.*) centra una delle immagini di (.*)\.|"
                        . "^(.*) l'ultima immagine (.*))", $field, $Match)) {
            $field = "<span style=\"color: #006D4F;\">"
                    . $Match[0] . "</span><br />";
        }

        if (preg_match("(^Con un gran fragore l'esplosione (.*)\.)", $field,
                       $Match)) {
            $field = "<span style=\"color: red;\">"
                    . $Match[0] . "</span><br />";
        }

        if (preg_match("(^(.*) una densa sfera di fuoco, WOOOOOM!)", $field,
                       $Match)) {
            $field = "<span style=\"color: grey;\">" . $Match[1]
                    . "una densa sfera di fuoco, </span>"
                    . "<span style=\"color: #800517;\">W</span>"
                    . "<span style=\"color: red;\">O</span>"
                    . "<span style=\"color: yellow;\">OOO</span>"
                    . "<span style=\"color: red;\">O</span>"
                    . "<span style=\"color: #800517;\">M!</span><br />";
        }

        if (preg_match("(^(.*) crolla addosso a (.*) lasciandolo stordito!)",
                       $field, $Match)) {
            $field = "<label style=\"color: grey;\">"
                    . $Match[0] . "</label><br />";
        }

        if (preg_match("(^Vorresti smettere di SANGUINARE cosi' tanto!)",
                       $field, $Match)) {
            $field = "<span style=\"color: grey;\">Vorresti smettere di </span>"
                    . "<span style=\"color: red;\">SANGUINARE</span>"
                    . "<span> cosi' tanto!</span><br />";
        }

        // Disintegrazione
        if (preg_match("(^(.*) si concentra e scaglia un enorme fascio di energia distruttiva contro (.*)!)",
                       $field, $Match)) {
            $field = "<label style=\"color: #00FF00;\">"
                    . $Match[0] . "</label><br />";
        }

        if (preg_match("(^La carica energetica si propaga (.*))", $field, $Match)) {
            $field = "<span style=\"color: white;\">"
                    . $Match[0] . "</span>";
        }

        // Cast
        if (preg_match("(^(.*) pronuncia le parole, '(.*)'.)", $field, $Match)) {
            $field = "<span style=\"color: #0055FF;\">"
                    . $Match[1] . " pronuncia le parole, '</span>"
                    . "<span style=\"color: cyan;\">" . $Match[2] . "</span>"
                    . "<span style=\"color: blue;\">'.</span><br />";
        }

        // Room:
        if (preg_match("(^(.*) (.*)$)", $field, $Match) && (preg_match("(^-=o=============================================o=- (.*)$)",
                                                                       $Rows[$index + 1],
                                                                       $temp_match))) {
            $field = "<span style=\"color: red;\">" . $Match[1] . "</span>"
                    . " " . $Match[2];
        }

        if (preg_match("(^-=o=============================================o=- (.*)$)",
                       $field, $Match)) {
            $field = "-=<span style=\"color: yellow;\">o</span>"
                    . "============================================="
                    . "<span style=\"color: yellow;\">o</span>=- "
                    . $Match[1] . "</span>";
        }

        if (preg_match("(^\[Uscite: (.*)\])", $field, $Match)) {
            $field = "<span style=\"color: yellow;\">[</span>"
                    . "<span style=\"color: green;\">Uscite</span>"
                    . "<span style=\"color: yellow;\">:</span>"
                    . "<span style=\"color: green;\"> " . $Match[1] . "</span>"
                    . "<span style=\"color: yellow;\">]</span><br />";
        }

        if (preg_match("(^Uscite Visibili:(.*))", $field, $Match)) {
            $field = "<span style=\"color: green;\">" . $Match[0] . "</span>";
        }

        if (preg_match("(^Est       - (.*))", $field, $Match)) {
            $field = "<span style=\"color: green;\">" . $Match[0] . "</span>";
        }

        if (preg_match("(^Sud       - (.*))", $field, $Match)) {
            $field = "<span style=\"color: green;\">" . $Match[0] . "</span>";
        }

        if (preg_match("(^Ovest     - (.*))", $field, $Match)) {
            $field = "<span style=\"color: green;\">" . $Match[0] . "</span>";
        }

        if (preg_match("(^Nord      - (.*))", $field, $Match)) {
            $field = "<span style=\"color: green;\">" . $Match[0] . "</span>";
        }

        if (preg_match("(^Altrove   - (.*))", $field, $Match)) {
            $field = "<span style=\"color: green;\">" . $Match[0] . "</span>";
        }

        //~ if (preg_match("(^(.*) segue con sguardo adorante il suo padrone.)", $field, $Match))
        //~ {
        //~ $field = "<span style=\"color: #0000FF;\">" . $Match[0] . "</span>";
        //~ }
        //~ 
        //~ if (preg_match("(^(.*) si concentra...)", $field, $Match))
        //~ {
        //~ $field = "<span style=\"color: #0000FF;\">" . $Match[0] . "</span>";
        //~ }



        $Log .= $field;
        $index++;
    }

    return $Log;
    /*
      echo "<pre> ROWS";
      print_r($Rows);
      echo "</pre>";
     */
}

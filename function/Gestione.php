<?php

// TODO convertire in classe Magazzino

function getDisponibilita($IdProdotto, $Richiesta = 1, $debug = 1)
{
    if (($Richiesta == "") && ($Richiesta < 1)) {
        $Richiesta = 1;
    }

    if ($debug != 1) {
        echo "[Debug][getDisponibilita]: IdProdotto: $IdProdotto <br />";
        echo "[Debug][getDisponibilita]:  Richiesta: $Richiesta <br />";
    }

    global $d;
    $Richiesta = floatval($Richiesta);

    $Articolo     = $d->GetRows("cod_int, ck_com", "tab_prodotti",
                                "id_prodotto = '$IdProdotto'", "", "", $debug);
    $giacProdotto = getGiacenza($Articolo[0]['cod_int'], $debug);

    switch ($giacProdotto) {
        case "":
            $Giacenza = "0.00";
            break;

        case ( floatval($giacProdotto) < floatval($Richiesta) ):
            ?>
            <script type="text/javascript">
                alert("Errore, la giacenza non è sufficiente per soddisfare la richiesta,\n"
                        + "imposto la quantità al massimo consentito dal magazzino.");
            </script><?php
            $Giacenza = floatval($giacProdotto);
            break;

        default:
            $Giacenza = floatval($Richiesta);
    }

    return $Giacenza;
}

function Check_piva_cf($string, $NomeCampo = "P.Iva/C.F.")
{
    if ((strlen($string) != 11) && (strlen($string) != 16)) {
        return "La lunghezza del campo $NomeCampo non è corretta";
    }

    //~ Verifica partita iva:
    if (strlen($string) == 11) {
        if (!preg_match("(^[0-9]+$)", $string)) {
            return "Errore, la partita IVA deve contenere solo cifre";
        } else {
            $s = 0;
            for ($i = 0; $i <= 9; $i += 2) {
                $s += ord($string[$i]) - ord('0');
            }

            for ($i = 1; $i <= 9; $i += 2) {
                $c = 2 * ( ord($string[$i]) - ord('0') );
                if ($c > 9) {
                    $c = $c - 9;
                }
                $s += $c;
            }

            if ((10 - $s % 10) % 10 != ord($string[10]) - ord('0')) {
                return "Errore, la partita IVA non è valida, "
                        . "il codice di controllo non corrisponde.";
            }
        }
    }
}

function getGiacenza($CodiceInternoProdotto, $debug = 1)
{
    global $d;

    if ($debug != 1) {
        echo "[Debug][getGiacenza]: CodiceInternoProdotto: $CodiceInternoProdotto <br />";
    }

    $Articolo = $d->GetRows("*", "tab_prodotti",
                            "cod_int = '$CodiceInternoProdotto'", "", "", $debug);

    if ($debug != 1) {
        echo "<pre>";
        print_r($Articolo);
        echo "</pre>";
    }

    /**
     * Se l'articolo è un composto non controllare la sua
     * giacenza ma quella dei suoi componenti:
     */
    if ($Articolo[0]['ck_com'] == "0") {
        if ($debug != 1) {
            echo "[Debug][getDisponibilita]: L'articolo e' un composto. <br />";
        }
        $disp_comp = array();

        $Componenti   = $d->GetRows("*", "view_prodotti",
                                    "id_prodotto = '$CodiceInternoProdotto'",
                                    "", "", $debug);
        $MultiploComp = getMultiplo($CodiceInternoProdotto, $debug);

        if ($debug != 1) {
            echo "[Debug][getGiacenza]: Avvio l'analisi dei componenti. <br />";
        }

        foreach ($Componenti as $key_com => $field_com) {
            if ($debug != 1) {
                echo "[Debug][getDisponibilita]: Componente: " . $field_com['cod_int'] . " <br />";
            }

            $giac_prod = $d->GetRows("sum(qta) as giac", "tab_magazzino",
                                     "cod_int = '" . $field_com['cod_int'] . "' AND ck_vend = '1'",
                                     "", "", $debug);

            if ($giac_prod[0]['giac'] == "") {
                $giac_prod[0]['giac'] = "0.00";
            }

            $giac_prod[0]['giac'] *= $MultiploComp;

            if ($debug != 1) {
                echo "[Debug][getDisponibilita]: Q.ta necessaria: "
                . floatval($_POST['qta' . $key] * $field_com['qta'])
                . " <br />";

                echo "[Debug][getDisponibilita]: Q.ta giacenza: "
                . floatval($giac_prod[0]['giac']) . " <br />";
            }

            if (floatval($_POST['qta' . $key] * $field_com['qta']) > floatval($giac_prod[0]['giac'])) {
                $errori .= "Errore, la giacenza per il componente \""
                        . $field_com['cod_int'] . " " . $field_com['modello']
                        . "\" non è sufficiente per soddisfare la richiesta. <br /> "
                        . "in magazzino: " . $giac_prod[0]['giac'] . ", richiesta: "
                        . ($_POST['qta' . $key] * $field_com['qta']) . " <br /> ";

                array_push($disp_comp, $giac_prod[0]['giac'] / $field_com['qta']);
            } else {
                array_push($disp_comp, $giac_prod[0]['giac'] / $field_com['qta']);
            }
        }
    } else {
        $giac_prod = $d->GetRows("sum(qta) as giac", "tab_magazzino",
                                 "cod_int = '" . $Articolo[0]['cod_int'] . "' AND ck_vend = '1'",
                                 "", "", $debug);
        $Multiplo  = getMultiplo($CodiceInternoProdotto, $debug);

        if ($debug != 1) {
            echo "[Debug][getDisponibilita]: Giacenza singolo prodotto: '"
            . $giac_prod[0]['giac'] . "' <br />";
        }

        if ($giac_prod[0]['giac'] == "") {
            $Giacenza = "0.00";
        } else {
            $Giacenza = $giac_prod[0]['giac'];
        }

        $Giacenza *= $Multiplo;

        if ($debug != 1) {
            echo "[Debug][getGiacenza]: Giacenza prodotto restituita: $Giacenza<br />";
        }
    }

    return $Giacenza;
}

function caricaMagazzino($CodiceInternoProdotto, $Quantita, $debug = 1)
{
    global $d;
    $tmp_array = $d->GetRows("*", "view_prodotti",
                             "cod_int = '$CodiceInternoProdotto'", "", "",
                             $debug);
    $Valori    = Array('cod_int'      => $CodiceInternoProdotto,
        'prz'          => 0,
        'sco'          => 0,
        'qta'          => 1,
        'ck_vend'      => 1,
        'id_documento' => 0,
        'data_acq'     => date('Y-m-d'),
        'id_prodotto'  => $tmp_array[0]['id_prodotto'],
        'manual_var'   => 0);
    unset($tmp_array);

    for ($i = 1; $i <= $Quantita; $i++) {
        $d->SaveRow($Valori, "", "tab_magazzino", $debug);
    }
    echo "<label class=\"ok\">Giacenza incrementata correttamente.</label>";
    return;
}

function scaricaMagazzino($CodiceInternoProdotto, $Quantita, $debug = 1)
{
    global $d;
    $GiacenzaAttuale = getGiacenza($CodiceInternoProdotto, $debug);
    $Multiplo        = getMultiplo($CodiceInternoProdotto, $debug);

    if ($debug != 1) {
        echo "[DEBUG][scaricaMagazzino]: Quantita: $Quantita, GiacenzaAttuale: "
        . floatval($GiacenzaAttuale / $Multiplo) . " <br />";
    }

    if ($Quantita > floatval($GiacenzaAttuale / $Multiplo)) {
        echo "<label class=\"err\">La quantit&agrave; da scaricare non pu&ograve;"
        . " essere maggiore della giacenza attuale, azione annullata.</label>";
        return;
    }

    $DaScaricare = $d->GetRows("*", "tab_magazzino",
                               "cod_int = '$CodiceInternoProdotto' AND ck_vend = '1'",
                               "", "data_acq limit $Quantita", $debug);

    foreach ($DaScaricare as $key => $field) {
        $d->UpdateRow(array('ck_vend'    => 0,
            'manual_var' => 0), "", "tab_magazzino",
                      "id_mag = '" . $field['id_mag'] . "'", $debug);
    }

    echo "<label class=\"ok\">Giacenza decrementata correttamente</label>";
    return;
}

/**
 * Recupera il moltiplicatore dell'unità di misura in base al codice prodotto.
 * @param type $CodiceInternoProdotto
 * @param type $debug
 */
function getMultiplo($CodiceInternoProdotto, $debug = 1)
{
    global $d;
    $UM = $d->GetRows("u.*", "tab_um u, tab_prodotti p",
                      "p.cod_int = '$CodiceInternoProdotto' AND u.id_um = p.id_um",
                      "", "", $debug);

    if ($debug != 1) {
        echo "<pre> UM ";
        print_r($UM);
        echo "</pre>";

        echo "[DEBUG][getMultiplo]: multiplo: '" . $UM[0]['multiplo'] . "' <br />";
    }


    return $UM[0]['multiplo'];
}

/**
 * name: VariazioneMagazzino
 * @param Codice interno del prodotto
 * @param Azione da intraprendere (carico/scarico) switch
 * @param Quantita' da variare
 * @return -
 */
function variaMagazzino($CodiceInternoProdotto, $Azione, $Quantita, $debug = 1)
{
    $Multiplo = getMultiplo($CodiceInternoProdotto, $debug);

    if ($Quantita <= 0) {
        return "<label class=\"err\">La quantit&agrave; deve essere un numero positivo, azione annullata.</label>";
    }

    switch ($Azione) {
        case "carico":
            caricaMagazzino($CodiceInternoProdotto,
                            floatval($Quantita / $Multiplo), 1);
            break;

        case "scarico":
            scaricaMagazzino($CodiceInternoProdotto,
                             floatval($Quantita / $Multiplo), 1);
            break;
    }
}

function AddDocumentRow($RowType, $IdProdotto, $IdIVA = "", $Note = "",
                        $Quantita = 1, $Sconto = 0, $Prezzo = 0)
{
    global $d;
    $DocumentRow       = array();
    $UpdateDocumentRow = "";

    if (!isset($_SESSION['documento']['dettaglio'])) {
        $_SESSION['documento']['dettaglio'] = Array();
    }

    if (is_numeric($IdProdotto) && ($IdProdotto > 0)) {
        // si tratta di un prodotto reale, estraggo tutti i dati dall'anagrafica:
        //~ echo "[Debug][AddDocumentRow]: si tratta di un prodotto reale, estraggo tutti i dati dall'anagrafica <br />";
        $ProdottoArray = $d->GetRows("*", "view_prodotti",
                                     "id_prodotto = '$IdProdotto' LIMIT 1", "",
                                     "", 1);
        $DocumentRow   = $ProdottoArray[0];
    } elseif (is_numeric($IdProdotto) && ($IdProdotto == 0)) {
        // si tratta di una voce arbitraria o una nota ( todo ):
        //~ echo "[Debug][AddDocumentRow]: si tratta di una voce arbitraria o una nota ( todo ) <br />";
        $DocumentRow['id_prodotto'] = 0;
        $DocumentRow['cod_int']     = "-";
        $DocumentRow['cod_forn']    = "-";
        $DocumentRow['um']          = "-";
    }

    //~ echo "[Debug][AddDocumentRow]: IVA: '$IdIVA' <br />";
    if ($IdIVA == "") {
        // Recupera i dati dell'iva del prodotto se non e' stato fornito l'id:
        //~ echo "[Debug][AddDocumentRow]: Recupera i dati dell'iva del prodotto se non e' stato fornito l'id <br />";
        $IvaArray = $d->GetRows("*", "tab_iva",
                                "id_iva = '" . $ProdottoArray[0]['id_iva'] . "'",
                                "", "", 1);

        $DocumentRow['id_iva']   = $IvaArray[0]['id_iva'];
        $DocumentRow['iva']      = $IvaArray[0]['iva'];
        $DocumentRow['desc_iva'] = $IvaArray[0]['desc_iva'];
        unset($IvaArray);
    } elseif (is_numeric($IdIVA)) {
        // Importa i dati dell'iva in base all'id fornito:
        //~ echo "[Debug][AddDocumentRow]: Importa i dati dell'iva in base all'id fornito <br />";

        $IvaArray = $d->GetRows("*", "tab_iva", "id_iva = '$IdIVA'", "", "", 1);

        //~ echo "<pre>";
        //~ print_r($IvaArray);
        //~ echo "</pre>";

        $DocumentRow['id_iva']   = $IvaArray[0]['id_iva'];
        $DocumentRow['iva']      = $IvaArray[0]['iva'];
        $DocumentRow['desc_iva'] = $IvaArray[0]['desc_iva'];
        unset($IvaArray);
    }

    //~ echo "[Debug][AddDocumentRow]: NOTE: $Note <br />";
    if ($Note == "") {
        if (is_numeric($IdProdotto) && ($IdProdotto > 0)) {
            /* si tratta di un prodotto, quindi utilizzo le note
             * dall'anagrafica per popolare il dato */
            $DocumentRow['note'] = $ProdottoArray[0]['note'];
        }
    } else {
        // utilizzo il valore fornito per popolare il campo:
        if (is_numeric($IdProdotto) && ($IdProdotto == 0)) {
            // si tratta di una voce arbitraria o una nota ( todo ):
            //~ echo "[Debug][AddDocumentRow]: si tratta di una voce arbitraria o una nota ( todo ) <br />";				
            $DocumentRow['marca']    = "";
            $DocumentRow['modello']  = $Note;
            $DocumentRow['voce_arb'] = $Note;
        } else {
            $DocumentRow['note'] = $Note;
        }
    }

    //~ echo "[Debug][AddDocumentRow]: Quantita: $Quantita <br />";
    if (($Quantita == "") || ($Quantita < 0)) {
        // Se la quantità scelta è nulla o < 1 allora impostala ad 1:
        //~ echo "[Debug][AddDocumentRow]: Quantita' inferiore a zero, la porto ad 1 <br />";
        $Quantita = 1;
    }

    if ($Quantita > 0) {
        if (($_SESSION['documento']['tipo_doc'] == 5)) {
            $DocumentRow['disp'] = getGiacenza($DocumentRow['cod_int']);
            $DocumentRow['qta']  = getDisponibilita($IdProdotto, $Quantita);
            $DocumentRow['prz']  = $ProdottoArray[0]['prz_ve'];
        } else {
            // disponibilita' in altri casi non realmente importante
            $DocumentRow['disp'] = getGiacenza($DocumentRow['cod_int']);
            $DocumentRow['qta']  = $Quantita;
            $DocumentRow['prz']  = $ProdottoArray[0]['prz_acq'];
        }
    }

    switch ($RowType) {
        case "prodotto":
            /*
             * 01/07/2012 12:49:13 CEST Claudio Giordano
             *
             * Verifico la presenza dello stesso articolo nel documento
             */
            foreach ($_SESSION['documento']['dettaglio'] as $key => $field) {
                if ($field['id_prodotto'] == $ProdottoArray[0]['id_prodotto']) {
                    //~ echo "[Debug][AddDocumentRow]: l'articolo e' presente nel documento con KEY: $key <br />";
                    $UpdateDocumentRow = $key;
                }
            }

            $DocumentRow['sco']      = $Sconto;
            $DocumentRow['voce_arb'] = "";

            unset($temp_array);
            break;

        case "nota":
            // TODO
            break;

        case "voce":
            $DocumentRow['sco'] = $Sconto;
            break;


        default:
            echo "[Debug][AddDocumentRow]: Errore, tipo di riga non riconsciuto, esco <br />";
            return;
    }


    //~ echo "[Debug][AddDocumentRow]: UpdateDocumentRow: '$UpdateDocumentRow' <br />";
    if (!is_numeric($UpdateDocumentRow)) {
        // accoda la riga al dettaglio:
        //~ echo "[Debug][AddDocumentRow]: aggiungo la nuova riga al documento <br />";
        array_push($_SESSION['documento']['dettaglio'], $DocumentRow);
    } else {
        /*
         * se e' stato trovato il prodotto nel dettaglio aggiorna/sostituisci la riga
         * incrementando anche la quantita'
         */
        //~ echo "[Debug][AddDocumentRow]: aggiorno la riga $UpdateDocumentRow del documento con i nuovi dati <br />";
        //~ echo "[Debug]: sess_qta: ".$_SESSION['documento']['dettaglio'][$UpdateDocumentRow]['qta']." <br />";
        //~ echo "[Debug]: row_qta: ".$DocumentRow['qta']." <br />";
        //~ echo "[Debug]: _qta: $Quantita <br />";
        $DocumentRow['qta']                                     = floatval(floatval($_SESSION['documento']['dettaglio'][$UpdateDocumentRow]['qta']) + floatval($Quantita));
        $_SESSION['documento']['dettaglio'][$UpdateDocumentRow] = $DocumentRow;
    }

    unset($ProdottoArray);
}

function VerificaStatoDocumenti($Stato = "1")
{
    global $d;

    // 0 pagato, 1 non pagato
    $StatiDocumento    = Array();
    $StatiDocumento[0] = Array('id_stato' => '0', 'stato' => 'Evaso');
    $StatiDocumento[1] = Array('id_stato' => '1', 'stato' => 'Non Evaso');

    $Where = "";

    switch ($Stato) {
        case "0":
            $Where = "ck_pag = '0'";
            break;

        case "1":
            $Where = "ck_pag = '1'";
            break;

        default:
            $Where = "";
    }


    // data, codice, tipo, ragione sociale, importo, scadenza, stato (ck_pag), link;
    $Documenti = $d->GetRows("data_doc, cod_documento, tipo_doc, rag_soc, totale, data_scad, ck_pag",
                             "view_documenti", $Where, "",
                             "data_doc, tipo_doc, rag_soc", 1);

    //~ echo "<pre>";
    //~ print_r($Documenti);
    //~ echo "</pre>";

    if (count($Documenti) > 0) {
        foreach ($Documenti as $key => $field) {
            if ($field['ck_pag'] == "0") {
                $BackgroundColor = "background-color: #90EE90;";
            } else {
                $BackgroundColor = "background-color: #F39A91;";
            }
            ?>

            <div style="width: 10%; clear: left; float: left; height: 1.5em; <?php echo $BackgroundColor; ?>"
                 title="Data documento">
            <?php echo $d->Inverti_Data($field['data_doc']); ?>
            </div>

            <div style="width: 10%; float: left; height: 1.5em; <?php echo $BackgroundColor; ?>" 
                 title="Codice documento">
            <?php echo $field['cod_documento']; ?>
            </div>

            <div style="width: 10%; float: left; height: 1.5em; <?php echo $BackgroundColor; ?>" 
                 title="Tipo documento">
            <?php echo $field['tipo_doc']; ?>
            </div>

            <div style="width: 38em; float: left; height: 1.5em; <?php echo $BackgroundColor; ?>" 
                 title="Ragione sociale">
            <?php echo $field['rag_soc']; ?>
            </div>

            <div style="width: 10%; float: left; height: 1.5em; text-align: right; <?php echo $BackgroundColor; ?>" 
                 title="Totale documento">
                     <?php printf("%.2f €", $field['totale']); ?>
            </div>

            <div style="width: 10%; float: left; height: 1.5em; text-align: right; <?php echo $BackgroundColor; ?>" 
                 title="Scadenza documento">
                     <?php echo $d->Inverti_Data($field['data_scad']); ?>
            </div>

            <div style="width: 12%; float: left; height: 1.5em; text-align: right; vertical-align: middle; <?php echo $BackgroundColor; ?>" 
                 title="Stato documento">
                     <?php $d->DrawControl("select", "ck_pag",
                                                 "Stato documento", "", $field,
                                                 null, "10em", "", "",
                                                 $StatiDocumento, "id_stato",
                                                 "stato");
                     ?>
                <!--&nbsp;<a href="#" title="Marca come saldato">
                    <img src="/Images/Links/apply.png" alt="apply" />
                </a>!-->
            </div>
                 <?php
                 }
             } else {
                 echo "[Debug]: nessun risultato corrispondente. <br />";
             }
         }
         
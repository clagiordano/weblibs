<?php

/**
 * @deprecated since Mail class 1.0
 */

function DirectSendMail($From, $To, $Subject, $Body, $Headers = "",
                        $Hostname = "localhost", $Port = "25", $Timeout = "30")
{
    // extract mail from string
    preg_match("(\b[A-Za-z0-9._%-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}\b)", $From,
               $matches);
    $MailFrom = $matches[0];
    $tmp      = explode("@", $MailFrom);
    $Domain   = $tmp[1];
    $uid      = md5(uniqid(time()));

    preg_match("(\b[A-Za-z0-9._%-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}\b)", $To,
               $matches);
    $MailTo = $matches[0];

    $smtp_server = fsockopen($Hostname, $Port, $errorno, $errstr, $Timeout);

    //$server_response = fgets($smtp_server);
    //echo "[Debug]: resp: $server_response <br />";

    /* The fsocketopen will time out after 30 seconds if the
     * connection failed. The $errorno and $errstr will contain
     * the error number and the error message if an error happens.
     */

    if (!$smtp_server) {
        // we have an error, do something
        //echo "[Debug]: error, exit. <br />";
        exit;
    }

    // To get the server response use the fgets function like this:
    /*
      $server_response = fgets($smtp_server);
      echo "[Debug]: $server_response <br />";
     */

    /* I'll not care about the server responses here; I want just to
     * send the email. If you want to write a good program you
     * should care.
     */

    //To send a string to the server use fwrite function:
    //fwrite($smtp_server, $command);
    // IMPORTANT: all commands to be sent to server must end with "\r\n" : CR + LF
    //Here is the code (no error handling here too):

    fwrite($smtp_server, "HELO erremmeweb.com\r\n");
    //$server_response = fgets($smtp_server);
    //echo "[Debug]: HELO resp: $server_response <br />";

    fwrite($smtp_server, "MAIL FROM: <$MailFrom>\r\n");
    //$server_response = fgets($smtp_server);
    //echo "[Debug]: MAIL FROM resp: $server_response <br />";

    fwrite($smtp_server, "RCPT TO: <$MailTo>\r\n");
    //$server_response = fgets($smtp_server);
    //echo "[Debug]: RCPT TO resp: $server_response <br />";

    fwrite($smtp_server, "DATA\r\n");
    //$server_response = fgets($smtp_server);
    //echo "[Debug]: DATA resp: $server_response <br />";

    fwrite($smtp_server,
           "Received: from $MailFrom by $Domain; " . date('r') . "\r\n");
    fwrite($smtp_server, "Date: " . date('r') . "\r\n");
    fwrite($smtp_server, "From: $From\r\n");
    fwrite($smtp_server, "Subject: $Subject\r\n");
    fwrite($smtp_server, "To: $To\r\n");
    fwrite($smtp_server, $Headers);
    fwrite($smtp_server, "\r\n$Body\r\n");

    fwrite($smtp_server, ".\r\nQUIT\r\n");
    /*
      $server_response = fgets($smtp_server);
      echo "[Debug]: QUIT resp: $server_response <br />";
     */
}

function DirectSendMail2($MailData, $Hostname = "localhost", $Port = "25",
                         $Timeout = "30")
{
    $boundary              = md5(uniqid(time()));
    $MailData['stdheader'] = "X-Mailer: PHP-Code\r\n";
    $MailData['stdheader'] .= "MIME-Version: 1.0\r\n";
    $MailData['stdheader'] .= "Content-Type: multipart/mixed; boundary=\"$boundary\"\r\n";
    $MailData['stdheader'] .= "Content-Transfer-Encoding: 7bit\r\n";

    // extract mail from string
    preg_match("(\b[A-Za-z0-9._%-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}\b)",
               $MailData['from'], $matches);
    $MailFrom = $matches[0];
    $tmp      = explode("@", $MailFrom);
    $Domain   = $tmp[1];


    $Rcpt = array();
    if (preg_match_all("(\b[A-Za-z0-9._%-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}\b)",
                       $MailData['to'], $matches)) {
        foreach ($matches[0] as $key => $field) {
            array_push($Rcpt, $field);
        }
    }

    if (isset($MailData['cc']) && (preg_match_all("(\b[A-Za-z0-9._%-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}\b)",
                                                  $MailData['cc'], $matches))) {
        foreach ($matches[0] as $key => $field) {
            array_push($Rcpt, $field);
        }
    }

    if (isset($MailData['cc']) && (preg_match_all("(\b[A-Za-z0-9._%-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}\b)",
                                                  $MailData['ccn'], $matches))) {
        foreach ($matches[0] as $key => $field) {
            array_push($Rcpt, $field);
        }
    }

    if (isset($MailData['att']) && (trim($MailData['att']) != "")) {
        $MailData['attach'] = "";
        $attachments        = explode(";", $MailData['att']);

        foreach ($attachments as $file_path) {
            $finfo          = new finfo(FILEINFO_MIME);
            $file_mime      = $finfo->file($file_path);
            //~ $file_mime = "application/vnd.ms-excel";
            $contents       = file_get_contents($file_path);
            $encoded_attach = chunk_split(base64_encode($contents));

            $MailData['attach'] .= "--$boundary\r\n";
            $MailData['attach'] .= "Content-Type: $file_mime; name=\"" . basename($file_path) . "\"\r\n";
            $MailData['attach'] .= "Content-Transfer-Encoding: base64\r\n";
            $MailData['attach'] .= "Content-Disposition: attachment; filename=\"" . basename($file_path) . "\"\r\n\r\n";
            $MailData['attach'] .= "$encoded_attach";
            $MailData['attach'] .= "--$boundary--\r\n\r\n";

            /*
              echo "<pre>";
              print_r($MailData['attach']);
              echo "</pre>";
             */
        }
    }

    if (isset($MailData['body']) && (trim($MailData['body'] != ""))) {
        $tmp_body         = $MailData['body'];
        $MailData['body'] = "\r\n--$boundary\r\n";
        $MailData['body'] .= "Content-Type: text/plain; charset=\"iso-8859-1\"\r\n";
        $MailData['body'] .= "Content-Transfer-Encoding: quoted-printable\r\n\r\n";
        $MailData['body'] .= "$tmp_body\r\n";
        $MailData['body'] .= "--$boundary--\r\n\r\n";
        /*

          echo "<pre>";
          print_r($MailData['body']);
          echo "</pre>";
         */
    }

    // START SERVER CONNECTIONS:
    $response    = array();
    $smtp_server = fsockopen($Hostname, $Port, $errorno, $errstr, $Timeout);
    array_push($response, fgets($smtp_server));

    if (!$smtp_server) {
        echo "[Debug][DirectSendMail2]: Error, cannot open connection, exit. <br />";
        exit;
    }

    fwrite($smtp_server, "HELO $Domain\r\n");
    array_push($response, fgets($smtp_server));
    fwrite($smtp_server, "MAIL FROM: <$MailFrom>\r\n");
    array_push($response, fgets($smtp_server));
    foreach ($Rcpt as $key => $field) {
        fwrite($smtp_server, "RCPT TO: <$field>\r\n");
        array_push($response, fgets($smtp_server));
    }
    fwrite($smtp_server, "DATA\r\n");
    array_push($response, fgets($smtp_server));
    fwrite($smtp_server, "From: " . $MailData['from'] . "\r\n");
    fwrite($smtp_server, "To: " . $MailData['to'] . "\r\n");
    if (isset($MailData['cc'])) {
        fwrite($smtp_server, "Cc: " . $MailData['cc'] . "\r\n");
    }
    fwrite($smtp_server,
           "Received: from $MailFrom by $Domain; " . date('r') . "\r\n");
    fwrite($smtp_server, "Date: " . date('r') . "\r\n");
    fwrite($smtp_server, "Subject: " . $MailData['subject'] . "\r\n");
    fwrite($smtp_server, $MailData['stdheader']);
    fwrite($smtp_server, $MailData['body']);
    if (isset($MailData['attach'])) {
        fwrite($smtp_server, $MailData['attach']);
    }

    fwrite($smtp_server, "\r\n.\r\nQUIT\r\n");
    array_push($response, fgets($smtp_server));

    //~ echo "<pre>";
    //~ print_r($response);
    //~ echo "</pre>";
}

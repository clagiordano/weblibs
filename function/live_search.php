<?php
/*
 * 28/08/2009 23:48:31 CEST Claudio Giordano
 * 22/12/2009   convertita in ajax
 *
 * composizione live search
 */

$root = substr($_SERVER['DOCUMENT_ROOT'], 0, -1);
require_once ("$root/conti/connection.php");
require_once ("$root/Function/Strings.php");
require_once ("$root/Function/Db.php");
require_once ("$root/Function/Debug.php");
require_once ("$root/Function/DataTime.php");

$_GET['str'] = ExpandSearch($_GET['str']);

$Sottoconti = GetRows("tab_sottoconti",
                      "concat(\"(\", codice_sottoconto, \") \", descr_sottoconto) LIKE '%"
        . $_GET['str'] . "%'", "descr_sottoconto", $db, 1,
                      "id_sottoconto, concat(\"(\", codice_sottoconto, \") \", descr_sottoconto) as descr_sottoconto");

$Anagrafiche = GetRows("tab_anagrafica",
                       "rag_soc LIKE '%" . $_GET['str']
        . "%'", "rag_soc", $db, 1);

$Result = array();
foreach ($Sottoconti as $key => $field) {
    array_push($Result, $field['descr_sottoconto']);
}

foreach ($Anagrafiche as $key => $field) {
    array_push($Result, $field['rag_soc']);
}
?>

    <?php if (count($Result) > 0) { ?>
    <table class="dettaglio" style="width: 100%;">
    <?php foreach ($Result as $key => $field) { ?>
            <tr>
                <td>
                    <a href="javascript: setText('<?php echo $_GET['object']; ?>', '<?php echo $field; ?>'); void(0);"
                       title="Click per selezionare la voce">
        <?php echo $field; ?>
                    </a>
                </td>
            </tr>
        <?php } ?>
<?php } else { ?>
        <tr><td>Nessun risultato corrispondente.</td></tr>
    </table>
<?php } ?>

<?php
/*
 * 28/08/2009 23:48:41 CEST Claudio Giordano
 * 22/12/2009   convertita in ajax
 *
 * fine live search
 */
?>

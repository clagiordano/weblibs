<table style="width: 100%;" class="footer-credit" rules="all">
    <tr>
        <td style="vertical-align: middle;">
            <a onclick="Javascript: window.open('http://gnu.it', '_blank'); void(0);">
                <img src="../../Img/sw/gnubanner-2.png" alt="GNU Project" title="Progetto GNU" />
            </a>
            <a onclick="Javascript: window.open('http://it.wikipedia.org/wiki/Linux', '_blank'); void(0);">
                <img src="../../Img/sw/linux.gif" alt="Linux" title="GNU/Linux" />
            </a>
            <a onclick="Javascript: window.open('http://www.debian.org', '_blank'); void(0);">
                <img src="../../Img/sw/debian.png" alt="debian.org" title="Debian - Debian è un sistema operativo (OS) libero (free) per il tuo computer." />
            </a>
            <a onclick="Javascript: window.open('http://www.apache.org', '_blank'); void(0);">
                <img src="../../Img/sw/apache.png" alt="apache.org" title="Apache" />
            </a>
            <a onclick="Javascript: window.open('http://www-it.mysql.com/', '_blank'); void(0);">
                <img src="../../Img/sw/mysql.png" alt="mysql.com" title="MySql" />
            </a>
            <a onclick="Javascript: window.open('http://it.php.net/', '_blank'); void(0);">
                <img src="../../Img/sw/php.png" alt="php.net" title="Php" />
            </a>
            <a onclick="Javascript: window.open('http://www.mozillaitalia.it/archive/index.html#p1', '_blank'); void(0);">
                <img src="../../Img/sw/firefox3.gif" alt="firefox.gif" "Firefox"/>
            </a>
            <a onclick="Javascript: window.open('http://validator.w3.org/check?uri=referer', '_blank'); void(0);">
                <img src="../../Img/validator/mini/valid-xhtml11.png"
                     alt="Valid XHTML 1.0 Strict"  />
            </a>
            <a onclick="Javascript: window.open('http://jigsaw.w3.org/css-validator/', '_blank'); void(0);">
                <img src="../../Img/validator/mini/vcss.gif" alt="CSS Valido!" />
            </a>
        </td>
    </tr>
</table>

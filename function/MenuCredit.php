<table cellspacing="0" cellpadding="0" border="0" style="width: 100%;">
    <tr>
        <td>
            <a href="http://it.wikipedia.org/wiki/Linux">
                <img style="border: 0" src="../../Img/sw/linux.gif" alt="Linux" />
            </a>
        </td>
    </tr>
    <tr>
        <td>
            <a href="http://www.debian.org">
                <img style="border: 0" src="../../Img/sw/debian.png" alt="debian.org" />
            </a>
        </td>
    </tr>
    <tr>
        <td>
            <a href="http://www.apache.org">
                <img style="border: 0" src="../../Img/sw/apache.png" alt="apache.org" />
            </a>
        </td>
    </tr>
    <tr>
        <td>
            <a href="http://www-it.mysql.com/">
                <img style="border: 0" src="../../Img/sw/mysql.gif" alt="mysql.com" />
            </a>
        </td>
    </tr>
    <tr>
        <td>
            <a href="http://it.php.net/">
                <img style="border: 0" src="../../Img/sw/php.png" alt="php.net" />
            </a>
        </td>
    </tr>
    <tr>
        <td>
            <a href="http://validator.w3.org/check?uri=referer">
                <img src="../../Img/validator/valid-xhtml10-blue" 
                     alt="Valid XHTML 1.0 Strict" style="border:0;width:85px;height:31px" />
            </a>
        </td>
    </tr>
    <tr>
        <td>
            <a href="http://jigsaw.w3.org/css-validator/">
                <img style="border:0;width:85px;height:31px" 
                     src="../../Img/validator/vcss-blue" alt="CSS Valido!" />
            </a>
        </td>
    </tr>
</table>

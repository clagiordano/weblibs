<?php

/*
 *  DataTime.php
 *
 *      Copyright 2008 Claudio Giordano <claudio.giordano@autistici.org>
 *
 *      Funzioni globali sostituite ed ottimizzate/potenziate dalla classe DataTime
 * 
 * @deprecated
 */

function Inverti_Data($data, $separator = "/", $explode = "-")
{
    if (($data != "0000-00-00") && ($data != "00-00-0000")) {
        $data_inv   = "";
        $data_array = explode($explode, $data);
        for ($index_data = count($data_array) - 1; $index_data >= 0; $index_data--) {
            $data_inv = $data_inv . $data_array[$index_data];
            if ($index_data != 0) {
                $data_inv = $data_inv . $separator;
            }
        }
        return $data_inv;
    } else {
        return "-";
    }
}

function Inverti_DataTime($data)
{
    if (($data != "0000-00-00") && ($data != "00-00-0000")) {
        $data_inv   = "";
        $datatime   = explode(" ", $data);
        $data_array = explode("-", $datatime[0]);

        for ($index_data = count($data_array) - 1; $index_data >= 0; $index_data--) {
            $data_inv = $data_inv . $data_array[$index_data];
            if ($index_data != 0) {
                $data_inv = $data_inv . "/";
            }
        }
        return $data_inv . " " . $datatime[1];
    } else {
        return "-";
    }
}

function DateTimeDiff($data_str1, $data_str2, $format = "Y-m-d H:i:s",
                      $unit = "h")
{
    switch ($unit) {
        case "h":
            // hours
            $unit = 3600;
            break;
        case "m":
            // minute
            $unit = 60;
            break;
        case "s":
            // second
            $unit = 1;
            break;
        case "d":
            // day
            $unit = 86400;
            break;
        case "w":
            // week
            $unit = 604800;
            break;
    }

    list($data1, $time1) = explode(' ', $data_str1);
    list($data2, $time2) = explode(' ', $data_str2);

    list($year1, $month1, $day1) = explode('-', $data1);
    list($year2, $month2, $day2) = explode('-', $data2);

    list($hour1, $min1, $sec1) = explode(':', $time1);
    list($hour2, $min2, $sec2) = explode(':', $time2);

    $res1 = mktime($hour1, $min1, $sec1, $month1, $day1, $year1);
    $res2 = mktime($hour2, $min2, $sec2, $month2, $day2, $year2);

    $diff = ($res2 - $res1) / $unit;
    $diff = round($diff, 2);
    //~ echo "Debug => diff: $diff<br />";

    return $diff;
}

?>

<?php

/*
 *      Gestione.php
 *
 *      Copyright 2008 Claudio Giordano <claudio.giordano@autistici.org>
 *
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 *      MA 02110-1301, USA.
 */

/*
 * $rag_soc, $cognome, $nome, $ind, $civ, $stato, $citta,
  $prov, $cap, $tel, $fax, $cell, $piva, $mail, $cont, $id_pag, $id_tipo,

  (rag_soc, cognome, nome, ind, civ, stato, citta,
  prov, cap, tel, fax, cell, piva, mail, cont, id_pag, id_tipo) values (
 */

function SaveCliForn($array, $db)
{
    // Converte la lettera iniziale in maiuscolo:
    foreach ($array as $key => $field) {
        $array[$key] = ucfirst($field);
    }

    $Tipo             = $array['id_tipo'];
    $array['id_tipo'] = GetIdTipo($array['id_tipo'], $db);
    if (trim($array['rag_soc']) == "") {
        $array['rag_soc'] = $array['nome'] . ' ' . $array['cognome'];
    }

    if (VerificaUnicita($array['rag_soc'], $array['id_tipo'], $db) == 0) {
        /*
         * Compongo la query con i dati provenienti dall'array del post:
         */
        $q = "INSERT INTO tab_anagrafica ";
        $k = "(";
        $f = ") values (";

        foreach ($array as $key => $field) {
            if ($key != "salva") {
                $k .= "$key, ";
                $f .= "'$field', ";
            }
        }
        $f .= ");";
        $q .= $k . $f;
        $q = str_replace(", )", ")", $q);

        /*
         * Query completata.
         */

        $r = mysql_query($q, $db) or die(StampaErr($q, '[SaveCliForn] '));
        //mysql_free_result($r); inutile dato che e' una query insert.

        $status = "<label class=\"ok\">Il $Tipo è stato salvato correttamente.</label>";
    } else {
        $status = "<label class=\"err\">Errore, il $Tipo è già presente sul database.</label>";
    }

    return $status;
}

function ModCliForn($array, $ToMod, $db)
{
    // Converte la lettera iniziale in maiuscolo:
    foreach ($array as $key => $field) {
        $array[$key] = ucfirst($field);
    }

    $Tipo             = $array['id_tipo'];
    $array['id_tipo'] = GetIdTipo($array['id_tipo'], $db);
    if (trim($array['rag_soc']) == "") {
        $array['rag_soc'] = $array['nome'] . ' ' . $array['cognome'];
    }

    /*
     * Compongo la query con i dati provenienti dall'array del post:
     */
    $q = "UPDATE tab_anagrafica SET ";

    foreach ($array as $key => $field) {
        if ($key != "salva") {
            $q .= "$key='$field', ";
        }
    }

    $q .= "WHERE id_anag='$ToMod'";
    $q = str_replace(", WHERE", "WHERE", $q);

    /*
     * Query completata.
     */

    $r      = mysql_query($q, $db) or die(StampaErr($q, '[ModCliForn] '));
    $status = "<label class=\"ok\">Il $Tipo è stato modificato correttamente.</label>";

    return $status;
}

function GetIdTipo($StrTipo, $db)
{
    $q      = "SELECT * FROM tab_tipo_anag WHERE tipo = '$StrTipo'";
    $r      = mysql_query($q, $db) or die(StampaErr($q, '[GetIdTipo] '));
    $row    = mysql_fetch_assoc($r);
    $IdTipo = $row['id_anag'];
    mysql_free_result($r);

    return $IdTipo;
}

function VerificaUnicita($Str, $Tipo, $db)
{
    /*
     * Restituisce il numero di elementi corrispondenti:
     */
    $q = "SELECT * FROM tab_anagrafica WHERE rag_soc = '$Str' AND id_tipo = '$Tipo'";
    $r = mysql_query($q, $db) or die(StampaErr($q, '[VerificaUnicita] '));
    $n = mysql_affected_rows($db);
    mysql_free_result($r);

    return $n;
}

?>

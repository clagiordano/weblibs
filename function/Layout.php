<?php

// Funzioni globali sostituite ed ottimizzate/potenziate dalla classe Database ma da spostare su FormBuilder

function DrawHeader($User, $Group, $PageTitle, $DefaultPageTitle)
{
    ?>
    <table style="width: 100%;" class="header">
        <tr>
            <td style="text-align: right; width: 4em;">
                <label>Utente:</label>
            </td>

            <td style="width: 25%;">
                <a onclick="Javascript: ShowFormDiv('frm_passwd.php',
                                '100px', '100px', '500px', '200px', 'popup');
                        void(0);"
                   title="Click per modificare i dati utente">
    <?php echo $User; ?>
                </a>
            </td>

            <th rowspan="2">
                <?php
                if ($PageTitle != "") {
                    echo $PageTitle;
                } else {
                    echo $DefaultPageTitle;
                }
                ?>
            </th>

        </tr>
        <tr>
            <td style="text-align: right;">
                <label>Gruppo:</label>
            </td>
            <td>
                <label class="ok">
    <?php echo $Group; ?>
                </label>
            </td>
        </tr>
    </table>
<?php }

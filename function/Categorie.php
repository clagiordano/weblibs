<?php

/*
 *      Categorie.php
 *      
 *      Copyright 2008 Claudio Giordano <claudio.giordano@autistici.org>
 *      
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *      
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *      
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 *      MA 02110-1301, USA.
 */

function VerificaCategoria($categoria, $db)
{
    $q = "SELECT * FROM tab_categorie WHERE cat = '$categoria'";
    $r = mysql_query($q, $db) or die(StampaErr($q));
    $n = mysql_affected_rows($db);

    return $n;
}

function SaveCategoria($categoria, $db)
{
    if (VerificaCategoria($categoria, $db) == 0) {
        //echo "La categoria � nuova, la salvo e recupero l'id <br />";
        $q      = "insert into tab_categorie (cat) values ('$categoria')";
        //echo "  Q = ".$q;
        $r      = mysql_query($q, $db) or die(StampaErr($q));
        $id_cat = mysql_insert_id();
        //echo "  ID = ".$id_cat."<br/>";
    } else {
        //echo "La categoria � gia' presente, recupero soltanto l'id <br />";
        $q      = "SELECT * FROM tab_categorie WHERE cat = '$categoria'";
        $r      = mysql_query($q, $db) or die(StampaErr($q));
        $row    = mysql_fetch_assoc($r);
        $id_cat = $row['id_cat'];
    }
    return $id_cat;
}

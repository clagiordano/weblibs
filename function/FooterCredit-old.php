<table cellspacing="1" cellpadding="0" border="0">
    <tr>
        <td style="vertical-align: top;">
            <a href="http://it.wikipedia.org/wiki/Linux">
                <img style="border: 0" src="../../Img/sw/linux.gif" alt="Linux" />
            </a>
            <br />
            <a href="http://www.debian.org">
                <img style="border: 0" src="../../Img/sw/debian.png" alt="debian.org" />
            </a>
        </td>
        <td style="vertical-align: top;">
            <a href="http://www.apache.org">
                <img style="border: 0" src="../../Img/sw/apache.png" alt="apache.org" />
            </a>
            <br />
            <a href="http://www-it.mysql.com/">
                <img style="border: 0" src="../../Img/sw/mysql.png" alt="mysql.com" />
            </a>
        </td>
        <td style="vertical-align: top;">
            <a href="http://it.php.net/">
                <img style="border: 0" src="../../Img/sw/php.png" alt="php.net" />
            </a>
            <br />
            <a href="http://www.mozillaitalia.it/archive/index.html#p1">
                <img style="border: 0" src="../../Img/sw/firefox.gif" alt="firefox.gif" />
            </a>
        </td>
        <td style="vertical-align: top;">
            <a href="http://validator.w3.org/check?uri=referer">
                <img src="../../Img/validator/mini/valid-xhtml11.png"
                     alt="Valid XHTML 1.0 Strict" style="border:0;" />
            </a>
            <br />
            <a href="http://jigsaw.w3.org/css-validator/">
                <img style="border:0;"
                     src="../../Img/validator/mini/vcss.gif" alt="CSS Valido!" />
            </a>
        </td>
    </tr>
</table>

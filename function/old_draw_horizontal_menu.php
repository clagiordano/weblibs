<?php

function DrawHorizontalMenu($BasePage = "home.php")
{
    $gruppi = $this->GetRows("gruppo", "tab_menu",
                             "perm like '%"
            . $_SESSION['id_tipo'] . "%'", "gruppo", "gruppo", 1);
    ?>

    <table class="menu" >
        <tr>
            <th>
                <a href="?act=welc">
                    <img src="<?php echo ImagePath; ?>Links/home.png" alt="Home" title="Home" />
                    Home
                </a>
            </th>

    <?php foreach ($gruppi as $key => $field) { ?>
                <th>
                    <a href="?gruppo=<?php echo strtolower($field['gruppo']); ?>">
                        <img src="<?php echo ImagePath; ?>Links/down.png"
                             alt="-" title="Down" />
                <?php echo $field['gruppo']; ?>
                    </a>
                </th>
    <?php } ?>

            <th style="text-align: center;">
                <a href="logout.php">
                    <img src="<?php echo ImagePath; ?>Links/exit.png"
                         alt="Esci" title="Esci" />
                    Esci
                </a>
            </th>
        </tr>
    </table>

<?php
}

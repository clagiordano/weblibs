<?php
define("Incident", "(I00\d+)");
define("Server", 20.00);
define("Spedizioni", 0.80);
define("Staging", 3.00);
define("Sostituzioni", 12.00);
define("Riconfigurazioni", 12.00);
define("Disinstallazioni", 12.00);
define("Ritiri", 1.00);

function CheckDettaglioRequest($db, $IdFiliale, $OutputType = "compilazione")
{
    // Azzero l'array del consuntivo
    $consuntivo                = array();
    $consuntivo['sp_mon']      = 0;
    $consuntivo['sp_las']      = 0;
    $consuntivo['sp_bar']      = 0;
    $consuntivo['sp_lett']     = 0;
    $consuntivo['sp_altro_hw'] = 0;

    $consuntivo['sos_mon']  = 0;
    $consuntivo['sos_las']  = 0;
    $consuntivo['sos_bar']  = 0;
    $consuntivo['sos_lett'] = 0;


    // Estraggo il contenuto della request richiesta
    $request = GetRows("view_requests", "id_filiale = '"
            . $IdFiliale . "'", "", $db, 1);

    // Estraggo il dettaglio della request richiesta
    $dettaglio = GetRows("view_macchine_assegnate",
                         "id_request = '"
            . $request[0]['id_request'] . "'", "tipo_mch", $db, 1);

    /*
     *  Verifica del tipo di server richiesto e consegnato:
     */
    if (preg_match(Incident, $request[0]['i_server'])) {
        // E' presente un incident per il server portato in filiale:
        $srv_richiesto = "Riciclato";
        //$consuntivo['server'] = "Riciclato";
    } else {
        // il server è nuovo:
        $srv_richiesto = "Nuovo";
        //$consuntivo['server'] = "Nuovo";
    }

    /*
     *  Verifica il dettaglio della request:
     */
    echo "[Debug]: totale pezzi: " . count($dettaglio) . " <br />";
    if (count($dettaglio) > 0) {
        //echo "[Debug]: totale pezzi: " . count($dettaglio) . " <br />";
        $RequestString = "";
        $n_pz          = 0;

        foreach ($dettaglio as $key => $field) {
            // Server:
            if (($field['tipo_mch'] == "Server") && $field['provenienza'] == "FORNITORE") {
                // il server è nuovo:
                $consuntivo['sp_srv_new'] += 1;  // spedizione server nuovo
                $consuntivo['st_srv'] += 1;   // staging
                $consuntivo['sos_srv'] += 1;   // sostituzione
                $n_pz += 1;
            }

            if (($field['tipo_mch'] == "Server") && ($field['provenienza'] == "MAINTENANCE")) {
                //echo "[Debug]: il server proviene dal polmone
                // spedizione server da maintenance
                $consuntivo['qi_server'] += 1;

                /*
                 * se il server non è nuovo non ha nè spedizione
                 * nè staging nè sostituzione
                 */
                $consuntivo['sp_srv_new'] = 0;
                $consuntivo['st_srv']     = 0;
                $consuntivo['sos_srv']    = 0;
                $n_pz += 1;
            }

            if (($field['tipo_mch'] == "Server") && $field['provenienza'] == "DA FILIALE") {
                /*
                 *  il server è riciclato,
                 * verifico su quante request corrisponde
                 */
                //echo "[Debug]: il server è riciclato, verifico su
                //quante request corrisponde <br />";
                $ck_srv = GetRows("tab_dett_requests",
                                  "id_macchina = '" . $field['id_macchina'] . "'"
                        . " AND id_request != '"
                        . $_SESSION['rollout']['id_request'] . "'", "", $db, 1);

                if (count($ck_srv) > 0) {
                    //echo "[Debug]: il server sta tornando in filiale come nuovo <br />";
                    //$consuntivo['qi_server'] += 1;
                    //$n_pz += 1;
                } else {
                    //echo "[Debug]: altrimenti il server è vecchio <br />";
                    if ($consuntivo['sp_srv_new'] > 0) {
                        /* se la request prevede la sostituzione del
                         * server vecchio con uno nuovo
                         * allora bisogna considerare il ritiro del vecchio server */
                        $consuntivo['rit_srv'] += 1;
                    }

                    $n_pz += 1;
                }
            }

            if (trim($field['provenienza']) == "FORNITORE") {
                // switch tipo mch
//					case "Desktop":
//						break;
            }



            // Pdl Nuovi sostituzione:
            if (($field['tipo_mch'] == "Desktop") && (trim($field['provenienza']) == "FORNITORE")) {
                //echo "[Debug]: Pdl nuovo in sostituzione <br />";
                //$sess_st_pdl += 1;
                $consuntivo['sp_pdl'] += 1;  // spedizione
                $consuntivo['st_pdl'] += 1;  // staging
                $consuntivo['sos_pdl'] += 1;  // sostituzione
                $consuntivo['dis_pdl'] = ($consuntivo['rit_pdl'] - $consuntivo['sos_pdl']);
                $n_pz += 1;
            }

            // Monitor Nuovi
            if (($field['tipo_mch'] == "Monitor") && (trim($field['provenienza']) == "FORNITORE")) {
                //echo "[Debug]: Pdl nuovo in sostituzione <br />";
                //$sess_st_pdl += 1;
                $consuntivo['sp_mon'] += 1;  // spedizione
                $consuntivo['sos_mon'] += 1;  // sostituzione
                $n_pz += 1;
            }

            // Prt laser nuove:
            if (($field['tipo_mch'] == "Stampante laser") && (trim($field['provenienza']) == "FORNITORE")) {
                //echo "[Debug]: printer laser nuova in sostituzione <br />";
                $consuntivo['sp_las'] += 1;
                $consuntivo['sos_las'] += 1;
                $n_pz += 1;
            }

            // Prt lan nuove:
            if (($field['tipo_mch'] == "Stampante di rete") && (trim($field['provenienza']) == "FORNITORE")) {
                //echo "[Debug]: printer di rete nuova in sostituzione <br />";
                $consuntivo['sp_prt_lan'] += 1;
                $consuntivo['sos_prt_lan'] += 1;
                $n_pz += 1;
            }

            // lettore assegni:
            if (($field['tipo_mch'] == "Lettore assegni") && (trim($field['provenienza']) == "FORNITORE")) {
                //echo "[Debug]: lettore assegni nuovo in sostituzione <br />";
                $consuntivo['sp_lett'] += 1;
                $consuntivo['sos_lett'] += 1;
                $n_pz += 1;
            }

            // lettore barcode:
            if (($field['tipo_mch'] == "Lettore barcode") && (trim($field['provenienza']) == "FORNITORE")) {
                //echo "[Debug]: lettore barcode nuovo in sostituzione <br />";
                $consuntivo['sp_lett'] += 1;
                $consuntivo['sos_lett'] += 1;
                $n_pz += 1;
            }

            // altro hw nuovo con provenienza fornitore (sp_altro_hw):
            if (($field['tipo_mch'] == "Lettore barcode") && (trim($field['provenienza']) == "FORNITORE")) {
                //echo "[Debug]: lettore barcode nuovo in sostituzione <br />";
                $consuntivo['sp_lett'] += 1;
                $consuntivo['sos_lett'] += 1;
                $n_pz += 1;
            }

            /*
             * Ritiro pdl old:
             *
             * tipo: 'Desktop' con provenienza 'DA FILIALE'
             * oppure inserimento rapido materiale vecchio
             * quindi con id_macchina == 'new'
             */

            if (($field['tipo_mch'] == "Desktop") && (trim($field['provenienza']) == "DA FILIALE")) {
                //echo "[Debug]: ritito pdl old <br />";
                $consuntivo['rit_pdl'] += 1;
                $consuntivo['dis_pdl'] = ($consuntivo['rit_pdl'] - $consuntivo['sos_pdl']);
                $n_pz += 1;
            }

            // Ritiro monitor
            if (($field['tipo_mch'] == "Monitor Lcd" || $field['tipo_mch'] == "Monitor Crt") && (trim($field['provenienza']) == "DA FILIALE")) {
                //echo "[Debug]: ritito monitor <br />";
                $consuntivo['rit_mon'] += 1;
                $consuntivo['dis_mon'] = ($consuntivo['rit_mon'] - $consuntivo['sos_mon']);
                $n_pz += 1;
            }

            // Ritiro lettorini
            if (($field['tipo_mch'] == "Lettore assegni") && (trim($field['provenienza']) == "DA FILIALE")) {
                //echo "[Debug]: ritiro lettore assegni <br />";
                $consuntivo['rit_let'] += 1;
                $n_pz += 1;
            }

            // Ritiro stampanti di cassa
            if (($field['tipo_mch'] == "Stampante di cassa") && (trim($field['provenienza']) == "DA FILIALE")) {
                //echo "[Debug]: ritiro stampante di cassa <br />";
                $consuntivo['rit_cas'] += 1;
                $n_pz += 1;
            }

            // Ritiro stampanti laser
            if (($field['tipo_mch'] == "Stampante laser") && (trim($field['provenienza']) == "DA FILIALE")) {
                //echo "[Debug]: ritiro stampante laser <br />";
                $consuntivo['rit_las'] += 1;
                $consuntivo['dis_las'] = ""; //($consuntivo['rit_las']-$request[0]['dis_las']);
                $n_pz += 1;
            }
        }

        echo "[Debug]: dis_pdl: " . $consuntivo['dis_pdl'] . " <br />";
        echo "[Debug]: dis_mon: " . $consuntivo['dis_mon'] . " impossibile conteggiare <br />";
        echo "[Debug]: dis_las: " . $consuntivo['dis_las'] . " impossibile conteggiare <br />";
        /*
          echo "<pre>";
          print_r($consuntivo);
          echo "</pre>";
         */
        ?>
        <table class="dettaglio" style="width: 50%;" rules="all">
            <tr>
                <th colspan="5">
                    Consuntivo Filiale: <?php echo $request[0]['den']; ?>

                    &nbsp;
                    <a onclick="Javascript: go('home.php?act=rollout&amp;reset=0&amp;tomod=<?php echo $request[0]['id_request']; ?>'); void(0);"
                       title="Compila Dettaglio Request">
                        <img src="/Images/Links/arch.png" alt="arch" />
                    </a>

                    &nbsp;
                    <a onclick="Javascript: window.open('frm_pdf_dett_request.php?rid=<?php echo $request[0]['id_request']; ?>', '_blank'); void(0);"
                       title="Stampa Riepilogo Request">
                        <img src="/Images/Links/pdf.png" alt="pdf" />
                    </a>
                </th>
            </tr>

            <tr><th colspan="5"><br /></th></tr>

            <tr>
                <th colspan="5">
                    Spedizioni:
                    <label><?php printf("( %.2f € pezzo )", Spedizioni); ?></label>
                </th>
            </tr>

            <tr>
                <th style="text-align: center;"><label>Tipo Macchina:</label></th>
                <th style="text-align: center;"><label>Previsti:</label></th>
                <th style="text-align: center;"><label>Effettivi:</label></th>
                <th style="text-align: center;"><label>Bilancio:</label></th>
                <th style="text-align: center;"><label>Subtotale:</label></th>
            </tr>

                        <!--<tr>
                            <td style="text-align: right;">Server Nuovo:</td>
                            <td style="text-align: center;" title="Previsti">
        <?php //echo $request[0]['sp_srv_new'];  ?>
                            </td>
                            <td style="text-align: center;" title="Effettivi">
        <?php //echo $consuntivo['sp_srv_new'];  ?>
                            </td>
                            <td style="text-align: center;" title="Bilancio">
                       <?php //echo ($consuntivo['sp_srv_new']-$request[0]['sp_srv_new']); ?>
                            </td>
                            <td style="text-align: center;">
        <?php //printf("%.2f €", ($consuntivo['sp_srv_new']*Spedizioni));  ?>
                            </td>
                        </tr> !-->

            <tr>
                <td style="text-align: right;"><label>Pdl Nuovi:</label></td>
                <td style="text-align: center;" title="Previsti">
                    <label class="err"><?php echo $request[0]['sp_pdl']; ?></label>
                </td>
                <td style="text-align: center;" title="Effettivi">
                    <label class="ok"><?php echo $consuntivo['sp_pdl']; ?></label>
                </td>
                <td style="text-align: center;" title="Bilancio">
                    <label><?php echo ($consuntivo['sp_pdl'] - $request[0]['sp_pdl']); ?></label>
                </td>
                <td style="text-align: center;">
                    <label class="ok">
        <?php
        if (($consuntivo['sp_pdl'] * Spedizioni) == 0) {
            echo "-";
        } else {
            printf("%.2f €", ($consuntivo['sp_pdl'] * Spedizioni));
        }
        ?>
                    </label>
                </td>
            </tr>

            <tr>
                <td style="text-align: right;">Monitor:</td>
                <td style="text-align: center;" title="Previsti">
            <?php echo $request[0]['sp_mon']; ?>
                </td>
                <td style="text-align: center;" title="Effettivi">
            <?php echo $consuntivo['sp_mon']; ?>
                </td>
                <td style="text-align: center;" title="Bilancio">
            <?php echo ($consuntivo['sp_mon'] - $request[0]['sp_mon']); ?>
                </td>
                <td style="text-align: center;">
            <?php
            if (($consuntivo['sp_mon'] * Spedizioni) == 0) {
                echo "-";
            } else {
                printf("%.2f €", ($consuntivo['sp_mon'] * Spedizioni));
            }
            ?>
                </td>
            </tr>

            <tr>
                <td style="text-align: right;">Stampante Laser:</td>
                <td style="text-align: center;" title="Previsti">
        <?php echo $request[0]['sp_las']; ?>
                </td>
                <td style="text-align: center;" title="Effettivi">
                        <?php echo $consuntivo['sp_las']; // aggiungere check sul dettaglio  ?>
                </td>
                <td style="text-align: center;" title="Bilancio">
                        <?php echo ($consuntivo['sp_las'] - $request[0]['sp_las']); ?>
                </td>
                <td style="text-align: center;">
                        <?php
                        if (($consuntivo['sp_las'] * Spedizioni) == 0) {
                            echo "-";
                        } else {
                            printf("%.2f €", ($consuntivo['sp_las'] * Spedizioni));
                        }
                        ?>
                </td>
            </tr>

            <tr>
                <td style="text-align: right;">Lettore Barcode:</td>
                <td style="text-align: center;" title="Previsti">
        <?php echo $request[0]['sp_bar']; ?>
                </td>
                <td style="text-align: center;" title="Effettivi">
        <?php echo $consuntivo['sp_bar']; // aggiungere check sul dettaglio  ?>
                </td>
                <td style="text-align: center;" title="Bilancio">
                    <?php echo ($consuntivo['sp_bar'] - $request[0]['sp_bar']); ?>
                </td>
                <td style="text-align: center;">
                    <?php
                    if (($consuntivo['sp_bar'] * Spedizioni) == 0) {
                        echo "-";
                    } else {
                        printf("%.2f €", ($consuntivo['sp_bar'] * Spedizioni));
                    }
                    ?>
                </td>
            </tr>

            <tr>
                <td style="text-align: right;">Lettore Assegni:</td>
                <td style="text-align: center;" title="Previsti">
        <?php echo $request[0]['sp_lett']; ?>
                </td>
                <td style="text-align: center;" title="Effettivi">
        <?php echo $consuntivo['sp_lett']; // aggiungere check sul dettaglio  ?>
                </td>
                <td style="text-align: center;" title="Bilancio">
                    <?php echo ($consuntivo['sp_lett'] - $request[0]['sp_lett']); ?>
                </td>
                <td style="text-align: center;">
                    <?php
                    if (($consuntivo['sp_lett'] * Spedizioni) == 0) {
                        echo "-";
                    } else {
                        printf("%.2f €", ($consuntivo['sp_lett'] * Spedizioni));
                    }
                    ?>
                </td>
            </tr>

            <tr>
                <td style="text-align: right;">Stampante di rete:</td>
                <td style="text-align: center;" title="Previsti">
        <?php echo $request[0]['sp_prt_lan']; ?>
                </td>
                <td style="text-align: center;" title="Effettivi">
        <?php echo $consuntivo['sp_prt_lan']; ?>
                </td>
                <td style="text-align: center;" title="Bilancio">
                    <?php echo ($consuntivo['sp_prt_lan'] - $request[0]['sp_prt_lan']); ?>
                </td>
                <td style="text-align: center;">
                    <?php
                    if (($consuntivo['sp_prt_lan'] * Spedizioni) == 0) {
                        echo "-";
                    } else {
                        printf("%.2f €", ($consuntivo['sp_prt_lan'] * Spedizioni));
                    }
                    ?>
                </td>
            </tr>

            <tr>
                <td style="text-align: right;">Altro Hardware:</td>
                <td style="text-align: center;" title="Previsti">
        <?php echo $request[0]['sp_altro_hw']; ?>
                </td>
                <td style="text-align: center;" title="Effettivi">
        <?php echo $consuntivo['sp_altro_hw']; // aggiungere check sul dettaglio  ?>
                </td>
                <td style="text-align: center;" title="Bilancio">
                    <?php echo ($consuntivo['sp_altro_hw'] - $request[0]['sp_altro_hw']); ?>
                </td>
                <td style="text-align: center;">
                    <?php
                    if (($consuntivo['sp_altro_hw'] * Spedizioni) == 0) {
                        echo "-";
                    } else {
                        printf("%.2f €", ($consuntivo['sp_altro_hw'] * Spedizioni));
                    }
                    ?>
                </td>
            </tr>

            <tr><th colspan="5"><br /></th></tr>

            <tr>
                <th colspan="5">
                    Staging:
                    <label><?php printf("( %.2f € pezzo )", Staging); ?></label>
                </th>
            </tr>

            <tr>
                <td style="text-align: center;"><label>Tipo Macchina:</label></td>
                <td style="text-align: center;"><label>Previsti:</label></td>
                <td style="text-align: center;"><label>Effettivi:</label></td>
                <td style="text-align: center;"><label>Bilancio:</label></td>
                <td style="text-align: center;"><label>Subtotale:</label></td>
            </tr>

                        <!--<tr>
                            <td style="text-align: right;">Server Nuovo:</td>
                            <td style="text-align: center;" title="Previsti">
                    <?php //echo $request[0]['st_srv'];  ?>
                            </td>
                            <td style="text-align: center;" title="Effettivi">
                    <?php //echo $consuntivo['st_srv'];  ?>
                            </td>
                            <td style="text-align: center;" title="Bilancio">
                    <?php //echo ($consuntivo['st_srv']-$request[0]['st_srv']);  ?>
                            </td>
                            <td style="text-align: center;">
                    <?php //printf("%.2f €", ($consuntivo['st_srv']*Staging));  ?>
                            </td>
                        </tr>!-->

            <tr>
                <td style="text-align: right;">Pdl Nuovi:</td>
                <td style="text-align: center;" title="Previsti">
                    <?php echo $request[0]['st_pdl']; ?>
                </td>
                <td style="text-align: center;" title="Effettivi">
        <?php echo $consuntivo['st_pdl']; ?>
                </td>
                <td style="text-align: center;" title="Bilancio">
        <?php echo ($consuntivo['st_pdl'] - $request[0]['st_pdl']); ?>
                </td>
                <td style="text-align: center;">
        <?php
        if (($consuntivo['st_pdl'] * Staging) == 0) {
            echo "-";
        } else {
            printf("%.2f €", ($consuntivo['st_pdl'] * Staging));
        }
        ?>
                </td>
            </tr>

            <tr><th colspan="5"><br /></th></tr>

            <tr>
                <th colspan="5">
                    Sostituzioni:
                    <label><?php printf("( %.2f € pezzo  )", Sostituzioni); ?></label>
                </th>
            </tr>

            <tr>
                <td style="text-align: center;"><label>Tipo Macchina:</label></td>
                <td style="text-align: center;"><label>Previsti:</label></td>
                <td style="text-align: center;"><label>Effettivi:</label></td>
                <td style="text-align: center;"><label>Bilancio:</label></td>
                <td style="text-align: center;"><label>Subtotale:</label></td>
            </tr>

                        <!--<tr>
                            <td style="text-align: right;">Server:</td>
                            <td style="text-align: center;" title="Previsti">
                    <?php //echo $request[0]['sos_srv'];  ?>
                            </td>
                            <td style="text-align: center;" title="Effettivi">
                    <?php //echo $consuntivo['sos_srv'];  ?>
                            </td>
                            <td style="text-align: center;" title="Bilancio">
                    <?php //echo ($consuntivo['sos_srv']-$request[0]['sos_srv']);  ?>
                            </td>
                            <td style="text-align: center;">
                    <?php //printf("%.2f €", ($consuntivo['sos_srv']*Sostituzioni));  ?>
                            </td>
                        </tr>!-->

            <tr>
                <td style="text-align: right;">Pdl:</td>
                <td style="text-align: center;" title="Previsti">
                    <?php echo $request[0]['sos_pdl']; ?>
                </td>
                <td style="text-align: center;" title="Effettivi">
        <?php echo $consuntivo['sos_pdl']; ?>
                </td>
                <td style="text-align: center;" title="Bilancio">
        <?php echo ($consuntivo['sos_pdl'] - $request[0]['sos_pdl']); ?>
                </td>
                <td style="text-align: center;">
        <?php
        if (($consuntivo['sos_pdl'] * Sostituzioni) == 0) {
            echo "-";
        } else {
            printf("%.2f €", ($consuntivo['sos_pdl'] * Sostituzioni));
        }
        ?>
                </td>
            </tr>

            <tr>
                <td style="text-align: right;">Monitor:</td>
                <td style="text-align: center;" title="Previsti">
        <?php echo $request[0]['sos_mon']; ?>
                </td>
                <td style="text-align: center;" title="Effettivi">
        <?php echo $consuntivo['sos_mon']; ?>
                </td>
                <td style="text-align: center;" title="Bilancio">
        <?php echo ($consuntivo['sos_mon'] - $request[0]['sos_mon']); ?>
                </td>
                <td style="text-align: center;">
        <?php
        if (($consuntivo['sos_mon'] * Sostituzioni) == 0) {
            echo "-";
        } else {
            printf("%.2f €", ($consuntivo['sos_mon'] * Sostituzioni));
        }
        ?>
                </td>
            </tr>

            <tr>
                <td style="text-align: right;">Stampante di rete:</td>
                <td style="text-align: center;" title="Previsti">
        <?php echo $request[0]['sos_prt_lan']; ?>
                </td>
                <td style="text-align: center;" title="Effettivi">
        <?php echo $consuntivo['sos_prt_lan']; ?>
                </td>
                <td style="text-align: center;" title="Bilancio">
                    <?php echo ($consuntivo['sos_prt_lan'] - $request[0]['sos_prt_lan']); ?>
                </td>
                <td style="text-align: center;">
                    <?php
                    if (($consuntivo['sos_prt_lan'] * Sostituzioni) == 0) {
                        echo "-";
                    } else {
                        printf("%.2f €",
                               ($consuntivo['sos_prt_lan'] * Sostituzioni));
                    }
                    ?>
                </td>
            </tr>

            <tr>
                <td style="text-align: right;">Stampante laser:</td>
                <td style="text-align: center;" title="Previsti">
                    <?php echo $request[0]['sos_las']; ?>
                </td>
                <td style="text-align: center;" title="Effettivi">
                    <?php echo $consuntivo['sos_las']; ?>
                </td>
                <td style="text-align: center;" title="Bilancio">
                    <?php echo ($consuntivo['sos_las'] - $request[0]['sos_las']); ?>
                </td>
                <td style="text-align: center;">
                    <?php
                    if (($consuntivo['sos_las'] * Sostituzioni) == 0) {
                        echo "-";
                    } else {
                        printf("%.2f €", ($consuntivo['sos_las'] * Sostituzioni));
                    }
                    ?>
                </td>
            </tr>

            <tr>
                <td style="text-align: right;">Lettore Assegni:</td>
                <td style="text-align: center;" title="Previsti">
                    <?php echo $request[0]['sos_lett']; ?>
                </td>
                <td style="text-align: center;" title="Effettivi">
                    <?php echo $consuntivo['sos_lett']; ?>
                </td>
                <td style="text-align: center;" title="Bilancio">
                    <?php echo ($consuntivo['sos_lett'] - $request[0]['sos_lett']); ?>
                </td>
                <td style="text-align: center;">
                    <?php
                    if (($consuntivo['sos_lett'] * Sostituzioni) == 0) {
                        echo "-";
                    } else {
                        printf("%.2f €", ($consuntivo['sos_lett'] * Sostituzioni));
                    }
                    ?>
                </td>
            </tr>

            <tr>
                <td style="text-align: right;">Lettore Barcode:</td>
                <td style="text-align: center;" title="Previsti">
                    <?php echo $request[0]['sos_bar']; ?>
                </td>
                <td style="text-align: center;" title="Effettivi">
                    <?php echo $consuntivo['sos_bar']; ?>
                </td>
                <td style="text-align: center;" title="Bilancio">
                    <?php echo ($consuntivo['sos_bar'] - $request[0]['sos_bar']); ?>
                </td>
                <td style="text-align: center;">
                    <?php
                    if (($consuntivo['sos_bar'] * Sostituzioni) == 0) {
                        echo "-";
                    } else {
                        printf("%.2f €", ($consuntivo['sos_bar'] * Sostituzioni));
                    }
                    ?>
                </td>
            </tr>

            <tr><th colspan="5"><br /></th></tr>

            <tr>
                <th colspan="5">
                    Riconfigurazioni:
                    <label><?php printf("( %.2f € pezzo )", Riconfigurazioni); ?></label>
                </th>
            </tr>

            <tr>
                <td style="text-align: center;"><label>Tipo Macchina:</label></td>
                <td style="text-align: center;"><label>Previsti:</label></td>
                <td style="text-align: center;"><label>Effettivi:</label></td>
                <td style="text-align: center;"><label>Bilancio:</label></td>
                <td style="text-align: center;"><label>Subtotale:</label></td>
            </tr>

            <tr>
                <td style="text-align: right;">Stampante di rete:</td>
                <td style="text-align: center;" title="Previsti">
                    <?php echo $request[0]['ric_prt_lan']; ?>
                </td>
                <td style="text-align: center;" title="Effettivi">
                    <?php
                    /* dato che sul dettaglio con viene inserito
                     * nulla riguardo le riconfigurazioni,
                     * non utilizzo la variabile: $consuntivo['ric_prt_lan'];
                     * ma riutilizzo lo stesso valore richiesto. */
                    echo $request[0]['ric_prt_lan'];
                    ?>
                </td>
                <td style="text-align: center;" title="Bilancio">
                    <?php echo ($request[0]['ric_prt_lan'] - $request[0]['ric_prt_lan']); ?>
                </td>
                <td style="text-align: center;">
                    <?php
                    if ((($request[0]['ric_prt_lan']) * Riconfigurazioni) == 0) {
                        echo "-";
                    } else {
                        printf("%.2f €",
                               ($request[0]['ric_prt_lan']) * Riconfigurazioni);
                    }
                    ?>
                </td>
            </tr>

            <tr>
                <td style="text-align: right;">Multifunzione:</td>
                <td style="text-align: center;" title="Previsti">
        <?php echo $request[0]['ric_multi']; ?>
                </td>
                <td style="text-align: center;" title="Effettivi">
        <?php
        /* dato che sul dettaglio con viene inserito
         * nulla riguardo le riconfigurazioni,
         * non utilizzo la variabile: $consuntivo['ric_multi'];
         * ma riutilizzo lo stesso valore richiesto. */
        echo $request[0]['ric_multi'];
        ?>
                </td>
                <td style="text-align: center;" title="Bilancio">
                    <?php echo ($request[0]['ric_multi'] - $request[0]['ric_multi']); ?>
                </td>
                <td style="text-align: center;">
                    <?php
                    if ((($request[0]['ric_multi']) * Riconfigurazioni) == 0) {
                        echo "-";
                    } else {
                        printf("%.2f €",
                               ($request[0]['ric_multi']) * Riconfigurazioni);
                    }
                    ?>
                </td>
            </tr>

            <tr><th colspan="5"><br /></th></tr>

            <tr>
                <th colspan="5">
                    Disinstallazioni:
                    <label><?php printf("( %.2f € pezzo )", Disinstallazioni); ?></label>
                </th>
            </tr>

            <tr>
                <td style="text-align: center;"><label>Tipo Macchina:</label></td>
                <td style="text-align: center;"><label>Previsti:</label></td>
                <td style="text-align: center;"><label>Effettivi:</label></td>
                <td style="text-align: center;"><label>Bilancio:</label></td>
                <td style="text-align: center;"><label>Subtotale:</label></td>
            </tr>

            <tr>
                <td style="text-align: right;">Server:</td>
                <td style="text-align: center;" title="Previsti">
                    <?php echo $request[0]['dis_srv']; ?>
                </td>
                <td style="text-align: center;" title="Effettivi">
                    <?php echo $consuntivo['dis_srv']; ?>
                </td>
                <td style="text-align: center;" title="Bilancio">
                    <?php echo ($consuntivo['dis_srv'] - $request[0]['dis_srv']); ?>
                </td>
                <td style="text-align: center;">
                    <?php
                    if ((($request[0]['dis_srv']) * Disinstallazioni) == 0) {
                        echo "-";
                    } else {
                        printf("%.2f €",
                               ($request[0]['dis_srv']) * Disinstallazioni);
                    }
                    ?>
                </td>
            </tr>

            <tr>
                <td style="text-align: right;">Pdl:</td>
                <td style="text-align: center;" title="Previsti">
        <?php echo $request[0]['dis_pdl']; ?>
                </td>
                <td style="text-align: center;" title="Effettivi">
        <?php echo $consuntivo['dis_pdl']; ?>
                </td>
                <td style="text-align: center;" title="Bilancio">
        <?php echo ($consuntivo['dis_pdl'] - $request[0]['dis_pdl']); ?>
                </td>
                <td style="text-align: center;">
        <?php
        if ((($consuntivo['dis_pdl']) * Disinstallazioni) == 0) {
            echo "-";
        } else {
            printf("%.2f €", ($consuntivo['dis_pdl']) * Disinstallazioni);
        }
        ?>
                </td>
            </tr>

            <tr>
                <td style="text-align: right;">Monitor:</td>
                <td style="text-align: center;" title="Previsti">
                    <?php echo $request[0]['dis_mon']; ?>
                </td>
                <td style="text-align: center;" title="Effettivi">
                    <?php echo $consuntivo['dis_mon']; ?>
                </td>
                <td style="text-align: center;" title="Bilancio">
                    <?php echo ($consuntivo['dis_mon'] - $request[0]['dis_mon']); ?>
                </td>
                <td style="text-align: center;">
        <?php
        if ((($consuntivo['dis_mon']) * Disinstallazioni) == 0) {
            echo "-";
        } else {
            printf("%.2f €", ($consuntivo['dis_mon']) * Disinstallazioni);
        }
        ?>
                </td>
            </tr>

            <tr>
                <td style="text-align: right;">Stampante laser:</td>
                <td style="text-align: center;" title="Previsti">
                    <?php echo $request[0]['dis_las']; ?>
                </td>
                <td style="text-align: center;" title="Effettivi">
                    <?php echo $consuntivo['dis_las']; ?>
                </td>
                <td style="text-align: center;" title="Bilancio">
                    <?php echo ($consuntivo['dis_las'] - $request[0]['dis_las']); ?>
                </td>
                <td style="text-align: center;">
        <?php
        if ((($consuntivo['dis_las']) * Disinstallazioni) == 0) {
            echo "-";
        } else {
            printf("%.2f €", ($consuntivo['dis_las']) * Disinstallazioni);
        }
        ?>
                </td>
            </tr>

            <tr>
                <td style="text-align: right;">Stampante di cassa:</td>
                <td style="text-align: center;" title="Previsti">
                    <?php echo $request[0]['dis_cas']; ?>
                </td>
                <td style="text-align: center;" title="Effettivi">
                    <?php echo $consuntivo['dis_cas']; ?>
                </td>
                <td style="text-align: center;" title="Bilancio">
                    <?php echo ($consuntivo['dis_cas'] - $request[0]['dis_cas']); ?>
                </td>
                <td style="text-align: center;">
                    subtotale
                </td>
            </tr>

            <tr>
                <td style="text-align: right;">Lettore Assegni:</td>
                <td style="text-align: center;" title="Previsti">
                    <?php echo $request[0]['dis_let']; ?>
                </td>
                <td style="text-align: center;" title="Effettivi">
                    <?php echo $consuntivo['dis_let']; ?>
                </td>
                <td style="text-align: center;" title="Bilancio">
                    <?php echo ($consuntivo['dis_let'] - $request[0]['dis_let']); ?>
                </td>
                <td style="text-align: center;">
                    subtotale
                </td>
            </tr>

            <tr>
                <td style="text-align: right;">Lettore Barcode:</td>
                <td style="text-align: center;" title="Previsti">
        <?php echo $request[0]['dis_bar']; ?>
                </td>
                <td style="text-align: center;" title="Effettivi">
                    <?php echo $consuntivo['dis_bar']; ?>
                </td>
                <td style="text-align: center;" title="Bilancio">
                    <?php echo ($consuntivo['dis_bar'] - $request[0]['dis_bar']); ?>
                </td>
                <td style="text-align: center;">
                    subtotale
                </td>
            </tr>

            <tr>
                <td style="text-align: right;">Fax:</td>
                <td style="text-align: center;" title="Previsti">
        <?php echo $request[0]['dis_fax']; ?>
                </td>
                <td style="text-align: center;" title="Effettivi">
                    <?php echo $consuntivo['dis_fax']; ?>
                </td>
                <td style="text-align: center;" title="Bilancio">
                    <?php echo ($consuntivo['dis_fax'] - $request[0]['dis_fax']); ?>
                </td>
                <td style="text-align: center;">
                    subtotale
                </td>
            </tr>

            <tr>
                <td style="text-align: right;">Altro Hardware:</td>
                <td style="text-align: center;" title="Previsti">
        <?php echo $request[0]['dis_altro_hw']; ?>
                </td>
                <td style="text-align: center;" title="Effettivi">
                    <?php echo $consuntivo['dis_altro_hw']; ?>
                </td>
                <td style="text-align: center;" title="Bilancio">
                    <?php echo ($consuntivo['dis_altro_hw'] - $request[0]['dis_altro_hw']); ?>
                </td>
                <td style="text-align: center;">
                    subtotale
                </td>
            </tr>

            <tr><th colspan="5"><br /></th></tr>

            <tr>
                <th colspan="5">
                    Ritiri:
                    <label><?php printf("( %.2f € pezzo )", Ritiri); ?></label>
                </th>
            </tr>

            <tr>
                <td style="text-align: center;"><label>Tipo Macchina:</label></td>
                <td style="text-align: center;"><label>Previsti:</label></td>
                <td style="text-align: center;"><label>Effettivi:</label></td>
                <td style="text-align: center;"><label>Bilancio:</label></td>
                <td style="text-align: center;"><label>Subtotale:</label></td>
            </tr>

            <tr>
                <td style="text-align: right;">Server:</td>
                <td style="text-align: center;" title="Previsti">
        <?php echo $request[0]['rit_srv']; ?>
                </td>
                <td style="text-align: center;" title="Effettivi">
                    <?php echo $consuntivo['rit_srv']; ?>
                </td>
                <td style="text-align: center;" title="Bilancio">
                    <?php echo ($consuntivo['rit_srv'] - $request[0]['rit_srv']); ?>
                </td>
                <td style="text-align: center;">
                    subtotale
                </td>
            </tr>

            <tr>
                <td style="text-align: right;">Pdl:</td>
                <td style="text-align: center;" title="Previsti">
        <?php echo $request[0]['rit_pdl']; ?>
                </td>
                <td style="text-align: center;" title="Effettivi">
        <?php echo $consuntivo['rit_pdl']; ?>
                </td>
                <td style="text-align: center;" title="Bilancio">
        <?php echo ($consuntivo['rit_pdl'] - $request[0]['rit_pdl']); ?>
                </td>
                <td style="text-align: center;">
                    subtotale
                </td>
            </tr>

            <tr>
                <td style="text-align: right;">Monitor:</td>
                <td style="text-align: center;" title="Previsti">
        <?php echo $request[0]['rit_mon']; ?>
                </td>
                <td style="text-align: center;" title="Effettivi">
                    <?php echo $consuntivo['rit_mon']; ?>
                </td>
                <td style="text-align: center;" title="Bilancio">
                    <?php echo ($consuntivo['rit_mon'] - $request[0]['rit_mon']); ?>
                </td>
                <td style="text-align: center;">
                    subtotale
                </td>
            </tr>

            <tr>
                <td style="text-align: right;">Stampante laser:</td>
                <td style="text-align: center;" title="Previsti">
        <?php echo $request[0]['rit_las']; ?>
                </td>
                <td style="text-align: center;" title="Effettivi">
                    <?php echo $consuntivo['rit_las']; ?>
                </td>
                <td style="text-align: center;" title="Bilancio">
                    <?php echo ($consuntivo['rit_las'] - $request[0]['rit_las']); ?>
                </td>
                <td style="text-align: center;">
                    subtotale
                </td>
            </tr>

            <tr>
                <td style="text-align: right;">Stampante di rete:</td>
                <td style="text-align: center;" title="Previsti">
        <?php echo $request[0]['rit_prt_lan']; ?>
                </td>
                <td style="text-align: center;" title="Effettivi">
                    <?php echo $consuntivo['rit_prt_lan']; ?>
                </td>
                <td style="text-align: center;" title="Bilancio">
                    <?php echo ($consuntivo['rit_prt_lan'] - $request[0]['rit_prt_lan']); ?>
                </td>
                <td style="text-align: center;">
                    subtotale
                </td>
            </tr>

            <tr>
                <td style="text-align: right;">Stampante di cassa:</td>
                <td style="text-align: center;" title="Previsti">
        <?php echo $request[0]['rit_cas']; ?>
                </td>
                <td style="text-align: center;" title="Effettivi">
                    <?php echo $consuntivo['rit_cas']; ?>
                </td>
                <td style="text-align: center;" title="Bilancio">
                    <?php echo ($consuntivo['rit_cas'] - $request[0]['rit_cas']); ?>
                </td>
                <td style="text-align: center;">
                    subtotale
                </td>
            </tr>

            <tr>
                <td style="text-align: right;">Lettore Assegni:</td>
                <td style="text-align: center;" title="Previsti">
        <?php echo $request[0]['rit_let']; ?>
                </td>
                <td style="text-align: center;" title="Effettivi">
                    <?php echo $consuntivo['rit_let']; ?>
                </td>
                <td style="text-align: center;" title="Bilancio">
                    <?php echo ($consuntivo['rit_let'] - $request[0]['rit_let']); ?>
                </td>
                <td style="text-align: center;">
                    subtotale
                </td>
            </tr>

            <tr>
                <td style="text-align: right;">Lettore Barcode:</td>
                <td style="text-align: center;" title="Previsti">
        <?php echo $request[0]['rit_bar']; ?>
                </td>
                <td style="text-align: center;" title="Effettivi">
                    <?php echo $consuntivo['rit_bar']; ?>
                </td>
                <td style="text-align: center;" title="Bilancio">
                    <?php echo ($consuntivo['rit_bar'] - $request[0]['rit_bar']); ?>
                </td>
                <td style="text-align: center;">
                    subtotale
                </td>
            </tr>

            <tr>
                <td style="text-align: right;">Fax:</td>
                <td style="text-align: center;" title="Previsti">
        <?php echo $request[0]['rit_fax']; ?>
                </td>
                <td style="text-align: center;" title="Effettivi">
                    <?php echo $consuntivo['rit_fax']; ?>
                </td>
                <td style="text-align: center;" title="Bilancio">
                    <?php echo ($consuntivo['rit_fax'] - $request[0]['rit_fax']); ?>
                </td>
                <td style="text-align: center;">
                    subtotale
                </td>
            </tr>

            <tr>
                <td style="text-align: right;">Altro Hardware:</td>
                <td style="text-align: center;" title="Previsti">
        <?php echo $request[0]['rit_altro_hw']; ?>
                </td>
                <td style="text-align: center;" title="Effettivi">
                    <?php echo $consuntivo['rit_altro_hw']; ?>
                </td>
                <td style="text-align: center;" title="Bilancio">
                    <?php echo ($consuntivo['rit_altro_hw'] - $request[0]['rit_altro_hw']); ?>
                </td>
                <td style="text-align: center;">
                    subtotale
                </td>
            </tr>
        </table>

        <?php
        if ($consuntivo['sp_srv_new'] > 0 && $consuntivo['rit_srv'] <= 0) {
            echo "[Debug]: errore, manca il server ritirato <br />";
        }

        echo "[Debug]: totale pezzi analizzati: $n_pz <br />";
        //echo "[Debug]: valore ritiri: " . ($n_pz*1.00) . " € <br />";
    }



    switch ($OutputType) {
        case "compilazione":
            if (is_numeric($_SESSION['rollout']['id_request'])) {
                /* echo "[Debug]: esiste una request (numerica) in sessione,
                 * quindi estraggo i dati e verifico i campi <br />";
                 * echo "[Debug]: id_request: " . $_SESSION['rollout']['id_request'] . " <br />";
                 */



                //~ print_r($res);
                //~ echo "<br />";

                if (intval($res[0]['i_server']) <= 0) {
                    $res[0]['i_server'] = 1;
                }



                // Composizione stringhe di verifica:
                if ($sess_srv != $res[0]['i_server']) {
                    $RequestString .= "<label class=\"err\">Server Nuovi: $sess_srv"
                            . " / " . $res[0]['i_server'] . "</label>, ";
                } else {
                    $RequestString .= "<label class=\"ok\">Server Nuovi: $sess_srv"
                            . " / " . $res[0]['i_server'] . "</label>, ";
                }

                if ($sess_st_pdl != $res[0]['sos_pdl']) {
                    $RequestString .= "<label class=\"err\">Client Nuovi: $sess_st_pdl"
                            . " / " . $res[0]['sos_pdl'] . "</label>, ";
                } else {
                    $RequestString .= "<label class=\"ok\">Client Nuovi: $sess_st_pdl"
                            . " / " . $res[0]['sos_pdl'] . "</label>, ";
                }

                if ($sess_sos_prt_lan != $res[0]['sos_prt_lan']) {
                    $RequestString .= "<label class=\"err\">Stampanti di Rete Nuove: $sess_sos_prt_lan"
                            . " / " . $res[0]['sos_prt_lan'] . "</label>,<br />";
                } else {
                    $RequestString .= "<label class=\"ok\">Stampanti di Rete Nuove: $sess_sos_prt_lan"
                            . " / " . $res[0]['sos_prt_lan'] . "</label>,<br />";
                }

                if ($sess_rit_srv != "1") {
                    $RequestString .= "<label class=\"err\">Server Vecchi: $sess_rit_srv"
                            . " / 1</label>, ";
                } else {
                    $RequestString .= "<label class=\"ok\">Server Vecchi: $sess_rit_srv"
                            . " / 1</label>, ";
                }

                if ($sess_rit_pdl != $res[0]['rit_pdl']) {
                    $RequestString .= "<label class=\"err\">Client Vecchi: $sess_rit_pdl"
                            . " / " . $res[0]['rit_pdl'] . "</label>, <br />";
                } else {
                    $RequestString .= "&nbsp;<label class=\"ok\">Client Vecchi: $sess_rit_pdl"
                            . " / " . $res[0]['rit_pdl'] . "</label>, <br />";
                }

                if ($sess_rit_mon != $res[0]['rit_mon']) {
                    $RequestString .= "<label class=\"err\">Monitor Vecchi: $sess_rit_mon"
                            . " / " . $res[0]['rit_mon'] . "</label>, ";
                } else {
                    $RequestString .= "<label class=\"ok\">Monitor Vecchi: $sess_rit_mon"
                            . " / " . $res[0]['rit_mon'] . "</label>, ";
                }

                if ($sess_rit_let != $res[0]['rit_let']) {
                    $RequestString .= "<label class=\"err\">Lettore Assegni Vecchi: $sess_rit_let"
                            . " / " . $res[0]['rit_let'] . "</label>, <br />";
                } else {
                    $RequestString .= "&nbsp;<label class=\"ok\">Lettore Assegni Vecchi: $sess_rit_let"
                            . " / " . $res[0]['rit_let'] . "</label>, <br />";
                }

                if ($sess_rit_cas != $res[0]['rit_cas']) {
                    $RequestString .= "<label class=\"err\">Stampanti di Cassa Vecchie: $sess_rit_cas"
                            . " / " . $res[0]['rit_cas'] . "</label>, ";
                } else {
                    $RequestString .= "<label class=\"ok\">Stampanti di Cassa Vecchie: $sess_rit_cas"
                            . " / " . $res[0]['rit_cas'] . "</label>, ";
                }

                if ($sess_rit_las != $res[0]['rit_las']) {
                    $RequestString .= "<label class=\"err\">Stampanti Laser Vecchie: $sess_rit_las"
                            . " / " . $res[0]['rit_las'] . "</label>,<br />";
                } else {
                    $RequestString .= "&nbsp;<label class=\"ok\">Stampanti Laser Vecchie: $sess_rit_las"
                            . " / " . $res[0]['rit_las'] . "</label>,<br />";
                }

                // Fine sezione verifica compilazione.
            }

            break;

        case "consuntivo":
            //echo "[Debug]: caso consuntivo <br />";

            break;
    }

    return array('request' => $request[0], 'consuntivo' => $consuntivo);
}
?>

<?php
/*
 * Db.php
 *
 * Copyright 2008 Claudio Giordano <claudio.giordano@autistici.org>
 * Funzioni globali sostituite ed ottimizzate/potenziate dalla classe Database
 *
 * define("DeveloperMailAddress", "claudio.giordano@autistici.org");
 * 
 * @deprecated
 * @version (Database) 1.0
 */
define("RegDataMySql", "[1-2][0-9][0-9][0-9]-[0-1][0-9]-[0-3][0-9]");
define("RegDataIt", "[0-3][0-9]/[0-1][0-9]/[1-2][0-9][0-9][0-9]");
define("RegTime", "[0-2][0-9]:[0-6][0-9]:[0-6][0-9]");

function SaveRow($Valori, $Esclusioni, $Tab, $db, $debug = 1, $CkFields = "")
{
    $Unique = 0;
    //~ echo "[Debug]: CkFields: $CkFields <br />";

    /*
     * Compongo la query con i dati provenienti dall'array del post:
     */
    $q = "INSERT INTO $Tab ";
    $k = "(";
    $f = ") values (";

    if ($Esclusioni != "") {
        foreach ($Valori as $key => $field) {
            if (!array_key_exists($key, $Esclusioni)) {
                $k .= "$key, ";
                $f .= "'" . trim(addslashes(stripslashes($field))) . "', ";
            } else {
                if ($debug != 1) {
                    echo "Escludo la key: $key dalla Query <br />";
                }
            }
        }
    } else {
        foreach ($Valori as $key => $field) {
            $k .= "$key, ";
            $f .= "'" . trim(addslashes(stripslashes($field))) . "', ";
        }
    }

    $f .= ");";
    $q .= $k . $f;
    $q = str_replace(", )", ")", $q);

    /*
     * Check Verifica Unicità valori:
     */
    $CkWere = "";
    if ($CkFields != "") {
        $CkArray = explode(",", $CkFields);
        $CkWere .= "(";
        foreach ($CkArray as $key => $field) {
            $CkWere .= "$field = '" . trim(addslashes(stripslashes($Valori[$field]))) . "' ";
            if ($key < count($CkArray) - 1) {
                $CkWere .= "OR ";
            }
        }
        $CkWere .= ")";

        $UniqueRows = GetRows($Tab, $CkWere, "", $db, $debug);
        $Unique     = count($UniqueRows);
    }

    //~ echo "[Debug]: CkWere: $CkWere <br />";
    //~ echo "[Debug]: Unique: $Unique <br />";

    if ($debug != 1) {
        echo "[Debug][SaveRow]: $q <br />";
    }

    SessionLog("[Debug][SaveRow]: $q", $db);

    /*
     * Query completata.
     */
    if ($Unique == 0) {
        $r         = mysql_query($q, $db) or die(StampaErr($q, '[SaveRow] '));
        $Status    = array();
        //$Status[0] =  "Debug => Query SaveRow: $q <br />";
        $Status[0] = "<label class=\"ok\">Dati inseriti correttamente. ("
                . date('d/m/Y H:i:s') . ")</label>";
        // Recupero l'id dell'ultimo elemento inserito:
        $Status[1] = mysql_insert_id();
    } else {
        $Status[0] = "<label class=\"err\">Errore, uno dei campi inseriti non "
                . "risulta univoco, salvataggio annullato.</label> <br />";
        $Status[1] = "";
        /*
          $Status[0] .= "<table style=\"width: 100%;\" >";

          foreach ($UniqueRows as $UniqueKey => $UniqueField)
          {
          $Status[0] .= "<tr>";
          foreach ($UniqueField as $key => $field)
          {
          $Status[0] .= "<td>" . $field . "</td>";
          }
          $Status[0] .= "</tr>";
          }
          $Status[0] .= "</table>";
         */
    }

    $_SESSION['last_query'] = $q;
    return $Status;
}

function UpdateRow($Valori, $Esclusioni, $Tab, $Where, $db, $debug = 1)
{
    /*
     * Compongo la query con i dati provenienti dall'array del post:
     */
    $q = "UPDATE $Tab SET ";

    if ($Esclusioni != "") {
        foreach ($Valori as $key => $field) {
            if (!array_key_exists($key, $Esclusioni)) {
                $q .= "$key='" . trim(addslashes(stripcslashes($field))) . "', ";
            } else {
                if ($debug != 1) {
                    echo "Escludo la key: $key dalla Query <br />";
                }
            }
        }
    } else {
        foreach ($Valori as $key => $field) {
            $q .= "$key='" . trim(addslashes(stripcslashes($field))) . "', ";
        }
    }

    $q .= "WHERE $Where ";
    $q = str_replace(", WHERE", " WHERE", $q);

    if ($debug != 1) {
        echo "[Debug][UpdateRow]: $q <br />";
    }

    SessionLog("[Debug][UpdateRow]: $q", $db);

    /*
     * Query completata.
     */

    $r                      = mysql_query($q, $db) or die(StampaErr($q,
                                                                    '[UpdateRow] '));
    //$Status =  "Debug => Query Update: $q <br />";
    $Status                 = "<label class=\"ok\">Dati modificati correttamente. (" . date('d/m/Y H:i:s') . ")</label>";
    // $Status = $q;
    $_SESSION['last_query'] = $q;
    return $Status;
}

function GetRows($Tab, $Where, $Order, $db, $debug = 1, $Fields = "*")
{
    $q = "SELECT $Fields FROM $Tab";

    if ($Where != "") {
        $q .= " WHERE $Where";
    }

    if ($Order != "") {
        $q .= " ORDER BY $Order";
    }

    $r       = mysql_query($q, $db) or die(StampaErr($q, '[GetRows]'));
    $n       = mysql_affected_rows($db);
    $Results = array();

    while ($Row = mysql_fetch_assoc($r)) {
        array_push($Results, $Row);
    }

    mysql_free_result($r);

    if ($debug != 1) {
        echo "[Debug][GetRows]: $q <br /><br />";
    }

    $_SESSION['last_query'] = $q;
    return $Results;
}

function GetResults($Query, $db, $debug = 1)
{
    $r       = mysql_query($Query, $db) or die(StampaErr($Query, '[GetResults]'));
    $n       = mysql_affected_rows($db);
    $Results = array();

    while ($Row = mysql_fetch_assoc($r)) {
        array_push($Results, $Row);
    }

    mysql_free_result($r);

    if ($debug != 1) {
        echo "[Debug][GetResults]: $Query <br /><br />";
    }

    /*
      $_SESSION['last_query'] = $q;
      $_SESSION['last_select'] = $q;
     */
    return $Results;
}

function GetId($Tab, $Where, $db, $debug = 1)
{
    $q   = "SELECT * FROM $Tab WHERE $Where";
    $r   = mysql_query($q, $db) or die(StampaErr($q, '[GetId] '));
    $row = mysql_fetch_assoc($r);
    mysql_free_result($r);

    if ($debug != 1) {
        echo "[Debug][GetId]: $q <br />";
    }

    $_SESSION['last_query'] = $q;
    return $row;
}

function CheckInclude($Action, $Default, $Perm, $db, $debug = 1)
{
    $q   = "Select * from tab_menu where act = '$Action' AND perm like '%$Perm%'";
    $r   = mysql_query($q, $db) or die(StampaErr($q, '[CheckInclude] '));
    $n   = mysql_affected_rows($db);
    $row = mysql_fetch_assoc($r);

    if ($n == 1) {
        $Page = $row['page'];
    } else {
        $Page = $Default;
    }

    if (trim($Action) == "") {
        $Page = $Default;
    }

    if ($debug != 1) {
        echo "[Debug][CheckInclude]: $q <br />";
    }

    $_SESSION['last_query'] = $q;
    return $Page;
}

function DeleteRows($Tab, $Where, $db, $debug = 1)
{
    $q = "DELETE FROM $Tab Where $Where";
    $r = mysql_query($q, $db) or die(StampaErr($q, '[DeleteRows] '));

    if ($debug != 1) {
        echo "[Debug][DeleteRows]: $q <br />";
    }

    SessionLog("[Debug][DeleteRows]: $q", $db);

    $_SESSION['last_query'] = $q;
    return TRUE;
}

function PageSplit($Page, $TotResults, $PageLimit = 20, $debug = 1)
{
    $order = "";
    switch ($Page) {
        case "all":
            // no limit;
            break;

        case "first":
            $start = 0;
            $order .= " LIMIT $start, " . $PageLimit;
            break;

        case "last":
            $start = (intval($TotResults / $PageLimit) * $PageLimit);
            $order .= " LIMIT $start, " . $PageLimit;
            break;

        case "next":
            if (($_SESSION['start'] + $PageLimit) > $TotResults) {
                $start = (intval($TotResults / $PageLimit) * $PageLimit);
            } else {
                $start = ($_SESSION['start'] + $PageLimit);
            }
            $order .= " LIMIT $start, " . $PageLimit;
            break;

        case "prev":
            if (($_SESSION['start'] - $PageLimit) < 0) {
                $start = 0;
            } else {
                $start = ($_SESSION['start'] - $PageLimit);
            }
            $order .= " LIMIT $start, " . $PageLimit;
            break;
    }

    $_SESSION['start'] = $start;
    return $order;
}

function ShowResultBar($TotResults, $LinkPage, $PageLimit = 20)
{
    if (isset($TotResults) && ($TotResults > 0)) {
        ?>
        <tr>
            <th style="text-align: left;">
                <label>Risultati visualizzati: </label>

                <label class="ok">
                    <?php
                    if ($_SESSION['start'] == 0) {
                        echo ($_SESSION['start'] + 1);
                    } else {
                        echo $_SESSION['start'];
                    }
                    ?>
                </label>

                &nbsp;-
                <label class="ok">
                    <?php
                    if ((($_SESSION['start'] + $PageLimit) <= $TotResults) && ($_GET['page'] != "all")) {
                        echo ($_SESSION['start'] + $PageLimit);
                    } else {
                        echo $TotResults;
                    }
                    ?>
                </label>

                <label>&nbsp;su&nbsp;</label>
                <label class="ok">
                    <?php echo $TotResults; ?>
                </label>

                <label>&nbsp;corrispondenti</label>

                &nbsp;
                <!--<a onclick="Javascript: window.open('frm_pdf_call.php?tipo=<?php //echo $_POST['rad_tipo'];  ?>', '_blank'); void(0);"
                    title="Esporta in Pdf" style="cursor: pointer;">
                        <img src="/Images/Links/pdf.png" alt="Pdf" /> Esporta
                </a>

                &nbsp;!-->
                <a onclick="Javascript: posta('0', '<?php echo $LinkPage; ?>&amp;page=first'); void(0);">
                    <img src="/Images/Links/first.png" alt="first" /> Inizio
                </a>

                &nbsp;
                <a onclick="Javascript: posta('0', '<?php echo $LinkPage; ?>&amp;page=prev'); void(0);">
                    <img src="/Images/Links/prev.png" alt="prev" /> Prec
                </a>

                &nbsp;
                <a onclick="Javascript: posta('0', '<?php echo $LinkPage; ?>&amp;page=next'); void(0);">
                    <img src="/Images/Links/next.png" alt="next" /> Succ
                </a>

                &nbsp;
                <a onclick="Javascript: posta('0', '<?php echo $LinkPage; ?>&amp;page=last'); void(0);">
                    <img src="/Images/Links/last.png" alt="last" /> Fine
                </a>

                &nbsp;
                <a onclick="Javascript: posta('0', '<?php echo $LinkPage; ?>&amp;page=all'); void(0);">
                    <img src="/Images/Links/all.png" alt="all" /> Tutti
                </a>

                <!--&nbsp;
                <a onclick="Javascript: posta('0', '<?php //echo $LinkPage;   ?>&amp;page=first&amp;exp=csv'); void(0);">
                    <img src="/Images/Links/xls.png" alt="xls" />
                    Esporta csv
                </a>

                &nbsp;
                <a onclick="Javascript: posta('0', '#'); void(0);">
                    <img src="/Images/Links/xls.png" alt="xls" />
                    Grafico dalla Ricerca
                </a>!-->
            </th>

        <?php } elseif (isset($TotResults) && ($TotResults == "0")) { ?>
            <th>
                <label class="err">Nessun risultato corrispondente.</label>
            </th>
        <?php } ?>
        <th style="text-align: right;">
            <a onclick="Javascript: posta('0', '<?php echo $LinkPage; ?>&amp;page=first'); void(0);"
               style="cursor: pointer;" title="Avvia la ricerca">
                <img src="/Images/Links/find.png" alt="Avvia la ricerca" />Avvia Ricerca
            </a>

            &nbsp;
            <a onclick="Javascript: go('<?php echo $LinkPage; ?>&amp;page=first'); void(0);"
               style="cursor: pointer;" title="Nuova Ricerca">
                <img src="/Images/Links/new.png" alt="Nuova Ricerca" />Nuova Ricerca
            </a>
        </th>
    </tr><?php
}

function SessionLog($LogString, $db)
{
    $q = "INSERT INTO tab_session (session_id, id_user, data, ip, action, user_agent) values ("
            . "'" . session_id() . "', '" . $_SESSION['id_user'] . "', '" . date('Y-m-d H:i:s')
            . "', '" . $_SERVER['REMOTE_ADDR'] . "', '" . addslashes(stripslashes($LogString))
            . "', '" . $_SERVER['HTTP_USER_AGENT'] . "')";
    $r = mysql_query($q, $db) or die(StampaErr($q, '[SaveRow] '));

    $_SESSION['last_query'] = $q;
    return TRUE;
}

function ShowEditBar($DescString, $Mod, $Links, $Action, $Span = 8,
                     $CountDett = 0, $PopupLink = "#", $ArbLinks = "")
{
    $ConfSalva = "Salvare i dati inseriti?";

    echo "<tr>"
    . "<th colspan=\"" . $Span . "\">"
    . $DescString . "&nbsp;";

    if (trim($Mod) != "") {
        if (is_numeric($Mod)) {
            echo "<label class=\"err_status\">(In Modifica)&nbsp;</label>";
        } else {
            echo "<label class=\"err_status\">(In Modifica Multipla)&nbsp;</label>";
        }
        $UrlSalva  = "?act=$Action&amp;tomod=" . $Mod . "&amp;salva=modifica";
        $UrlAction = "?act=$Action&amp;tomod=" . $Mod;
    } else {
        echo "<label class=\"err_status\">(Nuovo Inserimento)&nbsp;</label>";
        $UrlSalva  = "?act=$Action&amp;salva=salva";
        $UrlAction = "?act=$Action";
    }

    $ArraiLinks = explode(",", $Links);
    foreach ($ArraiLinks as $key => $link) {
        switch ($link) {
            case "new":
                echo "&nbsp;<a onclick=\"Javascript: "
                . "go_conf2('I dati attualmente inseriti \\nverranno persi, continuare?', "
                . "'home.php?act=" . $Action . "&amp;reset=0'); void(0);\">"
                . "<img src=\"/Images/Links/new.png\" alt=\"new\" title=\"Nuovo\" />"
                . "Nuovo"
                . "</a>&nbsp;";
                break;

            case "save":
                echo "&nbsp;<a onclick=\"Javascript: conferma_salva('0', '"
                . $ConfSalva . "', '" . $UrlSalva . "');\" >"
                . "<img src=\"/Images/Links/save.png\" alt=\"save\" title=\"Salva\" />"
                . "Salva"
                . "</a>&nbsp;";
                break;

            case "add":
                echo "&nbsp;<a onclick=\"Javascript: popUpWindow('" . $Action . "', '$PopupLink";
                if (trim($Mod) != "") {
                    echo "?tomod=$Mod";
                }

                echo "', '200px', '200px', '750px', '300px');\">"
                . "<img src=\"/Images/Links/add.png\" alt=\"add\" title=\"Aggiungi\" />"
                . "Aggiungi";
                echo "</a>&nbsp;";
                break;

            case "clear":
                echo "&nbsp;<a onclick=\"Javascript: go_conf2('Rimuovere tutte le voci dal dettaglio?', "
                . "'home.php?act=" . $Action . "&amp;svuota=0'); void(0);\">"
                . "<img src=\"/Images/Links/clear.png\" alt=\"clear\" />"
                . "Voci ( <label class=\"err\" title=\"Totale voci dettaglio\">"
                . $CountDett . "</label> )"
                . "</a>&nbsp;";
                break;

            case "del":
                break;

            case "blank":
                echo "&nbsp;<a onclick=\"Javascript: go('" . $UrlAction . "&amp;blank=0');\" >"
                . "<img src=\"/Images/Links/new.png\" alt=\"new\" title=\"Nuovo\" />"
                . "Nuovo"
                . "</a>&nbsp;";
                break;
        }
    }

    if ($ArbLinks != "") {
        foreach ($ArbLinks as $key => $field) {
            echo $field['link'];
        }
    }


    echo "</th>";
    echo "</tr>";
    //echo "<tr><th colspan=\"4\"><hr /></th></tr>";
}

function ShowWaitDiv()
{
    echo "<div id=\"wait\">"
    . "<img src=\"/Images/Varie/wait.gif\" alt=\"wait\" /><br />"
    . "<label>Aggiornamento pagina in corso, attendere...</label>"
    . "</div>";
}

function ShowCreditsBar()
{
    echo "<div id=\"footer\">";
    echo "<table style=\"width: 100%;\">
				<tr>
					<td style=\"vertical-align: middle;\">
					<a onclick=\"Javascript: window.open('http://gnu.it', '_blank'); void(0);\">
						<img src=\"/Images/Software/gnubanner-2.png\" alt=\"GNU Project\" title=\"Progetto GNU\" />
					</a>
					<a onclick=\"Javascript: window.open('http://it.wikipedia.org/wiki/Linux', '_blank'); void(0);\">
						<img src=\"/Images/Software/linux.gif\" alt=\"Linux\" title=\"GNU/Linux\" />
					</a>
					<a onclick=\"Javascript: window.open('http://www.debian.org', '_blank'); void(0);\">
						<img src=\"/Images/Software/debian.png\" alt=\"debian.org\"
							title=\"Debian - Debian è un sistema operativo (OS) libero (free) per il tuo computer.\" />
					</a>
					<a onclick=\"Javascript: window.open('http://www.apache.org', '_blank'); void(0);\">
						<img src=\"/Images/Software/apache.png\" alt=\"apache.org\" title=\"Apache\" />
					</a>
					<a onclick=\"Javascript: window.open('http://www-it.mysql.com/', '_blank'); void(0);\">
						<img src=\"/Images/Software/mysql.png\" alt=\"mysql.com\" title=\"MySql\" />
					</a>
					<a onclick=\"Javascript: window.open('http://it.php.net/', '_blank'); void(0);\">
						<img src=\"/Images/Software/php.png\" alt=\"php.net\" title=\"Php\" />
					</a>
					<a onclick=\"Javascript: window.open('http://www.mozillaitalia.it/archive/index.html#p1', '_blank'); void(0);\">
						<img src=\"/Images/Software/firefox3.gif\" alt=\"firefox.gif\" title=\"Firefox\" />
					</a>
					<a onclick=\"Javascript: window.open('http://validator.w3.org/check?uri=referer', '_blank'); void(0);\">
						<img src=\"/Images/Validator/valid-xhtml11.png\"
							alt=\"Valid XHTML 1.0 Strict\"  />
					</a>
					<a onclick=\"Javascript: window.open('http://jigsaw.w3.org/css-validator/', '_blank'); void(0);\">
						<img src=\"/Images/Validator/vcss.gif\" alt=\"CSS Valido!\" />
					</a>
				</td>
			</tr>
		</table>";
    echo "</div>";
}

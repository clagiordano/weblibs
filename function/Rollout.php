<?php
define("Incident", "(I00\d+)");

/*
  define("pIncident", 12.00);
  define("Server", 20.00);
  define("Spedizioni", 0.80);
  define("Staging", 3.00);
  define("Sostituzioni", 12.00);
  define("Riconfigurazioni", 12.00);
  define("Disinstallazioni", 12.00);
  define("Ritiri", 1.00);
 */
if (isset($_POST['pincident'])) {
    define("pIncident", $_POST['pincident']);
    define("Server", $_POST['server']);
    define("Spedizioni", $_POST['spedizioni']);
    define("Staging", $_POST['staging']);
    define("Sostituzioni", $_POST['sostituzioni']);
    define("Riconfigurazioni", $_POST['riconfigurazioni']);
    define("Disinstallazioni", $_POST['disinstallazioni']);
    define("Ritiri", $_POST['ritiri']);
}

$errori = "";

function CheckDettaglioRequest($IdFiliale)
{
    global $d;
    // Azzero l'array del consuntivo
    $consuntivo = array();
    $errori     = "";

    // Inizializzo le variabili per le spedizioni:
    $consuntivo['sp_srv']      = 0;
    $consuntivo['sp_srv_new']  = 0;
    $consuntivo['sp_pdl']      = 0;
    $consuntivo['sp_mon']      = 0;
    $consuntivo['sp_las']      = 0;
    $consuntivo['sp_cas']      = 0;
    $consuntivo['sp_bar']      = 0;
    $consuntivo['sp_lett']     = 0;
    $consuntivo['sp_altro_hw'] = 0;
    $consuntivo['sp_prt_lan']  = 0;

    // Inizializzo le variabili per lo staging:
    $consuntivo['st_srv'] = 0;
    $consuntivo['st_pdl'] = 0;

    // Inizializzo le variabili per le sostituzioni:
    $consuntivo['sos_srv']     = 0;
    $consuntivo['sos_pdl']     = 0;
    $consuntivo['sos_mon']     = 0;
    $consuntivo['sos_cas']     = 0;
    $consuntivo['sos_las']     = 0;
    $consuntivo['sos_bar']     = 0;
    $consuntivo['sos_let']     = 0;
    $consuntivo['sos_fax']     = 0;
    $consuntivo['sos_lett']    = 0;
    $consuntivo['sos_prt_lan'] = 0;

    // Inizializzo le variabili per le dismissioni:
    $consuntivo['dis_srv']      = 0;
    $consuntivo['dis_pdl']      = 0;
    $consuntivo['dis_mon']      = 0;
    $consuntivo['dis_las']      = 0;
    $consuntivo['dis_prt_lan']  = 0;
    $consuntivo['dis_cas']      = 0;
    $consuntivo['dis_let']      = 0;
    $consuntivo['dis_bar']      = 0;
    $consuntivo['dis_fax']      = 0;
    $consuntivo['dis_altro_hw'] = 0;

    // Inizializzo le variabili per i ritiri:
    $consuntivo['rit_srv']      = 0;
    $consuntivo['rit_pdl']      = 0;
    $consuntivo['rit_mon']      = 0;
    $consuntivo['rit_las']      = 0;
    $consuntivo['rit_prt_lan']  = 0;
    $consuntivo['rit_cas']      = 0;
    $consuntivo['rit_let']      = 0;
    $consuntivo['rit_bar']      = 0;
    $consuntivo['rit_fax']      = 0;
    $consuntivo['rit_altro_hw'] = 0;

    // Inizializzo le variabili per gli incident:
    $consuntivo['inc_srv'] = 0;

    // Inizializzo le variabili per i totali:
    $consuntivo['tot_sped']    = 0;
    $consuntivo['tot_stag']    = 0;
    $consuntivo['tot_sost']    = 0;
    $consuntivo['tot_ric']     = 0;
    $consuntivo['tot_dis']     = 0;
    $consuntivo['tot_rit']     = 0;
    $consuntivo['tot_inc']     = 0;
    $consuntivo['tot_fil']     = 0;
    $consuntivo['tot_rit_let'] = 0;
    $consuntivo['errori']      = "";


    // Estraggo il contenuto della request richiesta
    $request = $d->GetRows("*", "view_requests",
                           "id_filiale = '"
            . $IdFiliale . "'", "", "", 1);

    if (isset($request[0]['id_request']) && is_numeric($request[0]['id_request'])) {
        // Estraggo il dettaglio della request richiesta
        $dettaglio = $d->GetRows("*", "view_macchine_assegnate",
                                 "id_request = '"
                . $request[0]['id_request'] . "'", "", "tipo_mch", 1);

        /*
         *  Verifica del tipo di server richiesto e consegnato:
         */
        if (preg_match(Incident, $request[0]['i_server'])) {
            // E' presente un incident per il server portato in filiale:
            $srv_richiesto = "Riciclato";
            //$consuntivo['server'] = "Riciclato";
        } else {
            // il server è nuovo:
            $srv_richiesto = "Nuovo";
            //$consuntivo['server'] = "Nuovo";
        }

        /*
         *  Verifica il dettaglio della request:
         */
        //echo "[Debug]: totale pezzi: " . count($dettaglio) . " <br />";
        if (count($dettaglio) > 0) {
            //echo "[Debug]: data_pianif: ".$request[0]['data_pianif']." <br />";
            //echo "[Debug]: totale pezzi: " . count($dettaglio) . " <br />";
            $RequestString = "";
            $n_pz          = 0;

            foreach ($dettaglio as $key => $field) {
                /*
                 * Verifica componenti nuovi presenti in dettaglio
                 */
                if (trim($field['provenienza']) == "FORNITORE") {
                    switch ($field['tipo_mch']) {
                        case "Server":
                            // il server è nuovo:
                            //echo "[Debug]: Rilevato server nuovo. <br />";
                            $consuntivo['sp_srv_new'] += 1;  // spedizione server nuovo
                            $consuntivo['st_srv'] += 1;    // staging
                            $consuntivo['sos_srv'] += 1;    // sostituzione
                            //$consuntivo['dis_srv'] += 1; 		// disinstallazione
                            //$consuntivo['rit_srv'] += 1; 			// ritiro server vecchio
                            $n_pz += 1;
                            break;

                        case "Desktop":
                            //echo "[Debug]: Pdl nuovo in sostituzione <br />";
                            //$sess_st_pdl += 1;
                            $consuntivo['sp_pdl'] += 1;  // spedizione
                            $consuntivo['st_pdl'] += 1;  // staging
                            $consuntivo['sos_pdl'] += 1;  // sostituzione
                            $n_pz += 1;
                            break;

                        case "Monitor":
                            //echo "[Debug]: Pdl nuovo in sostituzione <br />";
                            //$sess_st_pdl += 1;
                            $consuntivo['sp_mon'] += 1;  // spedizione
                            $consuntivo['sos_mon'] += 1;  // sostituzione
                            $n_pz += 1;
                            break;

                        case "Stampante laser":
                            $consuntivo['sp_las'] += 1;
                            $consuntivo['sos_las'] += 1;
                            $n_pz += 1;
                            break;

                        case "Stampante di rete":
                            $consuntivo['sp_prt_lan'] += 1;
                            $consuntivo['sos_prt_lan'] += 1;
                            $n_pz += 1;
                            break;

                        case "Lettore assegni":
                            //echo "[Debug]: lettore assegni nuovo in sostituzione <br />";
                            $consuntivo['sp_lett'] += 1;
                            $consuntivo['sos_lett'] += 1;
                            $n_pz += 1;
                            break;

                        case "Lettore barcode":
                            //echo "[Debug]: lettore barcode nuovo in sostituzione <br />";
                            $consuntivo['sp_lett'] += 1;
                            $consuntivo['sos_lett'] += 1;
                            $n_pz += 1;
                            break;

                        default:
                            /* echo "[Debug]: altro hw nuovo con provenienza
                              fornitore (sp_altro_hw): " . $field['tipo_mch'] . "<br />"; */
                            $errori .= "altro hw nuovo con provenienza
									fornitore (sp_altro_hw): " . $field['tipo_mch'] . "<br />";
                            $consuntivo['sp_altro_hw'] += 1;
                            //$consuntivo['sp_altro_hw'] += 1;
                            $n_pz += 1;
                            break;
                    }
                }

                /*
                 * Verifica componenti vecchi in dettaglio
                 */
                if (trim($field['provenienza']) == "DA FILIALE") {
                    switch ($field['tipo_mch']) {
                        case "Server":
                            //echo "[Debug]: Rilevato server riciclato. <br />";
                            //echo "[Debug]: il server è riciclato, verifico su
                            //quante request corrisponde <br />";

                            /* $ck_srv = GetRows ("tab_dett_requests",
                              "id_macchina = '" . $field['id_macchina'] . "'"
                              . " AND id_request != '"
                              . $_SESSION['rollout']['id_request'] . "'", "", $db, 1); */

                            /*
                             * se la data di pianificazione è diversa dalla data_in del server
                             * allora il server sta tornando in filiale come incident:
                             */
                            if ($request[0]['data_pianif'] != $field['data_in']) {
                                //echo "[Debug]: Si tratta di server per la sostituzione come incident <br />";
                                $consuntivo['inc_srv'] += 1;
                            } else {
                                //echo "[Debug]: Si tratta di server ritirato <br />";
                                $consuntivo['rit_srv'] += 1;
                            }

                            /* if (count($ck_srv) > 0)
                              {
                              //echo "[Debug]: il server sta tornando in filiale come nuovo <br />";
                              //$consuntivo['qi_server'] += 1;
                              //$n_pz += 1;
                              }else{
                              //echo "[Debug]: altrimenti il server è vecchio <br />";
                              if ($consuntivo['sp_srv_new'] > 0)
                              {
                              /* se la request prevede la sostituzione del
                             * server vecchio con uno nuovo
                             * allora bisogna considerare il ritiro del vecchio server */
                            /* $consuntivo['rit_srv'] += 1;
                              }

                              $n_pz += 1;
                              } */


                            $n_pz += 1;
                            break;

                        case "Desktop":
                            //echo "[Debug]: ritito pdl old <br />";
                            $consuntivo['rit_pdl'] += 1;
                            $consuntivo['dis_pdl'] = ($consuntivo['rit_pdl'] - $consuntivo['sos_pdl']);
                            $n_pz += 1;
                            break;

                        case "Monitor Lcd":
                        case "Monitor Crt":
                            //echo "[Debug]: ritito monitor <br />";
                            $consuntivo['rit_mon'] += 1;
                            $n_pz += 1;
                            break;

                        case "Stampante laser":
                            //echo "[Debug]: ritiro stampante laser <br />";
                            $consuntivo['rit_las'] += 1;
                            //$consuntivo['dis_las'] = ""; //($consuntivo['rit_las']-$request[0]['dis_las']);
                            $n_pz += 1;
                            break;

                        case "Stampante di rete":
                            $consuntivo['rit_prt_lan'] += 1;
                            //$consuntivo['sos_prt_lan'] += 1;
                            $n_pz += 1;
                            break;

                        case "Lettore assegni":
                            //echo "[Debug]: ritiro lettore assegni <br />";
                            $consuntivo['rit_let'] += 1;
                            $consuntivo['tot_rit_let'] += 1;
                            $n_pz += 1;
                            break;

                        case "Lettore barcode":
                            //echo "[Debug]: ritiro lettore barcode <br />";
                            $consuntivo['rit_bar'] += 1;
                            //$consuntivo['rit_bar'] += 1;
                            $n_pz += 1;
                            break;

                        case "Stampante di cassa":
                            //echo "[Debug]: ritiro stampante di cassa <br />";
                            $consuntivo['rit_cas'] += 1;
                            $n_pz += 1;
                            break;

                        case "Fax":
                            //echo "[Debug]: ritiro fax <br />";
                            $consuntivo['rit_fax'] += 1;
                            $n_pz += 1;
                            break;

                        default:
                            /* echo "[Debug]: altro hw con provenienza
                              da filiale (dis+rit_altro_hw): " . $field['tipo_mch'] . "<br />"; */
                            $errori .= "altro hw con provenienza
									da filiale (dis+rit_altro_hw): " . $field['tipo_mch'] . "<br />";
                            $consuntivo['dis_altro_hw'] += 1;
                            $consuntivo['rit_altro_hw'] += 1;
                            //$consuntivo['sp_altro_hw'] += 1;
                            $n_pz += 1;
                            break;
                    }
                }

                /*
                 * Verifica dei componenti provenienti da maintenance:
                 */
                if (trim($field['provenienza']) == "MAINTENANCE") {
                    switch ($field['tipo_mch']) {
                        case "Server":
                            //echo "[Debug]: il server proviene dal polmone
                            // spedizione server da maintenance
                            //echo "[Debug]: Rilevato server da maintenance (polmone). <br />";
                            $consuntivo['inc_srv'] += 1;
                            $n_pz += 1;
                            break;
                    }
                }

                /*
                 * Verifica dei componenti trovati in filiale:
                 */
                if (trim($field['provenienza']) == "IN LOCO") {
                    switch ($field['tipo_mch']) {
                        case "Desktop":
                            //echo "[Debug]: Pdl nuovo trovato in filiale <br />";
                            $consuntivo['st_pdl'] += 1;  // staging
                            $consuntivo['sos_pdl'] += 1;  // sostituzione
                            $n_pz += 1;
                            break;

                        case "Stampante di rete":
                            $consuntivo['sos_prt_lan'] += 1; // sostituzione
                            $n_pz += 1;
                            break;
                    }
                }
            }

            /*
             * rimozione dal conteggio ritiri e spedizioni dei
             * componenti soggetti ad incident:
             */
            if ($consuntivo['sp_srv'] < $request[0]['qi_server']) {
                $errori .= "Errore, il numero degli incident sul server non
						non coincide con i quelli ritirati. <br />";
                $consuntivo['sp_srv'] = 0;
            } else {
                $consuntivo['sp_srv']  = ($consuntivo['sp_srv'] - $request[0]['qi_server']);
                $consuntivo['rit_srv'] = ($consuntivo['rit_srv'] - $request[0]['qi_server']);
            }

            if ($consuntivo['rit_mon'] < $request[0]['qi_mon']) {
                $errori .= "Errore, il numero degli incident sui
						monitor non coincide con i quelli ritirati. <br />";
                $consuntivo['rit_mon'] = 0;
            } else {
                $consuntivo['rit_mon'] = ($consuntivo['rit_mon'] - $request[0]['qi_mon']);
            }
            $consuntivo['sp_mon']  = ($consuntivo['sp_mon'] - $request[0]['qi_mon']);
            $consuntivo['inc_mon'] = $request[0]['qi_mon'];

            if ($consuntivo['rit_let'] < $request[0]['qi_lett']) {
                $errori .= "Errore, il numero degli incident sui
						lettori assegni non coincide con i quelli ritirati. <br />";
                $consuntivo['rit_let'] = 0;
            } else {
                $consuntivo['rit_let'] = ($consuntivo['rit_let'] - $request[0]['qi_lett']);
            }
            $consuntivo['sp_let']   = ($consuntivo['sp_lett'] - $request[0]['qi_lett']);
            $consuntivo['inc_lett'] = $request[0]['qi_lett'];

            if ($consuntivo['rit_cas'] < $request[0]['qi_prt']) {
                $errori .= "Errore, il numero degli incident sulle
						stampanti di cassa non coincide con i quelle ritirate. <br />";
                $consuntivo['rit_cas'] = 0;
            } else {
                $consuntivo['rit_cas'] = ($consuntivo['rit_cas'] - $request[0]['qi_prt']);
            }
            $consuntivo['sp_cas']  = ($consuntivo['sp_cas'] - $request[0]['qi_prt']);
            $consuntivo['inc_prt'] = $request[0]['qi_prt'];

            //echo "[Debug]: dis_srv: " . $consuntivo['dis_srv'] . " <br />";

            $consuntivo['dis_pdl'] = ($consuntivo['rit_pdl'] - $consuntivo['sos_pdl']);
            //echo "[Debug]: dis_pdl: " . $consuntivo['dis_pdl'] . " <br />";

            $consuntivo['dis_mon'] = ($consuntivo['rit_mon'] - $consuntivo['sos_mon']);
            //echo "[Debug]: dis_mon: " . $consuntivo['dis_mon'] . " <br />";

            $consuntivo['dis_las'] = ($consuntivo['rit_las'] - $consuntivo['sos_las']);
            //echo "[Debug]: dis_las: " . $consuntivo['dis_las'] . " <br />";

            $consuntivo['dis_cas'] = ($consuntivo['rit_cas'] - $consuntivo['sos_cas']);
            //echo "[Debug]: dis_cas: " . $consuntivo['dis_cas'] . " <br />";

            $consuntivo['dis_let'] = ($consuntivo['rit_let'] - $consuntivo['sos_let']);
            //echo "[Debug]: dis_let: " . $consuntivo['dis_let'] . " <br />";

            $consuntivo['dis_bar'] = ($consuntivo['rit_bar'] - $consuntivo['sos_bar']);
            //echo "[Debug]: dis_bar: " . $consuntivo['dis_bar'] . " <br />";

            $consuntivo['dis_fax'] = ($consuntivo['rit_fax'] - $consuntivo['sos_fax']);
            //echo "[Debug]: dis_fax: " . $consuntivo['dis_fax'] . " <br />";

            $consuntivo['dis_altro_hw'] = ($consuntivo['dis_altro_hw']); //-$consuntivo['sos_altro_hw']);
            //echo "[Debug]: dis_dis_altro_hw: " . $consuntivo['dis_altro_hw'] . " <br />";

            if ($consuntivo['sp_srv_new'] > 0 && $consuntivo['rit_srv'] <= 0) {
                //echo "[Debug]: errore, manca il server ritirato <br />";
                $errori .= "Errore, manca il server ritirato <br />";
            }

            //echo "[Debug]: totale pezzi analizzati: $n_pz <br />";
        }

        $consuntivo['errori'] = $errori;
    }

    if (!isset($request[0])) {
        $request[0] = "";
    }


    //echo "<br />[Debug]: Errori => " . $consuntivo['errori'] . " <br />";
    return array('request' => $request[0], 'consuntivo' => $consuntivo);
    //return $consuntivo;
}

function PrintCheckRequest($IdRequest, $OutputType = "compilazione")
{
    global $d;
    //echo "[Debug]: IdRequest: $IdRequest <br />";
    // Verifico se l'id passato è un array
    if (is_array($IdRequest)) {
        /* echo "[Debug]: l'id è un array, eseguo il conteggio
          massivo del dettaglio: <br />"; */

        // Flag per l'argomento passato
        $IsArray       = True;
        $ElencoFiliali = "<br />";
        $indice_fil    = 1;

        foreach ($IdRequest as $key_req => $field_req) {
            $errori         = "";
            $check          = CheckDettaglioRequest($field_req);
            $tmp_consuntivo = $check['consuntivo'];
            if (!isset($consuntivo)) {
                $consuntivo = $tmp_consuntivo;
                $errori     = $check['consuntivo']['errori'];
                $request[0] = $check['request'];
                $ElencoFiliali .= $indice_fil . ") - "
                        . $d->Inverti_Data($request[0]['data_pianif']) . " - "
                        . $request[0]['den'] . "<br />";
                //. "<br />Errori: <br />$errori <br />";
            } else {
                $errori .= $check['consuntivo']['errori'];
                $tmp_request = $check['request'];

                foreach ($tmp_consuntivo as $key_c => $field_c) {
                    $consuntivo[$key_c] += $tmp_consuntivo[$key_c];
                }

                foreach ($request[0] as $key_r => $field_r) {
                    $request[0][$key_r] += $tmp_request[$key_r];
                }
                $ElencoFiliali .= $indice_fil . ") - "
                        . $d->Inverti_Data($tmp_request['data_pianif']) . " - "
                        . $tmp_request['den'] . "<br />";
                //. "<br />Errori: <br />$errori <br />";
            }
            $indice_fil += 1;
        }
    } else {
        /*
          echo "[Debug]: l'id è un numero ($IdRequest), eseguo il conteggio
          singolo del dettaglio: <br />";
         */
        $check      = CheckDettaglioRequest($IdRequest);
        $consuntivo = $check['consuntivo'];
        $errori     = $check['consuntivo']['errori'];
        $request[0] = $check['request'];
    }

    /*
      echo "<pre>check['request']";
      print_r($check['request']);
      echo "</pre>";
     */

    switch ($OutputType) {
        case "compilazione":
            /* echo "[Debug]: esiste una request (numerica) in sessione,
             * quindi estraggo i dati e verifico i campi <br />";
             * echo "[Debug]: id_request: " . $_SESSION['rollout']['id_request'] . " <br />";
             */

            $RequestString = "";
            // Composizione stringhe di verifica:
            if ($consuntivo['inc_srv'] != $request[0]['qi_server']) {
                $style = "class=\"err\"";
            } else {
                $style = "class=\"ok\"";
            }
            $RequestString .= "<label $style>Incident Server: "
                    . $consuntivo['inc_srv'] . " / " . $request[0]['qi_server'] . "</label>, ";

            if ($consuntivo['sp_srv_new'] != $request[0]['sp_srv_new']) {
                $style = "class=\"err\"";
            } else {
                $style = "class=\"ok\"";
            }
            $RequestString .= "<label $style>Server Nuovi: "
                    . $consuntivo['sp_srv_new'] . " / " . $request[0]['sp_srv_new'] . "</label>, ";

            if ($consuntivo['rit_srv'] != $request[0]['rit_srv']) {
                $style = "class=\"err\"";
            } else {
                $style = "class=\"ok\"";
            }
            $RequestString .= "<label $style>Server Vecchi: "
                    . $consuntivo['rit_srv'] . " / " . $request[0]['rit_srv'] . "</label>, ";

            if ($consuntivo['sos_pdl'] != $request[0]['sos_pdl']) {
                $style = "class=\"err\"";
            } else {
                $style = "class=\"ok\"";
            }
            $RequestString .= "<label $style>Client Nuovi: "
                    . $consuntivo['sos_pdl'] . " / " . $request[0]['sos_pdl'] . "</label>, ";


            if ($consuntivo['rit_pdl'] != $request[0]['rit_pdl']) {
                $style = "class=\"err\"";
            } else {
                $style = "class=\"ok\"";
            }
            $RequestString .= "<label $style>Client Vecchi: "
                    . $consuntivo['rit_pdl'] . " / " . $request[0]['rit_pdl'] . "</label>, <br />";


            if ($consuntivo['sos_prt_lan'] != $request[0]['sos_prt_lan']) {
                $style = "class=\"err\"";
            } else {
                $style = "class=\"ok\"";
            }
            $RequestString .= "<label $style>Stampanti di rete nuove: "
                    . $consuntivo['sos_prt_lan'] . " / " . $request[0]['sos_prt_lan'] . "</label>, ";

            if ($consuntivo['rit_prt_lan'] != $request[0]['rit_prt_lan']) {
                $style = "class=\"err\"";
            } else {
                $style = "class=\"ok\"";
            }
            $RequestString .= "<label $style>Stampanti di rete Vecchie: "
                    . $consuntivo['rit_prt_lan'] . " / " . $request[0]['rit_prt_lan'] . "</label>, ";

            if ($consuntivo['rit_mon'] != $request[0]['rit_mon']) {
                $style = "class=\"err\"";
            } else {
                $style = "class=\"ok\"";
            }
            $RequestString .= "<label $style>Monitor Vecchi: "
                    . $consuntivo['rit_mon'] . " / " . $request[0]['rit_mon'] . "</label>, <br />";

            if ($consuntivo['rit_let'] != $request[0]['rit_let']) {
                $style = "class=\"err\"";
            } else {
                $style = "class=\"ok\"";
            }
            $RequestString .= "<label $style>Lettore Assegni Vecchi: "
                    . $consuntivo['tot_rit_let'] . " / " . $request[0]['rit_let'] . "</label>, ";


            if ($consuntivo['rit_cas'] != $request[0]['rit_cas']) {
                $style = "class=\"err\"";
            } else {
                $style = "class=\"ok\"";
            }
            $RequestString .= "<label $style>Stampanti di Cassa Vecchie: "
                    . $consuntivo['rit_cas'] . " / " . $request[0]['rit_cas'] . "</label>, ";


            if ($consuntivo['sp_lett'] != $request[0]['sp_lett']) {
                $style = "class=\"err\"";
            } else {
                $style = "class=\"ok\"";
            }
            $RequestString .= "<label $style>Lettori assegni nuovi: "
                    . $consuntivo['sp_lett'] . " / " . $request[0]['sp_lett'] . "</label>, <br />";

            if ($consuntivo['rit_las'] != $request[0]['rit_las']) {
                $style = "class=\"err\"";
            } else {
                $style = "class=\"ok\"";
            }
            $RequestString .= "<label $style>Stampanti Laser Vecchie: "
                    . $consuntivo['rit_las'] . " / " . $request[0]['rit_las'] . "</label>, ";

            // Fine sezione verifica compilazione.
            echo $RequestString;

            break;

        case "consuntivo":
            ?>
            <table class="dettaglio" style="width: 100%;" rules="all">
            <?php if (!$IsArray) { ?>
                    <tr>
                        <th colspan="5">
                            Consuntivo Filiale: <?php echo $request[0]['den']; ?>

                            &nbsp;
                            <a onclick="Javascript: go('home.php?act=rollout&amp;reset=0&amp;tomod=<?php echo $request[0]['id_request']; ?>'); void(0);"
                               title="Compila Dettaglio Request">
                                <img src="/Images/Links/arch.png" alt="arch" />
                            </a>

                            &nbsp;
                            <a onclick="Javascript: window.open('frm_pdf_dett_request.php?rid=<?php echo $request[0]['id_request']; ?>', '_blank'); void(0);"
                               title="Stampa Riepilogo Request">
                                <img src="/Images/Links/pdf.png" alt="pdf" />
                            </a>
                        </th>
                    </tr>

            <?php } else { ?>
                    <tr>
                        <th colspan="5">
                            Consuntivo cumulativo in base ai criteri di ricerca
                        </th>
                        <th>
                            Elenco Filiali interessate dal conteggio:
                        </th>
                    </tr>
            <?php } ?>

                <tr><th colspan="5"><br /></th></tr>

                <tr>
                    <th colspan="5">
                        Spedizioni:
                        <label><?php printf("( %.2f € pezzo )", Spedizioni); ?></label>
                    </th>
                    <th rowspan="100" style="vertical-align: top;">
            <?php echo $ElencoFiliali; ?>
                    </th>
                </tr>

                <tr>
                    <th style="text-align: center;"><label>Tipo Macchina:</label></th>
                    <th style="text-align: center;"><label>Previsti:</label></th>
                    <th style="text-align: center;"><label>Effettivi:</label></th>
                    <th style="text-align: center;"><label>Bilancio:</label></th>
                    <th style="text-align: center;"><label>Subtotale:</label></th>
                </tr>

                <tr>
                    <td style="text-align: right;">Server Nuovo:</td>
                    <td style="text-align: center;" title="Previsti">
                        <label class="err"><?php echo $request[0]['sp_srv_new']; ?></label>
                    </td>
                    <td style="text-align: center;" title="Effettivi">
                        <label class="ok"><?php echo $consuntivo['sp_srv_new']; ?></label>
                    </td>
                    <td style="text-align: center;" title="Bilancio">
            <?php echo ($consuntivo['sp_srv_new'] - $request[0]['sp_srv_new']); ?>
                    </td>
                    <td style="text-align: center;">
                        <label class="ok">
            <?php
            if (($consuntivo['sp_srv_new'] * Spedizioni) == 0) {
                echo "-";
            } else {
                $consuntivo['tot_sped'] += ($consuntivo['sp_srv_new'] * Spedizioni);
                printf("%.2f €", ($consuntivo['sp_srv_new'] * Spedizioni));
            }
            ?>
                        </label>
                    </td>
                </tr>

                <tr>
                    <td style="text-align: right;">Pdl Nuovi:</td>
                    <td style="text-align: center;" title="Previsti">
                        <label class="err"><?php echo $request[0]['sp_pdl']; ?></label>
                    </td>
                    <td style="text-align: center;" title="Effettivi">
                        <label class="ok"><?php echo $consuntivo['sp_pdl']; ?></label>
                    </td>
                    <td style="text-align: center;" title="Bilancio">
            <?php echo ($consuntivo['sp_pdl'] - $request[0]['sp_pdl']); ?>
                    </td>
                    <td style="text-align: center;">
                        <label class="ok">
            <?php
            if (($consuntivo['sp_pdl'] * Spedizioni) == 0) {
                echo "-";
            } else {
                $consuntivo['tot_sped'] += ($consuntivo['sp_pdl'] * Spedizioni);
                printf("%.2f €", ($consuntivo['sp_pdl'] * Spedizioni));
            }
            ?>
                        </label>
                    </td>
                </tr>

                <tr>
                    <td style="text-align: right;">Monitor:</td>
                    <td style="text-align: center;" title="Previsti">
                        <label class="err"><?php echo $request[0]['sp_mon']; ?></label>
                    </td>
                    <td style="text-align: center;" title="Effettivi">
                        <label class="ok"><?php echo $consuntivo['sp_mon']; ?></label>
                    </td>
                    <td style="text-align: center;" title="Bilancio">
                            <?php echo ($consuntivo['sp_mon'] - $request[0]['sp_mon']); ?>
                    </td>
                    <td style="text-align: center;">
                        <label class="ok">
            <?php
            if (($consuntivo['sp_mon'] * Spedizioni) == 0) {
                echo "-";
            } else {
                $consuntivo['tot_sped'] += ($consuntivo['sp_mon'] * Spedizioni);
                printf("%.2f €", ($consuntivo['sp_mon'] * Spedizioni));
            }
            ?>
                        </label>
                    </td>
                </tr>

                <tr>
                    <td style="text-align: right;">Stampante Laser:</td>
                    <td style="text-align: center;" title="Previsti">
                        <label class="err"><?php echo $request[0]['sp_las']; ?></label>
                    </td>
                    <td style="text-align: center;" title="Effettivi">
                        <label class="ok"><?php echo $consuntivo['sp_las']; ?></label>
                    </td>
                    <td style="text-align: center;" title="Bilancio">
                            <?php echo ($consuntivo['sp_las'] - $request[0]['sp_las']); ?>
                    </td>
                    <td style="text-align: center;">
                        <label class="ok">
            <?php
            if (($consuntivo['sp_las'] * Spedizioni) == 0) {
                echo "-";
            } else {
                $consuntivo['tot_sped'] += ($consuntivo['sp_las'] * Spedizioni);
                printf("%.2f €", ($consuntivo['sp_las'] * Spedizioni));
            }
            ?>
                        </label>
                    </td>
                </tr>

                <tr>
                    <td style="text-align: right;">Lettore Barcode:</td>
                    <td style="text-align: center;" title="Previsti">
                        <label class="err"><?php echo $request[0]['sp_bar']; ?></label>
                    </td>
                    <td style="text-align: center;" title="Effettivi">
                        <label class="ok"><?php echo $consuntivo['sp_bar']; ?></label>
                    </td>
                    <td style="text-align: center;" title="Bilancio">
                            <?php echo ($consuntivo['sp_bar'] - $request[0]['sp_bar']); ?>
                    </td>
                    <td style="text-align: center;">
                        <label class="ok">
            <?php
            if (($consuntivo['sp_bar'] * Spedizioni) == 0) {
                echo "-";
            } else {
                $consuntivo['tot_sped'] += ($consuntivo['sp_bar'] * Spedizioni);
                printf("%.2f €", ($consuntivo['sp_bar'] * Spedizioni));
            }
            ?>
                        </label>
                    </td>
                </tr>

                <tr>
                    <td style="text-align: right;">Lettore Assegni:</td>
                    <td style="text-align: center;" title="Previsti">
                        <label class="err"><?php echo $request[0]['sp_lett']; ?></label>
                    </td>
                    <td style="text-align: center;" title="Effettivi">
                        <label class="ok"><?php echo $consuntivo['sp_lett']; ?></label>
                    </td>
                    <td style="text-align: center;" title="Bilancio">
                            <?php echo ($consuntivo['sp_lett'] - $request[0]['sp_lett']); ?>
                    </td>
                    <td style="text-align: center;">
                        <label class="ok">
            <?php
            if (($consuntivo['sp_lett'] * Spedizioni) == 0) {
                echo "-";
            } else {
                $consuntivo['tot_sped'] += ($consuntivo['sp_lett'] * Spedizioni);
                printf("%.2f €", ($consuntivo['sp_lett'] * Spedizioni));
            }
            ?>
                        </label>
                    </td>
                </tr>

                <tr>
                    <td style="text-align: right;">Stampante di rete:</td>
                    <td style="text-align: center;" title="Previsti">
                        <label class="err"><?php echo $request[0]['sp_prt_lan']; ?></label>
                    </td>
                    <td style="text-align: center;" title="Effettivi">
                        <label class="ok"><?php echo $consuntivo['sp_prt_lan']; ?></label>
                    </td>
                    <td style="text-align: center;" title="Bilancio">
                            <?php echo ($consuntivo['sp_prt_lan'] - $request[0]['sp_prt_lan']); ?>
                    </td>
                    <td style="text-align: center;">
                        <label class="ok">
            <?php
            if (($consuntivo['sp_prt_lan'] * Spedizioni) == 0) {
                echo "-";
            } else {
                $consuntivo['tot_sped'] += ($consuntivo['sp_prt_lan'] * Spedizioni);
                printf("%.2f €", ($consuntivo['sp_prt_lan'] * Spedizioni));
            }
            ?>
                        </label>
                    </td>
                </tr>

                <tr>
                    <td style="text-align: right;">Altro Hardware:</td>
                    <td style="text-align: center;" title="Previsti">
                        <label class="err"><?php echo $request[0]['sp_altro_hw']; ?></label>
                    </td>
                    <td style="text-align: center;" title="Effettivi">
                        <label class="ok"><?php echo $consuntivo['sp_altro_hw']; ?></label>
                    </td>
                    <td style="text-align: center;" title="Bilancio">
                            <?php echo ($consuntivo['sp_altro_hw'] - $request[0]['sp_altro_hw']); ?>
                    </td>
                    <td style="text-align: center;">
                        <label class="ok">
            <?php
            if (($consuntivo['sp_altro_hw'] * Spedizioni) == 0) {
                echo "-";
            } else {
                $consuntivo['tot_sped'] += ($consuntivo['sp_altro_hw'] * Spedizioni);
                printf("%.2f €", ($consuntivo['sp_altro_hw'] * Spedizioni));
            }
            ?>
                        </label>
                    </td>
                </tr>

                <tr>
                    <th colspan="4" style="text-align: right;">Totale Spedizioni:</th>
                    <th style="text-align: center;">
                        <label class="ok"><?php printf("%.2f €", $consuntivo['tot_sped']); ?></label>
                    </th>
                </tr>

                <tr><th colspan="5"><br /></th></tr>

                <tr>
                    <th colspan="5">
                        Staging:
                        <label><?php printf("( %.2f € pezzo )", Staging); ?></label>
                    </th>
                </tr>

                <tr>
                    <th style="text-align: center;"><label>Tipo Macchina:</label></th>
                    <th style="text-align: center;"><label>Previsti:</label></th>
                    <th style="text-align: center;"><label>Effettivi:</label></th>
                    <th style="text-align: center;"><label>Bilancio:</label></th>
                    <th style="text-align: center;"><label>Subtotale:</label></th>
                </tr>

                <tr>
                    <td style="text-align: right;">Server Nuovo:</td>
                    <td style="text-align: center;" title="Previsti">
                        <label class="err"><?php echo $request[0]['st_srv']; ?></label>
                    </td>
                    <td style="text-align: center;" title="Effettivi">
                        <label class="ok"><?php echo $consuntivo['st_srv']; ?></label>
                    </td>
                    <td style="text-align: center;" title="Bilancio">
                            <?php echo ($consuntivo['st_srv'] - $request[0]['st_srv']); ?>
                    </td>
                    <td style="text-align: center;">
                        <label class="ok">
            <?php
            if (($consuntivo['st_pdl'] * Staging) == 0) {
                echo "-";
            } else {
                $consuntivo['tot_stag'] += ($consuntivo['st_srv'] * Server);
                printf("%.2f €", ($consuntivo['st_srv'] * Server));
            }
            ?>
                        </label>
                    </td>
                </tr>

                <tr>
                    <td style="text-align: right;">Pdl Nuovi:</td>
                    <td style="text-align: center;" title="Previsti">
                        <label class="err"><?php echo $request[0]['st_pdl']; ?></label>
                    </td>
                    <td style="text-align: center;" title="Effettivi">
                        <label class="ok"><?php echo $consuntivo['st_pdl']; ?></label>
                    </td>
                    <td style="text-align: center;" title="Bilancio">
            <?php echo ($consuntivo['st_pdl'] - $request[0]['st_pdl']); ?>
                    </td>
                    <td style="text-align: center;">
                        <label class="ok">
            <?php
            if (($consuntivo['st_pdl'] * Staging) == 0) {
                echo "-";
            } else {
                $consuntivo['tot_stag'] += ($consuntivo['st_pdl'] * Staging);
                printf("%.2f €", ($consuntivo['st_pdl'] * Staging));
            }
            ?>
                        </label>
                    </td>
                </tr>

                <tr>
                    <th colspan="4" style="text-align: right;">Totale Staging:</th>
                    <th style="text-align: center;">
                        <label class="ok"><?php printf("%.2f €", $consuntivo['tot_stag']); ?></label>
                    </th>
                </tr>

                <tr><th colspan="5"><br /></th></tr>

                <tr>
                    <th colspan="5">
                        Sostituzioni:
                        <label><?php printf("( %.2f € pezzo  )", Sostituzioni); ?></label>
                    </th>
                </tr>

                <tr>
                    <th style="text-align: center;"><label>Tipo Macchina:</label></th>
                    <th style="text-align: center;"><label>Previsti:</label></th>
                    <th style="text-align: center;"><label>Effettivi:</label></th>
                    <th style="text-align: center;"><label>Bilancio:</label></th>
                    <th style="text-align: center;"><label>Subtotale:</label></th>
                </tr>

                <tr>
                    <td style="text-align: right;">Server:</td>
                    <td style="text-align: center;" title="Previsti">
                        <label class="err"><?php echo $request[0]['sos_srv']; ?></label>
                    </td>
                    <td style="text-align: center;" title="Effettivi">
                        <label class="ok"><?php echo $consuntivo['sos_srv']; ?>
                    </td>
                    <td style="text-align: center;" title="Bilancio">
                            <?php echo ($consuntivo['sos_srv'] - $request[0]['sos_srv']); ?>
                    </td>
                    <td style="text-align: center;">
                        <label class="ok">
            <?php
            if (($consuntivo['sos_srv'] * Sostituzioni) == 0) {
                echo "-";
            } else {
                $consuntivo['tot_sost'] += ($consuntivo['sos_srv'] * Sostituzioni);
                printf("%.2f €", ($consuntivo['sos_srv'] * Sostituzioni));
            }
            ?>
                        </label>
                    </td>
                </tr>

                <tr>
                    <td style="text-align: right;">Pdl:</td>
                    <td style="text-align: center;" title="Previsti">
                        <label class="err"><?php echo $request[0]['sos_pdl']; ?></label>
                    </td>
                    <td style="text-align: center;" title="Effettivi">
                        <label class="ok"><?php echo $consuntivo['sos_pdl']; ?></label>
                    </td>
                    <td style="text-align: center;" title="Bilancio">
            <?php echo ($consuntivo['sos_pdl'] - $request[0]['sos_pdl']); ?>
                    </td>
                    <td style="text-align: center;">
                        <label class="ok">
            <?php
            if (($consuntivo['sos_pdl'] * Sostituzioni) == 0) {
                echo "-";
            } else {
                $consuntivo['tot_sost'] += ($consuntivo['sos_pdl'] * Sostituzioni);
                printf("%.2f €", ($consuntivo['sos_pdl'] * Sostituzioni));
            }
            ?>
                        </label>
                    </td>
                </tr>

                <tr>
                    <td style="text-align: right;">Monitor:</td>
                    <td style="text-align: center;" title="Previsti">
                        <label class="err"><?php echo $request[0]['sos_mon']; ?></label>
                    </td>
                    <td style="text-align: center;" title="Effettivi">
                        <label class="ok"><?php echo $consuntivo['sos_mon']; ?></label>
                    </td>
                    <td style="text-align: center;" title="Bilancio">
                            <?php echo ($consuntivo['sos_mon'] - $request[0]['sos_mon']); ?>
                    </td>
                    <td style="text-align: center;">
                        <label class="ok">
            <?php
            if (($consuntivo['sos_mon'] * Sostituzioni) == 0) {
                echo "-";
            } else {
                $consuntivo['tot_sost'] += ($consuntivo['sos_mon'] * Sostituzioni);
                printf("%.2f €", ($consuntivo['sos_mon'] * Sostituzioni));
            }
            ?>
                        </label>
                    </td>
                </tr>

                <tr>
                    <td style="text-align: right;">Stampante di rete:</td>
                    <td style="text-align: center;" title="Previsti">
                        <label class="err"><?php echo $request[0]['sos_prt_lan']; ?></label>
                    </td>
                    <td style="text-align: center;" title="Effettivi">
                        <label class="ok"><?php echo $consuntivo['sos_prt_lan']; ?></label>
                    </td>
                    <td style="text-align: center;" title="Bilancio">
                            <?php echo ($consuntivo['sos_prt_lan'] - $request[0]['sos_prt_lan']); ?>
                    </td>
                    <td style="text-align: center;">
                        <label class="ok">
            <?php
            if (($consuntivo['sos_prt_lan'] * Sostituzioni) == 0) {
                echo "-";
            } else {
                $consuntivo['tot_sost'] += ($consuntivo['sos_prt_lan'] * Sostituzioni);
                printf("%.2f €", ($consuntivo['sos_prt_lan'] * Sostituzioni));
            }
            ?>
                        </label>
                    </td>
                </tr>

                <tr>
                    <td style="text-align: right;">Stampante laser:</td>
                    <td style="text-align: center;" title="Previsti">
                        <label class="err"><?php echo $request[0]['sos_las']; ?></label>
                    </td>
                    <td style="text-align: center;" title="Effettivi">
                        <label class="ok"><?php echo $consuntivo['sos_las']; ?></label>
                    </td>
                    <td style="text-align: center;" title="Bilancio">
                            <?php echo ($consuntivo['sos_las'] - $request[0]['sos_las']); ?>
                    </td>
                    <td style="text-align: center;">
                        <label class="ok">
            <?php
            if (($consuntivo['sos_las'] * Sostituzioni) == 0) {
                echo "-";
            } else {
                $consuntivo['tot_sost'] += ($consuntivo['sos_las'] * Sostituzioni);
                printf("%.2f €", ($consuntivo['sos_las'] * Sostituzioni));
            }
            ?>
                        </label>
                    </td>
                </tr>

                <tr>
                    <td style="text-align: right;">Lettore Assegni:</td>
                    <td style="text-align: center;" title="Previsti">
                        <label class="err"><?php echo $request[0]['sos_lett']; ?></label>
                    </td>
                    <td style="text-align: center;" title="Effettivi">
                        <label class="ok"><?php echo $consuntivo['sos_lett']; ?></label>
                    </td>
                    <td style="text-align: center;" title="Bilancio">
                            <?php echo ($consuntivo['sos_lett'] - $request[0]['sos_lett']); ?>
                    </td>
                    <td style="text-align: center;">
                        <label class="ok">
            <?php
            if (($consuntivo['sos_lett'] * Sostituzioni) == 0) {
                echo "-";
            } else {
                $consuntivo['tot_sost'] += ($consuntivo['sos_lett'] * Sostituzioni);
                printf("%.2f €", ($consuntivo['sos_lett'] * Sostituzioni));
            }
            ?>
                        </label>
                    </td>
                </tr>

                <tr>
                    <td style="text-align: right;">Lettore Barcode:</td>
                    <td style="text-align: center;" title="Previsti">
                        <label class="err"><?php echo $request[0]['sos_bar']; ?></label>
                    </td>
                    <td style="text-align: center;" title="Effettivi">
                        <label class="ok"><?php echo $consuntivo['sos_bar']; ?></label>
                    </td>
                    <td style="text-align: center;" title="Bilancio">
                            <?php echo ($consuntivo['sos_bar'] - $request[0]['sos_bar']); ?>
                    </td>
                    <td style="text-align: center;">
                        <label class="ok">
            <?php
            if (($consuntivo['sos_bar'] * Sostituzioni) == 0) {
                echo "-";
            } else {
                $consuntivo['tot_sost'] += ($consuntivo['sos_bar'] * Sostituzioni);
                printf("%.2f €", ($consuntivo['sos_bar'] * Sostituzioni));
            }
            ?>
                        </label>
                    </td>
                </tr>

                <tr>
                    <th colspan="4" style="text-align: right;">Totale Sostituzioni:</th>
                    <th style="text-align: center;">
                        <label class="ok"><?php printf("%.2f €", $consuntivo['tot_sost']); ?></label>
                    </th>
                </tr>

                <tr><th colspan="5"><br /></th></tr>

                <tr>
                    <th colspan="5">
                        Riconfigurazioni:
                        <label><?php printf("( %.2f € pezzo )", Riconfigurazioni); ?></label>
                    </th>
                </tr>

                <tr>
                    <th style="text-align: center;"><label>Tipo Macchina:</label></th>
                    <th style="text-align: center;"><label>Previsti:</label></th>
                    <th style="text-align: center;"><label>Effettivi:</label></th>
                    <th style="text-align: center;"><label>Bilancio:</label></th>
                    <th style="text-align: center;"><label>Subtotale:</label></th>
                </tr>

                <tr>
                    <td style="text-align: right;">Stampante di rete:</td>
                    <td style="text-align: center;" title="Previsti">
                        <label class="err"><?php echo $request[0]['ric_prt_lan']; ?></label>
                    </td>
                    <td style="text-align: center;" title="Effettivi">
                        <label class="ok">
                            <?php
                            /* dato che sul dettaglio con viene inserito
                             * nulla riguardo le riconfigurazioni,
                             * non utilizzo la variabile: $consuntivo['ric_prt_lan'];
                             * ma riutilizzo lo stesso valore richiesto. */
                            echo $request[0]['ric_prt_lan'];
                            ?>
                        </label>
                    </td>
                    <td style="text-align: center;" title="Bilancio">
            <?php echo ($request[0]['ric_prt_lan'] - $request[0]['ric_prt_lan']); ?>
                    </td>
                    <td style="text-align: center;">
                        <label class="ok">
            <?php
            if ((($request[0]['ric_prt_lan']) * Riconfigurazioni) == 0) {
                echo "-";
            } else {
                $consuntivo['tot_ric'] += ($request[0]['ric_prt_lan'] * Riconfigurazioni);
                printf("%.2f €", ($request[0]['ric_prt_lan']) * Riconfigurazioni);
            }
            ?>
                        </label>
                    </td>
                </tr>

                <tr>
                    <td style="text-align: right;">Multifunzione:</td>
                    <td style="text-align: center;" title="Previsti">
                        <label class="err"><?php echo $request[0]['ric_multi']; ?></label>
                    </td>
                    <td style="text-align: center;" title="Effettivi">
                        <label class="ok">
            <?php
            /* dato che sul dettaglio con viene inserito
             * nulla riguardo le riconfigurazioni,
             * non utilizzo la variabile: $consuntivo['ric_multi'];
             * ma riutilizzo lo stesso valore richiesto. */
            echo $request[0]['ric_multi'];
            ?>
                        </label>
                    </td>
                    <td style="text-align: center;" title="Bilancio">
                            <?php echo ($request[0]['ric_multi'] - $request[0]['ric_multi']); ?>
                    </td>
                    <td style="text-align: center;">
                        <label class="ok">
            <?php
            if ((($request[0]['ric_multi']) * Riconfigurazioni) == 0) {
                echo "-";
            } else {
                $consuntivo['tot_ric'] += ($request[0]['ric_multi'] * Riconfigurazioni);
                printf("%.2f €", ($request[0]['ric_multi']) * Riconfigurazioni);
            }
            ?>
                        </label>
                    </td>
                </tr>

                <tr>
                    <th colspan="4" style="text-align: right;">Totale Riconfigurazioni:</th>
                    <th style="text-align: center;">
                        <label class="ok"><?php printf("%.2f €", $consuntivo['tot_ric']); ?></label>
                    </th>
                </tr>

                <tr><th colspan="5"><br /></th></tr>

                <tr>
                    <th colspan="5">
                        Disinstallazioni:
                        <label><?php printf("( %.2f € pezzo )", Disinstallazioni); ?></label>
                    </th>
                </tr>

                <tr>
                    <th style="text-align: center;"><label>Tipo Macchina:</label></th>
                    <th style="text-align: center;"><label>Previsti:</label></th>
                    <th style="text-align: center;"><label>Effettivi:</label></th>
                    <th style="text-align: center;"><label>Bilancio:</label></th>
                    <th style="text-align: center;"><label>Subtotale:</label></th>
                </tr>

                <tr>
                    <td style="text-align: right;">Server:</td>
                    <td style="text-align: center;" title="Previsti">
                        <label class="err"><?php echo $request[0]['dis_srv']; ?></label>
                    </td>
                    <td style="text-align: center;" title="Effettivi">
                        <label class="ok"><?php echo $consuntivo['dis_srv']; ?></label>
                    </td>
                    <td style="text-align: center;" title="Bilancio">
                            <?php echo ($consuntivo['dis_srv'] - $request[0]['dis_srv']); ?>
                    </td>
                    <td style="text-align: center;">
                        <label class="ok">
            <?php
            if ((($request[0]['dis_srv']) * Disinstallazioni) == 0) {
                echo "-";
            } else {
                $consuntivo['tot_dis'] += ($consuntivo['dis_srv'] * Disinstallazioni);
                printf("%.2f €", ($request[0]['dis_srv']) * Disinstallazioni);
            }
            ?>
                        </label>
                    </td>
                </tr>

                <tr>
                    <td style="text-align: right;">Pdl:</td>
                    <td style="text-align: center;" title="Previsti">
                        <label class="err"><?php echo $request[0]['dis_pdl']; ?></label>
                    </td>
                    <td style="text-align: center;" title="Effettivi">
                        <label class="ok"><?php echo $consuntivo['dis_pdl']; ?></label>
                    </td>
                    <td style="text-align: center;" title="Bilancio">
            <?php echo ($consuntivo['dis_pdl'] - $request[0]['dis_pdl']); ?>
                    </td>
                    <td style="text-align: center;">
                        <label class="ok">
            <?php
            if (($consuntivo['dis_pdl'] * Disinstallazioni) == 0) {
                echo "-";
            } else {
                $consuntivo['tot_dis'] += ($consuntivo['dis_pdl'] * Disinstallazioni);
                printf("%.2f €", ($consuntivo['dis_pdl'] * Disinstallazioni));
            }
            ?>
                        </label>
                    </td>
                </tr>

                <tr>
                    <td style="text-align: right;">Monitor:</td>
                    <td style="text-align: center;" title="Previsti">
                        <label class="err"><?php echo $request[0]['dis_mon']; ?></label>
                    </td>
                    <td style="text-align: center;" title="Effettivi">
                        <label class="ok"><?php echo $consuntivo['dis_mon']; ?></label>
                    </td>
                    <td style="text-align: center;" title="Bilancio">
                            <?php echo ($consuntivo['dis_mon'] - $request[0]['dis_mon']); ?>
                    </td>
                    <td style="text-align: center;">
                        <label class="ok">
            <?php
            if (($consuntivo['dis_mon'] * Disinstallazioni) == 0) {
                echo "-";
            } else {
                $consuntivo['tot_dis'] += ($consuntivo['dis_mon'] * Disinstallazioni);
                printf("%.2f €", ($consuntivo['dis_mon'] * Disinstallazioni));
            }
            ?>
                        </label>
                </tr>

                <tr>
                    <td style="text-align: right;">Stampante laser:</td>
                    <td style="text-align: center;" title="Previsti">
                        <label class="err"><?php echo $request[0]['dis_las']; ?></label>
                    </td>
                    <td style="text-align: center;" title="Effettivi">
                        <label class="ok"><?php echo $consuntivo['dis_las']; ?></label>
                    </td>
                    <td style="text-align: center;" title="Bilancio">
                            <?php echo ($consuntivo['dis_las'] - $request[0]['dis_las']); ?>
                    </td>
                    <td style="text-align: center;">
                        <label class="ok">
            <?php
            if ((($consuntivo['dis_las']) * Disinstallazioni) == 0) {
                echo "-";
            } else {
                $consuntivo['tot_dis'] += ($consuntivo['dis_las'] * Disinstallazioni);
                printf("%.2f €", ($consuntivo['dis_las']) * Disinstallazioni);
            }
            ?>
                        </label>
                    </td>
                </tr>

                <tr>
                    <td style="text-align: right;">Stampante di cassa:</td>
                    <td style="text-align: center;" title="Previsti">
                        <label class="err"><?php echo $request[0]['dis_cas']; ?></label>
                    </td>
                    <td style="text-align: center;" title="Effettivi">
                        <label class="ok"><?php echo $consuntivo['dis_cas']; ?></label>
                    </td>
                    <td style="text-align: center;" title="Bilancio">
                            <?php echo ($consuntivo['dis_cas'] - $request[0]['dis_cas']); ?>
                    </td>
                    <td style="text-align: center;">
                        <label class="ok">
            <?php
            if (($consuntivo['dis_cas'] * Disinstallazioni) == 0) {
                echo "-";
            } else {
                $consuntivo['tot_dis'] += ($consuntivo['dis_cas'] * Disinstallazioni);
                printf("%.2f €", ($consuntivo['dis_cas'] * Disinstallazioni));
            }
            ?>
                        </label>
                    </td>
                </tr>

                <tr>
                    <td style="text-align: right;">Lettore Assegni:</td>
                    <td style="text-align: center;" title="Previsti">
                        <label class="err"><?php echo $request[0]['dis_let']; ?></label>
                    </td>
                    <td style="text-align: center;" title="Effettivi">
                        <label class="ok"><?php echo $consuntivo['dis_let']; ?></label>
                    </td>
                    <td style="text-align: center;" title="Bilancio">
                            <?php echo ($consuntivo['dis_let'] - $request[0]['dis_let']); ?>
                    </td>
                    <td style="text-align: center;">
                        <label class="ok">
            <?php
            if (($consuntivo['dis_let'] * Disinstallazioni) == 0) {
                echo "-";
            } else {
                $consuntivo['tot_dis'] += ($consuntivo['dis_let'] * Disinstallazioni);
                printf("%.2f €", ($consuntivo['dis_let'] * Disinstallazioni));
            }
            ?>
                        </label>
                    </td>
                </tr>

                <tr>
                    <td style="text-align: right;">Lettore Barcode:</td>
                    <td style="text-align: center;" title="Previsti">
                        <label class="err"><?php echo $request[0]['dis_bar']; ?></label>
                    </td>
                    <td style="text-align: center;" title="Effettivi">
                        <label class="ok"><?php echo $consuntivo['dis_bar']; ?></label>
                    </td>
                    <td style="text-align: center;" title="Bilancio">
                            <?php echo ($consuntivo['dis_bar'] - $request[0]['dis_bar']); ?>
                    </td>
                    <td style="text-align: center;">
                        <label class="ok">
            <?php
            if (($consuntivo['dis_bar'] * Disinstallazioni) == 0) {
                echo "-";
            } else {
                $consuntivo['tot_dis'] += ($consuntivo['dis_bar'] * Disinstallazioni);
                printf("%.2f €", ($consuntivo['dis_bar'] * Disinstallazioni));
            }
            ?>
                        </label>
                    </td>
                </tr>

                <tr>
                    <td style="text-align: right;">Fax:</td>
                    <td style="text-align: center;" title="Previsti">
                        <label class="err"><?php echo $request[0]['dis_fax']; ?></label>
                    </td>
                    <td style="text-align: center;" title="Effettivi">
                        <label class="ok"><?php echo $consuntivo['dis_fax']; ?></label>
                    </td>
                    <td style="text-align: center;" title="Bilancio">
                            <?php echo ($consuntivo['dis_fax'] - $request[0]['dis_fax']); ?>
                    </td>
                    <td style="text-align: center;">
                        <label class="ok">
            <?php
            if (($consuntivo['dis_fax'] * Disinstallazioni) == 0) {
                echo "-";
            } else {
                $consuntivo['tot_dis'] += ($consuntivo['dis_fax'] * Disinstallazioni);
                printf("%.2f €", ($consuntivo['dis_fax'] * Disinstallazioni));
            }
            ?>
                        </label>
                    </td>
                </tr>

                <tr>
                    <td style="text-align: right;">Altro Hardware:</td>
                    <td style="text-align: center;" title="Previsti">
                        <label class="err"><?php echo $request[0]['dis_altro_hw']; ?></label>
                    </td>
                    <td style="text-align: center;" title="Effettivi">
                        <label class="ok"><?php echo $consuntivo['dis_altro_hw']; ?></label>
                    </td>
                    <td style="text-align: center;" title="Bilancio">
                            <?php echo ($consuntivo['dis_altro_hw'] - $request[0]['dis_altro_hw']); ?>
                    </td>
                    <td style="text-align: center;">
                        <label class="ok">
            <?php
            if (($consuntivo['dis_altro_hw'] * Disinstallazioni) == 0) {
                echo "-";
            } else {
                $consuntivo['tot_dis'] += ($consuntivo['dis_altro_hw'] * Disinstallazioni);
                printf("%.2f €", ($consuntivo['dis_altro_hw'] * Disinstallazioni));
            }
            ?>
                        </label>
                    </td>
                </tr>

                <tr>
                    <th colspan="4" style="text-align: right;">Totale Disinstallazioni:</th>
                    <th style="text-align: center;">
                        <label class="ok"><?php printf("%.2f €", $consuntivo['tot_dis']); ?></label>
                    </th>
                </tr>

                <tr><th colspan="5"><br /></th></tr>

                <tr>
                    <th colspan="5">
                        Ritiri:
                        <label><?php printf("( %.2f € pezzo )", Ritiri); ?></label>
                    </th>
                </tr>

                <tr>
                    <th style="text-align: center;"><label>Tipo Macchina:</label></th>
                    <th style="text-align: center;"><label>Previsti:</label></th>
                    <th style="text-align: center;"><label>Effettivi:</label></th>
                    <th style="text-align: center;"><label>Bilancio:</label></th>
                    <th style="text-align: center;"><label>Subtotale:</label></th>
                </tr>

                <tr>
                    <td style="text-align: right;">Server:</td>
                    <td style="text-align: center;" title="Previsti">
                        <label class="err"><?php echo $request[0]['rit_srv']; ?></label>
                    </td>
                    <td style="text-align: center;" title="Effettivi">
                        <label class="ok"><?php echo $consuntivo['rit_srv']; ?></label>
                    </td>
                    <td style="text-align: center;" title="Bilancio">
                            <?php echo ($consuntivo['rit_srv'] - $request[0]['rit_srv']); ?>
                    </td>
                    <td style="text-align: center;">
                        <label class="ok">
            <?php
            if ((($consuntivo['rit_srv']) * Ritiri) == 0) {
                echo "-";
            } else {
                $consuntivo['tot_rit'] += ($consuntivo['rit_srv'] * Ritiri);
                printf("%.2f €", ($consuntivo['rit_srv']) * Ritiri);
            }
            ?>
                        </label>
                    </td>
                </tr>

                <tr>
                    <td style="text-align: right;">Pdl:</td>
                    <td style="text-align: center;" title="Previsti">
                        <label class="err"><?php echo $request[0]['rit_pdl']; ?></label>
                    </td>
                    <td style="text-align: center;" title="Effettivi">
                        <label class="ok"><?php echo $consuntivo['rit_pdl']; ?></label>
                    </td>
                    <td style="text-align: center;" title="Bilancio">
            <?php echo ($consuntivo['rit_pdl'] - $request[0]['rit_pdl']); ?>
                    </td>
                    <td style="text-align: center;">
                        <label class="ok">
            <?php
            if ((($consuntivo['rit_pdl']) * Ritiri) == 0) {
                echo "-";
            } else {
                $consuntivo['tot_rit'] += ($consuntivo['rit_pdl'] * Ritiri);
                printf("%.2f €", ($consuntivo['rit_pdl']) * Ritiri);
            }
            ?>
                        </label>
                    </td>
                </tr>

                <tr>
                    <td style="text-align: right;">Monitor:</td>
                    <td style="text-align: center;" title="Previsti">
                        <label class="err"><?php echo $request[0]['rit_mon']; ?></label>
                    </td>
                    <td style="text-align: center;" title="Effettivi">
                        <label class="ok"><?php echo $consuntivo['rit_mon']; ?></label>
                    </td>
                    <td style="text-align: center;" title="Bilancio">
                            <?php echo ($consuntivo['rit_mon'] - $request[0]['rit_mon']); ?>
                    </td>
                    <td style="text-align: center;">
                        <label class="ok">
            <?php
            if ((($consuntivo['rit_mon']) * Ritiri) == 0) {
                echo "-";
            } else {
                $consuntivo['tot_rit'] += ($consuntivo['rit_mon'] * Ritiri);
                printf("%.2f €", ($consuntivo['rit_mon']) * Ritiri);
            }
            ?>
                        </label>
                    </td>
                </tr>

                <tr>
                    <td style="text-align: right;">Stampante laser:</td>
                    <td style="text-align: center;" title="Previsti">
                        <label class="err"><?php echo $request[0]['rit_las']; ?></label>
                    </td>
                    <td style="text-align: center;" title="Effettivi">
                        <label class="ok"><?php echo $consuntivo['rit_las']; ?></label>
                    </td>
                    <td style="text-align: center;" title="Bilancio">
                            <?php echo ($consuntivo['rit_las'] - $request[0]['rit_las']); ?>
                    </td>
                    <td style="text-align: center;">
                        <label class="ok">
            <?php
            if ((($consuntivo['rit_las']) * Ritiri) == 0) {
                echo "-";
            } else {
                $consuntivo['tot_rit'] += ($consuntivo['rit_las'] * Ritiri);
                printf("%.2f €", ($consuntivo['rit_las']) * Ritiri);
            }
            ?>
                        </label>
                    </td>
                </tr>

                <tr>
                    <td style="text-align: right;">Stampante di rete:</td>
                    <td style="text-align: center;" title="Previsti">
                        <label class="err"><?php echo $request[0]['rit_prt_lan']; ?></label>
                    </td>
                    <td style="text-align: center;" title="Effettivi">
                        <label class="ok"><?php echo $consuntivo['rit_prt_lan']; ?></label>
                    </td>
                    <td style="text-align: center;" title="Bilancio">
                            <?php echo ($consuntivo['rit_prt_lan'] - $request[0]['rit_prt_lan']); ?>
                    </td>
                    <td style="text-align: center;">
                        <label class="ok">
            <?php
            if ((($consuntivo['rit_prt_lan']) * Ritiri) == 0) {
                echo "-";
            } else {
                $consuntivo['tot_rit'] += ($consuntivo['rit_prt_lan'] * Ritiri);
                printf("%.2f €", ($consuntivo['rit_prt_lan']) * Ritiri);
            }
            ?>
                        </label>
                    </td>
                </tr>

                <tr>
                    <td style="text-align: right;">Stampante di cassa:</td>
                    <td style="text-align: center;" title="Previsti">
                        <label class="err"><?php echo $request[0]['rit_cas']; ?></label>
                    </td>
                    <td style="text-align: center;" title="Effettivi">
                        <label class="ok"><?php echo $consuntivo['rit_cas']; ?></label>
                    </td>
                    <td style="text-align: center;" title="Bilancio">
                            <?php echo ($consuntivo['rit_cas'] - $request[0]['rit_cas']); ?>
                    </td>
                    <td style="text-align: center;">
                        <label class="ok">
            <?php
            if ((($consuntivo['rit_cas']) * Ritiri) == 0) {
                echo "-";
            } else {
                $consuntivo['tot_rit'] += ($consuntivo['rit_cas'] * Ritiri);
                printf("%.2f €", ($consuntivo['rit_cas']) * Ritiri);
            }
            ?>
                        </label>
                    </td>
                </tr>

                <tr>
                    <td style="text-align: right;">Lettore Assegni:</td>
                    <td style="text-align: center;" title="Previsti">
                        <label class="err"><?php echo $request[0]['rit_let']; ?></label>
                    </td>
                    <td style="text-align: center;" title="Effettivi">
                        <label class="ok"><?php echo $consuntivo['rit_let']; ?></label>
                    </td>
                    <td style="text-align: center;" title="Bilancio">
                            <?php echo ($consuntivo['rit_let'] - $request[0]['rit_let']); ?>
                    </td>
                    <td style="text-align: center;">
                        <label class="ok">
            <?php
            if ((($consuntivo['rit_let']) * Ritiri) == 0) {
                echo "-";
            } else {
                $consuntivo['tot_rit'] += ($consuntivo['rit_let'] * Ritiri);
                printf("%.2f €", ($consuntivo['rit_let']) * Ritiri);
            }
            ?>
                        </label>
                    </td>
                </tr>

                <tr>
                    <td style="text-align: right;">Lettore Barcode:</td>
                    <td style="text-align: center;" title="Previsti">
                        <label class="err"><?php echo $request[0]['rit_bar']; ?></label>
                    </td>
                    <td style="text-align: center;" title="Effettivi">
                        <label class="ok"><?php echo $consuntivo['rit_bar']; ?></label>
                    </td>
                    <td style="text-align: center;" title="Bilancio">
                            <?php echo ($consuntivo['rit_bar'] - $request[0]['rit_bar']); ?>
                    </td>
                    <td style="text-align: center;">
                        <label class="ok">
            <?php
            if ((($consuntivo['rit_bar']) * Ritiri) == 0) {
                echo "-";
            } else {
                $consuntivo['tot_rit'] += ($consuntivo['rit_bar'] * Ritiri);
                printf("%.2f €", ($consuntivo['rit_bar']) * Ritiri);
            }
            ?>
                        </label>
                    </td>
                </tr>

                <tr>
                    <td style="text-align: right;">Fax:</td>
                    <td style="text-align: center;" title="Previsti">
                        <label class="err"><?php echo $request[0]['rit_fax']; ?></label>
                    </td>
                    <td style="text-align: center;" title="Effettivi">
                        <label class="ok"><?php echo $consuntivo['rit_fax']; ?></label>
                    </td>
                    <td style="text-align: center;" title="Bilancio">
                            <?php echo ($consuntivo['rit_fax'] - $request[0]['rit_fax']); ?>
                    </td>
                    <td style="text-align: center;">
                        <label class="ok">
            <?php
            if ((($consuntivo['rit_fax']) * Ritiri) == 0) {
                echo "-";
            } else {
                $consuntivo['tot_rit'] += ($consuntivo['rit_fax'] * Ritiri);
                printf("%.2f €", ($consuntivo['rit_fax']) * Ritiri);
            }
            ?>
                        </label>
                    </td>
                </tr>

                <tr>
                    <td style="text-align: right;">Altro Hardware:</td>
                    <td style="text-align: center;" title="Previsti">
                        <label class="err"><?php echo $request[0]['rit_altro_hw']; ?></label>
                    </td>
                    <td style="text-align: center;" title="Effettivi">
                        <label class="ok"><?php echo $consuntivo['rit_altro_hw']; ?></label>
                    </td>
                    <td style="text-align: center;" title="Bilancio">
                            <?php echo ($consuntivo['rit_altro_hw'] - $request[0]['rit_altro_hw']); ?>
                    </td>
                    <td style="text-align: center;">
                        <label class="ok">
            <?php
            if ((($consuntivo['rit_altro_hw']) * Ritiri) == 0) {
                echo "-";
            } else {
                $consuntivo['tot_rit'] += ($consuntivo['rit_altro_hw'] * Ritiri);
                printf("%.2f €", ($consuntivo['rit_altro_hw']) * Ritiri);
            }
            ?>
                        </label>
                    </td>
                </tr>

                <tr>
                    <th colspan="4" style="text-align: right;">Totale Ritiri:</th>
                    <th style="text-align: center;">
                        <label class="ok"><?php printf("%.2f €", $consuntivo['tot_rit']); ?></label>
                    </th>
                </tr>

                <tr><th colspan="5"><br /></th></tr>

                <tr>
                    <th colspan="5">
                        Incident:
                        <label><?php printf("( %.2f € pezzo )", pIncident); ?></label>
                    </th>
                </tr>

                <tr>
                    <th style="text-align: center;"><label>Tipo Macchina:</label></th>
                    <th style="text-align: center;"><label>Previsti:</label></th>
                    <th style="text-align: center;"><label>Effettivi:</label></th>
                    <th style="text-align: center;"><label>Bilancio:</label></th>
                    <th style="text-align: center;"><label>Subtotale:</label></th>
                </tr>

                <tr>
                    <td style="text-align: right;">Server:</td>
                    <td style="text-align: center;" title="Previsti">
                        <label class="err">
                            <?php echo $request[0]['qi_server']; ?>
                        </label>
                    </td>
                    <td style="text-align: center;" title="Effettivi">
                        <label class="ok">
                            <?php echo $consuntivo['inc_srv']; ?>
                        </label>
                    </td>
                    <td style="text-align: center;" title="Bilancio">
            <?php echo ($consuntivo['inc_srv'] - $request[0]['qi_server']); ?>
                    </td>
                    <td style="text-align: center;">
                        <label class="ok">
            <?php
            if ((($consuntivo['inc_srv']) * Server) == 0) {
                echo "-";
            } else {
                $consuntivo['tot_inc'] += ($consuntivo['inc_srv'] * Server);
                printf("%.2f €", ($consuntivo['inc_srv']) * Server);
            }
            ?>
                        </label>
                    </td>
                </tr>

                <tr>
                    <td style="text-align: right;">Lettore assegni:</td>
                    <td style="text-align: center;" title="Previsti">
                        <label class="err">
            <?php echo $request[0]['qi_lett']; ?>
                        </label>
                    </td>
                    <td style="text-align: center;" title="Effettivi">
                        <label class="ok">
            <?php echo $consuntivo['inc_lett']; ?>
                        </label>
                    </td>
                    <td style="text-align: center;" title="Bilancio">
            <?php echo ($consuntivo['inc_lett'] - $request[0]['qi_lett']); ?>
                    </td>
                    <td style="text-align: center;">
                        <label class="ok">
            <?php
            if ((($consuntivo['inc_lett']) * pIncident) == 0) {
                echo "-";
            } else {
                $consuntivo['tot_inc'] += ($consuntivo['inc_lett'] * pIncident);
                printf("%.2f €", ($consuntivo['inc_lett']) * pIncident);
            }
            ?>
                        </label>
                    </td>
                </tr>

                <tr>
                    <td style="text-align: right;">Monitor:</td>
                    <td style="text-align: center;" title="Previsti">
                        <label class="err">
                            <?php echo $request[0]['qi_mon']; ?>
                        </label>
                    </td>
                    <td style="text-align: center;" title="Effettivi">
                        <label class="ok">
            <?php echo $consuntivo['inc_mon']; ?>
                        </label>
                    </td>
                    <td style="text-align: center;" title="Bilancio">
            <?php echo ($consuntivo['inc_mon'] - $request[0]['qi_mon']); ?>
                    </td>
                    <td style="text-align: center;">
                        <label class="ok">
            <?php
            if ((($consuntivo['inc_mon']) * pIncident) == 0) {
                echo "-";
            } else {
                $consuntivo['tot_inc'] += ($consuntivo['inc_mon'] * pIncident);
                printf("%.2f €", ($consuntivo['inc_mon']) * pIncident);
            }
            ?>
                        </label>
                    </td>
                </tr>

                <tr>
                    <td style="text-align: right;">Stampante di cassa:</td>
                    <td style="text-align: center;" title="Previsti">
                        <label class="err">
                            <?php echo $request[0]['qi_prt']; ?>
                        </label>
                    </td>
                    <td style="text-align: center;" title="Effettivi">
                        <label class="ok">
            <?php echo $consuntivo['inc_prt']; ?>
                        </label>
                    </td>
                    <td style="text-align: center;" title="Bilancio">
            <?php echo ($consuntivo['inc_prt'] - $request[0]['qi_prt']); ?>
                    </td>
                    <td style="text-align: center;">
                        <label class="ok">
            <?php
            if ((($consuntivo['inc_prt']) * pIncident) == 0) {
                echo "-";
            } else {
                $consuntivo['tot_inc'] += ($consuntivo['inc_mon'] * pIncident);
                printf("%.2f €", ($consuntivo['inc_prt']) * pIncident);
            }
            ?>
                        </label>
                    </td>
                </tr>

                <tr>
                    <th colspan="4" style="text-align: right;">Totale Incident:</th>
                    <th style="text-align: center;">
                        <label class="ok"><?php printf("%.2f €", $consuntivo['tot_inc']); ?></label>
                    </th>
                </tr>

            <?php
            $consuntivo['tot_fil'] = ($consuntivo['tot_sped'] + $consuntivo['tot_stag'] + $consuntivo['tot_sost'] + $consuntivo['tot_ric'] + $consuntivo['tot_dis'] + $consuntivo['tot_rit'] + $consuntivo['tot_inc']);
            ?>

                <tr><th colspan="5"><br /></th></tr>

                <tr>
                    <th colspan="4" style="text-align: right;">Totale Filiale:</th>
                    <th style="text-align: center;">
                        <label class="ok"><?php printf("%.2f €", $consuntivo['tot_fil']); ?></label>
                    </th>
                </tr>

                <tr>
                    <th colspan="5">
                        <label class="err"><?php echo $errori; ?></label>
                    </th>
                </tr>
            </table><?php
            break;

        default:
            // caso non gestito
            echo "[Debug]: Formato di output non gestito: '$OutputType'. <br />";
            break;
    }
}

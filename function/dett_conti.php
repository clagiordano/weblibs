<?php
$root = substr($_SERVER['DOCUMENT_ROOT'], 0, -1);
require_once ("$root/conti/connection.php");
require_once ("$root/Function/Strings.php");
require_once ("$root/Function/Db.php");
require_once ("$root/Function/Debug.php");
require_once ("$root/Function/DataTime.php");
?>

<table style="width: 100%;" class="dettaglio">
    <?php
    if ($_POST['rad_tipo'] == "movimenti") {
        //<!-- Dettaglio movimento !-->
        $DettaglioOp = GetRows("view_piano_conti",
                               "id_piano_conti = '"
                . $field['id_piano_conti'] . "'", "", $db);

        foreach ($DettaglioOp as $key_dett => $field_dett) {
            $TotDare += $field_dett['importo_d'];
            $TotAvere += $field_dett['importo_a'];
            ?>
            <tr>
                <td style="width: 75px;"><br /></td>
                <td style="text-align: right;">
                    <label>&bull;</label>
                </td>
                <td>
                    <?php
                    if ($field_dett['id_anagrafica'] != 0) {
                        echo "(" . $field_dett['codice_sottoconto']
                        . ") " . htmlentities($field_dett['rag_soc']);
                    } else {
                        echo "(" . $field_dett['codice_sottoconto'] . ") "
                        . ucfirst(htmlentities(strtolower($field_dett['descr_sottoconto'])));
                    }
                    ?>
                </td>
                <td>
                <?php echo htmlentities($field_dett['descr']); ?>
                </td>

                    <?php if ($field_dett['da'] == "d") { ?>
                    <td style="text-align: center;" title="Importo Dare">
                    <?php echo "<label class=\"err\">"
                    . $field_dett['importo_d'] . "</label>";
                    ?>
                    </td>
                    <td style="text-align: center;" title="Importo Avere">-</td>
                    <?php } else { ?>
                    <td style="text-align: center;" title="Importo Dare">-</td>
                    <td style="text-align: center;" title="Importo Avere">
            <?php echo "<label class=\"ok\">"
            . $field_dett['importo_a'] . "</label>";
            ?>
                    </td>
                    <?php } ?>

                <td colspan="2">
                    <?php
                    if ($field_dett['id_iva'] != "0") {
                        echo $field_dett['desc_iva'];
                    } else {
                        echo "-";
                    }
                    ?>
                </td>
            </tr>
        <?php } ?>

        <tr><th colspan="8"><!-- Separatore !--><br /></th></tr>

    <?php
    } else {
        //<!-- Dettaglio mastrino !-->
        $cond_mastr     = "id_sottoconto = '" . $field['id_sottoconto'] . "' $descr_mastrino $interval_reg $interval_op";
        $DettaglioConto = GetRows("view_piano_conti", $cond_mastr, "data_reg",
                                  $db);

        foreach ($DettaglioConto as $key_dett => $field_dett) {
            ?>
            <tr>
                <td style="text-align: right;">
                <?php
                echo "(" . $field_dett['id_piano_conti'] . ") - "
                . "<label>" . Inverti_Data($field_dett['data_reg'])
                . "</label>&nbsp;";
                ?>
                </td>
                <td>
        <?php echo $field_dett['tipo_doc']; ?>
                </td>
                <!--<td>
                        <?php //echo  $field_dett['descr'];  ?>
                </td> !-->
                <td style="text-align: right;">
                    <label class="err">
        <?php echo $field_dett['importo_d'];
        $TotDare += $field_dett['importo_d'];
        ?>
                    </label>
                </td>
                <td style="text-align: right;">
                    <label class="ok">
                    <?php echo $field_dett['importo_a'];
                    $TotAvere += $field_dett['importo_a'];
                    ?>
                    </label>
                </td>

                <td style="text-align: right;">
                    <?php
                    if (in_array($field_dett['id_conto'], $SpDare)) {
                        $Saldo = ($TotDare - $TotAvere);
                    } else {
                        $Saldo = ($TotAvere - $TotDare);
                    }

                    if ($Saldo < 0) {
                        ?>
                        <label class="err">
                <?php printf("%.2f", $Saldo); ?>
                        </label>
        <?php } else { ?>
                        <label class="ok">
            <?php printf("%.2f", $Saldo); ?>
                        </label>
        <?php } ?>
                </td>
    <?php } ?>

        <tr><td colspan="8"><br /><!-- separatore !--></td></tr>
<?php } ?>
</table>
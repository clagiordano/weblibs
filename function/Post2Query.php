<?php

/*
 *      Gestione.php
 *
 *      Copyright 2008 Claudio Giordano <claudio.giordano@autistici.org>
 *
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 *      MA 02110-1301, USA.
 */

/* $rag_soc, $cognome, $nome, $ind, $civ, $stato, $citta,
  $prov, $cap, $tel, $fax, $cell, $piva, $mail, $cont, $id_pag, $id_tipo, */

function SaveCliForn($array, $db)
{

    $q = "INSERT INTO tab_anagrafica ";
    $k = "(";
    $f = ") values (";

    foreach ($array as $key => $field) {
        $k .= "$key, ";
        $f .= "'$field', ";
    }
    $f .= ");";
    $q .= $k . $f;

    $q = str_replace(", )", ")", $q);

    LogToSession("Query: $q");
}

function VerificaCampi($array)
{
    
}

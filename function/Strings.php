<?php

/*
 *      Strings.php
 *
 *      Copyright 2008 Claudio Giordano <claudio.giordano@autistici.org>
 *  Funzioni globali sostituite ed ottimizzate/potenziate dalla classe Conversion
 * 
 * @deprecated since Conversion 1.0
 */

function FindStr($stringa, $inizio, $fine, $offset, $foffset)
{
    $v_inizio = strpos($stringa, $inizio);
    if ($v_inizio !== false) {
        $v_inizio += strlen($inizio) - $offset;
        $v_fine  = strpos($stringa, $fine) - $foffset;
        $trovato = ucfirst(substr($stringa, $v_inizio, $v_fine - $v_inizio));
    } else {
        $trovato = "error";
    }

    return $trovato;
}

function ModoRicerca($val, $modo = "tutto")
{
    switch ($modo) {
        case "esatto":
            $iniz = "";
            $fine = "";
            break;
        case "inizio":
            $iniz = "";
            $fine = "%";
            break;
        case "fine":
            $iniz = "%";
            $fine = "";
            break;
        case "tutto":
            $iniz = "%";
            $fine = "%";
            break;
    }

    $valore = $iniz . $val . $fine;
    return $valore;
}

function GetTitoloPag($Action, $db)
{
    $q = "SELECT * FROM tab_menu WHERE act = '$Action'";
    $r = mysql_query($q, $db) or die(StampaErr($q));
    $n = mysql_affected_rows($db);

    $row    = mysql_fetch_assoc($r);
    $titolo = $row['titolo_pag'];

    return $titolo;
}

function AddBR($separatore, $stringa)
{
    $result    = "";
    $tmp_array = explode($separatore, $stringa);
    for ($index = 0; $index <= count($tmp_array) - 1; $index++) {
        if ($index != count($tmp_array) - 1) {
            $result = $result . $tmp_array[$index] . $separatore . "<br />";
        } else {
            $result = $result . $tmp_array[$index];
        }
    }
    return $result;
}

function ExpandSearch($string)
{
    // Sostituisce agli spazi nella ricerca con "%"
    $temp_val = explode(" ", $string);
    $string   = join("%", $temp_val);

    return $string;
}

function SplitString($string)
{
    $len    = strlen($string) - 1;
    $result = array();
    for ($i = 0; $i <= $len; $i++) {
        array_push($result, substr($string, $i, 1));
    }
    return $result;
}

function Check_piva_cf($string, $NomeCampo = "P.Iva/C.F.")
{
    if ((strlen($string) != 11) && (strlen($string) != 16)) {
        return "La lunghezza del campo $NomeCampo non è corretta";
    }

    //~ Verifica partita iva:
    if (strlen($string) == 11) {
        if (!ereg("^[0-9]+$", $string)) {
            return "La partita iva deve contenere solo cifre";
        } else {
            $s = 0;
            for ($i = 0; $i <= 9; $i += 2) {
                $s += ord($string[$i]) - ord('0');
            }

            for ($i = 1; $i <= 9; $i += 2) {
                $c = 2 * ( ord($string[$i]) - ord('0') );
                if ($c > 9) {
                    $c = $c - 9;
                }
                $s += $c;
            }

            if ((10 - $s % 10) % 10 != ord($string[10]) - ord('0')) {
                return "La partita IVA non è valida, "
                        . "il codice di controllo non corrisponde.";
            }
        }
    }

    //~ Verifica codice fiscale:
    if (strlen($string) == 16) {
        $string = strtoupper($string);
        if (!ereg("^[A-Z0-9]+$", $string)) {
            return "Il codice fiscale deve contenere solo caratteri alfanumerici.";
        } else {
            $s = 0;
            for ($i = 1; $i <= 13; $i += 2) {
                $c = $string[$i];
                if ('0' <= $c && $c <= '9') {
                    $s += ord($c) - ord('0');
                } else {
                    $s += ord($c) - ord('A');
                }
            }

            for ($i = 0; $i <= 14; $i += 2) {
                $c = $string[$i];
                switch ($c) {
                    case '0': $s += 1;
                        break;
                    case '1': $s += 0;
                        break;
                    case '2': $s += 5;
                        break;
                    case '3': $s += 7;
                        break;
                    case '4': $s += 9;
                        break;
                    case '5': $s += 13;
                        break;
                    case '6': $s += 15;
                        break;
                    case '7': $s += 17;
                        break;
                    case '8': $s += 19;
                        break;
                    case '9': $s += 21;
                        break;
                    case 'A': $s += 1;
                        break;
                    case 'B': $s += 0;
                        break;
                    case 'C': $s += 5;
                        break;
                    case 'D': $s += 7;
                        break;
                    case 'E': $s += 9;
                        break;
                    case 'F': $s += 13;
                        break;
                    case 'G': $s += 15;
                        break;
                    case 'H': $s += 17;
                        break;
                    case 'I': $s += 19;
                        break;
                    case 'J': $s += 21;
                        break;
                    case 'K': $s += 2;
                        break;
                    case 'L': $s += 4;
                        break;
                    case 'M': $s += 18;
                        break;
                    case 'N': $s += 20;
                        break;
                    case 'O': $s += 11;
                        break;
                    case 'P': $s += 3;
                        break;
                    case 'Q': $s += 6;
                        break;
                    case 'R': $s += 8;
                        break;
                    case 'S': $s += 12;
                        break;
                    case 'T': $s += 14;
                        break;
                    case 'U': $s += 16;
                        break;
                    case 'V': $s += 10;
                        break;
                    case 'W': $s += 22;
                        break;
                    case 'X': $s += 25;
                        break;
                    case 'Y': $s += 24;
                        break;
                    case 'Z': $s += 23;
                        break;
                }
            }

            if (chr($s % 26 + ord('A')) != $string[15]) {
                return "Il codice fiscale non è corretto, "
                        . "il codice di controllo non corrisponde";
            }
        }
    }
}

function provola($Text, $Separator, $Glue)
{
    $tmp_array = explode($Separator, $Text);

    $string = "";
    foreach ($tmp_array as $key => $field) {
        if ($key != count($tmp_array) - 1) {
            $string .= trim($field) . $Glue;
        } else {
            $string .= trim($field);
        }
    }

    //$string = join($Glue, $tmp_array);

    /*
      string = preg_replace("(\n)", "', '", $Text);
     * ok ma lascia uno spazio alla fine
     */

    return $string;
}

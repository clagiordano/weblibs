<?php

    //~ echo "<pre>";
        //~ print_r($_POST);
    //~ echo "</pre>";

    // Estrae le modalita' di pagamento dalla relativa tabella:
    $mod_pag = $d->GetRows("*", "tab_mod_pagamento", "", "", "pagamento");
    // Estrae i tipi anagrafica:
    $Tipi = $d->GetRows("", "tab_tipi_anagrafica", "id_tipo_anagrafica != ''", "", "descr_tipo");

    $Province = $d->GetRows("*", "tab_province", "", "", "provincia, targa", 1);
if (isset($_POST['id_prov']) && (is_numeric($_POST['id_prov']))) {
    $Comuni     = $d->GetRows("*", "tab_citta", "id_provincia = '"
        . $_POST['id_prov'] . "'", "", "citta", 1);
} elseif (isset($_GET['tomod']) && (is_numeric($_GET['tomod']))) {
    $Comuni     = $d->GetRows("*", "tab_citta", "", "", "citta", 1);
}

    //$_POST['stato'] = "Italia";
    $UrlSalva           = "?act=anagrafica&amp;salva=salva";
    $UrlPag             = "";
    $ConfSalva          = "Salvare i dati inseriti?";
    $n_mod_pag          = 0; // ??????????????

if (isset($_GET['salva'])) {
    $d->SaveErrors = "";
    require_once("Class/Php/Utils.php");
    if ((trim($_POST['ragione_sociale']) == "") && (trim($_POST['cognome']) == "")) {
        $d->SaveErrors .= "Il campo Ragione Sociale o il campo Cognome non possono essere vuoti.<br />";
    }

    if ((trim($_POST['piva']) == "")) {
        $d->SaveErrors .= "Il campo P.Iva/C.F. non può essere vuoto.<br />";
    } else {
        $d->SaveErrors .= Check_piva_cf($_POST['piva']);
    }

    if (trim($_POST['ragione_sociale']) == "") {
        $_POST['ragione_sociale'] = ucfirst($_POST['cognome'])
            . " " . ucfirst($_POST['nome']);
    }
        
    if (isset($_POST['indirizzo'])
            && (trim($_POST['indirizzo']) != "")
            && ($_POST['id_prov'] != "-")
            && ($_POST['id_citta'] != "-")) {
    // esiste l'indirizzo, città e provincia, estraggo le coordinate
    }

    if (($_GET['salva'] == "salva") && ($d->SaveErrors == "")) {
        $Esclusioni = array("Salva");
        $d->SaveRow($_POST, "", "tab_anagrafica", 1, "piva");
        //~ $r = $d->SaveRow($_POST, "", "tab_anagrafica", 1, "piva");
        //~ $status = $r[0];
    }

    if (($_GET['salva'] == "modifica") && ($d->SaveErrors == "")) {
        $Esclusioni = array("Salva");
        $where = "id_anagrafica = '" . $_GET['tomod'] . "'";
        $d->UpdateRow($_POST, $Esclusioni, "tab_anagrafica", $where, 1);
    }
}

if (isset($_GET['tomod']) && (is_numeric($_GET['tomod']))) {
    $UrlSalva = "?act=anagrafica&amp;salva=modifica&amp;tomod=" . $_GET['tomod'];
    $row = $d->GetRows(
        "*",
        "tab_anagrafica",
        "id_anagrafica = '" . $_GET['tomod'] . "'",
        "",
        "id_anagrafica"
    );

    if (empty($_POST)) {
        $_POST = $row[0];
    }
}
?>

<div id="search-form">
	<form method="post" action="<?php echo $_SERVER['REQUEST_URI']; ?>">
		<table class="form" style="width: 100%;">

			<?php if (!isset($_GET['act'])) {
?>
				<tr>
					<td style="text-align: right;">
							<a onclick="Javascript: go('frm_ricerca_avanzata.php?subact=select'); void(0);">
								<img src="../Images/Links/prev.png" alt="prev" />
								<label class="ok" style="cursor: pointer;">Indietro</label>
							</a>

							<a onclick="Javascript: ClosePopup(); void(0);">
								[ <label class="err" style="cursor: pointer;">Chiudi</label> ]
							</a>
					</td>
				</tr>
			<?php
                $d->ShowEditBar("Anagrafica:", $_GET['tomod'], "new,save", "frm_anag_articolo.php");
} else {
    $d->ShowEditBar("Anagrafica:", $_GET['tomod'], "new,save", $_GET['act']);
}
            ?>

			<tr>
				<td>
					<select name="id_tipo_anagrafica" size="1" style="width: 14.5em;">
						<?php foreach ($Tipi as $key => $field) {
?>
							<option value="<?php echo $field['id_tipo_anagrafica']; ?>"
								<?php if ($_POST['id_tipo_anagrafica'] == $field['id_tipo_anagrafica']) {
                                    echo "selected=\"selected\"";
} ?> >
								<?php echo "Tipo anagrafica: " . $field['descr_tipo']; ?>
							</option>
						<?php
} ?>
					</select>

					&nbsp;
					<input name="ragione_sociale" type="text" style="width: 38em;" maxlength="80"
						placeholder="Ragione Sociale"
						value="<?php if (isset($_POST['ragione_sociale'])) {
                            echo $_POST['ragione_sociale'];
} ?>" />
				</td>
			</tr>
			<tr>
				<td>
					<input name="cognome" type="text" style="width: 26em;" maxlength="35"
						placeholder="Cognome"
						value="<?php if (isset($_POST['cognome'])) {
                            echo $_POST['cognome'];
} ?>" />

					&nbsp;
					<input name="nome" type="text" style="width: 26em;" maxlength="35"
						placeholder="Nome"
						value="<?php if (isset($_POST['nome'])) {
                            echo $_POST['nome'];
} ?>" />
				</td>
			</tr>
			<tr>
				<td>
					<input name="indirizzo" type="text" style="width: 42em;" maxlength="100"
						placeholder="Indirizzo"
						value="<?php if (isset($_POST['indirizzo'])) {
                            echo $_POST['indirizzo'];
} ?>" />

					<input name="civico" type="text" style="width: 10.5em;"
						placeholder="Civico"
						maxlength="12" value="<?php if (isset($_POST['civico'])) {
                            echo $_POST['civico'];
} ?>" />
				</td>
			</tr>
			<tr>
				<td>
					<select name="stato" style="width: 11.3em;">
						<!--<option value="Austria">Stato: Austria</option>
						<option value="Germania">Stato: Germania</option>!-->
						<option value="Italia">Stato: Italia</option>
					</select>

					<select name="id_prov"
						onchange="Javascript: posta('0', '<?php echo $_SERVER['REQUEST_URI']; ?>'); void(0);">
						<option value="-">Seleziona provincia</option>
						<option value="-"></option>
						<?php foreach ($Province as $key => $field) {
?>
							<option value="<?php echo $field['id_provincia']; ?>"
								<?php if (isset($_POST['id_prov'])
                                    && ($_POST['id_prov'] == $field['id_provincia'])) {
                                     echo "selected=\"selected\"";
} ?> >
								<?php echo $field['targa']; ?>
							</option>
						<?php
} ?>
					</select>

					<select name="id_citta"
						onchange="Javascript: posta('0', '<?php echo $_SERVER['REQUEST_URI']; ?>'); void(0);">
						<option value="-">Seleziona comune</option>
						<option value="-"></option>
						<?php foreach ($Comuni as $key => $field) {
?>
							<option value="<?php echo $field['id_citta']; ?>"
								<?php if (isset($_POST['id_citta'])
                                    && ($_POST['id_citta'] == $field['id_citta'])) {
                                     echo "selected=\"selected\"";
} ?> >
								<?php echo $field['citta']; ?>
							</option>
						<?php
} ?>
					</select>

					<input name="cap" type="text" style="width: 6.3em;" title="Cap"
						placeholder="Cap" required pattern="\w+"
						maxlength="20" value="<?php if (isset($_POST['cap'])) {
                            echo $_POST['cap'];
} ?>" />
				</td>
			</tr>
			<tr>
				<td>
					<input name="telefono1" type="text" maxlength="20" style="width: 17em;"
						placeholder="Telefono"
						value="<?php if (isset($_POST['telefono1'])) {
                            echo $_POST['tel'];
} ?>" />

					<input name="fax1" type="text" maxlength="20" style="width: 17em;"
						placeholder="Fax"
						value="<?php if (isset($_POST['fax'])) {
                            echo $_POST['fax1'];
} ?>" />

					<input name="cellulare1" type="text" maxlength="20" style="width: 17.7em;"
						placeholder="Cellulare"
						value="<?php if (isset($_POST['cellulare1'])) {
                            echo $_POST['cellulare1'];
} ?>" />
				</td>
			</tr>
			<tr>
				<td>
					<input name="piva" type="text" maxlength="25" style="width: 18em;"
						title="Partita Iva o Codice Fiscale" required pattern="\w+"
						placeholder="Partita Iva o Codice Fiscale"
						value="<?php if (isset($_POST['piva'])) {
                            echo $_POST['piva'];
} ?>" />


				<input name="mail" type="text" maxlength="70" style="width: 34.5em;"
					placeholder="E-Mail"
					value="<?php if (isset($_POST['mail'])) {
                                    echo $_POST['mail'];
} ?>" />
				</td>
			</tr>
			<tr>
				<td>
					<input name="contatto" type="text" maxlength="60" style="width: 33em;"
						placeholder="Contatto"
						value="<?php if (isset($_POST['contatto'])) {
                            echo $_POST['contatto'];
} ?>" />

					<select name="id_pagamento" size="1" style="width: 20em;"
						required pattern="\d+"
						title="Seleziona la modalità di pagamento">
						<option value="-">Seleziona modalit&agrave; di pagamento</option>
						<?php if (count(mod_pag) != 0) {
                            foreach ($mod_pag as $key => $field) {
                                ?>
								<option value="<?php echo $mod_pag[$key]['id_pagamento']; ?>"
									<?php if (isset($_POST['id_pagamento']) && ($_POST['id_pagamento'] == $mod_pag[$key]['id_pagamento'])) {
                                        echo "selected=\"selected\"";
} ?>>
									<?php echo $mod_pag[$key]['pagamento']; ?>
								</option>
							<?php
                            }
} ?>
					</select>
				</td>
			</tr>

			<tr>

				<td>
					<input name="banca" type="text" style="width: 29em;" maxlength="50"
						placeholder="Nome Banca"
						value="<?php if (isset($_POST['banca'])) {
                            echo $_POST['banca'];
} ?>" />

					&nbsp;
					<input name="iban" type="text" style="width: 23em;" maxlength="23"
						placeholder="Codice Iban"
							value="<?php if (isset($_POST['iban'])) {
                                echo $_POST['iban'];
} ?>" />
				</td>
			</tr>

			<tr>
				<td colspan="5">
					<textarea name="note" rows="4" cols="57"
						style="width: 53em; height: 4em;"
						placeholder="Note" ><?php if (isset($_POST['note'])) {
                            echo $_POST['note'];
} ?></textarea>
				</td>
			</tr>
		</table>
	</form>
</div>

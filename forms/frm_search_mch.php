<?php error_reporting(E_ALL);
	$tot_res = -1;
	/*
	 *      frm_ricerca.php
	 *
	 *      Copyright 2008 Claudio Giordano <claudio.giordano@autistici.org>
	 *
	 *      This program is free software; you can redistribute it and/or modify
	 *      it under the terms of the GNU General Public License as published by
	 *      the Free Software Foundation; either version 3 of the License, or
	 *      (at your option) any later version.
	 *
	 *      This program is distributed in the hope that it will be useful,
	 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
	 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	 *      GNU General Public License for more details.
	 *
	 *      You should have received a copy of the GNU General Public License
	 *      along with this program; if not, write to the Free Software
	 *      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
	 *      MA 02110-1301, USA.
   */

	if (isset($_GET['del']) && is_numeric($_GET['del']))
	{
		$LogString = "Eliminata macchina con id " . $_GET['del'] . ", ";
		$LogString .= " dall'utente " . $_SESSION['desc_user']
			. " (" . $_SESSION['tipo'] . "), ";

		$mch = $d->GetRows ("*", "tab_dett_requests", "id_macchina = '"
			. $_GET['del'] . "'");
		if (count($mch) > 0)
		{
			$LogString .= " che era associata alla request '"
				. $mch[0]['id_request'] . "', ";
		}

		$mch = $d->GetRows ("*", "tab_macchine", "id_macchina = '"
			. $_GET['del'] . "'");

		$LogString .= " DUMP della macchina: ('" . join("', '", $mch[0]) . "')";

		$d->SessionLog($LogString);

		$Status = $d->DeleteRows ("tab_dett_requests", "id_macchina = '"
			. $_GET['del'] . "'");
		$Status = $d->DeleteRows ("tab_macchine", "id_macchina = '"
			. $_GET['del'] . "'");
	}

	/*
	 * Eseguo l'update della destinazione (dest_fil) per le macchine assegnate,
	 * se le macchine hanno id_stato_mch != 7 ovvero non sono state passate a maintenance
	 */
	if (empty($_POST))
	{
		$Assegnate = $d->GetRows ("*", "view_macchine_assegnate");

		foreach ($Assegnate as $key => $field)
		{
			if (($field['id_stato_mch'] != "7")
				|| ($field['id_stato_mch'] != "10"))
			{
				/*
				 * eseguo l'update della destinazione della macchina
				 * se l'id dello stato è != 7 ovvero "maintenance"
				 * se l'id dello stato è != 10 ovvero "commessa"
				 */
				$Valori = array('dest_fil' => $field['den']);

				if (($field['provenienza'] == "FORNITORE")
				&& ($field['id_stato_mch'] != "7")
				&& ($field['id_stato_mch'] != "10")
				&& ($field['tipo_mch'] != "Server"))
				{
					/*
					 * eseguo l'update dello stato della macchina con
					 * id_stato_mch= '4' ovvero "consegnata" se la macchina
					 * ha provenienza fornitore:
					 */
					$Valori['id_stato_mch'] = '4';
				}

				$Status = $d->UpdateRow ($Valori, "", "tab_macchine",
					"id_macchina = '" . $field['id_macchina'] . "'");
			}
		}

		unset($Assegnate);
		unset($Status);
		$d->SessionLog("Eseguito Update dest_fil macchine in "
			. (time()-$start) . " secondi.");
	}


	//ShowWaitDiv();

/*
	$OpenerUrl = "home.php?act=rollout";
	if ($_GET['tomod'])
	{
		$OpenerUrl .= "&tomod=" . $_GET['tomod'];
	}
*/
	$TipoMch 			= $d->GetRows ("*", "tab_tipi_mch", "", "tipo_mch");
	$Magazzini 		= $d->GetRows ("*", "tab_magazzino", "", "descr");
	$Provenienza 	= $d->GetRows ("distinct(provenienza)", "tab_macchine", "", "provenienza");
	$Stato 				= $d->GetRows ("*", "tab_stato_mch", "", "stato_mch");



		// Se è stata fatta una ricerca:
		if (!empty($_POST))
		{
			// Se il valore cercato è ==  "" setto il valore cercato = "%":
			if (trim($_POST['serial_asset']) == "")
			{
				$_POST['serial_asset'] = "%";
			}

			if (trim($_POST['marca_modello']) == "")
			{
				$_POST['marca_modello'] = "%";
			}

			if (trim($_POST['dest_fil']) == "")
			{
				$_POST['dest_fil'] = "%";
			}

			if (trim($_POST['id_macchina']) == "")
			{
				$_POST['id_macchina'] = "%";
			}

			$Where = "(marca LIKE '%" . $_POST['marca_modello']
				. "%' OR modello LIKE '%" . $_POST['marca_modello'] . "%')";

			$Where .= " AND (serial LIKE '%" . $_POST['serial_asset']
				. "%' OR asset LIKE '%" . $_POST['serial_asset'] . "%')";

			$Where .= " AND dest_fil LIKE '%" . $_POST['dest_fil'] . "%'";
				//. "%' OR asset LIKE '%" . $_POST['serial_asset'] . "%')";
				// Evita la selezione di macchine già utilizzate
				//. " AND (id_macchina not in (select id_macchina from tab_dett_requests)"

				/*
				 * Rende selezionabili le macchine gi&agrave; utilizzate,
				 * solo nel caso abbiano id = '1' ovvero i server riciclati
				 * dalle filiali
				 */
				//. " OR id_tipo_mch = '1')";
			/*
			 * Evita la selezione di macchine di altre città,
			 * fatta eccezione per RM / SUN / admin che necessitano
			 * di accedere in tutte le aree
			 */
			/*if ($_SESSION['id_tipo'] != "1" /* Admin */
				/* && $_SESSION['id_tipo'] != "7" /* RM */
				/* && $_SESSION['id_tipo'] != "5" /* SUN */ //)
			/* {
				$Where .= " AND id_magazzino = '" . $_SESSION['id_city'] . "'";
			} */

			if ($_POST['provenienza'] != "%")
			{
				$Where .= " AND provenienza LIKE '" . $_POST['provenienza'] . "'";
			}

			if ($_POST['id_tipo_mch'] != "%")
			{
				$Where .= " AND id_tipo_mch LIKE '" . $_POST['id_tipo_mch'] . "'";
			}

			if ($_POST['id_magazzino'] != "%")
			{
				$Where .= " AND id_magazzino LIKE '" . $_POST['id_magazzino'] . "'";
			}

			if ($_POST['id_stato_mch'] != "%")
			{
				$Where .= " AND id_stato_mch LIKE '" . $_POST['id_stato_mch'] . "'";
			}

			$tot_res = $d->GetRows ("count(id_macchina) as tot", "view_macchine", $Where);
			$tot_res = $tot_res[0]['tot'];
			$order = $d->PageSplit($_GET['page'], $tot_res);
			$Campi = $d->GetRows ("*", "view_macchine", $Where, "", "tipo_mch, dest_fil $order");

			$_SESSION['where'] = $Where;
			$_SESSION['order'] = $order;

			if ((trim($_POST['multi_serial']) != "") && ($_POST['multi_serial'] != "%"))
			{
				$tmp_array = explode("\n", $_POST['multi_serial']);
				$string = $d->MultiString($_POST['multi_serial'], "\n", '\', \'');

				$Where = "asset IN ('$string') OR serial IN ('$string')";

				$tot_res = $d->GetRows ("count(id_macchina) as tot", "view_macchine", $Where);
				$tot_res = $tot_res[0]['tot'];
				$order = $d->PageSplit($_GET['page'], $tot_res);
				$Campi = $d->GetRows ("*", "view_macchine", $Where, "", "tipo_mch, dest_fil $order");

				$_SESSION['where'] = $Where;
				$_SESSION['order'] = $order;
			}

			if ((trim($_POST['id_macchina']) != "") && ($_POST['id_macchina'] != "%"))
			{
				$tmp_array = explode("\n", $_POST['id_macchina']);
				$string = $d->MultiString($_POST['id_macchina'], "\n", '\', \'');

				$Where = "id_macchina IN ('$string')";

				$tot_res = $d->GetRows ("count(id_macchina) as tot", "view_macchine", $Where);
				$tot_res = $tot_res[0]['tot'];
				$order = $d->PageSplit($_GET['page'], $tot_res);
				$Campi = $d->GetRows ("*", "view_macchine", $Where, "", "tipo_mch, dest_fil $order", 0);

				$_SESSION['where'] = $Where;
				$_SESSION['order'] = $order;
			}
		}

		if (isset($_GET['rex']) && ($_GET['rex'] == "1"))
		{
			// ri assegno il precedente array con i risultati:
			$where = $_SESSION['where'];
			$order = $_SESSION['order'];
			$tot_res = $d->GetRows ("count(id_macchina) as tot", "view_macchine", $where);
			$tot_res = $tot_res[0]['tot'];
			$Campi = $d->GetRows ("*", "view_macchine", $where, "tipo_mch, dest_fil $order");
		}
?>

<div id="search-form">
	<form method="post" action="<?php echo $_SERVER['REQUEST_URI']; ?>">
		<table class="form" width="100%">
			<tr>
				<th colspan="4">Ricerca Macchine:</th>
			</tr>
			<tr>
				<td colspan="4">
					<select name="provenienza" title="Provenienza macchina"
						onchange="Javascript: posta('0', '<?php echo $_SERVER['REQUEST_URI']; ?>');" >
						<option value="%">Tutte le provenienze</option>
						<?php foreach ($Provenienza as $key => $field) { ?>
							<option value="<?php echo $field['provenienza']; ?>"
								<?php if (isset($_POST['provenienza'])
									&& ($_POST['provenienza'] == $field['provenienza'])) {
										echo "selected=\"selected\""; } ?> >
								<?php echo ucwords(strtolower($field['provenienza'])); ?>
							</option>
						<?php } ?>
					</select>

					<select name="id_tipo_mch" title="Tipo di macchina"
						onchange="Javascript: posta('0', '<?php echo $_SERVER['REQUEST_URI']; ?>');" >
						<option value="%">Tutti i tipi di macchine</option>
						<?php foreach ($TipoMch as $key => $field) { ?>
							<option value="<?php echo $field['id_tipo_mch']; ?>"
								<?php if (isset($_POST['id_tipo_mch'])
									&& ($_POST['id_tipo_mch'] == $field['id_tipo_mch'])) {
										echo "selected=\"selected\""; } ?> >
								<?php echo $field['tipo_mch']; ?>
							</option>
						<?php } ?>
					</select>

					<select name="id_stato_mch" title="Stato macchina"
						onchange="Javascript: posta('0', '<?php echo $_SERVER['REQUEST_URI']; ?>');" >
						<option value="%">Tutti gli stati</option>
						<?php foreach ($Stato as $key => $field) { ?>
							<option value="<?php echo $field['id_stato_mch']; ?>"
								<?php if (isset($_POST['id_stato_mch'])
									&& ($_POST['id_stato_mch'] == $field['id_stato_mch'])) {
										echo "selected=\"selected\""; } ?> >
								<?php echo $field['stato_mch']; ?>
							</option>
						<?php } ?>
					</select>

					&nbsp;
					<label>Seriale/Asset:</label>
					<input name="serial_asset" type="text" style="width: 12em;"
						onkeyup="Javascript: ValidateField(this, '^[a-zA-Z0-9-]*$');"
						onKeypress="Javascript: return SubmitEnter(this, event);"
						value="<?php if (isset($_POST['serial_asset'])) { echo $_POST['serial_asset']; } ?>" />

					&nbsp;
					<label>Marca/Modello:</label>
					<input name="marca_modello" type="text" style="width: 12em;"
						onKeyPress="return SubmitEnter(this,event);"
						value="<?php if (isset($_POST['marca_modello'])) { echo $_POST['marca_modello']; } ?>" />
				</td>
			</tr>

			<tr>
				<td>
					<select name="id_magazzino" title="Magazzino"
						onchange="Javascript: posta('0', '<?php echo $_SERVER['REQUEST_URI']; ?>');" >
						<option value="%">Tutti i magazzini</option>
						<?php foreach ($Magazzini as $key => $field) { ?>
							<option value="<?php echo $field['id_magazzino']; ?>"
								<?php if (isset($_POST['id_magazzino'])
									&& ($_POST['id_magazzino'] == $field['id_magazzino'])) {
										echo "selected=\"selected\""; } ?> >
								<?php echo $field['descr']; ?>
							</option>
						<?php } ?>
					</select>

					&nbsp;
					<label>ID Prodotto:</label>
					<textarea name="id_macchina" style="height: 1.5em;" rows="1"><?php if (isset($_POST['id_macchina'])) { echo $_POST['id_macchina']; } ?></textarea>

					&nbsp;
					<label>Destinazione:</label>
					<input name="dest_fil" type="text"
						onKeyPress="return SubmitEnter(this,event);"
						value="<?php if (isset($_POST['dest_fil'])) { echo $_POST['dest_fil']; } ?>" />

					&nbsp;
					<label>Seriale/Asset Multiplo:</label>
					<textarea name="multi_serial" style="height: 1.5em;" rows="1"><?php if (isset($_POST['multi_serial'])) { echo $_POST['multi_serial']; } ?></textarea>

				</td>
			</tr>

			<?php $d->ShowResultBar($tot_res, "home.php?act=search_mch"); ?>
		</table>
	</form>
</div>

<?php if ($tot_res > 0) { ?>
	<div id="search-result-2r">
		<form method="post" action="<?php echo $_SERVER['REQUEST_URI']; ?>">
			<table style="width: 100%;" class="dettaglio">
				<tr>
					<th><label>Provenienza:</label></th>
					<th><label>Tipo:</label></th>
					<th><label>Marca:</label></th>
					<th><label>Modello:</label></th>
					<th><label>Serial:</label></th>
					<th><label>Asset:</label></th>
					<th><label>Mac:</label></th>
					<th><label>Class.Hw:</label></th>
					<th><label>Magazzino:</label></th>
					<th><label>Destinazione:</label></th>
					<th><label>Stato:</label></th>
					<th>
						<a onclick="Javascript: if (CheckSelected('multisel') == true) {
							ShowFormDiv('frm_anag_macchina.php?tomod=multi',
							'200px', '200px', '770px', '320px', 'popup'); void(0); }else{ alert('Nessuna voce selezionata.'); }"
							 title="Modifica Selezionati">
							<img src="/Images/Links/edit.png" alt="edit" />
						</a>

						<a onclick="Javascript: SelectAll('multisel', 'comm'); void(0);"
							title="Commuta Selezione" >
							<img src="/Images/Links/select-all.png" alt="select-all" />
						</a>

						<!--
						<a onclick="Javascript: SelectAll('multisel', 'sel'); void(0);"
							title="Seleziona Tutti" >
							<img src="/Images/Links/select-all.png" alt="select-all" />
						</a>

						<a onclick="Javascript: SelectAll('multisel', 'unsel'); void(0);"
							title="Deseleziona Tutti" >
							<img src="/Images/Links/select-all.png" alt="select-all" />
						</a>
						!-->
					</th>
				</tr>
				<tr><th colspan="15"><hr /></th></tr>
				<?php foreach ($Campi as $key => $field) { ?>
					<tr <?php if ($key % 2) { echo 'id="res-row"'; } ?> >
						<td>
							<?php echo ucwords(strtolower($field['provenienza'])); ?>
						</td>
						<td>
							<?php echo $field['tipo_mch']; ?>
						</td>
						<td>
							<?php echo $field['marca']; ?>
						</td>
						<td>
							<?php echo $field['modello']; ?>
						</td>

						<td>
							<?php echo $field['serial']; ?>
						</td>
						<td>
							<?php echo $field['asset']; ?>
						</td>
						<td>
							<?php echo $field['mac']; ?>
						</td>
						<td>
							<?php echo $field['class_hw']; ?>
						</td>
						<td>
							<?php echo $field['magazzino']; ?>
						</td>
						<td>
							<?php echo $field['dest_fil']; ?>
						</td>
						<td>
							<?php echo $field['stato_mch']; ?>
						</td>
						<td>
							<a onclick="Javascript: ShowFormDiv('frm_anag_macchina.php?tomod=<?php
								echo $field['id_macchina']; ?>',
								'200px', '200px', '770px', '320px', 'popup'); void(0);"
								 title="Modifica Macchina">
								<img src="/Images/Links/edit.png" alt="edit" />
							</a>

							<input name="multisel" type="checkbox" style="vertical-align: middle;"
								value="<?php echo $field['id_macchina']; ?>" />

							<a onclick="Javascript: go_conf2('*** ATTENZIONE ***\n\nNon è possibile annullare l\'operazione,\nConfermi l\'eliminazione?',
									'<?php echo "home.php?act=search_mch&amp;page=first&amp;&amp;rex=1&amp;del="
										. $field['id_macchina']; ?>'); void(0);">
									<img src="/Images/Links/del.png" alt="Elimina" title="Elimina" />
								</a>
						</td>
					</tr>
				<?php } ?>
				<tr><th colspan="15"><hr /></th></tr>
			</table>
		</form>
	</div>
<?php } ?>

<?php ob_end_clean(); error_reporting(~E_ALL);

	//~ tipo 1: 1 loghi in angolo dati altro angolo
	//~ tipo 2: 2 loghi agli angoli dati al centro,

	define ( "eur", chr(128) );
	require_once("fpdf/fpdf.php");
	require_once("connection.php");

	// Dati Ditta:
	$Campi 					= $d->GetRows("*", "tab_config", "opt LIKE 'ditta%'", "", "opt", 1);
	$Ditta 					= array();
	$tot_imponibile = 0;
	$tot_iva 				= 0;


	foreach ($Campi as $key => $field)
	{
		$Ditta[$field['opt']] = $field['val'];
	}

#	echo "<pre>";
#		print_r($Ditta);
#	echo "</pre>";

	$temp_var = $d->GetRows("Comune", "tab_comuni", "ID = '"
		. $Ditta['ditta_id_citta'] . "'", "", "", 1);
	$Ditta['ditta_citta'] = $temp_var[0]['Comune'];

	$temp_var = $d->GetRows("Targa", "tab_province", "ID = '"
		. $Ditta['ditta_id_prov'] . "'", "", "", 1);
	$Ditta['ditta_prov'] = $temp_var[0]['Targa'];

	$Ditta['ditta_str_luogo']	= $Ditta['ditta_cap'] . " " . $Ditta['ditta_citta']
		. " (" . $Ditta['ditta_prov'] . ")";

	$Ditta['ditta_str_contatti'] 	= "Tel: " . $Ditta['ditta_telefono']
		. " - Fax: " . $Ditta['ditta_fax'] . " - E-Mail: " . $Ditta['ditta_mail'];

	// Dati Documento:
	$DatiDoc = $d->GetRows("*", "view_documenti d",
		"id_documento = '" . $_GET['id_doc'] . "'", "", "");

	// Dettaglio Documento:
	$DettaglioDoc = $d->GetRows("*", "tab_dett_documenti",
		"id_documento = '" . $_GET['id_doc'] . "'", "", "");
	
#	echo "<pre> DettaglioDoc";
#		print_r($DettaglioDoc);
#	echo "</pre>";

	class PDF extends FPDF
	{
		// Page header
		function Header()
		{
			global $d;
			global $Ditta;
			global $DatiDoc;

			Switch ($Ditta['ditta_layout_doc'])
			{
				case 1:
					
					if ($Ditta['ditta_doc_show_logo'] == 0)
					{
						$this->Image($Ditta['ditta_logo'], 5, 13, 49, 20);
						$this->Line(58, 10, 58, 30);

						$this->SetXY(59, 15);
					}else{
						$this->SetXY(5, 15);
					}

					$this->SetFont('Arial', 'I', 8);
					$this->Cell(142, 5, $Ditta['ditta_rag_soc'] . " - "
						. $Ditta['ditta_indirizzo'] . " - "
						. $Ditta['ditta_str_luogo'], 0, 0, 'L');

					if ($Ditta['ditta_doc_show_logo'] == 0)
					{
						$this->SetXY(59, 19);
					}else{
						$this->SetXY(5, 19);
					}

					$this->SetFont('Arial', 'I', 8);
					$this->Cell(142, 5, "P.Iva / C.F. "
					. $Ditta['ditta_piva'] . " - " . $Ditta['ditta_str_contatti'], 0, 0, 'L');

					if ($Ditta['ditta_doc_show_logo'] == 0)
					{
						$this->SetXY(59, 23);
					}else{
						$this->SetXY(5, 23);
					}

					$this->SetFont('Arial', 'I', 8);
					$this->Cell(142, 4, "Iban " . $Ditta['ditta_iban'], 0, 0, 'L');

					$this->Line(5, 35, 205, 35);
					$this->Ln(18);

					// Dati documento
					$this->SetX(5);
					$this->SetFont('Arial', '', 8);
					$this->Cell(40, 5, "Tipo documento:", 'B', '', '');
					$this->Cell(40, 5, "Numero:", 'B', '', '');
					$this->Cell(40, 5, "Data:", 'B', '', '');
					$this->Cell(80, 5, "Spett.le:", 'B', '', '');
					$this->Ln();

					$this->SetX(5);
					$this->SetFont('Arial', 'BI', 8);
					
					if (isset($DatiDoc[0]['tipo_doc']) && ($DatiDoc[0]['tipo_doc'] != ""))
					{
						$this->Cell(40, 5, $DatiDoc[0]['tipo_doc'], 0, '', '');
					}else
					{
						$this->Cell(40, 5, "-", 0, '', '');
					}
					
					if (isset($DatiDoc[0]['cod_documento']) && ($DatiDoc[0]['cod_documento'] != ""))
					{
						$this->Cell(40, 5, $DatiDoc[0]['cod_documento'], 0, '', '');
					}else
					{
						$this->Cell(40, 5, "-", 0, '', '');
					}

					if (isset($DatiDoc[0]['data_doc']) && ($DatiDoc[0]['data_doc'] != ""))
					{
						$this->Cell(40, 5, $d->Inverti_Data($DatiDoc[0]['data_doc']), 0, '', '');
					}else
					{
						$this->Cell(40, 5, "-", 0, '', '');
					}

					if (isset($DatiDoc[0]['ragione_sociale']) && ($DatiDoc[0]['ragione_sociale'] != ""))
					{
						$this->Cell(80, 5, $DatiDoc[0]['ragione_sociale'], 0, '', '');
					}else
					{
						$this->Cell(80, 5, "-", 0, '', '');
					}

					$this->Ln();

					if (isset($DatiDoc[0]['ind']) && ($DatiDoc[0]['ind'] != ""))
					{
						$this->SetX(5);
						$this->Cell(120, 5, '', 0, '', '');
						$this->Cell(80, 5, $DatiDoc[0]['ind'] . ", "
							. $DatiDoc[0]['civ'], 0, '', '');
						$this->Ln();
					}else
					{
						$this->Ln();				
					}

					
					if (isset($DatiDoc[0]['comune']) && ($DatiDoc[0]['Comune'] != ""))
					{
						$this->SetX(5);
						$this->Cell(120, 5, '', 0, '', '');
						$this->Cell(80, 5, $DatiDoc[0]['cap'] . " - "
							. $DatiDoc[0]['Comune'] . " (" . $DatiDoc[0]['Targa']
							. ")", 0, '', '');
						$this->Ln();
					}else
					{
						$this->Ln();				
					}

					if (isset($DatiDoc[0]['pagamento']) && ($DatiDoc[0]['pagamento'] != ""))
					{
						$this->SetX(5);
						$this->SetFont('Arial', '', 8);
						$this->Cell(40, 5, iconv("UTF-8", "ISO-8859-1//IGNORE", "Modalità di pagamento:"), 0, '', '');
					}else{
						$this->SetX(45);
					}
					
					if (isset($DatiDoc[0]['tel']) && ($DatiDoc[0]['tel'] != ""))
					{
						$this->SetFont('Arial', 'BI', 8);
						$this->Cell(80, 5, '', 0, '', '');
						$this->Cell(80, 5, "Tel. " . $DatiDoc[0]['tel'], 0, '', '');
						$this->Ln();
					}else
					{
						$this->Ln();				
					}
					
					if (isset($DatiDoc[0]['pagamento']) && ($DatiDoc[0]['pagamento'] != ""))
					{
						$this->SetX(5);
						$this->Cell(120, 5, $DatiDoc[0]['pagamento'], 'B', '', '');
					}else
					{
						$this->SetX(125);
					}
					
					if (isset($DatiDoc[0]['piva']) && ($DatiDoc[0]['piva'] != ""))
					{
						$this->Cell(80, 5, "P.Iva / C.F. " . $DatiDoc[0]['piva'], 'B', '', '');
						$this->Ln();
					}else{	
						$this->Ln();
					}
					$this->Ln();

				break;
			}
		}

		// Page footer
		function Footer()
		{
			global $d;
			global $Ditta;
			global $DatiDoc;
			global $DettaglioDoc;

			Switch ($Ditta['ditta_layout_doc'])
			{
				case 1:
					//~ Calcolo totali:
					foreach ($DettaglioDoc as $key => $field)
					{

						$temp_iva = $d->GetRows("*", "tab_iva", "id_iva = '"
							. $field['id_iva'] . "'");

						//~ echo "[Debug]: iva: " . $temp_iva[0]['iva'] . " <br />";

						$iva_prodotto = str_replace("1.", "0.", $temp_iva[0]['iva']);
						$tot_imponibile += ($field['qta']*$field['prz']);
						$tot_iva += ($field['qta']*$field['prz']) * $iva_prodotto;
					}

					// Separatore orizzontale totali:
					$this->Line(5, 267, 205, 267);

					$this->SetXY(5, -30);
					$this->SetFont('Arial', 'BI', 8);
					$this->Cell(10, 5, "Note: ", '', '', 'R');
					
					$this->SetFont('Arial', 'I', 8);
					$this->MultiCell(140, 5, stripslashes($DatiDoc[0]['note_doc']), 0);

					$this->SetXY(155, -30);
					$this->Cell(30, 5, 'Totale Imponibile: ', 'R', '', 'R');
					$this->Cell(20, 5, sprintf("%." . PRECISIONE_DECIMALI . "f " . eur, $tot_imponibile), 0, '', 'R');
					$this->Ln();

					$this->SetXY(155, -25);
					$this->Cell(30, 5, 'Sconto: ', 'R', '', 'R');
					$this->Cell(20, 5, sprintf("%." . PRECISIONE_DECIMALI . "f " . eur, 0), 0, '', 'R');
					$this->Ln();

					
					$this->SetXY(155, -20);
					$this->Cell(30, 5, 'Imponibile Scontato: ', 'R', '', 'R');
					$this->Cell(20, 5, sprintf("%." . PRECISIONE_DECIMALI . "f " . eur, 0), 0, '', 'R');
					$this->Ln();

					$this->SetX(155);
					$this->Cell(30, 5, 'Imposta: ', 'R', '', 'R');
					$this->Cell(20, 5, sprintf("%." . PRECISIONE_DECIMALI . "f " . eur, $tot_iva), 0, '', 'R');
					$this->Ln();

					$this->SetX(5);
					$this->Cell(150, 5, "Pagina: " . $this->PageNo() . '/{nb}', 0, '', 'L');
					$this->SetFont('Arial', 'BI', 8);
					$this->Cell(30, 5, 'Totale Euro: ', 'R', '', 'R');
					$this->Cell(20, 5, sprintf("%." . PRECISIONE_DECIMALI . "f " . eur, ($tot_imponibile+$tot_iva)), 0, '', 'R');
					$this->Ln();

				break;
			}
		}
	}

	// Istanzio la classe
	$pdf = new PDF();
	$pdf->AliasNbPages();
	$pdf->SetAutoPageBreak("on", 30);
	$pdf->AddPage($Ditta['ditta_doc_orientamento']);


	$pdf->SetFont('Arial', 'B', 8);

	//~ Intestazione dettaglio documento
	$pdf->SetX(5);

	if ($Ditta['ditta_doc_cod_forn'] == 0)
	{
		$pdf->Cell($Ditta['ditta_doc_cod_forn_width'], 7, 'Cod. Forn:', 'B', '', 'L');
	}

	if ($Ditta['ditta_doc_cod_int'] == 0)
	{
		$pdf->Cell($Ditta['ditta_doc_cod_int_width'], 7, 'Cod. Int:', 'B', '', 'L');
	}

	if ($Ditta['ditta_doc_descr'] == 0)
	{
		$pdf->Cell($Ditta['ditta_doc_desc_width'], 7, 'Descrizione:', 'B', '', 'L');
	}

#	if ($Ditta['ditta_doc_note_art'] == 0)
#	{
#		$pdf->Cell(20, 7, 'Note:', 'B', '', 'L');
#	}

	if ($Ditta['ditta_doc_qta'] == 0)
	{
		$pdf->Cell($Ditta['ditta_doc_qta_width'], 7, iconv("UTF-8", "ISO-8859-1//IGNORE", 'Quantità:'), 'B', '', 'L');
	}

	if ($Ditta['ditta_doc_prz'] == 0)
	{
		$pdf->Cell($Ditta['ditta_doc_prz_width'], 7, 'Prezzo:', 'B', '', 'L');
	}

	if ($Ditta['ditta_doc_sco'] == 0)
	{
		$pdf->Cell($Ditta['ditta_doc_sco_width'], 7, 'Sco:', 'B', '', 'L');
	}
	
	if ($Ditta['ditta_doc_importo'] == 0)
	{
		$pdf->Cell($Ditta['ditta_doc_importo_width'], 7, 'Importo:', 'B', '', 'L');
	}

	if ($Ditta['ditta_doc_iva'] == 0)
	{
		$pdf->Cell($Ditta['ditta_doc_iva_width'], 7, 'Iva:', 'B', '', 'L');
	}

	$pdf->Ln();

	$pdf->SetFont('Arial', '', 8);

	//~ Dettaglio documento
	foreach ($DettaglioDoc as $key => $field)
	{
		$prodotto = array();

		if ($field['id_prodotto'] != 0)
		{
			$temp = $d->GetRows("*", "view_prodotti",
				"id_prodotto = '" . $field['id_prodotto'] . "'");
			$prodotto = $temp[0];
			$prodotto['note_art']		= $field['note'];

#			echo "<pre>";
#				print_r($prodotto);
#			echo "<pre>";
		}else{

			$temp_iva = $d->GetRows("*", "tab_iva",
				"id_iva = '" . $field['id_iva'] . "'");

			$prodotto['cod_forn'] 	= "-";
			$prodotto['cod_int'] 		= "-";
			$prodotto['marca'] 			= "";
			$prodotto['modello'] 		= $field['voce_arb'];
			$prodotto['voce_arb'] 	= $field['voce_arb'];
			$prodotto['iva'] 				= $temp_iva[0]['iva'];
			$prodotto['desc_iva'] 	= $temp_iva[0]['desc_iva'];
			$prodotto['um'] 				= "-";
			$prodotto['note_art']		= $field['note'];
		}

		$prodotto['qta'] 					= $field['qta'];
		$prodotto['prz'] 					= $field['prz'];
		$prodotto['sco'] 					= $field['sco'];

		$pdf->SetX(5);

		if (($Ditta['ditta_doc_note_art'] == 0) && ($prodotto['note_art'] != ""))
		{
			$BORDER = "T";
		}else{
			$BORDER = "B";
		}
		
		if ($Ditta['ditta_doc_cod_forn'] == 0)
		{
			$pdf->Cell($Ditta['ditta_doc_cod_forn_width'], 7, $prodotto['cod_int'], $BORDER, '', 'L');
		}

		if ($Ditta['ditta_doc_cod_int'] == 0)
		{
			$pdf->Cell($Ditta['ditta_doc_cod_int_width'], 7, $prodotto['cod_int'], $BORDER, '', 'L');
		}

		if ($Ditta['ditta_doc_descr'] == 0)
		{
			$OldX = $pdf->GetX();
			$pdf->MultiCell($Ditta['ditta_doc_desc_width'], 7, $prodotto['marca'] . " " . $prodotto['modello'], $BORDER);
			
			if (strlen($prodotto['marca'] . " " . $prodotto['modello']) > 90)
			{
				$pdf->SetXY($OldX+$Ditta['ditta_doc_desc_width'], $pdf->GetY() - 14);
			}else{
				$pdf->SetXY($OldX+$Ditta['ditta_doc_desc_width'], $pdf->GetY() - 7);
			}
		}

#		if ($Ditta['ditta_doc_note_art'] == 0)
#		{
#			$pdf->Cell(20, 7, $prodotto['note'], 'B', '', 'L');
#		}

		if ($Ditta['ditta_doc_qta'] == 0)
		{
			if ($prodotto['um'] != "Pz")
			{
				$pdf->Cell($Ditta['ditta_doc_qta_width'], 7, sprintf("%." . PRECISIONE_DECIMALI . "f %s", $prodotto['qta'], $prodotto['um']), $BORDER, '', 'R');
			}else{
				$pdf->Cell($Ditta['ditta_doc_qta_width'], 7, sprintf("%." . PRECISIONE_DECIMALI . "d %s", $prodotto['qta'], $prodotto['um']), $BORDER, '', 'R');
			}
		}
		
		if ($Ditta['ditta_doc_prz'] == 0)
		{
			$pdf->Cell($Ditta['ditta_doc_prz_width'], 7, sprintf("%." . PRECISIONE_DECIMALI . "f", $prodotto['prz']), $BORDER, '', 'R');
		}

		if ($Ditta['ditta_doc_sco'] == 0)
		{
			$pdf->Cell($Ditta['ditta_doc_sco_width'], 7, sprintf("%." . PRECISIONE_DECIMALI . "f%%", $prodotto['sco']), $BORDER, '', 'R');
		}

		if ($Ditta['ditta_doc_importo'] == 0)
		{
			$pdf->Cell($Ditta['ditta_doc_importo_width'], 7, sprintf("%." . PRECISIONE_DECIMALI . "f", ($prodotto['qta']*$prodotto['prz'])), $BORDER, '', 'R');
		}

		if ($Ditta['ditta_doc_iva'] == 0)
		{
			$pdf->Cell($Ditta['ditta_doc_iva_width'], 7, $prodotto['desc_iva'], $BORDER, '', 'R');
		}

		$pdf->Ln();

		if (($Ditta['ditta_doc_note_art'] == 0) && ($prodotto['note_art'] != ""))
		{
			//~ $pdf->Ln();
			if (strlen($prodotto['marca'] . " " . $prodotto['modello']) > 90)
			{
				$pdf->SetXY(5, $pdf->GetY() + 7);
			}else{
				$pdf->SetX(5);
			}
			$pdf->MultiCell($Ditta['ditta_doc_note_art_width'], 7, "Note: " . $prodotto['note_art'], 'B');		
			//~ $pdf->Ln();
		}
	}

	$pdf->Output($DatiDoc[0]['tipo_doc'] . "_"
		. $DatiDoc[0]['cod_documento'] . ".pdf", 'I');

?>

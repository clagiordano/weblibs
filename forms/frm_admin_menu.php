<?php
/*
 * Amministrazione Menu
 *
 * Copyright 2008 Claudio Giordano <claudio.giordano@autistici.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 */

	if (!isset($_GET['tomod'])) { $_GET['tomod'] = ""; }

	if (isset($_GET['del']))
	{
		$Status = $d->DeleteRows ("tab_menu", "id_menu = '" . $_GET['del'] . "'");
	}

	if (isset($_GET['salva']))
	{
		$errori = "";
		$Esclusioni= array("add_menu" => "Salva");

		if (trim($_POST['titolo']) == "")
		{
			$errori .= "Errore, non è stata impostata una voce per il menu.<br />";
		}
		if (trim($_POST['link']) == "")
		{
			$errori .= "Errore, non è stata impostato il link alla pagina.<br />";
		}
		if (trim($_POST['gruppo']) == "")
		{
			$errori .= "Errore, non è stata impostato un gruppo per il menu.<br />";
		}
		if (trim($_POST['perm']) == "")
		{
			$errori .= "Errore, non sono stati impostati i permessi della pagina.<br />";
		}
		if (trim($_POST['act']) == "")
		{
			$errori .= "Errore, non è stata impostata l'azione per il link.<br />";
		}
		if (trim($_POST['page']) == "")
		{
			$errori .= "Errore, non è stata impostata una pagina da visualizzare.<br />";
		}

		if (($_GET['salva'] == "salva") && ($errori == ""))
		{
			$Campi = $d->SaveRow ($_POST, $Esclusioni, "tab_menu");
		}

		if (($_GET['salva'] == "modifica") && ($errori == ""))
		{
			$Campi = $d->UpdateRow ($_POST, $Esclusioni, "tab_menu", "id_menu = '"
				.$_GET['tomod'] . "'");
		}

		if ($errori != "")
		{
			?><label class="err"><?php echo $errori; ?></label><?php
		}else{ ?>
			<script type="text/javascript">
				document.location.href="?act=adm_menu"
			</script>
		<?php }
	}

	if (is_numeric($_GET['tomod']))
	{
		$menu_mod = $d->GetRows ("*", "tab_menu", "id_menu = '".$_GET['tomod']."'");
	}

	if (isset($_GET['idt']))
	{
		$campo 			= explode(";", $_GET['idt']);
		$id_menu 		= $campo[0];
		$id_group 	= $campo[1];
		$st_prec 		= $_GET['st']; // Stato precedente (0 attivato)
		$menurow 		= $d->GetRows ("*", "tab_menu", "id_menu='$id_menu'", "", "", 1);
		$perm_str 	= $menurow[0]['perm'];
		$perm_array = explode(";", $perm_str);

		array_pop($perm_array);

		if ($st_prec == 0)
		{
			$perm_str_new = "";
			// rimuovi l'id dalla stringa
			$st_new = 1;
			foreach ($perm_array as $key => $field)
			{
				if ($field != $id_group)
				{
					$perm_str_new .= $field . ";";
				}
			}
		}else{
			// aggiungi l'id alla stringa
			$st_new 			= 0;
			$perm_str 		.= $id_group . ";";
			$perm_array 	= explode(";", $perm_str);
			array_pop($perm_array);
			asort($perm_array);
			$perm_str_new = join($perm_array, ";");
			$perm_str_new .= ";";
		}

		$st_update = $d->UpdateRow (array('perm' => "$perm_str_new" ),
			array(), "tab_menu", "id_menu = '$id_menu'");
		//~ echo "Debug => $status <br />";
	}

	$Gruppi = $d->GetRows ("*", "tab_tipi", "", "id_tipo");
	$Menu = $d->GetRows ("*", "tab_menu", "id_menu != ''", "",
		"gruppo, ordine, titolo_pag");

	?>

<section id="search-form">
		<form method="post" action="#">
			<table style="width: 100%;" class="form">

				<?php $d->ShowEditBar("Voce Menu:", $_GET['tomod'],
					"new,save", $_GET['act'], 0); ?>

				<tr>
					<td style="text-align: left;">
						<input type="text" name="titolo" maxlength="30" size="20"
							placeholder="Titolo" title="Titolo"
							value="<?php if (isset($menu_mod[0]['titolo']))
							{ echo $menu_mod[0]['titolo']; } ?>" />

						&nbsp;
						<input type="text" name="titolo_pag" maxlength="50"
							size="40" placeholder="Titolo Pagina" title="Titolo Pagina"
							value="<?php if (isset($menu_mod[0]['titolo_pag']))
							{ echo $menu_mod[0]['titolo_pag']; } ?>" />

						&nbsp;
						<input type="text" name="titolo_gruppo" maxlength="50"
							style="width: 18.5em;" placeholder="Titolo Gruppo"
							title="Titolo Gruppo"
							value="<?php if (isset($menu_mod[0]['titolo_gruppo']))
							{ echo $menu_mod[0]['titolo_gruppo']; } ?>" />

						&nbsp;
						<input type="text" name="link" maxlength="200"
							placeholder="Link" style="width: 20em;" title="Link"
							value="<?php if (isset($menu_mod[0]['link']))
							{ echo $menu_mod[0]['link']; } ?>" />
					</td>
				</tr>

				<tr>
					<td>
						<input type="text" name="gruppo" maxlength="20" size="20"
							placeholder="Gruppo" title="Gruppo"
							value="<?php if (isset($menu_mod[0]['gruppo']))
							{ echo $menu_mod[0]['gruppo']; } ?>" />

						&nbsp;
						<input type="text" name="perm" size="10" placeholder="Permessi"
							title="Permessi"
							value="<?php if (isset($menu_mod[0]['perm']))
							{ echo $menu_mod[0]['perm']; } ?>" />

						&nbsp;
						<input type="text" name="tip" maxlength="50" size="20"
							placeholder="Suggerimento" title="Suggerimento"
							value="<?php if (isset($menu_mod[0]['tip']))
							{ echo $menu_mod[0]['tip']; } ?>" />

						&nbsp;
						<input name="act" type="text" maxlength="30" size="20"
							placeholder="Azione" title="Azione"
							value="<?php if (isset($menu_mod[0]['act']))
							{ echo $menu_mod[0]['act']; } ?>" />

						&nbsp;
						<input name="ordine" type="text" maxlength="30" size="10"
							placeholder="Ordine" title="Ordine"
							value="<?php if (isset($menu_mod[0]['ordine']))
							{ echo $menu_mod[0]['ordine']; } ?>" />

						&nbsp;
						<input name="page" type="text" maxlength="100"
							style="width: 20em;" placeholder="Percorso pagina"
							title="Percorso pagina"
							value="<?php if (isset($menu_mod[0]['page']))
							{ echo $menu_mod[0]['page']; } ?>" />
					</td>
				</tr>
				<tr>
					<td style="text-align: right;">
						<?php if (isset($st_update)) { echo $st_update; } ?>
					</td>
				</tr>
			</table>
		</form>
	</section>

	<?php if (count($Menu) != 0) { ?>
		<div id="search-result-6r">
			<form method="post" action="<?php echo $_SERVER['REQUEST_URI']; ?>">
				<table style="width: 100%;" class="dettaglio">
					<tr>
						<th><label>Titolo Pagina:</label></th>
						<th><label>Suggerimento:</label></th>
						<th><label>Permessi:</label></th>
						<th><label>Gruppo:</label></th>
						<th><label>Titolo Gruppo:</label></th>
						<th><label>Action:</label></th>
						<th><label>Ordine:</label></th>
						<th><br /></th>
					</tr>
					<tr><th colspan="8"><hr /></th></tr>
					<?php foreach ($Menu as $key => $fieldm) {
							if ($fieldm['perm'] == "")
							{
								$style = " class=\"err\"";
							}else{
								$style = " class=\"ok\"";
							} ?>
						<tr>
							<td >
								<a onclick="Javascript: go('?act=adm_menu&amp;tomod=<?php
									echo $fieldm['id_menu']; ?>'); void(0);"
									title="Modifica Voce" <?php echo $style; ?>>
									<?php echo $fieldm['titolo_pag']; ?>
								</a>
							</td>
							<td><?php echo $fieldm['tip']; ?></td>
							<td>
								<?php foreach ($Gruppi as $key_group => $field) {
									$flag_ck = $d->CheckAuth($field['id_tipo'], $fieldm['perm']);
									$url = "?act=adm_menu&amp;idt="
										. $fieldm['id_menu'] . ";" . $field['id_tipo']
										. "&amp;st=" . $flag_ck[1]; ?>
									<input name="ck<?php echo $fieldm['id_menu'].$field['id_tipo']; ?>"
										type="checkbox"
										title="Visibile al gruppo <?php echo $field['tipo']; ?>"
										<?php if (isset($flag_ck[0])) { echo $flag_ck[0]; }; ?>
										onclick="Javascript: go_conf('Modificare i permessi?' ,
										'<?php echo $url; ?>',
										'?act=adm_menu')" />
								<?php } ?>
							</td>
							<td><?php echo $fieldm['gruppo']; ?></td>
							<td><?php echo $fieldm['titolo_gruppo']; ?></td>
							<td><?php echo $fieldm['act']; ?></td>
							<td><?php echo $fieldm['ordine']; ?></td>

							<td style="text-align: left;">
								<a onclick="javascript: go_conf2('Eliminare la voce del menù?',
									'?act=adm_menu&amp;del=<?php echo $fieldm['id_menu']; ?>'); void(0);" title="Elimina" >
									<img src="/Images/Links/del.png" alt="Elimina" title="Elimina"
										style="border: 0px; width: 14px; height: 14px; vertical-align: middle;" />
								</a>
							</td>
						</tr>
					<?php } ?>
					<tr><th colspan="8"><hr /></th></tr>
				</table>
			</form>
		</div>
	<?php } ?>



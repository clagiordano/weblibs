<?php
	$Stato = $d->GetRows ("*", "tab_stato_call", "", "", "stato_call");
	$Tecnici = $d->GetRows ("id_tecnico, concat(nome, \" \", cognome) as tecnico",
		"tab_tecnici", "", "", "nome, cognome");
	$Ditte = $d->GetRows ("*", "tab_ditte", "", "", "ragione_sociale");

	if (!isset($_GET['act']))
	{
		$_GET['act'] = "";
	}


	if (empty($_POST) && isset($_GET['tomod']))
	{
		$_POST = $d->GetRows ("id_filiale, citta, prov, "
			. " concat(cod_fil, \": \", den) as filiale, "
			. " data_pianif, id_tecnico, id_stato, id_competenza, note_fil",
			"tab_filiali", "id_filiale = '"
			. $_GET['tomod'] . "'");
		$_POST = $_POST[0];
		$_POST['data_pianif'] = $d->Inverti_Data($_POST['data_pianif']);
		$_SESSION['anag_fil']['filiale'] = $_POST['filiale'] . " "
			. $_POST['citta'] . " (" . $_POST['prov'] . ")";
		$_POST['filiale'] = $_SESSION['anag_fil']['filiale'];
	}else{
		$_POST['filiale'] = $_SESSION['anag_fil']['filiale'];
	}

	if (isset($_GET['salva']))
	{
		$errori = "";
		if (trim($_POST['data_pianif']) == "")
		{
			$errori .= "Errore, selezionare una data di pianificazione.";
		}

		if ($_POST['id_stato'] == "-")
		{
			$errori .= "Errore, selezionare uno stato per la filiale.";
		}

		if ($_POST['id_tecnico'] == "-")
		{
			$errori .= "Errore, selezionare un tecnico per la filiale.";
		}

		if (($_GET['salva'] == "modifica") && ($errori == ""))
		{
			$_POST['data_pianif'] = $d->Inverti_Data($_POST['data_pianif']);
			$Status = $d->UpdateRow ($_POST, array('filiale' => ''),
				"tab_filiali", "id_filiale = '" . $_GET['tomod'] . "'");

			// Ri aggiorna l'opener con la stessa query last_select
		?>
			<script type="text/javascript">
				window.parent.document.location.href = window.parent.document.location.href + '&rex=1';
			</script>
		<?php
		}
	}
?>

<form method="post" action="<?php echo $_SERVER['REQUEST_URI']; ?>">
	<table class="form" style="width: 100%;">
		<tr>
			<th colspan="3" style="text-align: right;">
				<a onclick="Javascript: ClosePopup(); void(0);">
					<img src="/Images/Links/exit.png" alt="exit" />
					Chiudi
				</a>
			</th>
		</tr>

		<?php $d->ShowEditBar("Dati Pianificazione:", $_GET['tomod'], "save", $_GET['act']); ?>

		<tr>
			<td style="text-align: right;"><label>Denominazione Filiale:</label></td>
			<td colspan="2">
				<?php echo $_POST['filiale']; ?>
				<input name="filiale" type="hidden" value="<?php echo $_POST['filiale']; ?>" />
			</td>
		</tr>

		<tr>
			<td style="text-align: right;"><label>Data Pianificazione:</label></td>
			<td>
				<input name="data_pianif" type="text" style="width: 9em;"
					onfocus="showCalendarControl(this);"
					value="<?php if (isset($_POST['data_pianif']))
						{ echo $_POST['data_pianif']; } ?>" />
			</td>
		</tr>

		<tr>
			<td style="text-align: right;"><label>Stato Filiale:</label></td>
			<td>
				<select name="id_stato"
					onchange="Javascript: posta('0', '<?php echo $_SERVER['REQUEST_URI']; ?>'); void(0);" >
					<option value="-">Selezione stato filiale</option>
					<?php foreach ($Stato as $key => $field) { ?>
						<option value="<?php echo $field['id_stato_call']; ?>"
							<?php if ($_POST['id_stato'] == $field['id_stato_call']) { echo "selected=\"selected\""; } ?> >
							<?php echo htmlentities($field['stato_call']); ?>
						</option>
					<?php } ?>
				</select>
			</td>
		</tr>

		<tr>
			<td style="text-align: right;"><label>Competenza:</label></td>
			<td>
				<select name="id_competenza"
					onchange="Javascript: posta('0', '<?php echo $_SERVER['REQUEST_URI']; ?>'); void(0);" >
					<option value="-">Selezione competenza</option>
					<?php foreach ($Ditte as $key => $field) { ?>
						<option value="<?php echo $field['id_ditta']; ?>"
							<?php if ($_POST['id_competenza'] == $field['id_ditta']) { echo "selected=\"selected\""; } ?> >
							<?php echo htmlentities($field['ragione_sociale']); ?>
						</option>
					<?php } ?>
				</select>
			</td>
		</tr>

		<tr>
			<td style="text-align: right;"><label>Tecnico:</label></td>
			<td>
				<select name="id_tecnico"
					onchange="Javascript: posta('0', '<?php echo $_SERVER['REQUEST_URI']; ?>'); void(0);" >
					<option value="-">Selezione tecnico</option>
					<?php foreach ($Tecnici as $key => $field) { ?>
						<option value="<?php echo $field['id_tecnico']; ?>"
							<?php if ($_POST['id_tecnico'] == $field['id_tecnico'])
								{ echo "selected=\"selected\""; } ?> >
							<?php echo htmlentities($field['tecnico']); ?>
						</option>
					<?php } ?>
				</select>
			</td>
		</tr>

		<tr>
			<td style="text-align: right; vertical-align: top;">
				<label>Note:</label>
			</td>
			<td><textarea name="note_fil" style="width: 35em; height: 6em;"><?php if (isset($_POST['note_fil'])) { echo $_POST['note_fil']; } ?></textarea></td>
		</tr>

		<tr>
			<td colspan="4">
				<label class="err">
					<?php if(isset($errori)) { echo $errori; } ?>
				</label>
			</td>
		</tr>
		<tr>
			<td colspan="4">
				<label class="err">
					<?php if(isset($Status)) { echo $Status; } ?>
				</label>
			</td>
		</tr>
	</table>
</form>

<?php
	require_once("$root/Function/DataTime.php");
	// Define variables
	$Values=array(50,90,30,155,50,40,320,50,40,86,240,128,650,540,320,87,99);

	$var = GetRows ("tab_chiamate group by data_att", "", "data_att", $db, 0, "count(id_chiamata) as c_day, data_att");

	$x = array();
	$y = array();

	foreach ($var as $key => $field)
	{
		array_push($x, $field['c_day']);
		array_push($y, $field['data_att']);
	}

/*
	echo "<pre>x:";
		print_r($x);
	echo "</pre>";

	echo "<pre>y:";
		print_r($y);
	echo "</pre>";
*/

	$imgWidth=1250;
	$imgHeight=300;
	$grid=60;
	$graphspacing=0.05;

	//Creation of new array with hight adjusted values
	while (list($key, $val) = each($x))
	{
		if($val>$max){ $max=$val; }
	}

	for ($i=0; $i<count($x); $i++)
	{
		$graphValues[$i] = $x[$i] * (($imgHeight*(1-$graphspacing))/$max);
	}
	// Create image and define colors

	$image=imagecreate($imgWidth, $imgHeight);
	$colorWhite=imagecolorallocate($image, 255, 255, 255);
	$colorGrey=imagecolorallocate($image, 192, 192, 192);
	$colorBlue=imagecolorallocate($image, 0, 0, 255);
	$black = imagecolorallocate($image, 0, 0, 0);

	// Create border around image
	imageline($image, 0, 0, 0, $i-mgHeight, $colorGrey);
	imageline($image, 0, 0, $imgWidth, 0, $colorGrey);
	imageline($image, $imgWidth-1, 0, $imgWidth-1, $imgHeight-1, $colorGrey);
	imageline($image, 0, $imgHeight-1, $imgWidth-1, $imgHeight-1, $colorGrey);


/*
 * 	help:
	imageline  ( resource $image  , int $x1  , int $y1  , int $x2  , int $y2  , int $color  )
*/

	// Create grid
	for ($i=0; $i<($imgWidth/$grid); $i++)
	{
		imageline($image, $i*$grid, 0, $i*$grid, $imgHeight, $colorGrey);
		imagestring($image, 2, $i*$grid, 285, Inverti_Data($y[$i]), $black);
	}

	for ($i=0; $i<($imgHeight/$grid); $i++)
	{
		imageline($image, 0, $i*$grid, $imgWidth, $i*$grid, $colorGrey);
/*
		imagestring($image, 2, 0, $i*$grid, $x_lab[$i-1], $black);
*/
	}

	// Create line graph
	if($imgWidth/$grid>count($graphValues)){$space=$grid;}
	else{$space = $imgWidth/(count($graphValues)-1);}

	for ($i=0; $i<count($graphValues)-1; $i++)
	{
		imageline($image, $i*$space, ($imgHeight-$graphValues[$i]), ($i+1)*$space, ($imgHeight-$graphValues[$i+1]), $colorBlue);
	}

	// Output graph and clear image from memory
	imagepng($image, 'tmp/tmp.png');
	imagedestroy($image);

?>

<img src="tmp/tmp.png" alt="tmp" />



<?php
    $tot_res = "-1";
    
    // Estrae i tipi anagrafica:
    $Tipi = $d->GetRows(
        "",
        "tab_tipi_anagrafica",
        "id_tipo_anagrafica != ''",
        "",
        "descr_tipo"
    );

    if (isset($_GET['del']) && is_numeric($_GET['del'])) {
        $d->DeleteRows("tab_anagrafica", "id_anagrafica = '" . $_GET['del'] . "'", $db);
        $Result = "<label class=\"ok\">Anagrafica eliminata.</label>";
    }

    if (!empty($_POST)) {
        $Where = "1";

        if ($_POST['id_tipo_anagrafica'] != "%") {
            $Where .= " AND id_tipo_anagrafica = '" . $_POST['id_tipo_anagrafica'] . "'";
        }

        if (trim($_POST['ragione_sociale']) != "") {
            $tmp_var = $d->ExpandSearch($_POST['ragione_sociale']);
            $Where .= " AND ragione_sociale LIKE '%$tmp_var%'";
        }

        if (trim($_POST['nome']) != "") {
            $tmp_var = $d->ExpandSearch($_POST['nome']);
            $Where .= " AND nome LIKE '%$tmp_var%'";
        }

        if (trim($_POST['cognome']) != "") {
            $tmp_var = $d->ExpandSearch($_POST['cognome']);
            $Where .= " AND cognome LIKE '%$tmp_var%'";
        }

        if (trim($_POST['piva']) != "") {
            $tmp_var = $d->ExpandSearch($_POST['piva']);
            $Where .= " AND piva LIKE '%$tmp_var%'";
        }

        if (trim($_POST['tel']) != "") {
            $tmp_var = $d->ExpandSearch($_POST['telefono1']);
            $Where .= " AND (telefono1 LIKE '%$tmp_var%' OR cellulare1 LIKE '%$tmp_var%')";
        }

        $tot_res = count($d->GetRows("id_anagrafica", "view_anagrafica", $Where));
        $order = $d->PageSplit($_GET['page'], $tot_res);

        $Risultati = $d->GetRows(
            "*",
            "view_anagrafica",
            $Where,
            "",
            "id_tipo_anagrafica, ragione_sociale $order",
            1
        );
    }
?>

<section id="search-form">
	<form method="post" action="<?php echo $_SERVER['REQUEST_URI']; ?>">
		<table class="form" style="width: 99%;">
			<tr>
				<th colspan="2">
					Seleziona i campi fra i quali ricercare:

					&nbsp;
					<a href="home.php?act=anagrafica">
						<img src="Images/Links/new.png" alt="Nuova Anagrafica" />
						Nuova Anagrafica
					</a>
				</th>
			</tr>

			<tr>
				<td colspan="2">
					<select name="id_tipo_anagrafica">
						<option value="%"
							<?php if (isset($_POST['id_tipo_anagrafica']) && ($_POST['id_tipo_anagrafica'] == "%")) {
                                echo "selected=\"selected\"";
} ?>>Tutte le anagrafiche</option>
							
							<?php foreach ($Tipi as $key => $field) {
?>
								<option value="<?php echo $field['id_anagrafica']; ?>"
									<?php if ($_POST['id_tipo_anagrafica'] == $field['id_tipo_anagrafica']) {
                                        echo "selected=\"selected\"";
} ?> >
									<?php echo $field['descr_tipo']; ?>
								</option>
							<?php
} ?>
					</select>

					&nbsp;
					<input name="ragione_sociale" type="text" style="width: 16em;"
						placeholder="Ragione Sociale" title="Ragione Sociale"
						value="<?php if (isset($_POST['ragione_sociale'])) {
                            echo $_POST['ragione_sociale'];
} ?>"
						onKeyPress="return SubmitEnter(this,event);" />

					&nbsp;
					<input name="nome" type="text" style="width: 15em;"
						placeholder="Nome" title="Nome"
						value="<?php if (isset($_POST['nome'])) {
                            echo $_POST['nome'];
} ?>"
						onKeyPress="return SubmitEnter(this,event);" />

					&nbsp;
					<input name="cognome" type="text" style="width: 15em;"
						placeholder="Cognome" title="Cognome"
						value="<?php if (isset($_POST['cognome'])) {
                            echo $_POST['cognome'];
} ?>"
						onKeyPress="return SubmitEnter(this,event);" />

					&nbsp;
					<input name="piva" type="text" style="width: 16em;"
						placeholder="P.Iva o C.F." title="P.Iva o C.F."
						value="<?php if (isset($_POST['piva'])) {
                            echo $_POST['piva'];
} ?>"
						onKeyPress="return SubmitEnter(this,event);" />

					&nbsp;
					<input name="telefono1" type="text" style="width: 15em;"
						placeholder="Telefono o Cellulare" title="Telefono o Cellulare"
						value="<?php if (isset($_POST['telefono1'])) {
                            echo $_POST['telefono1'];
} ?>"
						onKeyPress="return SubmitEnter(this,event);" />
				</td>
			</tr>

			<?php $d->ShowResultBar($tot_res, "home.php?act=ricerca_av");   ?>

		</table>
	</form>
</section>

<?php if ($tot_res > 0) {
?>
	<section id="search-result">
		<form method="post" action="<?php echo $_SERVER['REQUEST_URI']; ?>">
			<table class="dettaglio" style="width: 100%;">
				<tr>
					<th><label>Tipo Anag.:</label></th>
					<th><label>Rag. sociale:</label></th>
					<th><label>Cognome:</label></th>
					<th><label>Nome:</label></th>
					<th><label>P. Iva/Cod. Fisc:</label></th>
					<th><label>Telefono:</label></th>
					<th><label>Cellulare:</label></th>
					<th><!-- links !--></th>
				</tr>
				<tr><th colspan="9"><hr /></th></tr>
				<?php
                foreach ($Risultati as $key => $field) {
                    $id_anag = $field['id_anagrafica'];
                    $ddelconferma = "Eliminare l'anagrafica: " . $field['ragione_sociale'] . " ?";
                    $delurl = "home.php?act=ricerca_av&amp;del=$id_anag";
                    ?>
					<tr>
                    <td><?php echo $field['descr_tipo']; ?></td>
                    <td>
                        <?php if (isset($subact)) {
?>
								<a onclick="javascript: conferma_salva('1', 'Confermi la selezione dell\'anagrafica?',
										'<?php echo $_SERVER['REQUEST_URI']; ?>&amp;select=<?php echo $field['id_anagrafica']; ?>'); void(0);"
										title="Seleziona anagrafica">
										<?php echo $field['ragione_sociale']; ?>
									</a>
								<?php
} else {
                                    echo $field['ragione_sociale'];
} ?>
						</td>
						<td><?php echo $field['cognome']; ?></td>
						<td><?php echo $field['nome']; ?></td>
						<td><?php echo $field['piva']; ?></td>
						<td><?php echo $field['telefono1']; ?></td>
						<td><?php echo $field['cellulare1']; ?></td>

							<td style="text-align: left;">
								<a href="home.php?act=anagrafica&amp;tomod=<?php
                                echo $id_anag; ?>" title="Modifica">
										<img src="/Images/Links/edit.png" alt="edit" />
								</a>

								<a onclick="Javascript: go_conf2('<?php echo $ddelconferma; ?>' ,
									'<?php echo $delurl; ?>'); void(0);"
									title="Elimina" style="cursor: pointer;">
										<img src="/Images/Links/del.png" alt="del" />
								</a>
							</td>
				<?php
                } ?>
				<tr><th colspan="9"><hr /></th></tr>
			</table>
		</form>
	</section>
<?php
}

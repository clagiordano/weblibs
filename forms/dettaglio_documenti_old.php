		<div id="dett_doc" class="expander">
			<div id="edit-bar">
				<img id="img_doc" src="/Images/Links/bott.png" alt="img_doc"
					style="width: 16px; height: 16px; border: none;"
					onclick="Javascript: Expand('dett_doc', 'img_doc', '2em',
						'/Images/Links/bott.png', '/Images/Links/top.png'); void(0);"
					title="Click per Visualizzare/Nascondere il dettaglio" />

				&nbsp;
				Dettaglio Documento:

				&nbsp;
				<a onclick="Javascript: ShowFormDiv('frm_ricerca_articoli.php?subact=documenti',
					'150px', '150px', '850px', '500px', 'popup'); void(0);">
					<img src="/Images/Links/add.png" alt="Aggiungi Articoli" title="Aggiungi Articoli" />
					Articolo
				</a>

				&nbsp;
				<a onclick="Javascript: ShowFormDiv('frm_multi_add.php?titolo=Nuova Voce&amp;subact=documenti',
					'150px', '150px', '550px', '200px', 'popup'); void(0);">
					<img src="/Images/Links/add.png" alt="Aggiungi Voce" title="Aggiungi Voce" />
					Voce
				</a>

				<!--&nbsp;
				<a onclick="javascript: go_conf2('Rimuovere le voci selezionate dal dettaglio?',
					'<?php //echo "?act=documenti&amp;del_sel=0"; ?>'); void(0);">
					<img src="/Images/Links/del.png" alt="del" />
					Selezionati
				</a>!-->

				&nbsp;
				<a onclick="javascript: go_conf2('Rimuovere tutti gli articoli dal documento?',
					'<?php echo "?act=documenti&amp;svuota=0"; ?>'); void(0);">
					<img src="/Images/Links/clear.png" alt="clear" />
					Articoli (<label class="err" title="Totale articoli in dettaglio">
						<?php echo count($_SESSION['documento']['dettaglio']); ?>
					</label>)
				</a>

				<br />

				<?php $d->DrawControl("text", "live_articolo", "Ricerca veloce articolo",
					"\w+", $_POST, null, "50em", "200",
					"onkeyup=\"Javascript: LiveSearch(this, 'lsearch', 'frm_live_search_articoli.php"
						. $BaseURL . "&amp;search=' + this.value);\""); ?>
			</div>

			<form method="post" action="<?php echo $_SERVER['REQUEST_URI']; ?>">
				<table style="width: 100%;">
					<tr>
						<td colspan="5">
							<table style="width: 100%;">
								<tr>
									<td colspan="8">
										<?php if ((count($_SESSION['documento']['dettaglio']) != 0)) { 
											$totale_iva 			= 0;
											$imponibile_merce = 0;
										?>
											<table class="dettaglio" style="width: 100%;" rules="all">
												<tr>
													<th><br /></th>
													<th style="width: 6em; text-align: left;">
														<label>Cod Forn:</label>
													</th>
													<th style="width: 6em; text-align: left;">
														<label>Cod Int:</label>
													</th>
													<th style="text-align: left;">
														<label>Descrizione:</label>
													</th>

													<th>
														<label>Note:</label>
													</th>

													<th style="text-align: center;">
														<label>Qta:</label>
													</th>

													<th style="text-align: center;" title="Imponibile Articolo">
														<label>Imponibile:</label>
													</th>
													<th style="text-align: center;" title="Iva">
														<label>Iva:</label>
													</th>
													<th style="text-align: center;" title="Sconto">
														<label>Sconto:</label>
													</th>
													<?php if ($_SESSION['documento']['tipo_doc'] != "1") { ?>
														<th style="text-align: center;" title="Disponibilità Articolo">
															<label>Disp:</label>
														</th>
													<?php } ?>
													<th style="text-align: center;" title="Importo Totale">
														<label>Importo:</label>
													</th>
													<th><br /></th>
												</tr>
												<tr>
													<th colspan="12"><hr /></th>
												</tr>
													<?php foreach ($_SESSION['documento']['dettaglio'] as $key => $field)
													{
														//~ echo "<pre> FIELD ";
															//~ print_r($field);
														//~ echo "</pre>";
														if ($field['sco'] == "")
														{
															$field['sco'] = 0;
															$imp_art = sprintf("%.4f", ($field['prz']*$field['qta']));
															//~ echo "Debug => $imp_art (sco 0)<br />";
														}else{
															$sconto = $field['prz']*($field['sco']/100);
															$imp_art = sprintf("%.4f", (($field['prz']-$sconto)*$field['qta']));
															//~ echo "Debug => imp_art: $imp_art <br />";
														}

														if (isset($field['id_iva']) && ($field['id_iva'] != ""))
														{
															$iva_art = 0;
															//~ echo "[Debug]: field['iva']: " . $field['iva'] . " <br />";
															$totale_iva += (($imp_art*$field['iva'])-$imp_art);
														}else{
															echo "[Debug]: ERRORE, iva == \"\" !!! <br />";
														}
															$imponibile_merce += $imp_art; ?>

														<tr>
															<td>
																<label><?php echo ($key+1) . ") "; ?></label>
															</td>

															<td style="text-align: left;">
																<?php echo $field['cod_forn']; ?>
															</td>

															<td style="text-align: left;">
																<?php echo $field['cod_int']; ?>
															</td>
															
															<?php if ($field['id_prodotto'] > 0) { ?>
																<td style="text-align: left;">
																	<?php echo htmlentities($field['modello']); ?>
																</td>
																
																<td style="text-align: left;">
																	<textarea name="note<?php echo $key; ?>" onchange="javascript: posta('1', '<?php echo "?act=documenti&amp;key=$key"; if (isset($_GET['tomod'])) { echo "&amp;tomod=" . $_GET['tomod']; }?>'); void(0);" style="width: 10em;"><?php echo $field['note']; ?></textarea>
																</td>
																	
															<?php }else{ ?>
																<td style="text-align: left;" colspan="2">
																	<?php echo htmlentities($field['modello']); ?>
																</td>
															<?php } ?>
																										
															

															<td style="text-align: center;">
																<input name="qta<?php echo $key; ?>"
																	onchange="javascript: posta('1', '<?php echo "?act=documenti&amp;key=$key";
																		if (isset($_GET['tomod']))
																		{ echo "&amp;tomod=" . $_GET['tomod']; }?>'); void(0);"
																	style="width: 5em;" value="<?php
																		if ($field['um'] != "Pz")
																		{
																			printf("%.2f", $field['qta']);
																		}else{
																			printf("%d", $field['qta']);
																		} ?>" />
															</td>

															<td style="text-align: center;">
																<input name="prz<?php echo $key; ?>"
																	onchange="javascript: posta('1', '<?php echo "?act=documenti&amp;key=$key";
																		if (isset($_GET['tomod']))
																		{ echo "&amp;tomod=" . $_GET['tomod']; }?>'); void(0);"
																	type="text" style="width: 5em;" value="<?php printf("%.4f", $field['prz']); ?>" />
															</td>

															<td style="text-align: center;">
																<?php echo $field['desc_iva']; ?>
															</td>

															<td style="text-align: center;">
																<input name="sco<?php echo $key; ?>"
																	onchange="javascript: posta('1', '<?php echo "?act=documenti&amp;key=$key";
																	if (isset($_GET['tomod']))
																		{ echo "&amp;tomod=" . $_GET['tomod']; }?>'); void(0);"
																	type="text" style="width: 5em;" value="<?php printf("%.2f", $field['sco']); ?>" />
															</td>

															<?php if ($_SESSION['documento']['tipo_doc'] != "1") { ?>
																<td style="text-align: center;">
																	<?php echo $field['disp']; ?>
																</td>
															<?php } ?>

															<td style="text-align: center;">
																<?php echo $imp_art; ?>
															</td>
															<td style="text-align: center;">
																<a style="cursor: pointer;" onclick="javascript: go_conf2('Eliminare l\'articolo dal documento?',
																	'?act=documenti&amp;del=<?php echo $key;
																	if (isset($_GET['tomod'])) { echo "&amp;tomod=" . $_GET['tomod']; }?>');">
																	<img src="/Images/Links/del.png" alt="Elimina" />
																</a>
															</td>
														</tr>
													<?php } ?>
											</table>
										<?php } ?>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</form>
		</div>

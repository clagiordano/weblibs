<?php
	/*
	 * Pagina di Visualizzazione/Ricerca/Stampa documenti.
	 */

	$tot_res = -1;

	if (!isset($_GET['page']))
	{
		$_GET['page'] = "first";
	}

	$TipiDoc = $d->GetRows ("*", "tab_tipi_doc", "", "", "tipo_doc", 1);

	if (!empty($_POST))
	{
		$Where = "id_tipo LIKE '" . $_POST['id_tipo_doc'] . "'";

		if (isset($_POST['ck_data_doc']))
		{
			/*
			 * Se una delle date è vuota ma il campo e' stato flaggato
			 * le date mancanti diventano la data odierna
			 */

			if (trim($_POST['data_doc_i']) == "")
			{
				$_POST['data_doc_i'] = date('d/m/Y');
			}

			if (trim($_POST['data_doc_f']) == "")
			{
				$_POST['data_doc_f'] = date('d/m/Y');
			}

			$inizio =  $d->Inverti_Data($_POST['data_doc_i']);
			$fine = $d->Inverti_Data($_POST['data_doc_f']);
			$Where .= " AND (data_doc >= '$inizio' AND data_doc <= '$fine')";
		}

		if (trim($_POST['cod_documento']) != "")
		{
			$Where .= " AND cod_documento LIKE '%"
				. $d->ExpandSearch($_POST['cod_documento']) . "%'";
		}

		if (trim($_POST['ragione_sociale']) != "")
		{
			$Where .= " AND ragione_sociale LIKE '%"
				. $d->ExpandSearch($_POST['ragione_sociale']) . "%'";
		}

		$tot_res 		= count($d->GetRows ("*", "view_documenti", $Where));
		$order 	 		= $d->PageSplit($_GET['page'], $tot_res);
		$Documenti	= $d->GetRows ("*", "view_documenti", $Where, "",
			" data_doc $order", 1);
	}


?>

<div id="search-form">
	<form method="post" action="<?php echo $_SERVER['REQUEST_URI']; ?>">
		<table class="form" style="width: 100%;">
			<tr>
				<th colspan="5">
					Seleziona i campi fra i quali ricercare:
				</th>
			</tr>
			<tr>
				<th colspan="5">
					<select name="id_tipo_doc"
						onchange="Javascript: posta('0', '<?php echo $_SERVER['REQUEST_URI']; ?>'); void(0);">
						<option value="%">Tutti i documenti</option>
						<?php foreach ($TipiDoc as $key => $field) { ?>
							<option value="<?php echo $field['id_tipo_doc']; ?>"
								<?php if(!empty($_POST['id_tipo_doc'])
									&& ($_POST['id_tipo_doc'] == $field['id_tipo_doc']))
									{ echo "selected=\"selected\""; } ?> >
								<?php echo $field['tipo_doc']; ?>
							</option>
						<?php } ?>
					</select>

					&nbsp;
					<label>Periodo:</label>
					<input name="ck_data_doc" type="checkbox" value="interval_doc"
						<?php if (isset($_POST['ck_data_doc'])
							&& ($_POST['ck_data_doc'] != "")) { echo "checked=\"checked\""; } ?> />

					<input name="data_doc_i" type="text" style="width: 6em;"
						title="Data iniziale" onfocus="showCalendarControl(this);"
						placeholder="Data iniziale"
						value="<?php if (isset($_POST['data_doc_i'])) { echo $_POST['data_doc_i']; } ?>" />

					<input name="data_doc_f" type="text" style="width: 6em;"
						title="Data finale" onfocus="showCalendarControl(this);"
						placeholder="Data finale"
						value="<?php if (isset($_POST['data_doc_f'])) { echo $_POST['data_doc_f']; } ?>" />

					<input name="cod_documento" type="text" style="width: 10em;"
						title="Cod. Documento" placeholder="Cod. Documento"
						onKeyPress="return SubmitEnter(this,event);"
						value="<?php if (isset($_POST['cod_documento']))
							{ echo $_POST['cod_documento']; } ?>" />

					<input name="ragione_sociale" type="text" style="width: 20em;"
						title="Rag. Sociale" placeholder="Rag. Sociale"
						onKeyPress="return SubmitEnter(this,event);"
						value="<?php if (isset($_POST['ragione_sociale']))
							{ echo $_POST['ragione_sociale']; } ?>" />

				</th>
			</tr>

			<?php $d->ShowResultBar($tot_res, "home.php?act=stato_doc"); ?>

		</table>
	</form>
</div>

<?php if (isset($Documenti) && count($Documenti) > 0) { ?>
	<div id="search-result-2r">
		<table class="form" style="width: 100%">
			<tr>
				<td style="width: 20px;"><br /></td>
				<td style="width: 150px;">
					<label>Tipo:</label>
				</td>
				<td style="width: 100px;">
					<label>Data:</label>
				</td>
				<td style="width: 150px;">
					<label>Cod. Documento:</label>
				</td>
				<td><label>Ragione Sociale:</label></td>
				<td style="width: 150px;">
					<label>Totale:</label>
				</td>

				<td><br /></td>
			</tr>
			<tr><td colspan="9"><hr /></td></tr>
		</table>

		<?php foreach ($Documenti as $key => $field) { ?>
		<div id="<?php echo "doc_$key"; ?>" class="expander">
			<table class="dettaglio" style="width: 100%;">
				<tr onclick="Javascript: Expand('doc_<?php echo $key; ?>',
								'img<?php echo $key; ?>', '1.7em', '/Images/Links/bott.png',
								'/Images/Links/top.png'); void(0);">

					<td style="width: 20px; text-align: center;" >
						<img id="img<?php echo $key; ?>" src="/Images/Links/bott.png" alt="expand" title="Espandi documento"
							onclick="Javascript: Expand('doc_<?php echo $key; ?>',
								'img<?php echo $key; ?>', '1.5em', '/Images/Links/bott.png',
								'/Images/Links/top.png'); void(0);" />
					</td>

					<td style="width: 150px;">
						<label><?php echo $field['tipo_doc']; ?></label>
					</td>

					<td style="width: 100px;">
						<label><?php echo $d->Inverti_Data($field['data_doc']); ?></label>
					</td>

					<td style="width: 150px;">
						<label><?php echo $field['cod_documento']; ?></label>
					</td>

					<td>
						<label><?php echo $field['ragione_sociale']; ?></label>
					</td>

					<td style="width: 150px; text-align: right;">
						<label><?php printf("%." . PRECISIONE_DECIMALI . "f € ", $field['totale']); ?></label>

						&nbsp;
						<a onclick="Javascript: go('home.php?act=documenti&amp;reset=0&amp;tomod=<?php
							echo $field['id_documento']; ?>'); void(0);" title="Modifica" >
							<img src="/Images/Links/edit.png" alt="edit" />
						</a>

						<?php if ($field['id_tipo'] != 1) { ?>
							<a onclick="Javascript: window.open('frm_pdf_documenti.php?id_doc=<?php
								echo $field['id_documento']; ?>', '_blank'); void(0);" title="Esporta in Pdf">
								<img src="/Images/Links/pdf.png" alt="Pdf" />
							</a>
						<?php }else{ ?>
							<img src="/Images/Links/pdfdis.png" alt="dis Pdf" />
						<?php } ?>
					</td>
				</tr>
			</table>

			<?php
				// Dettaglio documento
				$dett_documento = $d->GetRows ("*", "tab_dett_documenti",
					"id_documento = '" . $field['id_documento'] . "'", "", ""); ?>

				<table style="width: 100%;" class="dettaglio">
					<td style="width: 75px; text-align: right;"><br /></td>
					<th>Cod. Forn:</th>
					<th>Cod. Interno:</th>
					<th>Marca/Modello:</th>
					<th>Q.t&agrave;:</th>
					<th>Imponibile:</th>
					<th>Sconto:</th>
					<th>Totale imp:</th>
					<?php foreach ($dett_documento as $key_dett => $field_dett)
					{
						$prodotto = array();

						if ($field_dett['id_prodotto'] != 0)
						{
							$temp = $d->GetRows ("cod_forn, cod_int, marca, modello", "view_prodotti",
								"id_prodotto = '" . $field_dett['id_prodotto'] . "'");
							$prodotto = $temp[0];
						}else{
							$prodotto['cod_forn'] 	= "-";
							$prodotto['cod_int'] 		= "-";
							$prodotto['marca'] 			= "";
							$prodotto['modello'] 		= $field_dett['voce_arb'];
						}


						$tot = ($field_dett['qta']*$field_dett['prz']);
						$sco = (($field_dett['qta']*$field_dett['prz'])*$field_dett['sco'])/100;
						$tot = ($tot-$sco); ?>
						<tr>
							<td style="width: 75px; text-align: right;">
								<label>&bull;</label>
							</td>
							<td>
								<?php echo $prodotto['cod_forn']; ?>
							</td>
							<td>
								<?php echo $prodotto['cod_int']; ?>
							</td>
							<td>
								<?php echo $prodotto['marca'] . " " . $prodotto['modello']; ?>
							</td>
							<td title="Quantit&agrave;">
								<?php printf("%." . PRECISIONE_DECIMALI . "f", $field_dett['qta']);
									echo " "; //. $field['um']; ?>
							</td>
							<td title="Imponibile"><?php printf("%." . PRECISIONE_DECIMALI . "f € ", $field_dett['prz']); ?></td>
							<td title="Sconto"><?php printf("%." . PRECISIONE_DECIMALI . "f %% ", $field_dett['sco']); ?></td>
							<td title="Totale imponibile"><?php printf("%." . PRECISIONE_DECIMALI . "f € ", $tot); ?></td>
						</tr>
					<?php } ?>
				</table>
			</div>
		<?php } ?>
	</div>
<?php } ?>

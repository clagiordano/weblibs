<?php ob_end_clean();
	require_once('fpdf/fpdf.php');

	$dett = $d->GetRows ("*", "view_requests", "id_request = '"
		. $_GET['rid'] . "'");
	$dett = $dett[0];

	$list_mch = $d->GetRows ("*", "tab_dett_requests",
		"id_request = '" . $_GET['rid'] . "'", "", "", 1);

	function DrawHeaderReport($pdf, $dett)
	{
		global $d;

		// Header Report
		$pdf->SetMargins(6, 6, 6);
		$pdf->AddPage('L');
		$pdf->Image('dati/fsc3.jpg', 5.8, 5.7, 61.5, 21.5);

		$pdf->SetFont('Helvetica', '', 7);
		$pdf->Cell(61, 7, '', '0', '', '');
		$pdf->Cell(16, 7, 'Depositante', '1', '', '');
		$pdf->Cell(40, 7, 'Fujitsu Tecnology Solutions', '1', '', '');
		$pdf->Cell(41, 7, 'Via delle Industrie, 11', '1', '', '');
		$pdf->Cell(59, 7, '20090 Vimodrone (MI)', '1', '', '');
		$pdf->Cell(25, 7, 'Commessa Numero', '1', '', '');
		$pdf->Cell(42, 7, $dett['request'], '1', '', '');
		$pdf->ln();

		$pdf->Cell(61, 7, '', '0', '', '');
		$pdf->Cell(16, 7, 'Mittente', '1', '', '');
		$pdf->SetFont('Helvetica', 'B', 7);
		$pdf->Cell(40, 7, 'Unicredit', '1', '', '');
		$pdf->SetFont('Helvetica', '', 7);
		$pdf->Cell(9, 7, 'Ind.', '1', '', '');
		$pdf->Cell(52, 7, '', '1', '', '');
		$pdf->Cell(14, 7, 'Nom. di rif.', '1', '', '');
		$pdf->Cell(66, 7, '', '1', '', '');
		$pdf->Cell(26, 7, 'Data', '1', '', '');
		$pdf->ln();

		$pdf->Cell(61, 7, '', '0', '', '');
		$pdf->Cell(16, 7, 'Destinatario', '1', '', '');
		$pdf->Cell(40, 7, $dett['den'], '1', '', '');
		$pdf->Cell(9, 7, 'Ind.', '1', '', '');
		$pdf->Cell(52, 7, $dett['ind'], '1', '', '');
		$pdf->Cell(14, 7, 'Nom. di rif.', '1', '', '');
		$pdf->Cell(66, 7, '', '1', '', '');
		$pdf->Cell(26, 7, 'Data: ' . $d->Inverti_Data($dett['data_pianif']),
			'1', '', '');
		$pdf->ln();

		$pdf->Cell(45, 7, 'Annotazione Variazione', '1', '', 'C');
		$pdf->Cell(22, 7, 'Ora Inizio Attivit' . chr(224), '1', '', '');
		$pdf->Cell(40, 7, '', '1', '', '');
		$pdf->Cell(22, 7, 'Ora Fine Attivit' . chr(224), '1', '', '');
		$pdf->Cell(127, 7, '', '1', '', '');
		$pdf->Cell(16, 7, 'Pag', '1', '', 'C');
		$pdf->Cell(12, 7, '1/1', '1', '', 'C');
		$pdf->ln();

		$pdf->Cell(30, 7, 'Tipologia Servizio', '1', '', 'C');
		$pdf->Cell(64, 7, 'Quantit' . chr(224), '1', '', 'C');
		$pdf->Cell(40, 7, 'Modello', '1', '', 'C');
		$pdf->Cell(52, 7, 'Asset' , '1', '', 'C');
		$pdf->Cell(52, 7, 'S/N', '1', '', 'C');
		$pdf->Cell(46, 7, 'Esito / Note / Segnalazione Anomalia', '1', '', 'C');
		$pdf->ln();
	}

	function DrawFooterReport($pdf, $dett)
	{
		$pdf->SetFont('Helvetica', 'B', 9);
		$pdf->Cell(160, 7, 'Riservato alla Delivery', '1', '', 'C');
		$pdf->Cell(64, 7, 'Nome e Firma Tecnico', '1', '', 'C');
		$pdf->Cell(60, 7, 'Firma Cliente', '1', '', 'C');
		$pdf->ln();

		$pdf->SetFont('Helvetica', '', 7);
		$pdf->Cell(60, 7, 'Effettuata formattazione HDD: ', '1', '', 'R');
		$pdf->Cell(10, 7, 'SI', '1', '', 'C');
		$pdf->Cell(10, 7, 'NO', '1', '', 'C');
		$pdf->Cell(60, 7, 'Effettuare imballo apparati: ', '1', '', 'R');
		$pdf->Cell(10, 7, 'SI', '1', '', 'C');
		$pdf->Cell(10, 7, 'NO', '1', '', 'C');
		$pdf->Cell(64, 7, '', 'R', '', 'C');
		$pdf->Cell(60, 7, '', 'R', '', 'C');
		$pdf->ln();

		$pdf->Cell(60, 7, 'Riportare il num di apparati da ritirare: ', '1', '', 'R');
		$pdf->Cell(20, 7, '', '1', '', 'C');
		$pdf->Cell(60, 7, 'Riportare il num di colli da ritirare: ', '1', '', 'R');
		$pdf->Cell(20, 7, '', '1', '', 'C');
		$pdf->Cell(64, 7, '', 'R', '', 'C');
		$pdf->Cell(60, 7, '', 'R', '', 'C');
		$pdf->ln();

		$pdf->Cell(60, 7, 'Note relative al ritiro (orari particolari - ztl - altro): ', '1', '', 'R');
		$pdf->Cell(100, 7, '', '1', '', 'C');
		$pdf->Cell(64, 7, '', 'BR', '', 'C');
		$pdf->Cell(60, 7, '', 'BR', '', 'C');
		$pdf->ln();

		$pdf->SetFont('Helvetica', 'B', 9);
		$pdf->Cell(160, 7, 'Riservato alla Logistica', '1', '', 'C');
		$pdf->SetFont('Helvetica', '', 7);
		$pdf->Cell(90, 7, 'Trasporto a mezzo vettore  ', '1', '', 'C');
		$pdf->Cell(34, 7, '', '1', '', 'C');
		$pdf->ln();

		$pdf->Cell(60, 7, '', '1', '', 'R');
		$pdf->Cell(50, 7, 'Firma del conducente', '1', '', 'C');
		$pdf->Cell(50, 7, 'Firma del vettore', '1', '', 'C');
		$pdf->Cell(90, 7, 'Firma del cliente', '1', '', 'C');
		$pdf->Cell(34, 7, '', 'R', '', 'C');
		$pdf->ln();

		$pdf->Cell(60, 7, 'Mittente', 'LR', '', 'L');
		$pdf->Cell(50, 7, '', 'R', '', 'C');
		$pdf->Cell(50, 7, '', 'R', '', 'C');
		$pdf->Cell(90, 7, '', 'R', '', 'C');
		$pdf->Cell(34, 7, 'Porto Franco', 'R', '', 'R');
		$pdf->ln();

		$pdf->Cell(60, 7, 'Destinatario', 'LR', '', 'L');
		$pdf->Cell(50, 7, '', 'R', '', 'C');
		$pdf->Cell(50, 7, '', 'R', '', 'C');
		$pdf->Cell(90, 7, '', 'R', '', 'C');
		$pdf->Cell(34, 7, 'Consegna/Ritiro entro le 24h', 'R', '', 'R');
		$pdf->ln();

		$pdf->Cell(60, 7, 'Vettore', 'LRB', '', 'L');
		$pdf->Cell(50, 7, '', 'RB', '', 'C');
		$pdf->Cell(50, 7, '', 'RB', '', 'C');
		$pdf->Cell(90, 7, '', 'RB', '', 'C');
		$pdf->Cell(34, 7, 'Consegna/Ritito normale', 'RB', '', 'R');
		$pdf->ln();
	}

	function DrawReportRow($pdf, $dett)
	{
		$pdf->Cell(30, 7, '', '1', '', 'C');
		$pdf->Cell(64, 7, '', '1', '', 'C');
		$pdf->Cell(40, 7, '' , '1', '', 'C');
		$pdf->Cell(52, 7, '' , '1', '', 'C');
		$pdf->Cell(52, 7, '', '1', '', 'C');
		$pdf->Cell(46, 7, '', '1', '', 'C');
		$pdf->ln();
	}

	if (isset($_GET['rid']) && is_numeric($_GET['rid']))
	{
		global $d;
		$pdf = new FPDF();
		$pdf->SetMargins(6, 6, 6);
		$pdf->AddPage();

		// ------------- DETTAGLIO REQUEST -------------
		// Header
		$pdf->SetFont('Helvetica', 'B', 14);
		$pdf->Cell(200, 7, 'Riepilogo Request', '', '', 'C');
		$pdf->ln();

		$pdf->Cell(200, 5, '', '', '', 'C');
		$pdf->ln();

		// Dettaglio Pianificazione:
		$pdf->Cell(200, 7, 'Dettaglio Pianificazione:', 'B', '', 'L');
		$pdf->ln();

		$pdf->SetFont('Helvetica', '', 10);
		$pdf->Cell(50, 5, 'Data Pianificazione: ', '', '', 'R');
		$pdf->Cell(150, 5, $d->Inverti_Data($dett['data_pianif']), '', '', 'L');
		$pdf->ln();

		$pdf->Cell(50, 5, 'Codice Request: ', '', '', 'R');
		$pdf->Cell(150, 5, $dett['request'], '', '', 'L');
		$pdf->ln();
		$pdf->ln();

		// Dettaglio Filiale:
		$pdf->SetFont('Helvetica', 'B', 14);
		$pdf->Cell(200, 7, 'Dettaglio Filiale:', 'B', '', 'L');
		$pdf->ln();

		$pdf->SetFont('Helvetica', '', 10);
		$pdf->Cell(50, 5, 'Codice Filiale: ', '', '', 'R');
		$pdf->Cell(150, 5, $dett['cod_fil'], '', '', 'L');
		$pdf->ln();

		$pdf->Cell(50, 5, 'Denominazione: ', '', '', 'R');
		$pdf->Cell(150, 5, $dett['den'], '', '', 'L');
		$pdf->ln();

		$pdf->Cell(50, 5, 'Utenti: ', '', '', 'R');
		$pdf->Cell(150, 5, $dett['utenti'], '', '', 'L');
		$pdf->ln();

		$pdf->Cell(50, 5, 'Indirizzo: ', '', '', 'R');
		$pdf->Cell(150, 5, $dett['ind'], '', '', 'L');
		$pdf->ln();

		$pdf->Cell(50, 5, 'Localit' . chr(224) . ': ', '', '', 'R');
		$pdf->Cell(150, 5, $dett['citta'], '', '', 'L');
		$pdf->ln();

		$pdf->Cell(50, 5, 'Responsabile Operativo: ', '', '', 'R');
		$pdf->Cell(150, 5, $dett['rol'], '', '', 'L');
		$pdf->ln();

		$pdf->Cell(50, 5, 'Telefono: ', '', '', 'R');
		$pdf->Cell(150, 5, $dett['tel'], '', '', 'L');
		$pdf->ln();

		$pdf->Cell(50, 5, 'Cellulare: ', '', '', 'R');
		$pdf->Cell(150, 5, $dett['cell'], '', '', 'L');
		$pdf->ln();
		$pdf->ln();

		// Dettaglio Staging/Spedizione:
		$pdf->SetFont('Helvetica', 'B', 14);
		$pdf->Cell(200, 7, 'Dettaglio Staging/Spedizione:', 'B', '', 'L');
		$pdf->ln();

		$pdf->SetFont('Helvetica', '', 10);
		$pdf->Cell(50, 5, 'Server nuovo / Incident: ', '', '', 'R');
		$pdf->Cell(150, 5, $dett['i_server'], '', '', 'L');
		$pdf->ln();

		$pdf->Cell(50, 5, 'Quantit' . chr(224) . ' incident server: ', '', '', 'R');
		$pdf->Cell(150, 5, $dett['qi_server'], '', '', 'L');
		$pdf->ln();

		$pdf->Cell(50, 5, 'Nome nuovo server: ', '', '', 'R');
		$pdf->Cell(150, 5, $dett['nome_srv'], '', '', 'L');
		$pdf->ln();

		$pdf->Cell(50, 5, 'Spedizione server nuovo/i: ', '', '', 'R');
		$pdf->Cell(150, 5, $dett['sp_srv_new'], '', '', 'L');
		$pdf->ln();

		$pdf->Cell(50, 5, 'Spedizione Pdl: ', '', '', 'R');
		$pdf->Cell(150, 5, $dett['sp_pdl'], '', '', 'L');
		$pdf->ln();

		$pdf->Cell(50, 5, 'Spedizione Monitor: ', '', '', 'R');
		$pdf->Cell(150, 5, $dett['sp_mon'], '', '', 'L');
		$pdf->ln();

		$pdf->Cell(50, 5, 'Spedizione stampanti laser: ', '', '', 'R');
		$pdf->Cell(150, 5, $dett['sp_las'], '', '', 'L');
		$pdf->ln();

		$pdf->Cell(50, 5, 'Spedizione lettore barcode: ', '', '', 'R');
		$pdf->Cell(150, 5, $dett['sp_bar'], '', '', 'L');
		$pdf->ln();

		$pdf->Cell(50, 5, 'Spedizione lettore assegni: ', '', '', 'R');
		$pdf->Cell(150, 5, $dett['sp_lett'], '', '', 'L');
		$pdf->ln();

		$pdf->Cell(50, 5, 'Spedizione Stampanti di rete: ', '', '', 'R');
		$pdf->Cell(150, 5, $dett['sp_prt_lan'], '', '', 'L');
		$pdf->ln();

		$pdf->Cell(50, 5, 'Spedizione altro hardware: ', '', '', 'R');
		$pdf->Cell(150, 5, $dett['sp_altro_hw'], '', '', 'L');
		$pdf->ln();

		$pdf->Cell(50, 5, 'Staging Pdl: ', '', '', 'R');
		$pdf->Cell(150, 5, $dett['st_pdl'], '', '', 'L');
		$pdf->ln();

		$pdf->Cell(50, 5, 'Staging Server: ', '', '', 'R');
		$pdf->Cell(150, 5, $dett['st_srv'], '', '', 'L');
		$pdf->ln();
		$pdf->ln();

		// Dettaglio Sostituzioni/Riconfigurazioni:
		$pdf->SetFont('Helvetica', 'B', 14);
		$pdf->Cell(200, 5, 'Dettaglio Sostituzioni/Riconfigurazioni:', 'B', '', 'L');
		$pdf->ln();

		$pdf->SetFont('Helvetica', '', 10);
		$pdf->Cell(50, 5, 'Sostituzione Server: ', '', '', 'R');
		$pdf->Cell(150, 5, $dett['sos_srv'], '', '', 'L');
		$pdf->ln();

		$pdf->Cell(50, 5, 'Sostituzione Pdl: ', '', '', 'R');
		$pdf->Cell(150, 5, $dett['sos_pdl'], '', '', 'L');
		$pdf->ln();

		$pdf->Cell(50, 5, 'Sostituzione Monitor: ', '', '', 'R');
		$pdf->Cell(150, 5, $dett['sos_mon'], '', '', 'L');
		$pdf->ln();

		$pdf->Cell(50, 5, 'Incident Monitor: ', '', '', 'R');
		$pdf->Cell(150, 5, $dett['i_mon'], '', '', 'L');
		$pdf->ln();

		$pdf->Cell(50, 5, 'Q.ta Incident Monitor: ', '', '', 'R');
		$pdf->Cell(150, 5, $dett['qi_mon'], '', '', 'L');
		$pdf->ln();

		$pdf->Cell(50, 5, 'Sostituzione stapanti laser: ', '', '', 'R');
		$pdf->Cell(150, 5, $dett['sos_las'], '', '', 'L');
		$pdf->ln();

		$pdf->Cell(50, 5, 'Sostituzione lettore barcode: ', '', '', 'R');
		$pdf->Cell(150, 5, $dett['sos_bar'], '', '', 'L');
		$pdf->ln();

		$pdf->Cell(50, 5, 'Sostituzione lettore assegni: ', '', '', 'R');
		$pdf->Cell(150, 5, $dett['sos_lett'], '', '', 'L');
		$pdf->ln();

		$pdf->Cell(50, 5, 'Incident lettori assegni: ', '', '', 'R');
		$pdf->Cell(150, 5, $dett['i_lett'], '', '', 'L');
		$pdf->ln();

		$pdf->Cell(50, 5, 'Q.ta Incident lettori assegni: ', '', '', 'R');
		$pdf->Cell(150, 5, $dett['qi_lett'], '', '', 'L');
		$pdf->ln();

		$pdf->Cell(50, 5, 'Sostituzione stampanti di rete: ', '', '', 'R');
		$pdf->Cell(150, 5, $dett['sos_prt_lan'], '', '', 'L');
		$pdf->ln();

		$pdf->Cell(50, 5, 'Riconfigurazioni stampanti di rete: ', '', '', 'R');
		$pdf->Cell(150, 5, $dett['ric_prt_lan'], '', '', 'L');
		$pdf->ln();

		$pdf->Cell(50, 5, 'Nomi Stampanti di Rete: ', '', '', 'R');
		$pdf->Cell(150, 5, $dett['nomi_prt_lan'], '', '', 'L');
		$pdf->ln();

		$pdf->Cell(50, 5, 'Riconfigurazioni Multifunzione: ', '', '', 'R');
		$pdf->Cell(150, 5, $dett['ric_multi'], '', '', 'L');
		$pdf->ln();

		$pdf->Cell(50, 5, 'Nomi Multifunzione: ', '', '', 'R');
		$pdf->Cell(150, 5, $dett['nomi_multi'], '', '', 'L');
		$pdf->ln();

		$pdf->Cell(50, 5, 'Incident stampanti: ', '', '', 'R');
		$pdf->Cell(150, 5, $dett['i_prt'], '', '', 'L');
		$pdf->ln();

		$pdf->Cell(50, 5, 'Q.ta Incident stampanti: ', '', '', 'R');
		$pdf->Cell(150, 5, $dett['qi_prt'], '', '', 'L');
		$pdf->ln();
		$pdf->AddPage();

		// Dettaglio Disinstallazioni:
		$pdf->SetFont('Helvetica', 'B', 14);
		$pdf->Cell(200, 5, 'Dettaglio Disinstallazioni:', 'B', '', 'L');
		$pdf->ln();

		$pdf->SetFont('Helvetica', '', 10);
		$pdf->Cell(50, 5, 'Server ', '', '', 'R');
		$pdf->Cell(150, 5, $dett['dis_srv'], '', '', 'L');
		$pdf->ln();

		$pdf->Cell(50, 5, 'Pdl: ', '', '', 'R');
		$pdf->Cell(150, 5, $dett['dis_pdl'], '', '', 'L');
		$pdf->ln();

		$pdf->Cell(50, 5, 'Monitor: ', '', '', 'R');
		$pdf->Cell(150, 5, $dett['dis_mon'], '', '', 'L');
		$pdf->ln();

		$pdf->Cell(50, 5, 'Lettori Assegni: ', '', '', 'R');
		$pdf->Cell(150, 5, $dett['dis_let'], '', '', 'L');
		$pdf->ln();

		$pdf->Cell(50, 5, 'Lettori BArcode: ', '', '', 'R');
		$pdf->Cell(150, 5, $dett['dis_bar'], '', '', 'L');
		$pdf->ln();

		$pdf->Cell(50, 5, 'Stampanti di cassa: ', '', '', 'R');
		$pdf->Cell(150, 5, $dett['dis_cas'], '', '', 'L');
		$pdf->ln();

		$pdf->Cell(50, 5, 'Stampanti laser: ', '', '', 'R');
		$pdf->Cell(150, 5, $dett['dis_las'], '', '', 'L');
		$pdf->ln();

		$pdf->Cell(50, 5, 'Fax: ', '', '', 'R');
		$pdf->Cell(150, 5, $dett['dis_fax'], '', '', 'L');
		$pdf->ln();

		$pdf->Cell(50, 5, 'Altro hardware: ', '', '', 'R');
		$pdf->Cell(150, 5, $dett['dis_altro_hw'], '', '', 'L');
		$pdf->ln();
		$pdf->ln();

		// Dettaglio Ritiri:
		$pdf->SetFont('Helvetica', 'B', 14);
		$pdf->Cell(200, 5, 'Dettaglio Ritiri:', 'B', '', 'L');
		$pdf->ln();

		$pdf->SetFont('Helvetica', '', 10);
		$pdf->Cell(50, 5, 'Server ', '', '', 'R');
		$pdf->Cell(150, 5, $dett['rit_srv'], '', '', 'L');
		$pdf->ln();

		$pdf->Cell(50, 5, 'Pdl: ', '', '', 'R');
		$pdf->Cell(150, 5, $dett['rit_pdl'], '', '', 'L');
		$pdf->ln();

		$pdf->Cell(50, 5, 'Monitor: ', '', '', 'R');
		$pdf->Cell(150, 5, $dett['rit_mon'], '', '', 'L');
		$pdf->ln();

		$pdf->Cell(50, 5, 'Lettori Assegni: ', '', '', 'R');
		$pdf->Cell(150, 5, $dett['rit_let'], '', '', 'L');
		$pdf->ln();

		$pdf->Cell(50, 5, 'Stampanti di cassa: ', '', '', 'R');
		$pdf->Cell(150, 5, $dett['rit_cas'], '', '', 'L');
		$pdf->ln();

		$pdf->Cell(50, 5, 'Stampanti laser: ', '', '', 'R');
		$pdf->Cell(150, 5, $dett['rit_las'], '', '', 'L');
		$pdf->ln();

		$pdf->Cell(50, 5, 'Stampanti di rete: ', '', '', 'R');
		$pdf->Cell(150, 5, $dett['rit_prt_lan'], '', '', 'L');
		$pdf->ln();

		$pdf->Cell(50, 5, 'Lettore barcode: ', '', '', 'R');
		$pdf->Cell(150, 5, $dett['rit_bar'], '', '', 'L');
		$pdf->ln();

		$pdf->Cell(50, 5, 'Fax: ', '', '', 'R');
		$pdf->Cell(150, 5, $dett['rit_fax'], '', '', 'L');
		$pdf->ln();

		$pdf->Cell(50, 5, 'Altro hardware: ', '', '', 'R');
		$pdf->Cell(150, 5, $dett['rit_altro_hw'], '', '', 'L');
		$pdf->ln();
		$pdf->ln();

		$pdf->SetFont('Helvetica', 'B', 14);
		$pdf->Cell(200, 5, 'Note Request:', 'B', '', 'L');
		$pdf->ln();
		$pdf->ln();

		$pdf->SetFont('Helvetica', '', 10);
		$pdf->Cell(200, 5, $dett['note_req'], '', '', 'L');
		$pdf->ln();

		// ------------- DETTAGLIO HW NUOVO CONSEGNATO -------------
		// Header Dettaglio HW New
		$pdf->AddPage('L');
		$pdf->SetFont('Helvetica', 'B', 14);
		$pdf->Cell(280, 7, 'Dettaglio hardware consegnato in filiale "'
			. $dett['den'] . '":', '', '', '');
		$pdf->ln(10);

		$pdf->SetFont('Helvetica', 'B', 10);
		$pdf->Cell(10, 5, '', 'B', '', 'R');
		$pdf->Cell(30, 5, 'Tipo Macchina:', 'B', '', 'L');
		$pdf->Cell(30, 5, 'Marca:', 'B', '', 'L');
		$pdf->Cell(110, 5, 'Modello:', 'B', '', 'L');
		$pdf->Cell(40, 5, 'Serial:', 'B', '', 'L');
		$pdf->Cell(30, 5, 'Asset:', 'B', '', 'L');
		$pdf->Cell(30, 5, 'Mac:', 'B', '', 'L');
		$pdf->Ln();

		$i= 1;
		foreach ($list_mch as $key => $field)
		{
			$dati_mch = $d->GetRows ("*", "view_macchine",
				"id_macchina = '" . $field['id_macchina'] . "'");

				//echo "[Debug]: $i <br />";
				$pdf->SetFont('Helvetica', 'B', 10);
				$pdf->Cell(10, 5, $i . ")", '0', '', 'R');

				$pdf->SetFont('Helvetica', '', 10);
				$pdf->Cell(30, 5, $dati_mch[0]['tipo_mch'], '0', '', 'L');
				$pdf->Cell(30, 5, $dati_mch[0]['marca'], '0', '', 'L');
				$pdf->Cell(110, 5, $dati_mch[0]['modello'], '0', '', 'L');
				$pdf->Cell(40, 5, $dati_mch[0]['serial'], '0', '', 'L');
				$pdf->Cell(30, 5, $dati_mch[0]['asset'], '0', '', 'L');
				$pdf->Cell(30, 5, $dati_mch[0]['mac'], '0', '', 'L');
				$pdf->ln();

				$i += 1;
		}

		// ------------- REPORT ROLLOUT -------------

		DrawHeaderReport($pdf, $dett);

		// Dettaglio Report:
		$pdf->Cell(30, 7, 'Swap server con incident', '1', '', 'C');
		$pdf->Cell(64, 7, '1 Server', '1', '', 'C'); 	// da sostituire con il campo qi_srv
		$pdf->Cell(40, 7, 'Vedi request', '1', '', 'C');
		$pdf->Cell(52, 7, 'Vedi request' , '1', '', 'C');
		$pdf->Cell(52, 7, 'Vedi request', '1', '', 'C');
		$pdf->Cell(46, 7, 'Incident: ' . $dett['i_server'], '1', '', 'C');
		$pdf->ln();

		$pdf->Cell(30, 7, 'Spedizione Apparati', '1', '', 'C');
		$pdf->Cell(64, 7, $dett['sp_pdl'] . ' Pdl + ' . $dett['sp_prt_lan']
			. ' Prt Lan', '1', '', 'C');
		$pdf->Cell(40, 7, 'Vedi request' , '1', '', 'C');
		$pdf->Cell(52, 7, 'Vedi request' , '1', '', 'C');
		$pdf->Cell(52, 7, 'Vedi request', '1', '', 'C');
		$pdf->Cell(46, 7, '', '1', '', 'C');
		$pdf->ln();

		$pdf->Cell(30, 7, 'Staging Apparati', '1', '', 'C');
		$pdf->Cell(64, 7, $dett['st_pdl'] . ' Pdl', '1', '', 'C');
		$pdf->Cell(40, 7, 'Vedi request' , '1', '', 'C');
		$pdf->Cell(52, 7, 'Vedi request' , '1', '', 'C');
		$pdf->Cell(52, 7, 'Vedi request', '1', '', 'C');
		$pdf->Cell(46, 7, '', '1', '', 'C');
		$pdf->ln();

		$pdf->Cell(30, 7, 'Sostituzione Apparati', '1', '', 'C');
		$pdf->Cell(64, 7, $dett['sos_pdl'] . ' Pdl + ' . $dett['sos_prt_lan']
			. ' Prt Lan', '1', '', 'C');
		$pdf->Cell(40, 7, 'Vedi request' , '1', '', 'C');
		$pdf->Cell(52, 7, 'Vedi request' , '1', '', 'C');
		$pdf->Cell(52, 7, 'Vedi request', '1', '', 'C');
		$pdf->Cell(46, 7, '', '1', '', 'C');
		$pdf->ln();

		$pdf->Cell(30, 7, 'Riconfigurazione Apparati', '1', '', 'C');
		$pdf->Cell(64, 7, $dett['ric_multi'] . ' Multifunzione', '1', '', 'C');
		$pdf->Cell(40, 7, 'Vedi request' , '1', '', 'C');
		$pdf->Cell(52, 7, 'Vedi request' , '1', '', 'C');
		$pdf->Cell(52, 7, 'Vedi request', '1', '', 'C');
		$pdf->Cell(46, 7, '', '1', '', 'C');
		$pdf->ln();

		$pdf->Cell(30, 7, 'Disinstallazione Apparati', '1', '', 'C');
		$pdf->Cell(64, 7, $dett['dis_pdl'] . ' Pdl, ' . $dett['dis_mon']
			. ' Monitor, ' . $dett['dis_let'] . ' Lettori, ' . $dett['dis_cas']
			. ' Prt Cassa, ' . $dett['dis_las'] . ' Prt Laser' , '1', '', 'C');
		$pdf->Cell(40, 7, 'Vedi request' , '1', '', 'C');
		$pdf->Cell(52, 7, 'Vedi request' , '1', '', 'C');
		$pdf->Cell(52, 7, 'Vedi request', '1', '', 'C');
		$pdf->Cell(46, 7, '', '1', '', 'C');
		$pdf->ln();

		$pdf->Cell(30, 7, 'Ritiro Apparati', '1', '', 'C');
		$pdf->Cell(64, 7, $dett['rit_pdl'] . ' Pdl, ' . $dett['rit_mon']
			. ' Monitor, ' . $dett['rit_let'] . ' Lettori, ' . $dett['rit_cas']
			. ' Prt Cassa, ' . $dett['rit_las'] . ' Prt Laser' , '1', '', 'C');
		$pdf->Cell(40, 7, 'Vedi request' , '1', '', 'C');
		$pdf->Cell(52, 7, 'Vedi request' , '1', '', 'C');
		$pdf->Cell(52, 7, 'Vedi request', '1', '', 'C');
		$pdf->Cell(46, 7, '', '1', '', 'C');
		$pdf->ln();

		for ($i = 1; $i <= 5; $i++)
		{
			DrawReportRow($pdf, $dett);
		}

		DrawFooterReport($pdf, $dett);

		// Report per i soli incident:
		DrawHeaderReport($pdf, $dett);
		$tot_incident = $dett['qi_lett']+$dett['qi_prt']+$dett['qi_mon'];
		$i_lett = explode("-", $dett['i_lett']);
		$i_prt = explode("-", $dett['i_prt']);
		$i_mon = explode("-", $dett['i_mon']);

/*
		echo "<pre>";
			print_r($i_lett);
		echo "</pre>";
*/

		// Numero massimo di righe per pagina 12
		for ($i = 1; $i <= 12; $i++)
		{
			foreach ($i_lett as $key_i => $field_i)
			{
				if (is_string($field_i) && (strlen($field_i) > 4))
				{
					//echo "[Debug let]: $i - ".$field_i." <br />";
					$pdf->Cell(30, 7, 'incident lettore assegni', '1', '', 'C');
					$pdf->Cell(64, 7, '1 lettore assegni walther', '1', '', 'C');
					$pdf->Cell(40, 7, 'OMR3003' , '1', '', 'C');
					$pdf->Cell(52, 7, '' , '1', '', 'C');
					$pdf->Cell(52, 7, '', '1', '', 'C');
					$pdf->Cell(46, 7, $field_i, '1', '', 'C');
					$pdf->ln();
					$i++;
				}
			}

			foreach ($i_mon as $key_i => $field_i)
			{
				if (is_string($field_i) && (strlen($field_i) > 4))
				{
					//echo "[Debug mon]: $i - |".$field_i."| <br />";
					$pdf->Cell(30, 7, 'incident monitor', '1', '', 'C');
					$pdf->Cell(64, 7, '1 monitor', '1', '', 'C');
					$pdf->Cell(40, 7, '' , '1', '', 'C');
					$pdf->Cell(52, 7, '' , '1', '', 'C');
					$pdf->Cell(52, 7, '', '1', '', 'C');
					$pdf->Cell(46, 7, $field_i, '1', '', 'C');
					$pdf->ln();
					$i++;
				}
			}

			foreach ($i_prt as $key_i => $field_i)
			{
				if (is_string($field_i) && (strlen($field_i) > 4))
				{
					//echo "[Debug prt]: $i - |".$field_i."| <br />";
					$pdf->Cell(30, 7, 'incident stampante', '1', '', 'C');
					$pdf->Cell(64, 7, '1 stampante', '1', '', 'C');
					$pdf->Cell(40, 7, '' , '1', '', 'C');
					$pdf->Cell(52, 7, '' , '1', '', 'C');
					$pdf->Cell(52, 7, '', '1', '', 'C');
					$pdf->Cell(46, 7, $field_i, '1', '', 'C');
					$pdf->ln();
					$i++;
				}
			}

			$diff = 12 - $i;

			for ($r = 1; $r <= $diff+1 ; $r++)
			{
				//echo "[Debug]: $i - differenza vuota <br />";
				DrawReportRow($pdf, $dett);
				$i++;
			}


		}
		DrawFooterReport($pdf, $dett);

		$pdf->Output('Riepilogo.pdf', 'I');
	}else{
		echo "Errore, non e' stato fornito un identificativo valido per la request.";
	}



?>

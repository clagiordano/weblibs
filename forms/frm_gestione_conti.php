<?php
	$root = substr($_SERVER['DOCUMENT_ROOT'], 0, -1);
	require_once ("$root/Function/Strings.php");
	require_once ("$root/Function/Db.php");
	require_once ("$root/Function/Debug.php");
	require_once ("$root/Function/DataTime.php");

	$Conti = GetRows ("tab_conti", "", "descr_conto", $db);

	$dom_salva = "Salvare il nuovo conto/sottoconto?";

	if (isset($_GET['salva']))
	{
		$errori = "";
		switch ($_POST['rad_tipo'])
		{
			case "c":
				$Valori = array('codice_conto' => $_POST['codice'],
								 'descr_conto' => $_POST['descrizione']);
				$DestTab = "tab_conti";
				$Where = "id_conto = '" . $_GET['tomod'] . "'";
				break;

			case "sc":
				if (isset($_POST['req_anag']))
				{
					$req_anag = 0;
				}else{
					$req_anag = 1;
				}

				$Valori = array(		 'id_conto' => $_POST['id_conto'],
								'codice_sottoconto' => $_POST['codice'],
								 'descr_sottoconto' => $_POST['descrizione'],
										 'req_anag' => $req_anag);

				$DestTab = "tab_sottoconti";
				$Where = "id_sottoconto = '" . $_GET['tomod'] . "'";
				break;
		}

		if (trim($_POST['codice']) == "" || (!is_numeric($_POST['codice'])))
		{
			$errori .= "Errore, è necessario inserire un codice valido.<br />";
		}else{
			if (!isset($_GET['tomod']))
			{
				if ($_POST['rad_tipo'] == "conto")
				{
					$ver_unic_conto = GetRows ("tab_conti", "codice_conto = '"
						. $_POST['codice'] . "'", "", $db);
				}else{
					$ver_unic_conto = GetRows ("tab_sottoconti", "codice_sottoconto = '"
						. $_POST['codice'] . "'", "", $db);
				}

				if (count($ver_unic_conto) != 0)
				{
					$errori .= "Errore, il codice inserito è già in uso.<br />";
				}
			}
		}

		if (trim($_POST['descrizione']) == "")
		{
			$errori .= "Errore, è necessario inserire una descrizione.<br />";
		}

		if (($_POST['rad_tipo'] == "sc") && ($_POST['id_conto'] == "-"))
		{
			$errori .= "Errore, è necessario selezionare un conto di appartenenza per il sottoconto.<br />";
		}

		if (($_GET['salva'] == "salva") && ($errori == ""))
		{
			$Status = SaveRow ($Valori, $Esclusioni, $DestTab, $db);
			$Status = $Status[0];
		}

		if (($_GET['salva'] == "modifica") && ($errori == ""))
		{
			$Status = UpdateRow ($Valori, $Esclusioni, $DestTab, $Where, $db);
		}
	}

	if (isset($_GET['id_conto']) && isset($_GET['tomod']))
	{
		$_POST['id_conto'] = $_GET['id_conto'];
	}

	if (isset($_GET['tomod']))
	{
		$dom_salva = "Modificare i dati?";

		if (isset($_GET['tipo']))
		{
			if ($_GET['tipo'] == "c")
			{
				$sottoconto = GetRows ("tab_conti", "id_conto = '"
					. $_GET['tomod'] . "'", "", $db);

				//~ $_POST['id_conto'] = $_GET['tomod'];
				$_POST['codice'] = $sottoconto[0]['codice_conto'];
				$_POST['descrizione'] = $sottoconto[0]['descr_conto'];

			}else{
				$sottoconto = GetRows ("tab_sottoconti", "id_sottoconto = '"
					. $_GET['tomod'] . "'", "", $db);
				$_POST['id_conto'] = $_GET['id_conto'];
				$_POST['codice'] = $sottoconto[0]['codice_sottoconto'];
				$_POST['descrizione'] = $sottoconto[0]['descr_sottoconto'];
				$_POST['req_anag'] = $sottoconto[0]['req_anag'];
			}
		}
	}

	if (isset($_POST['id_conto']) && ($_POST['id_conto'] != "-"))
	{
		$Sottoconti = GetRows ("tab_conti, tab_sottoconti",
			"tab_conti.id_conto = tab_sottoconti.id_conto AND tab_sottoconti.id_conto like '"
			. $_POST['id_conto'] . "'",
			"tab_conti.descr_conto, tab_sottoconti.descr_sottoconto", $db);
	}

?>

<div id="search-form">
	<form method="post" action="<?php echo $_SERVER['REQUEST_URI']; ?>">
		<table style="width: 100%;" class="form">

			<?php ShowEditBar("Aggiungi/Modifica Conto:", $_GET['tomod'], "new,save", $_GET['act']); ?>

			<tr>
				<td>
					<label>Tipo:</label>
					<input name="rad_tipo" type="radio" value="c"
						<?php if (($_POST['rad_tipo'] == "c") || ($_GET['tipo'] == "c"))
						{
							echo "checked=\"checked\"";
						}else{
							echo 'checked="checked"';
						} ?>  />
						<label>Conto</label>

					<input name="rad_tipo" type="radio" value="sc"
						<?php if (($_POST['rad_tipo'] == "sc") || ($_GET['tipo'] == "sc"))
						{
							echo "checked=\"checked\"";
						} ?> />
						<label>Sottoconto</label>
				</td>
			</tr>
			<tr>
				<td>
					<label>Conto:</label>
					<select name="id_conto" onchange="Javascript: posta('0', 'home.php?act=gest_conti&amp;id_conto=' + this.value + '<?php
						if (isset($_GET['tomod'])) { echo "&amp;tomod=" . $_GET['tomod']; } ?>');" >

						<option value="-">Seleziona un conto</option>
						<option value="-" ></option>
						<option value="%" <?php if (!empty($_POST['id_conto']) && ($_POST['id_conto'] == "%")) { echo "selected=\"selected\""; } ?>>
							Tutti
						</option>
						<?php foreach ($Conti as $key => $field) { ?>
							<option value="<?php echo $field['id_conto']; ?>"
								<?php if(isset($_POST['id_conto'])
									&& ($_POST['id_conto'] == $field['id_conto']))
										{
											echo ' selected="selected"';
										} ?> >
								<?php echo "(" . $field['codice_conto'] . ") "
									. ucfirst(strtolower($field['descr_conto'])); ?>
							</option>
						<?php } ?>
					</select>
				</td>
			</tr>
			<tr>
				<td>
					<label>Codice:</label>
					<input name="codice" type="text" style="width: 7em;" maxlength="7"
						value="<?php if (isset($_POST['codice']))
						{
							echo $_POST['codice'];
						} ?>" />

					&nbsp;
					<label>Descrizione</label>
					<input name="descrizione" type="text" style="width: 30em;"
						value="<?php if (isset($_POST['descrizione']))
						{
							echo $_POST['descrizione'];
						} ?>" />

					<label>Ric. Anag.</label>
					<input name="req_anag" type="checkbox"
						<?php if ($_POST['req_anag'] == "0") {
							echo 'checked="checked"';
						} ?> />
				</td>
				<th style="text-align: right;">
					<a onclick="conferma_salva('0', '<?php echo $dom_salva; ?>',
						'home.php?act=gest_conti&amp;<?php
						if (isset($_GET['tomod']))
						{
							echo "salva=modifica&amp;tomod=" . $_GET['tomod'];
						}else{
							echo "salva=salva";
						} ?>'); void(0);" >
						<img src="img/save.png" alt="Salva"
							style="border: 0px; width: 14px; height: 14px; vertical-align: middle;" />
						Salva
					</a>
				</th>
			</tr>

			<?php if(count($Sottoconti) != 0) { ?>
				<tr>
					<td style="text-align: left;" colspan="3">
						<label>Risultati corrispondenti:</label>&nbsp;
						<label class="ok"><?php echo count($Sottoconti); ?></label>
					</td>
				</tr>
			<?php } ?>
			<?php if (isset($Sottoconti) && (count($Sottoconti) == 0)) { ?>
				<tr>
					<td style="text-align: left;">
						<label class="err">Nessun risultato corrispondente.</label>
					</td>
				</tr>
			<?php } ?>
			<tr>
				<td>
					<label class="err"><?php echo $errori; ?></label>
					<?php echo $Status; ?>
				</td>
			</tr>
		</table>
	</form>
</div>

<?php if (!empty($_POST)) { ?>
	<div id="search-result-5r">
		<table style="width: 100%;" class="dettaglio">
			<tr>
				<th>Conto:</th>
				<th>Sottoconto:</th>
				<th>Ric. Anag:</th>
			</tr>
			<tr><th colspan="3"><hr /></th></tr>
			<?php if(count($Sottoconti) != 0) { ?>
				<?php foreach ($Sottoconti as $key => $field) { ?>
					<tr>
						<td>
							<a href="home.php?act=gest_conti&amp;tomod=<?php
								echo $field['id_conto']; ?>&amp;tipo=c" title="Click per modificare il conto">
								<?php echo "(" . $field['codice_conto'] . ") "
									. ucfirst(strtolower($field['descr_conto'])); ?>
							</a>
						</td>
						<td>
							<a href="home.php?act=gest_conti&amp;tomod=<?php
								echo $field['id_sottoconto']; ?>&amp;tipo=sc&amp;id_conto=<?php echo $field['id_conto']; ?>"
								title="Click per modificare il sottoconto">
								<?php echo "(" . $field['codice_sottoconto'] . ") "
									. $field['descr_sottoconto']; ?>
							</a>
						</td>
						<td style="text-align: center;">
							<?php if ($field['req_anag'] == "0") {
								echo "Si";
							}else{
								echo "No";
							} ?>
						</td>
					</tr>
				<?php } ?>
			<?php } ?>
			<tr><th colspan="3"><hr /></th></tr>
		</table>
	</div>
<?php } ?>

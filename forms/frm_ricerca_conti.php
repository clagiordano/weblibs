<?php
/*
 *      frm_ricerca_conti.php
 *
 *      Copyright 2008 Claudio Giordano <claudio.giordano@autistici.org>
 *
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 3 of the License, or
 *      (at your option) any later version.
 *
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 *      MA 02110-1301, USA.
 */

	$root = substr($_SERVER['DOCUMENT_ROOT'], 0, -1);
	require_once("connection.php");
	require_once("$root/Function/Debug.php");
	require_once("$root/Function/Strings.php");
	require_once("$root/Function/Db.php");

	$Conti = GetRows ("tab_conti", "", "descr_conto", $db);

	if(isset($_GET['mod']))
	{
		$_SESSION['mod'] = $_GET['mod'];
	}else{
		unset($_SESSION['mod']);
	}

	if (isset($_GET['prod']))
	{
		switch ($_GET['subact'])
		{
			case "piano_conti":
				$OpenerUrl = "home.php?act=piano_conti";
				if (isset($_SESSION['piano']['tomod']))
				{
					$OpenerUrl .= "&tomod=" . $_SESSION['piano']['tomod'];
				}

				// metti in sessione il sottoconto scelto
				//~ $Sottoconto = GetRows ("tab_sottoconti", "id_sottoconto = '"
					//~ . $_GET['prod'] . "'", 'descr_sottoconto', $db);
				//~ $_SESSION['piano']['dettaglio'][$_GET['key']]['id_conto'] = $Sottoconto[0]['descr_sottoconto'];
				$_SESSION['piano']['dettaglio'][$_GET['key']]['id_sottoconto'] = $_GET['prod'];
				break;
		}
	}

 ?>

	<script type="text/javascript">
		<?php if (isset($_GET['prod'])) { ?>
			window.opener.location.href="<?php echo $OpenerUrl; ?>";
			window.close();
		<?php } ?>
	</script>

	<?php

		// Se è stata fatta una ricerca:
		if (isset($_POST['descr']))
		{
			// Se il valore cercato è ==  "" setto il valore cercato = "%":
			if (trim($_POST['descr']) == "")
			{
				$_POST['descr'] = "%";
			}

			// Compone la query per la ricerca dei prodotti:
			//~ Escludo dalla selezione articoli gia' composti.AND ck_com = '1' ????

			$Tab = "tab_conti, tab_sottoconti";
			$Where = "tab_conti.id_conto = tab_sottoconti.id_conto";
			$val = ModoRicerca($_POST['descr'], "tutto");

			// Sostituisce agli spazi nella ricerca con "%"
			$temp_val = explode(" ", $val);
			$val = join("%", $temp_val);
			$Where .= " AND tab_sottoconti.descr_sottoconto LIKE '". $val ."' ";

			/*
			 * Se è stata scelta una categoria dal /Images/Links/
			 * usala per filtrare i risultati:
			 */
			if ($_POST['id_conto'] != "%")
			{
				$Where .= "AND tab_sottoconti.id_conto = '" . $_POST['id_conto'] . "'";
			}
			$Sottoconti = GetRows ($Tab, $Where, "descr_sottoconto", $db);
		}
?>

<div id="search-form">
	<form method="post" action="<?php echo $_SERVER['REQUEST_URI']; ?>">
		<table class="form" width="100%">
			<tr>
				<th colspan="3">Ricerca Conto:</th>
				<!--<td colspan="2" style="text-align: right;">
					/*
					 * 12/08/2009 17:13:22 CEST Claudio Giordano
					 *
					 * Modificare successivamente in aggiunta causale.
					 */
					<a href="frm_articolo.php">
						<img src="/Images/Links/add.png" alt="Nuovo Articolo"
							style="border: 0px; width: 14px; height: 14px; vertical-align: middle;" />
						Nuovo Articolo
					</a>
				</td> !-->
			</tr>
			<tr>
				<td>
					<label>Conto:</label><br />
					</Images/Links/ name="id_conto" style="width: 26em;"
						onchange="Javascript: posta('0', '<?php echo $_SERVER['REQUEST_URI']; ?>'); void(0);" >
						<option value="%">Tutti</option>
						<?php foreach ($Conti as $key => $field) { ?>
							<option value="<?php echo $field['id_conto']; ?>"
								<?php if (isset($_POST['id_conto'])
									&& ($_POST['id_conto'] == $field['id_conto'])) {
										echo "/Images/Links/ed=\"/Images/Links/ed\""; } ?> >
								<?php echo "(" . $field['codice_conto'] . ") " . ucfirst(strtolower($field['descr_conto'])); ?>
							</option>
						<?php } ?>
					<//Images/Links/>
				</td>

				<td>
					<label>Sottoconto:</label><br />
					<input name="descr" type="text" style="width: 18em;"
						value="<?php if (isset($_POST['descr'])) { echo $_POST['descr']; } ?>" />
				</td>

				<th>
					<br />
					<a onclick="Javascript: posta('0', '<?php echo $_SERVER['REQUEST_URI']; ?>')"
						style="cursor: pointer;">
						<img src="/Images/Links/find" alt="find"
							style="border: 0px; width: 14px; height: 14px; vertical-align: middle;" />
						Avvia Ricerca
					</a>
				</th>
			</tr>

			<?php if(count($Sottoconti) != 0) { ?>
				<tr>
					<td style="text-align: left;" colspan="3">
						<label>Risultati corrispondenti:</label>&nbsp;
						<label class="ok"><?php echo count($Sottoconti); ?></label>
					</td>
				</tr>
			<?php } ?>
			<?php if (isset($Sottoconti) && (count($Sottoconti) == 0)) { ?>
				<tr>
					<td style="text-align: left;">
						<label class="err">Nessun risultato corrispondente.</label>
					</td>
				</tr>
			<?php } ?>
		</table>
	</form>
</div>

<?php if(isset($_POST['descr']) && (count($Sottoconti) != 0)) { ?>
	<div id="search-result-2r">
		<form method="post" action="<?php echo $_SERVER['REQUEST_URI']; ?>">
			<table border="0" cellspacing="1" width="100%" class="dettaglio">
				<tr>
					<th><label>Conto:</label></th>
					<th><label>Sottoconto:</label></th>
					<th><!-- Link !--></th>
				</tr>
				<tr><th colspan="10"><hr /></th></tr>
				<?php
					foreach ($Sottoconti as $key => $field)	{ ?>
						<tr>
							<td>
								<?php echo "(" . $field['codice_conto'] . ") "
									. ucfirst(strtolower($field['descr_conto'])); ?>
							</td>
							<td>
								<a onclick="javascript: conferma_posta('1', 'Confermi la selezione del sottoconto?', '<?php echo $_SERVER['REQUEST_URI']; ?>&amp;prod=<?php
										echo $field['id_sottoconto']; ?>'); void(0);" title="Seleziona sottoconto">
									<?php echo "(" . $field['codice_sottoconto'] . ") "
										. ucfirst(strtolower($field['descr_sottoconto'])); ?>
								</a>
							</td>

							<td style="text-align: center;">
								<label>
									<a onclick="javascript: posta('1', 'frm_ricerca_conti.php?prod=<?php
										echo $field['id_sottoconto'];
										if (isset($_GET['subact'])) { echo "&amp;subact=" . $_GET['subact']; }
										if (isset($_GET['pezzo'])) { echo "&amp;pezzo=" . $_GET['pezzo']; }
										if (isset($_GET['key'])) { echo "&amp;key=" . $_GET['key']; }  ?>');"
										title="Seleziona sottoconto" style="cursor: pointer;">
										<img src="/Images/Links//Images/Links/.png" alt="Seleziona"
											style="border: 0px; width: 14px; height: 14px; vertical-align: middle;" />
									</a>
								</label>
							</td>
						</tr>
					<?php } ?>
				<tr><th colspan="10"><hr /></th></tr>
			</table>
		</form>
	</div>
<?php } ?>

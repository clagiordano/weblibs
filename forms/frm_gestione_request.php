<?php
	$ConfSalva = "Salvare i dati inseriti?";

	if (isset($_GET['tomod']))
	{
		$UrlSalva = "?act=gest_req&amp;tomod=" . $_GET['tomod'] . "&amp;salva=modifica";
	}else{
		$_GET['tomod'] = "";
		$UrlSalva = "?act=gest_req&amp;salva=salva";
	}

	$Province = $d->GetRows ("distinct(prov)", "tab_filiali", "", "prov");
	if (isset($_POST['prov']))
	{
		$Citta = $d->GetRows ("distinct(citta)", "tab_filiali",
			"prov like '" . $_POST['prov'] . "'", "citta");
	}else{
		$Citta = array();
	}

	if (isset($_POST['citta']))
	{
		$Filiali = $d->GetRows ("id_filiale, concat(cod_fil, \": \", den) as filiale",
			"tab_filiali", " citta like '" . addslashes($_POST['citta'])
			. "' AND prov like '" . $_POST['prov'] . "'", "den");
	}else{
		$Filiali = array();
	}

	if (isset($_GET['tomod']) && empty($_POST))
	{
		// Importa i dati nel post dal db
		$res = $d->GetRows ("*", "tab_request", "id_request = '" . $_GET['tomod']. "'");
		$resFiliale = $d->GetRows ("*", "tab_filiali", "id_filiale = '" . $res[0]['id_filiale'] . "'");
		$_POST = $res[0];

		$_POST['prov'] = $resFiliale[0]['prov'];
		$Citta = $d->GetRows ("distinct(citta)", "tab_filiali",
			"prov like '" . $_POST['prov'] . "' ", "citta");

		$_POST['citta'] = $resFiliale[0]['citta'];
		$Filiali = $d->GetRows ("id_filiale, concat(cod_fil, \": \", den) as filiale",
			"tab_filiali", " citta like '" . addslashes($_POST['citta'])
			. "' AND prov like '" . $_POST['prov'] . "'", "den");

		$_POST['id_filiale'] = $resFiliale[0]['id_filiale'];

		// Recupero la data di pianificazione e la aggiungo all'array del post
		$pianif = $d->GetRows ("*", "tab_filiali", "id_filiale = '"
			. $_POST['id_filiale'] . "'");
		$_POST['data_pianif'] = $d->Inverti_Data($pianif[0]['data_pianif']);

	}

/*
	echo "<pre>";
		print_r($_POST);
	echo "</pre>";
*/

	if (!empty($_POST) && isset($_GET['salva']))
	{
		$errori = "";
		/*foreach ($_POST as $key => $field)
		{
			if (empty($field))
			{

				echo "[Debug]: ck field [$key]: $field <br />";

				$errori = "Errore, non sono stati compilati tutti i campi.";
			}
		}*/

		if (trim($_POST['request']) == "")
		{
			$errori .= "Errore, inserire il codice di request.";
		}

		if ($_POST['id_filiale'] == "%")
		{
			$errori .= "Errore, selezionare la filiale di riferimento.";
		}


		$_POST['data_request'] = date('Y-m-d');	// Giorno di inserimento
		/*
		 * Aggiorno la tabella filiali con la data di
		 * pianificazione ed il flag pianificato (id 3):
		 *
		 * SOLO se la chiamata non è già chiusa (id 1)
		 * in caso contrario data e stato non vengono aggiornati.
		 */
		$update = array('data_pianif' => $d->Inverti_Data($_POST['data_pianif']),
			'id_stato' => '3');

		if (($_GET['salva'] == "salva") && ($errori == ""))
		{
			$Status = $d->UpdateRow ($update, "", "tab_filiali", "id_filiale = '"
				. $_POST['id_filiale'] . "' AND id_stato != '1'");

			$Status = $d->SaveRow ($_POST, array('prov' => '', 'citta' => '',
				'data_pianif' => ''), "tab_request", 1, "request");

			$Status = $Status[0];
		}

		if (($_GET['salva'] == "modifica") && ($errori == ""))
		{
			$Status = $d->UpdateRow ($update, "", "tab_filiali", "id_filiale = '"
				. $_POST['id_filiale'] . "' AND id_stato != '1'");

			$Status = $d->UpdateRow ($_POST, array('prov' => '', 'citta' => '',
				'data_pianif' => ''), "tab_request", "id_request = '"
				. $_GET['tomod'] . "'");
		}
	}

	$Requests = $d->GetRows ("*", "view_requests", "", "data_pianif");
?>


<div id="search-result">
	<form method="post" action="<?php echo $_SERVER['REQUEST_URI']; ?>">
        <table class="form" style="width: 100%;">

			<?php $d->ShowEditBar("Gestione Request:", $_GET['tomod'],
				"new,save", $_GET['act']); ?>

      <tr>
				<th style="text-align: right;">
           Dati Request:
				</th>
				<td style="text-align: right;">
						<label>Codice Request:</label>
				</td>
				<td>
					<input name="request" type="text" style="width: 14em;" maxlength="50"
						value="<?php if (isset($_POST['request']))
							{ echo $_POST['request']; } ?>" />

					&nbsp;
					<label>Data Pianificazione:</label>
					<input name="data_pianif" type="text" onfocus="showCalendarControl(this);" style="width: 9em;"
							value="<?php if (isset($_POST['data_pianif']))
								{ echo $_POST['data_pianif']; } ?>" />

						<?php if (isset($_GET['tomod'])) { ?>
						&nbsp;
						<label>
							<a onclick="Javascript: window.open('frm_pdf_dett_request.php?rid=<?php
								echo $_GET['tomod']; ?>', '_blank'); void(0);"
								title="Stampa Riepilogo Request">
								<img src="/Images/Links/pdf.png" alt="pdf" />
								Esporta come Pdf
							</a>
						</label>
					<?php } ?>
				</td>
			</tr>

			<tr>
				<th></th>

				<td style="text-align: right;">
					<label>Filiale:</label>
			</td>
			<td>
				<!--	SOSTITUITO CON I 2 SELECT PROVINCIA/CITTA
				<a onclick="Javascript: ShowFrameDiv ('frm_popup_div_search.php?target=popup&amp;subact=gest_req', '200px', '200px', '700px', '280px', 'popup', 'popup_frame');">
				<?php if (isset($_SESSION['request']['id_filiale'])) {
					$Campi = $d->GetRows ("*", "tab_filiali", "id_filiale = '"
						. $_SESSION['request']['id_filiale'] . "'", "", $Order);
					echo $Campi[0]['den'];
				}else{
					echo "Seleziona Filiale";
				} ?>

			</a>!-->

				<select name="prov"
					onchange="Javascript: posta('0', '<?php
						echo $_SERVER['REQUEST_URI']; ?>'); void(0);" >
					<option value="%">Seleziona Provincia</option>
					<?php foreach ($Province as $key => $field) { ?>
						<option value="<?php echo $field['prov']; ?>"
							<?php if ($_POST['prov'] == $field['prov'])
								{ echo "selected=\"selected\""; }?> >
							<?php echo $field['prov']; ?>
						</option>
					<?php } ?>
				</select>

					&nbsp;
					<select name="citta"
						onchange="Javascript: posta('0', '<?php echo $_SERVER['REQUEST_URI']; ?>'); void(0);" >
						<option value="%">Seleziona Citt&agrave;</option>
						<?php foreach ($Citta as $key => $field) { ?>
							<option value="<?php echo $field['citta']; ?>"
								<?php if ($_POST['citta'] == $field['citta'])
									{ echo "selected=\"selected\""; }?> >
								<?php echo $field['citta']; ?>
							</option>
						<?php } ?>
					</select>

					&nbsp;
					<select name="id_filiale"
						onchange="Javascript: posta('0', '<?php echo $_SERVER['REQUEST_URI']; ?>'); void(0);" >
						<option value="%">Seleziona Filiale</option>
						<?php foreach ($Filiali as $key => $field) { ?>
							<option value="<?php echo $field['id_filiale']; ?>"
								<?php if ($_POST['id_filiale'] == $field['id_filiale'])
									{ echo "selected=\"selected\""; }?> >
								<?php echo $field['filiale']; ?>
							</option>
						<?php } ?>
					</select>
                </td>
            </tr>

			<tr>
				<td></td>
				<td style="text-align: right;">
					<label>Incident Lettori:</label>
				</td>
				<td colspan="2">
					<input name="i_lett" type="text" style="width: 25em;"
						value="<?php if (isset($_POST['i_lett'])) { echo $_POST['i_lett']; } ?>" />

					&nbsp;
					<label>Q.ta Incident Lettori:</label>
					<input name="qi_lett" type="text" maxlength="2" style="width: 3em;"
						value="<?php if (isset($_POST['qi_lett'])) { echo $_POST['qi_lett']; } ?>" />
				</td>
			</tr>

			<tr>
				<td></td>
				<td style="text-align: right;">
					<label>Incident Monitor:</label>
				</td>
				<td colspan="2">
					<input name="i_mon" type="text" style="width: 25em;"
						value="<?php if (isset($_POST['i_mon'])) { echo $_POST['i_mon']; } ?>" />

					&nbsp;
					<label>Q.ta Incident Monitor:</label>
					<input name="qi_mon" type="text" maxlength="2" style="width: 3em;"
						value="<?php if (isset($_POST['qi_mon'])) { echo $_POST['qi_mon']; } ?>" />
				</td>
			</tr>

			<tr><th colspan="4"><hr /></th></tr>

			<tr>
				<th style="text-align: right;">
					Staging/Spedizione:
				</th>

				<td style="text-align: right;">
					<label>I.Server:</label>
				</td>
				<td>
					<input name="i_server" type="text" style="width: 8em;" maxlength="20"
						value="<?php if (isset($_POST['i_server'])) { echo $_POST['i_server']; } ?>" />

						&nbsp;
						<label>Sp. Srv New:</label>
						<input name="sp_srv_new" type="text" style="width: 3em;" maxlength="3"
								value="<?php if (isset($_POST['sp_srv_new'])) { echo $_POST['sp_srv_new']; } ?>" />

						&nbsp;
						<label>Sp. Pdl:</label>
						<input name="sp_pdl" type="text" style="width: 3em;" maxlength="3"
								value="<?php if (isset($_POST['sp_pdl'])) { echo $_POST['sp_pdl']; } ?>" />

						&nbsp;
						<label>Sp. Prt Lan:</label>
						<input name="sp_prt_lan" type="text" style="width: 3em;" maxlength="3"
								value="<?php if (isset($_POST['sp_prt_lan'])) { echo $_POST['sp_prt_lan']; } ?>" />

						&nbsp;
						<label>Stag. Pdl:</label>
						<input name="st_pdl" type="text" style="width: 3em;" maxlength="3"
								value="<?php if (isset($_POST['st_pdl'])) { echo $_POST['st_pdl']; } ?>" />
				</td>
		</tr>

		<tr><th colspan="4"><hr /></th></tr>

            <tr>
				<th style="text-align: right;">
                    Sostituzioni/Riconfigurazioni:
                </th>

                <td style="text-align: right;">
                    <label>Pdl:</label>
                </td>
                <td>
                    <input name="sos_pdl" type="text" style="width: 3em;" maxlength="3"
                        value="<?php if (isset($_POST['sos_pdl'])) { echo $_POST['sos_pdl']; } ?>" />

					&nbsp;
                    <label>Prt. Lan:</label>
                    <input name="sos_prt_lan" type="text" style="width: 3em;" maxlength="3"
                        value="<?php if (isset($_POST['sos_prt_lan'])) { echo $_POST['sos_prt_lan']; } ?>" />

                    &nbsp;
                    <label>Riconf. Multifunzione:</label>
                    <input name="ric_multi" type="text" style="width: 3em;" maxlength="3"
                        value="<?php if (isset($_POST['ric_multi'])) { echo $_POST['ric_multi']; } ?>" />
                </td>
            </tr>

			<tr>
				<th><br /></th>
				<td style="text-align: right;">
					<label>Nomi Prt. Lan:</label>
				</td>

				<td>
					<input name="nomi_prt_lan" type="text" maxlength="200" style="width: 19.5em;"
						value="<?php if (isset($_POST['nomi_prt_lan'])) { echo $_POST['nomi_prt_lan']; } ?>" />

					&nbsp;
					<label>Nomi Multi Lan:</label>
					<input name="nomi_multi" type="text" maxlength="200" style="width: 19.5em;"
						value="<?php if (isset($_POST['nomi_multi'])) { echo $_POST['nomi_multi']; } ?>" />
				</td>
			</tr>

            <tr><th colspan="4"><hr /></th></tr>

            <tr>
				<th style="text-align: right;">
                    Disinstallazione Apparati:
                </th>

                <td style="text-align: right;">
                    <label>Server:</label>
                </td>
                <td>
					<input name="dis_srv" type="text" style="width: 3em;" maxlength="3"
                        value="<?php if (isset($_POST['dis_srv'])) { echo $_POST['dis_srv']; } ?>" />

                    &nbsp;
                    <label>Pdl:</label>
                    <input name="dis_pdl" type="text" style="width: 3em;" maxlength="3"
                        value="<?php if (isset($_POST['dis_pdl'])) { echo $_POST['dis_pdl']; } ?>" />

                    &nbsp;
                    <label>Monitor:</label>
                    <input name="dis_mon" type="text" style="width: 3em;" maxlength="3"
                        value="<?php if (isset($_POST['dis_mon'])) { echo $_POST['dis_mon']; } ?>" />

                    &nbsp;
                    <label>Lettore Assegni:</label>
                    <input name="dis_let" type="text" style="width: 3em;" maxlength="3"
                        value="<?php if (isset($_POST['dis_let'])) { echo $_POST['dis_let']; } ?>" />

				</td>
			</tr>
			<tr>
				<td></td>
				<td style="text-align: right;">
					<label>Prt. Cassa:</label>
				</td>
				<td>
                    <input name="dis_cas" type="text" style="width: 3em;" maxlength="3"
                        value="<?php if (isset($_POST['dis_cas'])) { echo $_POST['dis_cas']; } ?>" />

					&nbsp;
                    <label>Prt. Laser:</label>
                    <input name="dis_las" type="text" style="width: 3em;" maxlength="3"
                        value="<?php if (isset($_POST['dis_las'])) { echo $_POST['dis_las']; } ?>" />

                    &nbsp;
                    <label>Fax:</label>
                    <input name="dis_fax" type="text" style="width: 3em;" maxlength="3"
                        value="<?php if (isset($_POST['dis_fax'])) { echo $_POST['dis_fax']; } ?>" />
                </td>
            </tr>

			<tr><th colspan="4"><hr /></th></tr>

            <tr>
				<th style="text-align: right;">
                    Ritiro Apparati:
                </th>

                <td style="text-align: right;">
                    <label>Server:</label>
                </td>
                <td>
					<input name="rit_srv" type="text" style="width: 3em;" maxlength="3"
                        value="<?php if (isset($_POST['rit_srv'])) { echo $_POST['rit_srv']; } ?>" />

					&nbsp;
					<label>Pdl:</label>
                    <input name="rit_pdl" type="text" style="width: 3em;" maxlength="3"
                        value="<?php if (isset($_POST['rit_pdl'])) { echo $_POST['rit_pdl']; } ?>" />

                    &nbsp;
                    <label>Monitor:</label>
                    <input name="rit_mon" type="text" style="width: 3em;" maxlength="3"
                        value="<?php if (isset($_POST['rit_mon'])) { echo $_POST['rit_mon']; } ?>" />

                    &nbsp;
                    <label>Lettore Assegni:</label>
                    <input name="rit_let" type="text" style="width: 3em;" maxlength="3"
                        value="<?php if (isset($_POST['rit_let'])) { echo $_POST['rit_let']; } ?>" />
				</td>
			</tr>
			<tr>
				<td></td>
				<td style="text-align: right;">
					<label>Prt. Cassa:</label>
				</td>
				<td>
                    <input name="rit_cas" type="text" style="width: 3em;" maxlength="3"
                        value="<?php if (isset($_POST['rit_cas'])) { echo $_POST['rit_cas']; } ?>" />

					&nbsp;
                    <label>Prt. Laser:</label>
                    <input name="rit_las" type="text" style="width: 3em;" maxlength="3"
                        value="<?php if (isset($_POST['rit_las'])) { echo $_POST['rit_las']; } ?>" />

                    &nbsp;
                    <label>Fax:</label>
                    <input name="rit_fax" type="text" style="width: 3em;" maxlength="3"
                        value="<?php if (isset($_POST['rit_fax'])) { echo $_POST['rit_fax']; } ?>" />
                </td>
            </tr>

            <tr><th colspan="4"><hr /></th></tr>

			<tr>
				<th style="text-align: right; vertical-align: top;">
					Note Request:
				</th>
				<th></th>
				<td colspan="2">
					<textarea name="note_req" style="width: 50em; height: 4em;"><?php if (isset($_POST['note_req'])) { echo $_POST['note_req']; } ?></textarea>
				</td>
			</tr>

			<tr>
				<th colspan="8">
					<label class="err">
						<?php if (isset($errori)) { echo $errori; } ?>
					</label>
				</th>
			</tr>
			<tr>
				<th colspan="8">
					<label class="err">
						<?php if (isset($Status)) { echo $Status; } ?>
					</label>
				</th>
			</tr>

        </table>
    </form>
</div>

<!--
<div id="search-result-50">
	<table style="width: 100%;" class="dettaglio">
		<tr>
			<th><label>Pianificazione:</label></th>
			<th><label>Codice:</label></th>
			<th><label>Denominazione:</label></th>
			<th><label>Citta:</label></th>
			<th><label>Utenti:</label></th>
			<th><label>Sp. Pdl:</label></th>
			<th><label>Sp. Prt Lan:</label></th>
			<th><label>Stato:</label></th>
			<th></th>
		</tr>
		<tr><th colspan="8"><hr /></th></tr>
		<?php /*foreach ($Requests as $key => $field) {
			$color = $field['color']; ?>
			<tr>
				<td><?php echo Inverti_Data($field['data_pianif']); ?></td>
				<td>
					<a href="home.php?act=gest_req&amp;tomod=<?php
						echo $field['id_request']; ?>" title="Modifica">
						<?php echo $field['request']; ?>
					</a>
				</td>
				<?php if ($field['id_filiale'] != "0") { ?>
					<td><?php echo $field['den']; ?></td>
					<td><?php echo $field['citta']; ?></td>
					<td><?php echo $field['utenti']; ?></td>
				<?php }else{ ?>
					<td>-</td>
					<td>-</td>
					<td>-</td>
				<?php } ?>
				<td><?php echo $field['sp_pdl']; ?></td>
				<td><?php echo $field['sp_prt_lan']; ?></td>
				<td <?php echo "style=\"color: $color ! important; font-weight: bold\""; ?> >
					<?php echo $field['stato_call']; ?>
				</td>
				<!--<td>
					<a href="home.php?act=gest_req&amp;tomod=<?php //echo $field['id_request']; ?>"
						title="Modifica">
						<img src="/Images/Links/edit.png" alt="edit" />
					</a>

					<a href="home.php?act=gest_req&amp;todel=<?php //echo $field['id_request']; ?>"
						title="Elimina">
						<img src="/Images/Links/del.png" alt="del" />
					</a>
				</td>!-->
			</tr>
		<?php }*/ ?>
		<tr><th colspan="8"><hr /></th></tr>
	</table>
</div>
!-->

<?php
	//~ $root = substr($_SERVER['DOCUMENT_ROOT'], 0, -1);
	//~ require_once ("$root/Function/Strings.php");
	//~ require_once ("$root/Function/Db.php");
	//~ require_once ("$root/Function/Debug.php");
	//~ require_once ("$root/Function/DataTime.php");

	$condizione = array();
	array_push($condizione, "id_off != ''");

	if (isset($_GET['del_off']))
	{
		DeleteRows ("tab_offerte", "id_off = '" . $_GET['del_off'] . "'", $db);
		DeleteRows ("tab_dett_offerte", "id_offerta = '" . $_GET['del_off'] . "'", $db);

		$Result = "<label class=\"ok\">Offerta eliminata.</label>";
	}

	if (isset($_POST['ck_interval']))
	{
		//~ array_push($condizione, "ORDER BY id_off desc LIMIT 15");
	}

	if (isset($_POST['ck_cliente']))
	{
		$val_cli = ModoRicerca($_POST['val_cliente'], "tutto");
		$val_cli = ExpandSearch($val_cli);
		array_push($condizione, "tab_anagrafica.ragione_sociale LIKE '$val_cli'"
			. " AND tab_offerte.id_cliente = tab_anagrafica.id_anag");
	}

	foreach ($condizione as $key => $field)
	{
		if (($key+1) != count($condizione))
		{
			$campi .= $field . " AND ";
		}else{
			$campi .= $field;
		}
	}

	$campi .= " group by tab_offerte.id_off ";

	if (isset($_POST['ck_last15']))
	{
		//~ deve essere l'ultima condizione da controllare.
		$campi .= " ORDER BY data desc LIMIT 15";
	}

	if (!empty($_POST))
	{
		$Offerte = GetRows ("tab_offerte, tab_anagrafica", $campi, "data desc", $db);
	}
?>


	<form method="post" action="<?php echo $_SERVER['REQUEST_URI']; ?>">
		<table cellpadding="1" cellspacing="1" border="1" style="width: 100%;"
			rules="none" frame="box">
			<tr>
				<td colspan="5">
					<h3>Seleziona i campi fra i quali ricercare:</h3>
				</td>
			</tr>
			<tr>
				<td>
					<input name="ck_last15" type="checkbox" value="last15"
						title="Mostra solo le ultime 15 offerte."
						<?php if ($_POST['ck_last15'] != "") { echo "checked=\"checked\""; } ?> />
						<label>Ultime 15 offerte</label>

					<input name="ck_interval" type="checkbox" value="interval"
						<?php if ($_POST['ck_interval'] != "") { echo "checked=\"checked\""; } ?> />
						<label>Intervallo:</label>

					<input name="val_data_i" type="text" style="width: 7em;" title="Data iniziale"
							onfocus="showCalendarControl(this);"
							value="<?php if (isset($_POST['val_data_i'])) { echo $_POST['val_data_i']; } ?>" />

					<input name="val_data_f" type="text" style="width: 7em;" title="Data finale"
						onfocus="showCalendarControl(this);"
						value="<?php if (isset($_POST['val_data_f'])) { echo $_POST['val_data_f']; } ?>" />

					<input name="ck_cliente" type="checkbox" value="cliente"
						<?php if ($_POST['ck_cliente'] != "") { echo "checked=\"checked\""; } ?> />
						<label>Cliente</label>
					<input name="val_cliente" type="text" style="width: 20em;"
						value="<?php if (isset($_POST['val_cliente'])) { echo $_POST['val_cliente']; } ?>" />

					<a onclick="Javascript: posta('0', '<?php echo $_SERVER['REQUEST_URI']; ?>')"
						style="cursor: pointer;">
						<img src="/Images/Links/find.png" alt="Cerca" title="Avvia la ricerca"
							style="border: 0px; width: 16px; height: 16px;" />
					</a>
				</td>
			</tr>
		</table>
	</form>

	<br />

	<?php if (isset($_POST) && (count($_POST) > 0)) { ?>
		<?php if (count($Offerte) > 0) { ?>
			<table cellpadding="1" cellspacing="1" border="1" style="width: 100%;"
				rules="none" frame="box" class="dettaglio">
				<tr><th colspan="8"><br /></th></tr>
				<tr>
					<th colspan="8" style="text-align: right;">
						Risultati corrispondenti:&nbsp;
						<label class="ok"><?php echo count($Offerte); ?></label></th>
				</tr>
				<tr><th colspan="8"><br /></th></tr>
				<tr>
					<th>Data:</th>
					<th>Totale:</th>
					<th>Cliente:</th>
					<th>Utente:</th>
					<th></th>
					<th></th>
					<th></th>
					<th></th>
				</tr>
				<tr><th colspan="8"><hr /></th></tr>
				<?php foreach ($Offerte as $key => $field) { ?>
					<tr>
						<td><?php echo Inverti_DataTime($field['data']); ?></td>
						<td><?php echo $field['tot']; ?></td>
						<td>
							<?php if ($field['id_cliente'] == 0)
							{
								echo "-";
							}else{
								$anag = GetRows ("tab_anagrafica", "id_anag = '"
									. $field['id_cliente'] . "'", "", $db);
								echo $anag[0]['ragione_sociale'];
							} ?>
						</td>

						<td>
							<?php $user = GetRows ("tab_utenti", "id_user = '"
									. $field['id_utente'] . "'", "", $db);
								echo $user[0]['user']; ?>
						</td>

						<th style="text-align: center; width: 24px;">
							<a onclick="Javascript: popUpWindow('DettaglioOfferta', 'frm_vis_dettaglio_off.php?id_off=<?php
								echo $field['id_off']; ?>', '250', '250', '650', '250')"
								title="Visualizza Dettaglio" style="cursor: pointer;">
									<img src="..//Images/Links/db/info.png" alt="Dett"
										style="border: 0px; width: 14px; height: 14px;" />
							</a>
						</th>
						<th style="text-align: center; width: 24px;">
							<a onclick="Javascript: popUpWindow('Pdf', 'frm_esporta_pdf.php?id_off=<?php echo $field['id_off']; ?>',
									'350', '350', '850', '500'); void(0);"
								title="Esporta in Pdf" style="cursor: pointer;">
									<img src="/Images/Links/pdf.png" alt="Pdf"
										style="border: 0px; width: 14px; height: 14px;" />
							</a>
						</th>
						<th style="text-align: center; width: 24px;">
							<a href="?act=assembla&amp;drop=0&amp;id_off=<?php
								echo $field['id_off']; ?>" title="Modifica" style="cursor: pointer;">
								<img src="/Images/Links/edit.png" alt="M"
									style="border: 0px; width: 14px; height: 14px;" />
							</a>
						</th>
						<th style="text-align: center; width: 24px;">
							<a href="Javascript: go_conf2('Eliminare l\'offerta?',
								'?act=ricerca_off&amp;del_off=<?php echo $field['id_off']; ?>'); void(0);"
								title="Elimina" style="cursor: pointer;">
									<img src="/Images/Links/del.png" alt="D" />
							</a>
						</th>

					</tr>
				<?php } ?>
			</table>
		<?php }else{ ?>
			<table cellpadding="1" cellspacing="1" border="0" style="width: 100%;">
				<tr>
					<th style="text-align: right;">
						<label class="err">Nessun risultato corrispondente.</label>
					</th>
				</tr>
			</table>
		<?php } ?>
	<?php } ?>

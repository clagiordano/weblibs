<?php
date_default_timezone_set('Europe/Rome');
	if (!isset($_GET['tomod'])) { $_GET['tomod'] = ""; }
  if (!isset($_GET['act'])) { $_GET['act'] = ""; }
  if(isset($_GET['dell_att_news']))
  {
		$ar_update['allegato'] = '';	
    $Campi = $d->UpdateRow ($ar_update, '', "tab_news", "id_news = '"
				.$_GET['dell_att_news'] . "'");  
  }
	if (isset($_GET['del']))
	{
		$Status = $d->DeleteRows ("tab_news", "id_news = '" . $_GET['del'] . "'");
	}
	if ((isset($_GET['salva']))&&(!isset($_GET['add_foto'])))
	{
		$errori = "";
		$Esclusioni= array("add_news" => "Salva");

		if (trim($_POST['titolo_it']) == "")
		{
			$errori .= "Errore, non è stata impostata una voce per il titolo delle News.<br />";
		}
		if (trim($_POST['data']) == "")
		{
			$errori .= "Errore, non è stata impostato la data per la news.<br />";
		}else{
      $_POST['data'] = $d->Inverti_Data($_POST['data']);
    }
		if (trim($_POST['testo_it']) == "")
		{
			$errori .= "Errore, non è stata impostato il testo della news.<br />";
		}
		if (($_GET['salva'] == "salva") && ($errori == ""))
		{
			$Campi = $d->SaveRow ($_POST, $Esclusioni, "tab_news");
      $_GET['act'] = '';
		}

		if (($_GET['salva'] == "modifica") && ($errori == ""))
		{
			$Campi = $d->UpdateRow ($_POST, $Esclusioni, "tab_news", "id_news = '"
				.$_GET['tomod'] . "'");
		}
		if ($errori != "")
		{
			?><label class="err"><?php echo $errori; ?></label><?php
		}
     
	}
  if(!isset($_GET['att_news'])){	
	?>

	<?php
  
  if(( $_GET['tomod'] == "")&&($_GET['act']!='AddNew')){
	$News = $d->GetRows('*','tab_news','','','data');
  
	 if (count($News) != 0) { 
     if(!isset($_GET['act'])){$_GET['act']='';}
  ?>
<table style="width: 100%;" class="form">
  <?php
    $d->ShowEditBar("Gestione News:", $_GET['tomod'],
        "new", 'AddNew&gruppo=news'); 
    ?>
</table>

		<div id="search-result-6r">
			<form method="post" action="<?php echo $_SERVER['REQUEST_URI']; ?>">
				<table style="width: 100%;" class="dettaglio" >
					<tr>
						<th><label>Titolo :</label></th>
						<th><label>Data:</label></th>
						<th><label>Allegato:</label></th>
						<th><label>Sato:</label></th>
						<th><label>Foto:</label></th>
						<th colspan="2"></th>
					</tr>
					<tr><th colspan="8"><hr /></th></tr>
					<?php foreach ($News as $key => $fieldm) {
								$style = " class=\"ok\""; ?>
						<tr>
							<td >
								<a onclick="Javascript: go('?gruppo=news&act=adm_news&amp;tomod=<?php
									echo $fieldm['id_news']; ?>'); void(0);"
									title="Modifica Voce" <?php echo $style; ?>>
									<?php echo $fieldm['titolo_it']; ?>
								</a>
							</td>
							<td><?php echo $d->Inverti_Data($fieldm['data']); ?></td>
							<td>
                  <?php if($fieldm['allegato']==''){
                  ?>   
								<a onclick="Javascript: go('?gruppo=att_news&act=adm_news&att_news=<?php
									echo $fieldm['id_news']; ?>')" title="Carica allegato alla new" >
									<img src="../Images/Links/edit.png" alt="No allegato" title="No allegato"
										style="border: 0px; width: 14px; height: 14px; vertical-align: middle;" />
								</a>                  
                  <?php    
                  }else{
                      $titolo = $d->GetRows('titolo','tab_download',"id_download ='".$fieldm['allegato']."'",'','data');
                      echo $titolo[0]['titolo'];
                      ?>
                <div style="float: right; margin-right: 20px;">
  								<a onclick="Javascript: go('?gruppo=news&dell_att_news=<?php
									echo $fieldm['id_news']; ?>')" title="Carica allegato alla new" >
                  
									<img src="../Images/Links/cancel.png" alt="Delete" title="Elimina collegamento allegato"
										style="border: 0px; width: 14px; height: 14px; vertical-align: middle; margin-left: 2px;" />
								</a>
                </div>           
                  <?php } ?>
              
              </td>
							<td><?php echo $fieldm['attivo']; ?></td>
							<td>
                <?php if($fieldm['foto']!='')
                  {
                    $bgc = 'green';
                    $ftitolo = $fieldm['foto'];
                  }else{
                    $bgc = 'red';
                     $ftitolo = "No foto";
                  }
                  ?>
                <a onclick="Javascript: go('?gruppo=news&act=adm_news&amp;tomod=<?php
									echo $fieldm['id_news']; ?>&add_foto=1'); void(0);" title="Carica allegato alla new" >
                	<img src="../Images/Links/foto.png" alt="<?php echo $ftitolo;?>" title="<?php echo $ftitolo;?>"
										style="border: 0px; background: <?php echo $bgc;?>;width: 15px; vertical-align: middle;" />
                </a>
              </td>

							<td style="text-align: left;">
								<a onclick="Javascript: go('?gruppo=news&act=adm_news&amp;tomod=<?php
									echo $fieldm['id_news']; ?>')" title="Vai sottomenu" >
									<img src="../Images/Links/edit.png" alt="Dettaglio" title="Dettaglio"
										style="border: 0px; width: 14px; height: 14px; vertical-align: middle;" />
								</a>
								&nbsp;
								<a onclick="javascript: go_conf2('Eliminare la voce del menù?',
									'?gruppo=news&act=adm_news&amp;del=<?php echo $fieldm['id_news']; ?>'); void(0);" title="Elimina" >
									<img src="../Images/Links/del.png" alt="Elimina" title="Elimina"
										style="border: 0px; width: 14px; height: 14px; vertical-align: middle;" />
								</a>
							</td>
						</tr>
					<?php } ?>
					<tr><th colspan="8"><hr /></th></tr>
				</table>
			</form>
		</div>
	<?php }
			}else{
        if(!isset($_GET['add_foto'])){
				$News = $d->GetRows('*','tab_news',"id_news = '".$_GET['tomod'] ."'",'','data');
				include('box/News.php');
        }else{
          include('box/News_addfoto.php');
        }
		 }?>

<?php }else{
    include('form_download.php');
}
?>

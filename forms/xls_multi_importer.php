<?php 
session_start(); 

ini_set('display_startup_errors', 1);
ini_set('display_errors', 1);
error_reporting(E_ALL);
            
	define("XLS_IMPORT_PATH", "tmp/file_importato.xls");
	require_once PHP_PATH . "../php/excel_reader2.php";

	function getColumnInterval($String)
	{
		/**
		 * splitto la stringa contenente l'elenco delle colonne
		 * secondo la virgola
		 * es. (1,3,5-8) diventa:
		 * Array ([0] => 1, [1] => 3, [2] => 5-8)
		 */
		$temp_cols = explode(",", $String);
		$Cols = array();

		//~ echo "<pre>temp_cols";
			//~ print_r($temp_cols);
		//~ echo "</pre>"; 

		/**
		 * attraverso l'array temporaneo verificando ed aggiungendo
		 * i valori all'array reale
		 */
		foreach ($temp_cols as $key => $field)
		{
			//~ echo "[Debug]: field: $field <br />";
			//~ echo "[Debug]: check: " . strpos($field, "-") . " <br />";
			if (is_numeric($field) && (strpos($field, "-") == ""))
			{
				//~ echo "[Debug]: il valore $field è numerico e non contiene '-' <br />";
				/* 
				 * se il valore è effettivamente un numero e
				 * non contiene il carattere '-'
				 * allora aggiungo il valore all'array reale 
				 */
				array_push($Cols, $field);
			}
			else if (strpos($field, "-") != "")
			{
				//~ echo "[Debug]: il valore $field contiene '-' <br />";
				/*
				 * se il valore contiene la stringa '-'
				 * splitto la stringa secondo il carattere '-'
				 * e genero l'intervallo
				 */
				$tmp_interval = explode("-", $field);
				if ($tmp_interval[0] < $tmp_interval[1])
				{
					for ($i = $tmp_interval[0]; $i <= $tmp_interval[1]; $i++)
					{
						array_push($Cols, $i);
					}
				}else{
					for ($i = $tmp_interval[0]; $i >= $tmp_interval[1]; $i--)
					{
						array_push($Cols, $i);
					}
				}
				//~ echo "<pre> tmp_interval ";
					//~ print_r($tmp_interval);
				//~ echo "</pre>"; 
			}
			else
			{
				// altrimenti lo salto e stampo il debug
				//echo "[Debug]: La colonna $field inserita non è valida <br />";
				echo "Errore, La colonna $field inserita non è valida <br />";
			}
		}
		
		return $Cols;
	}

	if (isset($_FILES['file_up']['tmp_name'])
		&& ($_FILES['file_up']['tmp_name'] != ""))
	{
		copy($_FILES['file_up']['tmp_name'], XLS_IMPORT_PATH);
		//~ copy($_FILES['file_up']['tmp_name'], XLS_IMPORT_PATH . $_FILES['file_up']['name']);
		//~ echo "[Debug]: upload completato. <br />";
	}

	if (!file_exists(XLS_IMPORT_PATH))
	{
		$file_stat =  "<label class=\"err\">Non è ancora stato eseguito l'upload di un file.</label><br />";
	}
    else
    {
		$file_stat =  "<label class=\"ok\">E' già stato eseguito l'upload di un file.</label><br />";
	}

	if (file_exists(XLS_IMPORT_PATH))
	{        
		// imposto i parametri di connessione statici in modo da sopprimere
		// quella parte di form
		$_POST['db_host'] = "localhost";
		$_POST['db_user'] = "gestore";
		$_POST['db_pass'] = "polipo";
		$_POST['db_port'] = "3306";
		
		if ($_POST['db_host'] == "")
		{
			$_POST['db_host'] = "localhost";
		}
		
		if ($_POST['db_port'] == "")
		{
			$_POST['db_port'] = "3306";
		}

		if (!$db = mysql_connect($_POST['db_host'], $_POST['db_user'], $_POST['db_pass']))
		{
			$db_status = "connessione al database non riuscita.";
		}

		$q = "show databases";
		$r = mysql_query($q, $db);

		$db_list = array();

		while ($Row = mysql_fetch_assoc($r))
		{
			array_push($db_list, $Row);
		}

		if ($_POST['db_name'] != "-")
		{
			mysql_select_db($_POST['db_name']);

			$q = "show table status";
			$r = mysql_query($q, $db);

			$db_list_tab = array();

			while ($Row = mysql_fetch_assoc($r))
			{
				array_push($db_list_tab, $Row);
			}
		}else{
			mysql_select_db(Default_DB);
		}

		// Legge il file xls per l'importazione:
		$data = new Spreadsheet_Excel_Reader(XLS_IMPORT_PATH, false);
		/*
			echo $data->setOutputEncoding('CP1251');
			echo $data->read();
		*/

		/*
		 *  $sheets  -->  'cells'  -->  row --> column --> Interpreted value
		 *      -->  'cellsInfo' --> row --> column --> 'type' - Can be 'date',
		 * 			'number', or 'unknown'
		 *  	--> 'raw' - The raw data that Excel stores for that data cell
		 */

		if (is_numeric(trim($_POST['sheet'])))
		{
			$_POST['sheet'] = trim($_POST['sheet']);
		}else{
			$_POST['sheet'] = 0;
		}

		if (is_numeric(trim($_POST['row_limit'])))
		{
			$_POST['row_limit'] = trim($_POST['row_limit']);
		}else{
			$_POST['row_limit'] = $data->sheets[$_POST['sheet']]['numRows'];
		}

		//~ echo "[Debug]: num cols: " . $data->colcount($_POST['sheet']) . " <br />";

		if (trim($_POST['col_list']) != "")
		{
			$_POST['col_list'] = trim($_POST['col_list']);

			//~ echo "[Debug]: col_list: " . $_POST['col_list'] . " <br />";
			$Cols = getColumnInterval($_POST['col_list']);
		}
		else
		{
			$Cols = array(1);
		}

		if (is_numeric(trim($_POST['top_offset'])))
		{
			$_POST['top_offset'] = trim($_POST['top_offset']);
		}else{
			$_POST['top_offset'] = 1;
		}

		/*
		 * Check Profile:
		 */
		//~ switch($_POST['profile'])
		//~ {
			//~ case "request":
				/*
				 * estrai id della filiale tramite il codice filiale
				 * colonna 1 file request
				 */
				//~ break;
//~ 
			//~ case "magazzino":
				//~ break;

			//~ default:
				// case default
				//~ break;
		//~ }


		$Anteprima = "<table style=\"width: 100%; border: 1px solid black;\" rules=\"all\">";

		// Creazione selettore campi colonne:
		$Anteprima .= "<tr>";
		$Anteprima .= "<td>Riga:</td>";

		$q = "describe " . $_POST['db_dest_table'];
		$r = mysql_query($q, $db);
		$Fields = array();
		while ($Row = mysql_fetch_assoc($r))
		{
			array_push($Fields, $Row['Field']);
		}

		foreach ($Cols as $key => $field)
		{
			$Anteprima .= "<td>"
				. "<select name=\"dest_field$key\">";
				foreach ($Fields as $k => $f)
				{
					$Anteprima .= "<option value=\"$f\"";
					//~ echo "[Debug]: dest_field$key: $f <br />";
					if ($_POST['dest_field' . $key] == $f) 
					{ 
						$Anteprima .= " selected=\"selected\""; 
					}
					
					$Anteprima .= ">$f</option>";
				}

			$Anteprima .= "</select>";
			$Anteprima .= " ( colonna $field '" . chr($field + 64) .  "' )";
		}
		$Anteprima .= "</tr>";

		// Dettaglio
		for ($i = $_POST['top_offset']; $i <= ($_POST['row_limit']); $i++)
		{
			$Anteprima .= "<tr><td>$i)</td>";
			foreach ($Cols as $key_col => $col)
			{
				$Cella = trim($data->sheets[$_POST['sheet']]['cells'][$i][$col]);

				/*
				 * verifica se il valore è una data ed eventualmente
				 * lo converte nel formato mysql. */
				if (preg_match("(" . RegDataIt . ")", $Cella))
				{
					$Cella = Inverti_Data($Cella, "-", "/");
				}

				//~ if ($Cella == "palermo") { $Cella = 1; }
				//~ if ($Cella == "catania") { $Cella = 2; }

				/*
				 * verifica se il valore è un codice di request ed
				 *  eventualmente recupera l'id dalla tabella corrispondente. */
				//~ if (preg_match("(CR\d+)", $Cella))
				//~ {
					//~ $req = GetRows ("tab_request", "request = '"
						//~ . $Cella . "'", "", $db, 1);
					//~ $Cella = $req[0]['id_request'];
				//~ }
//~ 
				//~ if (preg_match("(BD\d+)", $Cella))
				//~ {
					//~ $req = GetRows ("tab_filiali", "cod_fil = '"
						//~ . $Cella . "'", "", $db, 1);
					//~ $Cella = $req[0]['id_filiale'];
				//~ }

				/*
				 * aggiunge la stringa eventualmente convertita all'output: */
				 $Anteprima .= "<td>" . $Cella . "</td>";

			}

			$Anteprima .= "</tr>";
		}
		$Anteprima .= "</table>";
	}
    
	if (isset($_GET['salva']) 
		&& ($_GET['salva'] == "salva") 
		&& !isset($_POST['bt_anteprima']))
	{
		//~ echo "[Debug]: Avvio l'importazione dei dati <br />";		
		$mysqli = new mysqli($_POST['db_host'], $_POST['db_user'], $_POST['db_pass'], $_POST['db_name'], $_POST['db_port']);
		
		if ($mysqli->connect_errno) {
			echo "Failed to connect to MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
		}
		//~ echo "[Debug]: host_info: ".  $mysqli->host_info . "<br />";
		
		$ImportColumns = getColumnInterval($_POST['col_list']);
		
		$DestFields = "(";
		foreach ($_POST as $key => $field)
		{
			if (preg_match( "(^dest_field(\d+)$)", $key, $Matches ))
			{
				//~ echo "<pre> Matches";
					//~ print_r($Matches);
				//~ echo "</pre>";
				$DestFields .= $field . ", ";
			}
		}
		$DestFields = preg_replace("(, $)", ") ", $DestFields);
		//~ echo "[Debug]: DestFields: $DestFields <br />";
		
		for ($i = $_POST['top_offset']; $i <= ($_POST['row_limit']); $i++)
		{
			//~ echo "[Debug]: Avvio importazione riga $i <br />";
			$Fields = "(";
			foreach ($ImportColumns as $key_col => $col)
			{
				$Cella = trim($data->sheets[$_POST['sheet']]['cells'][$i][$col]);

				/*
				 * verifica se il valore è una data ed eventualmente
				 * lo converte nel formato mysql. 
				 */
				if (preg_match("(" . RegDataIt . ")", $Cella))
				{
					$Cella = Inverti_Data($Cella, "-", "/");
				}
				
				$Fields .=  "'" . $Cella . "', ";
			}
			$Fields = preg_replace("(, $)", ");", $Fields);
			//~ echo "[Debug]: Fields: $Fields <br />";
			
			$RowQuery = "INSERT INTO " . $_POST['db_dest_table'] . " " . $DestFields;
			$RowQuery .= "VALUES " . $Fields;
			//~ echo "[Debug]: RowQuery: $RowQuery <br />";
			
			if ($mysqli->query($RowQuery))
			{
				echo "<label class=\"ok\">Riga $i inserita correttamente in tabella " . $_POST['db_dest_table'] . "</label><br />";
				//~ echo "[Debug]: Dati: $Fields <br />";
			}else{
				echo "<label class=\"err\">Errore durante l'inserimento della riga $i in tabella " . $_POST['db_dest_table'] . "</label><br />";
				//~ echo "[Debug]: Dati: $Fields <br />";
				echo "<label class=\"err\">L'errore riporta (" .  $mysqli->errno . ") " . $mysqli->error . "</label><br />";
			}
			
		}
	}
	

?>

<form enctype="multipart/form-data" method="post">
	<table  style="width: 100%; border: 1px solid black;">
		<tr>
			<th style="text-align: left;">
				Upload File: &nbsp;
				<input name="file_up" type="file" size="30" />
				<input type="submit" value="Upload" />
			</th>
		</tr>

		<tr><td colspan="2"><?php echo $file_stat; ?></td></tr>
	</table>
</form>

<br />

<table style="width: 100%; border: 1px solid black;">
	<form method="post">
		<tr>
			<td style="vertical-align: top;">
				<table style="width: 100%;">
					<tr>
						<td colspan="8">Opzioni importazione file:</td>
					</tr>
					<!--<tr>
						<td style="text-align: right;">Stato Upload:</td>
						<td><?php //echo $file_stat; ?></td>
					</tr>!-->
					<!--<tr>
						<td style="text-align: right;">File in Uso:</td>
						<td><?php //echo XLS_IMPORT_PATH; ?></td>
					</tr>!-->

					<tr>
						<td style="text-align: right;">Seleziona Foglio:</td>
						<td>								
							<select name="sheet" style="width: 10em;"
								onchange="Javascript: posta('1', '<?php echo $_SERVER['REQUEST_URI']; ?>'); void(0);">
								<?php for ($i = 0; $i < count($data->boundsheets); $i++) { ?>
									<option value="<?php echo $i; ?>"
										<?php if ($_POST['sheet'] == $i) { echo "selected=\"selected\""; } ?> >
											<?php echo $i + 1; ?>
									</option>
								<?php } ?>
								
							</select>
						</td>
					</tr>

					<tr>
						<td style="text-align: right;">Lista Colonne:</td>
						<td>
							<input name="col_list" type="text"
								value="<?php if ($_POST['col_list']) { echo $_POST['col_list']; } ?>" />
								
								<?php if (isset($data)) { ?> 
									<label>Colonne totali: <?php echo $data->colcount($_POST['sheet']); ?></label>
								<?php } ?>
						</td>
					</tr>

					<tr>
						<td style="text-align: right;">Offset Iniziale:</td>
						<td>
							<input name="top_offset" type="text"
								value="<?php if ($_POST['top_offset']) { echo $_POST['top_offset']; } ?>" />
								
						</td>
					</tr>

					<tr>
						<td style="text-align: right;">Limite Righe:</td>
						<td>
							<input name="row_limit" type="text"
								value="<?php if ($_POST['row_limit']) { echo $_POST['row_limit']; } ?>" />
								
								<?php if (isset($data)) { ?> 
									<label>Righe totali: <?php echo $data->rowcount($_POST['sheet']); ?></label>
								<?php } ?>
						</td>
					</tr>

					<!--<tr>
						<td style="text-align: right;">Profilo Importazione:</td>
						<td>
							<select name="profile">
								<option value="default"
									<?php //if ($_POST['profile'] == "default") {
										//echo "selected=\"selected\""; }?> >Default</option>
								<option value="request"
									<?php //if ($_POST['profile'] == "request") {
										//echo "selected=\"selected\""; }?> >Request</option>
								<option value="magazzino"
									<?php //if ($_POST['profile'] == "magazzino") {
										//echo "selected=\"selected\""; }?> >Magazzino</option>
							</select>
						</td>
					</tr>!-->
				</table>
			</td>

			<td>
				<table style="width: 100%;">
					<!--<tr>
						<td colspan="8">Parametri connessione database server:</td>
					</tr>
					<tr>
						<td style="text-align: right;">User:</td>
						<td>
							<input name="db_user" type="text"
								value="<?php //if ($_POST['db_user']) { echo $_POST['db_user']; } ?>"/>
						</td>
					</tr>

					<tr>
						<td style="text-align: right;">Password:</td>
						<td>
							<input name="db_pass" type="password"
								value="<?php //if ($_POST['db_pass']) { echo $_POST['db_pass']; } ?>" />
						</td>
					</tr>

					<tr>
						<td style="text-align: right;">Hostname:</td>
						<td>
							<input name="db_host" type="text"
								value="<?php //if ($_POST['db_host']) { echo $_POST['db_host']; } ?>" />
						</td>
					</tr>

					<tr>
						<td style="text-align: right;">Porta:</td>
						<td>
							<input name="db_port" type="text"
								value="<?php //if ($_POST['db_port']) { echo $_POST['db_port']; } ?>" />
						</td>
					</tr>!-->

					<tr>
						<td style="text-align: right;">Selezione database:</td>
						<td>
							<select name="db_name"
								onchange="javascript: posta('1', '<?php echo $_SERVER['REQUEST_URI']; ?>'); void(0);" >
								<option value="-">-</option>
								<?php foreach ($db_list as $key => $field) { ?>
									<option value="<?php echo $field['Database']; ?>"
										<?php if ($_POST['db_name'] == $field['Database']) { echo "selected=\"selected\""; } ?> >
										<?php echo $field['Database']; ?>
									</option>
								<?php } ?>
							</select>
						</td>
					</tr>

					<tr>
						<td style="text-align: right;">Selezione tabella:</td>
						<td>
							<select name="db_dest_table"
								onchange="javascript: posta('1', '<?php echo $_SERVER['REQUEST_URI']; ?>'); void(0);" >
								<option value="-">-</option>
								<?php foreach ($db_list_tab as $key => $field) { ?>
									<option value="<?php echo $field['Name']; ?>"
										<?php if ($_POST['db_dest_table'] == $field['Name']) { echo "selected=\"selected\""; } ?> >
										<?php echo $field['Name']; ?>
									</option>
								<?php } ?>
							</select>
						</td>
					</tr>

					<tr>
						<td style="text-align: right;" colspan="2">
							<input name="bt_anteprima" type="submit" value="Anteprima" />
						</td>
					</tr>
				</table>
				
				<?php if (isset($_POST['bt_anteprima'])) { ?>
					
					<tr><td colspan="2"><br /></td></tr>
					<tr><td colspan="2"><?php echo $Anteprima; ?></td></tr>
				
					<tr>
						<td colspan="2">
							<input name="bt_importa" type="button" value="Conferma importazione dati" 
								onclick="Javascript: conferma_salva('1', 
									'Importare i dati nella tabella scelta con la configurazione attuale?', 
									'?act=multi_imp&salva=salva')" />
						</td>
					</tr>
				<?php } ?>
			</form>
		</td>
	</tr>
</table>


<?php //echo $data->dump(true,true); ?>
</body>

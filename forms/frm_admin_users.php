<?php
/*
 * frm_admin_menu.php
 *
 * Copyright 2008 Claudio Giordano <claudio.giordano@autistici.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 */
	$errori = "";

	if (!isset($_GET['tomod']))
	{
		//echo "[Debug]: non esiste tomod <br />";
		$_GET['tomod'] = "";
	}else{
		//echo "[Debug]: esiste tomod: " . $_GET['tomod'] . " <br />";
	}

	if (empty($_POST) && is_numeric($_GET['tomod']))
	{
		//echo "[Debug]: importo i dati per la modifica <br />";
		$mod = $d->GetRows ("*", "tab_utenti",
			"id_user = '" . $_GET['tomod'] . "'");
		$_POST['tipo'] 			= $mod[0]['tipo'];
		$_POST['user'] 			= $mod[0]['user'];
		$_POST['desc_user'] = $mod[0]['desc_user'];
		$_POST['stato'] 		= $mod[0]['stato'];
	}

	if (!isset($_POST['tipo']) 
		|| ( !is_numeric($_POST['tipo'])) )
	{
		$_POST['tipo'] = "%";
	}

	if (isset($_GET['DEL']) && is_numeric($_GET['DEL']))
	{
		$Status = $d->DeleteRows ("tab_utenti", "id_user = '" . $_GET['DEL'] . "'");
	}


	if ((!empty($_POST)) && isset($_GET['salva']))
	{
		$errori = "";

		if (($_POST['tipo'] == "%") || ($_POST['tipo'] == "-"))
		{
			$errori .= "Errore, non è stato selezionato un gruppo per l'utente.<br />";
		}

		if (trim($_POST['user'] == "") || (strlen($_POST['user']) <= 3))
		{
			$errori .= "Errore, il nome utente non è valido.<br />";
		}

		if (isset($_POST['mpass']) && ($_POST['mpass'] == "0"))
		{
			$Esclusioni = array('mpass' => '0');
			if (trim($_POST['pass'] == "") || (strlen($_POST['pass']) < 4))
			{
				$errori .= "Errore, la password non è valida.<br />";
			}else{
				$_POST['pass'] = md5($_POST['pass']);
			}
		}else{
			//echo "Debug => escludo il campo pass dalla modifica <br />";
			$Esclusioni = array('pass' => '');
		}

		if (!isset($_POST['stato']))
		{
			$_POST['stato'] = "1";
		}

		$ck_uniq =  $d->GetRows ("*", "tab_utenti", "user = '" . $_POST['user'] .  "'");
		if (count($ck_uniq) != 0 && ($_GET['salva'] == "salva"))
		{
			$errori .= "Errore, il nome utente è già in uso.<br />";
		}

		if (($_GET['salva'] == "salva") && ($errori == ""))
		{
			$Status = $d->SaveRow ($_POST, $Esclusioni, "tab_utenti");
			$result = $Status[0];
		}

		if (($_GET['salva'] == "modifica") && ($errori == ""))
		{
			$Status = $d->UpdateRow ($_POST, $Esclusioni,  "tab_utenti", "user = '"
				. $_POST['user'] .  "'", 1);
			$result = $Status;
		}
	}


	if(isset($_GET['ID']) && (is_numeric($_GET['ID']))
		&& isset($_GET['ST']) && (is_numeric($_GET['ST'])))
	{
		$Status = $d->UpdateRow (array('stato' => $_GET['ST']), "",
			"tab_utenti", "id_user = '" . $_GET['ID'] . "'", 1);
	}

	$Groups = $d->GetRows ("*", "tab_tipi", "", "tipo");

	$Users = $d->GetRows ("*", "tab_utenti u, tab_tipi t",
		"u.tipo = t.id_tipo AND u.tipo like '" . $_POST['tipo'] . "'",
		"", "u.user", 1);
?>


<section id="search-form">
	<form method="post" action="<?php echo $_SERVER['REQUEST_URI']; ?>">
		<table class="form" style="width: 100%;">

			<?php $d->ShowEditBar("Gestione Utenti: ", $_GET['tomod'],
				"new,save", $_GET['act']); ?>

			<tr>
				<td colspan="2">
					<select name="tipo" onchange="javascript: posta('0', '?act=adm_user<?php
						if (isset($_GET['tomod'])) { echo "&amp;tomod=" . $_GET['tomod']; } ?>'); void(0);" >
						<option value="-">Seleziona un gruppo</option>
						<option value="-" ></option>
						<option value="%" <?php if (!empty($_POST['tipo']) && ($_POST['tipo'] == "%")) { echo "selected=\"selected\""; } ?>>
							Tutti
						</option>
						<?php foreach ($Groups as $key => $field) { ?>
							<option value="<?php echo $field['id_tipo']; ?>"
								<?php if(!empty($_POST['tipo']) && ($_POST['tipo'] == $field['id_tipo']))
									{ echo "selected=\"selected\""; } ?> >
								<?php echo $field['tipo']; ?>
							</option>
						<?php } ?>
					</select>

					<label>Utente:</label>
					<input name="user" type="text" pattern="^[a-zA-Z0-9-]+$"
						value="<?php if (isset($_POST['user'])) { echo $_POST['user']; } ?>" />

					<label>Desc. Utente:</label>
					<input name="desc_user" type="text" pattern="^[a-zA-Z0-9-\ \.]+$"
						value="<?php if (isset($_POST['desc_user'])) { echo $_POST['desc_user']; } ?>" />

					<label>Password:</label>
					<input name="pass" type="password"
						value="<?php if (isset($_POST['pass'])) { echo $_POST['pass']; } ?>" />

					<label>Stato:</label>
					<input name="stato" type="checkbox" value="0"
						<?php if (isset($_POST['stato'])
							&& ($_POST['stato'] == "0")) { echo "checked=\"checked\""; } ?> />

					<label>Modifica Password:</label>
					<input name="mpass" type="checkbox" value="0" />
				</td>
			</tr>
			<?php if ($errori != "") { ?>
				<tr>
					<td><label class="err"><?php echo $errori; ?></label></td>
				</tr>
			<?php } ?>

			<?php if (isset($result)) { ?>
				<tr>
					<td><label class="ok"><?php echo $result; ?></label></td>
				</tr>
			<?php } ?>
		</table>
	</form>
</section>

<div id="search-result-2r">
	<form method="post" action="#">
		<table style="width: 100%;" class="dettaglio">
			<?php if(count($Users) > 0) { ?>
				<tr>
					<th>Utente:</th>
					<th>Desc. Utente:</th>
					<th>Gruppo:</th>
					<th>Stato:</th>
					<th><!-- links !--></th>
				</tr>
				<tr><th colspan="5"><hr /></th></tr>
					<?php //while($row_users = mysql_fetch_assoc($r_users)) {
						foreach ($Users as $key => $field) {

						$id_user = $field['id_user'];
						$user = $field['user'];
						$ddelconferma = "Eliminare  l\'utente: $user?";
						$delurl = "?act=adm_user&amp;DEL=$id_user";

						if($field['stato'] == 0)
						{
							$stato = 'title="L\'utente è attivo, click per disablitarlo"'
								. ' checked="checked"';
							$dconferma = "Disabilitare l\'utente: $user?";
							$class_user = "ok";
							$url = "?act=adm_user&amp;ID=$id_user&amp;ST=1";
						}else
						{
							$stato = 'title="L\'utente non è attivo, click per ablitarlo"';
							$dconferma = "Abilitare l\'utente: $user?";
							$class_user = "err";
							$url = "?act=adm_user&amp;ID=$id_user&amp;ST=0";
						}
					?>
					<tr>
						<td>
							<a onclick="Javascript: go('?act=adm_user&amp;tomod=<?php
								echo $field['id_user']; ?>'); void(0);">
								<label class="<?php echo $class_user; ?>">
									<?php echo $user; ?>
								</label>
							</a>
						</td>
						<td><?php echo $field['desc_user']; ?></td>
						<td><?php echo $field['tipo']; ?></td>

						<td style="text-align: center;">
							<?php if($user != "admin") { ?>
								<input name="CHK<?php echo $id_user; ?>" type="checkbox" <?php echo $stato; ?>
									onclick="Javascript: go_conf('<?php echo $dconferma; ?>' ,
										'<?php echo  $url; ?>', '?act=adm_user')" />
							<?php } ?>
						</td>
						<td style="text-align: left;">
							<?php if ($user != "admin") { ?>
								<a onclick="Javascript: go_conf2('<?php echo $ddelconferma; ?>' ,
									'<?php echo $delurl; ?>'); void(0);">
									<img src="/Images/Links/del.png" alt="Elimina" title="Elimina" />
								</a>
							<?php } ?>
						</td>
					</tr>
				<?php } ?>
			<?php } ?>
				<tr><th colspan="5"><hr /></th></tr>
		</table>
	</form>
</div>

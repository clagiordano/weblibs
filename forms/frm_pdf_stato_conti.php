<?php ob_end_clean();
	  $bullet = chr(149);
	  	 $deg = chr(186);
	 $TotDare = 0;
	$TotAvere = 0;
	  $SpDare = array(1, 2, 3, 9, 7, 6, 4, 5, 10, 8);
	 $SpAvere = array(14, 16, 20, 17, 18, 15, 42, 43, 19);

	global $Titolo;
	global $Tipo;

$pdf = new PDF();
$pdf->AliasNbPages();
$pdf->SetMargins(4, 4, 4);

//~ ri esegue la stessa query della ricerca
	$campi = $_SESSION['qpdf']['campi'];
	  $ord = $_SESSION['qpdf']['ord'];
$Risultati = GetRows ("view_piano_conti", $campi, $ord, $db);

if (isset($_GET['tipo']) && $_GET['tipo'] != "")
{
	$Tipo = $_GET['tipo'];

	switch ($_GET['tipo'])
	{
		case "movimenti":
			$Titolo = "Stampa Movimenti";
			$pdf->AddPage('L');	// L = landscape
			$pdf->SetTitle($Titolo);
			$pdf->SetSubject($Titolo);
			break;

		case "mastrini":
			$Titolo = "Stampa Mastrini";
			$pdf->AddPage();
			$pdf->SetTitle($Titolo);
			$pdf->SetSubject($Titolo);
			break;
	}
}

/*
 * 22/08/2009 01:04:49 CEST Claudio Giordano
 *
 * AddPage([string orientation ,[ mixed format]])
 * Cell(float w [, float h [, string txt [, mixed border [, int ln [, string align [
 * , boolean fill [, mixed link]]]]]]])
 * Line(float x1, float y1, float x2, float y2)
 */

class PDF extends FPDF {
	function Header()
	{
		global $Titolo;
		global $Tipo;

		/*
		 * 22/08/2009 00:23:24 CEST Claudio Giordano
		 *
		 * Intestazione tabella:
		 */
		switch ($Tipo)
		{
			case "movimenti":
				//<!-- Intestazione movimento !-->
				$this->SetFont('Arial', 'BI', 20);
				$this->Cell(290, 10, $Titolo, 0, 0, 'C');
				$this->Ln(20);

				$this->SetFont('Arial', 'B', 9);

				$this->Cell(45, 4, 'Num. e data reg:', 0, '', 'R');
				$this->Cell(100, 4, 'N' . $deg . ' Prot:', 0);
				$this->Cell(30, 4, 'N' . $deg . ' Doc:', 0);
				$this->Cell(45, 4, 'Data Documento:', 0);
				$this->Cell(45, 4, 'Causale Op:', 0);
				$this->Cell(25, 4, '', 0);
				$this->Ln();

				$this->Cell(45, 4, '', "B");
				$this->Cell(100, 4, 'Sottoconto:', "B");
				$this->Cell(30, 4, 'Descrizione:', "B");
				$this->Cell(45, 4, 'Dare:', "B");
				$this->Cell(45, 4, 'Avere', "B");
				$this->Cell(25, 4, 'Iva:', "B");
				$this->Ln();

				$this->Cell(290, 4, '', 0);
				$this->Ln();
				break;

			case "mastrini":
				//<!-- Intestazione mastrino !-->
				$this->SetFont('Arial', 'BI', 20);
				$this->Cell(200, 10, $Titolo, 0, 0, 'C');
				$this->Ln(20);

				$this->SetFont('Arial', 'B', 9);

				$this->Cell(200, 4, "Sottoconto:", 0, 0, '');
				$this->Ln();

				$this->Cell(40, 4, "Num. e Data Reg:", "B", 0, 'R');
				$this->Cell(50, 4, "Causale:", "B", 0, '');
				$this->Cell(50, 4, "Descrizione:", "B", 0, '');
				$this->Cell(20, 4, "Dare:", "B", 0, 'C');
				$this->Cell(20, 4, "Avere:", "B", 0, 'C');
				$this->Cell(20, 4, "Saldo:", "B", 0, 'C');
				$this->Ln();
				break;
		}
	}

	function Footer()
	{
		//Position at 1.5 cm from bottom
		$this->SetY(-15);
		$this->SetFont('Arial', '', 9);
		$this->Cell(40, 4, 'Data: ' . date('d/m/Y'), 0, '', 'L');
		$this->Cell(0, 4, 'Pagina ' . $this->PageNo() . "/{nb}", 0, 0 ,'C');
	}
}


/*
 * 22/08/2009 00:53:38 CEST Claudio Giordano
 *
 * Dettaglio tabella:
 */


foreach ($Risultati as $key => $field)
{
	if ($field['n_protocollo'] == "")
	{
		$Proto =  "-";
	}else{
		$Proto = $field['n_protocollo'];
	}

	switch ($_GET['tipo'])
	{
		case "movimenti":
			//<!-- Intestazione movimento !-->
			$pdf->SetFont('Arial', 'B', 9);

			$pdf->Cell(45, 4, $field['id_piano_conti'] . " - "
				. Inverti_Data($field['data_reg']), 0, '', 'R');
			$pdf->Cell(100, 4, $Proto, 0);
			$pdf->Cell(30, 4, $field['cod_documento'], 0);
			$pdf->Cell(45, 4, Inverti_Data($field['data_doc']), 0);
			$pdf->Cell(45, 4, $field['tipo_doc'], 0);
			$pdf->Cell(25, 4, '', 0);
			$pdf->Ln();

			$pdf->SetFont('Arial', '', 9);

			//<!-- Dettaglio movimento !-->
			$DettaglioOp = GetRows ("view_piano_conti", "id_piano_conti = '"
				. $field['id_piano_conti']. "'", "", $db);

			foreach ($DettaglioOp as $key_dett => $field_dett)
			{
				$TotDare += $field_dett['importo_d'];
				$TotAvere += $field_dett['importo_a'];

				$pdf->Cell(45, 4, $bullet, 0, '', 'R');
				if ($field_dett['req_anag'] == 0)
				{
					$Sottoconto = "(" . $field_dett['codice_sottoconto'] . ") "
						. $field_dett['ragione_sociale'];
				}else{
					$Sottoconto = "(" . $field_dett['codice_sottoconto'] . ") "
						. ucfirst(strtolower($field_dett['descr_sottoconto']));
				}

				if ($field_dett['importo_d'] == "0.00")
				{
					$field_dett['importo_d'] = "-";
				}

				if ($field_dett['importo_a'] == "0.00")
				{
					$field_dett['importo_a'] = "-";
				}


				$pdf->Cell(100, 4, $Sottoconto, 0);
				$pdf->Cell(30, 4, $field_dett['descr'], 0);
				$pdf->Cell(45, 4, $field_dett['importo_d'], 0, '', 'C');
				$pdf->Cell(45, 4, $field_dett['importo_a'], 0, '', 'C');

				if ($field_dett['id_iva'] != 0)
				{
					$Iva = $field_dett['desc_iva'];
				}else{
					$Iva = "-";
				}
				$pdf->Cell(25, 4, $Iva, 0);
				$pdf->Ln();
			}
			$pdf->Ln();
			break;

		case "mastrini":
			//<!-- Intestazione mastrino !-->
			$pdf->SetFont('Arial', 'B', 9);

			if (($field['req_anag'] == 0)
				&& ($_GET['tipo'] == "movimenti"))
			{
				$sc = "(" . $field['codice_sottoconto']
					. ") " . $field['ragione_sociale'];
			}else{
				$sc = "(" . $field['codice_sottoconto'] . ") "
					. ucfirst(strtolower($field['descr_sottoconto']));
			}

			$pdf->Cell(200, 4, $sc, 0, 0, '');
			$pdf->Ln();

			//<!-- Dettaglio mastrino !-->
			$cond_mastr = "id_sottoconto = '" . $field['id_sottoconto']
				. "' $descr_mastrino $interval_reg $interval_op";
			$DettaglioConto = GetRows ("view_piano_conti", $cond_mastr, "data_reg", $db);

			$pdf->SetFont('Arial', '', 9);

			foreach ($DettaglioConto as $key_dett => $field_dett)
			{
				$Sottoconto = $field_dett['id_piano_conti'] . " - "
					. Inverti_Data($field_dett['data_reg']);
				$TotDare += $field_dett['importo_d'];
				$TotAvere += $field_dett['importo_a'];
				if (in_array($field_dett['id_conto'], $SpDare))
				{
					$Saldo = round($TotDare-$TotAvere, 2);
				}else{
					$Saldo = round($TotAvere-$TotDare, 2);
				}

				if ($field_dett['importo_d'] == "0.00")
				{
					$field_dett['importo_d'] = "-";
				}

				if ($field_dett['importo_a'] == "0.00")
				{
					$field_dett['importo_a'] = "-";
				}

				$pdf->Cell(40, 4, $Sottoconto, 0, 0, 'R');
				$pdf->Cell(50, 4, $field_dett['tipo_doc'], 0, 0, '');
				$pdf->Cell(50, 4, $field_dett['descr'], 0, 0, '');
				$pdf->Cell(20, 4, $field_dett['importo_d'], "LR", 0, 'R');
				$pdf->Cell(20, 4, $field_dett['importo_a'], "LR", 0, 'R');
				$pdf->Cell(20, 4, $Saldo, "LR", 0, 'R');
				$pdf->Ln();
			}
			$pdf->Ln();
			break;
	}
}

//~ totali:
$pdf->SetFont('Arial', 'B', 9);

switch ($_GET['tipo'])
{
	case "movimenti":
		//~ Totale ricerca
		$pdf->SetFont('Arial', 'B', 9);
		$pdf->Cell(175, 4, 'Totale Dare/Avere Ricerca:', 0, '', 'R');
		$pdf->Cell(45, 4, sprintf("%.2f", $TotDare), 0, '', 'C');
		$pdf->Cell(45, 4, sprintf("%.2f", $TotAvere), 0, '', 'C');
		$pdf->Cell(25, 4, '', 0, '', 'R');
		break;

	case "mastrini":
		$Saldo = sprintf("%.2f", $TotDare-$TotAvere);
		$pdf->Cell(140, 4, "Totali:", 0, 0, 'R');
		$pdf->Cell(20, 4, $TotDare, 0, 0, 'R');
		$pdf->Cell(20, 4, $TotAvere, 0, 0, 'R');
		$pdf->Cell(20, 4, $Saldo, 0, 0, 'R');
		$pdf->Ln();
		break;
}


$browser = GetBrowserFamily($_SERVER['HTTP_USER_AGENT']);
//~ echo "Debug => ".$browser[0]." <br />";

if ($browser[0] != "msie")
{
	$pdf->Output('StampaContabile.pdf', 'I');
}else{
	//~ $pdf->Output('StampaContabile.pdf', 'D');
	$pdf->Output('StampaContabile.pdf', 'D');
}



?>

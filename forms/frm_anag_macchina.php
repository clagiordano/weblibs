<?php
	$TipoMch 	= $d->GetRows ("*", "tab_tipi_mch", "", "", "tipo_mch", 1);
	$Magazzini 	= $d->GetRows ("*", "tab_magazzino", "", "", "descr", 1);
	$StatiMch 	= $d->GetRows ("*", "tab_stato_mch", "", "", "stato_mch", 1);

	if (!isset($_GET['tomod']))
	{
		$_GET['tomod'] = "";
	}

	if (!isset($_GET['act']))
	{
		$_GET['act'] = "";
	}

	$Marche = $d->GetRows ("distinct marca", "tab_macchine",
		"marca != ''", "", "marca", 1);
	$Modelli = $d->GetRows ("distinct modello", "tab_macchine",
		"modello != ''", "", "modello", 1);

	if (isset($_GET['salva']))
	{
		$errori = "";

		// Check sui campi dei quali verificare l'unicità
		$check_fields = array();
		$check_string = "";

		if (strtoupper(trim($_POST['serial'])) != "ND")
		{
			$_POST['serial'] = str_replace("'", "-",
				strtoupper($_POST['serial']));
			array_push($check_fields, "serial");
		}else{
			//$_POST['serial' . $key] = "N.D.";
		}

		if (strtoupper(trim($_POST['asset'])) != "ND"
			&& strtoupper(trim($_POST['asset'])) != "N.D.")
		{
			$_POST['asset'] = str_replace("'", "-",
				strtoupper($_POST['asset']));
			array_push($check_fields, "asset");
		}else{
			//$_POST['asset' . $key] = "N.D.";
		}

		if (count($check_fields) > 0)
		{
			$check_string = join(",", $check_fields);
		}else{
			$check_string = "";
		}


		if ((trim($_POST['serial']) == "")
			&& (trim($_POST['asset']) == "")
			&& (is_numeric($_GET['tomod'])))
		{
			$errori .= "Errore, è necessario compilare almeno uno dei
				campi fra serial ed asset. <br />";
		}

		if ($_POST['id_tipo_mch'] == "-")
		{
			$errori .= "Errore, è necessario selezionare il tipo di macchina. <br />";
		}

		if ($_POST['id_magazzino'] == "-")
		{
			$errori .= "Errore, è necessario selezionare un magazzino. <br />";
		}

		if ($_GET['salva'] == "salva" && $errori == "")
		{
			$Status = $d->SaveRow ($_POST, "", "tab_macchine", $db, 1, $check_string);
			$Status = $Status[0];

			// Ri aggiorna l'opener con la stessa query last_select
		?>
			<script type="text/javascript">
				window.parent.document.location.href = window.parent.document.location.href + '&rex=1';
			</script>
		<?php
		}

		if ($_GET['salva'] == "modifica" && $errori == ""
			&& is_numeric($_GET['tomod']))
		{
			$_POST['data_in'] = $d->Inverti_Data($_POST['data_in']);
			$_POST['data_out'] = $d->Inverti_Data($Campi[0]['data_out']);
			$Status = $d->UpdateRow ($_POST, "", "tab_macchine", "id_macchina = '"
				. $_GET['tomod'] . "'");
				// Ri aggiorna l'opener con la stessa query last_select
		?>
			<script type="text/javascript">
				window.parent.document.location.href = window.parent.document.location.href + '&rex=1';
			</script>
		<?php
		}elseif ($_GET['salva'] == "modifica" && $errori == ""
			&& is_string($_GET['tomod']))
		{
			//echo "[Debug]: Aggiornamento multiplo <br />";
			unset($id_list);
			$id_list = explode(";", $_GET['tomod']);

			foreach ($id_list as $key => $field)
			{
				if (is_numeric($field))
				{
					$_POST['data_in'] = $d->Inverti_Data($_POST['data_in']);
					$_POST['data_out'] = $d->Inverti_Data($Campi[0]['data_out']);
					$Esclusioni = array('serial' => '', 'asset' => '');
					$Status = $d->UpdateRow ($_POST, $Esclusioni, "tab_macchine",
						"id_macchina = '$field'");
				}
			}

			// Ri aggiorna l'opener con la stessa query last_select
		?>
			<script type="text/javascript">
				window.parent.document.location.href = window.parent.document.location.href + '&rex=1';
			</script>
		<?php
		}
	}

	if (isset($_GET['tomod']) && empty($_POST)
		&& is_numeric($_GET['tomod']))
	{
		$Campi = $d->GetRows ("*", "tab_macchine", "id_macchina = '"
			. $_GET['tomod'] . "'", "", "", 1);
		$_POST = $Campi[0];
		$_POST['data_in'] = $d->Inverti_Data($Campi[0]['data_in']);
		$_POST['data_out'] = $d->Inverti_Data($Campi[0]['data_out']);
	}
	elseif (isset($_GET['tomod']) && empty($_POST)
		&& ($_GET['tomod'] == "multi"))
	{
		?>
			<script type="text/javascript">
				selects = window.parent.document.getElementsByName('multisel');
				multi_mod = "";
				for (index in selects)
				{
					if (selects[index].checked)
					{
						//alert(selects[index].value);
						// converto gli id dei selezionati in una stringa da passare
						// alla stessa pagina:
						multi_mod = multi_mod + selects[index].value + ";";
					}
				}
				// eseguo il redirect passando alla pagina gli id dei selezionati:
				document.location.href = "frm_anag_macchina.php?tomod=" + multi_mod;
			</script>
		<?php
	}else{
		// Importo i dati del primo elemento da modificare:
		if (isset($_GET['tomod']))
		{
			unset($id_list);
			$id_list = explode(";", $_GET['tomod']);
			//echo "[Debug]: first element: " . $id_list[0] . " <br />";
			$Campi = $d->GetRows ("*", "tab_macchine",
				"id_macchina = '" . $_GET['tomod'] . "'");
			$_POST = $Campi[0];
			$_POST['data_in'] = $d->Inverti_Data($Campi[0]['data_in']);
			$_POST['data_out'] = $d->Inverti_Data($Campi[0]['data_out']);
		}
	}

?>

<form method="post" action="<?php echo $_SERVER['REQUEST_URI']; ?>">
	<table class="form" style="width: 100%;">
		<tr>
			<td style="text-align: right;" colspan="4">
				<a onclick="Javascript: ClosePopup(); void(0);">
					[ <label class="err" style="cursor: pointer;">Chiudi</label> ]
				</a>
			</td>
		</tr>

		<?php $d->ShowEditBar("Registrazione Macchina:", $_GET['tomod'], "new,save", $_GET['act']); ?>

		<tr>
			<td style="text-align: right;">
				<label>Provenienza:</label>
			</td>
			<td>
				<select name="provenienza" style="width: 19em;">
					<option value="-">Seleziona provenienza</option>
					<option value="DA FILIALE"
						<?php if ($_POST['provenienza'] == "DA FILIALE")
							{ echo "selected=\"selected\""; } ?> >DA FILIALE</option>
					<option value="FORNITORE"
						<?php if ($_POST['provenienza'] == "FORNITORE")
							{ echo "selected=\"selected\""; } ?> >FORNITORE</option>
					<option value="IN LOCO"
						<?php if ($_POST['provenienza'] == "IN LOCO")
							{ echo "selected=\"selected\""; } ?> >IN LOCO</option>
					<option value="MAINTENANCE"
					<?php if ($_POST['provenienza'] == "MAINTENANCE")
						{ echo "selected=\"selected\""; } ?> >MAINTENANCE</option>
				</select>

				<?php if (is_numeric($_GET['tomod'])) { ?>
					&nbsp;
					<label>Tipo Macchina:</label>
					<select name="id_tipo_mch" style="width: 22em;">
						<option value="-">Seleziona tipo macchina</option>
							<?php foreach ($TipoMch as $key => $field) { ?>
								<option value="<?php echo $field['id_tipo_mch']; ?>"
									<?php if ($_POST['id_tipo_mch'] == $field['id_tipo_mch'])
										{ echo "selected=\"selected\""; } ?> >
									<?php echo htmlentities($field['tipo_mch']); ?>
								</option>
							<?php } ?>
					</select>
				<?php } ?>


			</td>
		</tr>

		<tr>
			<?php if (is_numeric($_GET['tomod'])) { ?>
				<td style="text-align: right;">
					<label>Marca:</label>
				</td>

				<td>
					<select name="marca" style="width: 12.3em;">
						<option value="-">Marca</option>
						<?php foreach ($Marche as $key_m => $field_m) { ?>
							<option value="<?php echo $field_m['marca']; ?>"
								<?php if ($_POST['marca'] == $field_m['marca'])
									{ echo "selected=\"selected\""; } ?> >
								<?php echo htmlentities($field_m['marca']); ?>
							</option>
						<?php } ?>
					</select>

					&nbsp;
					<label>Modello:</label>
					<select name="modello" style="width: 18em;">
						<option value="-">Modello</option>
						<?php foreach ($Modelli as $key_m => $field_m) { ?>
							<option value="<?php echo $field_m['modello']; ?>"
								<?php if ($_POST['modello'] == $field_m['modello'])
									{ echo "selected=\"selected\""; } ?> >
								<?php echo htmlentities($field_m['modello']); ?>
							</option>
						<?php } ?>
					</select>
			<?php } ?>
			<?php if (!is_numeric($_GET['tomod'])) { ?>
				<td style="text-align: right;">
					<label>Class HW:</label>
				</td>

				<td>
			<?php }else{ ?>
				&nbsp;<label>Class HW:</label>
			<?php } ?>
				<select name="class_hw">
					<option value="">-</option>
					<option value="BIANCO"
						<?php if ($_POST['class_hw'] == "BIANCO")
							{ echo "selected=\"selected\""; } ?> >BIANCO</option>
					<option value="BLU"
						<?php if ($_POST['class_hw'] == "BLU")
							{ echo "selected=\"selected\""; } ?> >BLU</option>
				</select>
			</td>
		</tr>


		<?php if (is_numeric($_GET['tomod'])) { ?>
			<tr>
				<td style="text-align: right;">
					<label>Serial:</label>
				</td>
				<td>
					<input name="serial" type="text" style="width: 16em;"
							value="<?php if ($_POST['serial']) { echo $_POST['serial']; } ?>" />

					&nbsp;
					<label>Asset:</label>
					<input name="asset" type="text" style="width: 14em;"
						value="<?php if ($_POST['asset']) { echo $_POST['asset']; } ?>" />

					&nbsp;
					<label>Mac:</label>
					<input name="mac" type="text" style="width: 12em;"
						value="<?php if ($_POST['mac']) { echo $_POST['mac']; } ?>" />
				</td>
			</tr>
			<tr>
					<td style="text-align: right;">
						<label>Data In:</label>
					</td>
					<td>
						<input name="data_in" type="text" style="width: 16em;"
							onfocus="showCalendarControl(this);"
							value="<?php if (isset($_POST['data_in'])) { echo $_POST['data_in']; } ?>" />

						&nbsp;
						<label>DDT In:</label>
						<input name="ddt_in" type="text" style="width: 29.8em;"
							value="<?php if (isset($_POST['ddt_in'])) { echo $_POST['ddt_in']; } ?>" />
					</td>
				</tr>

				<?php } ?>

				<tr>
					<td style="text-align: right;">
						<label>Data Out:</label>
					</td>
					<td>
						<input name="data_out" type="text" style="width: 16em;"
							onfocus="showCalendarControl(this);"
							value="<?php if (isset($_POST['data_out'])) { echo $_POST['data_out']; } ?>" />

						&nbsp;
						<label>DDT Out:</label>
						<input name="ddt_out" type="text" style="width: 28.8em;"
							value="<?php if (isset($_POST['ddt_out'])) { echo $_POST['ddt_out']; } ?>" />
					</td>
				</tr>

				<?php if (is_numeric($_GET['tomod'])) { ?>

				<tr>
					<td style="text-align: right;">
						<label>Sugg. D'impiego:</label>
					</td>
					<td>
						<input name="impiego" type="text" style="width: 21em;"
							value="<?php if (isset($_POST['impiego'])) { echo $_POST['impiego']; } ?>" />

						&nbsp;
						<label>Destinazione:</label>
						<input name="dest_fil" type="text" style="width: 21em;"
							value="<?php if (isset($_POST['dest_fil'])) { echo $_POST['dest_fil']; } ?>" />
				</tr>
		<?php } ?>
		<tr>
			<td style="text-align: right;">
				<label>Magazzino:</label>
			</td>
			<td>
				<select name="id_magazzino" style="width: 19em;">
					<option value="-">Seleziona magazzino</option>
					<?php foreach ($Magazzini as $key => $field) { ?>
						<option value="<?php echo $field['id_magazzino']; ?>"
								<?php if ($_POST['id_magazzino'] == $field['id_magazzino'])
									{ echo "selected=\"selected\""; } ?> >
								<?php echo htmlentities($field['descr']); ?>
							</option>
					<?php } ?>
				</select>

				&nbsp;
				<label>Stato Macchina:</label>
				<select name="id_stato_mch" style="width: 21.5em;">
					<option value="-">Seleziona stato macchina</option>
					<?php foreach ($StatiMch as $key => $field) { ?>
						<option value="<?php echo $field['id_stato_mch']; ?>"
								<?php if ($_POST['id_stato_mch'] == $field['id_stato_mch'])
									{ echo "selected=\"selected\""; } ?> >
								<?php echo htmlentities($field['stato_mch']); ?>
							</option>
					<?php } ?>
				</select>
			</td>
		</tr>

		<tr>
			<td style="text-align: right;">
				<label>Magazzino di Rif:</label></td>
			<td>
				<input name="mag_rif" type="text" style="width: 51em;" maxlength="100"
					value="<?php if (isset($_POST['mag_rif'])) { echo $_POST['mag_rif']; } ?>" />
			</td>
		</tr>

		<?php if (is_numeric($_GET['tomod'])) { ?>
		<tr>
			<td style="text-align: right; vertical-align: top;">
				<label>Controllo Visivo:</label>
			</td>
			<td style="vertical-align: bottom;">
				<textarea name="ck_vis" style="width: 51.6em; height: 4em;"><?php if ($_POST['ck_vis']) { echo $_POST['ck_vis']; } ?></textarea>
			</td>
		</tr>
		<tr>
			<td style="text-align: right; vertical-align: top;">
				<label>Controllo Funzionale:</label>
			</td>
			<td>
				<textarea name="ck_funz" style="width: 51.6em; height: 4em;"><?php if ($_POST['ck_funz']) { echo $_POST['ck_funz']; } ?></textarea>
			</td>
		</tr>

		<tr>
			<td style="text-align: right; vertical-align: top;">
				<label>Note/Accessori:</label>
			</td>
			<td>
				<textarea name="note_acc" style="width: 51.6em; height: 4em;"><?php if ($_POST['note_acc']) { echo $_POST['note_acc']; } ?></textarea>
			</td>
		</tr>
		<?php } ?>
		<tr>
			<th colspan="8">
				<label class="err">
					<?php if (isset($errori)) { echo $errori; } ?>
				</label>
			</th>
		</tr>
		<tr>
			<th colspan="8">
				<label class="err">
					<?php if (isset($Status)) { echo $Status; } ?>
				</label>
			</th>
		</tr>
	</table>
</form>

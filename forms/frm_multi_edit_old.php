<?php
/*
 * frm_multi_edit.php
 * 
 * Copyright 2012 Claudio Giordano <claudio.giordano@autistici.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

	// Form per il multi add/modifica di categorie unita' ecc:

	switch ($_GET['type'])
	{
		case "um":
			$Table = "tab_um";
			$Order = "um";
			$Label = "Unit&agrave; di misura";
		break;
	}

	if (isset($_GET['salva']))
	{
		$errori = "";
		if (($_GET['salva'] == "salva") && ($errori == ""))
		{
			$Status = $d->SaveRow ($_POST, "", $Table, 1, $Order);
			$save_result = $Status[0];
		}

		if (($_GET['salva'] == "modifica") && ($errori == ""))
		{
			$save_result = $d->UpdateRow ($_POST, "", $Table,
				"id_$Order = '" . $_GET['tomod'] . "'");
		}

		// dopo il salvataggio ripopolo il select interessato:
		?>
			<script type="text/javascript">
				if (parent.document.getElementById('id_<?php echo $Order; ?>'))
				{
					select = parent.document.getElementById('id_<?php echo $Order; ?>');
				}else{
					select = parent.document.getElementById('id_<?php echo $Order; ?>');
				}

				select.options.length = 0;
				select.options[0] = new Option('Seleziona valore', '-');
				select.options[1] = new Option('', '-');

				<?php
					$Valori = $d->GetRows ("*", $Table, "", "", $Order, 1);
					foreach ($Valori as $key => $field)
					{
						?>
							select.options[<?php echo $key+2 ?>] = new Option('<?php echo $field[$Order]; ?>', 
								'<?php echo $field["id_" . $Order]; ?>');
						<?php
					}	
				?>

				ClosePopup();
			</script>
		<?php
	}

	$Valori = $d->GetRows ("*", $Table, "", "", $Order, 1);
	if (isset($_POST["id_" . $Order])
		&& is_numeric($_POST["id_" . $Order]))
	{
		$_GET['tomod'] = $_POST["id_" . $Order];

		$temp_array = $d->GetRows ("*", $Table,
			"id_$Order = '" . $_POST["id_" . $Order] ."'", "", "", 1);
		$_POST = $temp_array[0];
	}

?>



<form method="post" action="<?php echo $_SERVER['REQUEST_URI']; ?>">
	<table class="form" style="width: 100%;">

		<tr>
			<td style="text-align: right;"  colspan="2" >
				<a onclick="Javascript: ClosePopup(); void(0);">
					[ <label class="err" style="cursor: pointer;">Chiudi</label> ]
				</a>
			</td>
		</tr>

		<?php
			$d->ShowEditBar("$Label:", $_GET['tomod'], "new,save",
				"&type=" . $_GET['type'], 2);
		?>

		<tr><th colspan="2"><?php if (isset($save_result)) { echo $save_result; } ?></th></tr>

		<tr><th colspan="2"><br /></th></tr>

		<tr>
			<th style="text-align: right;">Valore:</th>
			<td>
				<input name="<?php echo $Order; ?>" type="text" style="width: 29.5em;"
					value="<?php if (isset($_POST[$Order])) { echo $_POST[$Order]; } ?>" />
			</th>
			</tr>

		<tr>
			<th style="vertical-align: top; text-align: right;">
				Valori attuali:
			</th>

			<td>
				<select name="id_<?php echo $Order; ?>" style="width: 30em;" size="10"
					onchange="Javascript: posta('0', '<?php echo $_SERVER['REQUEST_URI']; ?>'); void(0);">
					<?php foreach ($Valori as $key => $field) { ?>
						<option value="<?php echo $field['id_' . $Order]; ?>"

							<?php if (isset($_POST['id_' . $Order])
								&& $_POST['id_' . $Order] == $field['id_' .$Order])
								{
									echo "selected=\"selected\"";
								} ?> >

							<?php echo $field[$Order]; ?>
						</option>
					<?php } ?>
				</select>
			</td>
		</tr>
	</table>
</form>

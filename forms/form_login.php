<?php
/*
 * Form di accesso
 *
 * Copyright 2008 - 2015 Claudio Giordano <claudio.giordano@autistici.org>
 *
 */

if (filter_input(INPUT_POST, 'username') != "") {
    $a->logIn(
        filter_input(INPUT_POST, 'username'),
        filter_input(INPUT_POST, 'password'),
        SESSION_PREFIX
    );
}
?>
<!--
<form method="post" action="<?php echo filter_input(INPUT_SERVER, 'REQUEST_URI'); ?>">
    <div style="width: 100%;">
<?php if (ACCESS_STATUS == 0) {
?>
                        <div style="text-align: center;">
                            <input name="username" type="text" tabindex="1" required
                                   placeholder="Username" style="width: 20em;"/>
                        </div>

                        <div style="text-align: center;">
                            <input name="password" type="password" tabindex="2" required
                                   placeholder="Password" style="width: 20em;" />
                        </div>

                        <div style="text-align: center;">
                            <input type="submit" value="Accedi" tabindex="3"
                                   style="width: 20em;" />
                        </div>
<?php
} else {
?>
                        <div>
                            <label class="err">
                                Accesso temporaneamente disabilitato per manutenzione.
                            </label>
                        </div>
<?php
} ?>
        <div <?php
        if ($a->ErrorsOccurred === true) {
            echo "class=\"err\"";
        } else {
            echo "class=\"ok\"";
        }
?> >
<?php
if (isset($a->ErrorsOccurred)) {
    echo $a->ResultStatus;
}
?>
        </div>
    </div>
</form>!-->

<div class="container">
    <form method="post" action="<?php echo filter_input(
    INPUT_SERVER,
    'REQUEST_URI'
);
?>">
        <div class="form-group">
            <label for="inputUsername">Username</label>
            <input type="username" class="form-control" id="inputUsername" placeholder="Username">
        </div>
        <div class="form-group">
            <label for="inputPassword">Password</label>
            <input type="password" class="form-control" id="inputPassword" placeholder="Password">
        </div>
        <div class="checkbox">
            <label><input type="checkbox"> Remember me</label>
        </div>
        <button type="submit" class="btn btn-primary">Accedi</button>
    </form>
</div>
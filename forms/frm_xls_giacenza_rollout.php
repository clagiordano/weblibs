<?php session_start();
	$root = substr($_SERVER["DOCUMENT_ROOT"], 0, -1);
	require_once("connection.php");
	require_once("$root/Class/XlsExport.php");

	SendHeaders ("Giacenza_hw_nuovo_" . date('d-m-Y') . ".xls");


	$hw_new = $d->GetRows ("*", "view_macchine", "provenienza = 'FORNITORE'",
		"id_macchina");

    xlsBOF();

	xlsWriteLabel(0, 0, "PROGRESSIVO");	// contatore base 1
	xlsWriteLabel(0, 1, "TIPO /  PROVENIENZA");
	xlsWriteLabel(0, 2, "DDT Arrivo e/o REQUEST NUMBER (x resi e dismesse)");
	xlsWriteLabel(0, 3, "Rif.Ordine o Req. Number In (x Resi)");
	xlsWriteLabel(0, 4, "Nome Filiale Citta ( solo x dismessi)");
	xlsWriteLabel(0, 5, "DATA IN");
	xlsWriteLabel(0, 6, "ORDINE OUT (Utilizzato x l- uscita della merce)");
	xlsWriteLabel(0, 7, "DATA STAGING");
	xlsWriteLabel(0, 8, "DATA OUT");
	xlsWriteLabel(0, 9, "Filiale di Destinazione"); // den filiale
	xlsWriteLabel(0, 10, "Codice Filiale"); // cod_fil filiale
	xlsWriteLabel(0, 11, "Formattazione e allestimento Kit Accessori e cavetteria");
	xlsWriteLabel(0, 12, "NOTE  Accessori Mancanti");
	xlsWriteLabel(0, 13, "TIPOLOGIA APPARATO");
	xlsWriteLabel(0, 14, "CODICE ARTICOLO");
	xlsWriteLabel(0, 15, "MARCA");
	xlsWriteLabel(0, 16, "MODELLO");
	xlsWriteLabel(0, 17, "CLASSIFICAZIONE HW");
	xlsWriteLabel(0, 18, "CONTROLLO VISIVO");
	xlsWriteLabel(0, 19, "CONTROLLO FUNZIONALE");
	xlsWriteLabel(0, 20, "MATRICOLA / SERIAL NUMBER");
	xlsWriteLabel(0, 21, "UGIS / ASSET TAG");
	xlsWriteLabel(0, 22, "SCAFFALE / PALLET");
	xlsWriteLabel(0, 23, "Magazzino di Riferimento");
	xlsWriteLabel(0, 24, "NOTE/MAC ADDRES");
	xlsWriteLabel(0, 25, "Magazzino di Riferimento"); // magazzino vero
	xlsWriteLabel(0, 26, "SUGGERIMENTO D'IMPIEGO"); // inutile
	xlsWriteLabel(0, 27, "Destinazione UGIS"); // inutile
	xlsWriteLabel(0, 28, "LOCAZIONE"); // inutile

	$xlsRow = 1;
	foreach ($hw_new as $key => $field)
	{
		$req = $d->GetRows ("*", "tab_dett_requests", "id_macchina = '"
			. $field['id_macchina'] . "'");

		if (count($req) > 0)
		{
			$id_request = $req[0]['id_request'];
			$request = $req[0]['request'];

			$dett_pianif = $d->GetRows ("*", "view_requests",
				"id_request = '$id_request'");

			$data_st = $d->Inverti_Data($dett_pianif[0]['data_st']); // -1 pianif
			$data_out = $d->Inverti_Data($dett_pianif[0]['data_pianif']); // == pianif
			if ($data_out == "0000-00-00")
			{
				$data_out = "";
			}

		}

		xlsWriteLabel ($xlsRow, 0, $xlsRow);
		xlsWriteLabel ($xlsRow, 1, $field['provenienza']);
		xlsWriteLabel ($xlsRow, 2, $field['ddt_in']);
		xlsWriteLabel ($xlsRow, 3, "");
		xlsWriteLabel ($xlsRow, 4, "");
		xlsWriteLabel ($xlsRow, 5, Inverti_Data($field['data_in']));
		xlsWriteLabel ($xlsRow, 6, $request);
		xlsWriteLabel ($xlsRow, 7, $data_st);
		xlsWriteLabel ($xlsRow, 8, $data_out);
		xlsWriteLabel ($xlsRow, 9, $dett_pianif[0]['den']);
		xlsWriteLabel ($xlsRow, 10, $dett_pianif[0]['cod_fil']);
		xlsWriteLabel ($xlsRow, 11, $field['allestimento']);
		xlsWriteLabel ($xlsRow, 12, $field['note_acc']);
		xlsWriteLabel ($xlsRow, 13, $field['tipo_mch']);
		xlsWriteLabel ($xlsRow, 14, $field['cod_art']);
		xlsWriteLabel ($xlsRow, 15, $field['marca']);
		xlsWriteLabel ($xlsRow, 16, $field['modello']);
		xlsWriteLabel ($xlsRow, 17, $field['class_hw']);
		xlsWriteLabel ($xlsRow, 18, $field['ck_vis']);
		xlsWriteLabel ($xlsRow, 19, $field['ck_funz']);
		xlsWriteLabel ($xlsRow, 20, $field['serial']);
		xlsWriteLabel ($xlsRow, 21, $field['asset']);
		xlsWriteLabel ($xlsRow, 22, $field['pallet']);
		xlsWriteLabel ($xlsRow, 23, "00");
		xlsWriteLabel ($xlsRow, 24, $field['mac']);
		xlsWriteLabel ($xlsRow, 25, $field['magazzino']);
		xlsWriteLabel ($xlsRow, 26, "");
		xlsWriteLabel ($xlsRow, 27, "");
		xlsWriteLabel ($xlsRow, 28, "");

		$xlsRow++;
    }

    xlsEOF();
    exit();
?>

<script type="text/javascript">
	<!--window.close();!-->
</script>

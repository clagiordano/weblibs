<?php
	$root = substr($_SERVER["DOCUMENT_ROOT"], 0, -1);
	require_once("$root/Class/XlsExport.php");

	if (isset($_GET['exp']))
	{
		switch ($_GET['exp'])
		{
			case "hwnew":
				$d->SessionLog("Avviata esportazione giacenza nuovo (hwnew)");
				$FileName = "tmp/Giacenza_hw_nuovo_" . date('d-m-Y') . ".xls";
				$x = new XlsExport($FileName);

				$hw_new = $d->GetRows ("*", "view_macchine", "provenienza = 'FORNITORE'",
					"", "id_macchina", 1);

				$x->xlsWriteLabel(0, 0, "PROGRESSIVO");	// contatore base 1
				$x->xlsWriteLabel(0, 1, "TIPO /  PROVENIENZA");
				$x->xlsWriteLabel(0, 2, "DDT Arrivo e/o REQUEST NUMBER (x resi e dismesse)");
				$x->xlsWriteLabel(0, 3, "Rif.Ordine o Req. Number In (x Resi)");
				$x->xlsWriteLabel(0, 4, "Nome Filiale Citta ( solo x dismessi)");
				$x->xlsWriteLabel(0, 5, "DATA IN");
				$x->xlsWriteLabel(0, 6, "ORDINE OUT (Utilizzato x l- uscita della merce)");
				$x->xlsWriteLabel(0, 7, "DATA STAGING");
				$x->xlsWriteLabel(0, 8, "DATA OUT");
				$x->xlsWriteLabel(0, 9, "Filiale di Destinazione"); // den filiale
				$x->xlsWriteLabel(0, 10, "Codice Filiale"); // cod_fil filiale
				$x->xlsWriteLabel(0, 11, "Formattazione e allestimento Kit Accessori e cavetteria");
				$x->xlsWriteLabel(0, 12, "NOTE  Accessori Mancanti");
				$x->xlsWriteLabel(0, 13, "TIPOLOGIA APPARATO");
				$x->xlsWriteLabel(0, 14, "CODICE ARTICOLO");
				$x->xlsWriteLabel(0, 15, "MARCA");
				$x->xlsWriteLabel(0, 16, "MODELLO");
				$x->xlsWriteLabel(0, 17, "CLASSIFICAZIONE HW");
				$x->xlsWriteLabel(0, 18, "CONTROLLO VISIVO");
				$x->xlsWriteLabel(0, 19, "CONTROLLO FUNZIONALE");
				$x->xlsWriteLabel(0, 20, "MATRICOLA / SERIAL NUMBER");
				$x->xlsWriteLabel(0, 21, "UGIS / ASSET TAG");
				$x->xlsWriteLabel(0, 22, "SCAFFALE / PALLET");
				$x->xlsWriteLabel(0, 23, "Magazzino di Riferimento");
				$x->xlsWriteLabel(0, 24, "NOTE/MAC ADDRES");
				$x->xlsWriteLabel(0, 25, "Magazzino di Riferimento"); // magazzino vero
				$x->xlsWriteLabel(0, 26, "SUGGERIMENTO D'IMPIEGO"); // inutile
				$x->xlsWriteLabel(0, 27, "Destinazione UGIS"); // inutile
				$x->xlsWriteLabel(0, 28, "LOCAZIONE"); // inutile

				$xlsRow = 1;
				foreach ($hw_new as $key => $field)
				{
					$req = $d->GetRows ("*", "tab_dett_requests", "id_macchina = '"
						. $field['id_macchina'] . "'", "", "", 1);

					// prende la filiale di destinazione dalla tabella macchine
					$denominazione = $field['dest_fil'];

					if (count($req) > 0)
					{
						$id_request = $req[0]['id_request'];
						$dett_pianif = $d->GetRows ("*", "view_requests",
							"id_request = '$id_request'", "", "", 1);

						$request = $dett_pianif[0]['request'];
						//$data_st = Inverti_Data($dett_pianif[0]['data_st']); // -1 pianif
						$data_out = $d->Inverti_Data($dett_pianif[0]['data_pianif']); // == pianif

						/* Se la filiale di destinazione non è impostata,
						 * utilizza la denominazione della filiale */
						if ($denominazione == "")
						{
							$denominazione = $dett_pianif[0]['den'];
						}

						$cod_fil = $dett_pianif[0]['cod_fil'];

						if ($data_out == "0000-00-00")
						{
							$data_out = "";
						}

					}else{
						$id_request = "";
						$request = "";
						$data_st = "";
						$data_out = "";
						//$denominazione = "";
						$cod_fil = "";
					}

					switch ($field['tipo_mch'])
					{
						case "Stampante di rete":
						case "Stampante di cassa":
						case "Stampante laser":
							$field['tipo_mch'] = "PRINTER";
							break;
					}

					$x->xlsWriteLabel ($xlsRow, 0, $xlsRow);
					$x->xlsWriteLabel ($xlsRow, 1, $field['provenienza']);
					$x->xlsWriteLabel ($xlsRow, 2, $field['ddt_in']);
					$x->xlsWriteLabel ($xlsRow, 3, "");
					$x->xlsWriteLabel ($xlsRow, 4, "");
					$x->xlsWriteLabel ($xlsRow, 5, $d->Inverti_Data($field['data_in']));
					$x->xlsWriteLabel ($xlsRow, 6, $request);
					$x->xlsWriteLabel ($xlsRow, 7, $data_st);
					$x->xlsWriteLabel ($xlsRow, 8, $data_out);
					$x->xlsWriteLabel ($xlsRow, 9, $denominazione);
					$x->xlsWriteLabel ($xlsRow, 10, $cod_fil);
					$x->xlsWriteLabel ($xlsRow, 11, $field['allestimento']);
					$x->xlsWriteLabel ($xlsRow, 12, $field['note_acc']);
					$x->xlsWriteLabel ($xlsRow, 13, $field['tipo_mch']);
					$x->xlsWriteLabel ($xlsRow, 14, $field['cod_art']);
					$x->xlsWriteLabel ($xlsRow, 15, $field['marca']);
					$x->xlsWriteLabel ($xlsRow, 16, $field['modello']);
					$x->xlsWriteLabel ($xlsRow, 17, $field['class_hw']);
					$x->xlsWriteLabel ($xlsRow, 18, $field['ck_vis']);
					$x->xlsWriteLabel ($xlsRow, 19, $field['ck_funz']);
					$x->xlsWriteLabel ($xlsRow, 20, $field['serial']);
					$x->xlsWriteLabel ($xlsRow, 21, $field['asset']);
					$x->xlsWriteLabel ($xlsRow, 22, $field['pallet']);
					$x->xlsWriteLabel ($xlsRow, 23, "00");
					$x->xlsWriteLabel ($xlsRow, 24, $field['mac']);
					$x->xlsWriteLabel ($xlsRow, 25, $field['magazzino']);
					$x->xlsWriteLabel ($xlsRow, 26, $field['impiego']);
					$x->xlsWriteLabel ($xlsRow, 27, "");
					$x->xlsWriteLabel ($xlsRow, 28, "");

					$xlsRow++;
				}

				$x->WriteFile();
				$d->SessionLog("Terminata esportazione giacenza nuovo (hwnew)");
				exit(); ?>
				<script type="text/javascript">
					window.close();
				</script><?php
				break;

			case "hwold":
				$d->SessionLog("Avviata esportazione giacenza vecchio (hwold)");
				$FileName = "tmp/Giacenza_hw_dismesso_" . date('d-m-Y') . ".xls";
				$x = new XlsExport($FileName);

				$hw_new = $d->GetRows ("*", "view_macchine", "provenienza = 'DA FILIALE'"
					. " AND tipo_mch != 'Server'", "id_macchina", 1);

				$x->xlsWriteLabel(0, 0, "PROGRESSIVO");	// contatore base 1
				$x->xlsWriteLabel(0, 1, "TIPO /  PROVENIENZA");
				$x->xlsWriteLabel(0, 2, "DDT Arrivo e/o REQUEST NUMBER (x resi e dismesse)"); // ddt_in
				$x->xlsWriteLabel(0, 3, "Nome Filiale Citta ( solo x dismessi)"); // den
				$x->xlsWriteLabel(0, 4, "Codice Filiale"); // cod_fil filiale
				$x->xlsWriteLabel(0, 5, "DATA IN");
				$x->xlsWriteLabel(0, 6, "ORDINE OUT (Utilizzato x l- uscita della merce)");
				$x->xlsWriteLabel(0, 7, "DATA OUT");
				$x->xlsWriteLabel(0, 8, "Formattazione e allestimento Kit Accessori e cavetteria");
				$x->xlsWriteLabel(0, 9, "NOTE  Accessori Mancanti");
				$x->xlsWriteLabel(0, 10, "TIPOLOGIA APPARATO");
				$x->xlsWriteLabel(0, 11, "CODICE ARTICOLO");
				$x->xlsWriteLabel(0, 12, "MARCA");
				$x->xlsWriteLabel(0, 13, "MODELLO");
				$x->xlsWriteLabel(0, 14, "CLASSIFICAZIONE HW");
				$x->xlsWriteLabel(0, 15, "CONTROLLO VISIVO");
				$x->xlsWriteLabel(0, 16, "CONTROLLO FUNZIONALE");
				$x->xlsWriteLabel(0, 17, "MATRICOLA / SERIAL NUMBER");
				$x->xlsWriteLabel(0, 18, "UGIS / ASSET TAG");
				$x->xlsWriteLabel(0, 19, "SCAFFALE / PALLET");
				$x->xlsWriteLabel(0, 20, "Magazzino di Riferimento");
				$x->xlsWriteLabel(0, 21, "NOTE/MAC ADDRES");
				$x->xlsWriteLabel(0, 22, "SUGGERIMENTO D'IMPIEGO"); // inutile
				$x->xlsWriteLabel(0, 23, "Destinazione UGIS"); // inutile
				$x->xlsWriteLabel(0, 24, "LOCAZIONE"); // inutile

				$xlsRow = 1;
				foreach ($hw_new as $key => $field)
				{
					$req = $d->GetRows ("*", "tab_dett_requests", "id_macchina = '"
						. $field['id_macchina'] . "'", "", 1);

					if (count($req) > 0)
					{
						$id_request = $req[0]['id_request'];


						$dett_pianif = $d->GetRows ("*", "view_requests",
							"id_request = '$id_request'", "", 1);

						$request = $dett_pianif[0]['request'];
						//$data_st = Inverti_Data($dett_pianif[0]['data_st']); // -1 pianif
						$data_out = $d->Inverti_Data($dett_pianif[0]['data_pianif']); // == pianif
						$denominazione = $dett_pianif[0]['den'];
						$cod_fil = $dett_pianif[0]['cod_fil'];
						if ($data_out == "0000-00-00")
						{
							$data_out = "";
						}
					}else{
						$id_request = "";
						$request = "";
						$data_st = "";
						$data_out = "";
						$denominazione = "";
						$cod_fil = "";
					}

					switch ($field['tipo_mch'])
					{
						case "Stampante di rete":
						case "Stampante di cassa":
						case "Stampante laser":
							$field['tipo_mch'] = "PRINTER";
							break;
					}

					$x->xlsWriteLabel ($xlsRow, 0, $xlsRow);
					$x->xlsWriteLabel ($xlsRow, 1, $field['provenienza']);
					$x->xlsWriteLabel ($xlsRow, 2, $field['ddt_in']);
					$x->xlsWriteLabel ($xlsRow, 3, $denominazione);
					$x->xlsWriteLabel ($xlsRow, 4, $cod_fil);
					$x->xlsWriteLabel ($xlsRow, 5, $d->Inverti_Data($field['data_in']));
					$x->xlsWriteLabel ($xlsRow, 6, $request);
					$x->xlsWriteLabel ($xlsRow, 7, "");
					$x->xlsWriteLabel ($xlsRow, 8, $field['allestimento']);
					$x->xlsWriteLabel ($xlsRow, 9, $field['note_acc']);
					$x->xlsWriteLabel ($xlsRow, 10, $field['tipo_mch']);
					$x->xlsWriteLabel ($xlsRow, 11, $field['cod_art']);
					$x->xlsWriteLabel ($xlsRow, 12, $field['marca']);
					$x->xlsWriteLabel ($xlsRow, 13, $field['modello']);
					$x->xlsWriteLabel ($xlsRow, 14, $field['class_hw']);
					$x->xlsWriteLabel ($xlsRow, 15, $field['ck_vis']);
					$x->xlsWriteLabel ($xlsRow, 16, $field['ck_funz']);
					$x->xlsWriteLabel ($xlsRow, 17, $field['serial']);
					$x->xlsWriteLabel ($xlsRow, 18, $field['asset']);
					$x->xlsWriteLabel ($xlsRow, 19, $field['pallet']);
					$x->xlsWriteLabel ($xlsRow, 20, $field['magazzino']);
					$x->xlsWriteLabel ($xlsRow, 21, $field['mac']);
					$x->xlsWriteLabel ($xlsRow, 22, "");
					$x->xlsWriteLabel ($xlsRow, 23, $field['stato_mch']);
					$x->xlsWriteLabel ($xlsRow, 24, "");

					$xlsRow++;
				}

				$x->WriteFile();
				$d->SessionLog("Terminata esportazione giacenza vecchio (hwold)");
				exit(); ?>
				<script type="text/javascript">
					window.close();
				</script><?php
				break;

			case "hwfull":
				$d->SessionLog("Avviata esportazione giacenza completa (hwfull)");
				$FileName = "tmp/Giacenza_Palermo_al_" . date('d-m-Y') . ".xls";
				$x = new XlsExport($FileName);

				$hwfull = $d->GetRows ("*", "view_macchine", "", "id_macchina", 1);

				$x->xlsWriteLabel(0, 0, "ID Prodotto");	// contatore base 1
				$x->xlsWriteLabel(0, 1, "TIPO /  PROVENIENZA");
				$x->xlsWriteLabel(0, 2, "DDT Arrivo e/o REQUEST NUMBER (x resi e dismesse)"); // ddt_in
				$x->xlsWriteLabel(0, 3, "Nome Filiale Citta ( solo x dismessi)"); // den
				$x->xlsWriteLabel(0, 4, "Codice Filiale"); // cod_fil filiale
				$x->xlsWriteLabel(0, 5, "DATA IN"); // data_in
				$x->xlsWriteLabel(0, 6, "ORDINE OUT (Utilizzato x l- uscita della merce)"); // ddt_out
				$x->xlsWriteLabel(0, 7, "DATA OUT"); // data_out
				$x->xlsWriteLabel(0, 8, "Formattazione e allestimento Kit Accessori e cavetteria"); // allestimento
				$x->xlsWriteLabel(0, 9, "NOTE  Accessori Mancanti"); // note_acc
				$x->xlsWriteLabel(0, 10, "TIPOLOGIA APPARATO"); // tipo_mch
				$x->xlsWriteLabel(0, 11, "CODICE ARTICOLO");	// cod_art
				$x->xlsWriteLabel(0, 12, "MARCA");
				$x->xlsWriteLabel(0, 13, "MODELLO");
				$x->xlsWriteLabel(0, 14, "CLASSIFICAZIONE HW");
				$x->xlsWriteLabel(0, 15, "CONTROLLO VISIVO");
				$x->xlsWriteLabel(0, 16, "CONTROLLO FUNZIONALE");
				$x->xlsWriteLabel(0, 17, "MATRICOLA / SERIAL NUMBER");
				$x->xlsWriteLabel(0, 18, "UGIS / ASSET TAG");
				$x->xlsWriteLabel(0, 19, "SCAFFALE / PALLET");
				$x->xlsWriteLabel(0, 20, "Magazzino di Riferimento");	// EX colonna "U" 00 hw nuovo, 01 ritirato
				$x->xlsWriteLabel(0, 21, "NOTE/MAC ADDRES");
				$x->xlsWriteLabel(0, 22, "SUGGERIMENTO D'IMPIEGO"); // inutile
				$x->xlsWriteLabel(0, 23, "Destinazione UGIS"); // stato_mch
				$x->xlsWriteLabel(0, 24, "LOCAZIONE"); // inutile
				$x->xlsWriteLabel(0, 25, "");
				$x->xlsWriteLabel(0, 26, "");
				$x->xlsWriteLabel(0, 27, "");
				$x->xlsWriteLabel(0, 28, "SITO");	// sempre palermo
				$x->xlsWriteLabel(0, 29, "SOTTOSITO"); // desct magazzino reale

				$xlsRow = 1;
				foreach ($hwfull as $key => $field)
				{
					$req = $d->GetRows ("*", "tab_dett_requests", "id_macchina = '"
						. $field['id_macchina'] . "'", "", 1);

					if (count($req) > 0)
					{
						$id_request = $req[0]['id_request'];


						$dett_pianif 	 = $d->GetRows ("*", "view_requests", "id_request = '$id_request'");
						$request 		 	 = $dett_pianif[0]['request'];
						//$data_st 	 	 = Inverti_Data($dett_pianif[0]['data_st']); // -1 pianif
						//$data_out 	 	 = $d->Inverti_Data($dett_pianif[0]['data_pianif']); // == pianif
						$denominazione = $dett_pianif[0]['den'];
						$cod_fil 			 = $dett_pianif[0]['cod_fil'];

						if (($data_out == "0000-00-00")
							|| ($data_out == "00/00/0000"))
						{
							$data_out		 = "";
						}
					}else{
						$id_request 	 = "";
						$request 			 = "";
						$data_st 			 = "";
						$data_out 		 = "";
						$denominazione = "";
						$cod_fil 			 = "";
					}

					//echo "[Debug]: data_out: $data_out <br />";
					/*
					 * hw nuovo ddt_in -> ddt_in
					 * hw nuovo ddt_out -> cr da pianificazione
					 *
					 * hw old ddt_in -> cr da pianificazione
					 * hw old ddt_out ->  ddt_out
					 */

/*
					if ($field['provenienza'] == "FORNITORE")
					{
						$mag_rif = "00";
					}else{
						$mag_rif = "01";
					}
*/

					if (($field['ddt_in'] != ""))
					{
						$DDT_IN = $field['ddt_in'];
					}else{
						$DDT_IN = "";
						//$DDT_IN = $request;
					}

					if (($field['ddt_in'] != ""))
					{
						$DDT_IN = $field['ddt_in'];
					}else{
						$DDT_IN = "";
						//$DDT_IN = $request;
					}

					if (($field['ddt_out'] != ""))
					{
						$DDT_OUT = $field['ddt_out'];
					}else{
						$DDT_OUT = "";
						//$DDT_OUT = $request;
					}


					if (($field['data_out'] != "0000-00-00"))
					{
						$DATA_OUT = $d->Inverti_Data($field['data_out']);
					}else{
						$DATA_OUT = "";
					}



					switch ($field['tipo_mch'])
					{
						case "Stampante di rete":
						case "Stampante di cassa":
						case "Stampante laser":
							$field['tipo_mch'] = "PRINTER";
							break;
					}

					$x->xlsWriteLabel ($xlsRow, 0, $field['id_macchina']);
					$x->xlsWriteLabel ($xlsRow, 1, $field['provenienza']);
					$x->xlsWriteLabel ($xlsRow, 2, $DDT_IN);
					$x->xlsWriteLabel ($xlsRow, 3, $denominazione);
					$x->xlsWriteLabel ($xlsRow, 4, $cod_fil);
					$x->xlsWriteLabel ($xlsRow, 5, $d->Inverti_Data($field['data_in']));
					$x->xlsWriteLabel ($xlsRow, 6, $DDT_OUT);
					$x->xlsWriteLabel ($xlsRow, 7, $DATA_OUT);
					$x->xlsWriteLabel ($xlsRow, 8, $field['allestimento']);
					$x->xlsWriteLabel ($xlsRow, 9, $field['note_acc']);
					$x->xlsWriteLabel ($xlsRow, 10, $field['tipo_mch']);
					$x->xlsWriteLabel ($xlsRow, 11, $field['cod_art']);
					$x->xlsWriteLabel ($xlsRow, 12, $field['marca']);
					$x->xlsWriteLabel ($xlsRow, 13, $field['modello']);
					$x->xlsWriteLabel ($xlsRow, 14, $field['class_hw']);
					$x->xlsWriteLabel ($xlsRow, 15, $field['ck_vis']);
					$x->xlsWriteLabel ($xlsRow, 16, $field['ck_funz']);
					$x->xlsWriteLabel ($xlsRow, 17, $field['serial']); // str_shuffle($field['serial'])); //
					$x->xlsWriteLabel ($xlsRow, 18, $field['asset']); // str_shuffle($field['asset'])); //
					$x->xlsWriteLabel ($xlsRow, 19, $field['pallet']);
					$x->xlsWriteLabel ($xlsRow, 20, $field['mag_rif']); // $mag_rif
					$x->xlsWriteLabel ($xlsRow, 21, $field['mac']); // str_shuffle($field['mac'])); //
					$x->xlsWriteLabel ($xlsRow, 22, $field['impiego']);
					$x->xlsWriteLabel ($xlsRow, 23, $field['stato_mch']);
					$x->xlsWriteLabel ($xlsRow, 24, "");
					$x->xlsWriteLabel(0, 25, "");
					$x->xlsWriteLabel(0, 26, "");
					$x->xlsWriteLabel(0, 27, "");
					$x->xlsWriteLabel ($xlsRow, 28, "PALERMO");
					$x->xlsWriteLabel ($xlsRow, 29, $field['magazzino']);

					$xlsRow++;
				}

				$x->WriteFile();

				$d->SessionLog("Terminata esportazione giacenza completa (hwfull)");

				exit(); ?>
				<script type="text/javascript">
					window.close();
				</script><?php
				break;
		}
	}
?>

<table style="width: 100%;" class="form">
	<tr>
		<th colspan="2">Seleziona il tipo di dato da esportare:</th>
	</tr>
	<tr>
		<td style="width: 35px;"><br /></td>
		<th>
			<a onclick="Javascript: window.open('frm_export_rollout.php?exp=hwnew', '_blank'); void(0);"
				title="Click per avviare il download">
				<img src="/Images/Links/xls.png" alt="xls" />
				Giacenza hardware nuovo (in formato XLS)
			</a>
		</th>
	</tr>

	<tr>
		<td style="width: 35px;"><br /></td>
		<th>
			<a onclick="Javascript: window.open('frm_export_rollout.php?exp=hwold', '_blank'); void(0);"
				title="Click per avviare il download">
				<img src="/Images/Links/xls.png" alt="xls" />
				Giacenza hardware dismesso (in formato XLS)
			</a>
		</th>
	</tr>

	<tr>
		<td style="width: 35px;"><br /></td>
		<th>
			<a onclick="Javascript: window.open('frm_export_rollout.php?exp=hwfull', '_blank'); void(0);"
				title="Click per avviare il download">
				<img src="/Images/Links/xls.png" alt="xls" />
				Giacenza completa hardware (in formato XLS)
			</a>
		</th>
	</tr>
</table>




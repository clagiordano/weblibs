<?php
    require_once PHP_PATH . 'Gestione.php';
	// Estrae le modalita' di pagamento dalla relativa tabella:
	$mod_pag = $d->GetRows("*", "tab_mod_pagamento", "", "", "pagamento");
	// Estrae i tipi anagrafica:
	$Tipi = $d->GetRows("", "tab_tipo_anag", "id_anag != ''", "", "tipo");

	$Province = $d->GetRows("*", "tab_province", "", "", "Prov, Targa", 1);
	/*if (isset($_POST['id_prov']) && ($_POST['id_prov'] != "-"))
	{
		$Comuni 	= $d->GetRows("*", "tab_comuni", "ID_Prov = '"
			. $_POST['id_prov'] . "'", "", "Comune", 1);
	}else{
		$Comuni 	= $d->GetRows("*", "tab_comuni", "", "", "Comune", 1);
	}*/

	/**
	 * riattivata la query in modo da selezionare automaticamente tramite JS 
	 * la città senza dover auto ri postare
	 */
	$Comuni 	= $d->GetRows("*", "tab_comuni", "", "", "Comune", 1);

	//$_POST['stato'] = "Italia";
	$UrlSalva 			= "?act=anagrafica&amp;salva=salva";
	$UrlPag 				= "";
	$ConfSalva 			= "Salvare i dati inseriti?";
	$n_mod_pag 			= 0; // ??????????????

	if(isset($_GET['salva']))
	{
		$d->SaveErrors = "";
		if ((trim($_POST['ragione_sociale']) == "") && (trim($_POST['cognome']) == ""))
		{
			$d->SaveErrors .= "Il campo Ragione Sociale o il campo Cognome non possono essere vuoti.<br />";
		}

		if ((trim($_POST['piva']) == ""))
		{
			$d->SaveErrors .= "Il campo P.Iva/C.F. non può essere vuoto.<br />";
		}else{
			$d->SaveErrors .= Check_piva_cf( $_POST['piva'] );
		}

		if (trim($_POST['ragione_sociale']) == "")
		{
			$_POST['ragione_sociale'] = ucfirst($_POST['cognome'])
				. " " . ucfirst($_POST['nome']);
		}
		
		if (isset($_POST['indirizzo']) 
				&& (trim($_POST['indirizzo']) != "")
				&& ($_POST['id_prov'] != "-")
				&& ($_POST['id_citta'] != "-"))
				
		{
			// esiste l'indirizzo, città e provincia, estraggo le coordinate
		}

		if (($_GET['salva'] == "salva") && ($d->SaveErrors == ""))
		{
			$Esclusioni = array("Salva");
			$d->SaveRow($_POST, "", "tab_anagrafica", 1, "piva");
			//~ $r = $d->SaveRow($_POST, "", "tab_anagrafica", 1, "piva");
			//~ $status = $r[0];
		}

		if (($_GET['salva'] == "modifica") && ($d->SaveErrors == ""))
		{
			$Esclusioni = array("Salva");
			$where = "id_anag = '" . $_GET['tomod'] . "'";
			$d->UpdateRow($_POST, $Esclusioni, "tab_anagrafica", $where, 1);
		}
	}

	if (isset($_GET['tomod']) && (is_numeric($_GET['tomod'])))
	{
		$UrlSalva = "?act=anagrafica&amp;salva=modifica&amp;tomod=" . $_GET['tomod'];
		$row = $d->GetRows("*", "tab_anagrafica",
			"id_anag = '" . $_GET['tomod'] . "'", "", "id_anag");

		if (empty($_POST))
		{
			$_POST = $row[0];
		}
	}
?>

<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>
<script type="text/javascript">
	// https://developers.google.com/maps/documentation/javascript/geocoding#ReverseGeocoding
	// https://developers.google.com/maps/documentation/javascript/examples/geocoding-simple
	// JSON https://developers.google.com/maps/documentation/geocoding/index

	var geocoder;
	var map;
	function initialize() {
	  geocoder = new google.maps.Geocoder();
	  var latlng = new google.maps.LatLng(38.115651, 13.361435); // Palermo
	  var mapOptions = {
	    zoom: 16,
	    center: latlng,
	    mapTypeId: google.maps.MapTypeId.ROADMAP
	  };
	  map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
	};
	
	google.maps.event.addDomListener(window, 'load', initialize);
</script>

<div id="search-form">
	<form method="post" action="<?php echo $_SERVER['REQUEST_URI']; ?>">
		<table class="form" style="width: 100%;">

			<?php if (!isset($_GET['act'])) { ?>
				<tr>
					<td style="text-align: right;">
							<a onclick="Javascript: go('frm_ricerca_avanzata.php?subact=select'); void(0);">
								<img src="../Images/Links/prev.png" alt="prev" />
								<label class="ok" style="cursor: pointer;">Indietro</label>
							</a>

							<a onclick="Javascript: ClosePopup(); void(0);">
								[ <label class="err" style="cursor: pointer;">Chiudi</label> ]
							</a>
					</td>
				</tr>
			<?php
				$d->ShowEditBar("Anagrafica:", $_GET['tomod'], "new,save", "frm_anag_articolo.php");
			}else{
				$d->ShowEditBar("Anagrafica:", $_GET['tomod'], "new,save", $_GET['act']);
			}
			?>
			
			<tr><th colspan="3"><br /></th></tr>

			<tr>
				<th style="text-align: right;">Tipologia Anagrafica:&nbsp;</th>
				<td>					
					<?php $d->DrawControl("select", "id_tipo_anagrafica", 
							"Seleziona la tipologia dell'anagrafica", "\d+", $_POST, 
							"id_tipo", "39em;", "", "", $Tipi, "id_anag", "tipo"); ?>
				</td>
				
				<td rowspan="50">
					<div id="map-canvas" style="width: 29em; height: 45em; z-index: 1000 ! important;"></div>
				</td>
			</tr>
					
			<tr>
				<th style="text-align: right;">Ragione Sociale:&nbsp;</th>
				<td>
					<?php $d->DrawControl("text", "ragione_sociale", "Ragione Sociale", 
						"\w+", $_POST, null, "38.5em", "80"); ?>
				</td> 
			</tr>
			
			<tr>
				<th style="text-align: right;">Cognome:&nbsp;</th>
				<td>
					<?php $d->DrawControl("text", "cognome", "Cognome", 
						"\w+", $_POST, null, "38.5em", "35"); ?>
				</td> 
			</tr>
			
			<tr>
				<th style="text-align: right;">Nome:&nbsp;</th>
				<td>
					<?php $d->DrawControl("text", "nome", "Nome", 
						"\w+", $_POST, null, "38.5em", "35"); ?>
				</td> 
			</tr>
			
			<tr>
				<th style="text-align: right; color: green;">Ricerca Automatica Localit&agrave;:&nbsp;</th>
				<td>
					<?php $d->DrawControl("text", "localita", "Ricerca Automatica Localit&agrave;", 
						null, $_POST, null, "38.5em", "100", "onchange=\"Javascript: codeAddress();\""); ?>
				</td> 
			</tr>
			
			<tr>
				<th style="text-align: right;">Indirizzo completo:&nbsp;</th>
				<td>
					<?php $d->DrawControl("text", "indirizzo", "Indirizzo", 
						"\w+", $_POST, null, "38.5em", "200"); ?>
				</td> 
			</tr>
			
			<!-- <tr>
				<th style="text-align: right;">Civico:&nbsp;</th>
				<td>
					<?php /* $d->DrawControl("text", "civico", "Civico", 
						null, $_POST, null, "38.5em", "35"); */ ?>
				</td> 
			</tr> !-->
			
			<tr>
				<th style="text-align: right;">Stato:&nbsp;</th>
				<td>
					<select name="stato" style="width: 39em;">
						<option value="Italia">Italia</option>
					</select>
				</td>
			</tr>
			
			<tr>
				<th style="text-align: right;">Localit&agrave;:&nbsp;</th>
				<td>
					<?php $d->DrawControl("select", "id_prov", "Seleziona provincia", 
						"\d+", $_POST, null, "auto", "", 
						"onchange=\"Javascript: posta('0', '" . $_SERVER['REQUEST_URI'] . "'); void(0);\"", 
						$Province, "ID", "Targa"); ?>
					
					<?php $d->DrawControl("select", "id_citta", "Seleziona citt&agrave;", 
						"\d+", $_POST, null, "auto", "", 
						"onchange=\"Javascript: posta('0', '" . $_SERVER['REQUEST_URI'] . "'); void(0);\"", 
						$Comuni, "ID", "Comune"); ?>
					
					<?php $d->DrawControl("text", "cap", "Cap", 
						null, $_POST, null, "4em", "10"); ?>
				</td>
			</tr>
			
			<tr>
				<th style="text-align: right;">Telefono (1, 2, 3):&nbsp;</th>
				<td>
					<?php $d->DrawControl("text", "telefono1", "Telefono1", 
						null, $_POST, null, "12em", "20"); ?>
					
					<?php $d->DrawControl("text", "telefono2", "Telefono2", 
						null, $_POST, null, "12em", "20"); ?>
					
					<?php $d->DrawControl("text", "telefono3", "Telefono3", 
						null, $_POST, null, "12em", "20"); ?>
				</td>
			</tr>
			
			<tr>
				<th style="text-align: right;">Fax (1, 2, 3):&nbsp;</th>
				<td>
					<?php $d->DrawControl("text", "fax1", "Fax1", 
						null, $_POST, null, "12em", "20"); ?>
					
					<?php $d->DrawControl("text", "fax2", "Fax2", 
						null, $_POST, null, "12em", "20"); ?>
					
					<?php $d->DrawControl("text", "fax3", "Fax3", 
						null, $_POST, null, "12em", "20"); ?>
				</td>
			</tr>
			
			<tr>
				<th style="text-align: right;">Cellulare (1, 2, 3):&nbsp;</th>
				<td>
					<?php $d->DrawControl("text", "cellulare1", "Cellulare1", 
						null, $_POST, null, "12em", "20"); ?>
					
					<?php $d->DrawControl("text", "cellulare2", "Cellulare2", 
						null, $_POST, null, "12em", "20"); ?>
					
					<?php $d->DrawControl("text", "cellulare3", "Cellulare3", 
						null, $_POST, null, "12em", "20"); ?>
				</td>
			</tr>
			
			<tr>
				<th style="text-align: right;">Partita Iva o Codice Fiscale:&nbsp;</th>
				<td>
					<?php $d->DrawControl("text", "piva", "Partita Iva o Codice Fiscale", 
						"\w+", $_POST, null, "38.5em", "25"); ?>
				</td> 
			</tr>
			
			<tr>
				<th style="text-align: right;">Codice Rea:&nbsp;</th>
				<td>
					<?php $d->DrawControl("text", "rea", "Codice Rea", 
						null, $_POST, null, "38.5em", "25"); ?>
				</td> 
			</tr>
			
			<tr>
				<th style="text-align: right;">Nome contatto:&nbsp;</th>
				<td>
					<?php $d->DrawControl("text", "contatto", "Nome contatto", 
						null, $_POST, null, "38.5em", "60"); ?>
				</td> 
			</tr>
			
			<tr>
				<th style="text-align: right;">Indirizzo E-Mail:&nbsp;</th>
				<td>
					<?php $d->DrawControl("text", "mail", "Indirizzo E-Mail", 
						null, $_POST, null, "38.5em", "70"); ?>
				</td> 
			</tr>
			
			<tr>
				<th style="text-align: right;">Modalit&agrave; di pagamento:&nbsp;</th>
				<td>
					<?php $d->DrawControl("select", "id_pagamento", 
						"Seleziona modalit&agrave; di pagamento", 
						"\d+", $_POST, null, "39em", "", 
						"", $mod_pag, "id_pagamento", "pagamento"); ?>
				</td> 
			</tr>

			<tr>
				<th style="text-align: right;">Nome Banca:&nbsp;</th>
				<td>
					<?php $d->DrawControl("text", "banca", "Nome Banca", 
						null, $_POST, null, "38.5em", "50"); ?>
				</td>
			</tr>
			
			<tr>
				<th style="text-align: right;">Codice Iban:&nbsp;</th>
				<td>
					<?php $d->DrawControl("text", "iban", "Codice Iban", 
						null, $_POST, null, "38.5em", "25"); ?>
				</td>
			</tr>
				
			<tr>
				<th style="text-align: right; vertical-align: top;">Note:&nbsp;</th>
				<td>
					<?php $d->DrawControl("textarea", "note", "Note", 
						null, $_POST, null, "38.5em", "25"); ?>
				</td>
			</tr>
			
			<tr>
				<td>
					<?php 
						$d->DrawControl("hidden", "latitudine", "", null, $_POST);
						$d->DrawControl("hidden", "longitudine", "", null, $_POST);
					?>
				</td>
			</tr>
		</table>
	</form>
</div>

<script type="text/javascript">

	function codeAddress() {
	  var localita = document.getElementsByName('localita')[0].value;
	  geocoder.geocode( { 'address': localita}, function(results, status) {
	    if (status == google.maps.GeocoderStatus.OK) {
	      map.setCenter(results[0].geometry.location);
	      var marker = new google.maps.Marker({
	          map: map,
	          position: results[0].geometry.location
	      });     
	      
	      console.log("0) " + results[0].address_components[0].types
	      	+ " long_name: " + results[0].address_components[0].long_name 
	      	+ ", short_name: " + results[0].address_components[0].short_name + "\n"
	      	
	      	+ "1) " + results[0].address_components[1].types  
	      	+ " long_name: " + results[0].address_components[1].long_name 
	      	+ ", short_name: " + results[0].address_components[1].short_name + "\n"

	      	+ "2) " + results[0].address_components[2].types  
	      	+ " long_name: " + results[0].address_components[2].long_name 
	      	+ ", short_name: " + results[0].address_components[2].short_name + "\n"

	      	+ "3) " + results[0].address_components[3].types  
	      	+ " long_name: " + results[0].address_components[3].long_name 
	      	+ ", short_name: " + results[0].address_components[3].short_name + "\n"

	      	+ "4) " + results[0].address_components[4].types  
	      	+ " long_name: " + results[0].address_components[4].long_name 
	      	+ ", short_name: " + results[0].address_components[4].short_name + "\n"

	      	+ "5) " + results[0].address_components[5].types  
	      	+ " long_name: " + results[0].address_components[5].long_name 
	      	+ ", short_name: " + results[0].address_components[5].short_name + "\n"

	      	+ "6) " + results[0].address_components[6].types  
	      	+ " long_name: " + results[0].address_components[6].long_name 
	      	+ ", short_name: " + results[0].address_components[6].short_name + "\n"

	      	+ "7) " + results[0].address_components[7].types  
	      	+ " long_name: " + results[0].address_components[7].long_name 
	      	+ ", short_name: " + results[0].address_components[7].short_name + "\n"
	      	
	      	+ "fortmatted_address: " + results[0].formatted_address + "\n"

	      	+ "location: " + results[0].geometry.location + "\n");

				
				if (results[0].address_components[1].long_name != null)
				{
					document.getElementsByName('indirizzo')[0].value = results[0].address_components[1].long_name;

					if (results[0].address_components[0].long_name != null)
					{
						// commentato in quanto unificato con il campo indirizzo
						//document.getElementsByName('civico')[0].value = results[0].address_components[0].long_name;
						document.getElementsByName('indirizzo')[0].value += ", " 
								+ results[0].address_components[0].long_name;
					}
				}

				if (results[0].address_components[6].long_name != null)
				{
					// Auto selezione dello stato:
					console.log("Lo stato e' stato riconosciuto ed e': " 
							+ results[0].address_components[6].long_name);
					
					console.log("Verifico la sua presenza nel select, presenti "
							+ document.getElementsByName('stato')[0].length + " valori"); 
					for (var i=0; i<document.getElementsByName('stato')[0].length; i++)
			    {
						//console.log("i == " + i);
				    if (document.getElementsByName('stato')[0].options[i].innerHTML == results[0].address_components[6].long_name)
					  {
				    	console.log(document.getElementsByName('stato')[0].options[i].innerHTML 
								+ " == " +  results[0].address_components[6].long_name);
						  document.getElementsByName('stato')[0].options[i].selected = "selected";
					  }else{
						  console.log(document.getElementsByName('stato')[0].options[i].innerHTML 
								+ " != " +  results[0].address_components[6].long_name);
						}
			    }
				}

				if (results[0].address_components[4].short_name != null)
				{
					// Auto selezione della provincia:
					console.log("La provincia e' stata riconosciuto ed e': " 
							+ results[0].address_components[4].short_name);

					console.log("Verifico la sua presenza nel select, presenti "
							+ document.getElementsByName('id_prov')[0].length + " valori");
					for (var i=0; i<document.getElementsByName('id_prov')[0].length; i++)
			    {
						//console.log("i == " + i);
				    if (document.getElementsByName('id_prov')[0].options[i].innerHTML.trim() == results[0].address_components[4].short_name)
					  {
				    	console.log("'" + document.getElementsByName('id_prov')[0].options[i].innerHTML.trim() + "'" 
								+ " == '" +  results[0].address_components[4].short_name + "'");
						  document.getElementsByName('id_prov')[0].options[i].selected = "selected";

							// posto per la selezione della città:
							//document.forms[0].submit();
					  }else{
						  //console.log("'" + document.getElementsByName('id_prov')[0].options[i].innerHTML.trim() + "'" 
								//+ " != '" +  results[0].address_components[4].short_name + "'");
						}
			    }
				}

				if (results[0].address_components[3].long_name != null)
				{
					// Auto selezione della provincia:
					console.log("La citta' e' stata riconosciuto ed e': " 
							+ results[0].address_components[3].long_name);

					console.log("Verifico la sua presenza nel select, presenti "
							+ document.getElementsByName('id_citta')[0].length + " valori");
					for (var i=0; i<document.getElementsByName('id_citta')[0].length; i++)
			    {
						//console.log("i == " + i);
				    if (document.getElementsByName('id_citta')[0].options[i].innerHTML.trim() == results[0].address_components[3].long_name)
					  {
				    	console.log(document.getElementsByName('id_citta')[0].options[i].innerHTML.trim() 
								+ " == " +  results[0].address_components[3].long_name);
						  document.getElementsByName('id_citta')[0].options[i].selected = "selected";

							// posto per la selezione della città:
							//document.forms[0].submit();
					  }else{
						  //console.log("'" + document.getElementsByName('id_prov')[0].options[i].innerHTML.trim() + "'" 
								//+ " != '" +  results[0].address_components[4].short_name + "'");
						}
			    }
				}

				if (results[0].address_components[7].long_name != null)
				{
					document.getElementsByName('cap')[0].value = results[0].address_components[7].long_name;
				}

				if (results[0].geometry.location != null)
				{
					document.getElementsByName('latitudine')[0].value 	= results[0].geometry.location.lat();
				  document.getElementsByName('longitudine')[0].value 	= results[0].geometry.location.lng();
				}

	    } else {
	      alert('La località non è stata riconosciuta,\n' 
	    		+ 'si è verificato il seguente errore: ' + status);
	    }
	  });
	}
</script>

<?php if (!empty($_POST)) { 

	if (isset($_GET['tomod']) && (is_numeric($_GET['tomod'])))
	{
		// ripristino il campo località tramite i dati salvati ?>
		<script type="text/javascript">
			var SelectedCity = document.getElementsByName('id_citta')[0].selectedIndex;
			if (SelectedCity > 1)
			{
				document.getElementsByName('localita')[0].value = 
					document.getElementsByName('id_citta')[0].options[SelectedCity].innerHTML.trim();
			}

			if (document.getElementsByName('indirizzo')[0].value != "")
			{
				document.getElementsByName('localita')[0].value += 
					", " + document.getElementsByName('indirizzo')[0].value;
			}

			if (document.getElementsByName('cap')[0].value != "")
			{
				document.getElementsByName('localita')[0].value += 
					", " + document.getElementsByName('cap')[0].value;
			}
		</script>
	<?php } ?>

			<script type="text/Javascript">
				initialize(); 
				codeAddress();
			</script>
<?php } ?>

<?php session_start();
	$root = substr($_SERVER["DOCUMENT_ROOT"], 0, -1);
	require_once("$root/Class/XlsExport.php");
	require_once("$root/Class/Net.php");

	$FileName = "tmp/RollOut_WIN7_RITIRI_FIL_" . $_SESSION['rollout']['cod_fil'] . ".xls";
	$x = new XlsExport($FileName, "mail");

	$x->xlsWriteLabel(0, 0, "Branch Code:");
	$x->xlsWriteLabel(0, 1, $_SESSION['rollout']['cod_fil']);
	$x->xlsWriteLabel(1, 0, "N:");
	$x->xlsWriteLabel(1, 1, "Tipologia Apparato:");
	$x->xlsWriteLabel(1, 2, "Marca:");
	$x->xlsWriteLabel(1, 3, "Modello:");
	$x->xlsWriteLabel(1, 4, "Serial Number:");
	$x->xlsWriteLabel(1, 5, "Asset:");

	$xlsRow = 2;
	foreach ($_SESSION['rollout']['dettaglio'] as $key => $field)
	{
		if (($field['provenienza'] == "DA FILIALE")
			&& ($field['tipo_mch'] != "Server"))
		{
			$x->xlsWriteLabel ($xlsRow, 0, $xlsRow-1 . ")");
			$x->xlsWriteLabel ($xlsRow, 1, $field['tipo_mch']);
			$x->xlsWriteLabel ($xlsRow, 2, $field['marca']);
			$x->xlsWriteLabel ($xlsRow, 3, $field['modello']);
			$x->xlsWriteLabel ($xlsRow, 4, $field['serial']);
			$x->xlsWriteLabel ($xlsRow, 5, $field['asset']);
			$xlsRow++;
		}
	}

	$x->WriteFile();
	//'to' => 'UGIS - IT - ROLLOUT EUROPC - UniCredit Group - UniCredit Group <USIROLEURUNIGROUP.ugis@unicreditgroup.eu>',
	//'cc' => 'Leonardi Giorgio (UGIS) <Giorgio.Leonardi@unicreditgroup.eu>, Vesentini Doriana (UGIS) <Doriana.Vesentini@unicreditgroup.eu>',

	$MailData = array('from' => $_SESSION['rollout']['rol_mail'],
											'to' => 'Claudio Giordano <claudio.giordano@autistici.org>', // test
											//'to' => 'ugis.rollout@ts.fujitsu.com',
										 //'ccn' => 'Claudio Giordano <claudio.giordano@autistici.org>, Sun Informatica <commerciale@suninformatica.it>',
								 'subject' => 'RollOut Win7 - Elenco Apparecchiature da Ritirare - Fil. '
										. $_SESSION['rollout']['cod_fil'],
										'body' => "In allegato l'elenco delle apparecchiature ritirate per la filiale in oggetto.",
										 'att' => $FileName);

	//DirectSendMail2($MailData, "smtp.tiscali.it");
	$Status = $d->UpdateRow (array('id_stato' => 7), "", "tab_filiali",
		"cod_fil = '" . $_SESSION['rollout']['cod_fil'] . "'");

?>

<script type="text/javascript">
	alert("Elenco macchine ritirate inviato correttamente.");
	window.close();
</script>

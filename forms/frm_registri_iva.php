<?php
	$root = substr($_SERVER['DOCUMENT_ROOT'], 0, -1);
	require_once ("$root/Function/Strings.php");
	require_once ("$root/Function/Db.php");
	require_once ("$root/Function/Debug.php");
	require_once ("$root/Function/DataTime.php");

	$Anni = array();
	$Registri = GetRows ("tab_tipi_doc", "reg_iva != '-'", "tipo_doc", $db);
	$RowAnni =  GetRows ("tab_piano_conti", "", "data_reg", $db, 1, "data_reg");

	foreach ($RowAnni as $key => $field)
	{
		$tmp_anno = explode("-", $field['data_reg']);

		if (!in_array($tmp_anno[0], $Anni))
		{
			array_push($Anni, $tmp_anno[0]);
		}

	}
	asort($Anni);


	$Mesi = array('01' => 'Gennaio',
					'02' => 'Febbraio',
					'03' => 'Marzo',
					'04' => 'Aprile',
					'05' => 'Maggio',
					'06' => 'Giugno',
					'07' => 'Luglio',
					'08' => 'Agosto',
					'09' => 'Settembre',
					'10' => 'Ottobre',
					'11' => 'Novembre',
					'12' => 'Dicembre');

	if (!empty($_POST) && ($_POST['reg_iva'] != "-"))
	{
		if ($_POST['reg_iva'] == "-")
		{
			$errori .= "Errore, non è stato selezionato un registro iva.<br />";
		}

		if ($_POST['anno'] == "-")
		{
			$_POST['anno'] = date("Y");
		}

		if ($_POST['mese_i'] == "-")
		{
			$_POST['mese_i'] = "01";
		}

		if ($_POST['mese_f'] == "-")
		{
			$_POST['mese_f'] = "12";
		}

		$data_i = $_POST['anno'] . "-" . $_POST['mese_i'] . "-" . "01";
		//~ echo "Debug => data_i: $data_i <br />";

		$data_f = $_POST['anno'] . "-" . $_POST['mese_f'] . "-" . "31";
		//~ echo "Debug => data_f: $data_f <br />";

		$Where = "id_tipo_doc = '" . $_POST['reg_iva'] . "' AND id_iva != '0'"
			. " AND (data_reg >= '$data_i' AND data_reg <= '$data_f')"
			. " AND (desc_iva = '4%' OR desc_iva = '10%' OR desc_iva = '20%')"
			. " GROUP BY data_reg";

		//~ echo "Debug => Where: $Where <br /><br />";
		$Risultati = GetRows ("view_piano_conti", $Where, "data_reg, n_protocollo", $db);
	}
?>
<div id="search-form">
	<form method="post" action="<?php echo $_SERVER['REQUEST_URI']; ?>">
		<table class="form" style="width: 100%;">
			<tr>
				<th>Seleziona i criteri di ricerca:</th>
			</tr>
			<tr>
				<th>
					<select name="reg_iva">
						<option value="-">Seleziona Registro</option>
						<option value="-"></option>
						<?php foreach ($Registri as $key => $field) { ?>
							<option value="<?php echo $field['id_tipo_doc']; ?>"
								<?php if ($_POST['reg_iva'] == $field['id_tipo_doc']) { echo "selected=\"selected\""; } ?> >
								<?php echo $field['tipo_doc']; ?>
							</option>
						<?php } ?>
					</select>

					<select name="mese_i" style="width: 10em;">
						<option value="-">Mese Iniziale</option>
						<option value="-"></option>
						<?php foreach ($Mesi as $key => $field) { ?>
							<option value="<?php echo $key; ?>"
								<?php if ($_POST['mese_i'] == $key) { echo "selected=\"selected\""; } ?> >
								<?php echo $field; ?>
							</option>
						<?php } ?>
					</select>

					<select name="mese_f" style="width: 10em;">
						<option value="-"><label>Mese Finale</label></option>
						<option value="-"></option>
						<?php foreach ($Mesi as $key => $field) { ?>
							<option value="<?php echo $key; ?>"
								<?php if ($_POST['mese_f'] == $key) { echo "selected=\"selected\""; } ?> >
								<?php echo $field; ?>
							</option>
						<?php } ?>
					</select>

					<select name="anno">
						<option value="-">Anno</option>
						<option value="-"></option>
						<?php foreach ($Anni as $key => $field) { ?>
							<option value="<?php echo $field; ?>"
								<?php if ($_POST['anno'] == $field) { echo "selected=\"selected\""; } ?> >
								<?php echo $field; ?>
							</option>
						<?php } ?>
					</select>

					<a onclick="Javascript: posta('0', '<?php echo $_SERVER['REQUEST_URI']; ?>'); void(0);">
						<img src="img/find.png" alt="find"
							style="border: 0px; width: 14px; height: 14px; vertical-align: middle;" />
						Avvia Ricerca
					</a>

					&nbsp;
					<a onclick="Javascript: go('home.php?act=reg_iva'); void(0);"
						style="cursor: pointer;" title="Nuova Ricerca">
						<img src="img/cancel.png" alt="Nuova Ricerca"
							style="border: 0px; width: 14px; height: 14px; vertical-align: middle;" />
						Nuova Ricerca
					</a>
				</th>
			</tr>
			<tr>
				<td>
					<label class="err"><?php echo $errori; ?></label>
				</td>
			</tr>
			<?php if (count($Risultati) > 0) { ?>
				<tr>
					<td>
						<label>Risultati corrispondenti: </label>
						<label class="ok"><?php echo count($Risultati); ?></label>
					</td>
				</tr>
			<?php }elseif ((count($Risultati) == 0) && (!empty($_POST))) { ?>
				<tr>
					<td>
						<label class="err">Nessun risultato corrispondente</label>
					</td>
				</tr>
			<?php } ?>
		</table>
	</form>
</div>

<?php if (count($Risultati) > 0) { ?>
	<div id="search-result-3r">
		<table class="dettaglio" style="width: 100%;">
			<tr>
				<th>Data:</th>
				<th style="text-align: center;">N.Protocollo:</th>
				<th>Causale:</th>
				<th style="text-align: center;">N.Movimento:</th>
			</tr>
			<tr>
				<th colspan="2"><br /></th>
				<th style="text-align: right;">Imponibile:</th>
				<th style="text-align: right;">Imposta:</th>
			</tr>
			<tr><th colspan="8"><hr /></th></tr>
			<?php foreach ($Risultati as $key => $field)
			{
				 $Sum4 = GetRows ("view_piano_conti", "data_reg = '"
					. $field['data_reg'] . "' AND desc_iva = '4%'", "desc_iva", $db,
					1, "sum(imponibile) as imponibile, sum(imposta) as imposta");

				$Sum10 = GetRows ("view_piano_conti", "data_reg = '"
					. $field['data_reg'] . "' AND desc_iva = '10%'", "desc_iva", $db,
					1, "sum(imponibile) as imponibile, sum(imposta) as imposta");

				$Sum20 = GetRows ("view_piano_conti", "data_reg = '"
					. $field['data_reg'] . "' AND desc_iva = '20%'", "desc_iva", $db,
					1, "sum(imponibile) as imponibile, sum(imposta) as imposta");

				 $ImponibileTot4 += $Sum4[0]['imponibile'];
				    $ImpostaTot4 += $Sum4[0]['imposta'];

				$ImponibileTot10 += $Sum10[0]['imponibile'];
				   $ImpostaTot10 += $Sum10[0]['imposta'];

				$ImponibileTot20 += $Sum20[0]['imponibile'];
				   $ImpostaTot20 += $Sum20[0]['imposta'];

				?>
				<tr>
					<th><?php echo Inverti_Data($field['data_reg']); ?></th>
					<th style="text-align: center;"><?php echo $field['n_protocollo']; ?></th>
					<th><?php echo $field['tipo_doc']; ?></th>
					<th style="text-align: center;"><?php echo $field['id_piano_conti']; ?></th>
				</tr>
				<tr>
					<td colspan="2" style="text-align: right;">Totale IVA 4%:</td>
					<td style="text-align: right;"><?php echo $Sum4[0]['imponibile']; ?></td>
					<td style="text-align: right;"><?php echo $Sum4[0]['imposta']; ?></td>
				</tr>
				<tr>
					<td colspan="2" style="text-align: right;">Totale IVA 10%:</td>
					<td style="text-align: right;"><?php echo $Sum10[0]['imponibile']; ?></td>
					<td style="text-align: right;"><?php echo $Sum10[0]['imposta']; ?></td>
				</tr>
				<tr>
					<td colspan="2" style="text-align: right;">Totale IVA 20%:</td>
					<td style="text-align: right;"><?php echo $Sum20[0]['imponibile']; ?></td>
					<td style="text-align: right;"><?php echo $Sum20[0]['imposta']; ?></td>
				</tr>
				<tr><th colspan="8"><br /></th></tr>
			<?php } ?>
			<tr><th colspan="8"><hr /></th></tr>
			<tr>
				<th colspan="2" style="text-align: right;">Totale 4%:</th>
				<th style="text-align: right;"><?php printf("%.2f", $ImponibileTot4); ?></th>
				<th style="text-align: right;"><?php printf("%.2f", $ImpostaTot4); ?></th>
			</tr>
			<tr>
				<th colspan="2" style="text-align: right;">Totale 10%:</th>
				<th style="text-align: right;"><?php printf("%.2f", $ImponibileTot10); ?></th>
				<th style="text-align: right;"><?php printf("%.2f", $ImpostaTot10); ?></th>
			</tr>
			<tr>
				<th colspan="2" style="text-align: right;">Totale 20%:</th>
				<th style="text-align: right;"><?php printf("%.2f", $ImponibileTot20); ?></th>
				<th style="text-align: right;"><?php printf("%.2f", $ImpostaTot20); ?></th>
			</tr>
		</table>
	</div>
<?php } ?>

<?php

	$Depositi 	= $d->GetRows ("*", "tab_depositi", "", "", "desc_dep");
	$TipoArt 		= $d->GetRows ("*", "tab_tipo_art", "", "", "desc_art");
	$Categorie 	= $d->GetRows ("*", "tab_cat_merce", "", "", "categoria");

	if (!isset($_GET['page']))
	{
		$_GET['page'] = "first";
	}


	// Se è stata fatta una ricerca:
	if (!empty($_POST) && isset($_POST['modello']))
	{
		// Compone la query per la ricerca dei prodotti:
		$Where = "1";

		if (trim($_POST['articolo']) != "")
		{
			$val = $d->ModoRicerca($_POST['articolo'], "tutto");
			$val = $d->ExpandSearch($val);
			$Where = " (cod_forn LIKE '%$val%' OR cod_int LIKE '%$val%')";
		}

		if (trim($_POST['modello']) != "")
		{
			$val = $d->ModoRicerca($_POST['modello'], "tutto");
			$val = $d->ExpandSearch($val);
			$Where = " (marca LIKE '%$val%' OR modello LIKE '%$val%')";
		}

		if ($_POST['id_cat'] != "%")
		{
			$Where .= " AND id_cat = '" . $_POST['id_cat'] . "' ";
		}

		if ($_POST['id_tipo'] != "%")
		{
			$Where .= " AND id_tipo = '" . $_POST['id_tipo'] . "' ";
		}

		$tot_res 	= count($d->GetRows ("id_prodotto", "view_prodotti", $Where));
		$order 		= $d->PageSplit($_GET['page'], $tot_res);
		$Prodotti	= $d->GetRows ("*", "view_prodotti", $Where, "", "marca, modello $order");

		echo "<pre>Prodotti";
			print_r($Prodotti);
		echo "</pre>";

	}

	//~ $tot_res = count($d->GetRows ("*", "tab_magazzino", "ck_vend = '1'",
		//~ "cod_int", "cod_int", 1));
	//~ $order = $d->PageSplit($_GET['page'], $tot_res);
	//~ $Prodotti = $d->GetRows ("*", "tab_magazzino", "ck_vend = '1'",
		//~ "cod_int", "cod_int", 1);



?>

<section id="search-form">
	<form method="post" action="<?php echo $_SERVER['REQUEST_URI']; ?>">
		<table class="form" style="width: 100%;">
			<tr>
				<td colspan="2">
					<select name="id_cat" style="max-width: 20em;"
						onchange="Javascript: posta('0', '<?php echo $_SERVER['REQUEST_URI']; ?>');" >
						<option value="%">Tutte le categorie</option>
						<option value="%"></option>
						<?php foreach ($Categorie as $key => $field) { ?>
							<option value="<?php echo $field['id_cat']; ?>"
								<?php if (isset($_POST['id_cat'])
									&& ($_POST['id_cat'] == $field['id_cat'])) {
										echo "selected=\"selected\""; } ?> >
								<?php echo $field['categoria']; ?>
							</option>
						<?php } ?>
					</select>

					<select name="id_tipo" style="max-width: 20em;"
						onchange="Javascript: posta('0', '<?php echo $_SERVER['REQUEST_URI']; ?>');" >
						<option value="%">Tutti i tipi</option>
						<option value="%"></option>
						<?php foreach ($TipoArt as $key => $field) { ?>
							<option value="<?php echo $field['id_art']; ?>"
								<?php if (isset($_POST['id_tipo'])
									&& ($_POST['id_tipo'] == $field['id_art'])) {
										echo "selected=\"selected\""; } ?> >
								<?php echo $field['desc_art']; ?>
							</option>
						<?php } ?>
					</select>

					<input name="articolo" type="text" style="width: 10em;"
						onKeyPress="return SubmitEnter(this,event);"
						placeholder="Codice articolo"
						value="<?php if (isset($_POST['articolo'])) { echo $_POST['articolo']; } ?>" />

					<input name="modello" type="text" style="width: 30em;"
						onKeyPress="return SubmitEnter(this,event);"
						placeholder="Marca o Modello"
						value="<?php if (isset($_POST['modello'])) { echo $_POST['modello']; } ?>" />
				</th>
			</tr>

			<?php $d->ShowResultBar($tot_res, "home.php?act=stato_mag"); ?>

		</table>
	</form>
</section>

<?php if (isset($Prodotti) && count($Prodotti) > 0) { ?>
	<section id="search-result">
		<table class="form" style="width: 100%">
			<tr>
				<td style="width: 20px;"><br /></td>
				<td style="width: 100px;">
					<label>Cod Int:</label>
				</td>
				<td>
					<label>Modello:</label>
				</td>
				<td style="width: 150px;">
					<label>Tipo:</label>
				</td>
				<td style="width: 100px; text-align: center;">
					<label>Giacenza:</label>
				</td>
				<td style="width: 100px; text-align: center;">
					<label>Riordino:</label>
				</td>
			</tr>
			<tr><td colspan="9"><hr /></td></tr>
		</table>

		<?php foreach ($Prodotti as $key => $field) {
			$giac_prod = getGiacenza($field['cod_int'], 0);
			$prodotto = $d->GetRows ("*", "view_prodotti", "cod_int = '"
				. $field['cod_int'] . "'", "", "", 1);

		?>

		<div id="<?php echo "prod_$key"; ?>" class="expander">
			<table class="dettaglio" style="width: 100%;">
				<tr onclick="Javascript: Expand('prod_<?php echo $key; ?>',
					'img<?php echo $key; ?>', '2em', '/Images/Links/bott.png',
					'/Images/Links/top.png'); void(0);" >
					<td style="width: 20px; text-align: center;" >
						<img id="img<?php echo $key; ?>" src="/Images/Links/bott.png"
							alt="expand" title="Espandi articolo"
							onclick="Javascript: Expand('prod_<?php echo $key; ?>', 'img<?php echo $key; ?>', '2em',
								'/Images/Links/bott.png', '/Images/Links/top.png'); void(0);" />
					</td>
					<td style="width: 100px;">
						<label><?php echo $prodotto[0]['cod_int']; ?></label>
					</td>
					<td>
						<label><?php echo $prodotto[0]['modello']; ?></label>
					</td>
					<td style="width: 150px;">
						<label><?php echo $prodotto[0]['desc_art']; ?></label>
					</td>
					<td style="width: 100px; text-align: right;">
						<label><?php echo $giac_prod . " " . $prodotto[0]['um']; ?></label>
					</td>
					<td style="width: 100px; text-align: right;">
						<label><?php echo $prodotto[0]['riordino']; ?></label>
					</td>
				</tr>
			</table>

			<?php // Dettaglio Articolo
				$dett_prodotto = $d->GetRows ("*, sum(qta) as tot_by_doc", "tab_magazzino m, tab_documenti d",
					"cod_int = '" . $field['cod_int']
					. "' AND m.id_documento = d.id_documento", "m.id_documento", "data_acq", 1); ?>

				<table style="width: 100%;" class="dettaglio">
					<tr>
						<th><br /></th>
						<th>Data Documento:</th>
						<th>Codice Documento:</th>
						<th>Quantit&agrave;:</th>
						<th>Prezzo:</th>
						<th>Sconto:</th>
						<th>Imponibile:</th>
					</tr>

					<?php foreach ($dett_prodotto as $key_dett => $field_dett) {
						$tot = ($field_dett['qta']*$field_dett['prz']);
						$sco = (($field_dett['qta']*$field_dett['prz'])*$field_dett['sco'])/100;
						$tot = ($tot-$sco); ?>

						<tr>
							<td style="width: 75px; text-align: right;">
								<label>&bull;</label>
							</td>
							<td>
								<?php echo $d->Inverti_Data($field_dett['data_acq']); ?>
							</td>
							<td>
								<?php echo $field_dett['cod_documento']; ?>
							</td>
							<td>
								<?php printf("%.2f", $field_dett['tot_by_doc']);
									echo " " . $prodotto[0]['um']; ?>
							</td>
							<td><?php printf("%.2f €", $field_dett['prz']); ?></td>
							<td><?php printf("%.2f %%", $field_dett['sco']); ?></td>
							<td><?php printf("%.2f €", $tot); ?></td>
						</tr>
					<?php } ?>
				</table>
			</div>
		<?php } ?>
	</div>
</section>
<?php } ?>

<?php
	$root = substr($_SERVER['DOCUMENT_ROOT'], 0, -1);
	require_once("$root/Function/Db.php");
	require_once("$root/Function/Debug.php");
	require_once("$root/Function/DataTime.php");

	$da_array = array('-' => '-', 'd' => 'Dare', 'a' => 'Avere');
		 $Iva = GetRows ("tab_iva", "", "id_iva", $db);
	 $TipiDoc = GetRows ("tab_tipi_doc", '', "tipo_doc", $db);	//Causale documento


	if (isset($_GET['delanagdoc']))
	{
		unset($_SESSION['piano']['id_anagrafica']);
	}


	if (isset($_SESSION['piano']['id_anagrafica']))
	{
		$Anagrafica = GetRows ("tab_anagrafica", "id_anag = '"
			. $_SESSION['piano']['id_anagrafica'] . "'", "", $db);
	}

	if (isset($_GET['reset']) && ($_GET['reset'] == 0))
	{
		unset($_SESSION['piano']);
	}

	if (isset($_POST['cod_documento']))
	{
		$_SESSION['piano']['cod_documento'] = trim($_POST['cod_documento']);
	}

	if (isset($_POST['id_causale_doc']) && ($_POST['id_causale_doc'] != "-"))
	{
		if (($_SESSION['piano']['id_causale_doc'] != $_POST['id_causale_doc'])
			|| ($_SESSION['piano']['id_causale_doc'] == ''))
		{
/*
			echo "Debug => è stato cambiato il tipo di documento <br />";
*/
			$_SESSION['piano']['id_causale_doc'] = $_POST['id_causale_doc'];
			$rif_iva = GetRows ("tab_tipi_doc", "id_tipo_doc = '"
				. $_SESSION['piano']['id_causale_doc'] . "'", "tipo_doc", $db);
			$_SESSION['piano']['reg_iva'] = $rif_iva[0]['reg_iva'];
			$_SESSION['piano']['dettaglio'] = array();
			$_SESSION['piano']['iva'] = array();
/*
			$_SESSION['piano']['reg_iva'] = "-";
*/
			$n_prot = "-";

			switch ($_SESSION['piano']['reg_iva'])
			{
				case "a":
				case "ai":
					array_push($_SESSION['piano']['dettaglio'], array('da' => 'd'));
					array_push($_SESSION['piano']['dettaglio'], array('da' => 'a', 'id_sottoconto' => '154'));

					$_SESSION['piano']['iva'] = array();
					//~ iva ns/credito id 57
					array_push($_SESSION['piano']['iva'], array('da' => 'd', 'id_sottoconto' => '57'));
					break;

				case "v":
				case "vi":
					array_push($_SESSION['piano']['dettaglio'], array('da' => 'd', 'id_sottoconto' => '42'));
					array_push($_SESSION['piano']['dettaglio'], array('da' => 'a'));

					//~ iva ns/debito id 160
					$_SESSION['piano']['iva'] = array();
					array_push($_SESSION['piano']['iva'], array('da' => 'a', 'id_sottoconto' => '160'));
					break;

				case "c":
					// banche c/c attivi
					array_push($_SESSION['piano']['dettaglio'], array('da' => 'd', 'id_sottoconto' => '74'));
					// prodotti c/vendite
					array_push($_SESSION['piano']['dettaglio'], array('da' => 'a', 'id_sottoconto' => '291'));
					//~ rimborsi spese 330
					array_push($_SESSION['piano']['dettaglio'], array('da' => 'a', 'id_sottoconto' => '330'));

					//~ iva ns/debito id 160
					$_SESSION['piano']['iva'] = array();
					array_push($_SESSION['piano']['iva'], array('da' => 'a', 'id_sottoconto' => '160', 'id_iva' => 1));
					array_push($_SESSION['piano']['iva'], array('da' => 'a', 'id_sottoconto' => '160', 'id_iva' => 2));
					break;
			}
		}
	}

	if (isset($_GET['tomod']))
	{
		if (!$_SESSION['piano']['modificato'] == "y")
		{
			/*
			 * 10/09/2009 13:47:35 CEST Claudio Giordano
			 *
			 * in modalita' modifica, se non sono state già
			 * apportate modifiche importo in sessione i dati.
			 */

			unset($_SESSION['piano']);

			//~ Leggo i dati dell'operazione e li metto in sessione:
			$DatiOp = GetRows ("tab_piano_conti, tab_tipi_doc", "id_piano_conti = '"
				. $_GET['tomod'] . "' AND tab_piano_conti.id_causale_doc = tab_tipi_doc.id_tipo_doc",
				"", $db);

			 $_SESSION['piano']['cod_documento'] = $DatiOp[0]['cod_documento'];
			      $_SESSION['piano']['data_doc'] = Inverti_Data($DatiOp[0]['data_doc'], "/", "-");
			      $_SESSION['piano']['data_reg'] = Inverti_Data($DatiOp[0]['data_reg'], "/", "-");
			$_SESSION['piano']['id_causale_doc'] = $DatiOp[0]['id_causale_doc'];
			  $_SESSION['piano']['n_protocollo'] = $DatiOp[0]['n_protocollo'];
				   $_SESSION['piano']['reg_iva'] = $DatiOp[0]['reg_iva'];

			//~ Leggo il dettaglio dell'operazione e lo metto in sessione:
			$Dettaglio = GetRows ("view_piano_conti", "id_piano_conti = '"
				. $_GET['tomod'] . "'" , "", $db);
			$_SESSION['piano']['dettaglio'] = array();
			      $_SESSION['piano']['iva'] = array();

			foreach ($Dettaglio as $key => $field)
			{
				if ($field['id_iva'] == 0)
				{
					array_push($_SESSION['piano']['dettaglio'],
						array(         'da' => $field['da'],
									 'desc' => $field['descr'],
							'id_sottoconto' => $field['id_sottoconto'],
							'id_anagrafica' => $field['id_anagrafica'],
							   	'importo_d' => $field['importo_d'],
								'importo_a' => $field['importo_a'])
					);
				}else{
					//~ if ($field['da'] == "d")
					//~ {
						//~ $imponibile = $field['importo_d'];
					//~ }else{
						//~ $imponibile = $field['importo_a'];
					//~ }

					array_push($_SESSION['piano']['iva'],
						array( 		  'da' => $field['da'],
								  'id_iva' => $field['id_iva'],
							  'imponibile' => $field['imponibile'])
					);
				}
			}
		}
	}

	if (isset($_GET['svuota']) && ($_GET['svuota'] == 0))
	{
		unset($_SESSION['piano']['dettaglio']);
		$_SESSION['piano']['modificato'] = "y";
		$_SESSION['piano']['tomod'] = $_GET['tomod'];
	}

	if (isset($_GET['svuota_iva']) && ($_GET['svuota_iva'] == 0))
	{
		unset($_SESSION['piano']['iva']);
		$_SESSION['piano']['modificato'] = "y";
		$_SESSION['piano']['tomod'] = $_GET['tomod'];
	}

	if (isset($_GET['del']) && ($_GET['del'] != ""))
	{
		$iddel = $_GET['del'];
		unset($_SESSION['piano']['dettaglio'][$iddel]);
		$_SESSION['piano']['modificato'] = "y";
		$_SESSION['piano']['tomod'] = $_GET['tomod'];
	}

	if (isset($_GET['del_iva']) && ($_GET['del_iva'] != ""))
	{
		$iddel = $_GET['del_iva'];
		unset($_SESSION['piano']['iva'][$iddel]);
		$_SESSION['piano']['modificato'] = "y";
		$_SESSION['piano']['tomod'] = $_GET['tomod'];
	}

	if (isset($_GET['delanag']) && ($_GET['delanag'] != ""))
	{
		$key = $_GET['delanag'];
		unset($_SESSION['piano']['dettaglio'][$key]['id_anagrafica']);
		$_SESSION['piano']['modificato'] = "y";
		$_SESSION['piano']['tomod'] = $_GET['tomod'];
	}

	if (isset($_GET['delconto']) && ($_GET['delconto'] != ""))
	{
		$key = $_GET['delconto'];
		unset($_SESSION['piano']['dettaglio'][$key]['id_sottoconto']);
		$_SESSION['piano']['modificato'] = "y";
		$_SESSION['piano']['tomod'] = $_GET['tomod'];
	}

	if (isset($_GET['add']) && ($_GET['add'] != ""))
	{
		if (!isset($_SESSION['piano']['dettaglio']))
		{
			$_SESSION['piano']['dettaglio'] = array();
		}

		$_SESSION['piano']['modificato'] = "y";
		$_SESSION['piano']['tomod'] = $_GET['tomod'];
		array_push($_SESSION['piano']['dettaglio'], array());
	}

	if (isset($_GET['add_iva']) && ($_GET['add_iva'] != ""))
	{
		if (!isset($_SESSION['piano']['iva']))
		{
			$_SESSION['piano']['iva'] = array();
		}

		switch ($_SESSION['piano']['reg_iva'])
		{
			case "a":
			case "ai":
				//~ iva ns/credito id 57
				array_push($_SESSION['piano']['iva'], array('da' => 'd', 'id_sottoconto' => '57'));
				break;

			case "v":
			case "vi":
				//~ iva ns/debito id 160
				array_push($_SESSION['piano']['iva'], array('da' => 'a', 'id_sottoconto' => '160'));
				break;

			case "c":
				//~ iva ns/debito id 160
				array_push($_SESSION['piano']['iva'], array('da' => 'a', 'id_sottoconto' => '160'));
				break;
		}

		$_SESSION['piano']['modificato'] = "y";
		$_SESSION['piano']['tomod'] = $_GET['tomod'];
	}

	if ($_SESSION['piano']['reg_iva'] != "-")
	{
/*
		echo "Debug => prot: $n_prot <br />";
*/
		if (!isset($_GET['tomod']))
		{
			/*
			 * se l'operazione prevede la movimentazione dell'iva
			 * genero un numero di protocollo progressivo per l'anno
			 * e per il tipo di operazione
			 */
			$w_prog = "id_causale_doc = '" . $_SESSION['piano']['id_causale_doc'] . "'"
				. " AND (tab_piano_conti.data_reg >= '" . date('Y-01-01') . "'"
				. " AND tab_piano_conti.data_reg <= '" . date('Y-12-31') . "')";
			//~ echo "Debug => w_prog: $w_prog <br />";
			$Count = GetRows ("tab_piano_conti", $w_prog, "", $db, 1, "count(id_piano_conti) as conteggio");

			$n_prot = strtoupper($_SESSION['piano']['reg_iva'])
				. ($Count[0]['conteggio'] + 1) . "/" . date('y');
/*
			echo "Debug => prot: $n_prot <br />";
*/
		}else{
			$n_prot = $_SESSION['piano']['n_protocollo'];
		}
	}else{
		$n_prot = "-";
	}

	if (isset($_GET['salva']))
	{
		$errori = "";
		$Bilanciamento = 0;

		if (trim($_SESSION['piano']['data_doc']) == "")
		{
			$errori .= "Errore, la data documento non è valida.<br />";
		}

		if (trim($_SESSION['piano']['data_reg']) == "")
		{
			$errori .= "Errore, la data registrazione non è valida.<br />";
		}

		if ($_SESSION['piano']['id_causale_doc'] == "")
		{
			$errori .= "Errore, non è stato selezionata una causale per il operazione.<br />";
		}

		if ((isset($_SESSION['piano']['reg_iva'])
				&& $_SESSION['piano']['reg_iva'] != "-")
				&& (count($_SESSION['piano']['iva']) < 1))
		{
			$errori .= "Errore, l'operazione richiede la movimentazione dell'iva "
				. "ma non è stata inserita nessuna voce.<br />";
		}

		//~  verifico il dettaglio_op:
		if (isset($_SESSION['piano']['dettaglio'])
			&& (count($_SESSION['piano']['dettaglio']) > 0))
		{
			foreach ($_SESSION['piano']['dettaglio'] as $key => $field)
			{
				//echo "Debug [dettaglio] => bil pre: $Bilanciamento <br />";
				if ($field['da'] == "d")
				{
					$Bilanciamento -= round($field['importo_d'], 2);
				}else{
					$Bilanciamento += round($field['importo_a'], 2);
				}
				$Bilanciamento = round($Bilanciamento, 2);
				//~ echo "Debug [dettaglio] => bil post: $Bilanciamento <br /><br />";
			}

			if ((isset($_SESSION['piano']['reg_iva'])
				&& $_SESSION['piano']['reg_iva'] != "-")
				&& (count($_SESSION['piano']['iva']) != 0))
			{
				foreach ($_SESSION['piano']['iva'] as $key => $field)
				{
					//~ echo "Debug [iva] => bil pre: $Bilanciamento <br />";
					switch ($_SESSION['piano']['reg_iva'])
					{
						case "a":
							//~ echo "Debug => imponibile: " . $field['imponibile'] . " <br />";
							//~ echo "Debug =>   calc iva: " . $Iva[$field['id_iva']-1]['iva'] . " <br />";

							$iva = ($field['imponibile']*$Iva[$field['id_iva']-1]['iva'])-$field['imponibile'];
							//~ echo "Debug =>        iva: $iva<br />";
							$Bilanciamento -= round($iva, 2);
							break;

						case "v":
						case "c":
							$iva = ($field['imponibile']-($field['imponibile']/$Iva[$field['id_iva']-1]['iva']));
							$Bilanciamento +=  round($iva, 2);
							break;
					}
					//~ echo "Debug => [iva] bil post: $Bilanciamento <br /><br />";
				}
			}

			/*
			 * 31/08/2009 19:19:30 CEST Claudio Giordano
			 *
			 * arrotondamento finale
			 */
			$Bilanciamento = round($Bilanciamento, 2);
			if (($Bilanciamento != "0.00") || ($Bilanciamento != "0"))
			{
				//~ echo "Debug => bil post post: $Bilanciamento <br />";
				$errori .= "Errore, l'operazione non è bilanciata <br />";
			}
		}else{
			$errori .= "Errore, l'operazione non contiene nessuna voce.<br />";
		}

		//~ verifico dettaglio_iva:
		if ((isset($_SESSION['piano']['reg_iva'])
			&& $_SESSION['piano']['reg_iva'] != "-")
			&& (count($_SESSION['piano']['iva']) > 0))
		{
			foreach ($_SESSION['piano']['iva'] as $key => $field)
			{
				if ($field['id_iva'] == "-")
				{
					$errori .= "Errore, non è stata selezionata un'iva per la voce $key.<br />";
				}

				if (!is_numeric($field['imponibile'])
					|| $field['imponibile'] <= 0)
				{
					$errori .= "Errore, l'imponibile inserito per la voce $key non è valido.<br />";
				}
			}
		}

		$Valori = array('cod_documento' => $_SESSION['piano']['cod_documento'],
						     'data_doc' => Inverti_Data($_SESSION['piano']['data_doc'], "-", "/"),
							 'data_reg' => Inverti_Data($_SESSION['piano']['data_reg'], "-", "/"),
					   'id_causale_doc' => $_SESSION['piano']['id_causale_doc'],
					     'n_protocollo' => $n_prot,
								'spese' => round($_SESSION['piano']['spese'], 2),
							  'id_user' => $_SESSION['id_user']);

		//~ Salvo il documento:
		if (($_GET['salva'] == "salva") && ($errori == ""))
		{
			if (!isset($_GET['tomod']))
			{
				$Status = SaveRow ($Valori, "", "tab_piano_conti", $db, 0);
				$IdDocumento = $Status[1];	//~ last insert id
				//echo "Debug => nuovo inserimento: $IdDocumento<br />";
			}else{
				$Status = UpdateRow ($Valori, "", "tab_piano_conti", "id_piano_conti = '"
					. $_GET['tomod'] . "'", $db, 0);
				$IdDocumento = $_GET['tomod'];

				//echo "Debug => update operazione: $IdDocumento <br />";

				//~ Droppo il precedente dettaglio dell'operazione:
				$Status = DeleteRows ("tab_dett_piano_conti", "id_piano_conti = '"
					. $_GET['tomod'] . "'", $db);

				//echo "Debug => drop dettaglio <br />";
			}
		}

		//~ verifico la validità del dettaglio
		foreach ($_SESSION['piano']['dettaglio'] as $key => $field)
		{
			if ($field['da'] == '-')
			{
				$errori .= "Errore, non è stato selezionato il campo dare/avere "
					. " per la voce " . ($key+1) . ".<br />";
			}

			if (!is_numeric($field['id_sottoconto']))
			{
				$errori .= "Errore, non è stato selezionato un conto "
					. " per la voce " . ($key+1) . ".<br />";
			}

			if (($field['da'] == "d") && ($field['importo_d'] <= 0))
			{
				$errori .= "Errore, l'importo del campo dare per la voce " . ($key+1)
				. " non è valido.<br />";
			}

			if (($field['da'] == "a") && ($field['importo_a'] <= 0))
			{
				$errori .= "Errore, l'importo del campo avere per la voce " . ($key+1)
				. " non è valido.<br />";
			}

			$Conto = GetRows ("tab_sottoconti", "id_sottoconto = '"
				. $field['id_sottoconto'] . "'", "", $db);
			if (($Conto[0]['req_anag'] == 0)
				&& ($field['id_anagrafica'] == ''))
			{
				$errori .= "Errore, non è stata selezionata un'anagrafica per la voce "
					. ($key+1) . ".<br />";
			}
		}

		if (isset($_SESSION['piano']['dettaglio'])
			&& (count($_SESSION['piano']['dettaglio']) > 0)
			&& ($errori == ""))
		{
			//~ Salvo il dettaglio dell'operazione
			foreach ($_SESSION['piano']['dettaglio'] as $key => $field)
			{
				$ar_dettaglio_op = array(  'da' => $field['da'],
								'id_anagrafica' => $field['id_anagrafica'],
										'descr' => $field['desc'],
									'importo_d' => $field['importo_d'],
									'importo_a' => $field['importo_a'],
									   'id_iva' => $field['id_iva'],
								'id_sottoconto' => $field['id_sottoconto'],
							   'id_piano_conti' => $IdDocumento);

				$Status = SaveRow ($ar_dettaglio_op, "", "tab_dett_piano_conti", $db);
				$Result = $Status[0] . " <label class=\"ok\">N° Movimento: $IdDocumento</label>";

				if ($_SESSION['piano']['reg_iva'] != "-")
				{
					$Result .= ", <label class=\"ok\">N° Protocollo: $n_prot</label>";
				}
			}
		}

		//~ verifico e salvo il dettaglio dell'iva se presente/previsto:
		//~ echo "Debug => verifico e salvo il dettaglio dell'iva se presente/previsto: <br />";
		if (isset($_SESSION['piano']['iva'])
			&& (count($_SESSION['piano']['iva']) > 0)
			&& ($errori == ""))
		{
			//~ echo "Debug => analizzo l'array dell'iva <br />";
			foreach ($_SESSION['piano']['iva'] as $key => $field)
			{
				switch ($_SESSION['piano']['reg_iva'])
				{
					case "a":
					case "ai":
						$iva = ($field['imponibile']*$Iva[$field['id_iva']-1]['iva'])-$field['imponibile'];
						$importo_d = $iva;
						$importo_a = 0;
						//~ echo "Debug => importo dare: $iva <br />";
						$IdSottoconto = 57;
						break;

					case "v":
					case "vi":
					case "c":
						$iva = ($field['imponibile']-($field['imponibile']/$Iva[$field['id_iva']-1]['iva']));
						$importo_d = 0;
						$importo_a = $iva;
						//~ echo "Debug => importo avere: $iva <br />";
						$IdSottoconto = 160;
						break;
				}

				$Valori = array(		   'da' => $field['da'],
								'id_anagrafica' => '0', 	//~ $field['id_anagrafica'],
										'descr' => '',  	//~ $field['desc'],
									'importo_d' => $importo_d,
									'importo_a' => $importo_a,
								   'imponibile' => $field['imponibile'],
									  'imposta' => $iva,
									   'id_iva' => $field['id_iva'],
								'id_sottoconto' => $IdSottoconto,
							   'id_piano_conti' => $IdDocumento);

				$Status = SaveRow ($Valori, "", "tab_dett_piano_conti", $db);
				$Result = $Status[0] . " <label class=\"ok\">N° Movimento: $IdDocumento</label>"
					. ", <label class=\"ok\">N° Protocollo: $n_prot</label>";


				/*
				 * 10/09/2009 15:58:54 CEST Claudio Giordano
				 *
				 * resetto il flag di modifica:
				 */
				unset($_SESSION['piano']['modificato']);
			}
		}
	}

	if (isset($_SESSION['piano']['id_anagrafica']))
	{
		$Anagrafica = GetRows ("tab_anagrafica", "id_anag = '"
			. $_SESSION['piano']['id_anagrafica'] . "'", "", $db);
		$_SESSION['piano']['modificato'] = "y";
		$_SESSION['piano']['tomod'] = $_GET['tomod'];
	}

	/*if (isset($_POST['spese']))
	{
		if (is_numeric($_POST['spese']))
		{
			$_SESSION['piano']['spese'] = $_POST['spese'];
		}else{
			?><label class="err">
				Errore, il valore di spesa immesso non è valido.<br />
			</label><?php
		}
	}*/

	if (isset($_POST['cod_documento']))
	{
		$_SESSION['piano']['cod_documento'] = $_POST['cod_documento'];
		$_SESSION['piano']['modificato'] = "y";
		$_SESSION['piano']['tomod'] = $_GET['tomod'];
	}

	if (isset($_POST['data_doc']))
	{
		$_SESSION['piano']['data_doc'] = $_POST['data_doc'];
		$_SESSION['piano']['modificato'] = "y";
		$_SESSION['piano']['tomod'] = $_GET['tomod'];
	}

	if (isset($_POST['data_reg']))
	{
		$_SESSION['piano']['data_reg'] = $_POST['data_reg'];
		$_SESSION['piano']['modificato'] = "y";
		$_SESSION['piano']['tomod'] = $_GET['tomod'];
	}


	if (!empty($_POST) && isset($_GET['key']) && (!isset($_POST['cod_documento'])))
	{
		$key = $_GET['key'];
		$_SESSION['piano']['dettaglio'][$key]['da'] = $_POST['da' . $key];
		$_SESSION['piano']['dettaglio'][$key]['desc'] = $_POST['desc' . $key];
		$_SESSION['piano']['dettaglio'][$key]['importo_d'] = $_POST['importo_d' . $key];
		$_SESSION['piano']['dettaglio'][$key]['importo_a'] = $_POST['importo_a' . $key];
		$_SESSION['piano']['dettaglio'][$key]['id_iva'] = $_POST['id_iva' . $key];
		//~ $_SESSION['piano']['dettaglio'][$key]['causale'] = $_POST['causale' . $key];

		$_SESSION['piano']['modificato'] = "y";
		$_SESSION['piano']['tomod'] = $_GET['tomod'];
	}

	if (!empty($_POST) && isset($_GET['key_iva']) && (!isset($_POST['cod_documento'])))
	{
		$key = $_GET['key_iva'];
		$_SESSION['piano']['iva'][$key]['id_iva'] = $_POST['id_iva' . $key];
		$_SESSION['piano']['iva'][$key]['imponibile'] = $_POST['imponibile' . $key];

		$_SESSION['piano']['modificato'] = "y";
		$_SESSION['piano']['tomod'] = $_GET['tomod'];
	}
?>

<div id="search-form">
	<form method="post" action="<?php echo $_SERVER['REQUEST_URI']; ?>">
		<table style="width: 100%;" class="form">
			<tr>
				<th colspan="3">
					Dati Operazione:

					&nbsp;
					<a onclick="javascript: go_conf2('Procedere ed annullare l\'operazione attuale?',
						'<?php echo "?act=piano_conti&amp;reset=0"; ?>');  void(0);">
						<img src="img/new.png" alt="new" />Nuova
					</a>

					&nbsp;
					<a onclick="javascript: go_conf2('Salvare l\'operazione?',
						'?act=piano_conti&amp;salva=salva<?php
							if (isset($_GET['tomod'])) { echo "&amp;tomod=" . $_GET['tomod']; } ?>'); void(0);">
						<img src="img/save.png" alt="save" />Salva
					</a>
				</th>
				<th style="text-align: right;">
					<?php if (isset($_GET['tomod'])) { ?>
						<label class="err_status">In Modifica&nbsp;</label>
					<?php }else{ ?>
						<label class="err_status">Nuovo Inserimento&nbsp;</label>
					<?php } ?>
				</th>
			</tr>

			<tr>
				<td style="text-align: right;">
					<label>Causale:</label>
				</td>
				<td style="text-align: left;">
					<select name="id_causale_doc"
						onchange="javascript: conferma_posta('0', 'Modificando la causale l\'operazione verrà resettata, continuare?',
							'<?php echo "?act=piano_conti"; ?>', '?act=piano_conti'); void(0);">
						<option value="-">-</option>
						<?php foreach ($TipiDoc as $key => $field) { ?>
							<option value="<?php echo $field['id_tipo_doc']; ?>"
								<?php if(isset($_SESSION['piano']['id_causale_doc'])
									&& ($_SESSION['piano']['id_causale_doc'] == $field['id_tipo_doc']))
										{
											echo ' selected="selected"';
										} ?> > <?php echo $field['tipo_doc']; ?>
							</option>
						<?php } ?>
					</select>
				</td>

				<td style="text-align: right;">
					<label>N° Documento:</label>
				</td>
				<td style="text-align: left;">
					<input name="cod_documento" type="text" style="width: 14em; text-align: right;"
						onchange="javascript: posta('0', '<?php echo "?act=piano_conti";
						if (isset($_GET['tomod'])) { echo "&amp;tomod=" . $_GET['tomod']; }?>'); void(0);"
						value="<?php if(isset($_SESSION['piano']['cod_documento'])) {
								echo $_SESSION['piano']['cod_documento']; } ?>" />
				</td>
				<td></td>
			</tr>
			<tr>
				<td style="text-align: right;">
					<label>Data Registrazione:</label>
				</td>
				<td>
					<input name="data_reg" style="width: 14em; text-align: right;"
						onfocus="showCalendarControl(this);"
						onchange="javascript: posta('0', '<?php echo "?act=piano_conti";
						 if (isset($_GET['tomod'])) { echo "&amp;tomod=" . $_GET['tomod']; } ?>'); void(0);"
						value="<?php if (isset($_SESSION['piano']['data_reg']))
							{ echo $_SESSION['piano']['data_reg']; } ?>" />
				</td>

				<td style="text-align: right; ">
					<label>N° Protocollo:</label>
				</td>
				<td>
                    <label class="err">
                        <?php if (isset($_SESSION['piano']['n_protocollo'])) {
                                echo $_SESSION['piano']['n_protocollo'];
                            }else{
                                echo "Non ancora assegnato ($n_prot)";
                            } ?>
                        <!--<input name="spese" style="width: 14em; text-align: right;"
                            onchange="javascript: posta('0', '<?php //echo "?act=piano_conti"; ?>'); void(0);"
                            value="<?php //printf("%.2f", $_SESSION['piano']['spese']); ?>" />!-->
                    </label>
				</td>
			</tr>

			<tr>
				<td style="text-align: right; ">
					<label>Data Documento:</label>
				</td>
				<td>
					<input name="data_doc" style="width: 14em; text-align: right;"
						onfocus="showCalendarControl(this);"
						onchange="javascript: posta('0', '<?php echo "?act=piano_conti";
							if (isset($_GET['tomod'])) { echo "&amp;tomod=" . $_GET['tomod']; } ?>'); void(0);"
						value="<?php if (isset($_SESSION['piano']['data_doc']))
							{ echo $_SESSION['piano']['data_doc']; } ?>" />
				</td>

				<td style="text-align: right;">
					<label>Riferimento Iva:</label>
				</td>
				<td>
					<?php if (isset($_SESSION['piano']['id_causale_doc']))
					{
						echo strtoupper($_SESSION['piano']['reg_iva']);
					} ?>
				</td>
			</tr>
			<tr>
				<th style="text-align: right;" colspan="4">
					<a onclick="javascript: posta('0', '<?php echo "?act=piano_conti";
						if (isset($_GET['tomod'])) { echo "&amp;tomod=" . $_GET['tomod']; } ?>'); void(0);">
						<img src="img/apply.png" alt="Conferma Dati" />Conferma Dati
					</a>
				</th>
			</tr>
		</table>
	</form>
</div>

<div id="search-result-5r">
	<div id="dett_contabile" class="expander">
		<table style="width: 100%;" class="form">
			<tr>
                <th style="width: 20px; text-align: center;">
                    <img id="img_cont" alt="img_cont" src="img/bott.png"
						onclick="Javascript: Expand('dett_contabile', 'img_cont', '1.7em',
							'img/bott.png', 'img/top.png'); void(0);"
						title="Click per Visualizzare/Nascondere il dettaglio" />
                </th>
				<th colspan="4">
					Dettaglio Contabile:

					&nbsp;
					<a onclick="Javascript: go('?act=piano_conti&amp;add=1<?php
						if (isset($_GET['tomod'])) { echo "&amp;tomod=" . $_GET['tomod']; } ?>'); void(0);">
						<img src="img/add.png" alt="add" title="Aggiungi Voce" />Voce
					</a>

					&nbsp;
					<a onclick="javascript: conferma_salva('1', 'Rimuovere le voci selezionate dal dettaglio?',
						'<?php echo "?act=piano_conti&amp;del_sel=0";
						if (isset($_GET['tomod'])) { echo "&amp;tomod=" . $_GET['tomod']; } ?>'); void(0);">
						<img src="img/del.png" alt="del" /> Selezionati
					</a>

					&nbsp;
					<a onclick="javascript: go_conf2('Rimuovere tutte le voci dal dettaglio operazione?',
						'<?php echo "?act=piano_conti&amp;svuota=0";
						if (isset($_GET['tomod'])) { echo "&amp;tomod=" . $_GET['tomod']; }?>'); void(0);">
						<img src="img/clear.png" alt="clear" />Voci (
							<label class="err" title="Totale voci operazione">
								<?php echo count($_SESSION['piano']['dettaglio']); ?>
							</label>)
					</a>
				</th>
			</tr>

			<tr>
				<td colspan="5">
					<table border="1" style="width: 100%;" rules="none">
						<?php if (isset($errori) && ($errori != "")) { ?>
							<tr>
								<td colspan="8">
									<label class="err">
										<?php echo $errori; ?>
									</label>
								</td>
							</tr>
						<?php } ?>
						<?php if (isset($Result)) { ?>
							<tr>
								<td colspan="8">
									<?php echo $Result; ?>
								</td>
							</tr>
						<?php } ?>

						<tr>
							<td colspan="8">
								<?php if ((count($_SESSION['piano']['dettaglio']) != 0)) { ?>
									<form method="post" action="<?php echo $_SERVER['REQUEST_URI']; ?>">
										<table class="dettaglio" style="width: 100%;">
											<tr>
												<th><br /></th>
												<th style="width: 6em; text-align: left;">
													<label title="Dare/Avere">D/A:</label>
												</th>
												<th style="text-align: left;">
													<label>Conto:</label>
												</th>
												<th style="text-align: left;">
													<label>Rif. Anagrafica:</label>
												</th>
												<th style="text-align: left;">
													<label>Descrizione:</label>
												</th>
												<th style="text-align: center;">
													<label>Dare:</label>
												</th>
												<th style="text-align: center;">
													<label>Avere:</label>
												</th>
												<th><br /></th>
											</tr>
											<tr><th colspan="8"><hr /></th></tr>

											<?php
												if (count($_SESSION['piano']['iva']) > 0)
                                                {
													foreach	($_SESSION['piano']['iva'] as $key => $field)
													{
														switch ($_SESSION['piano']['reg_iva'])
														{
															case "v":
															case "vi":
															case "c":
																// Scorporo iva
																$ImpTotale += $field['imponibile'];

																if (($field['imponibile'] > 0) && ($field['id_iva'] != "-"))
																{
																	$avere = ($field['imponibile']/$Iva[$field['id_iva']-1]['iva']);
																	$DiffTot += $avere;
																	//echo "Debug => avere $avere <br />";

																	$iva = ($field['imponibile']-$avere);
																	//echo "Debug => iva $iva <br />";
																}

																$div = array();
																$div2 = array();
																// controllo fra quali voci suddividere il dare:
																foreach ($_SESSION['piano']['dettaglio'] as $key_div => $field_div)
																{

																	//echo "Debug => da: |" . $field_div['da'] . "|<br />";
																	if ($field_div['da'] == "d")
																	{
																		array_push($div, $key_div);
																	}else{
																		array_push($div2, $key_div);
																	}

																}

																// parte da aggiungere al dare delle voci
																$dare_part = ($ImpTotale/count($div));
																if (count($div2) != 0)
																{
																	$avere_part = ($DiffTot/count($div2));
																}

																// reimposto i valori delle voci del dettaglio
																// Scorporo automatico avere
																foreach ($div as $key_div)
																{
																	$_SESSION['piano']['dettaglio'][$key_div]['importo_d'] = $dare_part;
																}

																// Scorporo automatico dare
																/*foreach ($div2 as $key_div)
																{
																	$_SESSION['piano']['dettaglio'][$key_div]['importo_a'] = $avere_part;
																}*/

																$Bilanciamento += $iva;
																break;

															default:
																//~ echo "Debug => imponibile ".$field['imponibile']." <br />";
																$iva = ($field['imponibile']*$Iva[$field['id_iva']-1]['iva'])-$field['imponibile'];
																//~ echo "Debug => iva $iva <br />";
																$Bilanciamento += $iva;
																//~ echo "Debug => bilanciamento: $Bilanciamento <br />";
																break;
														}

														$Bilanciamento = round($Bilanciamento, 2);
													}
												}

												foreach ($_SESSION['piano']['dettaglio'] as $key => $field)
												{
													if ($field['da'] == "d")
													{
														$Bilanciamento -= $field['importo_d'];
													}else{
														$Bilanciamento += $field['importo_a'];
													}

													$Bilanciamento = round($Bilanciamento, 2);
													$Bilanciamento = round($Bilanciamento, 2); ?>
												<tr>
													<td style="text-align: right;" title="Numero Voce">
														<label><?php echo ($key+1) . ")&nbsp;"; ?></label>
													</td>

													<td style="text-align: left;">
														<select name="da<?php echo $key; ?>"
															onchange="javascript: posta('1', '<?php echo "?act=piano_conti&amp;key=$key";
																if (isset($_GET['tomod'])) { echo "&amp;tomod=" . $_GET['tomod']; } ?>'); void(0);">
															<?php foreach ($da_array as $key_da => $field_da) { ?>
																<option value="<?php echo $key_da; ?>"
																	<?php if ($key_da == $field['da']) { echo "selected=\"selected\""; } ?> >
																	<?php echo $field_da; ?>
																</option>
															<?php } ?>
														</select>
													</td>

													<td>
														<a onclick="javascript: popUpWindow('SelezioneConto',
															'frm_ricerca_conti.php?subact=piano_conti&amp;key=<?php echo $key; ?>', '300', '250', '700', '350');"
																title="Seleziona Conto" >
															<?php if (($field['id_sottoconto'] == '')) {
																echo "Seleziona Conto";
															}else{
																$Conto = GetRows ("tab_sottoconti", "id_sottoconto = '"
																	. $field['id_sottoconto'] . "'", "", $db);
																echo $Conto[0]['descr_sottoconto'];
															} ?>
														</a>
														<?php if ($field['id_sottoconto'] != '') { ?>
															<a onclick="javascript: go_conf2('Eliminare il riferimento al conto?',
																'?act=piano_conti&amp;delconto=<?php echo $key;
																	if (isset($_GET['tomod'])) { echo "&amp;tomod=" . $_GET['tomod']; } ?>'); void(0);"
																title="Rimuovi riferimento al conto">
																<img src="img/cancel.png" alt="Rimuovi riferimento al conto" />
															</a>
														<?php } ?>
													</td>

													<?php if (($field['id_sottoconto'] != '')
														&& ($Conto[0]['req_anag'] == 0)) { ?>
														<td>
															<a onclick="javascript: popUpWindow('SelezionaAnagrafica',
																'frm_ricerca_avanzata.php?subact=piano_conti&amp;key=<?php echo $key; ?>', '300', '250', '870', '500');" >
																<?php if ($field['id_anagrafica'] == '') { ?>
																	Seleziona Anagrafica
																<?php }else{
																		$Anagrafica = GetRows ("tab_anagrafica", "id_anag = '"
																			. $field['id_anagrafica'] . "'", 'ragione_sociale', $db);
																	echo $Anagrafica[0]['ragione_sociale'];
																 } ?>
															</a>
															<?php if ($field['id_anagrafica'] != '') { ?>
																<a onclick="javascript: go_conf2('Eliminare il riferimento all\'anagrafica?',
																	'?act=piano_conti&amp;delanag=<?php echo $key;
																		if (isset($_GET['tomod'])) { echo "&amp;tomod=" . $_GET['tomod']; }?>'); void(0);"
																	title="Rimuovi riferimento all'anagrafica">
																	<img src="img/cancel.png" alt="Rimuovi riferimento anagrafica" />
																</a>
															<?php } ?>
														</td>
													<?php }else{ ?>
														<td>&lsaquo;Non richiesto&rsaquo;</td>
													<?php } ?>

													<td style="text-align: left;">
														<input name="desc<?php echo $key; ?>" type="text"
															onchange="javascript: posta('1', '<?php echo "?act=piano_conti&amp;key=$key";
																if (isset($_GET['tomod'])) { echo "&amp;tomod=" . $_GET['tomod']; } ?>'); void(0);"
															style="text-align: left; width: 10em;"
															value="<?php if (isset($field['desc'])) {
																echo htmlentities($field['desc']);
															} ?>" />
													</td>

													<td style="text-align: center;">
														<input name="importo_d<?php echo $key; ?>"
															onchange="javascript: posta('1', '<?php echo "?act=piano_conti&amp;key=$key";
																if (isset($_GET['tomod'])) { echo "&amp;tomod=" . $_GET['tomod']; }  ?>'); void(0);"
															type="text" style="width: 6em;" value="<?php printf("%.2f", $field['importo_d']); ?>"
															<?php if (($field['da'] == "a") || ($field['da'] != "d")) { echo "disabled=\"disabled\""; } ?> />
													</td>
													<td style="text-align: center;">
														<input name="importo_a<?php echo $key ?>"
															onchange="javascript: posta('1', '<?php echo "?act=piano_conti&amp;key=$key";
																if (isset($_GET['tomod'])) { echo "&amp;tomod=" . $_GET['tomod']; } ?>'); void(0);"
															type="text" style="width: 6em;" value="<?php printf("%.2f", $field['importo_a']); ?>"
															<?php if (($field['da'] == "d") || ($field['da'] != "a")) { echo "disabled=\"disabled\""; } ?> />
													</td>

													<td style="text-align: center;">
														<input name="ck_sel<?php echo $key; ?>" type="checkbox" value="<?php echo $key; ?>" />
														<a style="cursor: pointer;" onclick="javascript: go_conf2('Eliminare la voce dall\'operazione?',
															'?act=piano_conti&amp;del=<?php echo $key;
																if (isset($_GET['tomod'])) { echo "&amp;tomod=" . $_GET['tomod']; } ?>'); void(0);" title="Rimuovi voce">
															<img src="img/del.png" alt="Elimina" />
														</a>
													</td>
												</tr>
											<?php } ?>
										</table>
									</form>
								<?php } ?>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</div>

	<br />

	<?php if (isset($_SESSION['piano']['reg_iva']) && ($_SESSION['piano']['reg_iva'] != "-")) { ?>
		<div id="dett_iva" class="expander">
			<form method="post" action="<?php echo $_SERVER['REQUEST_URI']; ?>">
				<table class="form" style="width: 100%;">
					<tr>
                        <th style="width: 20px; text-align: center;">
							<img id="img_iva"  alt="img_iva" src="img/bott.png"
								onclick="Javascript: Expand('dett_iva', 'img_iva', '1.7em',
										'img/bott.png', 'img/top.png'); void(0);"
									title="Click per Visualizzare/Nascondere il dettaglio" />
                        </th>
                        <th>
							Dettaglio Iva:

							&nbsp;
							<a onclick="Javascript: go('?act=piano_conti&amp;add_iva=1<?php
									if (isset($_GET['tomod'])) { echo "&amp;tomod=" . $_GET['tomod']; } ?>'); void(0);">
								<img src="img/add.png" alt="+" title="Aggiungi dettaglio Iva" />
								Dettaglio Iva
							</a>

							&nbsp;
							<a onclick="javascript: go_conf2('Rimuovere tutte le voci dal dettaglio iva?',
								'<?php echo "?act=piano_conti&amp;svuota_iva=0";
									if (isset($_GET['tomod'])) { echo "&amp;tomod=" . $_GET['tomod']; }?>'); void(0);">
								<img src="img/clear.png" alt="clear" />
								Voci (<label class="err" title="Totale voci operazione">
												<?php echo count($_SESSION['piano']['iva']); ?>
											</label>)
							</a>
						</th>
					</tr>

					<tr>
						<td colspan="2">
							<table class="dettaglio" style="width: 100%;">
								<?php if ((count($_SESSION['piano']['iva']) != 0)) { ?>
									<tr>
										<td><br /></td>
										<td style="text-align: left;"><label>%Iva:</label></td>
										<td style="text-align: left;"><label>Importo:</label></td>
										<td style="text-align: center;"><label>Iva:</label></td>
										<td style="text-align: center;"><label>Netto:</label></td>
										<td><br /></td>
									</tr>
									<tr><th colspan="8"><hr /></th></tr>

									<?php foreach ($_SESSION['piano']['iva'] as $key => $field) { ?>
										<tr>
											<td style="text-align: right;" title="Numero Voce">
												<label><?php echo ($key+1) . ")&nbsp;"; ?></label>
											</td>

											<td style="text-align: left;">
												<select name="id_iva<?php echo $key; ?>"
													onchange="javascript: posta('2', '<?php echo "?act=piano_conti&amp;key_iva=$key";
														if (isset($_GET['tomod'])) { echo "&amp;tomod=" . $_GET['tomod']; } ?>'); void(0);">
													<option value="-">-</option>
													<?php foreach ($Iva as $key_iva => $field_iva) { ?>
														<option value="<?php echo $field_iva['id_iva']; ?>"
															<?php if ($field_iva['id_iva'] == $field['id_iva'])
																{ echo "selected=\"selected\""; } ?> >
															<?php echo $field_iva['desc_iva']; ?>
														</option>
													<?php } ?>
												</select>
											</td>

											<td>
												<input name="imponibile<?php echo $key; ?>"
													onchange="javascript: posta('2', '<?php echo "?act=piano_conti&amp;key_iva=$key";
														if (isset($_GET['tomod'])) { echo "&amp;tomod=" . $_GET['tomod']; } ?>'); void(0);"
													type="text" style="width: 10em; text-align: right;"
													value="<?php printf("%.2f", $field['imponibile']); ?>" />
											</td>

											<td style="font-weight: normal; text-align: right;">
												<?php
													switch ($_SESSION['piano']['reg_iva'])
													{
														case "v":
														case "vi":
														case "c":
															// Scorporo iva
															if (($field['imponibile'] > 0) && ($field['id_iva'] != "-"))
															{
																$avere = ($field['imponibile']/$Iva[$field['id_iva']-1]['iva']);
																$DiffTot += $avere;
																//~ echo "Debug => avere $avere <br />";

																$iva = ($field['imponibile']-$avere);
																//echo "Debug => iva $iva <br />";
															}
															break;

														default:
															$iva = ($field['imponibile']*$Iva[$field['id_iva']-1]['iva'])-$field['imponibile'];
															break;
													}
													$Bilanciamento = round($Bilanciamento, 2);
													printf("%.2f", ($iva)); ?>
											</td>

											<td style="font-weight: normal; text-align: right;">
												<?php printf("%.2f", $avere); ?>
											</td>

											<td style="text-align: center;">
												<a style="cursor: pointer;" onclick="javascript: go_conf2('Eliminare la voce dall\'operazione?',
													'?act=piano_conti&amp;del_iva=<?php echo $key;
													if (isset($_GET['tomod'])) { echo "&amp;tomod=" . $_GET['tomod']; } ?>'); void(0);" title="Rimuovi voce">
													<img src="img/del.png" alt="Elimina" />
												</a>
											</td>
										</tr>
									<?php } ?>
								</table>
							</td>
						</tr>
					<?php } ?>
				</table>
			</form>
		</div>
	<?php } ?>

	<div id="status-bar">
		<?php $Bilanciamento = round($Bilanciamento, 2);
			if (($Bilanciamento != "0.00") || ($Bilanciamento != "0"))
			{
				$Bilanciamento = round($Bilanciamento, 2);
				//~ $Bilanciamento = round($Bilanciamento, 2);
				$Bilanciamento = "<label class=\"err\">$Bilanciamento</label>";
			}else{
				$Bilanciamento = "<label class=\"ok\">0.00</label>";
			} ?>
		<label>Bilanciamento: </label><?php echo $Bilanciamento; ?>,
		<label>Totale: </label><?php ; ?>
	</div>

<?php
/*
	echo "<pre>session";
	print_r($_SESSION);
	echo "</pre>";
*/
?>


<script type="text/javascript">
	Expand('dett_contabile', 'img_cont', '1.7em', 'img/bott.png', 'img/top.png');
	Expand('dett_iva', 'img_iva', '1.7em', 'img/bott.png', 'img/top.png');
</script>

</div>

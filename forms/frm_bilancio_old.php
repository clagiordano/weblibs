<?php

if (isset($_GET['tipo']) && $_GET['tipo'] != "")
{
	switch ($_GET['tipo'])
	{
		case "sp":
			$Titolo = "Stato Patrimoniale";
			$SpDare = array(1, 2, 3, 9, 7, 6, 4, 5, 10, 8);
			$SpAvere = array(14, 16, 20, 17, 18, 15, 42, 43, 19);
			break;

		case "ce":
			$Titolo = "Conto Economico";
			$SpDare = array(27, 25, 24, 22, 23, 29, 31, 28, 30, 32, 26, 33, 34, 35);
			$SpAvere = array(37, 38, 36, 39, 40);
			break;
	}
}else{
	$Titolo = "Stato Patrimoniale";
	$SpDare = array(1, 2, 3, 9, 7, 6, 4, 5, 10, 8);
	$SpAvere = array(14, 16, 20, 17, 18, 15, 42, 43, 19);
}
$Sottoconti = array();

//~ border-left: 1px solid black; border-right: 1px solid black;
?>

<div id="search-form">
	<form method="post" action="<?php echo $_SERVER['REQUEST_URI']; ?>">
		<table class="form" style="width: 100%;">
			<tr>
				<th>Seleziona il tipo di bilancio da stampare:</th>
			</tr>
			<tr>
				<td>
					<a href="home.php?act=bilancio&amp;tipo=sp">
						<img src="img/refresh.png" alt="refresh"
							style="border: 0px; width: 14px; height: 14px; vertical-align: middle;" />
						Stato Patrimoniale
					</a>

					&nbsp;
					<a href="home.php?act=bilancio&amp;tipo=ce">
						<img src="img/refresh.png" alt="refresh"
							style="border: 0px; width: 14px; height: 14px; vertical-align: middle;" />
						Conto Economico
					</a>
				</td>
			</tr>
		</table>
	</form>
</div>

<div id="search-result-2r">
	<table style="width: 100%;" class="form">
		<tr>
			<th colspan="4" style="text-align: center;">
				<?php echo $Titolo; ?>
				<a onclick="javascript: popUpWindow('BilancioPdf', 'frm_pdf_bilancio.php', '300', '300', '800', '600');"
					title="Esporta in formato pdf">
					<img src="img/pdf.png" alt="pdf"
						style="border: 0px; width: 18px; height: 18px; vertical-align: middle;" />
				</a>
			</th>
		</tr>
		<tr><th colspan="3"><br /></th></tr>

		<tr>
			<td style="vertical-align: top;">
				<!-- Tabella Dare !-->
				<table style="width: 100%;" class="dettaglio">
					<tr>
						<th colspan="2" style="text-align: center; border-bottom: 1px solid black;">
							<label>Attivo (Dare):</label>
						</th>
						<th style="text-align: center; border-bottom: 1px solid black;">
							<label>Importo:</label>
						</th>
					</tr>
					<tr><th colspan="3"><br /></th></tr>
					<?php foreach ($SpDare as $id) {
						$TotConto = 0;
						$Sottoconti = GetRows ("tab_conti, tab_sottoconti",
							"tab_conti.id_conto = '$id' AND tab_conti.id_conto = tab_sottoconti.id_conto",
							"", $db);
						foreach ($Sottoconti as $key => $field)
						{
							if ($key == 0)
							{ ?>
								<tr>
									<th>
										<label>
											<?php echo "(". $field['codice_conto'] . ")" ?>
										</label>
									</th>
									<th colspan="2">
										<label>
											<?php echo htmlentities(ucfirst(strtolower($field['descr_conto']))); ?>
										</label>
									</th>
								</tr>
								<tr>
									<td>
										<?php echo "(". $field['codice_sottoconto'] . ")" ?>
									</td>
									<td>
										<?php echo htmlentities(ucfirst(strtolower($field['descr_sottoconto']))); ?>
									</td>
									<td style="text-align: right;">
										<label class="err">
											<?php
												$Campi = GetRows ("tab_dett_piano_conti",
													"id_sottoconto = '" . $field['id_sottoconto'] . "'",
													"", $db, 1, "sum(importo_d) as tot_dare");

												if ($Campi[0]['tot_dare'] != "")
												{
													echo $Campi[0]['tot_dare'];
													$TotConto += $Campi[0]['tot_dare'];
													$TotDare += $Campi[0]['tot_dare'];
												}else{
													echo "0.00";
												}
											?>
										</label>
									</td>
								</tr>
							<?php }else{ ?>
								<tr>
									<td>
										<?php echo "(". $field['codice_sottoconto'] . ")" ?>
									</td>
									<td>
										<?php echo htmlentities(ucfirst(strtolower($field['descr_sottoconto']))); ?>
									</td>
									<td style="text-align: right;">
										<label class="err">
											<?php
												$Campi = GetRows ("tab_dett_piano_conti",
													"id_sottoconto = '" . $field['id_sottoconto'] . "'",
													"", $db, 1, "sum(importo_d) as tot_dare");

												if ($Campi[0]['tot_dare'] != "")
												{
													echo $Campi[0]['tot_dare'];
													$TotConto += $Campi[0]['tot_dare'];
													$TotDare += $Campi[0]['tot_dare'];
												}else{
													echo "0.00";
												}
											?>
										</label>
									</td>
								</tr>
							<?php }
							} ?>
							<tr>
								<th colspan="2" style="text-align: right; border-top: 1px solid black;">
									<label>Totale Conto:</label>
								</th>
								<th style="text-align: right; border-top: 1px solid black;">
									<label>
										<?php printf("%.2f", $TotConto); ?>
									</label>
								</th>
							</tr>
							<tr><th colspan="3"><br /></th></tr>
						<?php } ?>
				</table>
			</td>

			<td style="border-left: 1px solid black; width: 0em;"></td>

			<td style="vertical-align: top;">
				<!-- Tabella Avere !-->
				<table style="width: 100%;" class="dettaglio">
					<tr>
						<th colspan="2" style="text-align: center; border-bottom: 1px solid black;">
							<label>Passivo (Avere):</label>
						</th>
						<th style="text-align: center; border-bottom: 1px solid black;">
							<label>Importo:</label>
						</th>
					</tr>
					<tr><th colspan="3"><br /></th></tr>
					<?php foreach ($SpAvere as $id) {
						$TotConto = 0;
						$Sottoconti = GetRows ("tab_conti, tab_sottoconti",
							"tab_conti.id_conto = '$id' AND tab_conti.id_conto = tab_sottoconti.id_conto",
							"", $db);

						foreach ($Sottoconti as $key => $field)
						{
							if ($key == 0)
							{ ?>
								<tr>
									<th>
										<label>
											<?php echo "(". $field['codice_conto'] . ")" ?>
										</label>
									</th>
									<th colspan="2">
										<label>
											<?php echo htmlentities(ucfirst(strtolower($field['descr_conto']))); ?>
										</label>
									</th>
								</tr>
								<tr>
									<td>
										<?php echo "(". $field['codice_sottoconto'] . ")" ?>
									</td>
									<td>
										<?php echo htmlentities(ucfirst(strtolower($field['descr_sottoconto']))); ?>
									</td>
									<td style="text-align: right;">
										<label class="ok">
											<?php
												$Campi = GetRows ("tab_dett_piano_conti",
													"id_sottoconto = '" . $field['id_sottoconto'] . "'",
													"", $db, 1, "sum(importo_a) as tot_avere");

												if ($Campi[0]['tot_avere'] != "") {
													echo $Campi[0]['tot_avere'];
													$TotConto += $Campi[0]['tot_avere'];
													$TotAvere += $Campi[0]['tot_avere'];
												}else{
													echo "0.00";
												}
											?>
										</label>
									</td>
								</tr>
							<?php }else{ ?>
								<tr>
									<td>
										<?php echo "(". $field['codice_sottoconto'] . ")" ?>
									</td>
									<td>
										<?php echo htmlentities(ucfirst(strtolower($field['descr_sottoconto']))); ?>
									</td>
									<td style="text-align: right;">
										<label class="ok">
											<?php
												$Campi = GetRows ("tab_dett_piano_conti",
													"id_sottoconto = '" . $field['id_sottoconto'] . "'",
													"", $db, 1, "sum(importo_a) as tot_avere");

												if ($Campi[0]['tot_avere'] != "") {
													echo $Campi[0]['tot_avere'];
													$TotConto += $Campi[0]['tot_avere'];
													$TotAvere += $Campi[0]['tot_avere'];
												}else{
													echo "0.00";
												}
											?>
										</label>
									</td>
								</tr>
							<?php }
							} ?>
							<tr>
								<th colspan="2" style="text-align: right; border-top: 1px solid black;">
									<label>Totale Conto:</label>
								</th>
								<th style="text-align: right; border-top: 1px solid black;">
									<label>
										<?php printf("%.2f", $TotConto); ?>
									</label>
								</th>
							</tr>
							<tr><th colspan="3"><br /></th></tr>
						<?php } ?>
				</table>
			</td>
			<td style="border-left: 1px solid black; width: 0em;"></td>
		</tr>

		<tr>
			<td style="text-align: left;" colspan="4">
				<label>Totale Dare:</label>
				<label class="err"><?php printf("%.2f", $TotDare); ?></label>

				&nbsp;
				<label>Totale Avere:</label>
				<label class="ok"><?php printf("%.2f", $TotAvere); ?></label>

				&nbsp;
				<label>Saldo:</label>
				<?php
					$Saldo = ($TotAvere-$TotDare);
					if ($Saldo < 0) { ?>
						<label class="err">
							<?php printf("%.2f", $Saldo); ?>
						</label>
					<?php }else{ ?>
						<label class="ok">
							<?php printf("%.2f", $Saldo); ?>
						</label>
					<?php } ?>

			</td>
		</tr>
	</table>
</div>

<?php

	/*
	 * 24/08/2009 20:00:21 CEST Claudio Giordano
	 *
	 * aggiungere totale dare/avere  in fondo alla pagina
	 */

?>

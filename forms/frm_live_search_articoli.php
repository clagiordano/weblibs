<?php
	//~ echo "<pre> GET ";
		//~ print_r($_GET);
	//~ echo "</pre>";

	//~ echo "[Debug]: BaseURL: $BaseURL <br />";

	$BaseURL = "?act=documenti";
	if (isset($_GET['tomod']) && is_numeric($_GET['tomod']))
	{
		$BaseURL .= "&amp;tomod=" . $_GET['tomod'];
	}

	$TipiRicerca = Array('Codice ( Interno / Fornitore )' => 'cod_int,cod_forn',
                                         'Codice Barcode' => 'barc_forn',
                                   'Descrizione prodotto' => 'marca,modello',
                                              'Fornitore' => 'ragione_sociale',
                                                   'Note' => 'note'); ?>
	<table style="width: 100%;">
		<tr>
			<td style="text-align: right;">
				<a onclick="Javascript: document.getElementById('lsearch').style.display = 'none';">
					[<label class="err" style="cursor: pointer;">Chiudi</label>]
				</a>
			</td>
		</tr>
	</table>
	
<?php
	if (isset($_GET['search']) && ($_GET['search'] != ""))
	{
		if ($_SESSION['documento']['tipo_doc'] == 5)
		{
			$Prezzo = "prz_ve";
		}else{
			$Prezzo = "prz_acq";
		}

		foreach ($TipiRicerca as $key => $field)
		{
			$Where = "";
			if (preg_match("(.*,.*)", $field))
			{
				$temp_array = explode(",", $field);
				foreach ($temp_array as $tkey => $tfield)
				{
					if ($tkey == 0)
					{
						$Where .= "$tfield LIKE '%" . $_GET['search'] . "%' ";
					}else{
						$Where .= " OR $tfield LIKE '%" . $_GET['search'] . "%' ";
					}
				}
			}else{
				$Where .= "$field LIKE '%" . $_GET['search'] . "%' ";
			}
			$Risultati = $d->GetRows("*", "view_prodotti", $Where, "", $field . " limit 10", 1);

			if (count($Risultati) > 0) { ?>
				<table style="width: 100%;" class="dettaglio">
					<tr>
						<th colspan="2">
							Risultati corrsipondenti per '<?php echo $key; ?>'
							( <label class="ok"><?php echo count($Risultati); ?></label> ):
						</th>
					</tr>

					<tr><th colspan="2"><hr /></th></tr>

					<?php foreach ($Risultati as $key => $field) { ?>
						<tr onclick="Javascript: conferma_salva('0', 'Confermi la selezione?',
							'<?php echo $BaseURL; ?>&amp;selected=<?php echo $field['id_prodotto']; ?>');" >
							<td>
								<?php echo "( " . $field['cod_int'] . ") "
									. $field['marca'] . " " . $field['modello']; ?>
							</td>
							<td style="text-align: right;">
								<?php echo $field[$Prezzo]; ?>
							</td>
						</tr>
					<?php } ?>
					
					
				</table>
			<?php } ?>
			
		<?php } ?>
		<?php if (count($Risultati) == 1) { ?>
				<script type="text/javascript" id="runscript">
					//~ alert("prova");
				</script>
			<?php } ?>
	<?php }
?>

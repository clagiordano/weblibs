<?php
/*
 * frm_admin_menu.php
 *
 * Copyright 2008 Claudio Giordano <claudio.giordano@autistici.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 */

	if (!isset($_GET['tomod'])) { $_GET['tomod'] = ""; }
/*
	if (isset($_GET['del']))
	{
		$Status = $d->DeleteRows ("Tabella: tab_submenu_pb", "id_menu = '" . $_GET['del'] . "'");
	}
*/
	if (isset($_GET['salva']))
	{
		$errori = "";
		$Esclusioni= array("add_menu" => "Salva");

		if (trim($_POST['titolo_it']) == "")
		{
			$errori .= "Errore, non è stata impostata una voce per il menu.<br />";
		}
		if (trim($_POST['tip_it']) == "")
		{
			$errori .= "Errore, non è stata impostato il link alla pagina.<br />";
		}
		if (trim($_POST['gruppo']) == "")
		{
			$errori .= "Errore, non è stata impostato un gruppo per il menu.<br />";
		}
		if (trim($_POST['titolo_page_it']) == "")
		{
			$errori .= "Errore, non sono stati impostati i permessi della pagina.<br />";
		}
		if (trim($_POST['act']) == "")
		{
			$errori .= "Errore, non è stata impostata l'azione per il link.<br />";
		}

		if (($_GET['salva'] == "salva") && ($errori == ""))
		{
			$Campi = $d->SaveRow ($_POST, $Esclusioni, "tab_submenu_pb");
		}

		if (($_GET['salva'] == "modifica") && ($errori == ""))
		{
			$Campi = $d->UpdateRow ($_POST, $Esclusioni, "tab_submenu_pb", "id_submenu = '"
				.$_GET['tomod'] . "'");
		}

		if ($errori != "")
		{
			?><label class="err"><?php echo $errori; ?></label><?php
			$menu_mod[0]=$_POST;
		}else{ ?>
			<script type="text/javascript">
				document.location.href="?gruppo=admin&act=adm_menu_pb"
			</script>
		<?php }
	}

if(isset($_GET['gruppo']))
{
	$_SESSION['gruppo'] = $_GET['gruppo'];
	$_SESSION['id_menu'] = $_GET['id_menu'];
}else{
	$_GET['gruppo'] = $_SESSION['gruppo'];
	$_GET['id_menu'] = $_SESSION['id_menu'];
}
	if (is_numeric($_GET['tomod']))
	{
		$menu_mod = $d->GetRows ("*", "tab_submenu_pb", "id_submenu = '".$_GET['tomod']."'");
	}

	if (isset($_GET['id_menu']))
	{

		$menurow 		= $d->GetRows ("*", "tab_menu_pb1", "id_menu='".$_GET['id_menu']."'", "", "", 1);
	}

	$Menu = $d->GetRows ("*", "tab_submenu_pb", "gruppo = '".$_GET['gruppo']."'", "",
		"ordine");
print_r($_GET);
	?>
						<a onclick="Javascript: ClosePopup(); void(0);">
							[ <label class="err" style="cursor: pointer;">Chiudi</label> ]
						</a>

<section id="search-form">
		<form method="post" action="#">
			<table style="width: 100%;" class="form">

				<?php 
					if(!isset($_GET['act'])){ $_GET['act']='';}
					$d->ShowEditBar("Voce Sotto Menu:", $_GET['tomod'],
					"new,save", $_GET['act']);
					if(isset($_GET['reset'])) {
							$menu_mod[0]['gruppo'] = $_GET['gruppo'];
							$menu_mod[0]['ordine'] = $d->GetMax('tab_submenu_pb','ordine',"gruppo = '".$_GET['gruppo']."'");
						}
					 ?>

				<tr>
					<td style="text-align: left;">
						<input type="text" name="titolo_it" maxlength="30" size="20"
							placeholder="Titolo It"
							value="<?php if (isset($menu_mod[0]['titolo_it']))
							{ echo $menu_mod[0]['titolo_it']; } ?>" />

						&nbsp;
						<input type="text" name="titolo_en" maxlength="30" size="20"
							placeholder="Titolo en"
							value="<?php if (isset($menu_mod[0]['titolo_en']))
							{ echo $menu_mod[0]['titolo_en']; } ?>" />

						&nbsp;
						<input type="text" name="titolo_page_it" maxlength="50"
							size="40" placeholder="Titolo Pagina"
							value="<?php if (isset($menu_mod[0]['titolo_page_it']))
							{ echo $menu_mod[0]['titolo_page_it']; } ?>" />

						&nbsp;
						<input type="text" name="titolo_page_en" maxlength="50"
							placeholder="Titolo Pagina En" 
							value="<?php if (isset($menu_mod[0]['titolo_page_en']))
							{ echo $menu_mod[0]['titolo_page_en']; } ?>" />

					</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>
						<input type="text" name="gruppo" size="10" placeholder="Gruppo"
							value="<?php if (isset($menu_mod[0]['gruppo']))
							{ echo $menu_mod[0]['gruppo']; } ?>" />

						&nbsp;
						<input type="text" name="tip_it" maxlength="50" size="15"
							placeholder="Suggerimento"
							value="<?php if (isset($menu_mod[0]['tip_it']))
							{ echo $menu_mod[0]['tip_it']; } ?>" />

						&nbsp;
						<input type="text" name="tip_en" maxlength="50" size="15"
							placeholder="Suggerimento en"
							value="<?php if (isset($menu_mod[0]['tip_en']))
							{ echo $menu_mod[0]['tip_en']; } ?>" />

						&nbsp;
						<input name="act" type="text" maxlength="30" size="10"
							placeholder="Azione"
							value="<?php if (isset($menu_mod[0]['act']))
							{ echo $menu_mod[0]['act']; } ?>" />

						&nbsp;
						<input name="ordine" type="text" maxlength="4" size="5"
							placeholder="Ordine"
							value="<?php if (isset($menu_mod[0]['ordine']))
							{ echo $menu_mod[0]['ordine']; } ?>" />

						&nbsp;
						<input name="page" type="text" maxlength="30"
							size="40" placeholder="Percorso pagina"
							value="<?php if (isset($menu_mod[0]['page']))
							{ echo $menu_mod[0]['page']; } ?>" />
					</td>
				</tr>
				<tr>
					<td colspan="4" style="text-align: right;">
						<?php if (isset($st_update)) { echo $st_update; } ?>
					</td>
				</tr>
			</table>
		</form>
	</section>

	<?php if (count($Menu) != 0) { ?>
		<div id="search-result-6r">
			<table style="width: 100%;" class="dettaglio">
				<tr>
					<th width="150" align="right">Voce men&ugrave; principale:</th>
					<td><?php echo $menurow[0]['titolo_it'];?></td>
					<th width="150" align="right">Gruppo:</th>
					<td><?php echo $menurow[0]['gruppo'];?></td>
					<td><?php echo $menurow[0]['act'];?></td>
				</tr>
			</table>
			<form method="post" action="<?php echo $_SERVER['REQUEST_URI']; ?>">
				<table style="width: 100%;" class="dettaglio">
					<tr>
						<th><label>Titolo :</label></th>
						<th><label>Suggerimento:</label></th>
						<th><label>Titolo pagina:</label></th>
						<th><label>Gruppo:</label></th>
						<th><label>Action:</label></th>
						<th><label>Ordine:</label></th>
						<th colspan="2"></th>
					</tr>
					<tr><th colspan="8"><hr /></th></tr>
					<?php foreach ($Menu as $key => $fieldm) {
								$style = " class=\"ok\"";
					 ?>
						<tr>
							<td >
								<a onclick="Javascript: go('?gruppo=<?php echo $_GET['gruppo'];?>&id_menu=<?php echo $_GET['id_menu']; ?>&act=adm_menu_pb&amp;tomod=<?php
									echo $fieldm['id_submenu']; ?>'); void(0);"
									title="Modifica Voce" <?php echo $style; ?>>
									<?php echo $fieldm['titolo_it']; ?>
								</a>
							</td>
							<td><?php echo $fieldm['tip_it']; ?></td>
							<td>
								<?php echo $fieldm['titolo_page_it'];  ?>
							</td>
							<td><?php echo $fieldm['gruppo']; ?></td>
							<td><?php echo $fieldm['act']; ?></td>
							<td><?php echo $fieldm['ordine']; ?></td>

							<td style="text-align: left;">
                <a onclick="Javascript: go('?gruppo=<?php echo $fieldm['gruppo']; ?>&act=edit_page&amp;tomod=<?php
									echo $fieldm['id_menu']; ?>')" title="Vai sottomenu" >
									<img src="../Images/Links/edit.png" alt="Edita Pagina" title="Edita Pagina"
										style="border: 0px; width: 14px; height: 14px; vertical-align: middle;" />
								</a>
								&nbsp;
								<a onclick="javascript: go_conf2('Eliminare la voce del menù?',
									'?gruppo=adminact=adm_menu_pb&amp;del=<?php echo $fieldm['id_menu']; ?>'); void(0);" title="Elimina" >
									<img src="/Images/Links/del.png" alt="Elimina" title="Elimina"
										style="border: 0px; width: 14px; height: 14px; vertical-align: middle;" />
								</a>
							</td>
						</tr>
					<?php } ?>
					<tr><th colspan="8"><hr /></th></tr>
				</table>
			</form>
		</div>
	<?php }else{?>
	Non sono presenti voci nel sottomenu!!!
	<?php } ?>



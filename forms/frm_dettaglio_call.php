<?php
	$root = substr($_SERVER['DOCUMENT_ROOT'], 0, -1);
	require_once("$root/Function/Db.php");
	require_once("$root/Function/Debug.php");
	require_once("$root/Function/DataTime.php");
	require_once("$root/Function/Strings.php");

	$Dettaglio = GetRows ("view_call", "id_chiamata = '" . $_GET['tovis'] . "'", "", $db);
?>

<table style="width: 100%;" class="form">
	<tr>
		<th colspan="8" style="text-align: right;">
			<a onclick="Javscript: document.getElementById('<?php echo $_GET['target']; ?>').style.display = 'none';">
				<img src="img/exit.png" alt="chiudi" />
				Chiudi Dettaglio
			</a>
		</th>
	</tr>

	<tr><th colspan="2"><br /></th></tr>

	<tr><th colspan="2">Dettaglio Attivit&agrave;:</th></tr>
	<tr><th colspan="2"><hr /></th></tr>

	<tr>
		<td style="text-align: right;">N.Call: </td>
		<td>
			<label><?php echo $Dettaglio[0]['n_call']; ?></label>
			&nbsp;
			Data Attivit&agrave;: <label><?php echo Inverti_Data($Dettaglio[0]['data_att']); ?></label>

			&nbsp;
			Tecnico: <label><?php echo htmlentities($Dettaglio[0]['tecnico']); ?></label>
		</td>
	</tr>

	<tr>
		<td style="text-align: right;">Citt&agrave;: </td>
		<td>
			<label><?php echo $Dettaglio[0]['Comune'] . " (" . $Dettaglio[0]['Targa'] . ")"; ?></label>
			&nbsp;
			Denominazione: <label><?php echo $Dettaglio[0]['den']; ?></label>
		</td>
	</tr>

	<tr>
		<td style="text-align: right;">Indirizzo: </td>
		<td>
			<label><?php echo htmlentities($Dettaglio[0]['ind']); ?></label>
		</td>
	</tr>

	<tr>
		<td style="text-align: right;">Cliente: </td>
		<td>
			<label><?php echo htmlentities($Dettaglio[0]['cliente']); ?></label>
			&nbsp;
			Tipo Call: <label><?php echo htmlentities($Dettaglio[0]['tipo']); ?></label>

			&nbsp;
			Stato: <label><?php echo $Dettaglio[0]['stato_call']; ?></label>
		</td>
	</tr>

	<tr>
		<td style="text-align: right;">Referente: </td>
		<td>
			<label><?php echo $Dettaglio[0]['ref']; ?></label>
			&nbsp;
			Telefono: <label><?php echo $Dettaglio[0]['tel']; ?></label>

			&nbsp;
			N.Pdl: <label><?php echo $Dettaglio[0]['n_pdl']; ?></label>

			&nbsp;
			Compenso: <label><?php echo $Dettaglio[0]['econ']; ?></label>
		</td>
	</tr>

	<tr>
		<td style="text-align: right; vertical-align: top;">Note: </td>
		<td>
			<label><?php echo $Dettaglio[0]['note_call']; ?></label>
		</td>
	</tr>
</table>

<div id="search-result">
<?php
	require_once("$root/Function/libchart/classes/libchart.php");
	require_once("$root/Function/Db.php");
	require_once("$root/Function/Debug.php");
	require_once("$root/Function/DataTime.php");

	$Stati =  GetRows ("tab_stato_mch", "", "stato_mch", $db, 1);
/*
	echo "<pre>";
		print_r($Stati);
	echo "</pre>";
*/

	//Padding::Padding  ($top, $right, $bottom, $left)

	define("ChartPath", "tmp/chart.png");
	define("ChartWidth", 1000);
	define("ChartHeight", 400);

	$chart = new PieChart(ChartWidth, ChartHeight);
	$chart->getPlot()->setGraphPadding(new Padding("5", "5", "5", "5"));
	$chart->setTitle("Statistiche Magazzino RollOut");
	$dataSet = new XYDataSet();


	foreach ($Stati as $key => $field)
	{
		$stat = GetRows ("tab_macchine", "id_stato = '"
			. $field['id_stato_call'] . "'", "", $db, 1, "count(id_macchina) as count");
/*
		echo "[Debug]: count stato: ".$stat[0]['count']." <br />";
*/

		$dataSet->addPoint(new Point($field['stato_call']
			. " ( " . $stat[0]['count'] . " )", $stat[0]['count']));

	}

	$chart->setDataSet($dataSet);
	$chart->render(ChartPath);


?>


	<img src="<?php echo ChartPath; ?>" alt="chart" />
</div>


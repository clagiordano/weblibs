<?php

	ob_end_clean();
	require ('fpdf/fpdf.php');


$Oggetto = GetRows ("tab_impostazioni", "opt = 'off_oggetto'", "", $db);
$Oggetto = $Oggetto[0]['val'];
$Off_testo = GetRows ("tab_impostazioni", "opt = 'off_testo'", "", $db);
$Off_testo = $Off_testo[0]['val'];
$Off_note = GetRows ("tab_impostazioni", "opt = 'off_note'", "", $db);
$Off_note = explode('\\\n', $Off_note[0]['val']);


if (isset($_GET['id_off']))
{
	unset($_SESSION['macchina']);
	unset($_SESSION['accessori']);
	unset($_SESSION['assemblaggio']);
	unset($_SESSION['offerta_def']);
	unset($_SESSION['offerta']['id_anagrafica']);
	unset($_SESSION['img-case']);
	unset($_SESSION['img-monit']);
	unset($_SESSION['img-stp']);
	unset($_SESSION['debug']);

	$_SESSION['macchina'] = array();
	$_SESSION['accessori'] = array();

	$opt_off = $d->GetRows ("*", "tab_offerte",
		"id_off = '" . $_GET['id_off'] . "'");
	//~ echo "Debug => leggo i parametri dell'offerta <br />";
	$_SESSION['offerta']['assemblaggio'] 		= $opt_off[0]['asse'];
	$_SESSION['offerta']['offerta_def'] 		= $opt_off[0]['tot'];
	$_SESSION['offerta']['id_anagrafica'] 	= $opt_off[0]['id_cliente'];
	$_SESSION['offerta']['img-stp'] 				= $opt_off[0]['img1'];
	$_SESSION['offerta']['img-case'] 				= $opt_off[0]['img2'];
	$_SESSION['offerta']['img-monit'] 			= $opt_off[0]['img3'];

	$componenti = $d->GetRows ("*", "tab_dett_offerte",
		"id_offerta = '" . $_GET['id_off'] . "' AND comp != 'acc'");

	foreach ($componenti as $key => $field)
	{
		$dett_articolo = GetRows ("tab_prodotti", "cod_forn = '"
			. $field['cod_forn'] . "'", "descr", $db);

		$_SESSION['macchina'][$field['comp']]['id'] = $field['id_articolo'];
		$_SESSION['macchina'][$field['comp']]['desc'] = $dett_articolo[0]['descr'];
		$_SESSION['macchina'][$field['comp']]['prz'] = $field['prz'];
		$_SESSION['macchina'][$field['comp']]['disp'] = $dett_articolo[0]['disp'];
		$_SESSION['macchina'][$field['comp']]['cod'] = $dett_articolo[0]['cod_forn'];
		$_SESSION['macchina'][$field['comp']]['qta'] = $field['qta'];
	}

	$accessori = GetRows ("tab_dett_offerte", "id_offerta = '"
		. $_GET['id_off'] . "' AND comp = 'acc'", "", $db);
	foreach ($accessori as $key => $field)
	{
		$dett_accessorio = GetRows ("tab_prodotti", "cod_forn = '"
			. $field['cod_forn'] . "'", "descr", $db);

		array_push($_SESSION['accessori'], array("id" => $field['id_articolo'],
												"desc" => $dett_accessorio[0]['descr'],
												"prz" => $field['prz'],
												"disp" => $dett_accessorio[0]['disp'],
												"cod" => $dett_accessorio[0]['cod_forn'],
												"qta" => $field['qta']));
	}
}

	  $bullet = chr(149);
	  $ImgStp = $_SESSION['img-stp'];
     $ImgCase = $_SESSION['img-case'];
  $ImgMonitor = $_SESSION['img-monit'];
	  $LogoRM = "img/RM.jpg";
$Assemblaggio = (35*1.2);

if (isset($_SESSION['macchina']))
{
	foreach ($_SESSION['macchina'] as $mch)
	{
		$TotOfferta += $mch['prz'];
	}
	$TotOfferta = ($TotOfferta*1.3)*1.2;
}

if (isset($_SESSION['accessori']))
{
	foreach ($_SESSION['accessori'] as $acc)
	{
		$TotOfferta += (($acc['prz']*1.3)*1.2);
	}
}

if (isset($_SESSION['assemblaggio']) && ($_SESSION['assemblaggio'] == 0))
{
	$TotOfferta += $Assemblaggio;
}

class PDF extends FPDF {
	//Page footer
	function Footer() {
		//Position at 1.5 cm from bottom
		$this->SetY(-15);
		$this->SetFont('Times','I',10);
		$this->Cell(40, 4, 'Palermo: ' . date('d/m/Y'), '', '', 'L');
		$this->Cell('', 4, "Giorgio Mazzola", '', '', 'R');
	}
}

/*
 * Image(string file, float x, float y [, float w [, float h
 * [, string type [, mixed link]]]])
 *
 */

$pdf=new PDF();
$pdf->AliasNbPages();
$pdf->AddPage();

$pdf->SetFont('Times','',10);

// Intestazione laterale:
$pdf->Image('img/bordo.jpg',4,40,5,220);
// Logo:
$pdf->Image($LogoRM,8,8,50);

if (isset($ImgStp) && ($ImgStp != "upload/") && ($ImgStp != ""))
{
	$pdf->Image($ImgStp, 85, 75, '', 35);
}
if (isset($ImgCase) && ($ImgCase != "upload/") && ($ImgCase != ""))
{
	$pdf->Image($ImgCase, 125, 70, '', 35);
}
if (isset($ImgMonitor) && ($ImgMonitor != "upload/") && ($ImgMonitor != ""))
{
	$pdf->Image($ImgMonitor, 160, 70, '', 35);
}

$pdf->Cell('',40,'','');
$pdf->ln();

$pdf->SetFont('Times','B',10);
$pdf->Cell(18, 4,'Oggetto: ','');
$pdf->SetFont('Times','',10);
$pdf->Cell(60, 4, $Oggetto);
$pdf->ln();
$pdf->ln();
$pdf->Cell('', 4, $Off_testo);
$pdf->ln();
$pdf->ln();
$pdf->ln();


if (isset($_SESSION['offerta_def']) && ($_SESSION['offerta_def'] != $TotOfferta))
{
	$TotOfferta = $_SESSION['offerta_def'];
}

// Spaziatore per le immagini
$pdf->Cell('',45,'');
$pdf->ln();
$pdf->SetFont('Times','B',10);
$pdf->Cell('', 4, 'Totale Offerta: Euro ' . round($TotOfferta, 0)
	. ',00 Iva inclusa.', '', '', 'R');
$pdf->ln();
$pdf->ln();
$pdf->ln();

$pdf->SetFont('Times','B',10);
$pdf->Cell('', 4, "Caratteristiche: ");
$pdf->ln();
$pdf->SetFont('Times','',10);

if (isset($_SESSION['macchina']))
{
	foreach ($_SESSION['macchina'] as $mch)
	{
		$pdf->Cell(4, 4);
		$pdf->Cell(4, 4, $bullet, '', '', 'R');
		$pdf->Cell('', 4, $mch['desc']);
		$pdf->ln();
	}
}

if (isset($_SESSION['accessori']))
{
	foreach ($_SESSION['accessori'] as $acc)
	{
		$pdf->Cell(4, 4);
		$pdf->Cell(4, 4, $bullet, '', '', 'R');
		$pdf->Cell('', 4, $acc['desc']);
		$pdf->ln();
	}
}

if (isset($_SESSION['assemblaggio']) && ($_SESSION['assemblaggio'] == 0))
{
	$pdf->Cell(4, 4);
	$pdf->Cell(4, 4, $bullet, '', '', 'R');
	$pdf->Cell('', 4, 'Assemblaggio ed installazione.');
	$pdf->ln();
}

$pdf->ln();
$pdf->ln();
$pdf->ln();
$pdf->ln();
$pdf->SetFont('Times','',10);

foreach ($Off_note as $key => $field)
{
	if ($key == 0)
	{
		$pdf->Cell('', 4, "Nota: " . $field);
	}else{
		$pdf->Cell('', 4, $field);
	}
	$pdf->ln();
}

$pdf->Output('Offerta.pdf', 'I');

?>

<?php
/*
 * Amministrazione Menu
 *
 * Copyright 2008 - 2014 Claudio Giordano <claudio.giordano@autistici.org>
 *
 */

if (!isset($_GET['tomod'])) {
    $_GET['tomod'] = "";
}

if (isset($_GET['del'])) {
    $Status = $d->deleteRows("tab_menu", "id_menu = '" . $_GET['del'] . "'");
}

if (isset($_GET['salva'])) {
    $errori     = "";
    $Esclusioni = array("add_menu" => "Salva");

    if (trim($_POST['titolo']) == "") {
        $errori .= "Errore, non è stata impostata una voce per il menu.<br />";
    }
    if (trim($_POST['link']) == "") {
        $errori .= "Errore, non è stata impostato il link alla pagina.<br />";
    }
    if (trim($_POST['gruppo']) == "") {
        $errori .= "Errore, non è stata impostato un gruppo per il menu.<br />";
    }
    if (trim($_POST['perm']) == "") {
        $errori .= "Errore, non sono stati impostati i permessi della pagina.<br />";
    }
    if (trim($_POST['act']) == "") {
        $errori .= "Errore, non è stata impostata l'azione per il link.<br />";
    }
    if (trim($_POST['page']) == "") {
        $errori .= "Errore, non è stata impostata una pagina da visualizzare.<br />";
    }

    if (($_GET['salva'] == "salva") && ($errori == "")) {
        $Campi = $d->saveRow($_POST, $Esclusioni, "tab_menu");
    }

    if (($_GET['salva'] == "modifica") && ($errori == "")) {
        $Campi = $d->UpdateRow($_POST, $Esclusioni, "tab_menu",
                               "id_menu = '"
                . $_GET['tomod'] . "'");
    }

    if ($errori != "") {
        ?>
        <label class="err"><?php echo $errori; ?></label>
        <?php
    } else {
        ?>
        <script type="text/javascript">
            document.location.href = "?act=adm_menu"
        </script>
        <?php
    }
}

if (is_numeric($_GET['tomod'])) {
    $menu_mod = $d->GetRows("*", "tab_menu",
                            "id_menu = '" . $_GET['tomod'] . "'");
}

if (isset($_GET['idt'])) {
    $campo      = explode(";", $_GET['idt']);
    $id_menu    = $campo[0];
    $id_group   = $campo[1];
    $st_prec    = $_GET['st']; // Stato precedente (0 attivato)
    $menurow    = $d->getRows("*", "tab_menu", "id_menu=  '$id_menu'", "", "",
                              false);
    $perm_str   = $menurow[0]['perm'];
    $perm_array = explode(";", $perm_str);

    array_pop($perm_array);

    if ($st_prec == 0) {
        $perm_str_new = "";
        // rimuovi l'id dalla stringa
        $st_new       = 1;
        foreach ($perm_array as $key => $field) {
            if ($field != $id_group) {
                $perm_str_new .= $field . ";";
            }
        }
    } else {
        // aggiungi l'id alla stringa
        $st_new       = 0;
        $perm_str .= $id_group . ";";
        $perm_array   = explode(";", $perm_str);
        array_pop($perm_array);
        asort($perm_array);
        $perm_str_new = join($perm_array, ";");
        $perm_str_new .= ";";
    }

    $st_update = $d->updateRow(
            array('perm' => "$perm_str_new"), array(), "tab_menu",
            "id_menu = '$id_menu'"
    );
    //~ echo "Debug => $status <br />";
}

$Gruppi = $d->getRows("*", "tab_tipi", "", "id_tipo");
$Menu   = $d->getRows("*", "tab_menu", "id_menu != ''", "",
                      "gruppo, ordine, titolo_pag");
?>


<form class="form-inline" role="form">
    <?php
    $d->ShowEditBar(
            "Voce Menu:", filter_input(INPUT_GET, 'tomod', FILTER_VALIDATE_INT),
                                       "new,save",
                                       filter_input(INPUT_GET, 'act',
                                                    FILTER_SANITIZE_STRING), 0
    );
    ?>

    <input type="text" class="form-control" name="titolo"
           maxlength="30" size="20"
           placeholder="Titolo" title="Titolo"
           value="<?php
           if (isset($menu_mod[0]['titolo'])) {
               echo $menu_mod[0]['titolo'];
           }
           ?>" />

    <input type="text" class="form-control" name="titolo_pag" maxlength="50"
           size="40" placeholder="Titolo Pagina" title="Titolo Pagina"
           value="<?php
           if (isset($menu_mod[0]['titolo_pag'])) {
               echo $menu_mod[0]['titolo_pag'];
           }
           ?>" />

    <input type="text" class="form-control" name="titolo_gruppo" maxlength="50"
           style="width: 18.5em;" placeholder="Titolo Gruppo"
           title="Titolo Gruppo"
           value="<?php
           if (isset($menu_mod[0]['titolo_gruppo'])) {
               echo $menu_mod[0]['titolo_gruppo'];
           }
           ?>" />

    <input type="text" class="form-control" name="link" maxlength="200"
           placeholder="Link" style="width: 20em;" title="Link"
           value="<?php
           if (isset($menu_mod[0]['link'])) {
               echo $menu_mod[0]['link'];
           }
           ?>" />

    <input type="text" class="form-control" name="gruppo" maxlength="20" size="20"
           placeholder="Gruppo" title="Gruppo"
           value="<?php
           if (isset($menu_mod[0]['gruppo'])) {
               echo $menu_mod[0]['gruppo'];
           }
           ?>" />

    <input type="text" class="form-control" name="perm" size="10" placeholder="Permessi"
           title="Permessi"
           value="<?php
           if (isset($menu_mod[0]['perm'])) {
               echo $menu_mod[0]['perm'];
           }
           ?>" />

    <input type="text" class="form-control" name="tip" maxlength="50" size="20"
           placeholder="Suggerimento" title="Suggerimento"
           value="<?php
           if (isset($menu_mod[0]['tip'])) {
               echo $menu_mod[0]['tip'];
           }
           ?>" />

    <input name="act" class="form-control" type="text" maxlength="30" size="20"
           placeholder="Azione" title="Azione"
           value="<?php
           if (isset($menu_mod[0]['act'])) {
               echo $menu_mod[0]['act'];
           }
           ?>" />

    <input name="ordine" class="form-control" type="text" maxlength="30" size="10"
           placeholder="Ordine" title="Ordine"
           value="<?php
           if (isset($menu_mod[0]['ordine'])) {
               echo $menu_mod[0]['ordine'];
           }
           ?>" />

    <input name="page" class="form-control" type="text" maxlength="100"
           style="width: 20em;" placeholder="Percorso pagina"
           title="Percorso pagina"
           value="<?php
           if (isset($menu_mod[0]['page'])) {
               echo $menu_mod[0]['page'];
           }
           ?>" />

    <?php
    if (isset($st_update)) {
        echo '<div class="alert alert-warning" role="alert">' . $st_update . '</div>';
    }
    ?>
</form>

<?php if (count($Menu) != 0) {
    ?>
    <form method="post" action="<?php
    echo filter_input(INPUT_SERVER, 'REQUEST_URI');
    ?>">
        <table class="table table-striped table-hover">
            <tr>
                <th><label>Titolo Pagina:</label></th>
                <th><label>Suggerimento:</label></th>
                <th><label>Permessi:</label></th>
                <th><label>Gruppo:</label></th>
                <th><label>Titolo Gruppo:</label></th>
                <th><label>Action:</label></th>
                <th><label>Ordine:</label></th>
                <th><br /></th>
            </tr>
            <?php
            foreach ($Menu as $key => $fieldm) {
                if ($fieldm['perm'] == "") {
                    $style = " class=\"err\"";
                } else {
                    $style = " class=\"ok\"";
                }
                ?>
                <tr>
                    <td >
                        <a onclick="Javascript: go('?act=adm_menu&amp;tomod=<?php echo $fieldm['id_menu']; ?>'); void(0);"
                           title="Modifica Voce" <?php echo $style; ?>>
                               <?php echo $fieldm['titolo_pag']; ?>
                        </a>
                    </td>
                    <td><?php echo $fieldm['tip']; ?></td>
                    <td>
                        <?php
                        foreach ($Gruppi as $key_group => $field) {
                            $flag_ck = $d->CheckAuth($field['id_tipo'],
                                                     $fieldm['perm']);
                            $url     = "?act=adm_menu&amp;idt="
                                    . $fieldm['id_menu'] . ";" . $field['id_tipo']
                                    . "&amp;st=" . $flag_ck[1];
                            ?>
                            <input name="ck<?php echo $fieldm['id_menu'] . $field['id_tipo']; ?>"
                                   type="checkbox"
                                   title="Visibile al gruppo <?php echo $field['tipo']; ?>"
                                   <?php
                                   if (isset($flag_ck[0])) {
                                       echo $flag_ck[0];
                                   };
                                   ?>
                                   onclick="Javascript: go_conf('Modificare i permessi?',
                                                   '<?php echo $url; ?>',
                                                   '?act=adm_menu')" />
                               <?php }
                               ?>
                    </td>
                    <td><?php echo $fieldm['gruppo']; ?></td>
                    <td><?php echo $fieldm['titolo_gruppo']; ?></td>
                    <td><?php echo $fieldm['act']; ?></td>
                    <td><?php echo $fieldm['ordine']; ?></td>

                    <td style="text-align: left;">
                        <a onclick="javascript: go_conf2('Eliminare la voce del menù?',
                                        '?act=adm_menu&amp;del=<?php echo $fieldm['id_menu']; ?>');
                                void(0);" title="Elimina" >
                            <span class="glyphicon glyphicon-trash"></span>
                        </a>
                    </td>
                </tr>
            <?php }
            ?>
        </table>
    </form>
    <?php
}


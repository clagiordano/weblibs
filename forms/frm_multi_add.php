<?php

	switch ($_GET['subact'])
	{
		case "articolo_com":
			if (isset($_SESSION['composto']['tomod']))
			{
				$_SESSION['composto']['modificato'] = "y";
				$OpenerUrl = "home.php?act=articolo_com&tomod="
					. $_SESSION['composto']['tomod'];
			}else{
				$OpenerUrl = "home.php?act=articolo_com";
			}
			break;

		case "documenti":
			//~ echo "Debug => caso documenti <br />";
			if (isset($_SESSION['documento']['tomod']))
			{
				$OpenerUrl = "home.php?act=documenti&tomod="
					. $_SESSION['documento']['tomod'];
			}else{
				$OpenerUrl = "home.php?act=documenti";
			}
	}

	$Iva = $d->GetRows ("*", "tab_iva", "", "", "id_iva");

	if (isset($_POST['voce_arb']))
	{
		$Iva = $d->GetRows ("*", "tab_iva",
			"id_iva = '" . $_POST['id_iva'] . "'", "", "id_iva");

		$errori = "";
		if (trim($_POST['voce_arb']) == "")
		{
			$errori .= "Errore, il campo valore non può essere vuoto.<br />";
		}

		switch ($_GET['subact'])
		{
			case "documenti":
				if (isset($_POST['prz']) && (trim($_POST['prz']) == "") || !is_numeric($_POST['prz']))
				{
					$errori .= "Errore, il campo prezzo immesso non è valido.<br />";
				}

				if (isset($_POST['qta']) && trim($_POST['qta']) == "")
				{
					$_POST['qta'] = 1;
				}

				if (isset($_POST['qta']) && !is_numeric($_POST['qta']))
				{
					$errori .= "Errore, il campo quantità immesso non è valido.<br />";
				}

				if (isset($_POST['id_iva']) && !is_numeric($_POST['id_iva']))
				{
					$errori .= "Errore, il campo iva selezionato non è valido.<br />";
				}

				if (isset($_POST['sco']) && ((!is_numeric($_POST['sco']))
					|| ($_POST['sco'] < 0) || ($_POST['sco'] > 100)))
				{
					if (trim($_POST['sco']) == "")
					{
						$_POST['sco'] = 0;
					}else{
						$errori .= "Errore, il campo sconto immesso non è valido.<br />";
					}
				}
				break;
		}
	}

	if (isset($_POST['voce_arb']) && ($errori == ""))
	{
		//~ Aggiungo in sessione l'array con le info:
		switch ($_GET['subact'])
        {
			case "documenti":
				//~ if (!isset($_SESSION['documento']['dettaglio']))
				//~ {
					//~ $_SESSION['documento']['dettaglio'] = array();
				//~ }

				//~ array_push($_SESSION['documento']['dettaglio'],
					//~ array('id_prodotto' => 0,
									 //~ 'voce_arb' => $_POST['voce_arb'],
									  //~ 'modello' => $_POST['voce_arb'],
											  //~ 'prz' => sprintf("%.2f", $_POST['prz']),
											 //~ 'disp' => '-',
									 //~ 'cod_forn' => '-',
										//~ 'cod_int' => '-',
                        //~ 'sco' => sprintf("%.2f", $_POST['sco']),
												//~ 'qta' => sprintf("%.2f", $_POST['qta']),
										 //~ 'id_iva' => $Iva[0]['id_iva'],
									 //~ 'desc_iva' => $Iva[0]['desc_iva']));
					AddDocumentRow("voce", 0, $_POST['id_iva'], $_POST['voce_arb'], 
						$_POST['qta'], $_POST['sco'], $_POST['prz']);
				break;

			case "piano_conti":
				if (!isset($_SESSION['piano']['dettaglio']))
				{
					$_SESSION['piano']['dettaglio'] = array();
				}

				array_push($_SESSION['piano']['dettaglio'],
					array('id_prodotto' => 0,
												 'da' => '',		//~ dare/avere (d o a)
							'id_anagrafica' => '',
									  'voce_arb' => $_POST['voce_arb'],
							 	  'importo_d' => '',
								  'importo_a' => '',
								     'id_iva' => '',
								 'id_causale' => $_POST['causale'])
				);
				break;
		}
		//~ Riaggiorno la pagina genitore e chiudo il popup:
		?>
			<script type="text/javascript">
				parent.location.href="<?php echo $OpenerUrl; ?>";
			</script>

	<?php }else{ ?>
		<label class="err"><?php if (isset($errori)) { echo $errori; } ?></label>
	<?php } ?>

<form method="post" action="<?php echo $_SERVER['REQUEST_URI']; ?>">
	<table class="form" style="width: 100%;">
		<tr>
			<td style="text-align: right;" colspan="2">
				<a onclick="Javascript: ClosePopup(); void(0);">
					[ <label class="err" style="cursor: pointer;">Chiudi</label> ]
				</a>
			</td>
		</tr>

		<tr>
			<th colspan="2">
				<?php if (isset($_GET['titolo']))
					{
						echo $_GET['titolo'] . ":";
					} ?>

					&nbsp;
					<a onclick="javascript: conferma_salva('0', 'Aggiungere la nuova voce?',
						'<?php echo $_SERVER['REQUEST_URI']; ?>')" title="Aggiungi">
						<img src="/Images/Links/add.png" alt="Aggiungi" />
						Aggiungi
          </a>
   			</th>
		</tr>

		<tr><td><br /></td></tr>

		<tr>
			<td style="text-align: right;">
				<label>Descrizione:</label>
			</td>
			<td>
				<input name="voce_arb" type="text" style="width: 35em;"
					value="<?php if (isset($_POST['voce_arb']))
					{ echo $_POST['voce_arb']; } ?>" />
			</td>
		</tr>

		<?php switch ($_GET['subact'])
		{
			case "documenti": ?>
				<tr>
					<td style="text-align: right;">
						<label>Prezzo:</label>
					</td>
					<td>
						<input name="prz" type="text" style="width: 5em;"
							value="<?php if (isset($_POST['prz']))
							{ echo $_POST['prz']; }; ?>" />

						&nbsp;
						<label>Iva:</label>
						<select name="id_iva">
							<?php foreach ($Iva as $key => $field) { ?>
								<option value="<?php echo $field['id_iva']; ?>">
									<?php echo $field['desc_iva']; ?>
								</option>
							<?php } ?>
						</select>

						&nbsp;
						<label>Quantità:</label>
						<input name="qta" type="text" style="width: 5em;"
							value="<?php if (isset($_POST['qta']))
							{ echo $_POST['qta']; }; ?>" />

						&nbsp;
						<label>Sconto:</label>
						<input name="sco" type="text" style="width: 5em;"
							value="<?php if (isset($_POST['sco']))
							{ echo $_POST['sco']; }; ?>" />
					</td>
				</tr>
				<?php break;

				case "piano_conti": ?>
					<!--<tr>
						<td style="text-align: right;">
							<label>Causale:</label>
						</td>
						<td>
							<input name="causale" type="text" style="width: 35em;"
								value="<?php //if (isset($_POST['causale'])) { echo $_POST['causale']; }; ?>" />
						</td>
					</tr>
					<tr>
						<td style="text-align: right;">
							<label>Dare:</label>
						</td>
						<td>
							<input name="importo_d" type="text" style="width: 7em;"
								value="<?php //if (isset($_POST['importo_d'])) { echo $_POST['importo_d']; }; ?>" />

							&nbsp;
							<label>Avere:</label>
							<input name="importo_a" type="text" style="width: 7em;"
								value="<?php //if (isset($_POST['importo_a'])) { echo $_POST['importo_a']; }; ?>" />

						</td>
					</tr>!-->
					<?php break;

							case "categoria": ?>
								<tr>
									<td style="text-align: right;">
										<label>Categoria:</label>
									</td>
									<td>
										<input name="categoria" type="text" style="width: 35em;"
											value="<?php //if (isset($_POST['causale'])) { echo $_POST['causale']; }; ?>" />
									</td>
								</tr>
             <?php break;
			} ?>

	</table>
</form>

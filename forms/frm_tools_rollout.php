<?php
	//onclick="Javascript: window.open('docs/proxy.reg', '_self'); void(0);"
?>

<div id="search-result">
	<table style="width: 100%;" class="form">
		<tr>
			<th colspan="2">Documentazione:</th>
			<th colspan="2">Strumenti:</th>
		</tr>
		<tr>
			<td style="width: 35px;"><br /></td>
			<th>
				<a onclick="Javascript: window.open('docs/Bios_HP8100.pdf', '_blank'); void(0);"
					title="Manuale configurazione bios per HP 8100">
					<img src="/Images/Links/pdf.png" alt="pdf" />
					Bios HP8100
				</a>
			</th>

			<td style="width: 35px;"><br /></td>
			<th>
				<a onclick="Javascript: window.open('http://unetbootin.sourceforge.net/', '_blank'); void(0);"
					title="Link al sito di UNetbootin per il download">
					<img src="/Images/Links/app.png" alt="app" />
					UNetbootin
				</a>
			</th>
		</tr>

		<tr>
			<td style="width: 35px;"><br /></td>
			<th>
				<a onclick="Javascript: window.open('docs/CheckList_Client.pdf', '_blank'); void(0);"
					title="CheckList migrazione client">
					<img src="/Images/Links/pdf.png" alt="pdf" />
					CheckList Client
				</a>
			</th>

			<td style="width: 35px;"><br /></td>
			<th>
				<a onclick="Javascript: window.open('docs/clonezilla-claudio.iso', '_blank'); void(0);"
					title="Download file iso da utilizzare con unetbootin per rendere la pendrive bootable">
					<img src="/Images/Links/cd.png" alt="cd" />
					Download iso di boot ( 119 MB ) <br />
				</a>
				<label>&nbsp;&nbsp;&nbsp;&nbsp; ( md5: 0eac2420dad9c7ac28e7281b75d12e3b )</label>
			</th>
		</tr>

		<tr>
			<td style="width: 35px;"><br /></td>
			<th>
				<a onclick="Javascript: window.open('docs/CheckList_Server.pdf', '_blank'); void(0);"
					title="CheckList migrazione server">
					<img src="/Images/Links/pdf.png" alt="pdf" />
					CheckList Server
				</a>
			</th>

			<td style="width: 35px;"><br /></td>
			<th>
				<a onclick="Javascript: window.open('docs/blancco.iso', '_blank'); void(0);"
					title="Download file iso del software blancco">
					<img src="/Images/Links/cd.png" alt="cd" />
					Download iso blancco ( 34 MB ) <br />
				</a>
				<label>&nbsp;&nbsp;&nbsp;&nbsp; ( md5: a60097c2916fc8bbbef00cbddb07c93c )</label>
			</th>

		</tr>

		<tr>
			<td style="width: 35px;"><br /></td>
			<th>
				<a onclick="Javascript: window.open('docs/CheckList_Stampanti.pdf', '_blank'); void(0);"
					title="CheckList migrazione stampanti">
					<img src="/Images/Links/pdf.png" alt="pdf" />
					CheckList Stampanti
				</a>
			</th>

			<td style="width: 35px;"><br /></td>
			<th>
				<a href="docs/proxy.reg"
					title="Attivazione proxy per utenza tecnici \\n ( tasto destro salva oggetto/link come... )">
					<img src="/Images/Links/app.png" alt="app" />
						Attivazione proxy per utenza tecnici
				</a>
			</th>
		</tr>

		<tr>
			<td style="width: 35px;"><br /></td>
			<th>
				<a onclick="Javascript: window.open('docs/CheckList_Verifica_Client.pdf', '_blank'); void(0);"
					title="CheckList verica client">
					<img src="/Images/Links/pdf.png" alt="pdf" />
					CheckList Verifica Client
				</a>
			</th>
		</tr>

		<tr>
			<td style="width: 35px;"><br /></td>
			<th>
				<a onclick="Javascript: window.open('docs/Blanco_Manuale.pdf', '_blank'); void(0);"
					title="Manuale blancco">
					<img src="/Images/Links/pdf.png" alt="pdf" />
					Manuale Blancco
				</a>
			</th>
		</tr>

		<tr>
			<td style="width: 35px;"><br /></td>
			<th>
				<a onclick="Javascript: window.open('docs/Config_multif.pdf', '_blank'); void(0);"
					title="Manuale configurazione multifunzione">
					<img src="/Images/Links/pdf.png" alt="pdf" />
					Manuale Configurazione Multifunzione
				</a>
			</th>
		</tr>

		<tr>
			<td style="width: 35px;"><br /></td>
			<th>
				<a onclick="Javascript: window.open('docs/Report_Request.pdf', '_blank'); void(0);"
					title="Report request vuoto">
					<img src="/Images/Links/pdf.png" alt="pdf" />
					Report Request
				</a>
			</th>
		</tr>

		<tr>
			<td style="width: 35px;"><br /></td>
			<th>
				<a onclick="Javascript: window.open('docs/Report_Request.xls', '_blank'); void(0);"
					title="Report request vuoto in formato xls">
					<img src="/Images/Links/xls.png" alt="xls" />
					Report Request
				</a>
			</th>
		</tr>

		<tr>
			<td style="width: 35px;"><br /></td>
			<th>
				<a onclick="Javascript: window.open('docs/Password_Prisma.pdf', '_blank'); void(0);"
					title="Report request vuoto in formato xls">
					<img src="/Images/Links/pdf.png" alt="pdf" />
					Recupero password prisma
				</a>
			</th>
		</tr>
	</table>

		<ul>
		<li>
			<label>Compilazione dettaglio request:</label>
		</li>
		<ul>
			<li>
				Ricerca della filiale tramite la vista
				<label>Pianificazione Filiali</label>.
			</li>

			<li>
				Click sul link <img src="/Images/Links/arch.png" alt="arch" />
				<label>Compilazione dettaglio request</label> <br /> alla fine del rigo
				della filiale corrispondente.
			</li>

			<ul>
				<li><label>Hardware Nuovo (Spedito in filiale)</label></li>
				<ul>
					<li>
						In caso di hardware nuovo, quello da portare in filiale, <br />
						inserire il seriale o l'asset nel campo ricerca rapida <br />
						quindi fare click sul link di selezione
						<img src="/Images/Links/select.png" alt="select" /> e confermare.
					</li>
				</ul>
			</ul>

			<ul>
				<li><label>Hardware Nuovo (Trovato in filiale)</label></li>
				<ul>
					<li>
						In caso di hardware nuovo, trovato in filiale, <br />
						come stampanti od eventuali pc, eseguire la stessa procedura <br />
						utilizzata per l'hardware dismesso <br />
						<label class="err">ma selezionando come provenienza <br />
						"IN LOCO" anzichè "DA FILIALE"</label>.
					</li>
				</ul>
			</ul>

			<ul>
				<li><label>Hardware Vecchio (Ritirato dalla filiale)</label></li>
				<ul>
					<li>
						In caso di hardware vecchio, quello ritirato dalla filiale, <br />
						fare click sul link <img src="/Images/Links/new.png" alt="new" />
						<label>Nuovo</label> e compilare i relativi campi.
					</li>
					<li>
						In caso di assenza / illegibilit&agrave; di seriale e/o asset <br />
						inserire semplicemente "nd" (senza le virgolette) ed il controllo <br />
						sull'unicit&agrave; verr&agrave; ignorato.
					</li>
					<li>
						terminata la compilazione, fare click sul link
						<img src="/Images/Links/apply.png" alt="apply" /> <label>Applica</label>, <br />
						per confermare l'inserimento della nuova voce.
					</li>
				</ul>
			</ul>

			<li>
				Al termine della compilazione del dettaglio
				<label class="err">è assolutamente necessario</label> fare click, <br />
				sul link <img src="/Images/Links/save.png" alt="save" /> <label>Salva</label>,
				per associare definitivamente il dettaglio alla filiale scelta.
			</li>
		</ul>
	</ul>

</div>



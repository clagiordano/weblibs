<?php
    
    /**
     * frm_documenti.php
     * 
     * Modulo per la registrazione e modifica dei documenti con ricerca e 
     * compilazione semplificata di articoli, anagrafiche ecc.
     * 
     * Copyright 2008 - 2014 Claudio Giordano <claudio.giordano@autistici.org>
     */
	// require_once("$root/Class/Gestione.php");
    require_once PHP_PATH . "Gestione.php";

	$d->SaveErrors	= "";
	$tot_res = -1;
	$imponibile_merce = 0;
	$totale_iva = 0;
	$TipiDoc = $d->GetRows("*", "tab_tipi_doc", "", "", "tipo_doc", 1);
	
	$Settings = Array();
	$temp_array = $d->GetRows("*", "tab_config", "opt LIKE 'ditta_doc_comp%'", 
		"", "", 1);
	foreach ($temp_array as $key => $field)
	{
		$Settings[$field['opt']] = $field['val'];
	}
	
	$BaseURL = "?act=documenti";

	if (isset($_GET['tomod']) && is_numeric($_GET['tomod']))
	{
		$BaseURL .= "&amp;tomod=" . $_GET['tomod'];
	}


	//~ echo "<pre>";
		//~ print_r($_POST);
	//~ echo "</pre>";

	//~ echo "<pre>";
		//~ print_r($_SESSION['documento']);
	//~ echo "</pre>";

	if (isset($_GET['selected']) && is_numeric($_GET['selected']))
	{
		AddDocumentRow("prodotto", $_GET['selected']);
	}
	
	if (isset($_GET['reset'])
		&& ($_GET['reset'] == "0"))
	{
		unset ($_SESSION['documento']);
		//~ echo "[Debug]: count documento" 
			//~ . count($_SESSION['documento']) . " <br />";
		//~ echo "[Debug]: count documento dettaglio" 
			//~ . count($_SESSION['documento']['dettaglio']) . " <br />";
	}

	if (isset($_GET['svuota']) && ($_GET['svuota'] == "0"))
	{
		unset($_SESSION['documento']['dettaglio']);
	}

	if (isset($_GET['del']) && is_numeric($_GET['del']))
	{
		unset($_SESSION['documento']['dettaglio'][$_GET['del']]);
	}

	if (!isset($_SESSION['documento']['dettaglio']))
	{
		$_SESSION['documento']['dettaglio'] = array();
	}

	if (!isset($_SESSION['documento']['spese']))
	{
		$_SESSION['documento']['spese'] = 0;
	}

	if (!isset($_SESSION['documento']['note']))
	{
		$_SESSION['documento']['note'] = "";
	}

	if (!isset($_SESSION['documento']['note_testa']))
	{
		$_SESSION['documento']['note_testa'] = "";
	}

	if (isset($_GET['tomod']))
	{
		$LinkL = array();

		array_push($LinkL, array('link' => "&nbsp;<a onclick=\"Javascript: window.open('frm_pdf_documenti.php?id_doc="
			. $_GET['tomod'] . "', '_blank'); void(0);\" title=\"Stampa Documento\">
			<img src=\"" . IMG_PATH . "Links/pdf.png\" alt=\"pdf\" />Stampa Documento</a>"));


		if (empty($_POST) && (!isset($_SESSION['documento']['tomod']))
			&& is_numeric($_GET['tomod']))
		{
			//echo "[Debug]: importo i dati per la modifica <br />";
			$doc = $d->GetRows("*", "tab_documenti",
				"id_documento = '" . $_GET['tomod'] . "'");

			$_SESSION['documento']['cod_documento'] = $doc[0]['cod_documento'];
			$_SESSION['documento']['id_anagrafica'] = $doc[0]['id_anagrafica'];
			$_SESSION['documento']['tipo_doc'] 		= $doc[0]['id_tipo'];
			$_SESSION['documento']['spese'] 		= $doc[0]['spese'];
			$_SESSION['documento']['note'] 			= $doc[0]['note'];
			$_SESSION['documento']['note_testa'] 	= $doc[0]['note_testa'];
			$_SESSION['documento']['data'] 			= $d->Inverti_Data($doc[0]['data_doc']);
			$_SESSION['documento']['data_scad'] 	= $d->Inverti_Data($doc[0]['data_scad']);

			//$imponibile_merce 											= $doc[0]['imponibile'];
			//$totale_iva 														= $doc[0]['iva'];

			$dett_doc = $d->GetRows("*", "tab_dett_documenti",
				"id_documento = '" . $_GET['tomod'] . "'", "", "", 1);

			foreach ($dett_doc as $key => $field)
			{				 
				if ($field['id_prodotto'] != 0)
				{
					// prodotto
					AddDocumentRow("prodotto", $field['id_prodotto'], $field['id_iva'], 
						$field['note'], $field['qta'], $field['sco'], $field['prz']);

				}else{
					// voce arbitraria
					AddDocumentRow("voce", $field['id_prodotto'], $field['id_iva'], 
						$field['voce_arb'], $field['qta'], $field['sco'], $field['prz']);
				}
			}
		}

		// flag per impedire la reimportazione durante la modifica
		$_SESSION['documento']['tomod'] = $_GET['tomod'];
	}

	if (isset($_SESSION['documento']['id_anagrafica']))
	{
		$Anagrafica = $d->GetRows("*", "tab_anagrafica", "id_anag = '"
			. $_SESSION['documento']['id_anagrafica'] . "'");
	}

	if (!isset($_POST['spese'])
		&& isset($_SESSION['documento']['id_anagrafica'])
		&& isset($_SESSION['documento']['tipo']) == "P")
	{
		$Spese = $d->GetRows("*", "tab_anagrafica, tab_mod_pagamento", "id_anag = '"
			. $_SESSION['documento']['id_anagrafica']
			. "' AND tab_anagrafica.id_pag = tab_mod_pagamento.id_pagamento", "", "", 0);
		$_SESSION['documento']['spese'] = $Spese[0]['costo'];
	}

	if (isset($_POST['spese']))
	{
		if (is_numeric($_POST['spese']))
		{
			$_SESSION['documento']['spese'] = $_POST['spese'];
		}else{ ?>
			<label class="err">
				Errore, il valore di spesa immesso non è valido.<br />
			</label>
		<?php }
	}

	if (isset($_POST['cod_documento']))
	{
		$_SESSION['documento']['cod_documento'] = trim(stripslashes($_POST['cod_documento']));
		$_SESSION['documento']['data'] 					= $_POST['data'];
		$_SESSION['documento']['data_scad'] 		= $_POST['data_scad'];
	}
	
	if (isset($_POST['note_testa']))
	{
		$_SESSION['documento']['note_testa'] = stripslashes($_POST['note_testa']);
	}
	
	if (isset($_POST['note']))
	{
		$_SESSION['documento']['note'] = stripslashes($_POST['note']);
	}	

	if (isset($_POST['tipo_doc']) && isset($_SESSION['documento']['tipo_doc'])
		&& ($_SESSION['documento']['tipo_doc'] != $_POST['tipo_doc']))
	{
		unset ($_SESSION['documento']);
		$_SESSION['documento']['spese'] = 0;
		$_SESSION['documento']['dettaglio'] = array();
	}

	if (isset($_POST['tipo_doc']) && ($_POST['tipo_doc'] != "-"))
	{
		$_SESSION['documento']['tipo_doc'] = $_POST['tipo_doc'];
	}

	if (!empty($_POST) && isset($_GET['key'])
		&& (!isset($_POST['cod_documento'])))
	{
		$key = $_GET['key'];
		// echo "Debug => qta richiesta: " . $_POST['qta' . $key] . " <br />";

		/*
		 * Se il tipo di documento è 5 ovvero fattura vendita
		 * e l'articolo è reale (quindi con un id e non una voce
		 * arbitraria con id = '-')
		 * verifica che la giacenza sia sufficiente:
		 */
		if (($_SESSION['documento']['tipo_doc'] == "5")
			&& ($_SESSION['documento']['dettaglio'][$key]['id_prodotto'] != 0))
		{
			if ($_SESSION['documento']['dettaglio'][$key]['qta'] != $_POST['qta' . $key])
			{
				$mod_qta = getDisponibilita($_SESSION['documento']['dettaglio'][$key]['id_prodotto'],
					$_POST['qta' . $key], 1);

				if ($mod_qta == "0.00")
				{
					$d->SaveErrors .= "Errore, impossibile variare la quantit&agrave;, il"
							. " prodotto non e' disponibile.<br />";
				}elseif ($mod_qta != $_POST['qta' . $key])
				{
					$d->SaveErrors .= "Errore, la giacenza non &egrave; sufficiente per "
						. "soddisfare la richiesta, <br /> imposto la quantit&agrave;"
						. " al massimo consentito dal magazzino.<br />";
					$_SESSION['documento']['dettaglio'][$key]['qta'] = $mod_qta;
				}else{
					$_SESSION['documento']['dettaglio'][$key]['qta'] = $_POST['qta' . $key];
				}
			}
		}elseif  (($_SESSION['documento']['tipo_doc'] != "5")) // && ($_SESSION['documento']['dettaglio'][$key]['id_prodotto'] != 0)
		{
			$_SESSION['documento']['dettaglio'][$key]['qta'] = $_POST['qta' . $key];
		}

		if ($_POST['sco' . $key] > 100)
		{
			$_POST['sco' . $key] = 100;
		}

		if ($_POST['sco' . $key] < 0)
		{
			$_POST['sco' . $key] = 0;
		}

		$_SESSION['documento']['dettaglio'][$key]['prz'] = $_POST['prz' . $key];
		$_SESSION['documento']['dettaglio'][$key]['sco'] = $_POST['sco' . $key];
		$_SESSION['documento']['dettaglio'][$key]['note'] = $_POST['note' . $key];
	}

	if (isset($_GET['salva']))
	{
		$d->SaveErrors = "";

		// Verifica unicita' documento ( accoppiata tipo codice )
		$VerificaDocumento = $d->GetRows("*", "tab_documenti",
			"cod_documento = '" . $_SESSION['documento']['cod_documento'] . "'"
			. " AND id_tipo = '" . $_SESSION['documento']['tipo_doc'] . "'", "", "", 1);

		if ( (count($VerificaDocumento) > 0) && ($_GET['salva'] == "salva"))
		{
			$d->SaveErrors .= "Errore, il codice documento inserito &egrave; esiste gi&agrave;',
				salvataggio annullato.<br />";
		}

		if (trim($_SESSION['documento']['cod_documento']) == "")
		{
			$d->SaveErrors .= "Errore, il numero di documento non può essere vuoto.<br />";
		}

		if (trim($_SESSION['documento']['data']) == "")
		{
			$d->SaveErrors .= "Errore, la data del documento non è valida.<br />";
		}

		if (!isset($_SESSION['documento']['id_anagrafica']) 
			|| ($_SESSION['documento']['id_anagrafica'] == ""))
		{
			$d->SaveErrors .= "Errore, non è stata selezionata un'anagrafica.<br />";
		}

		if ($_SESSION['documento']['tipo_doc'] == "")
		{
			$d->SaveErrors .= "Errore, non è stato selezionato il tipo di documento.<br />";
		}
		
		if (isset($_SESSION['documento']['dettaglio']) 
			&& (count($_SESSION['documento']['dettaglio']) < 1))
		{
			$d->SaveErrors .= "Errore, il documento non contiene nessuna voce.<br />";
		}

		if (isset($_SESSION['documento']['dettaglio'])
			&& (count($_SESSION['documento']['dettaglio']) > 0))
		{
			foreach ($_SESSION['documento']['dettaglio'] as $key => $field)
			{
				if ($field['sco'] == "0.00")
				{
					$imp_art = sprintf( "%." . PRECISIONE_DECIMALI . "f", ( $field['prz'] * $field['qta'] ) );
				}else{
					$sconto = $field['prz'] * ( $field['sco'] / 100 );
					$imp_art = sprintf( "%." . PRECISIONE_DECIMALI . "f", (($field['prz']-$sconto)*$field['qta']));
				}

				if (isset($field['id_iva']) && ($field['id_iva'] != ""))
				{
					$iva_art = 0;
					$totale_iva += (($imp_art*$field['iva'])-$imp_art);
				}
				$imponibile_merce += $imp_art;
			}
		}else{
			$d->SaveErrors .= "Errore, il documento non contiene nessuna voce.<br />";
		}

		// FIXME VERIFICA DEL DETTAGLIO PRIMA DEL SALVATAGGIO
		

		//~ Salvo il documento:
		if (isset($_GET['salva']) && ($d->SaveErrors == ""))
		{
			$Valori = array('cod_documento' => $_SESSION['documento']['cod_documento'],
                            'id_anagrafica' => $_SESSION['documento']['id_anagrafica'],
                            	 'data_doc' => $d->Inverti_Data($_SESSION['documento']['data']),
                            	'data_scad' => $d->Inverti_Data($_SESSION['documento']['data_scad']),
                            		 'note' => $_SESSION['documento']['note'],
                               'note_testa' => $_SESSION['documento']['note_testa'],
                               'imponibile' => sprintf("%." . PRECISIONE_DECIMALI . "f", $imponibile_merce),
                               		  'iva' => sprintf("%." . PRECISIONE_DECIMALI . "f", ($totale_iva)),
                                   'totale' => sprintf("%." . PRECISIONE_DECIMALI . "f", ($imponibile_merce+$_SESSION['documento']['spese'])+$totale_iva),
                                    'spese' => sprintf($_SESSION['documento']['spese']),
                                  'id_tipo' => $_SESSION['documento']['tipo_doc'],
                                   'ck_pag' => 1 );
							 
			if ($_GET['salva'] == "salva")
			{										 
				$d->SaveRow($Valori, "", "tab_documenti", 1);
				//~ $ResultStatus = $Status[0];
				//~ $IdDocumento 	= $Status[1];
				$IdDocumento = $d->LastID;
			}
			
			if ($_GET['salva'] == "modifica")
			{
				//~ Update documento (e drop articoli by id)
				$d->UpdateRow($Valori, "", "tab_documenti",
					"id_documento = '" . $_GET['tomod'] . "'");
				$d->DeleteRows("tab_dett_documenti",
					"id_documento = '" . $_GET['tomod'] . "'");
				$d->DeleteRows("tab_magazzino",
					"id_documento = '" . $_GET['tomod'] . "'");
				$IdDocumento = $_GET['tomod'];
			}
			
			//~ Salvo il dettaglio del documento
			foreach ($_SESSION['documento']['dettaglio'] as $key => $field)
			{
				$Valori = array('id_prodotto' => $field['id_prodotto'],
                                        'prz' => $field['prz'],
                                        'sco' => $field['sco'],
                                        'qta' => $field['qta'],
                                   'voce_arb' => $field['voce_arb'],
                                     'id_iva' => $field['id_iva'],
                                       'note' => $field['note'],
							   'id_documento' => $IdDocumento );

				$d->SaveRow($Valori, "", "tab_dett_documenti", 1);
				//~ $ResultStatus = $Status[0];
			}
			
			//~ Salvo gli articoli in magazzino
			foreach ($_SESSION['documento']['dettaglio'] as $key => $field)
			{
				// Preparo l'array con i dati:
				$Valori = array('id_prodotto' => $field['id_prodotto'],
                                    'cod_int' => $field['cod_int'],
                                        'prz' => $field['prz'],
                                        'sco' => $field['sco'],
                                    'ck_vend' => 1,
                                        'qta' => $field['qta'],
                                   'data_acq' => $d->Inverti_Data($_SESSION['documento']['data']),
                               'id_documento' => $IdDocumento );

				// Discriminante per le voci arbitrarie,
				// che non incrementano/decrementano il magazzino
				if (($field['cod_int'] != "-") && ($d->SaveErrors == ""))
				{
					switch ($_SESSION['documento']['tipo_doc'])
					{
						case "1":       // fattura acquisto, carico il magazzino
                            while ( $field['qta'] != 0 )
                            {
//                                echo "[DEBUG]: field_qta: " . $field['qta'] . " <br />";
                                
                                if ( $field['qta'] > 0 && $field['qta'] <= 1 ) 
                                {
                                    $Valori['qta'] = $field['qta'];
                                    $d->SaveRow( $Valori, "", "tab_magazzino", 1 );
                                    $field['qta'] -= $field['qta'];
                                }
                                
                                if ( $field['qta'] > 1 ) 
                                {
                                    $Valori['qta'] = 1;
                                    $d->SaveRow( $Valori, "", "tab_magazzino", 1 );
                                    $field['qta'] -= 1;
                                }
                            }
						break;

						case "5":	// fattura vendita
							// 	ripeto il ciclo finche' tutta la giacenza non e' stata aggiornata
							//~ $d->SessionLog("[Debug][Scarico Magazzino]: Fattura vendita");
							//~ $d->SessionLog("[Debug][Scarico Magazzino]: Qta necessaria: " . $Valori['qta']);

							// Acquisizione dati prodotto dall'anagrafica:
							$Articolo = $d->GetRows("*", "view_prodotti", "id_prodotto = '"
								. $_SESSION['documento']['dettaglio'][$key]['id_prodotto'] . "'");

							// Verifica preventiva se l'articolo e' un composto o un prodotto finito
							if ($Articolo[0]['ck_com'] == "0")
							{
								//~ $d->SessionLog("[Debug][Scarico Magazzino]: L'articolo e' un composto.");
								$Componenti = $d->GetRows("*", "view_prodotti", "id_prodotto = '"
									. $_SESSION['documento']['dettaglio'][$key]['id_prodotto'] . "'");

								foreach ($Componenti as $key_com => $field_com)
								{
									//~ echo "Debug => foreach componenti... <br />";
									$Need = $field_com['qta'];
									ScaricoMagazzino($field['cod_int'], $Need, 0);
								}
							}else{
								//~ $d->SessionLog("[Debug][Scarico Magazzino]: L'articolo non e' un composto");

								if (isset($_GET['tomod']))
								{
									$d->SessionLog("[Debug][Scarico Magazzino]: Il documento e' in modifica.");

									//~ Se il documento e' in modifica recupera la qta precedente,
									$dett_documento = $d->GetRows("*", "tab_dett_documenti",
										"id_documento = '" . $_GET['tomod'] . "' AND id_prodotto = '"
										. $Valori['id_prodotto'] .  "'", "", "", 1);

									$d->SessionLog("[Debug][Scarico Magazzino]: Q.t&agrave; precedente: "
										. $dett_documento[0]['qta']);

									//~ quindi aggiorna solo la differenza di giacenza
									//~ $Need = $Valori['qta'] - $dett_documento[0]['qta'];
									$Need = $Valori['qta'];
									$d->SessionLog("[Debug][Scarico Magazzino]: Differenza q.t&agrave;: $Need");

									//~ FIX: se la giacenza deve essere ri aumentata?
									//~ Modificare il flag da 0 (venduto) ad 1 (NON venduto)

								}else{
									$Need = $Valori['qta'];
								}

								//~ $d->SessionLog("[Debug][Scarico Magazzino]: Need: $Need");
								ScaricoMagazzino($field['cod_int'], $Need, 0);
							}
            break;
          }
        }
			}
			
			// redirect finale per attivazione modifica:
			if ($_GET['salva'] == "salva")
			{
				$_SESSION['documento']['tomod'] = $IdDocumento; ?>
					<script type="text/javascript">
						alert("Documento salvato correttamente, attivo la modifica.");
						document.location.href="home.php?act=documenti&tomod=<?php
							echo $IdDocumento; ?>";
					</script>
				<?php
			}
		}
	}

	if ( (!isset($_SESSION['documento']['cod_documento'])
		|| ($_SESSION['documento']['cod_documento'] == ""))
		&& ( isset($_SESSION['documento']['tipo_doc']) ))
	{
		// Genero il numero di documento univoco
		$temp_array = $d->GetRows("count(id_documento) as cod_anno", "tab_documenti",
			"cod_documento LIKE '%/" . date('Y')
			. "' AND id_tipo = '" . $_SESSION['documento']['tipo_doc'] . "'",
			"", "cod_documento desc limit 1", 1);

		//~ echo "<pre>";
			//~ print_r($temp_array);
		//~ echo "</pre>";

		$_SESSION['documento']['cod_documento'] = ($temp_array['0']['cod_anno']+1) . "/" . date('Y');
	}
?>

<div id="search-form">
	<form method="post" action="<?php echo $_SERVER['REQUEST_URI']; ?>">
		<?php
			if (isset($_GET['tomod']) && is_numeric($_GET['tomod']))
			{
				$d->ShowEditBar("Dati Documento:", $_GET['tomod'],
					"new,save", $_GET['act'], 8, 0, "#", $LinkL);
			}else{
				$d->ShowEditBar("Dati Documento:", $_GET['tomod'],
					"new,save", $_GET['act']);
			} ?>
			
		<div>
			<?php $d->DrawControl("select", "tipo_doc", "Selezione Causale",
				"\d+", $_SESSION['documento'], null, "auto", "",
				"onchange=\"javascript: conferma_posta('0',
					'Modificando la causale l\'operazione verrà resettata, continuare?',
					'?act=documenti', '?act=documenti'); void(0);\"",
				$TipiDoc, "id_tipo_doc", "tipo_doc"); ?>

				<?php if (isset($_SESSION['documento']['tipo_doc'])
					&& is_numeric($_SESSION['documento']['tipo_doc'])) { ?>

						&nbsp;
						<?php $d->DrawControl("text", "cod_documento", "Codice documento",
							"\w+", $_SESSION['documento'], null, "14em", "50",
							"onchange=\"Javascript: posta('0', '" . $BaseURL . "'); void(0);\""); ?>

						&nbsp;
						<?php $d->DrawControl("text", "data", "Data documento",
							"\w+", $_SESSION['documento'], null, "10em", "10",
							"onfocus=\"showCalendarControl(this);\" onchange=\"Javascript: posta('0', '" 
								. $BaseURL . "'); void(0);\""); ?>
								
						&nbsp;
						<?php $d->DrawControl("text", "data_scad", "Data scadenza",
							"", $_SESSION['documento'], null, "10em", "10",
							"onfocus=\"showCalendarControl(this);\" onchange=\"Javascript: posta('0', '" 
								. $BaseURL . "'); void(0);\""); ?>
							
						&nbsp;
						<?php $d->DrawControl("text", "spese", "Spese incasso",
							"\w+", $_SESSION['documento'], null, "4.5em", "10",
							"onchange=\"Javascript: posta('0', '" . $BaseURL . "'); void(0);\""); ?>
					
					<br />
					<a onclick="Javascript: ShowFormDiv('frm_ricerca_avanzata.php?subact=select',
						'150px', '150px', '870px', '500px', 'popup');">
						<?php if (!isset($_SESSION['documento']['id_anagrafica'])) { ?>
							<img src="<?php echo IMG_PATH ?>Links/select.png" alt="select" />
							Seleziona Anagrafica
						<?php }else{
							echo $Anagrafica[0]['ragione_sociale'];
						 } ?>
					</a>
					
					<a onclick="javascript: posta('0', '<?php echo "?act=documenti"; ?>'); void(0);">
						<img src="<?php echo IMG_PATH ?>Links/apply.png" alt="Conferma Dati" />
							Conferma Dati
					</a>

					<br />
					<?php $d->DrawControl("textarea", "note_testa", "Note di testa",
							"", $_SESSION['documento'], null, "58.5em", "",
							"onchange=\"Javascript: posta('0', '" . $BaseURL . "'); void(0);\""); ?>
					
					<br />
					<?php $d->DrawControl("textarea", "note", "Note di coda",
							"", $_SESSION['documento'], null, "58.5em", "",
							"onchange=\"Javascript: posta('0', '" . $BaseURL . "'); void(0);\""); ?>
				</div>
			<?php } ?>
	</form>
</div>

<?php if (isset($_SESSION['documento']['tipo_doc'])) { ?>
	<div id="search-result">
		<div id="dett_doc" class="expander">
			<div id="edit-bar">
				<img id="img_doc" src="<?php echo IMG_PATH ?>Links/bott.png" alt="img_doc"
					style="width: 16px; height: 16px; border: none;"
					onclick="Javascript: Expand('dett_doc', 'img_doc', '2em',
						'<?php echo IMG_PATH ?>Links/bott.png', '<?php echo IMG_PATH ?>Links/top.png'); void(0);"
					title="Click per Visualizzare/Nascondere il dettaglio" />

				&nbsp;
				Dettaglio Documento:

				&nbsp;
				<a onclick="Javascript: ShowFormDiv('frm_ricerca_articoli.php?subact=documenti',
					'150px', '150px', '850px', '500px', 'popup'); void(0);">
					<img src="<?php echo IMG_PATH ?>Links/add.png" alt="Aggiungi Articoli" title="Aggiungi Articoli" />
					Articolo
				</a>

				&nbsp;
				<a onclick="Javascript: ShowFormDiv('frm_multi_add.php?titolo=Nuova Voce&amp;subact=documenti',
					'150px', '150px', '550px', '200px', 'popup'); void(0);">
					<img src="<?php echo IMG_PATH ?>Links/add.png" alt="Aggiungi Voce" title="Aggiungi Voce" />
					Voce
				</a>

				<!--&nbsp;
				<a onclick="javascript: go_conf2('Rimuovere le voci selezionate dal dettaglio?',
					'<?php //echo "?act=documenti&amp;del_sel=0"; ?>'); void(0);">
					<img src="<?php echo IMG_PATH ?>Links/del.png" alt="del" />
					Selezionati
				</a>!-->

				&nbsp;
				<a onclick="javascript: go_conf2('Rimuovere tutti gli articoli dal documento?',
					'<?php echo "?act=documenti&amp;svuota=0"; ?>'); void(0);">
					<img src="<?php echo IMG_PATH ?>Links/clear.png" alt="clear" />
					Articoli (<label class="err" title="Totale articoli in dettaglio">
						<?php echo count($_SESSION['documento']['dettaglio']); ?>
					</label>)
				</a>

				<br />

				<?php $d->DrawControl("text", "live_articolo", "Ricerca veloce articolo",
					"\w+", $_POST, null, "50em", "200",
					"onkeyup=\"Javascript: LiveSearch(this, 'lsearch', 'frm_live_search_articoli.php"
						. $BaseURL . "&amp;search=' + this.value);\""); ?>
			</div>

			<form method="post" action="<?php echo $_SERVER['REQUEST_URI']; ?>">
                
                <div style="min-width: 3%; height: 2em; float: left; clear: left;" 
                     title="Numero riga"><label>N</label>
                </div>
                
				<?php if ((count($_SESSION['documento']['dettaglio']) != 0)) { 
					$totale_iva 			= 0;
					$imponibile_merce = 0; 
					
					foreach ($_SESSION['documento']['dettaglio'] as $key => $field) {
						if ($field['sco'] == "")
						{
							$field['sco'] = 0;
							$imp_art = sprintf("%." . PRECISIONE_DECIMALI . "f €", ($field['prz']*$field['qta']));
							//~ echo "Debug => $imp_art (sco 0)<br />";
						}else{
							$sconto = $field['prz']*($field['sco']/100);
							$imp_art = sprintf("%." . PRECISIONE_DECIMALI . "f €", (($field['prz']-$sconto)*$field['qta']));
							//~ echo "Debug => imp_art: $imp_art <br />";
						}
						
						if (isset($field['id_iva']) && ($field['id_iva'] != ""))
						{
							$iva_art = 0;
							//~ echo "[Debug]: field['iva']: " . $field['iva'] . " <br />";
							$totale_iva += (($imp_art*$field['iva'])-$imp_art);
						}else{
							echo "[Debug]: ERRORE, iva == \"\" !!! <br />";
						}
						
						$imponibile_merce += $imp_art; ?>
				
						<div class="dettaglio" style="width: 99.6%; vertical-align: middle;">
							<div style="min-width: 3%; height: 2em; float: left; clear: left;" 
								title="Numero riga">
								<label>
									<?php echo "[ " . ($key+1) . " ] "; ?>
								</label>
							</div>
							
							<?php if ($Settings['ditta_doc_comp_codice'] == 0) { ?>
								<div style="min-width: 12%; height: 2em; float: left;" title="Codice ( Forn / Int )">						
									<?php echo $field['cod_forn'] . " / " . $field['cod_int']; ?>
								</div>
							<?php } ?>
							
							<?php if ($field['id_prodotto'] > 0) { ?>	
								<?php if ($Settings['ditta_doc_comp_descr'] == 0) { ?>
									<div style="min-width: 40%; height: 2em; float: left;" title="Descrizione prodotto">
										<?php echo htmlentities($field['modello']); ?>
									</div>
								<?php } ?>
								
								<?php if ($Settings['ditta_doc_comp_note'] == 0) { ?>
									<div style="min-width: 10%; height: 2em; float: left;" title="Note">
										<textarea name="note<?php echo $key; ?>" onchange="javascript: posta('1', '<?php echo "?act=documenti&amp;key=$key"; if (isset($_GET['tomod'])) { echo "&amp;tomod=" . $_GET['tomod']; }?>'); void(0);" style="width: 10em; height: 2em;"><?php echo $field['note']; ?></textarea>
									</div>
								<?php } ?>
							<?php }else{ ?>
								<?php if ($Settings['ditta_doc_comp_descr'] == 0) { ?>
									<div style="min-width: 50.5%; height: 2em; float: left;" title="Descrizione prodotto">
										<?php echo htmlentities($field['modello']); ?>
									</div>							
								<?php } ?>
							<?php } ?>
							
							<?php if ($Settings['ditta_doc_comp_qta'] == 0) { ?>
								<div style="min-width: 5%; height: 2em; text-align: center; float: left;" 
									title="Quantit&agrave;">
									<input name="qta<?php echo $key; ?>"
										onchange="javascript: posta('1', '<?php echo "?act=documenti&amp;key=$key";
											if (isset($_GET['tomod']))
											{ echo "&amp;tomod=" . $_GET['tomod']; }?>'); void(0);"
										style="width: 5em;" value="<?php
											if ($field['um'] != "Pz")
											{
												printf("%." . PRECISIONE_DECIMALI . "f", $field['qta']);
											}else{
												printf("%." . PRECISIONE_DECIMALI . "f", $field['qta']);
											} ?>" />
								</div>
							<?php } ?>
							
							<?php if ($Settings['ditta_doc_comp_imp'] == 0) { ?>
								<div style="min-width: 5%; height: 2em; text-align: center; float: left;" 
									title="Imponibile Articolo">
									<input name="prz<?php echo $key; ?>"
										onchange="javascript: posta('1', '<?php echo "?act=documenti&amp;key=$key";
											if (isset($_GET['tomod']))
											{ echo "&amp;tomod=" . $_GET['tomod']; }?>'); void(0);"
										type="text" style="width: 5em;" value="<?php printf("%." . PRECISIONE_DECIMALI . "f", $field['prz']); ?>" />
								</div>
							<?php } ?>
							
							<?php if ($Settings['ditta_doc_comp_iva'] == 0) { ?>
								<div style="min-width: 5%; height: 2em; text-align: center; float: left;"
									title="IVA">
									<?php echo $field['desc_iva']; ?>
								</div>
							<?php } ?>
							
							<?php if ($Settings['ditta_doc_comp_sco'] == 0) { ?>
								<div style="min-width: 5%; height: 2em; text-align: center; float: left;"
									title="Sconto">
									<input name="sco<?php echo $key; ?>"
										onchange="javascript: posta('1', '<?php echo "?act=documenti&amp;key=$key";
										if (isset($_GET['tomod']))
											{ echo "&amp;tomod=" . $_GET['tomod']; }?>'); void(0);"
										type="text" style="width: 5em;" value="<?php printf("%." . PRECISIONE_DECIMALI . "f", $field['sco']); ?>" />
								</div>
							<?php } // TODO: aggiungere gli altri sconti a seguire. ?>
							
							<?php if ($_SESSION['documento']['tipo_doc'] == "5") { ?>
								<div style="min-width: 5%; height: 2em; text-align: center; vertical-align: middle; float: left;" 
									title="Disponibilità Articolo">
									<?php echo $field['disp']; ?>
								</div>
							<?php } ?>
							
							<?php if ($Settings['ditta_doc_comp_tot'] == 0) { ?>
								<div style="min-width: 8%; height: 2em; text-align: right; float: left;"
									title="Importo Totale">
									<?php echo $imp_art; ?>
								</div>
							<?php } ?>
							
							<div style="width: auto; text-align: center;">
								<a style="cursor: pointer;" onclick="javascript: go_conf2('Eliminare l\'articolo dal documento?',
									'?act=documenti&amp;del=<?php echo $key;
									if (isset($_GET['tomod'])) { echo "&amp;tomod=" . $_GET['tomod']; }?>');">
									<img src="<?php echo IMG_PATH ?>Links/del.png" alt="Elimina" />
								</a>
							</div>
						</div>
					<?php } ?>
				<?php } ?>
			</form>
		</div>

		<br />

		<div style="text-align: right ! important; width: 99.6%;">
			<div style="text-align: rigth; float: left; width: 90%;">
				<label>Imponibile Merce</label>:&nbsp;
			</div>
			<div style="text-align: left;">
				<label class="err">
					<?php printf("%." . PRECISIONE_DECIMALI . "f €", $imponibile_merce); ?>
				</label>
			</div>
			
			<div style="text-align: rigtht; float: left; width: 90%;">
				<label>Imponibile Totale</label>:&nbsp;
			</div>
			<div style="text-align: left;">
				<label class="err">
					<?php printf("%." . PRECISIONE_DECIMALI . "f €", $imponibile_merce+$_SESSION['documento']['spese']); ?>
				</label>
			</div>
			
			<div style="text-align: rigtht; float: left; width: 90%;">
				<label>Totale Iva</label>:&nbsp;
			</div>
			<div style="text-align: left;">
				<label class="err">
					<?php printf("%." . PRECISIONE_DECIMALI . "f €", ($totale_iva)); ?>
				</label>
			</div>
			
			<div style="text-align: rigtht; float: left; width: 90%;">
				<label>Totale Documento</label>:&nbsp;
			</div>
			<div style="text-align: left;">
				<label class="err">
					<?php printf("%." . PRECISIONE_DECIMALI . "f €", ($imponibile_merce+$_SESSION['documento']['spese'])+$totale_iva); ?>
				</label>
			</div>
		</div>

		<script type="text/javascript">
			Expand('dett_doc', 'img_doc', '2em', '<?php echo IMG_PATH ?>Links/bott.png',
				'<?php echo IMG_PATH ?>Links/top.png');
		</script>
<?php } ?>

<?php
/*
	echo "<pre>";
		print_r($_SESSION['documento']);
	echo "</pre>";
*/

	//~ echo "<pre> session dettaglio";
		//~ print_r($_SESSION['documento']['dettaglio']);
	//~ echo "</pre>";
?>
</div>



<?php session_start();
	$root = substr($_SERVER["DOCUMENT_ROOT"], 0, -1);
	require_once("$root/Class/XlsExport.php");

	$FileName = "tmp/RollOut_WIN7_RITIRI_FIL_" . $_SESSION['rollout']['cod_fil'] . ".xls";
	$x = new XlsExport($FileName);

	$hw_new = $d->GetRows ("*", "view_macchine", "provenienza = 'FORNITORE'",
		"id_macchina");

	$x->xlsWriteLabel(0, 0, "Branch Code:");
	$x->xlsWriteLabel(0, 1, $_SESSION['rollout']['cod_fil']);

	$x->xlsWriteLabel(1, 0, "N:");
	$x->xlsWriteLabel(1, 1, "Tipologia Apparato:");
	$x->xlsWriteLabel(1, 2, "Marca:");
	$x->xlsWriteLabel(1, 3, "Modello:");
	$x->xlsWriteLabel(1, 4, "Serial Number:");
	$x->xlsWriteLabel(1, 5, "Asset:");

	$xlsRow = 2;
	foreach ($_SESSION['rollout']['dettaglio'] as $key => $field)
	{
		if (($field['provenienza'] == "DA FILIALE")
			&& ($field['tipo_mch'] != "Server"))
		{
			$x->xlsWriteLabel ($xlsRow, 0, $xlsRow-1 . ")");
			$x->xlsWriteLabel ($xlsRow, 1, $field['tipo_mch']);
			$x->xlsWriteLabel ($xlsRow, 2, $field['marca']);
			$x->xlsWriteLabel ($xlsRow, 3, $field['modello']);
			$x->xlsWriteLabel ($xlsRow, 4, $field['serial']);
			$x->xlsWriteLabel ($xlsRow, 5, $field['asset']);
			$xlsRow++;
		}
	}

	$x->WriteFile();
  exit();

?>

<script type="text/javascript">
	<!--window.close();!-->
</script>

<?php
/**
 * frm_multi_edit.php
 * 
 * Copyright 2012 - 2014 Claudio Giordano <claudio.giordano@autistici.org>
 * 
 */

	// Form per il multi add/modifica di categorie unita' ecc:

	//~ echo "<pre>";
		//~ print_r($_GET);
	//~ echo "</pre>";

	switch ($_GET['type'])
	{
		case "um":
			$Table = "tab_um";
			$Description = "um";
			$Order = "um";
			$Label = "Unit&agrave; di misura";
		break;
		
		case "dep":
			$Table = "tab_depositi";
			$Description = "dep";
			$Order = "dep";
			$Label = "Deposito";
		break;
		
		case "cat":
			$Table = "tab_cat_merce";
			$Description = "cat";
			$Order = "cat";
			$Label = "Categoria Merceologica";
		break;
		
		case "iva":
			$Table = "tab_iva";
			$Description = "desc_iva";
			$Order = "iva";
			$Label = "IVA";
		break;
    
        case "tipiprod":
			$Table = "tab_tipo_art";
			$Description = "desc_art";
			$Order = "desc_art";
			$Label = "Tipo prodotto";
		break;
	}
	
	if (isset($_GET['del']) && is_numeric($_GET['del']))
	{
		$Status = $d->DeleteRows ($Table, "id_$Order = '" . $_GET['del'] . "'");
	}

	if (isset($_GET['salva']))
	{
		$errori = "";
		if (($_GET['salva'] == "salva") && ($errori == ""))
		{
			$Status = $d->SaveRow ($_POST, "", $Table, 1, $Order);
			$save_result = $Status[0];
		}

		if (($_GET['salva'] == "modifica") && ($errori == ""))
		{
			$save_result = $d->UpdateRow ($_POST, "", $Table,
				"id_$Order = '" . $_GET['tomod'] . "'");
		}

		// dopo il salvataggio ripopolo il select interessato:
		?>
			<script type="text/javascript">
				if (parent.document.getElementById('id_<?php echo $Order; ?>'))
				{
					select = parent.document.getElementById('id_<?php echo $Order; ?>');
				}else{
					select = parent.document.getElementById('id_<?php echo $Order; ?>');
				}

				select.options.length = 0;
				select.options[0] = new Option('Seleziona valore', '-');
				select.options[1] = new Option('', '-');

				<?php
					$Valori = $d->GetRows ("*", $Table, "", "", $Order, 1);
					foreach ($Valori as $key => $field)
					{
						?>
							select.options[<?php echo $key+2 ?>] = new Option('<?php echo $field[$Description]; ?>', 
								'<?php echo $field["id_" . $Order]; ?>');
						<?php
					}	
				?>

				ClosePopup();
			</script>
		<?php
	}

	$Valori = $d->GetRows ("*", $Table, "", "", $Order, 1);
	if (isset($_GET['tomod'])
		&& is_numeric($_GET['tomod']))
	{
		//~ $_GET['tomod'] = $_POST["id_" . $Order];

		$temp_array = $d->GetRows ("*", $Table,
			"id_$Order = '" . $_GET['tomod'] . "'", "", "", 1);
		$_POST = $temp_array[0];
	}

?>



<form method="post" action="<?php echo $_SERVER['REQUEST_URI']; ?>">
	<table class="form" style="width: 100%;">

		<tr>
			<td style="text-align: right;"  colspan="4" >
				<a onclick="Javascript: ClosePopup(); void(0);">
					[ <label class="err" style="cursor: pointer;">Chiudi</label> ]
				</a>
			</td>
		</tr>

		<?php
			$d->ShowEditBar("$Label:", $_GET['tomod'], "new,save",
				"&type=" . $_GET['type'], 4);
		?>

		<tr><th colspan="4"><?php if (isset($save_result)) { echo $save_result; } ?></th></tr>

		<tr><th colspan="4"><br /></th></tr>
		
		<tr>
			<?php if ($Order != "iva") { ?>
				<th style="text-align: right;">Valore:</th>
				<td>
					<input name="<?php echo $Order; ?>" type="text" style="width: 29.5em;"
						required pattern="\w+"
						value="<?php if (isset($_POST[$Order])) { echo $_POST[$Order]; } ?>" />
				</th>
			<?php }else{ ?>
				<th style="text-align: right;">Valore:</th>
				<td>
					<input name="<?php echo $Order; ?>" type="text" style="width: 5em;"
						required pattern="\w+"
						value="<?php if (isset($_POST[$Order])) { echo $_POST[$Order]; } ?>" />
				</td>
				
				<th style="text-align: right;">Descrizione:</th>
				<td>
					<input name="<?php echo $Description; ?>" type="text" style="width: 20em;"
						required pattern="\w+"
						value="<?php if (isset($_POST[$Description])) { echo $_POST[$Description]; } ?>" />
						
				</td>
			<?php } ?>
		</tr>
		
	</table>
	
	<table class="dettaglio" style="width: 100%;">
		<tr><th colspan="2"><h4>Valori attuali:</h4></th></tr>
		
		<tr><th colspan="2"><hr /></th></tr>
		
		<?php foreach ($Valori as $key => $field) { ?>
			<tr>
				<td>
					<a onclick="Javascript: go('<?php echo $_SERVER['REQUEST_URI']; ?>&tomod=<?php 
						echo $field["id_$Order"]; ?>'); void(0);">
						<?php echo $field[$Description]; ?>
					</a>
				</td>
				
				<td style="text-align: right;">
					<a title="Elimina voce" 
						onclick="Javascript: go_conf2('ATTENZIONE\nL\'azione può provocare la perdita di dati, usare cautela!\n\nEliminare la voce selezionata?', '<?php 
						echo $_SERVER['REQUEST_URI']; ?>&del=<?php echo $field["id_$Order"]; ?>'); void(0);">
						<img src="/Images/Links/del.png" alt="del"  />
					</a>
				</td>
			</tr>
		<?php } ?>
		
		<tr><th colspan="2"><hr /></th></tr>
		
	</table>
</form>

<?php

	switch ($_SESSION['id_menu'])
	{

		case "47":
			$ID = "id_generalita";
			$Table = "tab_generalita";
			$Descrizione = "Paziente";
			$ElencoCampi = Array('n_cartella' => 'N. Cartella:',
														'cognome' => 'Cognome:',
														'nome' => 'Nome:',
														'sesso' => 'Sesso:',
														'luogo_nascita' => 'Luogo di nascita:',
														'data_nascita' => 'Data di nascita:',
														'eta' => 'Eta\':',
														'altezza' => 'Altezza:',
														'Targa' => 'Provincia:',
														'Comune' => 'Comune:',
														'cap' => 'Cap:',
														'indirizzo' => 'Indirizzo:',
														'telefono' => 'Telefono:',
														'cellulare' => 'Cellulare:',
														'skype' => 'Contatto skype:',
														'email' => 'Indirizzo E-mail:',
														'stato_civile' => 'Stato Civile:',
														'coniuge' => 'Coniugato con:',
														'istruzione' => 'Istruzione:',
														'attivita_attuale' => 'Attivita\' lavorativa attuale:',
														'tipo_lavoro' => 'Tipo di lavoro svolto:',
														'attivita_attuale_anni' => 'Attivita\' lavorativa principale ( anni ):',
														'eta_pensionamento' => 'Eta\' pensionamento:',
														'forma_clinica' => 'Forma clinica:',
														'diagnosi' => 'Diagnosi:',
														'progression' => 'Rate of progression:',
														'esordio_sintomo' => 'Sintomo d\'esordio',
														'esordio_data' => 'Data esordio:',
														'esordio_eta' => 'Eta\' esordio:',
														'diagnosi_data' => 'Data diagnosi:',
														'diagnosi_eta' => 'Ritardo diagnostico ( mesi ):',
														'esordio_sede' => 'Sede d\'esordio:',
														'esordio_esame' => 'Primo esame dopo l\'esordio:',
														'esordio_1_consulto' => 'Int. I sintomo - I consulto (mesi):',
														'esordio_1_neurologo' => 'Int. I sintomo - I neurologo (mesi):',
														'esordio_1_conferma' => 'Int. I sintomo - conferma diagnosi (mesi):',
														'esordio_1_centro' => 'Int. I sintomo - I visita al Centro (mesi):',
														'diagnosi_centro' => 'Diagnosi effettuata c/o il Centro:',
														'n_visite_centro' => 'Tot. visite c/o il Centro:',
														'follow_up' => 'Paziente seguito in follow-up:',
														'n_ricoveri_centro' => 'N. ricoveri:',
														'esordio_1_frs' => 'Esordio - I ALSFRS-R (mesi):',
														'data_exitus' => 'Data exitus:',
														'esordio_exitus' => 'Intervallo esordio-exitus:',
														'causa_exitus' => 'Causa exitus:');
			break;

		case "48":
			$ID = "id_anamnesi_familiare";
			$Table = "tab_anamnesi_familiare";
			$Descrizione = "Anamnesi familiare";
			$ElencoCampi = Array( 'genitori_consanguinei' => 'Genitori consanguinei:',
														'sla' => 'SLA:',
														'demenza' => 'Demenza:',
														'parkinson' => 'Parkinson:',
														'altre_malattie_neurologiche' => 'Altre malattie neurologiche:',
														'allergie' => 'Allergie:',
														'diabete' => 'Diabete:',
														'neoplasie' => 'Neoplasie:',
														'malattie_tiroide' => 'Malattie tiroide:',
														'altre' => 'Altre:',
														'note' => 'Note:');
			break;

		case "49":
			$ID = "id_anamnesi_fisiologica";
			$Table = "tab_anamnesi_fisiologica";
			$Descrizione = "Anamnesi fisiologica";
			$ElencoCampi = Array('nascita_prematura' => 'Nascita prematura:',
														'parto_eutocico' => 'Parto eutocico:',
														'primi_atti_fisiologici' => 'Primi atti fisiologici:',
														'primi_passi' => 'Primi passi ( mesi ):',
														'rendimento_scolastico' => 'Insufficienza mentale:',
														'abile_leva' => 'Abile leva:',
														'esentato_leva' => 'Esentato leva:',
														'menarca' => 'Menarca:',
														'menopausa' => 'menopausa:',
														'sposato' => 'Sposato/a:',
														'cicli' => 'Cicli:',
														'n_gravidanze' => 'N. gravidanze:',
														'n_aborti' => 'N. aborti:',
														'n_figli' => 'N. figli:',
														'attivita_fisica' => 'Attivita fisica:',
														'alimentazione' => 'Alimentazione:',
														'alvo' => 'Alvo:',
														'diuresi' => 'Diuresi:',
														'fuma' => 'Fuma:',
														'durata_fumo' => 'Durata fumo (anni):',
														'n_sigarette' => 'N. sigarette (die):',
														'beve_alcol' => 'Beve alcol:',
														'durata_alcol' => 'Durata alcol (anni):',
														'n_bicchieri' => 'N. bicchieri (die):',
														'caffe' => 'Beve caffe\':',
														'n_caffe' => 'N. caffe\' (die):',
														'ritmo_sonno_sveglia' => 'Ritmo sonno/sveglia:',
														'destrimane' => 'Destrimane:',
														'note' => 'Note:');
			break;

		case "50":
			$ID = "id_scala_appel";
			$Table = "tab_scala_appel";
			$Descrizione = "Scala di appel";
			$ElencoCampi = Array('score_bulbare' => 'Score bulbare:',
														'score_resp' => 'Score respirazione:',
														'score_forza_musc' => 'Score forza muscolare:',
														'score_arti_sup' => 'Score arti superiori:',
														'score_arti_inf' => 'Score arti inferiori:',
														'tot_test' => 'Totale:');
			break;

		case "51":
			$ID = "id_testLab";
			$Table = "tab_testLab";
			$Descrizione = "Test di laboratorio";
			$ElencoCampi = Array('glicemia' => 'Glicemia:',
														'cpk' => 'CPK:',
														'ldh' => 'LDH:',
														'cl' => 'CL:',
														'na' => 'Na:',
														'k' => 'K:',
														'uricemia' => 'Uricemia:',
														'colesterolo_tot' => 'Colesterolo totale:',
														'colesterolo_HDL' => 'Colesterolo HDL:',
														'colesterolo_LDL' => 'Colesterolo LDL:',
														'trigliceridi' => 'Trigliceridi:',
														'note' => 'Note:');
			break;

		case "52":
			$ID = "id_testLab_ormoni";
			$Table = "tab_testLab_ormoni";
			$Descrizione = "Ormoni sessuali";
			$ElencoCampi = Array('beta_estradiolo' => '17 beta - estradiolo:',
														'dheas' => 'DHEAS:',
														'testosterone_libero' => 'Testosterone libero:',
														'testosterone_totale' => 'Testosterone totale:');
			break;

		case "53":
			$ID = "id_testLab_liquor";
			$Table = "tab_testLab_liquor";
			$Descrizione = "Esame del liquor";
			$ElencoCampi = Array('aspetto' => 'Aspetto:',
														'colore' => 'Colore:',
														'proteinorrachia' => 'Proteinorrachia:',
														'glicorrachia' => 'Glicorrachia:',
														'cellule' => 'Cellule (n/mmc):',
														'immunoglobuline' => 'Immunoglobuline liquorali:',
														'bandeOligoclo'=> 'Bande Oligoclo:');
			break;

		case "54":
			$ID = "id_testLab_autoanticorpi";
			$Table = "tab_testLab_autoanticorpi";
			$Descrizione = "Autoanticorpi";
			$ElencoCampi = Array('anti_nucleo' => 'Ab Anti-nucleo:',
														'anti_nucleo_res' => 'Esito:',
														'anti_neurone' => 'Anti-neurone:',
														'anti_neurone_res' => 'Esito:',
														'anti_muscolo_liscio' => 'Anti-muscolo liscio:',
														'anti_muscolo_liscio_res' => 'Esito:',
														'anti_assone' => 'Anti-assone IgG:',
														'anti_assone_res' => 'Esito:',
														'anti_mielina' => 'Anti-mielina IgG:',
														'anti_mielina_res' => 'Esito:',
														'anti_gmi' => 'Anti GMI IgG:',
														'anti_gmi_res' => 'Esito:',
														'anti_mag' => 'Anti Mag IgG:',
														'anti_mag_res' => 'Esito:',
														'anti_nervo' => 'Anti-nervo GQ1B IgG:',
														'anti_nervo_res' => 'Esito:',
														'anti_assone2' => 'Anti-assone IgM:',
														'anti_assone2_res' => 'Esito:',
														'anti_mielina2' => 'Anti-mielina IgM:',
														'anti_mielina2_res' => 'Esito:',
														'anti_gmi2' => 'Anti GMI IgM:',
														'anti_gmi2_res' => 'Esito:',
														'anti_solfatidi' => 'Anti-solfatidi IgM:',
														'anti_solfatidi_res' => 'Esito:');
			break;

		case "55":
			$ID = "id_testLab_tiroidea";
			$Table = "tab_testLab_tiroidea";
			$Descrizione = "Indici di funzionalita' tiroide";
			$ElencoCampi = Array('t3' => 'T3:',
														't3_res' => 'Esito:',
														't4' => 'T4:',
														't4_res' => 'Esito:',
														'tsh' => 'TSH:',
														'tsh_res' => 'Esito:');
			break;

		case "56":
			$ID = "id_testLab_epatica";
			$Table = "tab_testLab_epatica";
			$Descrizione = "Indici di funzionalita' epatica";
			$ElencoCampi = Array('tgo' => 'TGO (AST):',
														'tgp' => 'TGP (ALT):',
														'fosfatasiAlcalina' => 'Fosfatasi Alcalina:',
														'gamma_gt' => 'gamma-GT:',
														'bilirubina_tot' => 'Bilirubina totale:');
			break;

		case "57":
			$ID = "id_testLab_renale";
			$Table = "tab_testLab_renale";
			$Descrizione = "Indici di funzionalita' renale";
			$ElencoCampi = Array('azotemia' => 'Azotemia:',
														'creatinina' => 'Creatinina:');
			break;

		case "60":
			$ID = "id_anamnesi_patologica_remota";
			$Table = "tab_anamnesi_patologica_remota";
			$Descrizione = "Anamnesi patologica remota";
			$ElencoCampi = Array('dermatologiche' => 'Dermatologiche:',
														'otorrinolaringologo' => 'Occhi, orecchie, naso e gola:',
														'respiratorio' => 'Apparato respiratorio:',
														'cardiovascolare' => 'Apparato cardiovascolare:',
														'muscolo_scheletriche' => 'Muscolo-scheletriche:',
														'gastrointestinali' => 'Apparato gastrointestinale:',
														'genito_urinarie' => 'Genito-urinarie:',
														'endocrino_dismetaboliche' => 'Endocrino-dismetaboliche:',
														'neurologiche' => 'Neurologiche:',
														'psichiatriche' => 'Psichiatriche:',
														'emato_immunitario' => 'Ematol. / sist. immunitario:',
														'note' => 'Note:');
			break;

		case "61":
			$ID = "id_patologie_associate";
			$Table = "tab_patologie_associate";
			$Descrizione = "Patologie associate";
			$ElencoCampi = Array('cancro' => 'Cancro:',
														'compressione_midollare' => 'Compressione midollare:',
														'demenza' => 'Demenza:',
														'diabete_mellito' => 'Diabete mellito:',
														'parkinson' => 'Malattia di Parkinson:',
														'pelle' => 'Malattie della pelle:',
														'sn' => 'Malattie del SN:',
														'tiroidee' => 'Malattie tiroidee:',
														'allergiche' => 'Malattie allergiche:',
														'poliomielite' => 'Poliomielite:',
														'ernie_discali' => 'Ernie discali:',
														'interventi_chirurgici' => 'Interventi chirurgici o traumi:');
			break;

		case "62":
			$ID = "id_esame_obiettivo";
			$Table = "tab_esame_obiettivo";
			$Descrizione = "Esame obiettivo";
			$ElencoCampi = Array('condizioni_generali' => 'Condizioni generali:',
														'cute' => 'Cute:',
														'sottocutaneo' => 'Sottocutaneo:',
														'masse_muscolari' => 'Masse muscolari:',
														'ossa_articolazioni' => 'Ossa ed articolazioni:',
														'capo' => 'Capo:',
														'capo_stazioni_linfonodali' => 'Capo (stazioni linfonodali):',
														'collo' => 'Collo (compresa tiroide):',
														'torace' => 'Torace:',
														'mammelle' => 'Mammelle:',
														'cuore' => 'Cuore:',
														'vasi_periferici' => 'Vasi periferici:',
														'addome' => 'Addome:',
														'schiena' => 'Schiena:',
														'arti_superiori' => 'Arti superiori:',
														'arti_inferiori' => 'Arti inferiori:',
														'genitali_esterni' => 'Genitali esterni:',
														'linfonodi' => 'Linfonodi:',
														'neurologico' => 'Esame neurologico (sintesi):');
			break;

		case "63":
			$ID = "id_elettromiografia";
			$Table = "tab_elettromiografia";
			$Descrizione = "Elettromiografia";
			$ElencoCampi = Array('elettromiografia' => 'Esame Elettromiografico:');
			break;

		case "64":
			$ID = "id_risonanza_magnetica";
			$Table = "tab_risonanza_magnetica";
			$Descrizione = "Risonanza Magnetica";
			$ElencoCampi = Array('cerebrale' => 'Cerebrale:',
														'cervicale' => 'Cervicale:',
														'dorsale' => 'Dorsale:',
														'lombo_sacrale' => 'Lombo-sacrale:');
			break;

		case "65":
			$ID = "id_tac";
			$Table = "tab_tac";
			$Descrizione = "TC";
			$ElencoCampi = Array('cerebrale' => 'Cerebrale:',
														'cervicale' => 'Cervicale:',
														'dorsale' => 'Dorsale:',
														'lombo_sacrale' => 'Lombo-sacrale:');
			break;

		case "66":
			$ID = "id_diagnosi";
			$Table = "tab_diagnosi";
			$Descrizione = "Diagnosi";
			$ElencoCampi = Array();
			break;

		case "67":
			$ID = "id_test_genetici";
			$Table = "tab_test_genetici";
			$Descrizione = "Test genetici";
			$ElencoCampi = Array('atassie_sca' => 'Atassie - SCA:',
														'atassie_sca_note' => 'Note:',
														'malattia_fried' => 'Malattia di Friedreich (gene X25):',
														'malattia_fried_note' => 'Note:',
														'demenza' => 'Demenza fronto-temporale (gene MAPT):',
														'demenza_note' => 'Note:',
														'sma' => 'SMA (gene SMN e NAIP):',
														'sma_note' => 'Note:',
														'paraparesi_spast_dom' => 'Paraparesi spastica dominante:',
														'paraparesi_spast_dom_note' => 'Note:',
														'paraparesi_spast_rec1' => 'Paraparesi spastica recessiva (3q27, 16q24):',
														'paraparesi_spast_rec1_note' => 'Note:',
														'paraparesi_spast_rec2' => 'Paraparesi spastica recessiva (paraplegina):',
														'paraparesi_spast_rec2_note' => 'Note:',
														'sla_sod' => 'SOD 1:',
														'sla_sod_note' => 'Note:',
														'sla_alsin' => 'Alsin:',
														'sla_alsin_note' => 'Note:',
														'kennedy' => 'Malattia di Kennedy:',
														'kennedy_note' => 'Note:',
														'fus' => 'FUS:',
														'fus_note' => 'Note:',
														'tardp' => 'TARDP:',
														'tardp_note' => 'Note:',
														'c9orf72' => 'C9orf72:',
														'c9orf72_note' => 'Note:',
														'vcp' => 'VCP:',
														'vcp_note' => 'Note:');
			break;

		case "68":
			$ID = "id_peso";
			$Table = "tab_pesoBMI";
			$Descrizione = "Peso e BMI";
			$ElencoCampi = Array('peso' => 'Peso ( Kg ):',
														'altezza' => 'Altezza ( cm ):',
														'bmi' => 'Valore BMI risultante:',
														'commento' => 'Commento:');
			break;

		case "69":
			$ID = "id_vfg_esofagea";
			$Table = "tab_vfg_esofagea";
			$Descrizione = "Videofluorografia esofagea";
			$ElencoCampi = Array('alterazione' => 'Alterazione della fase orale:',
														'ristagno' => 'Ristagno nei seni piriformi:',
														'aspirazione' => 'Aspirazione mdc nelle vie aeree:',
														'vfg_esofagea' => 'Videofluorografia Esofagea:');
			break;

		case "70":
			$ID = "id_testLab_pneumologica";
			$Table = "tab_testLab_pneumologica";
			$Descrizione = "Pneumologia";
			$ElencoCampi = Array('fvc' => 'FVC (%):',
														'ph' => 'pH:',
														'pco2' => 'pCO2 (mmHg):',
														'po2' => 'pO2 (mmHg):',
														'so2' => 'sO2 (%):',
														'pco2_mmhg' => 'Bicarbonati:',
														'sat_notturna' => 'Tempo sO2<89% (minuti):',
														'sat_note' => 'Sat. Notturna - Note:',
														'visita_note' => 'Visita pneumologica - Note:');
			break;

		case "72":
			$ID = "id_alsfrsr";
			$Table = "tab_alsfrsr";
			$Descrizione = "ALSFRS-R";
			$ElencoCampi = Array('eseguito_da' => 'Test eseguito da:',
														'score_bulbare' => 'Score bulbare:',
														'score_arti_sup' => 'Score arti superiori:',
														'score_arti_inf' => 'Score arti inferiori:',
														'score_resp' => 'Score respiratorio:',
														'tot_test' => 'Totale:');
			break;

		case "73":
			$ID = "id_alsfrsex";
			$Table = "tab_alsfrsex";
			$Descrizione = "ALSFRS-EX";
			$ElencoCampi = Array('eseguito_da' => 'Test eseguito da:',
														'tot_test' => 'Totale:');
			break;

		case "75":
			$ID = "id_alssqol";
			$Table = "tab_alssqol";
			$Descrizione = "ALS-Specific Quality of Life Questionnaire-Revised";
			$ElencoCampi = Array('tot_test' => 'Totale:');
			break;

		case "76":
			$ID = "id_spect";
			$Table = "tab_spect";
			$Descrizione = "SPECT";
			$ElencoCampi = Array('spect' => 'SPECT:');
			break;

		case "77":
			$ID = "id_pet";
			$Table = "tab_pet";
			$Descrizione = "PET";
			$ElencoCampi = Array('pet' => 'PET:');
			break;

		case "78":
			$ID = "id_hads";
			$Table = "tab_hads";
			$Descrizione = "Hospital Anxiety and Depression Scale";
			$ElencoCampi = Array('eseguito_da' => 'Test eseguito da:',
														'tot_test' => 'Totale:');
			break;

		case "79":
			$ID = "id_als_depression";
			$Table = "tab_als_depression";
			$Descrizione = "ADI";
			$ElencoCampi = Array('eseguito_da' => 'Test eseguito da:',
														'tot_test' => 'Totale:');
			break;

		case "80":
			$ID = "id_fab";
			$Table = "tab_fab";
			$Descrizione = "Frontal Assessment Battery";
			$ElencoCampi = Array('tot_test' => 'Totale:');
			break;

		case "81":
			$ID = "id_frsbe";
			$Table = "tab_frsbe";
			$Descrizione = "Frontal Systems Behavior Scale";
			$ElencoCampi = Array('eseguito_da' => 'Test eseguito da:',
														'tot_apa_prima' => 'Apathy ( prima ):',
														'tot_apa_prima_cor' => 'Apathy ( prima corr ):',

														'tot_dis_prima' => 'Disinibithion ( prima ):',
														'tot_dis_prima_cor' => 'Disinibithion ( prima corr ):',

														'tot_exe_prima' => 'Executive dysfunction ( prima ):',
														'tot_exe_prima_cor' => 'Executive dysfunction ( prima corr ):',

														'tot_test_prima' => 'Totale ( prima ):',
														'tot_test_prima_cor' => 'Totale ( prima corr ):',

														'tot_apa_dopo' => 'Apathy ( dopo ):',
														'tot_apa_dopo_cor' => 'Apathy ( dopo corr ):',

														'tot_dis_dopo' => 'Disinibithion ( dopo corr ):',
														'tot_dis_dopo_cor' => 'Disinibithion ( dopo ):',

														'tot_exe_dopo' => 'Executive dysfunction ( dopo ):',
														'tot_exe_dopo_cor' => 'Executive dysfunction ( dopo corr ):',

														'tot_test_dopo' => 'Totale ( dopo ):',
														'tot_test_dopo_cor' => 'Totale ( dopo corr ):');
			break;

		case "83":
			$ID = "id_testLab_altriEsami";
			$Table = "tab_testLab_altriEsami";
			$Descrizione = "Altri esami";
			$ElencoCampi = Array('esame' => 'Nome Esame:',
														'file_note' => 'Descrizione:');
			break;

		case "85":
			$ID = "id_potenziali_evocati";
			$Table = "tab_potenziali_evocati";
			$Descrizione = "Potenziali evocati";
			$ElencoCampi = array('pem' => 'PEM:',
														'pess' => 'PESS:',
														'pev' => 'PEV:',
														'baep' => 'BAEP:');
			break;

		case "87":
			$ID = "id_farmaci_praticati";
			$Table = "tab_farmaci_praticati";
			$Descrizione = "Farmaci praticati";
			$ElencoCampi = array('farmaco' => 'Farmaco:',
														'posologia' => 'Posologia:');
			break;


		case "90":
			$ID = "id_anamnesi_patologica_prossima";
			$Table = "tab_anamnesi_patologica_prossima";
			$Descrizione = "Anamnesi patologica prossima";
			$ElencoCampi = array('descrizione' => 'Anamnesi:');
			break;

		case "91":
			$ID = "id_certificato";
			$Table = "tab_certificati";
			$Descrizione = "Certificato";
			$ElencoCampi = array('certificato' => 'Titolo Certificato:',
														'note' => 'Descrizione:');
			break;

		case "92":
			$ID = "id_ricovero";
			$Table = "tab_ricoveri";
			$Descrizione = "Ricovero";
			$ElencoCampi = array('ricovero' => 'Ricovero:',
														'note' => 'Descrizione:');
			break;

		case "93":
			$ID = "id_anticorpi";
			$Table = "tab_anticorpi";
			$Descrizione = "Anticorpi";
			$ElencoCampi = array('vdrl' => 'VDRL:',
														'vdrl_res' => 'Esito:',
														'tpha' => 'TPHA:',
														'tpha_res' => 'Esito:',
														'ab_tg' => 'Ab Tg:',
														'ab_tg_res' => 'Esito:',
														'ab_tiroide' => 'Ab Tiroide:',
														'ab_tiroide_res' => 'Esito:',
														'hiv' => 'HIV:',
														'hiv_res' => 'Esito:',
														'htlv1' => 'HTLV1:',
														'htlv1_res' => 'Esito:',
														'hcv' => 'HCV:',
														'hcv_res' => 'Esito:',
														'hbv' => 'HBV:',
														'hbv_res' => 'Esito:',
														'yo' => 'Yo:',
														'yo_res' => 'Esito:',
														'hu' => 'Hu:',
														'hu_res' => 'Esito:',
														'rhi' => 'Rhi:',
														'rhi_res' => 'Esito:');
			break;

		case "94":
			$ID = "id_mmse";
			$Table = "tab_mmse";
			$Descrizione = "MMSE";
			$ElencoCampi = array('tot_test' => 'Totale:',
														'tot_test_corr' => 'Totale corretto:');
			break;

		case "95":
			$ID = "id_bdi";
			$Table = "tab_bdi";
			$Descrizione = "BDI";
			$ElencoCampi = array('eseguito_da' => 'Test eseguito da:',
														'tot_test' => 'Totale:');
			break;

		case "96":
			$ID = "id_npi";
			$Table = "tab_npi";
			$Descrizione = "NPI";
			$ElencoCampi = array('tot_test' => 'Totale:');
			break;

		case "97":
			$ID = "id_fbi";
			$Table = "tab_fbi";
			$Descrizione = "FBI";
			$ElencoCampi = array('tot_test' => 'Totale:');
			break;

		case "98":
			$ID = "id_neoffi";
			$Table = "tab_neoffi";
			$Descrizione = "NEO-FFI";
			$ElencoCampi = array('tot_n' => 'Score N:',
														'tot_nc' => 'Score N (conv):',
														'tot_e' => 'Score E:',
														'tot_ec' => 'Score E (conv):',
														'tot_o' => 'Score O:',
														'tot_oc' => 'Score O (conv):',
														'tot_a' => 'Score A:',
														'tot_ac' => 'Score A (conv):',
														'tot_c' => 'Score C:',
														'tot_cc' => 'Score C (conv):');
			break;

		case "99":
			$ID = "id_mcgill";
			$Table = "tab_mcgill";
			$Descrizione = "McGill";
			$ElencoCampi = array('tot_test' => 'Totale:');
			break;

		case "100":
			$ID = "id_cbi";
			$Table = "tab_cbi";
			$Descrizione = "CBI";
			$ElencoCampi = array('tot_tempo' => 'Burden Tempo:',
														'tot_evolutivo' => 'Burden Evolutivo:',
														'tot_fisico' => 'Burden Fisico:',
														'tot_sociale' => 'Burden Sociale:',
														'tot_emotivo' => 'Burden Emotivo:',
														'tot_test' => 'Totale:');
			break;

		case "101":
			$ID = "id_deglutizione";
			$Table = "tab_deglutizione";
			$Descrizione = "Deglutizione";
			$ElencoCampi = array('bicchiere' => 'Prova del bicchiere d\'acqua ( sec ):',
														'disfagia' => 'Disfagia:',
													 'disf_note' => 'Note:',
														 		 'peg' => 'PEG:',
														'data_peg' => 'Data impianto:',
														'peg_note' => 'Note:');
			break;

		case "102":
			$ID = "id_fas";
			$Table = "tab_fas";
			$Descrizione = "FAS";
			$ElencoCampi = array('tot_f' => 'N. lettera "F":',
										  //~ 'tot_f_corr' => 'N. lettera "F" ( corr ):',
											 	   'tot_a' => 'N. lettera "A":',
											//~ 'tot_a_corr' => 'N. lettera "A" ( corr ):',
													 'tot_s' => 'N. lettera "S":',
										  //~ 'tot_s_corr' => 'N. lettera "S" ( corr ):',
											  'tot_test' => 'Totale:',
									 'tot_test_corr' => 'Totale ( corr ):');
			break;

		case "103":
			$ID = "id_fluenze";
			$Table = "tab_fluenze";
			$Descrizione = "Fluenze Semantiche";
			$ElencoCampi = array('tot_1' => 'Categoria 1:',
											 	   'tot_2' => 'Categoria 2:',
													 'tot_3' => 'Categoria 3:',
											  'tot_test' => 'Totale:',
									 'tot_test_corr' => 'Totale ( corr ):');
			break;
	}
?>

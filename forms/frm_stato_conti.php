<script type="text/javascript">
    function pippo (string, object, target)
    {
        xmlhttp = CreateAjaxRequestObject();
        IdTarget = document.getElementById(target);
        document.getElementById(target).style.width = object.style.width;

        xmlhttp.onreadystatechange = function()
        {
            if(xmlhttp.readyState == 4)
            {
                IdTarget.innerHTML = xmlhttp.responseText;
                var coord = GetObjCoord(object);
                var x = coord['x'];
                var y = coord['y'];
                MoveObj('lsearch', x, y, 'block');
            }
        }
      
        xmlhttp.open("GET", '../Function/live_search.php?str='
            + string + '&target=' + target + '&object=' + object.id, true);
        xmlhttp.send(null); 
    }

    function Expand2 (ContainerId, ImageId, DefaultH, ImgSrc1, ImgSrc2)
    {
        alert("expand2")
        xmlhttp = CreateAjaxRequestObject();

        if(xmlhttp.readyState == 4)
        {
            Expand(ContainerId, ImageId, DefaultH, ImgSrc1, ImgSrc2)
            IdTarget.innerHTML = xmlhttp.responseText;
        }
        
        xmlhttp.open("GET", '../Function/dett_conti.php', true);
        xmlhttp.send(null);
    }
</script>
<?php
	$root = substr($_SERVER['DOCUMENT_ROOT'], 0, -1);
	require_once ("$root/Function/Strings.php");
	require_once ("$root/Function/Db.php");
	require_once ("$root/Function/Debug.php");
	require_once ("$root/Function/DataTime.php");
	define ("PageLimit", 20);

	$SpDare = array(1, 2, 3, 9, 7, 6, 4, 5, 10, 8);
	$SpAvere = array(14, 16, 20, 17, 18, 15, 42, 43, 19);
	$condizione = array();
	$TotDare = 0;
	$TotAvere = 0;

	if (isset($_GET['del']))
	{
		/*
		 * DeleteRows ("tab_offerte", "id_ord = '" . $_GET['del'] . "'", $db);
		 * DeleteRows ("tab_dett_offerte", "id_orderta = '" . $_GET['del'] . "'", $db);
		 * $Result = "<label class=\"ok\">Offerta eliminata.</label>";
		 */
	}

	if (isset($_POST['ck_interval_reg']))
	{
		/*
		 * Se una delle date è vuota ma il campo e' stato flaggato
		 * le date mancanti diventano la data odierna
		 */

		if (trim($_POST['val_data_i_reg']) == "")
		{
			$_POST['val_data_i_reg'] = date('d/m/Y');
		}

		if (trim($_POST['val_data_f_reg']) == "")
		{
			$_POST['val_data_f_reg'] = date('d/m/Y');
		}

		$inizio =  Inverti_Data($_POST['val_data_i_reg'], "-", "/");
		$fine = Inverti_Data($_POST['val_data_f_reg'], "-", "/");
		array_push($condizione, "(data_reg >= '$inizio' AND data_reg <= '$fine')");
		if ($_POST['rad_tipo'] == "mastrini")
		{
			$interval_reg = " AND (data_reg >= '$inizio' AND data_reg <= '$fine')";
		}
	}

	if (isset($_POST['ck_interval_op']))
	{
		/*
		 * Se una delle date è vuota ma il campo e' stato flaggato
		 * le date mancanti diventano la data odierna
		 */

		if (trim($_POST['val_data_i_op']) == "")
		{
			$_POST['val_data_i_op'] = date('d/m/Y');
		}

		if (trim($_POST['val_data_f_op']) == "")
		{
			$_POST['val_data_f_op'] = date('d/m/Y');
		}

		$inizio =  Inverti_Data($_POST['val_data_i_op'], "-", "/");
		$fine = Inverti_Data($_POST['val_data_f_op'], "-", "/");
		array_push($condizione, "(data_doc >= '$inizio' AND data_doc <= '$fine')");
		if ($_POST['rad_tipo'] == "mastrini")
		{
			$interval_op = " AND (data_doc >= '$inizio' AND data_doc <= '$fine')";
		}
	}

	if (isset($_POST['ck_sottoconto']))
	{
		$val_sott = FindStr($_POST['val_sottoconto'] . "$", ") ", "$", 0, 0);
		if ($val_sott == "error")
		{
			$val_sott = $_POST['val_sottoconto'];
			$is_anag = "true";
		}
		$val_sott = ModoRicerca($val_sott, "tutto");
		$val_sott = ExpandSearch($val_sott);
		array_push($condizione, "(descr_sottoconto LIKE '$val_sott' OR ragione_sociale LIKE '$val_sott')");
		$descr_mastrino = " AND (descr_sottoconto LIKE '$val_sott' OR ragione_sociale LIKE '$val_sott')";
	}

	if (count($condizione) == 0)
	{
		array_push($condizione, "1");
	}

	foreach ($condizione as $key => $field)
	{
		if (($key+1) != count($condizione))
		{
			$campi .= $field . " AND ";
		}else{
			$campi .= $field;
		}
	}

	if ($_POST['rad_tipo'] == "mastrini")
	{
		$campi .= " GROUP BY id_sottoconto";
		$order = "descr_sottoconto, data_reg";
	}else{
		$campi .= " GROUP BY id_piano_conti";
		$order = "data_reg, id_piano_conti";
	}

	if (isset($_GET['page']))
	{
		$tot_res = count(GetRows ("view_piano_conti", $campi, $order, $db));

		switch ($_GET['page'])
		{
			case "all":
				// no limit;
				break;

			case "first":
				$start = 0;
				$order .= " LIMIT $start, " . PageLimit;
				break;

			case "last":
				$start = (intval($tot_res / PageLimit) * PageLimit);
				$order .= " LIMIT $start, " . PageLimit;
				break;

			case "next":
				if (($_SESSION['start'] + PageLimit) > $tot_res)
				{
					$start = (intval($tot_res / PageLimit) * PageLimit);
				}else{
					$start = ($_SESSION['start'] + PageLimit);
				}
				$order .= " LIMIT $start, " . PageLimit;
				break;

			case "prev":
				if (($_SESSION['start'] - PageLimit) < 0)
				{
					$start = 0;
				}else{
					$start = ($_SESSION['start'] - PageLimit);
				}
				$order .= " LIMIT $start, " . PageLimit;
				break;
		}

		$_SESSION['start'] = $start;
		//echo "Debug => start: $start <br />";
	}

	if (!empty($_POST))
	{
		$Risultati = GetRows ("view_piano_conti", $campi, $order, $db);

		 $_SESSION['qpdf']['cond'] = $condizione;
		$_SESSION['qpdf']['campi'] = $campi;
		  $_SESSION['qpdf']['ord'] = $order;
	}

	?>

<div id="search-form">
	<form method="post" action="<?php echo $_SERVER['REQUEST_URI']; ?>">
		<table class="form" style="width: 100%;">
			<tr>
				<th colspan="8">
					Seleziona i campi fra i quali ricercare:
				</th>
			</tr>

			<tr>
				<td>
					<label>Tipo Ricerca:</label>
					<input name="rad_tipo" type="radio" value="movimenti"
						<?php if ($_POST['rad_tipo'] == "movimenti")
						{
							echo "checked=\"checked\"";
						}else{
							echo 'checked="checked"';
						} ?>  />
						<label>Movimenti</label>

					<input name="rad_tipo" type="radio" value="mastrini"
						<?php if ($_POST['rad_tipo'] == "mastrini")
						{
							echo "checked=\"checked\"";
						} ?> />
						<label>Mastrini</label>
				</td>
			</tr>

			<tr>
				<td colspan="8">
					<input name="ck_interval_reg" type="checkbox" value="interval_reg"
						<?php if ($_POST['ck_interval_reg'] != "") { echo "checked=\"checked\""; } ?> />
						<label>Periodo Registrazione:</label>
					<input name="val_data_i_reg" type="text" style="width: 8em;" title="Data iniziale"
						onfocus="showCalendarControl(this);"
						value="<?php if (isset($_POST['val_data_i_reg'])) { echo $_POST['val_data_i_reg']; } ?>" />

					<input name="val_data_f_reg" type="text" style="width: 8em;" title="Data finale"
						onfocus="showCalendarControl(this);"
						value="<?php if (isset($_POST['val_data_f_reg'])) { echo $_POST['val_data_f_reg']; } ?>" />

					&nbsp;
					<input name="ck_interval_op" type="checkbox" value="interval_op"
						<?php if ($_POST['ck_interval_op'] != "") { echo "checked=\"checked\""; } ?> />
						<label>Periodo Documento:</label>
					<input name="val_data_i_op" type="text" style="width: 8em;" title="Data iniziale"
						onfocus="showCalendarControl(this);"
						value="<?php if (isset($_POST['val_data_i_op'])) { echo $_POST['val_data_i_op']; } ?>" />

					<input name="val_data_f_op" type="text" style="width: 8em;" title="Data finale"
						onfocus="showCalendarControl(this);"
						value="<?php if (isset($_POST['val_data_f_op'])) { echo $_POST['val_data_f_op']; } ?>" />
				</td>
			</tr>

			<tr>
				<th colspan="8">
					<input name="ck_sottoconto" type="checkbox" value="sottoconto"
						<?php if ($_POST['ck_sottoconto'] != "") { echo "checked=\"checked\""; } ?> />
						<label>Sottoconto:</label>
					<input name="val_sottoconto"  id="val_sottoconto" type="text" style="width: 34em;"
						onkeyup="Javascript: pippo(this.value, this, 'lsearch');"

						value="<?php if (isset($_POST['val_sottoconto'])) { echo $_POST['val_sottoconto']; } ?>" />

					<a onclick="Javascript: posta('0', 'home.php?act=stato_conti&amp;page=first'); void(0);"
						style="cursor: pointer;" title="Avvia la ricerca">
						<img src="img/find.png" alt="Avvia la ricerca" />
						Avvia Ricerca
					</a>

					&nbsp;
					<a onclick="Javascript: go('home.php?act=stato_conti'); void(0);"
						style="cursor: pointer;" title="Nuova Ricerca">
						<img src="img/cancel.png" alt="Nuova Ricerca" />
						Nuova Ricerca
					</a>
				</th>
			</tr>

			<tr><td colspan="8"><br /><!-- separatore !--></td></tr>

			<tr>
				<?php if (!empty($_POST)) { ?>
					<?php if ($tot_res > 0) { ?>
						<th colspan="7" style="text-align: left;">
							<label>Risultati Corrispondenti:</label>
							<label class="ok"><?php echo $tot_res; ?></label>

							&nbsp;
							<a onclick="Javascript: window.open('frm_pdf_stato_conti.php?tipo=<?php
								echo $_POST['rad_tipo']; ?>', '_blank'); void(0);"
								title="Esporta in Pdf" style="cursor: pointer;">
									<img src="img/pdf.png" alt="Pdf" /> Esporta
							</a>

							&nbsp;
							<a onclick="Javascript: posta('0', 'home.php?act=stato_conti&amp;page=first'); void(0);">
								<img src="img/first.png" alt="first" /> Inizio
							</a>

							&nbsp;
							<a onclick="Javascript: posta('0', 'home.php?act=stato_conti&amp;page=prev'); void(0);">
								<img src="img/prev.png" alt="prev" /> Prec
							</a>

							&nbsp;
							<a onclick="Javascript: posta('0', 'home.php?act=stato_conti&amp;page=next'); void(0);">
								<img src="img/next.png" alt="next" /> Succ
							</a>

							&nbsp;
							<a onclick="Javascript: posta('0', 'home.php?act=stato_conti&amp;page=last'); void(0);">
								<img src="img/last.png" alt="last" /> Fine
							</a>

							&nbsp;
							<a onclick="Javascript: posta('0', 'home.php?act=stato_conti&amp;page=all'); void(0);">
								<img src="img/all.png" alt="all" /> Tutti
							</a>
						</th>

						<td>
							<label>Visualizzati: </label>
							<label class="ok">
								<?php if ($start == 0) { echo ($start + 1); }else{ echo $start; } ?>
							</label>

							&nbsp;-&nbsp;

							<label class="ok">
								<?php if ((($start + PageLimit) <= $tot_res) && ($_GET['page'] != "all"))
								{
									echo ($start + PageLimit);
								}else{
									echo $tot_res;
								} ?>
							</label>
						</td>
					<?php }else{ ?>
						<td style="text-align: left;">
							<label class="err">Nessun risultato corrispondente.</label>
						</td>
					<?php } ?>
				<?php }else{ ?>
					<td></td>
				<?php } ?>
			</tr>
		</table>
	</form>
</div>

<?php if (isset($_POST) && (count($_POST) > 0)) { ?>
	<div id="search-result-5r">
		<?php if (count($Risultati) > 0) { ?>
			<table style="width: 100%;" class="dettaglio">
				<?php if ($_POST['rad_tipo'] == "movimenti") { ?>
					<tr>
						<th style="width: 150px;">N° - Data Movimento:</th>
						<th>N° Protocollo:</th>
						<th>N° Documento:</th>
						<th>Data Documento:</th>
						<th>Causale Op:</th>
						<th colspan="3"><br /></th>
					</tr>
					<!--<tr>
						<th><br /></th>
						<th>Sottoconto:</th>
						<th>Descrizione:</th>
						<th>Dare:</th>
						<th>Avere:</th>
						<th colspan="2"><label>Iva:</label></th>
					</tr>!-->
				<?php }else{ ?>
					<tr>
						<th colspan="5">Sottoconto:</th>
					</tr>
					<tr>
						<th>Num. e Data Reg:</th>
						<th>Causale:</th>
						<!--<th>Descrizione:</th> !-->
						<th>Dare:</th>
						<th>Avere:</th>
						<th>Saldo:</th>
						<th><br /></th>
					</tr>
				<?php } ?>
				<tr><th colspan="8"><hr /></th></tr>
			</table>

			<?php foreach ($Risultati as $key => $field) { ?>
				<div id="expander<?php echo $key; ?>" class="expander">
                    <table style="width: 100%;" class="dettaglio">
						<?php if ($_POST['rad_tipo'] == "movimenti") { ?>
							<!-- Intestazione movimento !-->
							<tr>
                                <td style="width: 20px; text-align: center;">
                                    <img id="img<?php echo $key; ?>" alt="img<?php echo $key; ?>"
                                         src="img/bott.png"
                                        onclick="Javascript: Expand('expander<?php echo $key; ?>', 'img<?php echo $key; ?>', '1.5em',
                                            'img/bott.png', 'img/top.png'); void(0);"
                                        title="Click per Visualizzare/Nascondere il dettaglio" />
                                </td>
								<td title="Numero e Data Registrazione" style="text-align: right; width: 150px;">
									<?php echo "(" . $field['id_piano_conti'] . ") - "
										. "<label>" . Inverti_Data($field['data_reg']) . "</label>&nbsp;";
									?>
								</td>

								<td title="N° Protocollo">
									<label>
										<?php if ($field['n_protocollo'] == "")
											{
												echo "-";
											}else{
												echo $field['n_protocollo'];
											} ?>
									</label>
								</td>

								<td title="N° Documento">
									<label><?php echo $field['cod_documento']; ?></label>
								</td>

								<td title="Data Documento">
									<label><?php echo Inverti_Data($field['data_doc']); ?></label>
								</td>

								<td title="Causale Operazione">
									<label><?php echo $field['tipo_doc']; ?></label>
								</td>

								<td colspan="2">
									<a href="home.php?act=piano_conti&amp;tomod=<?php echo $field['id_piano_conti']; ?>&amp;reset=0"
										title="Modifica Operazione" style="cursor: pointer;">
										<img src="img/edit.png" alt="edit" />
									</a>
								</td>
							</tr>
						<?php }else{ ?>
							<!-- Intestazione mastrino !-->
							<tr>
                                <td style="width: 20px; text-align: center;">
                                   <img id="img<?php echo $key; ?>" alt="img<?php echo $key; ?>"
                                         src="img/bott.png"
                                        onclick="Javascript: Expand('expander<?php echo $key; ?>', 'img<?php echo $key; ?>', '1.5em',
                                            'img/bott.png', 'img/top.png'); void(0);"
                                        title="Click per Visualizzare/Nascondere il dettaglio" />
                                </td>
								<td colspan="6" title="Sottoconto">
									<label>
										<?php
											if (($field['req_anag'] == 0) && ($_POST['rad_tipo'] == "movimenti"))
                                            {
												echo "(" . $field['codice_sottoconto']
													. ") " . htmlentities($field['ragione_sociale']);
											}else{
												echo "(" . $field['codice_sottoconto'] . ") "
													. ucfirst(htmlentities(strtolower($field['descr_sottoconto'])));
											} ?>
									</label>
								</td>
							</tr>
						<?php } ?>
					</table>

                    <table style="width: 100%;" class="dettaglio">
                        <?php if ($_POST['rad_tipo'] == "movimenti")
                        {
                            //<!-- Dettaglio movimento !-->
                            $DettaglioOp = GetRows ("view_piano_conti", "id_piano_conti = '"
                                . $field['id_piano_conti']. "'", "", $db);

                            foreach ($DettaglioOp as $key_dett => $field_dett)
                            {
                                $TotDare += $field_dett['importo_d'];
                                $TotAvere += $field_dett['importo_a']; ?>
                                <tr>
                                    <td style="width: 75px;"><br /></td>
                                    <td style="text-align: right;">
                                        <label>&bull;</label>
                                    </td>
                                    <td>
                                        <?php if ($field_dett['id_anagrafica'] != 0) {
                                            echo "(" . $field_dett['codice_sottoconto']
                                                . ") " . htmlentities($field_dett['ragione_sociale']);
                                        }else{
                                            echo "(" . $field_dett['codice_sottoconto'] . ") "
                                                . ucfirst(htmlentities(strtolower($field_dett['descr_sottoconto'])));
                                        } ?>
                                    </td>
                                    <td>
                                        <?php echo htmlentities($field_dett['descr']); ?>
                                    </td>

                                    <?php if ($field_dett['da'] == "d") { ?>
                                        <td style="text-align: center;" title="Importo Dare">
                                            <?php echo "<label class=\"err\">"
                                                . $field_dett['importo_d'] . "</label>"; ?>
                                        </td>
                                        <td style="text-align: center;" title="Importo Avere">-</td>
                                    <?php }else{ ?>
                                        <td style="text-align: center;" title="Importo Dare">-</td>
                                        <td style="text-align: center;" title="Importo Avere">
                                            <?php echo "<label class=\"ok\">"
                                                . $field_dett['importo_a'] . "</label>"; ?>
                                        </td>
                                    <?php } ?>

                                    <td colspan="2">
                                        <?php if ($field_dett['id_iva'] != "0")
                                        {
                                            echo $field_dett['desc_iva'];
                                         }else{
                                            echo "-";
                                        } ?>
                                    </td>
                                 </tr>
                            <?php } ?>

                            <tr><th colspan="8"><!-- Separatore !--><br /></th></tr>

                            <?php }else{
                            //<!-- Dettaglio mastrino !-->
                            $cond_mastr = "id_sottoconto = '" . $field['id_sottoconto']	. "' $descr_mastrino $interval_reg $interval_op";
                            $DettaglioConto = GetRows ("view_piano_conti", $cond_mastr, "data_reg", $db);

                            foreach ($DettaglioConto as $key_dett => $field_dett)
                            { ?>
                                <tr>
                                    <td style="text-align: right;">
                                        <?php echo "(" . $field_dett['id_piano_conti'] . ") - "
                                            . "<label>" . Inverti_Data($field_dett['data_reg'])
                                            . "</label>&nbsp;";
                                        ?>
                                    </td>
                                    <td>
                                        <?php echo  $field_dett['tipo_doc']; ?>
                                    </td>
                                    <!--<td>
                                        <?php //echo  $field_dett['descr']; ?>
                                    </td> !-->
                                    <td style="text-align: right;">
                                        <label class="err">
                                            <?php echo  $field_dett['importo_d'];
                                                $TotDare += $field_dett['importo_d']; ?>
                                        </label>
                                    </td>
                                    <td style="text-align: right;">
                                        <label class="ok">
                                            <?php echo  $field_dett['importo_a'];
                                                $TotAvere += $field_dett['importo_a']; ?>
                                        </label>
                                    </td>

                                    <td style="text-align: right;">
                                        <?php
                                            if (in_array($field_dett['id_conto'], $SpDare))
                                            {
                                                $Saldo = ($TotDare-$TotAvere);
                                            }else{
                                                $Saldo = ($TotAvere-$TotDare);
                                            }

                                            if ($Saldo < 0) { ?>
                                                <label class="err">
                                                    <?php printf("%.2f", $Saldo); ?>
                                                </label>
                                            <?php }else{ ?>
                                                <label class="ok">
                                                    <?php printf("%.2f", $Saldo); ?>
                                                </label>
                                            <?php } ?>
                                    </td>
                            <?php } ?>

                                <tr><td colspan="8"><br /><!-- separatore !--></td></tr>
                            <?php } ?>
                    </table>
				</div>
					<?php } ?>
					<!-- Totali !-->
				<table style="width: 100%;">
					<tr><th colspan="8"><hr /></th></tr>
					<tr>
						<th colspan="3" style="text-align: right;">
							Totali:
						</th>
						<th style="text-align: right;">
							<label class="err">
								<?php printf("%.2f", $TotDare); ?>
							</label>
						</th>

						<th style="text-align: right;">
							<label class="ok">
								<?php printf("%.2f", $TotAvere); ?>
							</label>
						</th>

						<th style="text-align: right;">
							<?php
								//~ if (in_array($field_dett['id_conto'], $SpDare))
								//~ {
									$Saldo = ($TotDare-$TotAvere);
								//~ }else{
									//~ $Saldo = ($TotAvere-$TotDare);
								//~ }
								if ($Saldo < 0) { ?>
									<label class="err">
										<?php printf("%.2f", $Saldo); ?>
									</label>
								<?php }else{ ?>
									<label class="ok">
										<?php printf("%.2f", $Saldo); ?>
									</label>
								<?php } ?>
						</th>
					</tr>
				</table>
			<?php }	?>
	</div>
<?php } ?>

<div id="lsearch" class="form" style="font-weight: normal;"
	onmouseup="javascript: document.getElementById('lsearch').style.display = 'none';">
</div>

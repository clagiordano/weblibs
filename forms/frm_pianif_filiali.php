<?php
	$tot_res 				= -1;
	$tot_pdl 				= 0;
	$tot_let_sp 		= 0;
	$tot_prt_lan		= 0;

	if (!isset($_GET['tomod']))
	{
		$_GET['tomod'] = "";
	}

	if (is_numeric($_GET['tomod']) && is_numeric($_GET['key'])
		&& !empty($_POST))
	{
		$Valori = array('id_stato' => $_POST['id_stato_call' . $_GET['key']]);
		$Status = $d->UpdateRow ($Valori, "", "tab_filiali",
			"id_filiale = '" . $_GET['tomod'] . "'");
	}

	$Stato = $d->GetRows ("*", "tab_stato_call", "", "", "stato_call", 1);
	$Ditte = $d->GetRows ("*", "tab_ditte", "", "", "ragione_sociale");
	$Province = $d->GetRows ("distinct(prov)", "tab_filiali", "", "", "prov");
	if (isset($_POST['prov']))
	{
		$Citta = $d->GetRows ("distinct(citta)", "tab_filiali",
			"prov like '" . $_POST['prov'] . "'", "", "citta");
	}else{
		$Citta = array();
	}
	if (isset($_POST['citta']))
	{
		$Filiali = $d->GetRows ("id_filiale, concat(den, \" (\", cod_fil, \")\") as filiale",
			"tab_filiali", " citta like '" . addslashes($_POST['citta'])
			. "' AND prov like '" . $_POST['prov'] . "'", "",	"den");
	}else{
		$Filiali = array();
	}

	if (!empty($_POST))
	{
		if (trim($_POST['cod_fil']) == "")
		{
			$_POST['cod_fil'] = "%";
		}

		$where = " prov like '" . $_POST['prov'] . "'";
		$where .= " AND citta like '" . addslashes($_POST['citta']) . "'";
		$where .= " AND id_stato like '" . $_POST['id_stato_call'] . "'";
		$where .= " AND id_filiale_f like '" . $_POST['id_filiale'] . "'";
		$where .= " AND cod_fil like '%" . $_POST['cod_fil'] . "%'";
		$where .= " AND id_ditta like '" . $_POST['id_ditta'] . "'";

		if (isset($_POST['ck_data_att']))
		{
			/*
			 * Se una delle date è vuota ma il campo e' stato flaggato
			 * le date mancanti diventano la data odierna
			 */

			if (trim($_POST['data_att_i']) == "")
			{
				$_POST['data_att_i'] = date('d/m/Y');
			}

			if (trim($_POST['data_att_f']) == "")
			{
				$_POST['data_att_f'] = date('d/m/Y');
			}

			$inizio =  $d->Inverti_Data($_POST['data_att_i']);
			$fine = $d->Inverti_Data($_POST['data_att_f']);
			$where .= " AND (data_pianif >= '$inizio' AND data_pianif <= '$fine')";
		}

		if (trim($_POST['den']) == "")
		{
			$_POST['den'] = "%";
		}else{
			$where .= " AND den like '%" . $d->ExpandSearch($_POST['den']) . "%'";
		}

		$tot_res = count($d->GetRows ("id_request", "view_pianificazione",
			$where, "", "den"));
		$order = $d->PageSplit($_GET['page'], $tot_res);
		$Results = $d->GetRows ("*", "view_pianificazione", $where, "",
			"data_pianif, den, citta $order", 1);
		$_SESSION['where'] = $where;
		$_SESSION['order'] = $order;
	}

	if (isset($_GET['rex']) && ($_GET['rex'] == "1"))
	{
		// ri assegno il precedente array con i risultati:
		$tot_res = count($d->GetRows ("id_request", "view_pianificazione",
			$where, "", "den"));
		$where = $_SESSION['where'];
		$order = $_SESSION['order'];
		$Results = $d->GetRows ("*", "view_pianificazione", $where, "",
			"data_pianif, den, citta $order");
	}

?>

<div id="search-form">
	<form method="post" action="?act=pianif_fil">
    <table class="form" style="width: 100%;">
			<tr>
				<th colspan="2">Seleziona i campi fra i quali ricercare:</th>
			</tr>
			<tr>
				<td colspan="2">
					<label>Codice Filiale:</label>
					<input name="cod_fil" type="text" onKeyPress="return SubmitEnter(this,event);"
						style="width: 7em;"
						value="<?php if (isset($_POST['cod_fil'])) { echo $_POST['cod_fil']; } ?>" />

					&nbsp;
					<label>Denominazione:</label>
					<input name="den" type="text" onKeyPress="return SubmitEnter(this,event);"
						style="width: 11em;"
						value="<?php if (isset($_POST['den'])) { echo $_POST['den']; } ?>" />

					&nbsp;
					<select name="id_stato_call"
						onchange="Javascript: posta('0',
							'?act=pianif_fil&amp;page=first'); void(0);" >
							<option value="%">Tutti gli stati</option>
							<?php foreach ($Stato as $key => $field) { ?>
							<option value="<?php echo $field['id_stato_call']; ?>"
								<?php if (isset($_POST['id_stato_call'])
									&& ($_POST['id_stato_call'] == $field['id_stato_call']))
									{ echo "selected=\"selected\""; } ?> >
								<?php echo htmlentities($field['stato_call']); ?>
							</option>
							<?php } ?>
					</select>

					&nbsp;
					<select name="id_ditta"
						onchange="Javascript: posta('0', '?act=pianif_fil&amp;page=first'); void(0);" >
							<option value="%">Tutte le competenze</option>
							<?php foreach ($Ditte as $key => $field) { ?>
							<option value="<?php echo $field['id_ditta']; ?>"
								<?php if (isset($_POST['id_ditta'])
									&& ($_POST['id_ditta'] == $field['id_ditta']))
									{ echo "selected=\"selected\""; } ?> >
								<?php echo htmlentities($field['ragione_sociale']); ?>
							</option>
							<?php } ?>
					</select>
				</td>
			</tr>

			<tr>
				<td colspan="2">
					<label>Periodo Attività:</label>
					<input name="ck_data_att" type="checkbox" value="interval_att"
						<?php if (isset($_POST['ck_data_att'])
							&& ($_POST['ck_data_att'] != "")) { echo "checked=\"checked\""; } ?> />
							<input name="data_att_i" type="text" style="width: 7em;"
								title="Data iniziale"
						onfocus="showCalendarControl(this);"
						value="<?php if (isset($_POST['data_att_i'])) { echo $_POST['data_att_i']; } ?>" />

					<input name="data_att_f" type="text" style="width: 7em;" title="Data finale"
						onfocus="showCalendarControl(this);"
						value="<?php if (isset($_POST['data_att_f'])) { echo $_POST['data_att_f']; } ?>" />

					&nbsp;
					<label>Localit&agrave;:</label>
					<select name="prov"
						onchange="Javascript: posta('0', '?act=pianif_fil&amp;page=first'); void(0);" >
						<option value="%">Tutte le province</option>
						<?php foreach ($Province as $key => $field) { ?>
							<option value="<?php echo $field['prov']; ?>"
								<?php if (isset($_POST['prov'])
									&& ($_POST['prov'] == $field['prov']))
									{ echo "selected=\"selected\""; }?> >
								<?php echo $field['prov']; ?>
							</option>
						<?php } ?>
					</select>

					&nbsp;
					<select name="citta"
						onchange="Javascript: posta('0', '?act=pianif_fil&amp;page=first'); void(0);" >
						<option value="%">Tutte le citt&agrave;</option>
						<?php foreach ($Citta as $key => $field) { ?>
							<option value="<?php echo $field['citta']; ?>"
								<?php if ($_POST['citta'] == $field['citta'])
									{ echo "selected=\"selected\""; }?> >
								<?php echo $field['citta']; ?>
							</option>
						<?php } ?>
					</select>

					&nbsp;
					<select name="id_filiale"
						onchange="Javascript: posta('0', '?act=pianif_fil&amp;page=first'); void(0);" >
						<option value="%">Tutte le filiali</option>
						<?php foreach ($Filiali as $key => $field) { ?>
							<option value="<?php echo $field['id_filiale']; ?>"
								<?php if ($_POST['id_filiale'] == $field['id_filiale'])
									{ echo "selected=\"selected\""; }?> >
								<?php echo $field['filiale']; ?>
							</option>
						<?php } ?>
					</select>
				</td>
			</tr>

			<?php $d->ShowResultBar($tot_res, "home.php?act=pianif_fil"); ?>

		</table>
	</form>
</div>

<div id="search-result-5r">
	<form method="post" action="?act=pianif_fil">
		<?php if ($tot_res > 0) { ?>
			<table class="dettaglio" style="width: 100%;">
			<tr>
				<th>Data:</th>
				<th title="Codice Request">Cod. Request:</th>
				<th>Città:</th>
				<th>Filiale:</th>
				<th title="Pdl da spedire">Pdl:</th>
				<th title="Spedizione Lettori">Lett:</th>
				<th title="Spedizione Stampanti">Prt Lan:</th>
				<th>Comp:</th>
				<th>Srv:</th>
				<th>Stato Srv:</th>
				<th>Stato Fil:</th>
			</tr>
			<tr><th colspan="16"><hr /></th></tr>
			<?php foreach ($Results as $key => $field) {
				$request = $d->GetRows ("id_request, request, sp_pdl, rit_pdl, qi_lett, i_server, sp_lett, sp_srv_new, sos_prt_lan",
				"tab_request", "id_filiale = '" . $field['id_filiale_f'] . "'");
				$id_request = $request[0]['id_request'];
				$color = $field['color'];

				$check_full = CheckDettaglioRequest($field['id_filiale_f']);
				$check = $check_full['consuntivo'];

				if (($request[0]['i_server'] != "")
					&& ($request[0]['sp_srv_new'] == 0))
				{
					$srv = "OLD";
				}elseif (($request[0]['i_server'] == "")
					&& ($request[0]['sp_srv_new'] != 0))
				{
					$srv = "NEW";
				}else{
					$srv = "-";
				}

				$tot_pdl 	 	 += ($request[0]['sp_pdl'] - $check['sp_pdl']);
				$tot_let_sp  += ($request[0]['sp_lett'] - $check['sp_lett']);
				$tot_prt_lan += ($request[0]['sos_prt_lan'] - $check['sos_prt_lan']);

				$dvk = $d->GetRows ("stato_mch, color", "view_dvk",
					"id_request = '$id_request'", "", "", 1);

				if (empty($dvk[0]['color']))
				{
					$color_srv = "";
					$stato_srv = "-";
				}else{
					$color_srv = $dvk[0]['color'];
					$stato_srv = $dvk[0]['stato_mch'];
				}
			?>
			<tr <?php if ($key % 2) { echo 'id="res-row"'; } ?> >
				<td><?php echo $d->Inverti_Data($field['data_pianif']); ?></td>
				<td><?php echo wordwrap($request[0]['request'], 10, "<br />"); ?></td>
				<td>
					<?php echo htmlentities(ucfirst(strtolower($field['citta'])))
						. " (" . $field['prov'] . ")"; ?>
				</td>
				<td>
					<?php echo htmlentities(ucfirst(strtolower($field['den']))); ?>
				</td>

				<td style="text-align: center;"
					title="Pdl Nuovi: Effettivi / Richiesti (Utenti)"
					class="<?php if ($check['sp_pdl'] == $request[0]['sp_pdl'])
						{ echo "ok"; }else{ echo "err"; }; ?>">
					<?php echo $check['sp_pdl'] . " / " . $request[0]['sp_pdl']
						. " (" . $field['utenti'] . ")"; ?>
				</td>

				<td style="text-align: center;"
					title="Lettori Nuovi: Effettivi / Richiesti"
					class="<?php if ($check['sp_lett'] == $request[0]['sp_lett'])
					{ echo "ok"; }else{ echo "err"; }; ?>">
					<?php echo $check['sp_lett'] . " / " . $request[0]['sp_lett']; ?>
				</td>

				<td style="text-align: center;"
					title="Stampanti di rete nuove: Effettive / Richieste"
					class="<?php if ($check['sos_prt_lan'] == $request[0]['sos_prt_lan'])
					{ echo "ok"; }else{ echo "err"; }; ?>">
					<?php echo $check['sos_prt_lan'] . " / " . $request[0]['sos_prt_lan']; ?>
				</td>

				<td>
					<?php echo $field['ragione_sociale']; ?>
				</td>

				<td>
					<?php echo $srv; ?>
				</td>

				<td <?php echo "style=\"color: $color_srv ! important; font-weight: bold\""; ?>>
					<?php echo $stato_srv; ?>
				</td>

				<td <?php echo "style=\"color: $color ! important; font-weight: bold\""; ?>>
					<?php echo $field['stato_call']; ?>
				</td>

				<td>
					<a onclick="Javascript: ShowFormDiv('frm_filiali.php?tomod=<?php
						echo $field['id_filiale_f']; ?>',
						'200px', '200px', '600px', '270px', 'popup'); void(0);"
						 title="Modifica Dati Pianificazione">
						<img src="/Images/Links/edit.png" alt="edit" />
					</a>

					<?php if (is_numeric($id_request)) { ?>
						<a onclick="Javascript: window.open('frm_pdf_dett_request.php?rid=<?php echo $id_request; ?>', '_blank'); void(0);"
							title="Stampa Riepilogo Request">
							<img src="/Images/Links/pdf.png" alt="pdf" />
						</a>

						<a onclick="Javascript: window.open('frm_pdf_ritiri.php?rid=<?php echo $id_request; ?>', '_blank'); void(0);"
							title="Stampa Dettaglio Ritiri">
							<img src="/Images/Links/pdf.png" alt="pdf" />
						</a>

						<a onclick="Javascript: go('home.php?act=gest_req&amp;tomod=<?php echo $id_request; ?>'); void(0);"
							title="Modifica Dati Request">
							<img src="/Images/Links/read.png" alt="read" />
						</a>

						<a onclick="Javascript: go('home.php?act=rollout&amp;reset=0&amp;tomod=<?php echo $id_request; ?>'); void(0);"
							title="Compila Dettaglio Request">
							<img src="/Images/Links/arch.png" alt="arch" />
						</a>
					<?php }else{ ?>
						<img src="/Images/Links/pdfdis.png" alt="pdf" title="Non disponibile"/>
						<img src="/Images/Links/pdfdis.png" alt="pdf" title="Non disponibile"/>
						<img src="/Images/Links/readdis.png" alt="readdis" title="Non disponibile"/>
						<img src="/Images/Links/archdis.png" alt="arcdis" title="Non disponibile"/>
					<?php } ?>
				</td>
			</tr>
		<?php } ?>
		<tr><th colspan="16"><hr /></th></tr>
		<tr>
			<th colspan="16">
				<?php echo $tot_pdl; ?> pdl da spedire,
				&nbsp;<?php echo $tot_let_sp; ?> lettori assegni da spedire,
				&nbsp;<?php echo $tot_prt_lan; ?> stampanti di rete da spedire.
			</th>
		</tr>
		</table>
	<?php } ?>
	</form>
</div>






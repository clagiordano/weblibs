<?php error_reporting(E_ALL);

	if (!isset($_GET['tomod']))
	{
		unset($_SESSION['offerta']['tomod']);
		$_GET['tomod'] = "";
	}else{
		$_SESSION['offerta']['tomod'] = $_GET['tomod'];
	}

	if (isset($_GET['reset']))
	{
		unset($_SESSION['offerta']);
	}

	if (isset($_GET['del']))
	{
		if (is_numeric($_GET['del']))
		{
			unset($_SESSION['offerta']['accessori'][$_GET['del']]);
		}else{
			unset($_SESSION['offerta']['macchina'][$_GET['del']]);
		}

		$_SESSION['offerta']['modificata'] = "y";
	}

	$LinkL = Array();
	array_push($LinkL, array('link' => "&nbsp;<a onclick=\"Javascript: window.open('frm_esporta_pdf.php', '_blank'); void(0);\" "
		. "title=\"Stampa Dettaglio Ritiri\">"
		. "<img src=\"/Images/Links/pdf.png\" alt=\"pdf\" />Esporta in pdf</a>"));

	$TotaleOff = 0;
	$Componenti = array('alim' => 'Alimentatore',
											'case' => 'Case',
												'mb' => 'Motherboard',
											 'cpu' => 'Cpu',
											 'hdd' => 'Hard Disk',
											 'ram' => 'Ram',
											 'vga' => 'Scheda Video',
											'mast' => 'Masterizzatore',
											'lett' => 'Lettore',
										 'mcard' => 'Lettore M.Card',
											 'mon' => 'Monitor',
											'tast' => 'Tastiera',
											 'mou' => 'Mouse',
											'cass' => 'Casse');

	if (isset($_SESSION['offerta']['id_anagrafica']))
	{
		$Anagrafica = $d->GetRows ("*", "tab_anagrafica", "id_anag = '"
			. $_SESSION['offerta']['id_anagrafica'] . "'");
	}

	if (isset($_GET['del_rif']))
	{
		unset($_SESSION['offerta']['id_anagrafica']);
		//~ $_SESSION['offerta']['modificata'] = "y";
	}

	if (!isset($_SESSION['offerta']['assemblaggio']))
	{
		//~ echo "Debug => non c'e' asse in sessione lo setto a 0 <br />";
		$_SESSION['offerta']['assemblaggio'] = 0;
	}

	if (!isset($_SESSION['offerta']['macchina']))
	{
		$_SESSION['offerta']['macchina'] = array();
	}


	if (isset($_GET['asse']))
	{
		//~ echo "Debug => esiste il get di asse, imposto asse in sessione = ".$_GET['asse']." <br />";
		$_SESSION['offerta']['assemblaggio'] = $_GET['asse'];
		//~ $_SESSION['offerta']['modificata'] = "y";
	}


	if (isset($_GET['tomod']) && is_numeric($_GET['tomod']) && empty($_POST))
	{
		unset($_SESSION['offerta']);

		$opt_off = $d->GetRows ("*", "tab_offerte",
			"id_off = '" . $_GET['tomod'] . "'");
		//~ echo "Debug => leggo i parametri dell'offerta <br />";
		$_SESSION['offerta']['assemblaggio'] 							= $opt_off[0]['asse'];
		$_SESSION['offerta']['offerta_def'] 							= $opt_off[0]['tot'];
		$_SESSION['offerta']['offerta']['id_anagrafica'] 	= $opt_off[0]['id_cliente'];
		$_SESSION['offerta']['img-stp'] 									= $opt_off[0]['img1'];
		$_SESSION['offerta']['img-case'] 									= $opt_off[0]['img2'];
		$_SESSION['offerta']['img-monit'] 								= $opt_off[0]['img3'];

		$componenti = $d->GetRows ("*", "tab_dett_offerte",
			"id_offerta = '" . $_GET['tomod'] . "' AND comp != 'acc'");

		foreach ($componenti as $key => $field)
		{
			$dett_articolo = $d->GetRows ("*", "tab_prodotti",
				"cod_forn = '" . $field['cod_forn'] . "'", "descr");

			$_SESSION['offerta']['macchina'][$field['comp']]['id'] = $field['id_articolo'];
			$_SESSION['offerta']['macchina'][$field['comp']]['desc'] = $dett_articolo[0]['descr'];
			$_SESSION['offerta']['macchina'][$field['comp']]['prz'] = $field['prz'];
			$_SESSION['offerta']['macchina'][$field['comp']]['disp'] = $dett_articolo[0]['disp'];
			$_SESSION['offerta']['macchina'][$field['comp']]['cod'] = $dett_articolo[0]['cod_forn'];
			$_SESSION['offerta']['macchina'][$field['comp']]['qta'] = $field['qta'];
		}

		$accessori = $d->GetRows ("*", "tab_dett_offerte", "id_offerta = '"
			. $_GET['tomod'] . "' AND comp = 'acc'", "");
		foreach ($accessori as $key => $field)
		{
			$dett_accessorio = $d->GetRows ("*", "tab_prodotti", "cod_forn = '"
				. $field['cod_forn'] . "'", "descr");

			array_push($_SESSION['accessori'],
				array("id" => $field['id_articolo'],
						"desc" => $dett_accessorio[0]['descr'],
						 "prz" => $field['prz'],
						"disp" => $dett_accessorio[0]['disp'],
						 "cod" => $dett_accessorio[0]['cod_forn'],
						 "qta" => $field['qta']));
		}

		$_SESSION['offerta']['tomod'] = $_GET['tomod'];
	}

	if (isset($_POST['offerta_def']))
	{
		$_SESSION['offerta_def'] = $_POST['offerta_def'];
		$_SESSION['offerta']['modificata'] = "y";
	}


	if (isset($_SESSION['offerta']['macchina']))
	{
		foreach ($_SESSION['offerta']['macchina'] as $elem )
		{
			$TotaleOff += $elem['prezzo'];
		}
	}

	if (isset($_SESSION['offerta']['accessori']))
	{
		foreach ($_SESSION['offerta']['accessori'] as $elem )
		{
			$TotaleOff += $elem['prezzo'];
		}
	}

	if (isset($_GET['immagini']))
	{
		if ($_FILES['img-stp']['name'] != "")
		{
			copy($_FILES['img-stp']['tmp_name'], "upload/" . $_FILES['img-stp']['name']);
			$_SESSION['img-stp'] = "upload/" . $_FILES['img-stp']['name'];
		}

		if ($_FILES['img-case']['name'] != "")
		{
			copy($_FILES['img-case']['tmp_name'], "upload/" . $_FILES['img-case']['name']);
			$_SESSION['img-case'] = "upload/" . $_FILES['img-case']['name'];
		}

		if ($_FILES['img-monit']['name'] != "")
		{
			copy($_FILES['img-monit']['tmp_name'], "upload/" . $_FILES['img-monit']['name']);
			$_SESSION['img-monit'] = "upload/" . $_FILES['img-monit']['name'];
		}
	}

	if (isset($_GET['del_img']))
	{
		unset($_SESSION['img-case']);
		unset($_SESSION['img-monit']);
		unset($_SESSION['img-stp']);

		$result_img = "<label class=\"ok\">Immagini rimosse dall'offerta.</label>";
	}

	if (isset($_GET['salva']))
	{
		if ($_GET['salva'] == "modifica")
		{

			$Status = $d->DeleteRows ("tab_dett_offerte", "id_offerta = '"
				. $_GET['tomod'] . "' AND comp != 'acc'");

			$Status = $d->DeleteRows ("tab_dett_offerte", "id_offerta = '"
				. $_GET['tomod'] . "' AND comp = 'acc'");
		}

		//~ if ($_SESSION['assemblaggio'] == 0)
		//~ {
			//~ $Assemblaggio = (35*1.2);
			//~ $f_asse = 0;
		//~ }else{
			//~ $Assemblaggio = 0;
			//~ $f_asse = 1;
		//~ }

		$TotaleOff = sprintf("%.2f", ((($TotaleOff*1.3)*1.2)+$Assemblaggio));

		if (isset($_SESSION['offerta_def']) && ($_SESSION['offerta_def'] != $TotOfferta))
		{
			$TotaleOff = $_SESSION['offerta_def'];
		}

		$Valori = array('data' => date("Y-m-d H:i:s"),
										 'tot' => $TotaleOff,
										'asse' => $_SESSION['assemblaggio'],
							'id_cliente' => $_SESSION['offerta']['id_anagrafica'],
										'img1' => $_SESSION['img-stp'],
										'img2' => $_SESSION['img-case'],
										'img3' => $_SESSION['img-monit'],
							 'id_utente' => GetIdUser($_SESSION['rm_user'], $db));

		//~ echo "Debug => get salva: |".$_GET['salva'] ."| <br />";
		//~ Opzioni offerta
		if ($_GET['salva'] == "salva")
		{
			//~ echo "Debug => salvo la nuova offerta <br />";
			$Status = $d->SaveRow ($Valori, "", "tab_offerte");
			$result = $Status[0];
			$id_offerta = $Status[1];
		}else{
			//~ echo "Debug => Aggiorna i dati dell'offerta <br />";
			$Status = $d->UpdateRow ($Valori, "", "tab_offerte",
				"id_off = '" . $_GET['tomod'] . "'");
			$id_offerta = $_GET['tomod'];
			$result = $Status;
		}

		//~ Macchina
		foreach ($_SESSION['macchina'] as $key => $field)
		{
			$Valori = array('cod_forn' => $field['cod'],
													 'prz' => $field['prz'],
													 'qta' => $field['qta'],
													'comp' => $key,
													'disp' => $field['disp'],
										'id_offerta' => $id_offerta);
			$Status = $d->SaveRow ($Valori, "", "tab_dett_offerte");
		}

		//~ Accessori
		foreach ($_SESSION['accessori'] as $key => $field)
		{
			$Valori = array('cod_forn' => $field['cod'],
													 'prz' => $field['prz'],
													 'qta' => $field['qta'],
													'comp' => "acc",
													'disp' => $field['disp'],
									  'id_offerta' => $id_offerta);
			$Status = $d->SaveRow ($Valori, "", "tab_dett_offerte");
		}
		//~ echo "Debug => result: $result <br />";
	}

?>

<div id="search-results">
	<form method="post" action="<?php echo $_SERVER['REQUEST_URI']; ?>">
		<table class="form" style="width: 100%;">

			<?php $d->ShowEditBar("Offerta Attrezzature Informatiche: ",
				$_GET['tomod'], "new,save", $_GET['act'], 8, 0, "#", $LinkL); ?>

			<tr>
				<td style="text-align: right">
					<label>Totale Offerta:</label>
					<label class="err">
						<?php
							if ($_SESSION['offerta']['assemblaggio'] == 0)
							{
								$Assemblaggio = (35*1.2);
							}else{
								$Assemblaggio = 0;
							}
							$TotaleOff = sprintf("%.2f", ((($TotaleOff*1.3)*1.2)+$Assemblaggio));
							echo  $TotaleOff; ?>
					</label>

					&nbsp;
					<a onclick="Javascript: document.forms[0].offerta_def.value = '<?php
						echo $TotaleOff; ?>'; void(0);" style="cursor: pointer;">
						<img src="/Images/Links/first.png" alt="first"
							title="Offerta definitiva come totale." />
					</a>

					<input name="offerta_def" type="text" size="10"
						value="<?php if(isset($_SESSION['offerta_def']))
							{ echo $_SESSION['offerta_def']; }else{
								echo $TotaleOff; } ?>" />

					<a onclick="Javascript: posta('0', 'home.php?act=assembla<?php
						if (isset($_GET['tomod'])) { echo "&amp;tomod=" . $_GET['tomod']; } ?>'); "
						style="cursor: pointer;" >
						<img src="/Images/Links/apply.png" alt="apply"
							title="Applica variazione offerta." />
					</a>
				</td>
			</tr>
		</table>
	</form>
</div>

<div id="search-result">
	<table class="dettaglio" style="width: 100%;">
		<tr>
			<th>
				<fieldset>
					<legend>Fase 1 - Selezione Cliente:</legend>
					<label>Riferimento Cliente:</label>
					<a onclick="javascript: ShowFormDiv('frm_ricerca_avanzata.php?subact=offerta',
						'150px', '150px', '870px', '350px', 'popup');" >
						<?php if (!isset($_SESSION['offerta']['id_anagrafica'])) { ?>
							<img src="/Images/Links/select.png" alt="select" />
							Seleziona cliente
						<?php }else{
							echo $Anagrafica[0]['ragione_sociale']; ?>

						<a onclick="Javascript: go_conf('Eliminare il riferimento al cliente?',
						'?act=assembla&amp;del_rif=0<?php if (isset($_GET['tomod']))
							{ echo "&amp;tomod=" . $_GET['tomod']; } ?>',
							'?act=assembla<?php if (isset($_GET['tomod']))
							{ echo "&amp;tomod=" . $_GET['tomod']; } ?>');">
							<img src="/Images/Links/cancel.png" alt="del_rif" title="Elimina riferimento cliente" />
						</a>
						<?php } ?>
					</a>
				</fieldset>
			</th>
		</tr>

		<tr><th colspan="8"><br /></th></tr>

		<tr>
			<th>
				<fieldset>
					<legend>Fase 2 - Selezione Componenti:</legend>
					<table style="width: 100%;" class="dettaglio">
						<tr>
							<th colspan="8" style="font-style: italic ! important;">
								Componenti Computer:
							</th>
						</tr>
						<tr>
							<th colspan="8"></th>
						</tr>

						<tr>
							<th style="width: 90px;">
								<label>Componente:</label>
							</th>
							<th style="width: 40px; text-align: center;">
								<label>Art:</label>
							</th>
							<th>
								<label>Descrizione:</label>
							</th>
							<th style="text-align: center;">
								<label>Qta:</label>
							</th>
							<th style="text-align: center;">
								<label>Prz:</label>
							</th>
							<th style="text-align: center;" title="Disponibilità Attuale">
								<label>DA:</label>
							</th>
							<?php if (isset($_GET['tomod'])) { ?>
								<th style="text-align: center;" title="Disponibilità Precedente">
									<label>DP:</label>
								</th>
							<?php } ?>
							<th>&nbsp;</th>
						</tr>

						<?php foreach ($Componenti as $key => $field) { ?>
							<tr>
								<td style="text-align: right;">
									<?php echo $field . ":"; ?>
								</td>

								<td style="text-align: center;">
									<?php if(isset($_SESSION['offerta']['macchina'][$key]))
									{
										echo $_SESSION['offerta']['macchina'][$key]['cod_forn'];
									}else{
										echo "-";
									} ?>
								</td>

								<td style="vertical-align: top;">
									<a onclick="Javascript: ShowFormDiv('frm_ricerca_articoli_off.php?pezzo=<?php echo $key; ?>&amp;subact=offerta',
										'150px', '200px', '850px', '320px', 'popup'); void(0);">
										<img src="/Images/Links/select.png" alt="select" />
										<?php if(isset($_SESSION['offerta']['macchina'][$key]))
										{
											echo htmlentities($_SESSION['offerta']['macchina'][$key]['modello']);
										}else{
											echo "Seleziona componente...";
										} ?>
									</a>
								</td>

								<td style="text-align: center;">
									<?php if(isset($_SESSION['offerta']['macchina'][$key]))
									{
										echo $_SESSION['offerta']['macchina'][$key]['qta'];
									}else{
										echo "-";
									} ?>
								</td>

								<td style="text-align: center;">
									<?php if(isset($_SESSION['offerta']['macchina'][$key]))
									{
										echo $_SESSION['offerta']['macchina'][$key]['prezzo'];
									}else{
										echo "-";
									} ?>
								</td>

								<td style="text-align: center;">
									<label class="<?php if ($_SESSION['offerta']['macchina'][$key]['disp'] > 0)
										{
											echo "ok";
										}else{
											echo "err\" title=\"Articolo non più disponibile\"";} ?>" >
										<?php if(isset($_SESSION['offerta']['macchina'][$key]))
										{
											echo $_SESSION['offerta']['macchina'][$key]['disp'];
										}else{
											echo "-";
										} ?>
									</label>
								</td>

								<?php if (isset($_GET['tomod']) && ($_GET['tomod'] != "")) {
									$where_art = "id_offerta = '" . $_GET['tomod']
										. "' AND cod_forn = '" . $_SESSION['offerta']['macchina'][$key]['cod_forn'] . "'";
									$articolo = $d->GetRows ("*", "tab_dett_offerte", $where_art); ?>
									<td style="text-align: center;" >
										<label class="<?php if ($articolo[0]['disp'] > 0) {
											echo "ok"; }else{ echo "err"; } ?>">
											<?php if (isset($articolo[0]['disp']))
												{ echo $articolo[0]['disp']; } ?>
										</label>
								</td>
								<?php } ?>

								<th style="text-align: center;">
									<?php if(isset($_SESSION['offerta']['macchina'][$key])) { ?>
										<a href="?act=assembla<?php
											if (isset($_GET['tomod'])) {
												echo "&amp;tomod=" . $_GET['tomod']; } ?>&amp;del=<?php
													echo $key; ?>" title="Rimuovi Articolo">
											<img src="/Images/Links/del.png" alt="del" />
										</a>
									<?php } ?>
								</th>
							</tr>
						<?php } ?>

						<tr>
								<th colspan="8"><br /></th>
						</tr>
						<tr>
							<th colspan="8" style="font-style: italic ! important;">
								Componenti Aggiuntivi:
							</th>
						</tr>
						<tr>
							<th colspan="8"></th>
						</tr>

						<?php if(isset($_SESSION['offerta']['accessori']))
						{
							foreach ($_SESSION['offerta']['accessori'] as $key => $acc)
							{ ?>
								<tr>
									<td style="text-align: right;"><br /></td>

									<td style="text-align: center;">
										<?php echo $acc['cod_forn']; ?>
									</td>

									<td>
										<a onclick="Javascript: ShowFormDiv('frm_ricerca.php?pezzo=acc',
											'150px', '200px', '850px', '320px', 'popup'); void(0);">
											<?php echo $acc['modello']; ?>
										</a>
									</td>

									<td style="text-align: center;">
										<?php echo $acc['qta']; ?>
									</td>

									<td style="text-align: center;">
										<?php echo $acc['prezzo']; ?>
									</td>
									<td style="text-align: center;">
										<label class="<?php if ($acc['disp'] > 0) {
											echo "ok";
										}else{
											echo "err";
										} ?>">
											<?php echo $acc['disp']; ?>
										</label>
									</td>
									<th style="text-align: center;">
										<a href="?act=assembla<?php
										if (isset($_GET['tomod']))
										{
											echo "&amp;tomod=" . $_GET['tomod'];
										} ?>&amp;del=<?php echo $key; ?>">
											<img src="/Images/Links/del.png" alt="del" />
										</a>
									</th>
								</tr>
							<?php } ?>
						<?php } ?>
						<tr>
							<th colspan="8">
								&nbsp;&nbsp;&nbsp;&nbsp;
								<a onclick="Javascript: ShowFormDiv('frm_ricerca_articoli_off.php?pezzo=acc',
									'150px', '200px', '850px', '320px', 'popup'); void(0);">
									<img src="/Images/Links/add.png" alt="add" />
									Aggiungi
								</a>
							</th>
						</tr>
					</table>
				</fieldset>
			</th>
		</tr>

		<tr><th colspan="8"><br /></th></tr>

		<tr>
			<th colspan="8">
				<fieldset>
					<legend>Fase 3 - Personalizzazione Offerta:</legend>
					<label>Assemblaggio ed installazione.</label>
					<input name="assemb" type="text" style="width: 6em;"
						value="<?php if (isset($_POST['assemb']))
							{ echo $_POST['assemb']; }else{ echo "40.00"; }	?>" />

					<input name="ck_man" type="checkbox"
						<?php if ($_SESSION['offerta']['assemblaggio'] == 0)
							{
								/* FIXME: 2011-04-01 Claudio Giordano
								 *
								 * Assemblaggio ed installazione separati */
								echo 'checked="checked"'; $st = 1;
							}else{
								$st = 0;
							} ?>
						onchange="Javascript: posta('2', 'home.php?act=assembla&amp;asse=<?php
							echo $st; if (isset($_GET['tomod']))
							{
								echo "&amp;tomod=" . $_GET['tomod']; } ?>'); " />

				</fieldset>
			</th>
		</tr>


		<tr>
			<th colspan="8" style="text-align: right;">
				<?php echo $result; ?><br />
			</th>
		</tr>

		<tr>
			<th colspan="h">
				<fieldset>
					<legend>Fase 4 - Aggiunta Immagini:</legend>
					<form enctype="multipart/form-data" method="post" action="<?php echo $_SERVER['REQUEST_URI']; ?>">
						Stampante:&nbsp;<input name="img-stp" type="file" />&nbsp;
						Case:&nbsp;<input name="img-case" type="file" />&nbsp;
						Monitor:&nbsp;<input name="img-monit" type="file" />&nbsp;

						<a onclick="Javascript: posta('1', 'home.php?act=assembla&amp;immagini=0'); void(0);"
							style="cursor: pointer;" >
							<img src="/Images/Links/apply.png" alt="apply"
								title="Applica le immagini all'offerta" />
						</a>

						<a onclick="Javascript: conferma_salva('0', 'Rimuovere le immagini dall\'offerta?',
							'home.php?act=assembla&amp;del_img=0<?php
							if (isset($_GET['tomod'])) { echo "&amp;tomod=" . $_GET['tomod']; } ?>'); "
							style="cursor: pointer;" title="Rimuovi le immagini dall'offerta" >
							<img src="/Images/Links/cancel.png" alt="Rimuovi le immagini dall'offerta" />
						</a>
					</form>
				</fieldset>
			</th>
		</tr>
		<tr>
			<td colspan="8" style="text-align: right;">
				<?php echo $result_img; ?>
			</td>
		</tr>


	</table>


<?php
	echo "<pre> GET ";
		print_r($_GET);
	echo "</pre>";

	echo "<pre> POST ";
		print_r($_POST);
	echo "</pre>";


	echo "<pre> SESSION offerta ";
		print_r($_SESSION['offerta']);
	echo "</pre>";
?>
</div>

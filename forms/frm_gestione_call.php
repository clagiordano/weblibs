<?php
	$UrlSalva = 'home.php?act=call&amp;salva=salva';
	$ConfSalva = "Salvare i dati inseriti?";

	$Tecnici = $d->GetRows ("id_tecnico, concat(nome, \" \", cognome) as tecnico",
		"tab_tecnici", "", "tecnico");
	$stato_call = $d->GetRows ("*", "tab_stato_call", "", "", "stato_call");

	if (isset($_POST['id_tipo_call']) && $_POST['id_tipo_call'] != "-"
	&& ($_POST['id_tipo_call'] != $_SESSION['tipo_call']))
	{
		$econ = $d->GetRows ("*", "tab_attivita, tab_cat_att", "tab_attivita.id_attivita = '"
				. $_POST['id_tipo_call'] . "' AND tab_attivita.id_cat = tab_cat_att.id_cat_att");
		$_POST['econ'] = $econ[0]['prz'];
		$_SESSION['tipo_call'] = $_POST['id_tipo_call'];
	}

	if (isset($_GET['salva']))
	{
		$errori = "";
		$_POST['ind'] = addslashes($_POST['ind']);
		$_POST['den'] = addslashes($_POST['den']);
		$_POST['note'] = addslashes($_POST['note']);

		if (trim($_POST['n_call']) == "")
		{
			$errori .= "Errore, è necessario inserire un numero di chiamata valido.<br />";
		}

		if ($_POST['id_tecnico'] == "-")
		{
			$errori .= "Errore, è necessario selezionare un tecnico.<br />";
		}

		if ($_POST['cliente'] == "-")
		{
			$errori .= "Errore, è necessario selezionare un cliente.<br />";
		}

		if ($_POST['id_tipo_call'] == "-")
		{
			$errori .= "Errore, è necessario selezionare un tipo di attività.<br />";
		}

		if ($_POST['id_stato_call'] == "-")
		{
			$errori .= "Errore, è necessario selezionare uno stato per l'attività.<br />";
		}

		if (trim($_POST['data_att']) == "")
		{
				$_POST['data_att'] = date('d/m/Y');
		}else{
				$_POST['data_att'] = Inverti_Data($_POST['data_att'], "-", "/");
		}

		if (($_GET['salva'] == "salva") && ($errori == ""))
		{
			$Esclusioni = array("cliente" => $_POST['cliente'], 'id_prov' => '');
			$Status = SaveRow ($_POST, $Esclusioni, "tab_chiamate", $db);
			$Status = $Status[0];
		}

		if (($_GET['salva'] == "modifica") && ($errori == ""))
		{
			$Esclusioni = array("cliente" => $_POST['cliente'], 'id_prov' => '');
			$Status = UpdateRow ($_POST, $Esclusioni, "tab_chiamate", "id_chiamata = '" . $_GET['tomod'] . "'" , $db, 1);
		}
	}

	if (isset($_GET['tomod']))
	{
		$UrlSalva = 'home.php?act=call&amp;tomod=' . $_GET['tomod'] . '&amp;salva=modifica';
		$ConfSalva = "Salvare i dati modificati?";
	}

	if (is_numeric($_GET['tomod']) && empty($_POST))
	{
		$Call = $d->GetRows ("*", "view_call",
			"id_chiamata = '" . $_GET['tomod'] . "'");

		$cli = $d->GetRows ("*", "tab_attivita",
			"id_attivita = '" . $Call[0]['id_attivita'] . "'");
		$_POST 									= $Call[0];
		$_POST['cliente'] 			= $cli[0]['cliente'];
		$_POST['id_tipo_call'] 	= $cli[0]['id_attivita'];
		$_POST['id_prov'] 			= $Call[0]['id_prov'];
		$_POST['note'] 					= $Call[0]['note_call'];

		$Tipo_call = $d->GetRows ("id_attivita, concat(tipo, \" (\", note, \")\") as tipo",
			"tab_attivita", "cliente = '".$_POST['cliente']."'", "", "tipo");
	}

	$Clienti 	= $d->GetRows ("cliente", "tab_attivita", "", "cliente");
	$Province = $d->GetRows ("*", "tab_province", "", "Targa" );

	if (isset($_POST['id_prov']) && $_POST['id_prov'] != "-")
	{
		$Comuni = $d->GetRows ("Comune", "tab_comuni",
			"ID_Prov = '" . $_POST['id_prov'] . "'");
	}

	if (isset($_POST['cliente']) && $_POST['cliente'] != "-")
	{
		$Tipo_call = $d->GetRows ("id_attivita, concat(tipo, \" (\", note, \")\") as tipo",
			"tab_attivita", "cliente = '".$_POST['cliente']."'", "", "tipo");
	}
?>

<div id="search-form">
	<form method="post" action="<?php echo $_SERVER['REQUEST_URI']; ?>">
		<table class="form" style="width: 100%;">
			<?php $d->ShowEditBar(" Chiamata:", $_GET['tomod'], "new,save", $_GET['act']); ?>

			<tr>
				<td style="text-align: right;">
					<label>N.Call:</label>
				</td>
				<td>
					<input name="n_call" type="text" style="width: 14em;"
						value="<?php if (isset($_POST['n_call']))
							{ echo $_POST['n_call']; } ?>" />

						&nbsp;
						<label>Data Attività:</label>
						<input name="data_att" type="text" onfocus="showCalendarControl(this);"
							style="width: 9em;"
						 value="<?php if (isset($_POST['data_att']))
							{ echo Inverti_Data($_POST['data_att']); } ?>" />

						&nbsp;
						<label>Tecnico:</label>
						<select name="id_tecnico" style="width: 21.5em;">
							<option value="-">Seleziona Tecnico</option>
							<option value="-"></option>
							<?php foreach ($Tecnici as $key => $field) { ?>
								<option value="<?php echo $field['id_tecnico']; ?>"
									<?php if ($_POST['id_tecnico'] == $field['id_tecnico']) { echo "selected=\"selected\""; } ?> >
										<?php echo htmlentities($field['tecnico']); ?>
								</option>
							<?php } ?>
						</select>
				</td>
		</tr>

		<tr>
			<td style="text-align: right;">
				<label>Denominazione:</label>
			</td>
			<td>
				<input name="den" type="text" style="width: 27em;"
				 value="<?php if (isset($_POST['den']))
					{ echo $_POST['den']; } ?>" />

				&nbsp;
				<label>Prov:</label>
				<select name="id_prov" style="width: 4.0em;"
					onchange="javascript: posta('0', 'home.php?act=call<?php
						if (isset($_GET['tomod'])) { echo "&amp;tomod=" . $_GET['tomod']; } ?>'); void(0);">
							<option value="-">-</option>
							<?php foreach ($Province as $key => $field) { ?>
							<option value="<?php echo $field['ID']; ?>"
									<?php if ($_POST['id_prov'] == $field['ID']) { echo "selected=\"selected\""; } ?> >
									<?php echo $field['Targa']; ?>
							</option>
							<?php } ?>
				</select>

				&nbsp;
				<label>Citt&agrave;:</label>
				<select name="id_citta" style="width: 20em;">
					<option value="-">-</option>
						<?php foreach ($Comuni as $key => $field) { ?>
						<option value="<?php echo $field['ID']; ?>"
								<?php if ($_POST['id_citta'] == $field['ID']) { echo "selected=\"selected\""; } ?> >
								<?php echo htmlentities(ucfirst(strtolower($field['Comune']))); ?>
						</option>
						<?php } ?>
					</select>
			</td>
		</tr>

		<tr>
			<td style="text-align: right;"><label>Indirizzo:</label></td>
			<td>
				<input name="ind" type="text" style="width: 62.7em;"
					value="<?php if (isset($_POST['ind']))
						{ echo $_POST['ind']; } ?>" />
			</td>
		</tr>

		<tr>
			<td style="text-align: right;"><label>Cliente:</label></td>
			<td>
				<select name="cliente" style="width: 11.5em;"
					onchange="javascript: posta('0', 'home.php?act=call<?php
					if (isset($_GET['tomod'])) { echo "&amp;tomod=" . $_GET['tomod']; } ?>'); void(0);">
						<option value="-">Seleziona Cliente</option>
						<option value="-"></option>
						<?php foreach ($Clienti as $key => $field) { ?>
						<option value="<?php echo $field['cliente']; ?>"
							<?php if ($_POST['cliente'] == $field['cliente'])
								{ echo "selected=\"selected\""; } ?> >
							<?php echo $field['cliente']; ?>
						</option>
						<?php } ?>
				</select>

				&nbsp;
				<label>Tipo Call:</label>
				<select name="id_tipo_call" style="width: 20em;"
					onchange="javascript: posta('0', 'home.php?act=call<?php
					if (isset($_GET['tomod'])) { echo "&amp;tomod=" . $_GET['tomod']; } ?>'); void(0);">
						<option value="-">Seleziona Tipo Call</option>
						<option value="-"></option>
						<?php foreach ($Tipo_call as $key => $field) { ?>
						<option value="<?php echo $field['id_attivita']; ?>"
							<?php if ($_POST['id_tipo_call'] == $field['id_attivita']) { echo "selected=\"selected\""; } ?> >
							<?php echo htmlentities($field['tipo']); ?>
						</option>
						<?php } ?>
				</select>

				&nbsp;
				<label>Stato Call:</label>
				<select name="id_stato_call" style="width: 12.5em;">
						<option value="-">Seleziona Stato Call</option>
						<option value="-"></option>
						<?php foreach ($stato_call as $key => $field) { ?>
						<option value="<?php echo $field['id_stato_call']; ?>"
							<?php if ($_POST['id_stato_call'] == $field['id_stato_call']) { echo "selected=\"selected\""; } ?> >
							<?php echo $field['stato_call']; ?>
						</option>
						<?php } ?>
				</select>
			</td>
		</tr>
		<tr>
			<td style="text-align: right;">
				<label>Referente:</label>
			</td>
			<td>
				<input name="ref" type="text" style="width: 21em;"
					 value="<?php if (isset($_POST['ref'])) { echo $_POST['ref']; } ?>" />

				&nbsp;
				<label>Telefono:</label>
				<input name="tel" type="text" style="width: 11em;"
				value="<?php if (isset($_POST['tel'])) { echo $_POST['tel']; } ?>" />

				&nbsp;
				<label>N.Pdl:</label>
				<input name="n_pdl" type="text" style="width: 4.5em;"
					value="<?php if (isset($_POST['n_pdl'])) { echo $_POST['n_pdl']; } ?>" />

				&nbsp;
				<label>Compenso:</label>
				<input name="econ" type="text" style="width: 5em;"
					value="<?php if (isset($_POST['econ'])) { echo $_POST['econ']; } ?>" />
			</td>
		</tr>

		<tr>
			<td style="vertical-align: top; text-align: right;">
				<label>Note:</label>
			</td>
			<td>
				<textarea name="note" type="text" style="width: 63em;" cols="90" rows="8"><?php if (isset($_POST['note'])) { echo $_POST['note']; } ?></textarea>
			</td>
		</tr>

		<tr>
			<td colspan="4">
				<label class="err"><?php if (isset($errori)) { echo $errori;  } ?></label>
			</td>
		</tr>
		<tr>
			<td colspan="4">
				<label class="ok"><?php if (isset($errori)) { echo $Status; } ?></label>
			</td>
		</tr>
	</table>
</form>

<?php
/*
	echo "<pre>";
		print_r($_SESSION);
	echo "</pre>";*/ ?>

</div>



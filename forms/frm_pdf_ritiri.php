<?php ob_end_clean();
	require_once("fpdf/fpdf.php");

	$request = $d->GetRows ("request,den,ind", "view_requests",
		"id_request = '" . $_GET['rid'] . "'");
	$dettaglio_ritiri = $d->GetRows ("tipo_mch, marca, modello, provenienza, serial, asset, ck_funz, note_acc",
		"view_macchine_assegnate", "id_request = '" . $_GET['rid']
		. "' AND provenienza = 'DA FILIALE' and tipo_mch != 'Server'", "", "tipo_mch", 1);

	class PDF extends FPDF
	{
		//Page footer
		function Footer()
		{
			global $request;
			//Position at 2cm from bottom
			$this->SetY(-20);
			$this->SetFont('Arial', 'B', 9);
			$this->Cell(30, 4, "Mittente: ", '', '', 'R');
			$this->Cell(170, 4, "Banco di Sicilia Filiale \""
				. $request[0]['den'] . "\", Indirizzo: " . $request[0]['ind']);
			$this->Ln();

			$this->Cell(30, 4, "Destinatario: ", '', '', 'R');
			$this->Cell(170, 4, "Sicilia Distribuzione Merci, "
				. "Indirizzo: Via Badia, 70 (90100 Palermo)");
			$this->Ln();

			$this->SetFont('Arial', '', 9);
			$this->Cell(40, 4, 'Data: ' . date('d/m/Y'), 0, '', 'L');
			$this->Cell(0, 4, 'Pagina ' . $this->PageNo() . "/{nb}", 0, 0 ,'C');
		}
	}

	$pdf = new PDF();
	$pdf->AliasNbPages();
	$pdf->SetMargins(6, 6, 6);
	$pdf->AddPage('L');

	// Header
	$pdf->SetFont('Helvetica', 'B', 14);
	$pdf->Cell(60, 7, 'Dettaglio Ritiri Filiale: ', '', '', 'R');

	$pdf->SetFont('Helvetica', '', 14);
	$pdf->Cell(220, 7, $request[0]['den'] . ", Rif. Request: "
		. $request[0]['request'], '', '', '');
	$pdf->ln(10);

	$pdf->SetFont('Helvetica', 'B', 10);
	$pdf->Cell(10, 5, '', 'B', '', 'R');
	$pdf->Cell(35, 5, 'Tipo Macchina:', 'B', '', 'L');
	$pdf->Cell(35, 5, 'Marca:', 'B', '', 'L');
	$pdf->Cell(35, 5, 'Modello:', 'B', '', 'L');
	$pdf->Cell(70, 5, 'Serial:', 'B', '', 'L');
	$pdf->Cell(70, 5, 'Asset:', 'B', '', 'L', '0');
	$pdf->Cell(15, 5, 'Funz:', 'B', '', 'L');
	$pdf->Cell(15, 5, 'Acces:', 'B', '', 'L');
	$pdf->ln();

	$colli = 0;
	$i = 1;
	foreach ($dettaglio_ritiri as $key => $field)
	{
		$pdf->SetFont('Helvetica', 'B', 10);
		$pdf->Cell(10, 5, $i . ")", '0', '', 'R');

		$pdf->SetFont('Helvetica', '', 10);
		$pdf->Cell(35, 5, $field['tipo_mch'], '0', '', 'L');
		$pdf->Cell(35, 5, $field['marca'], '0', '', 'L');
		$pdf->Cell(35, 5, $field['modello'], '0', '', 'L');
		$pdf->Cell(70, 5, $field['serial'], '0', '', 'L');
		$pdf->Cell(70, 5, $field['asset'], '0', '', 'L');
		$pdf->Cell(15, 5, $field['ck_funz'], '0', '', 'L');
		$pdf->Cell(15, 5, $field['note_acc'], '0', '', 'L');
		$pdf->ln();

		$colli += 1;
		$i += 1;
	}

	if ($colli == 0)
	{
		$pdf->SetFont('Helvetica', '', 10);
		$pdf->Cell(200, 5, 'Nessun componente da consegnare.', '0', '', 'L');
		$pdf->ln();
	}

	$pdf->Output('Ritiri.pdf', 'I');
?>

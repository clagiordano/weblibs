<?php session_start();
    //~ echo "[Debug]: avvio esportazione personalizzata <br />";
    require_once("connection.php");
    
    $d->CheckSession("progsla", "", "../pubblica/index.php");
    
    //~ echo "ExportArray:<pre>";
        //~ print_r($_SESSION['ExportArray']);
    //~ echo "</pre>";
    
    $root = substr($_SERVER["DOCUMENT_ROOT"], 0, -1);
    require_once("$root/prog_sla/classi/XlsExport.php");            // locale
    //~ require_once("$root/classi/XlsExport.php");	// remoto
    
    $d->SessionLog("Avviata esportazione personalizzata.");
    $FileName = "tmp/esportazione_personalizzata_" . date('d-m-Y') . ".xls";
    $x = new XlsExport($FileName);
    
    // preparazione primo gruppo di intestazioni ( generalità ):
    
    //~ $Generalita = $d->GetRows("*", "view_generalita", "", "", "", 1);

    $x->xlsWriteLabel(0, 0, "Data:");   // contatore base 1
    $x->xlsWriteLabel(0, 1, "N. Cartella:");
    $x->xlsWriteLabel(0, 2, "Cognome:");
    $x->xlsWriteLabel(0, 3, "Nome:");
    $x->xlsWriteLabel(0, 4, "Sesso:");
    $x->xlsWriteLabel(0, 5, "Luogo di nascita:");
    $x->xlsWriteLabel(0, 6, "Età:");
    $x->xlsWriteLabel(0, 7, "Altezza:");
    $x->xlsWriteLabel(0, 8, "Prov:");
    $x->xlsWriteLabel(0, 9, "Città:");
    $x->xlsWriteLabel(0, 10, "Cap:");
    $x->xlsWriteLabel(0, 11, "Indirizzo:");
    $x->xlsWriteLabel(0, 12, "Telefono:");
    $x->xlsWriteLabel(0, 13, "E-Mail:");
    $x->xlsWriteLabel(0, 14, "Stato Civile:");
    $x->xlsWriteLabel(0, 15, "Istruzione:");
    $x->xlsWriteLabel(0, 16, "Attività lavorativa attuale:");
    $x->xlsWriteLabel(0, 17, "Tipo di lavoro svolto:");
    $x->xlsWriteLabel(0, 18, "Attività lavorativa principale ( anni ):");
    $x->xlsWriteLabel(0, 19, "Età pensionamento:");
    $x->xlsWriteLabel(0, 20, "Forma clinica:");
    $x->xlsWriteLabel(0, 21, "Diagnosi:");
    $x->xlsWriteLabel(0, 22, "Rate of progression:");
    $x->xlsWriteLabel(0, 23, "Sintomo d'esordio:");
    $x->xlsWriteLabel(0, 24, "Età esordio:");
    $x->xlsWriteLabel(0, 25, "Ritardo diagnostico ( mesi ):");
    $x->xlsWriteLabel(0, 26, "Sede d'esordio:");
    $x->xlsWriteLabel(0, 27, "Primo esame dopo l'esordio:");
    $x->xlsWriteLabel(0, 28, "Int. 1° sintomo - 1° consulto (mesi):");
    $x->xlsWriteLabel(0, 29, "Int. 1° sintomo - 1° neurologo (mesi):");
    $x->xlsWriteLabel(0, 30, "Int. 1° sintomo - 1° visita al Centro (mesi):");
    $x->xlsWriteLabel(0, 31, "Diagnosi effettuata c/o il Centro:");
    $x->xlsWriteLabel(0, 32, "N. ricoveri:");
    $x->xlsWriteLabel(0, 33, "Intervallo esordio - I ALSFRS-R:");
    $x->xlsWriteLabel(0, 34, "Intervallo esordio-exitus:");
    $x->xlsWriteLabel(0, 35, "Causa exitus:");
    
    // fine intestazioni generalità
        
    $QTabelle = "";
    $QCampi = "";
    $QWhere = "";
    $xlsCol = 36;
foreach ($_SESSION['ExportArray'] as $key => $field) {
    switch ($field)
    {
        case "tabella":
            $QTabelle .= $key . ", ";
            $QWhere .= "$key.id_generalita = view_generalita.id_generalita AND ";
            break;
                                    
        case "campo":
            //~ echo "[Debug]: KEY: $key <br />";
            if (preg_match("((.*)_as_(.*))", $key, $match)) {
            //~ echo "MATCH<pre>";
                    //~ print_r($match);
                //~ echo "</pre>";
                    
                $key = $match[1] . " as " . $match[2];
                //~ echo "[Debug]: KEY post edit: $key <br />";
            }
                
            // aggiunta successive intestazioni per i campi scelti:
                    
            // separo il nome della tabella dal nome del campo:
            $TempArray = explode("+", $key);
            //~ echo "TempArray: <pre>";
                //~ print_r($TempArray);
            //~ echo "</pre>";
                
            // estraggo l'etichetta per il campo corrente tramite query:
            $TempRow = $d->GetRows(
                "*",
                "tab_switch",
                "tabella = '" . $TempArray[0] . "' AND nome_campo = '"
                . $TempArray[1] . "'",
                "",
                "",
                1
            );
                    
            //~ echo "TempRow: <pre>";
                //~ print_r($TempRow);
            //~ echo "</pre>";
                
            // aggiungo l'intestazione per il campo corrente:
            $x->xlsWriteLabel(0, $xlsCol, $TempRow[0]['etichetta'] . ":");
            //~ echo "[Debug]: HeadColNum: $HeadColNum <br />";
            $xlsCol++;
                
            //~ echo "[Debug]: KEY " . str_replace("+", ".", $key) . " <br />";
            $QCampi .= str_replace("+", ".", $key) . ", ";
            break;
    }
}
        
if (($QTabelle != "")
        && ($QCampi != "") ) {
    $QCampiBase = "view_generalita.data, view_generalita.n_cartella, view_generalita.cognome, view_generalita.nome, view_generalita.sesso, view_generalita.luogo_nascita, view_generalita.eta, view_generalita.altezza, view_generalita.Targa, view_generalita.Comune, view_generalita.cap, view_generalita.indirizzo, view_generalita.telefono, view_generalita.email, view_generalita.stato_civile, view_generalita.istruzione, view_generalita.attivita_attuale, view_generalita.tipo_lavoro, view_generalita.attivita_attuale_anni, view_generalita.eta_pensionamento, view_generalita.forma_clinica, view_generalita.diagnosi, view_generalita.progression, view_generalita.esordio_sintomo, view_generalita.esordio_eta, view_generalita.diagnosi_eta, view_generalita.esordio_sede, view_generalita.esordio_esame, view_generalita.esordio_1_consulto, view_generalita.esordio_1_neurologo, view_generalita.esordio_1_centro, view_generalita.diagnosi_centro, view_generalita.n_visite_centro, view_generalita.esordio_1_frs, view_generalita.esordio_exitus, view_generalita.causa_exitus";
    $QTabelle = "view_generalita, " . ereg_replace(", $", "", $QTabelle);
    $QCampi = $QCampiBase . ", " . ereg_replace(", $", "", $QCampi);
    $QWhere = ereg_replace("AND $", "", $QWhere);
    //~ echo "[Debug]: QTabelle: $QTabelle <br />";
    //~ echo "[Debug]:   QCampi: $QCampi <br />";
    //~ echo "[Debug]:   QWhere: $QWhere <br />";
    
    $Results = $d->GetRows(
        $QCampi,
        $QTabelle,
        $QWhere,
        "",
        "view_generalita.cognome, view_generalita.nome, view_generalita.n_cartella",
        0
    );
    //~ $Results = $d->Query("SELECT $QCampi FROM $QTabelle WHERE $QWhere ORDER BY view_generalita.cognome, view_generalita.nome, view_generalita.n_cartella LIMIT 2", 1);
        
    //~ echo "[Debug]: " . count($Results) . " <br />";
        
    //~ echo "<pre>";
        //~ print_r($Results);
    //~ echo "</pre>";
}
    

    $xlsRow = 1;
foreach ($Results as $key => $field) {
    $xlsCol = 0;
    // inserire conversioni dati char
        
    //~ echo "<pre>field";
        //~ print_r($field);
    //~ echo "</pre>";
        
    // attraverso l'array del risultato corrente per stampare i campi:
    foreach ($field as $keyr => $fieldr) {
    // verifica se e' un campo data e convertilo:
        //~ if (preg_match(RegDataMySql, $fieldr))
        //~ {
            //~ $fieldr = $d->Inverti_Data($fieldr);
        //~ }
            
        //~ echo "[Debug]: fieldr: $fieldr <br />";
        $x->xlsWriteLabel($xlsRow, $xlsCol, $fieldr);
        $xlsCol++;
    }
        
    $xlsRow++;
}

    $x->WriteFile();
    $d->SessionLog("Terminata esportazione personalizzata.");
    exit();
?>

<script type="text/javascript">
	window.close();
</script>






<?php
	$root = substr($_SERVER['DOCUMENT_ROOT'], 0, -1);

	$Where = "pallet != ''";
	if (isset($_POST['pallet']))
	{
		$Where .= " AND pallet LIKE '" . $_POST['pallet'] . "'";
	}

	$Pallets = $d->GetRows ("distinct(pallet)", "view_macchine", $Where, "tipo_mch");

/*
	echo "<pre>";
		print_r($Pallets);
	echo "</pre>";
*/

/*
	$tot_res = count(GetRows ("view_macchine", "pallet != \"\"", "pallet, tipo_mch", $db));
	$order = PageSplit($_GET['page'], $tot_res);
	$Pallets = GetRows ("view_macchine", "pallet != \"\"", "pallet, tipo_mch", $db);
*/
?>

<div id="search-form">
	<form method="post" action="<?php echo $_SERVER['REQUEST_URI']; ?>">
		<table class="form" style="width: 100%;">
			<tr>
				<th>Seleziona i campi fra i quali ricercare:</th>
			</tr>
			<tr>
				<td>
					<label>Descrizione Pallet</label>
					<select name="pallet"
						onchange="Javascript: posta('0', '<?php echo $_SERVER['REQUEST_URI']; ?>'); void(0);">
						<option value="%">Tutti i pallet</option>
						<?php foreach ($Pallets as $key => $field) { ?>
							<option value="<?php echo $field['pallet']; ?>"
								<?php if ($_POST['pallet'] == $field['pallet']) { echo "selected=\"selected\""; } ?> >
								<?php echo $field['pallet']; ?>
							</option>
						<?php } ?>
					</select>
				</td>
			</tr>
			<?php //ShowResultBar($tot_res, "home.php?act=pallet"); ?>
		</table>
	</form>
</div>

<div id="search-result-2r">
	<form method="post" action="<?php echo $_SERVER['REQUEST_URI']; ?>">
		<table class="dettaglio" style="width: 100%;">
			<?php foreach ($Pallets as $key => $field) {
				$dett_pallet = $d->GetRows ("*", "view_macchine", "pallet = '"
					. $field['pallet'] . "'", "tipo_mch"); ?>
				<tr>
					<th colspan="8">
						Dettaglio Pallet: &nbsp;
						<label class="ok"><?php echo $field['pallet']; ?></label>

						&nbsp;
						<a onclick="Javascript: window.open('frm_pdf_pallet.php?pallet=<?php
							echo $field['pallet']; ?>', '_blank'); void(0);">
							<img src="/Images/Links/pdf.png" alt="pdf"
								title="Stampa il pallet selezionato in formato pdf" />
						</a>
					</th>
				</tr>
				<tr>
					<th></th>
					<th></th>
					<th>Tipo Macchina:</th>
					<th>Marca:</th>
					<th>Modello:</th>
					<th>Serial:</th>
					<th>Asset:</th>
				</tr>
				<tr><th colspan="8"><hr /></th></tr>
				<?php foreach ($dett_pallet as $key_d => $field_d) { ?>
					<tr>
						<th><br /></th>
						<td><label><?php echo $key_d+1 . ")"; ?></label></td>
						<td><?php echo $field_d['tipo_mch']; ?></td>
						<td><?php echo $field_d['marca']; ?></td>
						<td><?php echo $field_d['modello']; ?></td>
						<td><?php echo $field_d['serial']; ?></td>
						<td><?php echo $field_d['asset']; ?></td>
					</tr>
				<?php } ?>

				<tr><th colspan="8"><br /></th></tr>
			<?php } ?>
		</table>
	</form>
</div>

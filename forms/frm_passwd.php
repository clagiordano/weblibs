<?php
	if (!empty($_POST))
	{
		// verifica se l'id ed il nome utente coincidono
		$user = $d->GetRows ("*", "tab_utenti", "id_user = '"
			. $_SESSION['id_user'] . "'");
		if ($user[0]['desc_user'] == $_SESSION['desc_user'])
		{
			//echo "id ed utente coincidono!";
			if (isset($_POST['cpassword'])
				&& ($_POST['cpassword'] == $_POST['conf_cpassword'])
				&& trim($_POST['cpassword']) != ""
				&& trim($_POST['conf_cpassword']) != "")
			{
				// le password coincidono quindi eseguo l'update
				$Valori = array('pass' => md5($_POST['cpassword']));
				$Esclusioni = array('conf_cpassword' => '');
				$Status = $d->UpdateRow ($Valori, $Esclusioni, "tab_utenti",
					"id_user = '" . $_SESSION['id_user'] . "'");
			}else{
				$Status = "<label class=\"err\">Errore, le password"
					. " inserite non coincidono o sono vuote.</label> <br />";
			}
		}else{
			$Status = "<label class=\"err\">Errore impossibile"
				. " modificare la password.</label> <br />";
		}
	}

?>
<form method="post">
	<table class="form" style="width: 100%;">
		<tr>
			<td style="text-align: right;" colspan="4">
				<a onclick="Javascript: ClosePopup(); void(0);">
					[ <label class="err" style="cursor: pointer;">Chiudi</label> ]
				</a>
			</td>
		</tr>
		<tr>
			<th colspan="4">
				Modifica dati utente:

				&nbsp;
				<label>
					<a onclick="Javascript: conferma_salva('0',
						'Salvare i dati inseriti?', '<?php
							echo $_SERVER['REQUEST_URI']; ?>'); void(0);">
						<img src="/Images/Links/save.png" alt="save"
							style="width: 14px; height: 14px;" />
						Salva
					</a>
				</label>
			</th>
		</tr>
		<tr><th colspan="4"><hr /></th></tr>
		<tr>
			<td style="text-align: right;">
				<label>Password:</label>
			</td>
			<td>
				<input name="cpassword" type="password" />
			</td>
		</tr>

		<tr>
			<td style="text-align: right;">
				<label>Conferma Password:</label>
			</td>
			<td>
				<input name="conf_cpassword" type="password" />
			</td>
		</tr>

		<tr>
			<td colspan="4">
				<?php if (isset($Status)) { echo $Status; } ?>
			</td>
		</tr>
	</table>
</form>

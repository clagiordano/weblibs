<?php
	$root = substr($_SERVER['DOCUMENT_ROOT'], 0, -1);
	require_once ("$root/Function/Strings.php");
	require_once ("$root/Function/Db.php");
	require_once ("$root/Function/Debug.php");
	require_once ("$root/Function/DataTime.php");

	$condizione = array();
/*
	array_push($condizione, "id_ord != ''");
*/


	if (isset($_GET['del']))
	{
/*
		DeleteRows ("tab_offerte", "id_ord = '" . $_GET['del'] . "'", $db);
		DeleteRows ("tab_dett_offerte", "id_orderta = '" . $_GET['del'] . "'", $db);

		$Result = "<label class=\"ok\">Offerta eliminata.</label>";
*/
	}

	if (isset($_POST['ck_interval']))
	{
		/*
		 * Se una delle date è vuota ma il campo e' stato flaggato
		 * le date mancanti diventano la data odierna
		 */

		if (trim($_POST['val_data_i']) == "")
		{
			$_POST['val_data_i'] = date('d/m/Y');
		}

		if (trim($_POST['val_data_f']) == "")
		{
			$_POST['val_data_f'] = date('d/m/Y');
		}

		$inizio =  Inverti_Data($_POST['val_data_i'], "-", "/");
		$fine = Inverti_Data($_POST['val_data_f'], "-", "/");
		array_push($condizione, "(data >= '$inizio' AND data <= '$fine')");
	}

	if (isset($_POST['ck_cliente']))
	{
		$val_cli = ModoRicerca($_POST['val_cliente'], "tutto");
		$val_cli = ExpandSearch($val_cli);
		array_push($condizione, "tab_anagrafica.ragione_sociale LIKE '$val_cli'"
			. " AND tab_ordini.id_anagrafica = tab_anagrafica.id_anag");
	}

	if (isset($_POST['stato']))
	{
		array_push($condizione, "ck_stato LIKE '" . $_POST['stato'] . "'");
	}

	foreach ($condizione as $key => $field)
	{
		if (($key+1) != count($condizione))
		{
			$campi .= $field . " AND ";
		}else{
			$campi .= $field;
		}
	}

	$campi .= " group by tab_ordini.id_ordine ";

/*
	if (isset($_POST['ck_last15']))
	{
		//~ deve essere l'ultima condizione da controllare.
		$campi .= " ORDER BY data desc LIMIT 15";
	}
*/

	if (!empty($_POST))
	{
		$Ordini = GetRows ("tab_ordini, tab_anagrafica", $campi, "data desc", $db);
	}
?>

<div id="search-form">
	<form method="post" action="<?php echo $_SERVER['REQUEST_URI']; ?>">
		<table class="form" style="width: 100%;">
			<tr>
				<th colspan="5">
					Seleziona i campi fra i quali ricercare:
				</th>
			</tr>
			<tr>
				<td>
					<input name="ck_interval" type="checkbox" value="interval"
						<?php if ($_POST['ck_interval'] != "") { echo "checked=\"checked\""; } ?> />
						<label>Intervallo:</label>
					<input name="val_data_i" type="text" style="width: 7em;" title="Data iniziale"
						onfocus="showCalendarControl(this);"
						value="<?php if (isset($_POST['val_data_i'])) { echo $_POST['val_data_i']; } ?>" />

					<input name="val_data_f" type="text" style="width: 7em;" title="Data finale"
						onfocus="showCalendarControl(this);"
						value="<?php if (isset($_POST['val_data_f'])) { echo $_POST['val_data_f']; } ?>" />

					<input name="ck_cliente" type="checkbox" value="cliente"
						<?php if ($_POST['ck_cliente'] != "") { echo "checked=\"checked\""; } ?> />
						<label>Cliente</label>
					<input name="val_cliente" type="text" style="width: 19em;"
						value="<?php if (isset($_POST['val_cliente'])) { echo $_POST['val_cliente']; } ?>" />

					<label>Stato:</label>
						<input name="stato" type="radio" value="%"
							<?php if ($_POST['stato'] == "%") { echo "checked=\"checked\""; } ?>
							checked="checked" />Tutti
						<input name="stato" type="radio" value="1"
							<?php if ($_POST['stato'] == "1") { echo "checked=\"checked\""; } ?>
							/>Inevasi
						<input name="stato" type="radio" value="0"
							<?php if ($_POST['stato'] == "0") { echo "checked=\"checked\""; } ?>
							/>Evasi

					<a onclick="Javascript: posta('0', '<?php echo $_SERVER['REQUEST_URI']; ?>')"
						style="cursor: pointer;">
						<img src="img/find.png" alt="Cerca" title="Avvia la ricerca"
							style="border: 0px; width: 16px; height: 16px;" />
					</a>
				</td>
			</tr>
			<tr>
				<?php if (isset($_POST['stato'])) { ?>
					<?php if (count($Ordini) > 0) { ?>
						<td colspan="8" style="text-align: left;">
							<label>Risultati corrispondenti:</label>
							<label class="ok"><?php echo count($Ordini); ?></label>
						</td>
					<?php }else{ ?>
						<td style="text-align: right;">
							<label class="err">Nessun risultato corrispondente.</label>
						</td>
					<?php } ?>
				<?php }else{ ?>
					<td><br /></td>
				<?php } ?>
			</tr>
		</table>
	</form>
</div>

<?php if (isset($_POST) && (count($_POST) > 0)) { ?>
	<div id="search-result-2r">
		<?php if (count($Ordini) > 0) { ?>
			<table cellpadding="1" cellspacing="1" border="1" style="width: 100%;"
				rules="none" frame="box" class="dettaglio">
				<tr>
					<th>Data:</th>
					<th>Totale:</th>
					<th>Cliente:</th>
					<th>Utente:</th>
					<th>Stato:</th>
					<th><!-- links !--></th>
				</tr>
				<tr><th colspan="8"><hr /></th></tr>
				<?php foreach ($Ordini as $key => $field) { ?>
					<tr>
						<td><?php echo Inverti_DataTime($field['data']); ?></td>
						<td><?php echo $field['tot']; ?></td>
						<td>
							<?php if ($field['id_anagrafica'] == 0)
							{
								echo "-";
							}else{
								$anag = GetRows ("tab_anagrafica", "id_anag = '"
									. $field['id_anagrafica'] . "'", "", $db);
								echo $anag[0]['ragione_sociale'];
							} ?>
						</td>

						<td>
							<?php $user = GetRows ("tab_utenti", "id_user = '"
									. $field['id_utente'] . "'", "", $db);
								echo $user[0]['user']; ?>
						</td>

						<td>
							<?php if ($field['ck_stato'] == 1)
								{
									echo "Inevaso";
								}else{
									echo "Evaso";
								}
								/*
								 * FIXME 20/07/2009  Claudio Giordano checkbox stato dell'ordine
								 *
								 *  Modificare con un checkbox per settare lo stato dell'ordine
								 */
							 ?>
						</td>

						<td style="text-align: left;">
							<a onclick="Javascript: popUpWindow('Dettaglio', 'frm_vis_dettaglio_off.php?id_ord=<?php
								echo $field['id_ordine']; ?>', '250', '250', '650', '250')"
								title="Visualizza Dettaglio" style="cursor: pointer;">
									<img src="img/info.png" alt="Dett"
										style="border: 0px; width: 14px; height: 14px;" />
							</a>

							<a onclick="Javascript: popUpWindow('Pdf', 'frm_esporta_pdf.php?id_ord=<?php echo $field['id_ordine']; ?>',
									'350', '350', '850', '500'); void(0);"
								title="Esporta in Pdf" style="cursor: pointer;">
									<img src="img/pdf.png" alt="Pdf"
										style="border: 0px; width: 14px; height: 14px;" />
							</a>

							<?php if ($field['ck_stato'] == 1) { /*  href="?act=ordini&amp;drop=0&amp;id_ord=<?php
									echo $field['id_ordine']; ?>" */ ?>
								<a title="Modifica Ordine" href="#"
										style="cursor: pointer;">
									<img src="img/edit.png" alt="Modifica Ordine"
										style="border: 0px; width: 14px; height: 14px;" />
								</a>

								<a onclick="Javascript: go_conf2('Eliminare l\'ordine?',
									'?act=ricerca_off&amp;del=<?php echo $field['id_ordine']; ?>'); void(0);"
									title="Elimina Ordine" style="cursor: pointer;">
									<img src="img/del.png" alt="Elimina Ordine"
										style="border: 0px; width: 14px; height: 14px;" />
								</a>
							<?php } ?>
						</td>
					</tr>
				<?php } ?>
			</table>
		<?php } ?>
	</div>
<?php } ?>



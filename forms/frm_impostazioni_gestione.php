<?php
/**
 * Modulo di gestione delle impostazioni di ditta, pdf, compilazione 
 * documenti, logo e relative colonne visualizzate.
 * 
 * Copyright 2008 - 2014 Claudio Giordano <claudio.giordano@autistici.org>
 */
//~ echo "[Debug]: ditta_id_prov: " . $_POST['ditta_id_prov'] . " <br />";

$TOMOD = filter_input(INPUT_GET, 'tomod', FILTER_VALIDATE_INT);
$DefaultConfig = Array('ditta_rag_soc'             => 'Ragione Sociale',
    'ditta_piva'                => 'P. IVA',
    'ditta_indirizzo'           => 'Indirizzo, civico',
    'ditta_telefono'            => 'Telefono',
    'ditta_cognome'             => 'Cognome',
    'ditta_nome'                => 'Nome',
    'ditta_mail'                => 'info@dominio.it',
    'ditta_responsabile'        => 'Nome Cognome',
    'ditta_cap'                 => 'Cap',
    'ditta_id_prov'             => '0',
    'ditta_id_citta'            => '0',
    'ditta_stato'               => 'Italia',
    'ditta_fax'                 => 'Fax',
    'ditta_cellulare'           => 'Cellulare',
    'ditta_logo'                => 'img/logo.jpg',
    'ditta_layout_doc'          => '1',
    'ditta_iban'                => 'IBAN',
    'ditta_doc_show_logo'       => '0',
    'ditta_doc_cod_forn'        => '1',
    'ditta_doc_cod_int'         => '0',
    'ditta_doc_descr'           => '0',
    'ditta_doc_qta'             => '0',
    'ditta_doc_prz'             => '0',
    'ditta_doc_sco'             => '0',
    'ditta_doc_importo'         => '0',
    'ditta_doc_iva'             => '0',
    'ditta_doc_note_art'        => '1',
    'ditta_doc_orientamento'    => 'P',
    'ditta_doc_cod_forn_width'  => '18',
    'ditta_doc_cod_int_width'   => '18',
    'ditta_doc_desc_width'      => '110',
    'ditta_doc_qta_width'       => '18',
    'ditta_doc_prz_width'       => '18',
    'ditta_doc_sco_width'       => '10',
    'ditta_doc_importo_width'   => '18',
    'ditta_doc_iva_width'       => '10',
    'ditta_doc_note_art_width'  => '200',
    'ditta_doc_comp_codice'     => '0',
    'ditta_doc_comp_descr'      => '0',
    'ditta_doc_comp_note'       => '0',
    'ditta_doc_comp_qta'        => '0',
    'ditta_doc_comp_imp'        => '0',
    'ditta_doc_comp_iva'        => '0',
    'ditta_doc_comp_sco'        => '0',
    'ditta_doc_comp_sco2'       => '1',
    'ditta_doc_comp_sco3'       => '1',
    'ditta_doc_comp_tot'        => '0',
    'ditta_precisione_decimali' => '2');

// Verifica esistenza opzioni tramite i valori di default "DefaultConfig":
foreach ($DefaultConfig as $key => $field) {
    //~ echo "[Debug]: key: $key <br />";
    //~ echo "[Debug]: field $field <br />";
    $temp_var = $d->GetRows("*", "tab_config", "opt = '$key'", "", "", 1);
    //~ echo "<pre> temp_var";
    //~ print_r($temp_var);
    //~ echo "</pre>";
    //~ echo "[Debug]: count array: " . count($temp_var) . " <br />";

    if (count($temp_var) < 1) {
        //~ echo "[Debug]: opzione mancante la aggiungo al database config<br />";
        // se l'opzione manca aggiungila alla tabella:
        $Status = $d->SaveRow(Array('opt' => $key, 'val' => $field), "",
                              "tab_config", 1);
    } else {
        if (trim($temp_var[0]['val']) == "") {
            //~ echo "[Debug]: valore di default mancante <br />";
            // se l'opzione esiste ma il valore e' nullo aggiorna la tabella:
            $Status = $d->UpdateRow(Array('val' => $field), "", "tab_config",
                                    "opt = '$key'", 1);
        }
    }
}

if (isset($_GET['salva']) && ($_GET['salva'] == "modifica")) {
    $file_stat = "";
    umask(0);

    // e' stata sostituita l'immagine
    if (isset($_FILES['fditta_logo']['tmp_name']) && ($_FILES['fditta_logo']['tmp_name'] != "")) {
        //~ echo "[Debug]: esiste il file temporaneo <br />";

        if (copy($_FILES['fditta_logo']['tmp_name'],
                 IMG_PATH . $_FILES['fditta_logo']['name'])) {
            $d->SessionLog("L'upload del file " . $_FILES['fditta_logo']['name']
                    . " &egrave; stato eseguito correttamente.");

            $file_stat .= "<label class=\"ok\">L'upload del file "
                    . $_FILES['fditta_logo']['name'] . " &egrave; stato
					eseguito correttamente.</label><br />";
            $_POST['ditta_logo'] = "img/" . $_FILES['fditta_logo']['name'];
        } else {
            $d->SessionLog("Si &egrave; verificato un errore durante l'upload del file: "
                    . $_FILES['fditta_logo']['name']);
            $file_stat = "<label class=\"err\">Si &egrave; verificato un errore
					durante l'upload del file: " . $_FILES['fditta_logo']['name'] . "</label><br />";
        }
    }

    //$Status = $d->UpdateRow ($_POST, "", "tab_ditta", "id_ditta = '1'");
    foreach ($_POST as $key => $field) {
        //~ echo "[Debug]:   KEY: $key <br />";
        //~ echo "[Debug]: FIELD: $field <br />";
        //~ echo "[Debug]: update tab_config set val='$field' where opt='$key' <br />";
        $d->Query("update tab_config set val='$field' where opt='$key'", 1);
    }
}

$Province = $d->GetRows("*", "tab_province", "", "", "Prov, Targa", 1);
if (isset($_POST['ditta_id_prov']) && ($_POST['ditta_id_prov'] != "-")) {
    $Comuni = $d->GetRows("*", "tab_comuni",
                          "ID_Prov = '"
            . $_POST['ditta_id_prov'] . "'", "", "Comune", 1);
} else {
    $Comuni = $d->GetRows("*", "tab_comuni", "", "", "Comune", 1);
}


if (empty($_POST)) {
    $Settings = $d->GetRows("*", "tab_config", "", "", "id_config", 1);
    foreach ($Settings as $key => $field) {
        $_POST[$field['opt']] = $field['val'];
    }
}

//~ echo "[Debug]: ditta_id_prov: " . $_POST['ditta_id_prov'] . " <br />";
//~ echo "<pre>";
//~ print_r($_POST);
//~ echo "</pre>";
?>

<div id="search-result">
    <form method="post" action="<?php echo $_SERVER['REQUEST_URI']; ?>" enctype="multipart/form-data">
<?php $d->ShowEditBar("Anagrafica ditta:", $TOMOD, "save",
                         $_GET['act']); ?>

        <div>
<?php
$d->DrawControl("text", "ditta_rag_soc", "Ragione Sociale", "\w+", $_POST, null,
                "38.5em", "200");

echo "&nbsp;";
$d->DrawControl("text", "ditta_nome", "Nome Responsabile",
                "[a-z]{1}[a-z_]{2,29}", $_POST, null, "10em", "30");

echo "&nbsp;";
$d->DrawControl("text", "ditta_cognome", "Cognome Responsabile",
                "[a-z]{1}[a-z_]{2,29}", $_POST, null, "10em", "30");
?>
        </div>

        <div>
            <select name="stato" title="Seleziona stato">
                <option value="Italia">Italia</option>
            </select>

            <?php
            $d->DrawControl("select", "ditta_id_prov", "Seleziona provincia",
                            "\d+", $_POST, null, "auto", "",
                            "onchange=\"Javascript: posta('0', '" . $_SERVER['REQUEST_URI'] . "'); void(0);\"",
                            $Province, "ID", "Targa");
            ?>

            &nbsp;
<?php $d->DrawControl("select", "ditta_id_citta", "Seleziona citt&agrave;",
                          "\d+", $_POST, null, "auto", "", "", $Comuni, "ID",
                          "Comune");
?>

            &nbsp;
            <?php $d->DrawControl("text", "ditta_cap", "Cap", "\w+", $_POST,
                                      null, "6em", "6");
            ?>

            &nbsp;
            <?php $d->DrawControl("text", "ditta_indirizzo", "Indirizzo",
                                      "\w+", $_POST, null, "17.5em", "30");
            ?>
        </div>

        <div>
<?php $d->DrawControl("text", "ditta_piva", "Partita IVA o Codice Fiscale",
                          "\w+", $_POST, null, "16em", "16");
?>

            &nbsp;
<?php $d->DrawControl("text", "ditta_telefono", "Telefono", "\w+", $_POST,
                          null, "14em", "20");
?>


            &nbsp;
            <?php $d->DrawControl("text", "ditta_fax", "Fax", "\w+", $_POST,
                                      null, "11.5em", "20");
            ?>

            &nbsp;
            <?php $d->DrawControl("text", "ditta_cellulare", "Cellulare",
                                      "\w+", $_POST, null, "16em", "20");
            ?>
        </div>

        <div>
<?php
$d->DrawControl("text", "ditta_mail", "Indirizzo E-Mail",
                "^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$", $_POST,
                null, "30em", "70");
?>

            &nbsp;
            <?php $d->DrawControl("text", "ditta_iban", "Codice IBAN", "",
                                      $_POST, null, "30em", "200");
            ?>
        </div>

        <div><br /></div>


        <?php $d->ShowEditBar("Compilazione documento:", $TOMOD,
                                 "save", $_GET['act']); ?>

        <div style="text-align: right; float: left; width: 14em;">
            <label>Mostra codici</label>:
        </div>

        <div>
            <?php
            $d->DrawControl("radio", "ditta_doc_comp_codice", "Si", "", $_POST,
                            0);

            $d->DrawControl("radio", "ditta_doc_comp_codice", "No", "", $_POST,
                            1);
            ?>
        </div>

        <div style="text-align: right; float: left; width: 14em;">
            <label>Mostra descrizione</label>:
        </div>

        <div>
<?php
$d->DrawControl("radio", "ditta_doc_comp_descr", "Si", "", $_POST, 0);

$d->DrawControl("radio", "ditta_doc_comp_descr", "No", "", $_POST, 1);
?>
        </div>

        <div style="text-align: right; float: left; width: 14em;">
            <label>Mostra note</label>:
        </div>

        <div>
<?php
$d->DrawControl("radio", "ditta_doc_comp_note", "Si", "", $_POST, 0);

$d->DrawControl("radio", "ditta_doc_comp_note", "No", "", $_POST, 1);
?>
        </div>

        <div style="text-align: right; float: left; width: 14em;">
            <label>Mostra imponibile</label>:
        </div>

        <div>
<?php
$d->DrawControl("radio", "ditta_doc_comp_imp", "Si", "", $_POST, 0);

$d->DrawControl("radio", "ditta_doc_comp_imp", "No", "", $_POST, 1);
?>
        </div>

        <div style="text-align: right; float: left; width: 14em;">
            <label>Mostra IVA</label>:
        </div>

        <div>
<?php
$d->DrawControl("radio", "ditta_doc_comp_iva", "Si", "", $_POST, 0);

$d->DrawControl("radio", "ditta_doc_comp_iva", "No", "", $_POST, 1);
?>
        </div>

        <div style="text-align: right; float: left; width: 14em;">
            <label>Mostra sconto</label>:
        </div>

        <div>
        <?php
        $d->DrawControl("radio", "ditta_doc_comp_sco", "Si", "", $_POST, 0);

        $d->DrawControl("radio", "ditta_doc_comp_sco", "No", "", $_POST, 1);
        ?>
        </div>

            <?php
            // TODO: 
            // aggiungere i 2 sconti dopo aver inserito le colonne 
            // corrispondenti nella tabella dettaglio_doc 
            ?>

        <div style="text-align: right; float: left; width: 14em;">
            <label>Mostra imponibile totale</label>:
        </div>

        <div>
<?php
$d->DrawControl("radio", "ditta_doc_comp_tot", "Si", "", $_POST, 0);

$d->DrawControl("radio", "ditta_doc_comp_tot", "No", "", $_POST, 1);
?>
        </div>

        <div><br /></div>

<?php $d->ShowEditBar("Impostazioni documento:", $TOMOD, "save",
                         $_GET['act']); ?>

        <div>
            <label class="ok">Logo attuale</label>:
            <img src="<?php echo $_POST['ditta_logo']; ?>" alt="ditta_logo"
                 />

            <label class="err">Carica nuovo logo</label>:
            <input name="fditta_logo" type="file" style="width: 30em;"/>
        </div>

        <div>
            <label>Personalizzazione documento</label>:

            &nbsp;
            <a onclick="Javascript: window.open('frm_pdf_documenti.php?id_doc=0', '_blank'); void(0);" 
               title="Anteprima documento">
                <img src="<?php echo IMG_PATH; ?>Links/pdf.png" alt="Pdf" />
                Anteprima
            </a>
        </div>

        <div style="text-align: right; float: left; width: 16.5em;">
            <label>Orientamento pagina</label>:
        </div>	

        <div>
            <?php
            $d->DrawControl("radio", "ditta_doc_orientamento", "Verticale", "",
                            $_POST, "P");

            $d->DrawControl("radio", "ditta_doc_orientamento", "Orizzontale",
                            "", $_POST, "L");
            ?>
        </div>

        <div style="text-align: right; float: left; width: 16.5em;">
            <label>Mostra logo</label>:
        </div>

        <div>
            <?php
            $d->DrawControl("radio", "ditta_doc_show_logo", "Si", "", $_POST, 0);

            $d->DrawControl("radio", "ditta_doc_show_logo", "No", "", $_POST, 1);
            ?>
        </div>

        <div style="text-align: right; float: left; width: 16.5em;">
            <label>Mostra codice fornitore</label>:
        </div>

        <div>
<?php
$d->DrawControl("radio", "ditta_doc_cod_forn", "Si", "", $_POST, 0);

$d->DrawControl("radio", "ditta_doc_cod_forn", "No", "", $_POST, 1);
?>

            &nbsp;
            <label>Larghezza colonna ( mm )</label>:
            <?php $d->DrawControl("text", "ditta_doc_cod_forn_width",
                                      "Larghezza colonna", "\w+", $_POST, null,
                                      "5em", "5");
            ?>
        </div>

        <div style="text-align: right; float: left; width: 16.5em;">
            <label>Mostra codice interno</label>:
        </div>

        <div>
<?php
$d->DrawControl("radio", "ditta_doc_cod_int", "Si", "", $_POST, 0);

$d->DrawControl("radio", "ditta_doc_cod_int", "No", "", $_POST, 1);
?>

            &nbsp;
            <label>Larghezza colonna ( mm )</label>:
            <?php $d->DrawControl("text", "ditta_doc_cod_int_width",
                                      "Larghezza colonna", "\w+", $_POST, null,
                                      "5em", "5");
            ?>
        </div>

        <div style="text-align: right; float: left; width: 16.5em;">
            <label>Mostra descrizione</label>:
        </div>

        <div>
<?php
$d->DrawControl("radio", "ditta_doc_descr", "Si", "", $_POST, 0);

$d->DrawControl("radio", "ditta_doc_descr", "No", "", $_POST, 1);
?>

            &nbsp;
            <label>Larghezza colonna ( mm )</label>:
<?php $d->DrawControl("text", "ditta_doc_desc_width", "Larghezza colonna",
                          "\w+", $_POST, null, "5em", "5");
?>
        </div>

        <div style="text-align: right; float: left; width: 16.5em;">
            <label>Mostra note articolo</label>:
        </div>

        <div>
<?php
$d->DrawControl("radio", "ditta_doc_note_art", "Si", "", $_POST, 0);

$d->DrawControl("radio", "ditta_doc_note_art", "No", "", $_POST, 1);
?>

            &nbsp;
            <label>Larghezza colonna ( mm )</label>:
<?php
$d->DrawControl("text", "ditta_doc_note_art_width",
                "Larghezza colonna ( il campo viene stampato sul rigo seguente all'articolo )",
                "\w+", $_POST, null, "5em", "5");
?>
        </div>

        <div style="text-align: right; float: left; width: 16.5em;">
            <label>Mostra quantit&agrave;</label>:
        </div>

        <div>
            <?php
            $d->DrawControl("radio", "ditta_doc_qta", "Si", "", $_POST, 0);

            $d->DrawControl("radio", "ditta_doc_qta", "No", "", $_POST, 1);
            ?>

            &nbsp;
            <label>Larghezza colonna ( mm )</label>:
            <?php $d->DrawControl("text", "ditta_doc_qta_width",
                                      "Larghezza colonna", "\w+", $_POST, null,
                                      "5em", "5");
            ?>
        </div>

        <div style="text-align: right; float: left; width: 16.5em;">
            <label>Mostra prezzo articolo</label>:
        </div>

        <div>
            <?php
            $d->DrawControl("radio", "ditta_doc_prz", "Si", "", $_POST, 0);

            $d->DrawControl("radio", "ditta_doc_prz", "No", "", $_POST, 1);
            ?>

            &nbsp;
            <label>Larghezza colonna ( mm )</label>:
<?php $d->DrawControl("text", "ditta_doc_prz_width", "Larghezza colonna",
                          "\w+", $_POST, null, "5em", "5");
?>
        </div>

        <div style="text-align: right; float: left; width: 16.5em;">
            <label>Mostra sconto articolo</label>:
        </div>

        <div>
            <?php
            $d->DrawControl("radio", "ditta_doc_sco", "Si", "", $_POST, 0);

            $d->DrawControl("radio", "ditta_doc_sco", "No", "", $_POST, 1);
            ?>

            &nbsp;
            <label>Larghezza colonna ( mm )</label>:
<?php $d->DrawControl("text", "ditta_doc_sco_width", "Larghezza colonna",
                          "\w+", $_POST, null, "5em", "5");
?>
        </div>

        <div style="text-align: right; float: left; width: 16.5em;">
            <label>Mostra importo articolo</label>:
        </div>

        <div>
            <?php
            $d->DrawControl("radio", "ditta_doc_importo", "Si", "", $_POST, 0);

            $d->DrawControl("radio", "ditta_doc_importo", "No", "", $_POST, 1);
            ?>

            &nbsp;
            <label>Larghezza colonna ( mm )</label>:
<?php $d->DrawControl("text", "ditta_doc_importo_width",
                          "Larghezza colonna", "\w+", $_POST, null, "5em", "5");
?>
        </div>

        <div style="text-align: right; float: left; width: 16.5em;">
            <label>Mostra iva articolo</label>:
        </div>

        <div>
<?php
$d->DrawControl("radio", "ditta_doc_iva", "Si", "", $_POST, 0);

$d->DrawControl("radio", "ditta_doc_iva", "No", "", $_POST, 1);
?>

            &nbsp;
            <label>Larghezza colonna ( mm )</label>:
    <?php $d->DrawControl("text", "ditta_doc_iva_width",
                              "Larghezza colonna", "\w+", $_POST, null, "5em",
                              "5");
    ?>
        </div>

        <div><br /></div>

        <!-- ditta_layout_doc !-->

        <div><br /></div>

<?php //$d->ShowEditBar("Impostazioni Offerta:", $TOMOD, "save", $_GET['act'], 2);  ?>



    </form>

<?php
//~ $temp_struct = $d->Query("describe tab_config", 0);
//~ echo "<pre>";
//~ print_r($temp_struct);
//~ echo "</pre>";
//~ $temp_struct = $d->GetRows ("*", "tab_config", "", "", "", 0);
//~ echo "<pre>";
//~ print_r($temp_struct);
//~ echo "</pre>";
?>

</div>





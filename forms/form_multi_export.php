<?php
    $_SESSION['ExportArray'] = $_POST;
    
    $Tabelle = $d->GetRows(
        "descrizione, tabella",
        "tab_switch",
        "",
        "descrizione",
        "descrizione",
        1
    );
?>

<form method="post" action="<?php echo $_SERVER['REQUEST_URI']; ?>">
	<div>
		<h2>Seleziona tabelle e campi da esportare:</h2>
		<?php
            $d->DrawControl(
                "checkbox",
                "mostra_in_colonne",
                "<label>Mostra la selezione dei campi in colonne</label>",
                "",
                $_POST,
                "",
                "10em",
                "",
                "onclick=\"javascript: posta('0', '#'); void(0);\""
            );
        ?>
	</div>

	<div>
		<?php foreach ($Tabelle as $key => $field) {
            $d->DrawControl(
                "checkbox",
                $field['tabella'],
                "<label>" . $field['descrizione'] . "</label>",
                "",
                $_POST,
                "tabella",
                "10em",
                "",
                "onclick=\"javascript: posta('0', '#'); void(0);\""
            ); ?>
			
			<?php
            if (isset($_POST[$field['tabella']])) {
                $Campi = $d->GetRows(
                    "*",
                    "tab_switch",
                    "tabella = '" . $field['tabella'] . "'",
                    "",
                    "etichetta",
                    1
                );
                    
                echo "<div>";
                
                $i = 1;
                foreach ($Campi as $keyc => $fieldc) {
                // indentazione
                    echo "&nbsp;&nbsp;&nbsp;&nbsp;";
                    
                    $d->DrawControl(
                        "checkbox",
                        $fieldc['tabella'] . "+" . str_replace(" ", "_", $fieldc['nome_campo']),
                        $fieldc['etichetta'],
                        "",
                        $_POST,
                        "campo",
                        "10em",
                        "",
                        "onclick=\"javascript: posta('0', '#'); void(0);\""
                    );
                    
                    if (isset($_POST['mostra_in_colonne'])) {
                        if (($i % 4) == 0) {
                            echo "<br />";
                        }
                    } else {
                        echo "<br />";
                    }
                                    
                    $i += 1;
                }
                
                echo "</div>";
            }
            echo "<br /><br />";
}
        ?>
	</div>
	
	<div id="toolbar">
		<input name="EsportaGen" type="button" value="Esporta generalit&agrave;" 
			onclick="javascript: window.open('form_export_generalita.php', '_blank'); void(0);" />
			
		<input name="Esporta" type="button" value="Esporta ricerca" 
			onclick="javascript: window.open('form_multi_export_xls.php', '_blank'); void(0);" />
			
		<input name="Azzera" type="button" value="Azzera parametri" 
			onclick="javascript: go_conf2('Azzerare i criteri di esportazione?', '?gruppo=admin&act=export');" />
	</div>
	
</form>

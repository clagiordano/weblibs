<?php
/*
 * Form di accesso multi account con layout css3 integrato.
 * 
 * TODO aggiungere checkbox per salvare la sessione di accesso !? "ricordami" che salva un cookie
 * TODO convertire selettore modalità di accesso in select !?
 * 
 * Copyright 2008 - 2014 Claudio Giordano <claudio.giordano@autistici.org>
 *
 */

if (filter_input(INPUT_POST, 'username') != "") {
    $a->logIn(
        filter_input(INPUT_POST, 'username'),
        filter_input(INPUT_POST, 'password'),
        SESSION_PREFIX
    );
}
?>
<style type="text/css">
    #login-container {
        display: block;
        /*border: 2px solid white;*/
        vertical-align: top;
    }

    #login-methods {
        position: relative;
        /*border: 2px solid red;*/
        display: block;
        float: left;
        min-height: 150px;
        width: 49%;
    }

    #login-form {
        position: relative;
        /*border: 2px solid green;*/
        display: block;
        float: right;
        min-height: 150px;
        width: 49%;
        text-align: center;
    }

    #login-form input {
        height: 30px;
        width: 250px;
        margin: 2px;
        border-radius: 5px;
        box-shadow: inset 0 3px 8px rgba(0,0,0,.4);
        font-weight: bold;
    }

    #login-form-username {
        background-image: url('../weblibs/img/dialog/avatar-default-symbolic.svg');
        background-repeat: no-repeat;
        background-position: right;
    }

    #login-form-password {
        background-image: url('../weblibs/img/dialog/dialog-password-symbolic.svg');
        background-repeat: no-repeat;
        background-position: right;
    }

    #login-form-signin {
        box-shadow: 0 3px 8px rgba(0,0,0,.4);
    }

    #login-status {
        position: relative;
        display: block;
        width: 100%;
        /*border: 2px solid blue;*/
        min-height: 20px;
        clear: both;
    }

</style>

<form method="post" action="<?php echo filter_input(INPUT_SERVER, 'REQUEST_URI'); ?>">
    <div id="login-container" style="width: 100%;">
        <?php if (LOGIN_ACCESS_STATUS == 0) {
?>
            <?php if (LOGIN_ALLOW_SOCIAL === true) {
?>
                <div id="login-methods">
                    <dl>
                        <dt><input type="radio" name="login-methods" /><img src="img/logo_gn2.ico" /> Accesso locale</dt>
                        <dt>Accesso Google</dt>
                        <dt>Accesso Facebook</dt>
                        <dt>Accesso altri</dt>
                    </dl>
                </div>
            <?php
} ?>
            <div id="login-form">
                <input id="login-form-username" name="username" type="text" 
                       tabindex="1" required placeholder="Username" />
                <input id="login-form-password" name="password" type="password" 
                       tabindex="2" required placeholder="Password" />

                <br /><br />
                <a href="?act=forgotpass">Password dimenticata?</a>

                <?php if (LOGIN_ALLOW_REGISTER === true) {
?>
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <a href="?act=register">Registrati</a>
                <?php
} ?>

                <br /><br />
                <input id="login-form-signin" type="submit" value="Accedi" tabindex="3" />
            </div>

        <?php
} else {
?>
            <div id="login-status">
                <label class="err">
                    Accesso temporaneamente disabilitato per manutenzione.
                </label>
            </div>
        <?php
} ?>

        <div id="login-status" 
                <?php if ($a->ErrorsOccurred === true) {
                    echo "class=\"err\"";
} else {
    echo "class=\"ok\"";
} ?> >
<?php if (isset($a->ErrorsOccurred)) {
    echo $a->ResultStatus;
} ?>
        </div>
    </div>
</form>


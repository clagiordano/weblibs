<?php
	$loc = array( 0 => array('40.533333, 15.316667', '38.116667, 13.366667'),
					1 => array('38.1, 13.35', '37.816667, 13.583333'),
					2 => array('38.1, 13.383333', '38.1, 13.366667'),
					3 => array('38.1, 13.383333', '37.933333, 12.483333'),
					4 => array('38.1, 13.366667', '38.116667, 13.333333'));

	echo "<pre>Example Location:";
		print_r($loc);
	echo "</pre>";
?>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAxB1PQeiSxT8fjQz4cwx770zRwtSFGzio&sensor=false" type="text/javascript"></script>

<script type="text/javascript">

    var geocoder, location1, location2, gDir;

	function initialize()
	{
		geocoder = new GClientGeocoder();
		gDir = new GDirections();
		GEvent.addListener(gDir, "load", function() {
			var drivingDistanceMiles = gDir.getDistance().meters / 1609.344;
			var drivingDistanceKilometers = gDir.getDistance().meters / 1000;
			document.getElementById('results').innerHTML = '<strong>Address 1: </strong>' + location1.address + ' (' + location1.lat + ':' + location1.lon + ')<br /><strong>Address 2: </strong>' + location2.address + ' (' + location2.lat + ':' + location2.lon + ')<br /><strong>Driving Distance: </strong>' + drivingDistanceMiles + ' miles (or ' + drivingDistanceKilometers + ' kilometers)';
		});
	}

	function showLocation(L1, L2)
	{
/*
		alert(L1 + " to " + L2)
*/
		geocoder.getLocations(L1, function (response) {
			if (!response || response.Status.code != 200)
			{
				alert("Sorry, we were unable to geocode the first address");
			}
			else
			{
				location1 = {lat: response.Placemark[0].Point.coordinates[1], lon: response.Placemark[0].Point.coordinates[0], address: response.Placemark[0].address};
				geocoder.getLocations(L2, function (response) {
					if (!response || response.Status.code != 200)
					{
						alert("Sorry, we were unable to geocode the second address");
					}
					else
					{
						location2 = {lat: response.Placemark[0].Point.coordinates[1], lon: response.Placemark[0].Point.coordinates[0], address: response.Placemark[0].address};
						gDir.load('from: ' + location1.address + ' to: ' + location2.address);
					}
				});
			}
		});
	}
</script>

<body onload="initialize()">

<form action="#" onsubmit="showLocation(); return false;">
      <p>
        <input type="text" name="address1" value="Address 1" class="address_input" size="40" />
        <input type="text" name="address2" value="Address 2" class="address_input" size="40" />
        <input type="submit" name="find" value="Search" />
      </p>
    </form>
    <p id="results"></p>





<?php foreach ($loc as $key => $field) { ?>
	<script type="text/javascript">
		initialize();
		showLocation('<?php echo $field[0]; ?>', '<?php echo $field[1]; ?>')
	</script>
<?php } ?>


</body>

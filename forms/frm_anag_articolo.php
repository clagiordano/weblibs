<?php
   /**
    * Modulo per l'inserimento e la modifica dell'anagrafica di un articolo
    *
    * Copyright 2008 - 2014 Claudio Giordano <claudio.giordano@autistici.org>
    *
    */

    /**
     * Array per i select
     */
    $Um             = $d->GetRows( "*", "tab_um", "", "id_um");
    $Depositi       = $d->GetRows( "*", "tab_depositi", "", "dep");
    $TipoArt        = $d->GetRows( "*", "tab_tipo_art", "", "desc_art");
	$Categorie      = $d->GetRows( "*", "tab_cat_merce", "", "cat");
    $Iva            = $d->GetRows( "*", "tab_iva", "", "id_iva");
	$Fornitori      = $d->GetRows( "*", "tab_anagrafica", "id_tipo_anagrafica = '1'", "", "ragione_sociale");
    
	$ConfSalva      = "Salvare i dati inseriti?";
	$UrlSalva       = "?act=articolo&amp;salva=salva";
	$_POST['data']  = date('Y-m-d');
    
//    echo "[DEBUG]: GET TOMOD: '" . $_GET['tomod'] . "' <br />";
    $TOMOD          = filter_input( INPUT_GET, 'tomod', FILTER_VALIDATE_INT );
//    echo "[DEBUG]: TOMOD: '$TOMOD' <br />";

	if (isset($_GET['add']))
	{
		if (!isset($_SESSION['documento']['dettaglio']))
		{
			$_SESSION['documento']['dettaglio'] = array();
		}

		//~ Estraggo le informazioni riguardo il prodotto secondo l'id:
		$dett_prodotto = $d->GetRows ("*", "view_prodotti", "id_prodotto = '" . $_GET['add'] . "'");

		// id 5: fattura vendita
		if ($_SESSION['documento']['tipo_doc'] == "5")
		{
				$prezzo = "prz_ve";
		}else{
				$prezzo = "prz_acq";
		}

		array_push($_SESSION['documento']['dettaglio'],
                        array("id" => $_GET['add'],
                           "marca" => $dett_prodotto[0]['marca'],
                         "modello" => $dett_prodotto[0]['modello'],
							 "prz" => sprintf("%." . PRECISIONE_DECIMALI . "f", $dett_prodotto[0][$prezzo]),
							"disp" => $dett_prodotto[0]['disp'],
                        "cod_forn" => $dett_prodotto[0]['cod_forn'],
							 "sco" => sprintf("%." . PRECISIONE_DECIMALI . "f", $dett_prodotto[0]['sco']),
							 "qta" => sprintf("%." . PRECISIONE_DECIMALI . "f", 1),
                         "cod_int" => $dett_prodotto[0]['cod_int'],
                        "desc_iva" => $dett_prodotto[0]['desc_iva'],
                             "iva" => $dett_prodotto[0]['iva']));

		$result = "<label class=\"ok\"></label>"; ?>

		<script type="text/javascript">
			alert('Articolo aggiunto al dettaglio.');
			window.opener.location.href="home.php?act=documenti";
			ClosePopup();
		</script>
	<?php }

	if (isset($_GET['salva']))
	{
		if ($_POST['id_cat'] == "-")
		{
			// Assegna la categoria generica con id 1.
			$_POST['id_cat'] = 1;
		}

		if ($_POST['id_dep'] == "-")
		{
			// Assegna deposito generico con id 1.
			$_POST['id_dep'] = 1;
		}

		if (isset($_POST['ck_com']))
		{
			$_POST['ck_com'] = 0;
		}else{
			$_POST['ck_com'] = 1;
		}

		$errori = "";

		if (trim($_POST['cod_int'] == ""))
		{
			$errori .= "Errore, il codice interno non può essere vuoto.<br />";
		}else{
			// Verifico l'unicità del codice interno:
			$unic_cod_int = $d->GetRows("*", "tab_prodotti",
				"cod_int = '" . $_POST['cod_int'] . "'");
			if ( !isset( $TOMOD ) && count( $unic_cod_int ) != 0 )
			{
				$errori .= "Errore, il codice interno è già in uso.<br />";
			}
		}

		//~ if ((trim($_POST['marca']) == ""))
		//~ {
			//~ $errori .= "Errore, la marca non può essere vuota.<br />";
		//~ }

		//~ if ((trim($_POST['modello']) == ""))
		//~ {
			//~ $errori .= "Errore, la descrizione del prodotto non puo&ograve; essere vuota.<br />";
		//~ }

		if (($_POST['id_um'] == "-") || ($_POST['id_um'] == ""))
		{
			$errori .= "Errore, selezionare un'unità di misura.<br />";
		}

		if (($_POST['id_forn'] == "-") || ($_POST['id_forn'] == ""))
		{
			$errori .= "Errore, selezionare un fornitore per l'articolo.<br />";
		}

		if (($_POST['id_tipo'] == "-") || ($_POST['id_tipo'] == ""))
		{
			$errori .= "Errore, selezionare il tipo di articolo.<br />";
		}

		if ( ( $_GET['salva'] == "salva" ) 
                && ($errori == "") 
                && is_numeric( $_GET['tomod'] ) )
		{
			$_POST['cod_forn'] 	= strtoupper($_POST['cod_forn']);
			$_POST['cod_int'] 	= strtoupper($_POST['cod_int']);
#			$_POST['marca'] 		= ucfirst(strtolower($_POST['marca']));
			//~ $_POST['modello'] 	= ucfirst(strtolower($_POST['modello']));

			$Status = $d->SaveRow($_POST, "", "tab_prodotti", 1, "cod_int,cod_forn");
			$result = $Status[0];
			$id_nuovo_articolo =  $Status[1];
			//~ echo "Debug => id_nuovo_articolo: $id_nuovo_articolo <br />";
		}
        
        if ( ( $_GET['salva'] == "modifica" ) 
                && ( $errori == "" ) 
                && is_numeric( $_GET['tomod'] ) )
		{
			$_POST['cod_int'] = strtoupper($_POST['cod_int']);
#			$_POST['marca'] = ucfirst(strtolower($_POST['marca']));
			//~ $_POST['modello'] = ucfirst(strtolower($_POST['modello']));
			$result = $d->UpdateRow($_POST, "", "tab_prodotti", "id_prodotto = '" . $TOMOD . "'", 1);
		}
        else if ( ( $_GET['salva'] == "modifica" ) 
            && ( $errori == "" )
			&& is_string( $_GET['tomod'] ) ) 
        {
            //echo "[Debug]: Aggiornamento multiplo <br />";
			unset($id_list);
			$id_list = explode(";", $_GET['tomod']);

			foreach ($id_list as $key => $field)
			{
				if ( is_numeric( $field ) )
				{
					$Esclusioni = array('serial' => '', 'asset' => '');
					$Status = $d->UpdateRow( $_POST, $Esclusioni, "tab_prodotti", "id_prodotto = '$field'");
				}
			}
        }

		if ($errori != "")
		{
			echo "<label class=\"err\">$errori</label>";
		}
	}

	if ( isset( $_GET['id_art'] ) )
	{
		$_POST = $d->GetRows ("*", "tab_prodotti", "id_prodotto = '" . $_GET['id_art'] . "'");
	}

	if ( isset($TOMOD) && ($TOMOD != "") )
	{
		$UrlSalva = "?act=articolo&amp;salva=modifica&amp;tomod=" . $TOMOD;
		$ConfSalva = "Modificare i dati?";
		$row = $d->GetRows ("*", "tab_prodotti", "id_prodotto = '" . $TOMOD . "'");
        
		foreach ($row[0] as $key => $field)
		{
			$_POST[$key] = $field;
		}
	}
    else if ( $_GET['tomod'] == "multi" ) 
    {
        ?><script type="text/javascript">
            getMultiSelect( 'multisel', 'frm_anag_articolo.php?tomod=' );
        </script><?php
    }
    else
    {
        // Importo i dati del primo elemento da modificare:
		if (isset($_GET['tomod']))
		{
			unset($id_list);
			$id_list = explode(";", $_GET['tomod']);
			// echo "[Debug]: first element: " . $id_list[0] . " <br />";
			$row = $d->GetRows("*", "tab_prodotti", "id_prodotto = '" . $id_list[0] . "'" );
            
            $_POST = $row[0];
//            foreach ($row[0] as $key => $field)
//            {
//                $_POST[$key] = $field;
//            }
		}
    }
?>

<script type="text/javascript">
	function SetCodInt(cod_int, cod_forn, target)
	{
		cint = document.getElementsByName(cod_int)[0].value;
		cforn = document.getElementsByName(cod_forn)[0].value
		document.getElementsByName(target)[0].value = cforn + "-" + cint;
	}
</script>

<div id="search-form">
	<form method="post" action="<?php echo $_SERVER['REQUEST_URI']; ?>">
		<table style="width: auto;" class="form">
            
			<?php if ( !isset( $_GET['act'] ) 
                    && ( $_GET['tomod'] != "multi" ) ) { ?>
				<tr>
					<td colspan="2" style="text-align: right;">
                        <a onclick="Javascript: go('frm_ricerca_articoli.php?subact=documenti'); void(0);">
                            <img src="..<?php echo IMG_PATH ?>Links/prev.png" alt="prev" />
                            <label class="ok" style="cursor: pointer;">Indietro</label>
                        </a>

                        <a onclick="Javascript: ClosePopup(); void(0);">
                            [ <label class="err" style="cursor: pointer;">Chiudi</label> ]
                        </a>
					</td>
				</tr>
            <?php } else if ( $_GET['tomod'] == "multi" ) { ?>
                <tr>
                    <td colspan="2" style="text-align: right;">
                        <a onclick="Javascript: ClosePopup(); void(0);">
                            [ <label class="err" style="cursor: pointer;">Chiudi</label> ]
                        </a>
                    </td>
                </tr>
			<?php
				$d->ShowEditBar("Anagrafica articolo:", $TOMOD, "new,save", "frm_anag_articolo.php");
			}else{
				$d->ShowEditBar("Anagrafica articolo:", $TOMOD, "new,save", $_GET['act']);
			}
			?>

			<tr>
				<td style="text-align: right;" colspan="2">
					<?php  if (isset($result)) { echo $result; } ?>
				</td>
			</tr>

			<tr>
                <th colspan="2">
					<?php if (isset($id_nuovo_articolo)) { ?>
						&nbsp;
						<a href="javascript: go_conf2('Aggiungere l\'articolo al documento?',
							'<?php echo $_SERVER['REQUEST_URI']; ?>&amp;add=<?php echo $id_nuovo_articolo; ?>'); void(0);" >
							<img src="<?php echo IMG_PATH ?>Links/add.png" alt="Aggiungi Articolo al documento" />
							Articolo al documento
						</a>
					<?php } ?>
				</th>
			</tr>

            <?php if ( is_numeric( $_GET['tomod'] ) 
                    || ( !isset( $_GET['tomod'] ) ) ) { ?>
                <tr>
                    <th style="text-align: right;">Codice Fornitore:&nbsp;</th>
                    <td>
                        <?php $d->DrawControl("text", "cod_forn", "Codice Fornitore", 
                            "^[a-zA-Z0-9-_\ \.]{2,20}$", $_POST, null, "28em", "20",
                            "onkeyup=\"SetCodInt('cod_forn', 'id_forn', 'cod_int');\""); ?>
                    </td> 
                </tr>

                <tr>
                    <th style="text-align: right;">Codice Interno:&nbsp;</th>
                    <td>
                        <?php $d->DrawControl("text", "cod_int", "Codice Interno", 
                            "^[a-zA-Z0-9-_\ \.]{2,20}$", $_POST, null, "28em", "25"); ?>
                    </td> 
                </tr>

                <tr>
                    <th style="text-align: right;">Codice Barcode:&nbsp;</th>
                    <td>
                        <?php $d->DrawControl("text", "barc_forn", "Codice Barcode", 
                            "", $_POST, null, "28em", "30"); ?>
                    </td> 
                </tr>
                
                <tr>
                    <th style="text-align: right;">Marca:&nbsp;</th>
                    <td>
                        <?php $d->DrawControl("text", "marca", "Marca", 
                            "", $_POST, null, "28em", "50"); ?>
                    </td> 
                </tr>

                <tr>
                    <th style="text-align: right;">Descrizione:&nbsp;</th>
                    <td>
                        <?php $d->DrawControl("text", "modello", "Descrizione", 
                            "", $_POST, null, "28em", "150"); ?>
                    </td> 
                </tr>
            <?php } ?>
            
            <tr>
                <th style="text-align: right;">Unit&agrave;:&nbsp;</th>
				<td>
					<?php $d->DrawControl("select", "id_um", "Seleziona unit&agrave;", 
						"\d+", $_POST, null, "28.7em", "", "", $Um, "id_um", "um"); ?>
                    
                    <a onclick="Javascript: ShowFormDiv('frm_multi_edit.php?type=um',
							'150px', '150px', '500px', '280px', 'popup'); void(0);">
						<img src="<?php echo IMG_PATH ?>Links/edit.png" alt="edit" title="Gestisci unit&agrave; di misura" />
					</a>
				</td>
			</tr>
            
            <tr>
                <th style="text-align: right;">Deposito:&nbsp;</th>
				<td>
					<?php $d->DrawControl("select", "id_dep", "Seleziona deposito", 
						"\d+", $_POST, null, "28.7em", "", "", $Depositi, "id_dep", "dep"); ?>
                    
                    <a onclick="Javascript: ShowFormDiv('frm_multi_edit.php?type=dep',
							'150px', '150px', '500px', '280px', 'popup'); void(0);">
						<img src="<?php echo IMG_PATH ?>Links/edit.png" alt="edit" title="Gestisci depositi merci" />
					</a>
				</td>
			</tr>

            <tr>
                <th style="text-align: right;">Tipo:&nbsp;</th>
				<td>
					<?php $d->DrawControl("select", "id_tipo", "Seleziona tipo prodotto", 
                        "\d+", $_POST, null, "28.7em", "", "", $TipoArt, "id_art", "desc_art"); ?>
                    
                    <a onclick="Javascript: ShowFormDiv('frm_multi_edit.php?type=tipiprod',
							'150px', '150px', '500px', '280px', 'popup'); void(0);">
						<img src="<?php echo IMG_PATH ?>Links/edit.png" alt="edit" title="Gestisci tipi prodotto" />
					</a>
				</td>
			</tr>

            <tr>
                <th style="text-align: right;">Categoria:&nbsp;</th>
				<td>
					<?php $d->DrawControl("select", "id_cat", "Seleziona categoria prodotto", 
                        "\d+", $_POST, null, "28.7em", "", "", $Categorie, "id_cat", "cat"); ?>
                    
                    <a onclick="Javascript: ShowFormDiv('frm_multi_edit.php?type=cat',
							'150px', '150px', '500px', '280px', 'popup'); void(0);">
						<img src="<?php echo IMG_PATH ?>Links/edit.png" alt="edit" title="Gestisci tipi prodotto" />
					</a>
				</td>
			</tr>
            
            <?php if ( is_numeric( $_GET['tomod'] )
                    || ( !isset( $_GET['tomod'] ) ) ) { ?>
                <tr>
                    <th style="text-align: right;">Fornitore:&nbsp;</th>
                    <td>
                        <?php $d->DrawControl("select", "id_forn", "Seleziona fornitore", 
                            "\d+", $_POST, null, "28.7em", "", "", $Fornitori, "id_anag", "ragione_sociale"); ?>
                    </td>
                </tr>
            <?php } ?>
            
            <tr>
				<th style="text-align: right;">Ubicazione:&nbsp;</th>
				<td>
					<?php $d->DrawControl("text", "ubicazione", "Ubicazione", 
						"", $_POST, null, "28em", "150"); ?>
				</td> 
			</tr>

            <tr>
				<th style="text-align: right;">Corridoio:&nbsp;</th>
				<td>
					<?php $d->DrawControl("text", "corridoio", "Corridoio", 
						"", $_POST, null, "28em", "150"); ?>
				</td> 
			</tr>
            
            <tr>
				<th style="text-align: right;">Ripiano:&nbsp;</th>
				<td>
					<?php $d->DrawControl("text", "ripiano", "Ripiano", 
						"", $_POST, null, "28em", "150"); ?>
				</td> 
			</tr>
            
            <tr>
				<th style="text-align: right;">Prezzo di acquisto:&nbsp;</th>
				<td>
					<?php $d->DrawControl("text", "prz_acq", "Prezzo di acquisto", 
						"", $_POST, null, "28em", "150"); ?>
				</td> 
			</tr>

            <tr>
				<th style="text-align: right;">Prezzo di vendita:&nbsp;</th>
				<td>
					<?php $d->DrawControl("text", "prz_ve", "Prezzo di vendita", 
						"", $_POST, null, "28em", "150"); ?>
				</td> 
			</tr>
            
            <tr>
                <th style="text-align: right;">IVA:&nbsp;</th>
				<td>
					<?php $d->DrawControl("select", "id_iva", "Seleziona IVA", 
                        "\d+", $_POST, null, "28.7em", "", "", $Iva, "id_iva", "desc_iva"); ?>
                    
                    <a onclick="Javascript: ShowFormDiv('frm_multi_edit.php?type=iva',
							'150px', '150px', '500px', '280px', 'popup'); void(0);">
						<img src="<?php echo IMG_PATH ?>Links/edit.png" alt="edit" />
					</a>
				</td>
			</tr>

            <tr>
				<th style="text-align: right;">Soglia di riordino:&nbsp;</th>
				<td>
					<?php $d->DrawControl("text", "riordino", "Soglia di riordino", 
						"", $_POST, null, "28em", "150"); ?>
				</td> 
			</tr>
            
            <tr>
				<th style="text-align: right;">Ordinati fornitore:&nbsp;</th>
				<td>
					<?php $d->DrawControl("text", "ordinati", "Ordinati fornitore", 
						"", $_POST, null, "28em", "150"); ?>
				</td> 
			</tr>
            
            <tr>
				<th style="text-align: right;">Prodotto composto:&nbsp;</th>
				<td>
					<?php $d->DrawControl("checkbox", "ck_com", "Il prodotto &egrave; un composto?", 
						"", $_POST, null, "28em", "150"); ?>
				</td> 
			</tr>
            
            <tr>
				<th style="text-align: right; vertical-align: top;">Note:&nbsp;</th>
				<td>
					<?php $d->DrawControl("textarea", "note", "Note", 
						"", $_POST, null, "28em", "150"); ?>
				</td> 
			</tr>
		</table>
	</form>
</div>

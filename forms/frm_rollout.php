<?php
	$root = substr($_SERVER['DOCUMENT_ROOT'], 0, -1);
	require_once("$root/Function/Rollout.php");

	$Requests = $d->GetRows ("id_request, request, concat(den, \"
		(\", cod_fil, \")\") as filiale, data_pianif, cod_fil",
		"view_requests", "", "", "den, request", 1);
	$TipoMch = $d->GetRows ("*", "tab_tipi_mch", "", "", "tipo_mch", 1);

	$Marche = $d->GetRows ("distinct marca", "tab_macchine", "marca != ''",
		"marca", "", 1);
	$Modelli = $d->GetRows ("distinct modello", "tab_macchine", "modello != ''",
		"modello", "", 1);

	$Status = "";
	$errori = "";
	$LinkL = Array();
	if (!isset($_GET['tomod']))
	{
		$_GET['tomod'] = "";
		$Links = "";
	}else{
		$Links = "new,save";
	}


	if (is_numeric($_GET['tomod']))
	{
		array_push($LinkL, array('link' => "&nbsp;<a onclick=\"Javascript: go('home.php?act=gest_req&amp;tomod="
			. $_GET['tomod'] . "'); void(0);\" title=\"Modifica Dati Request\">
			<img src=\"/Images/Links/edit.png\" alt=\"edit\" />Modifica</a>"));

		array_push($LinkL, array('link' => "&nbsp;<a onclick=\"Javascript: window.open('frm_pdf_dett_request.php?rid="
			. $_GET['tomod'] . "', '_blank'); void(0);\" title=\"Stampa Riepilogo Request\">
			<img src=\"/Images/Links/pdf.png\" alt=\"pdf\" />Riepilogo</a>"));

		array_push($LinkL, array('link' => "&nbsp;<a onclick=\"Javascript: window.open('frm_pdf_ritiri.php?rid="
			. $_GET['tomod'] . "', '_blank'); void(0);\" title=\"Stampa Dettaglio Ritiri\">
			<img src=\"/Images/Links/pdf.png\" alt=\"pdf\" />Ritiri</a>"));

		array_push($LinkL, array('link' => "&nbsp;<a onclick=\"Javascript: window.open('frm_xls_ritiri_rollout.php', '_blank');
			void(0);\" title=\"Esporta Dettaglio Ritiri\">
			<img src=\"/Images/Links/xls.png\" alt=\"xls\" />Ritiri</a>"));

		array_push($LinkL, array('link' => "&nbsp;<a onclick=\"Javascript: window.open('frm_mail_ritiri_rollout.php', '_blank');
			void(0);\" title=\"Invia Mail Dettaglio Ritiri\">
			<img src=\"/Images/Links/mail.png\" alt=\"mail\" />Mail Ritiri</a>"));

		array_push($LinkL, array('link' => "&nbsp;<a onclick=\"Javascript: window.open('frm_mail_hwnew_rollout.php', '_blank');
			void(0);\" title=\"Invia Mail Mac Address\">
			<img src=\"/Images/Links/mail.png\" alt=\"mail\" />Mail Mac</a>"));
	}

	if (!empty($_POST) && isset($_GET['key'])
		&& is_numeric($_GET['key']))
	{
		$key = $_GET['key'];

		//~ echo "[Debug]: è una nuova macchina, verifico i campi <br />";
		//~ echo "[Debug]: tipo: " . $field['id_tipo_mch'] . " <br />";
		if ($_POST['tipo_mch' . $key] == "-")
		{
			$errori .= "Errore, è necessario selezionare il tipo di macchina. <br />";
		}

		if ($_POST['marca' . $key] == "-")
		{
			$errori .= "Errore, è necessario selezionare la marca. <br />";
		}

		if ($_POST['modello' . $key] == "-")
		{
			$errori .= "Errore, è necessario selezionare il modello. <br />";
		}

		//~ echo "[Debug]: campi: " . $field['serial'] . " <br />";
		//~ echo "[Debug]: campi: " . $field['asset'] . " <br />";
		if ((trim($_POST['serial' . $key]) == "") && (trim($_POST['asset' . $key]) == ""))
		{
			$errori .= "Errore, è necessario compilare almeno"
				. " uno dei campi fra serial ed asset. <br />";
		}

		// Recupero l'id del tipo di macchina selezionato:
		//~ echo "[Debug]: recupero dalla tabella il tipo
		//~ di macchina in base alla descrizione <br />";
		$mch = $d->GetRows ("*", "tab_tipi_mch", "tipo_mch = '"
			. $_POST['tipo_mch' . $key] . "'", "", 1);
		$_POST['id_tipo_mch' . $key] = $mch[0]['id_tipo_mch'];

		//~ echo "[Debug]: ci sono errori? : $errori <br />";
		/*
		 * Salvo la nuova macchina in tabella:
		 */
		if ($errori == "")
		{
/*
			if ($_POST['tipo_mch' . $key] == "Server")
			{
				//$provenienza = "MAINTENANCE";
				$provenienza = "DA FILIALE";
			}else{
				$provenienza = "DA FILIALE";
			}
*/

			// Check sui campi dei quali verificare l'unicità
			$check_fields = array();
			$check_string = "";

			if (strtoupper(trim($_POST['serial' . $key])) != "ND")
			{
				$_POST['serial' . $key] = str_replace("'", "-",
					strtoupper($_POST['serial' . $key]));
				array_push($check_fields, "serial");
			}else{
				//$_POST['serial' . $key] = "N.D.";
			}

			if (strtoupper(trim($_POST['asset' . $key])) != "ND"
				&& strtoupper(trim($_POST['asset' . $key])) != "N.D.")
			{
				$_POST['asset' . $key] = str_replace("'", "-",
					strtoupper($_POST['asset' . $key]));
				array_push($check_fields, "asset");
			}else{
				//$_POST['asset' . $key] = "N.D.";
			}

			if (count($check_fields) > 0)
			{
				$check_string = join(",", $check_fields);
			}else{
				$check_string = "";
			}

			/*if ($_POST['tipo_mch' . $key] != "Server")
			{
				$stato_mch = "5";
			}else{
				$stato_mch = "1";
			}*/

			if ($_POST['ck_funz' . $key] == "")
			{
				// apparecchiatura non funzionante
				$_POST['ck_funz' . $key] = "KO";
			}

			if ($_POST['note_acc' . $key] == "")
			{
				// accessori mancanti
				$_POST['note_acc' . $key] = "NO";
			}

			$stato_mch = "5";

			// Assegnazione classificazione hw class_hw
			if (preg_match("(^FL.*|00005.*)", $_POST['asset' . $key]))
			{
				//echo "[Debug]: (^FL.*|00005.*) BIANCO <br />";
				$class_hw = "BIANCO";
			}else{
				//echo "[Debug]: BLU <br />";
				$class_hw = "BLU";
			}

			$Valori = array('id_tipo_mch' => $_POST['id_tipo_mch' . $key],
							   'tipo_mch' => $_POST['tipo_mch' . $key],
							'provenienza' => $_POST['provenienza' . $key], //$provenienza,
										'marca' => $_POST['marca' . $key],
									'modello' => $_POST['modello' . $key],
									 'serial' => strtoupper($_POST['serial' . $key]),
										'asset' => strtoupper($_POST['asset' . $key]),
									 'pallet' => strtoupper($_POST['pallet' . $key]),
						 'id_magazzino' => $_SESSION['id_city'],
								  'ck_funz' => $_POST['ck_funz' . $key],
								 'note_acc' => $_POST['note_acc' . $key],
						 'id_stato_mch' => $stato_mch,
								 'class_hw' => $class_hw,
								 // date('Y-m-d'), NON PIÙ LA DATA ODIERNA MA LA DATA DI PIANIFICAZIONE DELLA FILIALE!
									'data_in' => $_SESSION['rollout']['data_pianif'],
							 //'dest_fil' => addslashes(stripslashes($_SESSION['rollout']['den'])),
							        'mac' => strtoupper($_POST['mac' . $key]));



			$Status = $d->SaveRow ($Valori, array('tipo_mch' => ''),
				"tab_macchine", 1, $check_string);

			/*
			 * Quindi recupero l'id della nuova macchina appena inserita,
			 * per il successivo inserimento nel dettaglio della request
			 */
			//~ echo "[Debug]: last_insert_id(): " . $Status[1] . " <br />";
			if (!is_numeric($Status[1]))
			{
				$errori .= "Errore, il salvataggio è stato annullato.<br />";
			}else{
				/*
				 * Modifico l'id macchina in sessione dopo il salvataggio corretto
				 * in modo che non sia più modificabile dal dettaglio request:
				 */
				//~ echo "[Debug]: modifico l'id in sessione dopo il salvataggio corretto' <br />";
				//~ echo "[Debug]: key: " . $_SESSION['rollout']['dettaglio'][$key] . " <br />";
				$_SESSION['rollout']['dettaglio'][$key] = $Valori;
				$_SESSION['rollout']['dettaglio'][$key]['id_macchina'] = $Status[1];
			}

			$Status = $Status[0];
		}
	}

	if (isset($_GET['salva']))
	{
		$errori = "";
		if (!is_numeric($_SESSION['rollout']['id_request']))
		{
			$errori .= "Errore, non è stata selezionata una request. <br />";
		}

/*
		if (count($_SESSION['rollout']['dettaglio']) < 1)
		{
			$errori .= "Errore, il dettaglio non contiene nessuna voce, salvataggio annullato. <br />";
		}
*/

		if ($_GET['salva'] == "salva" && $errori == "")
		{
			foreach ($_SESSION['rollout']['dettaglio'] as $key => $field)
			{
				$Valori = array('id_request' => $_SESSION['rollout']['id_request'],
					'id_macchina' => $field['id_macchina']);
				$Status = $d->SaveRow ($Valori, "", "tab_dett_requests", 1);
				$Status = $Status[0];
			}
			?>
			<script type="text/javascript">
				document.location.href="home.php?act=rollout&tomod=<?php
					echo $_POST['id_request']; ?>";
			</script>
			<?php
		}

		if ($_GET['salva'] == "modifica" && $errori == "")
		{
			//~ echo "[Debug]: salvataggio del dettaglio request : $errori <br />";
			//~ echo "[Debug]: ci sono errori2? : $errori <br />";
			if ($errori == "")
			{
				//~ Rimuovo il dettaglio precedente riferito a questo id:
				//~ echo "[Debug]: rimuovo il precedente dettaglio <br />";
				$Status = $d->DeleteRows ("tab_dett_requests", "id_request = '"
					. $_GET['tomod'] . "'", 1);

				foreach ($_SESSION['rollout']['dettaglio'] as $key => $field)
				{
					$Valori = array('id_request' => $_SESSION['rollout']['id_request'],
						'id_macchina' => $field['id_macchina']);
					$Status = $d->SaveRow ($Valori, "", "tab_dett_requests", 1);
					$Status = $Status[0];

					/*
					 * UPDATE DELLA TABELLA MACCHINE CON L'ID "CONSEGNATO IN FILIALE"
					 * PER LO SCARICO DEL MAGAZZINO
					 */

					if ($field['provenienza'] == "FORNITORE")
					{
						//echo "[Debug]: faccio l'update della data_out per la macchina: " . $field['serial'] . " <br />";
						$DatiUpdate = array('data_out' => $_SESSION['rollout']['data_pianif'],
																'id_stato_mch' => 4);
						$Status = $d->UpdateRow ($DatiUpdate, "", "tab_macchine",
							"id_macchina = '" . $field['id_macchina'] . "'");
					}
				}
			}
		}
	}

	if (isset($_GET['reset']) && ($_GET['reset'] == "0"))
	{
		//~ echo "[Debug]: resetto l'array rollout <br />";
		unset($_SESSION['rollout']);
	}

	if (isset($_GET['tomod']))
	{
		/*
		 * se si sta modificando la request, la prima volta importo il dettaglio,
		 * successivamente imposto il flag in sessione inmod che impedisce
		 * l'importazione del dettaglio durante la modifica.
		 *
		 */
		//~ echo "[Debug]: c'e' un id di modifica <br />";
		if (!isset($_SESSION['rollout']['inmod']))
		{
			//~ echo "[Debug]: non c'e' il flag di modifica impostato <br />";
			//~ echo "[Debug]: quindi importo i dati della request <br />";
			$res = $d->GetRows ("*", "view_requests", "id_request = '"
				. $_GET['tomod'] . "'", "", "request", 1);

			//~ print_r($res);
			//~ echo "<br />";

			$_SESSION['rollout']['id_request'] = $res[0]['id_request'];
			$_SESSION['rollout']['id_filiale'] = $res[0]['id_filiale'];
			$_SESSION['rollout']['cod_fil'] 	 = $res[0]['cod_fil'];
			$_SESSION['rollout']['rol_mail'] 	 = $res[0]['rol'] . ' <' . $res[0]['mail'] . '>';
/*
			echo "[Debug]: rol:" . $_SESSION['rollout']['rol_mail'] . " <br />";
*/
			$_SESSION['rollout']['den'] 			  = $res[0]['den'];
			$_SESSION['rollout']['data_pianif'] = $res[0]['data_pianif'];
			$_POST['id_request'] 								= $res[0]['id_request'];

			/*
			 * echo "[Debug]: importo il dettaglio delle macchine
			 * in base alla request <br />";
			 */
			$list_mch = $d->GetRows ("*", "tab_dett_requests", "id_request = '"
				. $_GET['tomod'] . "'", "", "", 1);

/*
			echo "<pre>";
				print_r($list_mch);
			echo "</pre>";
*/

			//~ echo "<br />";

			if (!isset($_SESSION['rollout']['dettaglio']))
			{
				//~ echo "[Debug]: inizializzo l'array del dettaglio <br />";
				$_SESSION['rollout']['dettaglio'] = array();
			}

			//~ echo "[Debug]: ciclo di riempimento array dettaglio <br />";
			foreach ($list_mch as $key => $field)
			{
				$mch = $d->GetRows ("*", "view_macchine", "id_macchina = '"
				 . $field['id_macchina'] . "'", "", "", 1);
				array_push($_SESSION['rollout']['dettaglio'], $mch[0]);
			}
			//~ print_r($_SESSION['rollout']['dettaglio']);
			//~ echo "<br />";

			//~ echo "[Debug]: imposto il flag di modifica <br />";
			$_SESSION['rollout']['inmod'] = "inmod";
		}
	}

	if (isset($_GET['svuota']) && ($_GET['svuota'] == "0"))
	{
		//~ echo "[Debug]: rimuovo tutte le voci dal dettaglio <br />";
		unset($_SESSION['rollout']['dettaglio']);
	}

	if (isset($_GET['del']))
	{
		//~ echo "[Debug]: rimuovo la singola voce dal dettaglio <br />";
		unset($_SESSION['rollout']['dettaglio'][$_GET['del']]);
	}

	if (isset($_POST['id_request']))
	{
		$_SESSION['rollout']['id_request'] = $_POST['id_request'];
	}

	if (isset($_GET['selected']))
	{
		if (!isset($_SESSION['rollout']['dettaglio']))
		{
			$_SESSION['rollout']['dettaglio'] = array();
		}

		$in_array = "falso";
		foreach ($_SESSION['rollout']['dettaglio'] as $key => $field)
		{
			if ($field['id_macchina'] == $_GET['selected'])
			{
				$in_array = "vero";
			}
		}

		if ($in_array == "falso")
		{
			//~ Estraggo le informazioni riguardo il prodotto secondo l'id':
			$dettaglio = $d->GetRows ("*", "view_macchine", "id_macchina = '"
				. $_GET['selected'] . "'");

			//~ aggiungo l'articolo al documento.<br />";
			array_push($_SESSION['rollout']['dettaglio'],
				array("id_macchina" => $dettaglio[0]['id_macchina'],
							"provenienza" => $dettaglio[0]['provenienza'],
										"marca" => $dettaglio[0]['marca'],
									"modello" => $dettaglio[0]['modello'],
									 "serial" => $dettaglio[0]['serial'],
										"asset" => $dettaglio[0]['asset'],
											"mac" => $dettaglio[0]['mac'],
									 "pallet" => $dettaglio[0]['pallet'],
									"ck_funz" => $dettaglio[0]['ck_funz'],
								 "note_acc" => $dettaglio[0]['note_acc'],
								 "tipo_mch" => $dettaglio[0]['tipo_mch']));
		}else{
			?>
				<script type="text/javascript">
					alert("Errore, la voce selezionata è già presente in dettaglio.");
				</script>
			<?php
		}
	}


	if (isset($_GET['blank']))
	{
		if (!isset($_SESSION['rollout']['dettaglio']))
		{
			$_SESSION['rollout']['dettaglio'] = array();
		}

		//~ aggiungo l'articolo VUOTO al documento.<br />";
		array_push($_SESSION['rollout']['dettaglio'],
			array("id_macchina" => 'new',
						"provenienza" => '',
									"marca" => '',
								"modello" => '',
								 "serial" => '',
									"asset" => '',
										"mac" => '',
								 "pallet" => '',
							  "ck_funz" => '',
							 "note_acc" => '',
					 "id_magazzino" => '',
							 "tipo_mch" => ''));

		asort($_SESSION['rollout']['dettaglio']);
	}
?>
<style type="text/css">
	.dettaglio input {
		text-align: left ! important
	};
</style>

<div id="search-form">
	<form method="post" action="<?php echo $_SERVER['REQUEST_URI']; ?>">
		<table class="form" style="width: 100%;">

			<?php $d->ShowEditBar("RollOut:", $_GET['tomod'], $Links,
					$_GET['act'], 8, 0, "#", $LinkL); ?>

			<tr>
				<th style="text-align: right; width: 18em;">
					Selezione request da compilare:
				</th>

				<td>
					<select name="id_request"
						<?php
							$ChangeMsg = "Selezionando un\'altra request, i dati attualmente inseriti verranno persi, continuare?"; ?>
							onchange="javascript: conferma_posta('0', '<?php echo $ChangeMsg; ?>',
								'home.php?act=rollout&amp;reset=0&amp;tomod=' + this.value,
								'<?php echo $_SERVER['REQUEST_URI']; ?>'); void(0);" >

						<option value="-">Selezione request</option>
						<?php foreach ($Requests as $key => $field) { ?>
							<option value="<?php echo $field['id_request']; ?>"
								<?php if ($_SESSION['rollout']['id_request'] == $field['id_request'])
								{
									echo "selected=\"selected\"";
								} ?> >
								<?php echo $field['filiale'] . " - " . $field['request']; ?>
							</option>
						<?php } ?>
					</select>

				<?php if (is_numeric($_GET['tomod'])) { ?>

					</td>
				</tr>

				<tr>
					<th style="text-align: right; vertical-align: top;" >
						Riepilogo Request:<br />
						Data Attività: <?php echo $d->Inverti_Data($_SESSION['rollout']['data_pianif']); ?>
					</th>
					<td>
						<?php	PrintCheckRequest($_SESSION['rollout']['id_filiale']); ?>
					</td>
				</tr>

				<tr><th colspan="8"><label class="err_status"><?php echo $errori; ?></label></th></tr>
				<tr><th colspan="8"><label class="ok"><?php echo $Status; ?></label></th></tr>

				<?php $d->ShowEditBar("Dettaglio Request:", $_GET['tomod'], "blank,clear",
					$_GET['act'], 8, count($_SESSION['rollout']['dettaglio'])); ?>

				<tr>
					<th colspan="8">
						Selezione Rapida Macchina:
						<input name="str" type="text" style="width: 20em;"
							onkeyup="Javascript: LiveSearch (this, 'lsearch', 'live_search2.php?str=' + this.value + '<?php
								if (isset($_GET['tomod'])) { echo "&amp;tomod=" . $_GET['tomod']; } ?>');"
							value="<?php  if (isset($_POST['str'])) { echo stripslashes($_POST['str']); } ?>" />

							<a onclick="Javascript: LiveSearch (forms[0].str, 'lsearch', 'live_search2.php?str=' + forms[0].str.value + '<?php
								if (isset($_GET['tomod'])) { echo "&amp;tomod=" . $_GET['tomod']; } ?>');">
								<img src="/Images/Links/find.png" alt="Find"
									title="Forza ricerca rapida" />
							</a>
					</th>
			</tr>
			<?php } ?>
		</table>
	</form>
</div>

<?php
/*
		echo "<pre> SESSION ROLLOUT ";
			print_r($_SESSION['rollout']);
		echo "</pre>";
*/
?>

<?php if (is_numeric($_SESSION['rollout']['id_request'])) { ?>
	<div id="search-result-7r">
		<form method="post" action="<?php echo $_SERVER['REQUEST_URI']; ?>">
			<table class="dettaglio" style="width: 100%;">
				<tr>
					<th>Provenienza:</th>
					<th>Tipo Macchina:</th>
					<th>Marca:</th>
					<th>Modello:</th>
					<th>Serial:</th>
					<th>Asset:</th>
					<th>Mac:</th>
					<!--<th>Pallet:</th>!-->
					<th>Funz:</th>
					<th>Acc:</th>
					<th><br /></th>
				</tr>
				<tr><th colspan="16"><hr /></th></tr>

				<?php if (isset($_SESSION['rollout']['dettaglio'])) {
					//~ echo "[Debug]: esiste l'array dettaglio <br />";
					foreach ($_SESSION['rollout']['dettaglio'] as $key => $field)
					{ ?>
						<tr>
							<?php if ($field['id_macchina'] == "new") { ?>
								<td>
									<select name="provenienza<?php echo $key; ?>">
										<option value="DA FILIALE"
											<?php if ($_POST['provenienza' . $key] == "DA FILIALE")
												{ echo "selected=\"selected\""; } ?> >DA FILIALE</option>
										<option value="FORNITORE"
											<?php if ($_POST['provenienza' . $key] == "FORNITORE")
												{ echo "selected=\"selected\""; } ?> >FORNITORE</option>
										<option value="IN LOCO"
											<?php if ($_POST['provenienza' . $key] == "IN LOCO")
												{ echo "selected=\"selected\""; } ?> >IN LOCO</option>
									</select>
								<td>
									<select name="tipo_mch<?php echo $key; ?>">
										<option value="-">Tipo macchina</option>
										<?php foreach ($TipoMch as $key_m => $field_m) { ?>
											<option value="<?php echo $field_m['tipo_mch']; ?>"
												<?php if ($_POST['tipo_mch' . $key] == $field_m['tipo_mch'])
													{ echo "selected=\"selected\""; } ?> >
												<?php echo htmlentities($field_m['tipo_mch']); ?>
											</option>
										<?php } ?>
									</select>
								</td>
								<td>
									<select name="marca<?php echo $key; ?>">
										<option value="-">Marca</option>
										<?php foreach ($Marche as $key_m => $field_m) { ?>
											<option value="<?php echo $field_m['marca']; ?>"
												<?php if ($_POST['marca' . $key] == $field_m['marca'])
													{ echo "selected=\"selected\""; } ?> >
												<?php echo htmlentities($field_m['marca']); ?>
											</option>
										<?php } ?>
									</select>
								</td>
								<td>
									<select name="modello<?php echo $key; ?>">
										<option value="-">Modello</option>
										<?php foreach ($Modelli as $key_m => $field_m) { ?>
											<option value="<?php echo $field_m['modello']; ?>"
												<?php if ($_POST['modello' . $key] == $field_m['modello'])
													{ echo "selected=\"selected\""; } ?> >
												<?php echo htmlentities($field_m['modello']); ?>
											</option>
										<?php } ?>
									</select>
								</td>
								<td>
									<input name="serial<?php echo $key; ?>" type="text" style="width: 11em;"
										onkeyup="ValidateField(this, '^[a-zA-Z0-9-]*$');"
										value="<?php if (isset($_POST['serial' . $key])) { echo stripslashes($_POST['serial' . $key]); } ?>" />
								</td>
								<td>
									<input name="asset<?php echo $key; ?>" type="text" style="width: 8em;"
										onkeyup="ValidateField(this, '^[a-zA-Z0-9-]*$');"
										value="<?php if (isset($_POST['asset' . $key])) { echo stripslashes($_POST['asset' . $key]); } ?>" />
								</td>

								<td>
									<input name="mac<?php echo $key; ?>" type="text" style="width: 8em;"
										onkeyup="ValidateField(this, '^[a-fA-F0-9-]*$');"
										value="<?php if (isset($_POST['mac' . $key])) { echo stripslashes($_POST['mac' . $key]); } ?>" />
								</td>


								<td style="background-color: red ! important;">
									<input name="ck_funz<?php echo $key; ?>" type="checkbox"
										title="L'appareccahiatura funziona?" value="OK"
										<?php if ($_POST['ck_funz' . $key] == "OK") { echo "checked=\"checked\""; } ?> />
								</td>

								<td style="background-color: red ! important;">
									<input name="note_acc<?php echo $key; ?>" type="checkbox"
										title="Presenza Accessori (cavi, tastiere etc)" value="SI"
										<?php if ($_POST['note_acc' . $key] == "SI") { echo "checked=\"checked\""; } ?> />
								</td>

							<?php }else{ ?>
								<td><?php echo $field['provenienza']; ?></td>
								<td><?php echo $field['tipo_mch']; ?></td>
								<td><?php echo $field['marca']; ?></td>
								<td title="Il modello viene troncato solo in visualizzazione.">
									<?php echo substr($field['modello'], 0, 16); ?>
								</td>
								<td><?php echo $field['serial']; ?></td>
								<td><?php echo $field['asset']; ?></td>
								<td><?php echo $field['mac']; ?></td>
								<td><?php echo $field['ck_funz']; ?></td>
								<td title="Le note vengono troncate solo in visualizzazione.">
										<?php echo substr($field['note_acc'], 0, 16); ?>
								</td>

							<?php } ?>

							<td style="text-align: right;">
								<?php if (is_numeric($field['id_macchina'])) { ?>
									<a href="Javascript: ShowFormDiv('frm_anag_macchina.php?tomod=<?php
										echo $field['id_macchina']; ?>',
										'200px', '200px', '770px', '320px', 'popup'); void(0);"
										 title="Modifica Macchina">
										<img src="/Images/Links/edit.png" alt="edit" />
									</a>
								 <?php }else{ ?>
									 <input name="apply" type="button" value="OK"
											title="Conferma e salva i dati dell'apparecchiatura corrente."
											onclick="Javascript: conferma_salva('1', 'Salvare ed aggiungere il dispositivo al dettaglio?',
											'<?php echo "?act=rollout&amp;key=$key";
												if (isset($_GET['tomod'])) { echo "&amp;tomod=" . $_GET['tomod']; } ?>'); void(0);" />
									 <?php } ?>

								<a style="cursor: pointer;" onclick="javascript: go_conf2('Eliminare la voce dall\'operazione?',
									'?act=rollout&amp;del=<?php echo $key;
										if (isset($_GET['tomod'])) { echo "&amp;tomod=" . $_GET['tomod']; } ?>'); void(0);" title="Rimuovi voce">
									<img src="/Images/Links/del.png" alt="Elimina" />
								</a>
							</td>
					<?php }
				}else{
					//~ echo "[Debug]: l'array dettaglio NON ESISTE! <br />";
				} ?>
				<tr><th colspan="16"><hr /></th></tr>
			</table>
		</form>

		<?php
	/*
		echo "<pre> POST: ";
			print_r($_POST);
		echo "</pre>";
	*/

/*
		echo "<pre>session rollout: ";
			print_r($_SESSION);
		echo "</pre>";
*/

/*
		echo "<pre> DETTAGLIO: ";
			print_r($_SESSION['rollout']['dettaglio']);
		echo "</pre>";
*/
	?>
	</div>
<?php } ?>

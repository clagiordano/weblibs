<?php session_start();
	$root = substr($_SERVER["DOCUMENT_ROOT"], 0, -1);
	require_once("$root/Function/Db.php");
	require_once("$root/Function/Debug.php");
	require_once("$root/Function/Strings.php");
	require_once("$root/Function/DataTime.php");
	require_once("$root/Class/XlsExport.php");

	$Completate = GetRows ("view_requests", "id_stato = '7'", "cod_fil", $db);

	foreach ($Completate as $key_c => $field_c)
	{
		$FileName = "export/RollOut_WIN7_RITIRI_FIL_" . $field_c['cod_fil'] . ".xls";

		$x = new XlsExport($FileName, "export");
		$x->xlsWriteLabel(0, 0, "Branch Code:");
		$x->xlsWriteLabel(0, 1, $_SESSION['rollout']['cod_fil']);
		$x->xlsWriteLabel(1, 0, "N:");
		$x->xlsWriteLabel(1, 1, "Tipologia Apparato:");
		$x->xlsWriteLabel(1, 2, "Marca:");
		$x->xlsWriteLabel(1, 3, "Modello:");
		$x->xlsWriteLabel(1, 4, "Serial Number:");
		$x->xlsWriteLabel(1, 5, "Asset:");

		$xlsRow = 2;
		$Dettaglio = GetRows ("view_macchine_assegnate",
			"id_request = '" . $field_c['id_request'] . "'", "tipo_mch", $db);
		foreach ($Dettaglio as $key => $field)
		{
			if (($field['provenienza'] == "DA FILIALE")
				&& ($field['tipo_mch'] != "Server"))
			{
				$x->xlsWriteLabel ($xlsRow, 0, $xlsRow-1 . ")");
				$x->xlsWriteLabel ($xlsRow, 1, $field['tipo_mch']);
				$x->xlsWriteLabel ($xlsRow, 2, $field['marca']);
				$x->xlsWriteLabel ($xlsRow, 3, $field['modello']);
				$x->xlsWriteLabel ($xlsRow, 4, $field['serial']);
				$x->xlsWriteLabel ($xlsRow, 5, $field['asset']);
				$xlsRow++;
			}
		}
		$x->WriteFile();
	}

?>

<script type="text/javascript">
	<!--window.close();!-->
</script>

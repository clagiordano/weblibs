<div id="search-result">
<?php
	require_once("$root/Function/libchart/classes/libchart.php");

	$Stati = $d->GetRows ("*", "tab_stato_call", "", "", "stato_call");
	//Padding::Padding  ($top, $right, $bottom, $left)

	define("ChartPath", "tmp/chart.png");
	define("ChartWidth", 1000);
	define("ChartHeight", 400);

	$chart = new PieChart(ChartWidth, ChartHeight);
	$chart->getPlot()->setGraphPadding(new Padding("5", "5", "5", "5"));
	$chart->setTitle("Stato rollout filiali Unicredit - Sicilia");
	$dataSet = new XYDataSet();


	foreach ($Stati as $key => $field)
	{
		$stat = $d->GetRows ("count(id_filiale) as count", "tab_filiali",
			"id_stato = '" . $field['id_stato_call'] . "'");
/*
		echo "[Debug]: count stato: ".$stat[0]['count']." <br />";
*/

		$dataSet->addPoint(new Point($field['stato_call']
			. " ( " . $stat[0]['count'] . " )", $stat[0]['count']));

	}

	$chart->setDataSet($dataSet);
	$chart->render(ChartPath);

?>


	<img src="<?php echo ChartPath; ?>" alt="chart" />
</div>


<?php session_start();
	$root = substr($_SERVER["DOCUMENT_ROOT"], 0, -1);
	require_once("$root/Class/XlsExport.php");
	require_once("$root/Class/Net.php");

	$FileName = "tmp/HW_NEW_" . $_SESSION['rollout']['cod_fil'] . ".xls";
	$x = new XlsExport($FileName, "mail");

	$x->xlsWriteLabel(0, 0, "Filiale: ");
	$x->xlsWriteLabel(0, 1, $_SESSION['rollout']['den']
		. "(" . $_SESSION['rollout']['cod_fil'] . ")");

	$x->xlsWriteLabel(1, 0, "N:");
	$x->xlsWriteLabel(1, 1, "Tipologia Apparato:");
	$x->xlsWriteLabel(1, 2, "Marca:");
	$x->xlsWriteLabel(1, 3, "Modello:");
	$x->xlsWriteLabel(1, 4, "Serial Number:");
	$x->xlsWriteLabel(1, 5, "Asset:");
	$x->xlsWriteLabel(1, 6, "Mac:");

	$xlsRow = 2;
	foreach ($_SESSION['rollout']['dettaglio'] as $key => $field)
	{
		if (($field['provenienza'] == "FORNITORE")
			&& ($field['tipo_mch'] == "Desktop"))
		{
			$x->xlsWriteLabel ($xlsRow, 0, $xlsRow-1 . ")");
			$x->xlsWriteLabel ($xlsRow, 1, $field['tipo_mch']);
			$x->xlsWriteLabel ($xlsRow, 2, $field['marca']);
			$x->xlsWriteLabel ($xlsRow, 3, $field['modello']);
			$x->xlsWriteLabel ($xlsRow, 4, $field['serial']);
			$x->xlsWriteLabel ($xlsRow, 5, $field['asset']);
			$x->xlsWriteLabel ($xlsRow, 6, $field['mac']);
			$xlsRow++;
		}
	}

	$x->WriteFile();
	//'to' => 'UGIS - IT - ROLLOUT EUROPC - UniCredit Group - UniCredit Group <USIROLEURUNIGROUP.ugis@unicreditgroup.eu>',
	//'cc' => 'Leonardi Giorgio (UGIS) <Giorgio.Leonardi@unicreditgroup.eu>, Vesentini Doriana (UGIS) <Doriana.Vesentini@unicreditgroup.eu>',
	//'ccn' => 'Claudio Giordano <claudio.giordano@autistici.org>, Sun Informatica <commerciale@suninformatica.it>',
	$MailData = array('from' => "[dbRollOut] <info@erremmeweb.com>",
											 'to' => 'Claudio Giordano <claudio.giordano@autistici.org>', // test
											 //'to' => 'ugis.delivery@ts.fujitsu.com, ugis.supporto@ts.fujitsu.com, ugis.rollout@ts.fujitsu.com, ugis.presidio@ts.fujitsu.com',
											 'cc' => 'Claudio Giordano <claudio.giordano@autistici.org>, Francesco Azzaro <francescoazzaro@suninformatica.it>',
									'subject' => 'Hardware New Fil. ' . $_SESSION['rollout']['den']
										. ' (' . $_SESSION['rollout']['cod_fil'] . ')',
										 'body' => "In allegato l'elenco delle apparecchiature nuove per la filiale in oggetto.",
											'att' => $FileName);

	//DirectSendMail2($MailData, "smtp.tiscali.it");
	$Status = $d->UpdateRow (array('id_stato' => 8), "", "tab_filiali",
		"cod_fil = '" . $_SESSION['rollout']['cod_fil'] . "' AND id_stato = '3'");
?>

<script type="text/javascript">
	alert("Elenco macchine nuove inviato correttamente.");
	window.close();
</script>

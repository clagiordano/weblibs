<div id="search-result">
<?php error_reporting(0);
	if (!ini_get('safe_mode'))
	{
		set_time_limit(2000);
		ini_set("post_max_size", "15M");
		ini_set("upload_max_filesize", "15M");
		//~ echo "[Debug]: cambio i settaggi di php <br />";
	}
	$root = substr($_SERVER['DOCUMENT_ROOT'], 0, -1);
	require_once("$root/Class/Net.php");
	require_once("$root/Function/xlsread/reader.php"); //old reader
	//require_once("$root/Function/excel_reader2.php"); // new reader

	if (!isset($_POST['profile']))
	{
		$_POST['profile'] = "";
	}

	define("XLS_IMPORT_PATH", "tmp/import.xls");
	define("UploadDir", "/var/spool/web/roll2010/upload/");
	define("RegDataItOld", "[0-3][0-9]/[0-1][0-9]/[1-2][0-9][0-9][0-9]");
	define("Request", "(CR\d+)");
	define("FilCode", "(RT\d+)");
	define("ColsRequest", 71);
	define("ColsGiacenza", 29);

	$MailData['from'] = "[dbRollOut] <info@erremmeweb.com>";
	$MailData['to'] = "Realizzazioni Multimediali <erremmeweb@erremmeweb.com>";
	$file_stat = "";
	umask(0);

	if(isset($_FILES['file_up']['tmp_name'])
		&& ($_FILES['file_up']['tmp_name'] != ""))
	{
		// copio il file nella cartella upload:
		if (copy($_FILES['file_up']['tmp_name'], UploadDir . $_FILES['file_up']['name']))
		{
			$d->SessionLog("Eseguito upload del file: " . $_FILES['file_up']['name']);
		}else{
			$d->SessionLog("Errore durante la copia del file: " . $_FILES['file_up']['name']);
			$file_stat .= "Errore durante la copia del file. <br />";
			echo "[Debug]: $file_stat  <br />";
		}

		if (move_uploaded_file($_FILES['file_up']['tmp_name'], XLS_IMPORT_PATH))
		{
			$d->SessionLog("L'upload del file " . $_FILES['file_up']['name']
				. " &egrave; stato eseguito correttamente.");

			$file_stat .= "<label class=\"ok\">L'upload del file "
				. $_FILES['file_up']['name'] . " &egrave; stato
				eseguito correttamente.</label><br />";
			$_SESSION['file_up'] = $_FILES['file_up']['name'];
		}else{
			$d->SessionLog("Si &egrave; verificato un errore durante l'upload del file: "
				. $_FILES['file_up']['name']);
			$file_stat = "<label class=\"err\">Si &egrave; verificato un errore
				durante l'upload del file: " . $_FILES['file_up']['name'] . "</label><br />";
		}
	}

	if (isset($_POST['bt_submit']))
	{
		$d->SessionLog("Avviata importazione del file xls per il formato "
			. $_POST['profile']);
		// Legge il file xls per l'importazione:
		$data = new Spreadsheet_Excel_Reader();
		echo $data->setOutputEncoding('CP1251');
		echo $data->read(XLS_IMPORT_PATH);

		//$data = new Spreadsheet_Excel_Reader(XLS_IMPORT_PATH, false);
	}

	if (isset($_POST['sheet']) && is_numeric(trim($_POST['sheet'])))
	{
		$_POST['sheet'] = trim($_POST['sheet']);
	}else{
		$_POST['sheet'] = 0;
	}

	if (isset($_POST['top_offset']) && is_numeric(trim($_POST['top_offset'])))
	{
		$_POST['top_offset'] = trim($_POST['top_offset']);
	}else{
		$_POST['top_offset'] = 1;
	}

	if (isset($data))
	{
		if (isset($_POST['row_limit']) && is_numeric(trim($_POST['row_limit'])))
		{
			$_POST['row_limit'] = trim($_POST['row_limit']);
		}else{
			$_POST['row_limit'] = $data->sheets[$_POST['sheet']]['numRows'];
		}
	}

	/*
	 * Check Profile:
	 */
	//$Anteprima = "<table style=\"width: 100%; border: 1px solid black;\" rules=\"all\">";
	$start = time();
	switch($_POST['profile'])
	{
		case "request":
			/*
			 * Forzo la selezione del foglio, del numero di righe
			 * e l'offset iniziale
			 */
			$_POST['sheet'] 			= 1; // secondo foglio
			$_POST['row_limit'] 	= $data->sheets[$_POST['sheet']]['numRows'];
			$_POST['top_offset'] 	= 3;
			$msg_status 					= "";
			$Log 									= array();

			/*echo "[Debug]: 1:71: "
				. trim($data->sheets[$_POST['sheet']]['cells'][1][71])
				. " <br />"; */

			if (trim($data->sheets[$_POST['sheet']]['cells'][1][71]) == "Note")
			{
				/*
				 * rimuovo dalla selezione massiva la colonna
				 * della data di pianificazione
				 * dato che andr� in una tabella separata,
				 *  id colonna: 18 xls
				 * , 'data_pianif' -> tab_filiali con update in base al cod_fil
				 */
				$Cols = array(1, 16, 17, 19, 21, 24, 25, 26, 27, 28, 29,
					30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42,
					43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55,
					56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68,
					69, 70, 71, 72, 74);
				//echo "[Debug]: count col: " . count($Cols) . " <br />";

				$Fields = array('id_filiale', 'request', 'data_request', 'data_att',
					'report_cl',  'i_lett', 'qi_lett', 'i_prt', 'qi_prt', 'i_server',
					'qi_server', 'sp_srv_new', 'sp_pdl', 'sp_las', 'sp_mon',
					'sp_bar', 'sp_lett', 'sp_prt_lan', 'sp_altro_hw', 'st_pdl',
					'st_srv', 'sos_pdl', 'sos_srv', 'sos_las', 'sos_lett',
					'sos_bar', 'sos_mon', 'sos_prt_lan', 'ric_prt_lan', 'ric_multi',
					'dis_pdl', 'dis_mon', 'dis_srv', 'dis_let', 'dis_cas',
					'dis_fax', 'dis_bar', 'dis_las', 'dis_altro_hw', 'rit_pdl',
					'rit_mon', 'rit_srv', 'rit_let', 'rit_cas', 'rit_las',
					'rit_prt_lan', 'rit_bar', 'rit_fax', 'rit_altro_hw',
					'nomi_prt_lan', 'nomi_multi', 'nome_srv_new', 'note_req',
					'req_integ', 'note_integ');
				//echo "[Debug]: count field: " . count($Fields) . " <br />";

				for ($i = $_POST['top_offset'];
					$i <= ($_POST['row_limit'] + $_POST['top_offset']); $i++)
				{
					//echo "[Debug]: i: $i <br />";
					//$Anteprima .= "<tr><td>$i)</td>";
					$Values = array();
					foreach ($Cols as $key_col => $col)
					{
						// per ogni cella del rigo
						$Cella = trim($data->sheets[$_POST['sheet']]['cells'][$i][$col]);

						if ($col == 1)
						{
							$cod_fil = $Cella;
						}
						array_push($Values, $Cella);
					}
					$Valori = array_combine($Fields, $Values);

					$DatiFiliale = array('cod_fil' => trim($data->sheets[$_POST['sheet']]['cells'][$i][1]),
																	'den' => trim($data->sheets[$_POST['sheet']]['cells'][$i][2]),
																	'ind' => trim($data->sheets[$_POST['sheet']]['cells'][$i][3]),
																	'cap' => trim($data->sheets[$_POST['sheet']]['cells'][$i][4]),
																'citta' => trim($data->sheets[$_POST['sheet']]['cells'][$i][5]),
																 'prov' => trim($data->sheets[$_POST['sheet']]['cells'][$i][6]),
																	'reg' => 'SICILIA',
																	'rol' => trim($data->sheets[$_POST['sheet']]['cells'][$i][8]),
																	'tel' => trim($data->sheets[$_POST['sheet']]['cells'][$i][9]),
																 'cell' => trim($data->sheets[$_POST['sheet']]['cells'][$i][10]),
																 'mail' => trim($data->sheets[$_POST['sheet']]['cells'][$i][11]),
															 'utenti' => trim($data->sheets[$_POST['sheet']]['cells'][$i][12]),
																 'forn' => 'FST',
															 'variaz' => trim($data->sheets[$_POST['sheet']]['cells'][$i][14]),
																'dispo' => trim($data->sheets[$_POST['sheet']]['cells'][$i][15]));


					// Recupero l'id della filiale in base al codice filiale:
					$temp_val = $d->GetRows ("id_filiale, den", "tab_filiali",
						"cod_fil = '$cod_fil'", "", "", 1);
					$Valori['id_filiale'] = $temp_val[0]['id_filiale'];
					$den_filiale = $temp_val[0]['den'];

					if (!is_numeric($Valori['id_filiale']))
					{
						$den_filiale = trim($data->sheets[$_POST['sheet']]['cells'][$i][2]);
						array_push($Log, "la request &egrave; associata ad
							una filiale non presente sul datatabase, aggiungo
							la nuova filiale ($den_filiale) sul database! <br />");

						// Aggiungo la filiale mancante sul db:
						if (trim($data->sheets[$_POST['sheet']]['cells'][$i][1]) != "")
						{
							$Status = $d->SaveRow ($DatiFiliale, "", "tab_filiali", 1);
							$Valori['id_filiale'] = $Status[1]; // last_insert id
						}

						//$Valori['id_filiale'] = 518; // id filiale fittizia.
					}else{
						// Aggiorno i dati per la filiale:
						if (trim($data->sheets[$_POST['sheet']]['cells'][$i][1]) != "")
						{
							$Status = $d->UpdateRow ($DatiFiliale, "", "tab_filiali",
								"id_filiale = '" . $Valori['id_filiale'] . "'");
						}
					}

					$Valori['data_request'] = $d->Inverti_Data($Valori['data_request']);
					$Valori['data_att'] 	 	= $d->Inverti_Data($Valori['data_att']);


					if ($Valori['request'] != "")
					{
						//echo "[Debug]: request: '$ck_request' <br />";
						/* verifica per eventuale update */
						if (count($d->GetRows ("id_request", "tab_request",
							"request = '" . $Valori['request'] . "'")) == 0)
						{
							array_push($Log, "Inserimento della nuova request: '"
								. $Valori['request'] . "' per la filiale '$den_filiale'");
							$d->SessionLog("Inserimento della nuova request: '"
								. $Valori['request'] . "' per la filiale '$den_filiale'");
							$Status = $d->SaveRow ($Valori, "", "tab_request");
						}else{
							array_push($Log, "[AGGIORNAMENTO] Request: '"
								. $Valori['request'] . "' della filiale '$den_filiale'");
							$d->SessionLog("[AGGIORNAMENTO] Request: '"
								. $Valori['request'] . "' della filiale '$den_filiale'");
							$Status = $d->UpdateRow ($Valori, "", "tab_request",
								"request = '" . $Valori['request'] . "'");
						}

						/*
						 * per qualche strano motivo in fase di importazione
						 * tutte le date vengono incrementate di 1 giorno
						 * per cui sottraggo un giorno alla data importata.
						 */
						//echo "[Debug]: data_pianif '" . $data->sheets[$_POST['sheet']]['cells'][$i][18] . "', Filiale '$den_filiale' <br />";
						$data_temp_cell = $data->sheets[$_POST['sheet']]['cells'][$i][18];
						//echo "[data_temp_cell]: '$data_temp_cell' <br />";
						if (isset($data_temp_cell)
							&& (trim($data_temp_cell) != "")
							&& (trim($data_temp_cell) != "00/00/00"))
						{
							//echo "[Debug]: data". $data->sheets[$_POST['sheet']]['cells'][$i][18] . " <br />";
							$data_cell = $d->Inverti_Data(trim($data_temp_cell));
							//echo "[Debug]: data_cell: '$data_cell' <br />";
							$data_new = strtotime('-1 day', strtotime($data_cell));
							$data_pianif = date ( 'Y-m-d', $data_new);
							//echo "[data_pianif]: '$data_pianif' <br />";
						}else{
							//echo "[Debug]: la data e' vuota la setto a 0000-00-00' <br />";
/*
							echo "[Debug]: Errore, la data di pianificazione per la
								filiale '$den_filiale' e' vuota. <br />";
*/
							$data_pianif = "0000-00-00";
						}

						echo "[PIANIFICAZIONE]: $data_pianif, [FILIALE]: '$den_filiale'. <br />";

						if ($data_pianif > date('Y-m-d'))
						{
							//echo "[Debug]: data_pianif: $data_pianif <br />";
							/*
							 *  se la data di pianificazione � superiore
							 *  alla data odierna esegui l'update dello
							 * stato della filiale con "pianificata"  (id 3)
							 */
								//echo "[Debug]: la data di pianificazione �
								//superiore alla data odierna <br />";
							$Status = $d->UpdateRow (array('data_pianif' => $data_pianif,
																			'id_stato' => '3'),
								"", "tab_filiali", "id_filiale = '"
								. $Valori['id_filiale'] . "' AND id_stato != '8'", 1);
						}elseif ($data_pianif == "0000-00-00")
						{
							/*
							 * se la data � vuota setto la filiale come "sospesa"
							 */
							$Status = $d->UpdateRow (array('data_pianif' => $data_pianif,
																							'id_stato' => '4'),
								"", "tab_filiali", "id_filiale = '" . $Valori['id_filiale'] . "'", 1);
						}
						else{
							/*
							 * altrimenti esegui solo update della data
							 */
							$Status = $d->UpdateRow (array('data_pianif' => $data_pianif),
								"", "tab_filiali", "id_filiale = '" . $Valori['id_filiale'] . "'", 1);
						}

					}else{
						//echo "[Debug]: manca il codice di request, riga ignorata <br />";
						//echo "[Debug]: Errore, il codice di request non � standard: '$ck_request'. <br />";
						//exit();
					}

					//$Anteprima .= "</tr>";
				}

				//$Anteprima .= "</table>";

				$Status = "<label class=\"ok\">Importazione File Completata! ("
					. date('d-m-Y H:i:s') . ")</label>";

				// elimino il file temporaneo:
				unlink(XLS_IMPORT_PATH);

				// Invio la mail di notifica:
				asort($Log);
				$log_mail 						= join("\r\n", $Log);
				$log_status 					= join("<br />", $Log);
				$MailData['subject'] 	= "[dbRollOut] Importazione Request";
				$MailData['body'] 		= "E' stato eseguito l'upload del file: '"
					. $_SESSION['file_up'] . "'.\r\n"
					. "Il profilo di importazione e': '" . $_POST['profile'] . "'.\r\n"
					. "L'utente che ha eseguito l'upload e': '"
					. $_SESSION['desc_user'] . " (" . $_SESSION['tipo'] . ")'\r\n\r\n"
					. "E' possibile accedere ad una copia del file tramite il percorso: \r\n"
					. "http://" . $_SERVER['HTTP_HOST'] . "/roll2010/upload/ \r\n\r\n"
					. "***** Log Operazione *****\r\n$log_mail\r\n\r\n";

				DirectSendMail2($MailData, "smtp.tiscali.it");

			}else{
				$d->SessionLog("Errore, il file contiene un numero di
					colonne diverso da quello atteso.");

				$Status = "<label class=\"err\">Errore, il file contiene
					un numero di colonne diverso da quello atteso.</label>";
			}
		break;

		case "giacenza":
			/*echo "[Debug]: 1:29: "
				. trim($data->sheets[$_POST['sheet']]['cells'][1][29])
				. " <br />";*/
			if (trim($data->sheets[$_POST['sheet']]['cells'][1][29]) == "LOCAZIONE")
			{
				/*
				 * Forzo la selezione del foglio, del numero di righe
				 * e l'offset iniziale
				 */
				$_POST['sheet'] = 0; // primo foglio
				$_POST['row_limit'] = $data->sheets[$_POST['sheet']]['numRows'];
				$_POST['top_offset'] = 2;

				$Cols = array(2, 3, 6, 8, 9, 12, 13, 14, 15, 16,
					17, 18, 19, 20, 21, 22, 23, 25, 26);

				$Fields = array('provenienza', 'ddt_in', 'data_in', 'data_st',
					'data_out', 'allestimento', 'note_acc', 'id_tipo_mch',
					'cod_art', 'marca', 'modello', 'class_hw', 'ck_vis',
					'ck_funz', 'serial', 'asset', 'pallet', 'mac',
					'id_magazzino');

				for ($i = $_POST['top_offset'];
					$i <= ($_POST['row_limit'] + $_POST['top_offset']); $i++)
				{
					//$Anteprima .= "<tr><td>$i)</td>";
					$Values = array();
					$id_request = "";
					foreach ($Cols as $key_col => $col)
					{
						$Cella = trim($data->sheets[$_POST['sheet']]['cells'][$i][$col]);

						// Verifica se la colonna � una data ed eventualmente convertila:
						if (preg_match(RegDataIt, $Cella))
						{
							$Cella = $d->Inverti_Data($Cella);
						}

						// Converte i nomi dei magazzini e i tipi hw in id:
						switch ($Cella)
						{
							case "palermo":
							case "PALERMO":
							case "server":
							case "SERVER":
								$Cella = 1;
							break;

							case "catania":
							case "CATANIA":
							case "desktop":
							case "DESKTOP":
								$Cella = 2;
							break;

							case "STP":
							case "stp":
							case "PRINTER":
							case "printer":
								$Cella = 3;
							break;
						}

						/*
						 * aggiunge la stringa eventualmente convertita all'output:
						 */
						//$Anteprima .= "<td>$Cella</td>";
						array_push($Values, $Cella);
					}

					$Valori = array_combine($Fields, $Values);

					/* verifica se il seriale letto � valido,
					 * se � gia' presente esegue l'update della macchina
					 * e la associa alla request corrispondente se presente
					 */

					$req_code = trim($data->sheets[$_POST['sheet']]['cells'][$i][7]);
					//echo "[Debug]: req_code: '$req_code' <br />";
					$serial = trim($data->sheets[$_POST['sheet']]['cells'][$i][21]);
					if ($serial != "")
					{
						//echo "[Debug]: serial: '$serial' <br />";
						// recupero l'id della request col 7 file xls:
						if (preg_match(Request, $req_code))
						{
							$req = $d->GetRows ("id_request", "tab_request",
								"request = '$req_code'", "", "", 1);
							$id_request = $req[0]['id_request'];
							//echo "[Debug]: id_request: '$id_request' <br />";
						}

						if (count($d->GetRows ("*", "tab_macchine", "serial = '"
							. $serial . "'")) == 0)
						{
							// insert
							$Status = $d->SaveRow ($Valori, "", "tab_macchine", 1);
							$id_macchina = $Status[1];
						}else{
							// update
							$Status = $d->UpdateRow ($Valori, "", "tab_macchine",
								"serial = '$serial'", 1);
							$Campi = $d->GetRows ("*", "tab_macchine", "serial = '$serial'");
							$id_macchina = $Campi[0]['id_macchina'];
						}

						if (is_numeric($id_request))
						{
							// aggiunta macchina al dettaglio request
							//echo "[Debug]: $id_request <br />";

							/* tenere in considerazione l'hw vecchio
							 * eventualmente inserito manualmente,
							 * questo impedisce lo svuotamento del dettaglio in base
							 * all'id_request sulla tabella dettaglio
							 *
							 * aggiungere la macchina al dettaglio request
							 * senza svuotarlo ma verificando la presenza
							 * della macchina nello stesso.
							 */

							// Verifico se la macchina � gi� presente nel dettaglio
							$macchina = array('id_request' => $id_request,
												'id_macchina' => $id_macchina);
							if (count($d->GetRows ("*", "tab_dett_requests", "id_macchina = '$id_macchina'")) == 0)
							{
								// non � presente, quindi la aggiungo e la associo alla request
								$Status = $d->SaveRow ($macchina, "", "tab_dett_requests");
							}else{
								// update di qualche genere?
							}
						}
					}
					//$Anteprima .= "</tr>";
				}

				//$Anteprima .= "</table>";
				$d->SessionLog("Importazione File Completata!");

				$Status = "<label class=\"ok\">Importazione File Completata! ("
					. date('d-m-Y H:i:s') . ")</label>";

				//echo "[Debug]: Importazione Hardware New Completata. ("
					//. date('d-m-Y H:i:s') . ")<br />";

				/*
				 *
				 * Avvio importazione server old:
				 *
				 */


				//echo "[Debug]: Avvio importazione server old: <br />";

				/*
				 * Forzo la selezione del foglio, del numero di righe
				 * e l'offset iniziale
				 */
				$_POST['sheet'] = 1; // secondo foglio
				$_POST['row_limit'] = $data->sheets[$_POST['sheet']]['numRows'];
				//echo "[Debug]: num rows: ". $data->sheets[$_POST['sheet']]['numRows']." <br />";
				$_POST['top_offset'] = 2;

				$Cols = array(2, 3, 6, 8, 9, 12, 13, 14, 15, 16, 17, 18,
					19, 20, 21, 22, 23, 24, 25, 26, 27, 28);
				//echo "[Debug]: count " .count($Cols). " <br />";

				$Fields = array('provenienza', 'ddt_in', 'data_in', 'data_st',
					'data_out', 'allestimento', 'note_acc', 'hostname', 'ip',
					'srv_code', 'id_tipo_mch', 'cod_art', 'marca', 'modello',
					'class_hw', 'ck_vis', 'ck_funz', 'serial', 'asset',
					'pallet', 'id_magazzino', 'mac');
				//echo "[Debug]: count field " . count($Fields). " <br />";

				for ($i = $_POST['top_offset'];
					$i <= ($_POST['row_limit'] + $_POST['top_offset']); $i++)
				{
					//$Anteprima .= "<tr><td>$i)</td>";
					//echo "[Debug]: I: $i <br />";
					$Values = array();
					$id_request = "";
					foreach ($Cols as $key_col => $col)
					{
						$Cella = trim($data->sheets[$_POST['sheet']]['cells'][$i][$col]);

						if (preg_match(RegDataIt, $Cella))
						{
							$Cella = $d->Inverti_Data($Cella);
						}

						// Converte i nomi dei magazzini e i tipi hw in id:
						switch ($Cella)
						{
							case "palermo":
							case "PALERMO":
							case "server":
							case "SERVER":
								$Cella = 1;
								break;

							case "catania":
							case "CATANIA":
							case "desktop":
							case "DESKTOP":
								$Cella = 2;
								break;

							case "STP":
							case "stp":
							case "PRINTER":
							case "printer":
								$Cella = 3;
								break;
						}

						/*
						 * aggiunge la stringa eventualmente convertita all'output:
						 */
						//$Anteprima .= "<td>$Cella</td>";
						array_push($Values, $Cella);
					}

					/*echo "<pre> Values ";
						print_r($Values);
					echo "</pre>"; */
					$Valori = array_combine($Fields, $Values);

					/* verifica se il seriale letto � valido,
					 * se � gia' presente esegue l'update della macchina
					 * e la associa alla request corrispondente se presente
					 */

					$req_code = trim($data->sheets[$_POST['sheet']]['cells'][$i][4]);
					//echo "[Debug]: req_code: $req_code <br /><br />";
					$serial = trim($data->sheets[$_POST['sheet']]['cells'][$i][24]);
					if ($serial != "")
					{
						//echo "[Debug]: serial: '$serial' <br />";
						// recupero l'id della request col 4 file xls:
						if (preg_match(Request, $req_code))
						{
							$req = $d->GetRows ("id_request", "tab_request", "request = '$req_code'");
							$id_request = $req[0]['id_request'];
						}

						if (count($d->GetRows ("*", "tab_macchine", "serial = '$serial'")) == 0)
						{
							// insert
							$Status = $d->SaveRow ($Valori, "", "tab_macchine");
							$id_macchina = $Status[1];
						}else{
							// update
							$Status = $d->UpdateRow ($Valori, "", "tab_macchine", "serial = '$serial'");
							$Campi = $d->GetRows ("*", "tab_macchine", "serial = '$serial'");
							$id_macchina = $Campi[0]['id_macchina'];
						}

						if (is_numeric($id_request))
						{
							// aggiunta macchina al dettaglio request
							//echo "[Debug]: $id_request <br />";

							/* tenere in considerazione l'hw vecchio
							 * eventualmente inserito manualmente,
							 * questo impedisce lo svuotamento del dettaglio in base
							 * all'id_request sulla tabella dettaglio
							 *
							 * aggiungere la macchina al dettaglio request
							 * senza svuotarlo ma verificando la presenza
							 * della macchina nello stesso.
							 */

							// Verifico se la macchina � gi� presente nel dettaglio
							$macchina = array('id_request' => $id_request,
												'id_macchina' => $id_macchina);
							if (count($d->GetRows ("*", "tab_dett_requests", "id_macchina = '$id_macchina'")) == 0)
							{
								// non � presente, quindi la aggiungo e la associo alla request
								$Status = $d->SaveRow ($macchina, "", "tab_dett_requests");
							}else{
								// update di qualche genere?
							}

						}
					}
					//$Anteprima .= "</tr>";
				}

				//$Anteprima .= "</table>";
				$d->SessionLog("Importazione File Completata!");

				$Status = "<label class=\"ok\">Importazione File Completata! ("
					. date('d-m-Y H:i:s') . ")</label>";
				//echo "[Debug]: Importazione Server Old Completata. ("
					//. date('d-m-Y H:i:s') . ")<br />";

				// elimino il file temporaneo:
				unlink(XLS_IMPORT_PATH);

			}else{
				$d->SessionLog("Errore, il file contiene un numero di
					colonne diverso da quello atteso.");

				$Status = "<label class=\"err\">Errore, il file contiene
					un numero di colonne diverso da quello atteso.</label>";
			}
		break;

		case "reimport_giac":
			$Esclusioni = array();
			if (trim($data->sheets[$_POST['sheet']]['cells'][1][25]) == "LOCAZIONE")
			{
				//~ Resetto il time limit dello sript:
				set_time_limit(2000);

				echo "[Debug]: Avvio l'importazione <br />";
				/*
				 * Forzo la selezione del foglio, del numero di righe
				 * e l'offset iniziale
				 */
				$_POST['sheet'] 			= 0; // primo foglio
				$_POST['row_limit'] 	= $data->sheets[$_POST['sheet']]['numRows'];
				$_POST['top_offset'] 	= 2;

				// colonna 11 e 24 opzionali,
				// rispettivamente id_tipo_mch (necessaria query)
				// e filiale di destinazione entrambi i campi dinamici
				$Cols = array(1, 2, 3, 6, 7, 8, 9, 10, 12, 13, 14, 15, 16,
					17, 18, 19, 20, 21, 22, 23, 30); //, 11, 24
				//echo "[Debug]: count: ".count($Cols)."<br />";

				$Fields = array('id_macchina', 'provenienza', 'ddt_in', 'data_in',
					'ddt_out', 'data_out', 'allestimento', 'note_acc',
					'cod_art', 'marca', 'modello', 'class_hw', 'ck_vis',
					'ck_funz', 'serial', 'asset', 'pallet',
					'mag_rif', 'mac', 'impiego', 'id_magazzino');

					// , 'id_tipo_mch', 'dest_fil'
					//echo "[Debug]: count: ".count($Fields)."<br />";

				/*
				 *	VERIFICA ASSOCIAZIONE ARRAY
						$i = 0;
						foreach ($Cols as $key => $field)
						{
							echo "[Debug]: $field -> " . $Fields[$i] . " <br />";
							$i++;
						}
					*/

				for ($i = $_POST['top_offset'];
					$i <= ($_POST['row_limit'] + $_POST['top_offset']); $i++)
				{
					echo "[Debug]: Analisi Riga $i <br />";
					$Values = array();
					$Esclusioni = array();
					foreach ($Cols as $key_col => $col)
					{
						// per ogni cella del rigo
						$Cella = trim($data->sheets[$_POST['sheet']]['cells'][$i][$col]);
						array_push($Values, $Cella);
					}
					$Valori = array_combine($Fields, $Values);

					$Valori['data_in'] 		 = $d->Inverti_Data($Valori['data_in']);
					$Valori['data_out'] 	 = $d->Inverti_Data($Valori['data_out']);
					$tmp_stato = $d->GetRows ("*", "tab_stato_mch", "stato_mch = '"
						. trim($data->sheets[$_POST['sheet']]['cells'][$i][24]) . "'");

					if (is_numeric($tmp_stato[0]['id_stato_mch'])
						&& ($tmp_stato[0]['id_stato_mch'] > 0))
					{
						$Valori['id_stato_mch'] = $tmp_stato[0]['id_stato_mch'];
					}else{
						$Valori['id_stato_mch'] = 6; // in magazzino
					}

					//echo "[Debug]: id_stato_mch: " . $Valori['id_stato_mch'] . " <br />";

					//~ if (preg_match("=CERCA.VERT(", $Valori['ddt_in']))
					//~ {
						//~ array_push($Esclusioni, array('ddt_in' => ''));
					//~ }

					//~ if (preg_match("=CERCA.VERT(", $Valori['ddt_out']))
					//~ {
						//~ array_push($Esclusioni, array('ddt_out' => ''));
					//~ }

					/*
					 * $temp_res = $d->GetRows ("*", "tab_tipi_mch", "tipo_mch = '" . $Valori['id_tipo_mch'] . "'");
					 * $Valori['id_tipo_mch'] = $temp_res[0]['id_tipo_mch'];
					 */

					$temp_res = $d->GetRows ("*", "tab_magazzino",
						"descr = '" . $Valori['id_magazzino'] . "'", "", "", 1);
					$Valori['id_magazzino'] = $temp_res[0]['id_magazzino'];
					//echo "[Debug]: id_magazzino: ".$Valori['id_magazzino']."  <br />";

					/*
					 * ESCLUSIONI
					 */
					$Esclusioni = array('id_macchina' => '');

					if (!is_numeric($Valori['id_magazzino']))
					{
						echo "[Debug]: Errore, fallita importazione id magazzino,
							campo ignorato <br />";
						array_push($Esclusioni, array('id_magazzino' => ''));
					}

					//~ array_push($Esclusioni, array('id_magazzino' => '',
																				 //~ 'id_macchina' => ''));
					//~ if (count($d->GetRows ("id_macchina", "tab_macchine",
						//~ "serial = '" . $Valori['serial']
						//~ . "' AND asset = '" . $Valori['asset'] . "'", "", "", 1)) != 0)
					//~ {

						// Update Dati
						//~ if ((trim($Valori['serial']) != "")
							//~ && (trim($Valori['asset']) != "")
							//~ && ($Valori['serial'] != "ND")
							//~ && ($Valori['asset'] != "ND"))
						//~ {
							//~ $Status = $d->UpdateRow ($Valori, $Esclusioni, "tab_macchine",
								//~ "serial = '" . $Valori['serial']
								//~ . "' AND asset = '" . $Valori['asset'] . "'", 1);

							// AGGIORNAMENTO DATI BASATO SULL'ID E NON PI� SUL SERIALE/ASSET
							$Status = $d->UpdateRow ($Valori, $Esclusioni, "tab_macchine",
								"id_macchina = '" . $Valori['id_macchina'] . "'", 1);

							//~ if (mysql_affected_rows() > 1)
							//~ {
								//~ $msg = "Attenzione: L'update eseguito tramite la riga $i"
									//~ . " ha aggiornato " . mysql_affected_rows() . " record.";
								//~ $d->SessionLog($msg);
								//~ echo "$msg <br />";
							//~ }
						//~ }
					//~ }else{
						//~ // Nuovo inserimento
						//~ echo "[Debug]: Nuovo inserimento <br />";
						//~ $msg = "Attenzione: La riga $i non risulta sul database, "
							//~ . " aggiungo il nuovo record record.";
						//~ $d->SessionLog($msg);
						//~ $Status = $d->SaveRow ($Valori, "", "tab_macchine");
					//~ }
				}

				$Status = "<label class=\"ok\">Aggiornamento giacenza completato! ("
					. date('d-m-Y H:i:s') . ")</label>";

				$d->SessionLog("Aggiornamento giacenza completato!");

				$Status = "<label class=\"ok\">Importazione File Completata! ("
					. date('d-m-Y H:i:s') . ")</label>";

				// elimino il file temporaneo:
				unlink(XLS_IMPORT_PATH);

			}else{
				$d->SessionLog("Errore, il file contiene un numero di
					colonne diverso da quello atteso.");

				$Status = "<label class=\"err\">Errore, il file contiene
					un numero di colonne diverso da quello atteso.</label>";
			}
		break;
	}
/*
	$data_start = strtotime("01 January 2010");
	echo "[Debug]: data_start: $data_start <br />";
	echo "[Debug]: TIME: " . time() . " <br />";
	echo "[Debug]: date: " . date('Y-m-d') . " <br />";
	echo "[Debug]: date: " . date('Y-m-d', 40381+$data_start) . " <br />"; //22/07/2010
	echo "[Debug]: date: " . date('Y-m-d', 40732+$data_start) . " <br />"; //08/07/2011
	echo "<pre>";
		print_r(getdate(40732));
	echo "</pre>";
*/


	//$Anteprima .= "</table>";
?>

	<form enctype="multipart/form-data" method="post">
		<table style="width: 100%; border: 1px solid black;">
			<tr>
				<th style="text-align: left;">
					Upload File: &nbsp;
					<input name="file_up" type="file" size="30" />
					<input type="submit" value="Upload" />
				</th>
			</tr>

			<tr><td colspan="2"><?php echo $file_stat; ?></td></tr>
		</table>
	</form>

	<form method="post">
		<table  style="width: 100%; border: 1px solid black;">
			<tr>
				<th style="text-align: left;">
					Selezione formato di importazione: &nbsp;
				</th>
			</tr>
			<tr>
				<td>
					<label>Formato:</label>
					<input name="profile" type="radio" value="request" checked="checked" />Sinottico
					<!--<input name="profile" type="radio" value="giacenza" />Giacenza !-->
					<input name="profile" type="radio" value="reimport_giac" />Aggiornamento Giacenza

					<!--
					&nbsp;
					<label>Foglio:</label>
					<input name="sheet" type="text"
						value="<?php //if ($_POST['sheet']) { echo $_POST['sheet']; } ?>" >

					&nbsp;
					<label>Offset Iniziale:</label>
					<input name="top_offset" type="text"
						value="<?php //if ($_POST['top_offset']) { echo $_POST['top_offset']; } ?>" />

					&nbsp;
					<label>Limite Righe:</label>
					<input name="row_limit" type="text"
						value="<?php //if ($_POST['row_limit']) { echo $_POST['row_limit']; } ?>" />
					!-->
				</td>
			</tr>
			<tr>
				<td><input name="bt_submit" type="submit" value="Avvia Importazione" /></td>
			</tr>
			<tr><td colspan="2"><?php if (isset($Status)) { echo $Status; } ?></td></tr>
			</table>
		</table>
	</form>

	<?php
		if (isset($log_status)) { echo $log_status; }
		//echo $Anteprima;
		//echo "[Debug]: time: " . (time() - $start) . " sec <br />"; ?>
</div>

<?php
	// Elimino il file dopo l'importazione ?:
?>

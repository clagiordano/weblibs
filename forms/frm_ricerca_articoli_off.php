<?php
	$root 						= substr($_SERVER['DOCUMENT_ROOT'], 0, -1);
	$tot_res					= -1;
	$pezzo 						= $_GET['pezzo'];

	if (!isset($_GET['page']))
	{
		$_GET['page'] = "first";
	}

	//~ if (!isset($_POST['modello']))
	//~ {
		//~ $_POST['modello'] = $_GET['pezzo'];
		//~ $Results = $d->GetRows ("*", "tab_listino_forn", "modello like '%"
			//~ . $_POST['modello']. "%'", "", "fornitore, marca, modello", 0);
	//~ }

	$Fornitori 	= $d->GetRows ("distinct(fornitore) as fornitore",
		"tab_listino_forn", "", "", "cat_forn");
	$Categorie 	= $d->GetRows ("distinct(cat_forn) as cat_forn",
		"tab_listino_forn", "", "", "cat_forn");

	//~ if(isset($_GET['mod']))
	//~ {
		//~ $_SESSION['offerta']['mod'] = $_GET['mod'];
	//~ }else{
		//~ unset($_SESSION['offerta']['mod']);
	//~ }

	if (isset($_SESSION['offerta']['tomod']))
	{
		$OpenerUrl = "home.php?act=assembla&tomod="
			. $_SESSION['offerta']['tomod'];
	}else{
		$OpenerUrl = "home.php?act=assembla";
	}

	if (!isset($_SESSION['offerta']))
	{
		$_SESSION['offerta'] = array();
	}

	if (!isset($_SESSION['offerta']['macchina']))
	{
		$_SESSION['offerta']['macchina'] = array();
	}


	if (isset($_GET['prod']))
	{
		//~ echo "[Debug]: è stato selezionato un prodotto <br />";
		$prod 	= $_GET['prod'];

		// Se la quantità scelta è nulla o < 1 allora impostala ad 1:
		if (!is_numeric($_POST[$prod]) || ($_POST[$prod] < 0))
		{
			$_POST[$prod] = "1";
		}

		$dett_prodotto = $d->GetRows ("*", "tab_listino_forn",
			"id_prodotto = '" . $_GET['prod'] . "'", "", "", 1);

		$dett_prodotto[0]['qta'] = $_POST[$prod];
		if ($dett_prodotto[0]['disp'] == "-1")
		{
			$dett_prodotto[0]['disp'] = "?";
		}

		if ($pezzo != "acc")
		{
			if (!isset($_SESSION['offerta']['macchina'][$pezzo]))
			{
				$_SESSION['offerta']['macchina'][$pezzo] = array();
			}

			$_SESSION['offerta']['macchina'][$pezzo] = $dett_prodotto[0];
		}else{
			if (!isset($_SESSION['offerta']['accessori']))
			{
				$_SESSION['offerta']['accessori'] = array();
			}
			 array_push($_SESSION['offerta']['accessori'], $dett_prodotto[0]);
		} ?>

		<script type="text/javascript">
			<?php if (isset($_GET['prod'])) { ?>
				parent.location.href="<?php echo $OpenerUrl; ?>";
			<?php } ?>
		</script>
<?php }

	// Se è stata fatta una ricerca:
	if (!empty($_POST))
	{
		$Where = "1";
		if ($_POST['fornitore'] != "%")
		{
			$Where .= " AND fornitore = '" . $_POST['fornitore'] . "' ";
		}

		if ($_POST['modello'] != "%")
		{
			$val = $d->ExpandSearch($_POST['modello']);
			$Where .= " AND (marca LIKE '%$val%' OR modello LIKE '%$val%')";
		}

		if ($_POST['cat_forn'] != "%")
		{
			$Where .= " AND cat_forn = '" . $_POST['cat_forn'] . "' ";
		}

		$tot_res = count($d->GetRows ("id_prodotto", "tab_listino_forn", $Where));
		$order = $d->PageSplit($_GET['page'], $tot_res);
		$Results = $d->GetRows ("*", "tab_listino_forn", $Where, "",
			"fornitore, marca, modello $order", 1);
	}
?>

<div id="search-form">
	<form method="post" action="<?php echo $_SERVER['REQUEST_URI']; ?>">
		<table class="form" style="width: 100%;">
			<tr>
				<th>Ricerca Articoli:</th>
				<td style="text-align: right;">
					<a onclick="Javascript: ClosePopup(); void(0);">
						[ <label class="err" style="cursor: pointer;">Chiudi</label> ]
					</a>
				</td>
			</tr>
			<tr>
				<td colspan="4">
					<select name="fornitore"
						onchange="Javascript: posta('0', '<?php echo $_SERVER['REQUEST_URI']; ?>'); void(0);">
						<option value="%">Tutti i Fornitori</option>
						<?php foreach ($Fornitori as $key => $field) { ?>
							<option value="<?php echo $field['fornitore']; ?>"
								<?php if (isset($_POST['fornitore'])
									&& ($_POST['fornitore'] == $field['fornitore'])) {
										echo "selected=\"selected\""; } ?> >
								<?php echo $field['fornitore']; ?>
							</option>
						<?php } ?>
					</select>

					<select name="cat_forn" style="max-width: 20em;"
						onchange="Javascript: posta('0', '<?php echo $_SERVER['REQUEST_URI']; ?>');" >
						<option value="%">Tutte le Categorie</option>
						<?php foreach ($Categorie as $key => $field) { ?>
							<option value="<?php echo $field['cat_forn']; ?>"
								<?php if (isset($_POST['cat_forn'])
									&& ($_POST['cat_forn'] == $field['cat_forn'])) {
										echo "selected=\"selected\""; } ?> >
								<?php echo $field['cat_forn']; ?>
							</option>
						<?php } ?>
					</select>

					&nbsp;
					<label>Marca/Modello:</label>
					<input name="modello" type="text" style="width: 30em;"
						onKeyPress="return SubmitEnter(this,event);"
						value="<?php if (isset($_POST['modello'])) { echo $_POST['modello']; } ?>" />
				</th>
			</tr>

			<?php $d->ShowResultBar($tot_res,
				"frm_ricerca_articoli_off.php?page=first&amp;pezzo=$pezzo"); ?>

		</table>
	</form>
</div>

<?php if (!empty($_POST) && (count($Results) != 0)) { ?>
	<div id="search-result-2r">
		<form method="post" action="<?php echo $_SERVER['REQUEST_URI']; ?>">
			<table style="width: 100%;" class="dettaglio">
				<tr>
					<th><label>Fornitore:</label></th>
					<th><label>Cod:</label></th>
					<th><label>Marca/Modello:</label></th>
					<th><label>Prezzo:</label></th>
					<th><label>Disp:</label></th>
					<?php if (!isset($_GET['act'])) { ?>
						<th colspan="2"><label>Qta:</label></th>
					<?php } ?>
				</tr>
				<tr><th colspan="10"><hr /></th></tr>
				<?php foreach ($Results as $key => $field) { ?>
					<tr>
						<td>
							<?php echo $field['fornitore']; ?>
						</td>
						<td>
							<?php echo $field['cod_forn']; ?>
						</td>
						<td>
							<?php echo $field['marca'] . " " . $field['modello'];  ?>
						</td>
						<td style="text-align: right;">
							<?php printf("%.2f €", $field['prezzo']);  ?>
						</td>
						<td style="text-align: center;">
							<?php if ($field['disp'] == "-1") {
								echo "?";
							}else{
								echo $field['disp'];
							} ?>
						</td>

						<?php if (!isset($_GET['act'])) { ?>
							<td>
								<input name="<?php echo $field['id_prodotto']; ?>" type="text"
									maxlength="5" style="text-align: right; width: 3em;" />
							</td>

							<td style="text-align: center;">
								<a onclick="javascript: posta('1', 'frm_ricerca_articoli_off.php?prod=<?php
									echo $field['id_prodotto'];
									if (isset($_GET['subact'])) { echo "&amp;subact=" . $_GET['subact']; }
									if (isset($_GET['pezzo'])) { echo "&amp;pezzo=" . $_GET['pezzo']; }?>');"
									title="Seleziona prodotto" style="cursor: pointer;">
									<img src="/Images/Links/select.png" alt="Seleziona prodotto" />
								</a>
							</td>
						<?php }else{ ?>
							<td style="text-align: right;">
								<?php if ($field['ck_com'] == "0") { ?>
									<a href="home.php?act=articolo_com&amp;tomod=<?php
										echo $field['id_prodotto']; ?>" title="Modifica Composto">
										<img src="/Images/Links/edit2.png" alt="edit2" />
									</a>
								<?php } ?>

								<a href="home.php?act=articolo&amp;tomod=<?php
									echo $field['id_prodotto']; ?>" title="Modifica Articolo">
									<img src="/Images/Links/edit.png" alt="edit" />
								</a>

								<a href="javascript: go_conf2('Eliminare l\'articolo?', 'home.php?act=ricerca_art&amp;del=<?php
									echo $field['id_prodotto']; ?>'); void(0);" title="Elimina Articolo">
									<img src="/Images/Links/del.png" alt="del"/>
								</a>
							</td>
						<?php } ?>
					</tr>
				<?php } ?>
				<tr><th colspan="10"><hr /></th></tr>
			</table>
		</form>
	</div>
<?php } ?>

<?php
	$root = substr($_SERVER['DOCUMENT_ROOT'], 0, -1);
	require_once("connection.php");
	require_once("$root/Function/Db.php");
	require_once("$root/Function/Debug.php");
	require_once("$root/Function/Strings.php");

	$Province = GetRows ("tab_province", "", "Prov", $db);

	if (isset($_POST['targa']))
	{
		$Comuni = GetRows ("tab_comuni", "ID_Prov = '" . $_POST['targa'] . "'", "Comune", $db);
	}

	if (isset($_POST['comune']))
	{
		$WhereCom = "tab_comuni.ID = '" . $_POST['comune'] .
			"' AND tab_comuni.ID_Prov = tab_province.ID ";
		$Comune = GetRows ("tab_comuni, tab_province", $WhereCom, "Comune", $db);
	}
?>

<form method="post" action="<?php echo $_SERVER['REQUEST_URI']; ?>">
	<table cellpadding="1" cellspacing="1" border="0" style="width: 100%;">
		<tr>
			<th colspan="3">Selezione Locatità</th>
		</tr>
		<tr>
			<td style="text-align: right; width: 4em;">
				<label>Provincia:</label>
			</td>
			<td>
				<select name="targa" onchange="Javascript: posta('0',
					'<?php echo $_SERVER['REQUEST_URI']; ?>'); void(0);" >
					<option>-</option>
					<?php foreach ($Province as $key => $field) { ?>
						<option value="<?php echo $field['ID']; ?>"
							<?php if ($_POST['targa'] == $field['ID']) {
								echo "selected=\"selected\""; } ?> >
							<?php echo $field['Targa']; ?>
						</option>
					<?php } ?>
				</select>

				&nbsp;
				<label>Comune:</label>
				<select name="comune" style="width: 20em;" onchange="Javascript: posta('0',
					'<?php echo $_SERVER['REQUEST_URI']; ?>'); void(0);" >
					<option>-</option>
					<?php foreach ($Comuni as $key => $field) { ?>
						<option value="<?php echo $field['ID']; ?>"
							<?php if ($_POST['comune'] == $field['ID']) {
								echo "selected=\"selected\""; } ?> >
							<?php echo $field['Comune']; ?>
						</option>
					<?php } ?>
				</select>

				&nbsp;
				<label>Cap:</label>
				<input name="cap" type="text" disabled="disabled" style="width: 7em;"
					value="<?php if(isset($_POST['comune'])) { echo $Comune[0]['Cap']; }; ?>" />
			</td>
		</tr>

		<?php if (isset($_POST['comune'])) { ?>
			<tr><th colspan="3"><br /></th></tr>
			<tr><th colspan="3">Riepilogo selezione</th></tr>

			<tr>
				<td style="text-align: right;">
					<label>Provincia:</label>
				</td>
				<td><?php echo $Comune[0]['Prov']; ?></td>
			</tr>
			<tr>
				<td style="text-align: right;">
					<label>Comune:</label>
				</td>
				<td><?php echo $Comune[0]['Comune']; ?></td>
			</tr>
			<tr>
				<td style="text-align: right;">
					<label>Cap:</label>
				</td>
				<td><?php echo $Comune[0]['Cap']; ?></td>
			</tr>

			<tr>
				<td colspan="3" style="text-align: right;">
					<a onclick="Javascript: SelectLocalita ('<?php echo trim($Comune[0]['Targa']); ?>',
						'<?php echo addslashes(trim($Comune[0]['Comune'])); ?>',
						'<?php echo trim($Comune[0]['Cap']); ?>'); void(0);">
						<img src="img/select.png" alt="" title="Seleziona località"
							style="border: 0px; width: 14px; height: 14px; vertical-align: middle;" />
						Seleziona località
					</a>
				</td>
			</tr>
		<?php } ?>
	</table>
</form>



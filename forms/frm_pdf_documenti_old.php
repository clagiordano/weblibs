<?php ob_end_clean(); error_reporting(~E_ALL);

	//~ tipo 1: 2 loghi agli angoli dati al centro,
	//~ tipo 2: 1 loghi in angolo dati altro angolo

	define ("eur", chr(128));
	require_once("fpdf/fpdf.php");
	//define("LogoPath", "img/logo.jpg"); // passato sul config

	// Dati Ditta:
	$Campi 			= $d->GetRows ("*", "tab_config", "opt LIKE 'ditta%'", "", "opt", 1);
	$Ditta = array();

	foreach ($Campi as $key => $field)
	{
		$Ditta[$field['opt']] = $field['val'];
	}

	//~ TODO switch tipo fattura
	//~ TODO header/footer statico


	$temp_var = $d->GetRows ("Comune", "tab_comuni", "ID = '"
		. $Ditta['ditta_id_citta'] . "'", "", "", 1);
	$Ditta['ditta_citta'] = ucfirst(strtolower($temp_var[0]['Comune']));

	$temp_var = $d->GetRows ("Targa", "tab_province", "ID = '"
		. $Ditta['ditta_id_prov'] . "'", "", "", 1);
	$Ditta['ditta_prov'] = $temp_var[0]['Targa'];

	$Ditta['ditta_str_luogo']	= $Ditta['ditta_cap'] . " " . $Ditta['ditta_citta']
		. " (" . $Ditta['ditta_prov'] . ")";

	$Ditta['ditta_str_contatti'] 	= "Tel: " . $Ditta['ditta_telefono']
		. " - Fax: " . $Ditta['ditta_fax'] . " - E-Mail: " . $Ditta['ditta_mail'];

	//~ echo "<pre>";
		//~ print_r($Ditta);
	//~ echo "</pre>";

	// Dati Documento:
	$Doc = $d->GetRows ("*", "view_documenti d",
		"id_documento = '" . $_GET['id_doc'] . "'", "", "");

	// Dettaglio Documento:
	$DettaglioDoc = $d->GetRows ("*", "tab_dett_documenti",
		"id_documento = '" . $_GET['id_doc'] . "'", "", "");

	//~ Intestazione
	//~ $pdf->SetFont('Arial', 'B', 10);
	//~ $pdf->Image($Ditta['ditta_logo'], 5, 5, 50);
	//~ $pdf->Image($Ditta['ditta_logo'], 155, 5, 50);
	//~ $pdf->Cell(200, 4, $Ditta['ditta_ragione_sociale'], '', '', 'C');
	//~ $pdf->Ln();
	//~ $pdf->SetFont('Arial', 'I', 10);
	//~ $pdf->Cell(200, 4, $Ditta['ditta_indirizzo'], '', '', 'C');
	//~ $pdf->Ln();
	//~ $pdf->Cell(200, 4, $Ditta['ditta_str_luogo'], '', '', 'C');
	//~ $pdf->Ln();
	//~ $pdf->Cell(200, 4, $Ditta['ditta_str_contatti'], '', '', 'C');
	//~ $pdf->Ln();
	//~ $pdf->Cell(200, 4, "P.Iva/C.F.: " . $Ditta['ditta_piva'], '', '', 'C');
	//~ $pdf->Ln();
	//~ $pdf->Cell(200, 4, '', 'T', '', 'C');
	//~ $pdf->Ln();

	class PDF extends FPDF
	{
		function Header()
		{
			// Seleziona Arial grassetto 15
			$this->SetFont('Arial','B',15);
			// Muove verso destra
			$this->Cell(80);
			// Titolo in riquadro
			$this->Cell(30,10,'Title',1,0,'C');
			// Interruzione di linea
			$this->Ln(20);
		}

		function Footer()
		{
			// Va a 1.5 cm dal fondo della pagina
			$this->SetY(-15);
			// Seleziona Arial corsivo 8
			$this->SetFont('Arial','I',8);
			// Stampa il numero di pagina centrato
			$this->Cell(0,10,'Page '.$this->PageNo(),0,0,'C');
		}
	}

	$pdf = new FPDF();
	$pdf->SetMargins(5, 5, 5);
	$pdf->AddPage();

	// Dati Documento
	//~ $pdf->SetFont('Arial', 'B', 8);
	//~ $pdf->Cell(100, 4, 'Destinazione Merce', '', '', '');
//~
	//~ $pdf->Cell(15, 4, 'Spett.le: ', '', '', 'R');
	//~ $pdf->SetFont('Arial', '', 8);
	//~ $pdf->Cell(85, 4, $Doc[0]['ragione_sociale'], '', '', '');
	//~ $pdf->Ln();
//~
	//~ $pdf->Cell(100, 4);
	//~ $pdf->Cell(15, 4, '', '', '', 'R');
	//~ $pdf->Cell(85, 4, $Doc[0]['ind'] . ", " . $Doc[0]['civ'], '', '', '');
	//~ $pdf->Ln();
//~
	//~ $pdf->Cell(30, 4, 'Causale Documento:', '', '', 'R');
	//~ $pdf->SetFont('Arial', 'B', 8);
	//~ $pdf->Cell(70, 4, $Doc[0]['tipo_doc'], '', '', '');
	//~ $pdf->SetFont('Arial', '', 8);
	//~ $pdf->Cell(15, 4, '', '', '', 'R');
	//~ $pdf->Cell(85, 4, $Doc[0]['cap'] . ", " . $Doc[0]['citta']
		//~ . " (" . $Doc[0]['prov'] . ")", '', '', '');
	//~ $pdf->Ln();
//~
	//~ $pdf->Cell(30, 4, 'Data Documento:', '', '', 'R');
	//~ $pdf->Cell(70, 4, $d->Inverti_Data($Doc[0]['data_doc']), '', '', '');
	//~ $pdf->Cell(15, 4, '', '', '', 'R');
	//~ $pdf->Cell(85, 4, "P.Iva/C.F.: " . $Doc[0]['piva'], '', '', '');
	//~ $pdf->Ln();
//~
	//~ $pdf->Cell(30, 4, 'Codice Documento:', '', '', 'R');
	//~ $pdf->Cell(70, 4, $d->Inverti_Data($Doc[0]['cod_documento']), '', '', '');
	//~ $pdf->Cell(100, 4);
	//~ $pdf->Ln();
//~
	//~ $pdf->Cell(30, 4, 'Pagamento:', '', '', 'R');
	//~ $pdf->Cell(70, 4, $d->Inverti_Data($Doc[0]['pagamento']), '', '', '');
	//~ $pdf->Cell(100, 4);
	//~ $pdf->Ln();

	/*  Dettaglio Documento	*/
	//~ $pdf->Cell(200, 4);
	//~ $pdf->Ln();
	//~ $pdf->SetFont('Arial', 'BI', 10);
	//~ $pdf->Cell(200, 4, 'Dettaglio Documento', '', '', 'C');
	//~ $pdf->Ln();
//~
	//~ $pdf->SetFont('Arial', '', 8);
	//~ $pdf->Cell(17, 4, 'Articolo:', 'TLRB', '', 'C');
	//~ $pdf->Cell(105, 4, 'Descrizione:', 'TLRB', '', '');
	//~ $pdf->Cell(17, 4, 'Q.ta:', 'TLRB', '', 'C');
	//~ $pdf->Cell(17, 4, 'Prz.Un:', 'TLRB', '', 'C');
	//~ $pdf->Cell(10, 4, 'Sco:', 'TLRB', '', 'C');
	//~ $pdf->Cell(17, 4, 'Importo:', 'TLRB', '', 'C');
	//~ $pdf->Cell(17, 4, 'Iva:', 'TLRB', '', 'C');
	//~ $pdf->Ln();

	$tot_imponibile = 0;
	$tot_iva = 0;

	foreach ($DettaglioDoc as $key => $field)
	{
		$prodotto = array();

		if ($field['id_prodotto'] != 0)
		{
			$temp = $d->GetRows ("*", "view_prodotti",
				"id_prodotto = '" . $field['id_prodotto'] . "'");
			$prodotto = $temp[0];
		}else{

			$temp_iva = $d->GetRows ("*", "tab_iva",
				"id_iva = '" . $field['id_iva'] . "'");

			$prodotto['cod_forn'] 	= "-";
			$prodotto['cod_int'] 		= "-";
			$prodotto['marca'] 			= "";
			$prodotto['modello'] 		= $field['voce_arb'];
			$prodotto['voce_arb'] 	= $field['voce_arb'];
			$prodotto['iva'] 				= $temp_iva[0]['iva'];
			$prodotto['desc_iva'] 	= $temp_iva[0]['desc_iva'];
			$prodotto['um'] 				= "-";
		}

		$prodotto['qta'] 					= $field['qta'];
		$prodotto['prz'] 					= $field['prz'];
		$prodotto['sco'] 					= $field['sco'];


		/*echo "<pre>";
			print_r($prodotto);
		echo "</pre>";*/


		$prodotto['iva'] = str_replace("1.", "0.", $prodotto['iva']);
		$tot_imponibile += ($prodotto['qta']*$prodotto['prz']);
		$tot_iva += ($prodotto['qta']*$prodotto['prz']) * $prodotto['iva'];

		//~ $pdf->Cell(17, 4, $prodotto['cod_int'], 'TLRB', '', 'C');
		//~ $pdf->Cell(105, 4, $prodotto['modello'], 'TLRB', '', '');
		//~ $pdf->Cell(17, 4, sprintf("%.2f %s", $prodotto['qta'], $prodotto['um']), 'TLRB', '', 'R');
		//~ $pdf->Cell(17, 4, sprintf("%.2f", $prodotto['prz']), 'TLRB', '', 'R');
		//~ $pdf->Cell(10, 4, sprintf("%.2f", $prodotto['sco']), 'TLRB', '', 'R');
		//~ $pdf->Cell(17, 4, sprintf("%.2f", ($prodotto['qta']*$prodotto['prz'])), 'TLRB', '', 'R');
		//~ $pdf->Cell(17, 4, $prodotto['desc_iva'], 'TLRB', '', 'R');
		//~ $pdf->Ln();
	}

	/* Piè di pagina/Totale documento: */
	//~ $pdf->Ln();
	//~ $pdf->Cell(150, 4, '', '', '', 'R');
	//~ $pdf->Cell(30, 4, 'Totale Imponibile: ', 'TL', '', 'R');
	//~ $pdf->Cell(20, 4, sprintf("%.2f " . eur, $tot_imponibile), 'TR', '', 'R');
	//~ $pdf->Ln();
	//~ $pdf->Cell(150, 4, '', '', '', 'R');
	//~ $pdf->Cell(30, 4, 'Sconto: ', 'L', '', 'R');
	//~ $pdf->Cell(20, 4, "??", 'R', '', 'R');
	//~ $pdf->Ln();
	//~ $pdf->Cell(150, 4, '', '', '', 'R');
	//~ $pdf->Cell(30, 4, 'Imponibile Scontato: ', 'L', '', 'R');
	//~ $pdf->Cell(20, 4, "??", 'R', '', 'R');
	//~ $pdf->Ln();
	//~ $pdf->Cell(150, 4, '', '', '', 'R');
	//~ $pdf->Cell(30, 4, 'Imposta: ', 'L', '', 'R');
	//~ $pdf->Cell(20, 4, sprintf("%.2f " . eur, $tot_iva), 'R', '', 'R');
	//~ $pdf->Ln();
	//~ $pdf->SetFont('Arial', 'B', 8);
	//~ $pdf->Cell(150, 4, '', '', '', 'R');
	//~ $pdf->Cell(30, 4, 'Totale Euro: ', 'TLB', '', 'R');
	//~ $pdf->Cell(20, 4, sprintf("%.2f " . eur, ($tot_imponibile+$tot_iva)), 'TRB', '', 'R');
	//~ $pdf->Ln();

	$pdf->Output($Doc[0]['tipo_doc']. ".pdf", 'I');

?>

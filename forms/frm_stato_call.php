<?php
	$tot_res			= -1;
	$stato_call 	= $d->GetRows ("*", "tab_stato_call", "", "", "stato_call");
	$Tecnici 			= $d->GetRows ("id_tecnico, concat(nome, \" \", cognome) as tecnico", "tab_tecnici");
	$Tipo_call 		= $d->GetRows ('id_attivita, concat(cliente, ": ", tipo, " (", note, ")") as tipo',
										"tab_attivita", "", "", "cliente, tipo");
	$Categorie 		= $d->GetRows ('id_cat_att, concat(categoria, " (", prz, ")") as categoria',
									"tab_cat_att", "", "", "categoria");

	if (!empty($_POST))
	{
		$where 	= "id_stato_call LIKE '" . $_POST['id_stato_call'] . "'";
		$where .= " AND id_tecnico LIKE '" . $_POST['id_tecnico'] . "'";

		if ($_POST['id_tipo_att'] != "call")
		{
			$where .= " AND id_attivita LIKE '" . $_POST['id_tipo_att'] . "'";
		}else{
			$where .= " AND id_attivita != '45'";
		}

		if (isset($_POST['ck_data_att']))
		{
			/*
			 * Se una delle date è vuota ma il campo e' stato flaggato
			 * le date mancanti diventano la data odierna
			 */

			if (trim($_POST['data_att_i']) == "")
			{
				$_POST['data_att_i'] = date('d/m/Y');
			}

			if (trim($_POST['data_att_f']) == "")
			{
				$_POST['data_att_f'] = date('d/m/Y');
			}

			$inizio = $d->Inverti_Data($_POST['data_att_i']);
			$fine 	= $d->Inverti_Data($_POST['data_att_f']);
			$where .= " AND (data_att >= '$inizio' AND data_att <= '$fine')";
		}

		if (isset($_POST['ck_note_call']))
		{
			$where .= " AND note_call LIKE '%"
				. $d->ExpandSearch($_POST['note_call']) .  "%'";
		}

		if ($_POST['id_cat'] != "%")
		{
			$where .= " AND id_cat = '" . $_POST['id_cat'] . "'";
		}

		if (trim($_POST['n_call']) != "")
		{
			$where .= " AND n_call LIKE '%" . trim($_POST['n_call']) . "%'";
		}else{
			$_POST['n_call'] = "%";
		}

		if (trim($_POST['den']) != "")
		{
			$where .= " AND den LIKE '%" . trim($_POST['den']) . "%'";
		}else{
			$_POST['den'] = "%";
		}

		if (trim($_POST['citta']) != "")
		{
			$where .= " AND Comune LIKE '%" . trim($_POST['citta']) . "%'";
		}else{
			$_POST['citta'] = "%";
		}

		$tot_res 	= count($d->GetRows ("id_chiamata", "view_call",
								$where, "", "data_att, n_call"));
		$order 		= $d->PageSplit($_GET['page'], $tot_res);
		$Calls 		= $d->GetRows ("*", "view_call", $where, "",
									"data_att, n_call $order");
	}
?>

<div id="search-form">
	<form method="post" action="<?php echo $_SERVER['REQUEST_URI']; ?>">
		<table class="form" style="width: 100%;">
			<tr>
				<th colspan="2">Seleziona i campi fra i quali ricercare:</th>
			</tr>
			<tr>
				<td colspan="2">
					<label>N.Call:</label>
					<input name="n_call" type="text" style="width: 9em;"
						value="<?php if (isset($_POST['n_call'])) { echo $_POST['n_call']; } ?>"
						onKeyPress="return SubmitEnter(this,event);" />

					&nbsp;
					<label>Denominazione:</label>
					<input name="den" type="text" style="width: 17em;"
						value="<?php if (isset($_POST['den'])) { echo $_POST['den']; } ?>"
						onKeyPress="return SubmitEnter(this,event);" />

					&nbsp;
					<label>Stato Call:</label>
					<select name="id_stato_call" style="width: 8em;"
							onchange="Javascript: posta('0', '<?php echo $_SERVER['REQUEST_URI']; ?>'); void(0);" >
							<option value="%">Tutte</option>
							<?php foreach ($stato_call as $key => $field) { ?>
							<option value="<?php echo $field['id_stato_call']; ?>"
											<?php if ($_POST['id_stato_call'] == $field['id_stato_call']) { echo "selected=\"selected\""; } ?> >
									<?php echo htmlentities($field['stato_call']); ?>
							</option>
							<?php } ?>
					</select>

					&nbsp;
					<label>Tecnico:</label>
					<select name="id_tecnico" style="width: 17em;"
						onchange="Javascript: posta('0', '<?php echo $_SERVER['REQUEST_URI']; ?>'); void(0);" >
							<option value="%">Tutti</option>
							<?php foreach ($Tecnici as $key => $field) { ?>
							<option value="<?php echo $field['id_tecnico']; ?>"
											<?php if ($_POST['id_tecnico'] == $field['id_tecnico']) { echo "selected=\"selected\""; } ?> >
									<?php echo htmlentities($field['tecnico']); ?>
							</option>
							<?php } ?>
					</select>
				</td>
			</tr>

			<tr>
				<td colspan="2">
					<label>Periodo Attività:</label>
					<input name="ck_data_att" type="checkbox" value="interval_att"
						<?php if ($_POST['ck_data_att'] != "") { echo "checked=\"checked\""; } ?> />
                    <input name="data_att_i" type="text" style="width: 7em;" title="Data iniziale"
						onfocus="showCalendarControl(this);"
						value="<?php if (isset($_POST['data_att_i'])) { echo $_POST['data_att_i']; } ?>" />

					<input name="data_att_f" type="text" style="width: 7em;" title="Data finale"
						onfocus="showCalendarControl(this);"
						value="<?php if (isset($_POST['data_att_f'])) { echo $_POST['data_att_f']; } ?>" />

					&nbsp;
					<label>Tipo Attività:</label>
                    <select name="id_tipo_att" style="width: 20em;"
                            onchange="javascript: posta('0', '<?php echo $_SERVER['REQUEST_URI']; ?>'); void(0);">
                        <option value="%">Tutte</option>
                        <option value="call"
							<?php if ($_POST['id_tipo_att'] == "call") { echo "selected=\"selected\""; } ?> >Chiamate</option>
                        <?php foreach ($Tipo_call as $key => $field) { ?>
                        <option value="<?php echo $field['id_attivita']; ?>"
                                <?php if ($_POST['id_tipo_att'] == $field['id_attivita']) { echo "selected=\"selected\""; } ?> >
                            <?php echo htmlentities($field['tipo']); ?>
                        </option>
                        <?php } ?>
                    </select>

					&nbsp;
					<label>Note Call:</label>
					<input name="ck_note_call" type="checkbox" value="ck_note_call"
						<?php if ($_POST['ck_note_call'] != "") { echo "checked=\"checked\""; } ?> />
					<input name="note_call" type="text" style="width: 15.5em;"
						value="<?php if (isset($_POST['note_call'])) { echo $_POST['note_call']; } ?>"
						onKeyPress="return SubmitEnter(this,event);" />
				</td>
			</tr>

			<tr>
				<td>
						<label>Categoria:</label>
						<select name="id_cat" style="width: 17em;"
							onchange="Javascript: posta('0', '<?php echo $_SERVER['REQUEST_URI']; ?>'); void(0);" >
								<option value="%">Tutte</option>
								<?php foreach ($Categorie as $key => $field) { ?>
								<option value="<?php echo $field['id_cat_att']; ?>"
												<?php if ($_POST['id_cat'] == $field['id_cat_att']) { echo "selected=\"selected\""; } ?> >
										<?php echo htmlentities($field['categoria']); ?>
								</option>
								<?php } ?>
						</select>

                    &nbsp;
                    <label>Citt&agrave;:</label>
                    <input name="citta" type="text" style="width: 17em;"
						value="<?php if (isset($_POST['citta'])) { echo $_POST['citta']; } ?>"
						onKeyPress="return SubmitEnter(this,event);" />
				</td>
			</tr>

			<?php $d->ShowResultBar($tot_res, "home.php?act=stato_call&page=first"); ?>
		</table>
	</form>
</div>

<div id="search-result-5r">
	<?php if ($tot_res > 0) { ?>
		<table class="dettaglio" style="width: 100%;">
			<tr>
				<th></th>
				<th>N.Call:</th>
				<th>Denominazione</th>
				<th>Data:</th>
				<th>Città:</th>
				<th>Stato:</th>
				<th>Tecnico:</th>
				<th>Cliente:</th>
				<th>Tipo Call:</th>
				<th>Categoria:</th>
				<th>N.Pdl:</th>
				<th></th>
			</tr>
			<tr><th colspan="16"><hr /></th></tr>

			<?php foreach ($Calls as $key => $field) {
						switch ($field['id_stato_call']) {
								case 1:
										$class = "call_comp";
										break;
								case 2:
										$class = "call_cors";
										break;
								case 3:
										$class = "call_pian";
										break;
								case 4:
									 $class = "call_sosp";
									 break;
                } ?>
				<tr>
					<td><label><?php echo $key+1 . ") "; ?></label></td>
					<td style="text-align: left;">
						<a href="home.php?act=call&amp;tomod=<?php echo $field['id_chiamata']; ?>"
							title="Modifica Chiamata"> <?php echo $field['n_call']; ?>
						</a>
					</td>
					<td><?php echo $field['den']; ?></td>
					<td><?php echo $d->Inverti_Data($field['data_att']); ?></td>
					<td>
						<?php echo htmlentities(ucfirst(strtolower($field['Comune'])))
								. " (" . $field['Targa'] . ")"; ?>
					</td>
					<td <?php echo "class=\"$class\""; ?>><?php echo $field['stato_call']; ?></td>
					<td><?php echo htmlentities($field['tecnico']); ?></td>
					<td><?php echo $field['cliente']; ?></td>
					<td><?php echo htmlentities($field['tipo']); ?></td>
					<td><?php echo $field['categoria'] . " (" . $field['tempo']. ")"; ?></td>
					<td style="text-align: center;">
						<?php echo $field['n_pdl']; ?>
					</td>
					<td style="text-align: center;">
						<label class="ok"><?php printf("%.2f €", $field['econ']); ?></label>
					</td>
                    <td>
						<img src="/Images/Links/info.png" alt="info"
							onmouseover="Javascript: ShowPopupDiv ('frm_dettaglio_call.php?tovis=<?php
								echo $field['id_chiamata']; ?>&amp;target=popup', '200px', '200px', '650px', '280px', 'popup', '');"
							onclick="Javascript: ShowPopupDiv ('frm_dettaglio_call.php?tovis=<?php
								echo $field['id_chiamata']; ?>&amp;target=popup', '200px', '200px', '650px', '280px', 'popup', '');"
							onmouseout="javascript: window.setTimeout(function() { document.getElementById('popup').style.display = 'none'; }, 3000);" />
                        <a href="home.php?act=call&amp;tomod=<?php
                            echo $field['id_chiamata']; ?>" title="Modifica Chiamata">
                            <img src="/Images/Links/edit.png" alt="edit" />
                        </a>
                    </td>
				</tr>
			<?php } ?>
			<tr><th colspan="16"><hr /></th></tr>
		<?php } ?>
	</table>
</div>


<div id="search-result">
	<form method="post" action="<?php echo $_SERVER['REQUEST_URI']; ?>">
		<select name="id_chart"
			onchange="Javascript: posta('0', '<?php echo $_SERVER['REQUEST_URI']; ?>'); void(0);">
			<option value="-">Seleziona tipo di grafico</option>
			<option value="-"></option>
			<option value="1" <?php if ($_POST['id_chart'] == "1") { echo "selected=\"selected\""; } ?>>
				Chiamate Ricevute/Chiuse per giorno
			</option>
			<option value="2" <?php if ($_POST['id_chart'] == "2") { echo "selected=\"selected\""; } ?>>
				Chiamate chiuse per tecnico
			</option>
			<option value="3" <?php if ($_POST['id_chart'] == "3") { echo "selected=\"selected\""; } ?>>
				Chiamate/Tecnici per Attività
			</option>
			<option value="4" <?php if ($_POST['id_chart'] == "4") { echo "selected=\"selected\""; } ?>>
				Chiamate per categoria
			</option>
			<option value="5" <?php if ($_POST['id_chart'] == "5") { echo "selected=\"selected\""; } ?>>
				Chiamate per Provincia
			</option>
		</select>
	</form>

<?php
	require_once("$root/Function/libchart/classes/libchart.php");
	require_once("$root/Function/DataTime.php");

	//Padding::Padding  ($top, $right, $bottom, $left)

	define("ChartPath", "tmp/chart.png");
	define("ChartWidth", 1000);
	define("ChartHeight", 400);

	switch($_POST['id_chart'])
	{
		case "1":
			$chart = new VerticalBarChart(ChartWidth, ChartHeight);

			$rCall4day = GetRows ("tab_chiamate WHERE data_att <= '" . date('Y-m-d') . "' AND n_call != 'Pianificata'"
				. " group by data_att", "", "data_att", $db, 1, "count(id_chiamata) as c_day, data_att");
			$rCallclose4day = GetRows ("tab_chiamate WHERE data_att <= '" . date('Y-m-d') . "' AND n_call != 'Pianificata' AND id_stato_call = '1'"
				. " group by data_att", "", "data_att", $db, 1, "count(id_chiamata) as c_day, data_att");

			/*$max_day = count($rCall4day);
			$diff = $max_day - count($rCallclose4day); */

			$dataSet = new XYSeriesDataSet();
			$call4day = new XYDataSet();
			$callclose4day = new XYDataSet();

			foreach ($rCall4day as $key => $field)
			{
				$call4day->addPoint(new Point(Inverti_Data($field['data_att']), $field['c_day']));
				$callclose4day->addPoint(new Point(Inverti_Data($rCallclose4day[$key]['data_att']), $rCallclose4day[$key]['c_day']));
				$tot_call += $field['c_day'];
				$tot_closed_call += $rCallclose4day[$key]['c_day'];
			}

			echo "Chiamate totali Gestite: <label>$tot_call</label>, "
				. "Giorni di attività: <label>" . count($rCall4day) . "</label>, "
				. "Media chiamate giornaliere ricevute: <label>" . round($tot_call/count($rCall4day), 2) . "</label>, "
				. "Media chiamate giornaliere chiuse: <label>" . round($tot_closed_call/count($rCall4day), 2) . "</label><br />";

			$dataSet->addSerie("Chiamate ricevute per giorno", $call4day);
			$dataSet->addSerie("Chiamate chiuse per giorno", $callclose4day);
			$chart->setTitle("Statistiche Chiamate Ricevute/Chiuse per giorno");
			$chart->setDataSet($dataSet);
			$chart->render(ChartPath);
			echo '<img src="' . ChartPath . '" alt="chart" />';
			break;

		case "2":
			$chart = new PieChart(ChartWidth, ChartHeight);
			$chart->getPlot()->setGraphPadding(new Padding("5", "5", "30", "110"));
			$chart->setTitle("Chiamate chiuse per tecnico");
			$Stats = GetRows ("view_call WHERE data_att <= '" . date('Y-m-d') . "' AND n_call != 'Pianificata' AND id_stato_call = '1'"
				. " group by tecnico", "", "tecnico", $db, 1, "count(id_chiamata) as c_day, tecnico");

			$dataSet = new XYDataSet();
			foreach ($Stats as $key => $field)
			{
				$dataSet->addPoint(new Point($field['tecnico']
					. " (" . $field['c_day'] . ")", $field['c_day']));
			}

			$chart->setDataSet($dataSet);
			$chart->render(ChartPath);
			echo '<img src="' . ChartPath . '" alt="chart" />';
			break;

		case "3":
			$chart = new PieChart(ChartWidth, ChartHeight);
			$chart->getPlot()->setGraphPadding(new Padding("5", "5", "150", "30"));
			$chart->setTitle("Chiamate/Tecnici per Attività");
			$result = GetRows ("view_call WHERE data_att <= '" . date('Y-m-d')
				. "' group by id_attivita", "", "cliente", $db, 1,
					'concat(cliente, ": ", tipo, " (", note, ")") as attivita,'
				. ' count(id_attivita) as tot_call,'
				. ' count(distinct(id_tecnico)) as tot_tec');

/*
			$dataSet = new XYSeriesDataSet();
*/
			$call4att = new XYDataSet();
/*
			$tec4att = new XYDataSet();
*/
			foreach ($result as $key => $field)
			{
				$call4att->addPoint(new Point("[" . $field['tot_call'] . "] " . $field['attivita'], $field['tot_call']));
/*
				$tec4att->addPoint(new Point($field['attivita'], $field['tot_tec']));
*/
			}

/*
			$dataSet->addSerie("Chiamate per attività", $call4att);
			$dataSet->addSerie("Tecnici per attività", $tec4att);
*/
/*
			$chart->setDataSet($dataSet);
*/
			$chart->setDataSet($call4att);
			$chart->render(ChartPath);
			echo '<img src="' . ChartPath . '" alt="chart" />';
			break;

		case "4":
			$chart = new PieChart(ChartWidth, ChartHeight);
			$chart->getPlot()->setGraphPadding(new Padding("5", "5", "50", "30"));
			$chart->setTitle("Chiamate per categoria");
			$Stats = GetRows ("view_call WHERE data_att <= '" . date('Y-m-d') . "' AND id_stato_call = '1'"
				. " group by categoria", "", "categoria", $db, 1, 'count(id_chiamata) as c_day,concat(categoria, " ", prz) as cat');

			$dataSet = new XYDataSet();
			foreach ($Stats as $key => $field)
			{
				$dataSet->addPoint(new Point("Categoria " . $field['cat'] . " € (" . $field['c_day'] . " chiamate)", $field['c_day']));
			}

			$chart->setDataSet($dataSet);
			$chart->render(ChartPath);
			echo '<img src="' . ChartPath . '" alt="chart" />';
			break;

		case "5":
			$chart = new PieChart(ChartWidth, ChartHeight);
			$chart->getPlot()->setGraphPadding(new Padding("5", "5", "50", "30"));
			$chart->setTitle("Chiamate per Provincia");

			$Stats = GetRows ("view_call WHERE data_att >= '2010-03-01' and data_att <= '2010-03-31"
				. "' AND id_stato_call = '1' AND id_tecnico = '2' and id_attivita != '45' GROUP BY Targa", "", "Targa", $db, 0, "count(id_chiamata) as tot_prov, Targa");

			$dataSet = new XYDataSet();
			foreach ($Stats as $key => $field)
			{
				$dataSet->addPoint(new Point("Provincia: " . $field['Targa'] . " (" . $field['tot_prov'] . ")", $field['tot_prov']));
			}

			$chart->setDataSet($dataSet);
			$chart->render(ChartPath);
			echo '<img src="' . ChartPath . '" alt="chart" />';
			break;
	}

?>

</div>

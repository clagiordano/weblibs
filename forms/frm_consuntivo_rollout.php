<?php
	$root = substr($_SERVER['DOCUMENT_ROOT'], 0, -1);
	require_once("$root/Function/Rollout.php");
	$tot_res = "";

	if (isset($_GET['tomod']) && (is_numeric($_GET['tomod']))
		&& is_numeric($_GET['key']) && !empty($_POST))
	{
		$Valori = array('id_stato' => $_POST['id_stato_call' . $_GET['key']]);
		$Status = $d->UpdateRow ($Valori, "", "tab_filiali", "id_filiale = '"
			. $_GET['tomod'] . "'", "", 1);
	}

	$Stato = $d->GetRows ("*", "tab_stato_call", "", "", "stato_call", 1);
	//$Tecnici = GetRows ("tab_tecnici", "", "", $db, 1,
		//"id_tecnico, concat(nome, \" \", cognome) as tecnico");
	$Ditte = $d->GetRows ("*", "tab_ditte", "", "", "ragione_sociale", 1);
	$Province = $d->GetRows ("distinct(prov)", "tab_filiali", "prov != ''", "", "prov", 1);

	if (isset($_POST['prov']))
	{
		$Citta = $d->GetRows ("distinct(citta)", "tab_filiali",
			"prov like '" . $_POST['prov'] . "' ", "", "citta", 1);
	}else{
		$Citta = array();
	}
	if (isset($_POST['citta']))
	{
		$Filiali = $d->GetRows ("id_filiale, concat(den, \" (\", cod_fil, \")\") as filiale",
			"tab_filiali", " citta like '" . addslashes($_POST['citta']) . "' AND prov like '"
			. $_POST['prov'] . "'", "", "den", 1);
	}else{
		$Filiali = array();
	}

	if (!empty($_POST))
	{
		$where = "";
		if (trim($_POST['cod_fil']) == "")
		{
			$_POST['cod_fil'] = "%";
		}

		$where .= " prov LIKE '" . $_POST['prov'] . "'";
		$where .= " AND citta LIKE '" . addslashes($_POST['citta']) . "'";
		$where .= " AND id_stato LIKE '" . $_POST['id_stato_call'] . "'";
		$where .= " AND id_filiale_f LIKE '" . $_POST['id_filiale'] . "'";
		$where .= " AND cod_fil LIKE '%" . $_POST['cod_fil'] . "%'";
		$where .= " AND id_ditta LIKE '" . $_POST['id_ditta'] . "'";

		if (isset($_POST['ck_data_att']))
		{
			/*
			 * Se una delle date è vuota ma il campo e' stato flaggato
			 * le date mancanti diventano la data odierna
			 */

			if (trim($_POST['data_att_i']) == "")
			{
				$_POST['data_att_i'] = date('d/m/Y');
			}

			if (trim($_POST['data_att_f']) == "")
			{
				$_POST['data_att_f'] = date('d/m/Y');
			}

			$inizio = $d->Inverti_Data($_POST['data_att_i'], "-", "/");
			$fine = $d->Inverti_Data($_POST['data_att_f'], "-", "/");
			$where .= " AND (data_pianif >= '$inizio' AND data_pianif <= '$fine')";
		}

		/*if (isset($_POST['ck_request']))
		{
			$where .= " AND request != ''";
		}*/

		if (trim($_POST['den']) == "")
		{
			$_POST['den'] = "%";
		}else{
			$where .= " AND den LIKE '%" . $d->ExpandSearch($_POST['den']) . "%'";
		}

		$tot_res = count($d->GetRows ("*", "view_pianificazione", $where, "", "den", 1));
		//$order = PageSplit($_GET['page'], $tot_res);
		// Forzo la visualizzazione di tutti i risultati: ("all")
		$order = $d->PageSplit("all", $tot_res);
		$Results = $d->GetRows ("*", "view_pianificazione", $where,
			"data_pianif, den, citta $order", "", 1);

		$ListFil = array();
		foreach ($Results as $key => $field)
		{
			array_push($ListFil, $field['id_filiale_f']);
		}
	}else{
		$_POST['server'] = "20.00";
		$_POST['spedizioni'] = "0.80";
		$_POST['staging'] = "3.00";
		$_POST['sostituzioni'] = "12.00";
		$_POST['riconfigurazioni'] = "12.00";
		//$_POST['disinstallazioni'] = "12.00";
		$_POST['disinstallazioni'] = "0.00";
		$_POST['ritiri'] = "1.00";
		$_POST['pincident'] = "12.00";
	}
?>

<div id="search-form">
	<form method="post" action="?act=consuntivo">
        <table class="form" style="width: 100%;">
					<tr>
						<th colspan="2">Seleziona i campi fra i quali ricercare:</th>
					</tr>
					<tr>
						<td colspan="2">
							<label>Codice Filiale:</label>
							<input name="cod_fil" type="text"
								onKeyPress="return SubmitEnter(this,event);"
								style="width: 7em;"
								value="<?php if (isset($_POST['cod_fil']))
									{ echo $_POST['cod_fil']; } ?>" />

							&nbsp;
							<label>Denominazione:</label>
							<input name="den" type="text"
								onKeyPress="return SubmitEnter(this,event);"
								style="width: 11em;"
								value="<?php if (isset($_POST['den'])) { echo $_POST['den']; } ?>" />

							&nbsp;
							<select name="id_stato_call">
									<option value="%">Tutti gli stati</option>
									<?php foreach ($Stato as $key => $field) { ?>
									<option value="<?php echo $field['id_stato_call']; ?>"
													<?php if ($_POST['id_stato_call'] == $field['id_stato_call'])
														{ echo "selected=\"selected\""; } ?> >
											<?php echo htmlentities($field['stato_call']); ?>
									</option>
									<?php } ?>
							</select>

							&nbsp;
							<select name="id_ditta">
									<option value="%">Tutte le competenze</option>
									<?php foreach ($Ditte as $key => $field) { ?>
									<option value="<?php echo $field['id_ditta']; ?>"
													<?php if ($_POST['id_ditta'] == $field['id_ditta'])
														{ echo "selected=\"selected\""; } ?> >
											<?php echo htmlentities($field['ragione_sociale']); ?>
									</option>
									<?php } ?>
							</select>
						</td>
					</tr>

					<tr>
						<td colspan="2">
							<label>Periodo Attività:</label>
							<input name="ck_data_att" type="checkbox" value="interval_att"
								<?php if ($_POST['ck_data_att'] != "") { echo "checked=\"checked\""; } ?> />
									<input name="data_att_i" type="text" style="width: 7em;"
										title="Data iniziale"
										onfocus="showCalendarControl(this);"
										value="<?php if (isset($_POST['data_att_i'])) { echo $_POST['data_att_i']; } ?>" />

							<input name="data_att_f" type="text" style="width: 7em;" title="Data finale"
								onfocus="showCalendarControl(this);"
								value="<?php if (isset($_POST['data_att_f'])) { echo $_POST['data_att_f']; } ?>" />

							&nbsp;
							<label>Localit&agrave;:</label>
							<select name="prov"
								onchange="Javascript: posta('0',
									'?act=consuntivo&amp;page=first'); void(0);" >
								<option value="%">Tutte le province</option>
								<?php foreach ($Province as $key => $field) { ?>
									<option value="<?php echo $field['prov']; ?>"
										<?php if ($_POST['prov'] == $field['prov'])
											{ echo "selected=\"selected\""; }?> >
										<?php echo $field['prov']; ?>
									</option>
								<?php } ?>
							</select>

							&nbsp;
							<select name="citta"
								onchange="Javascript: posta('0',
									'?act=consuntivo&amp;page=first'); void(0);" >
								<option value="%">Tutte le citt&agrave;</option>
								<?php foreach ($Citta as $key => $field) { ?>
									<option value="<?php echo $field['citta']; ?>"
										<?php if ($_POST['citta'] == $field['citta'])
											{ echo "selected=\"selected\""; }?> >
										<?php echo $field['citta']; ?>
									</option>
								<?php } ?>
							</select>

							&nbsp;
							<select name="id_filiale"
								onchange="Javascript: posta('0',
									'?act=consuntivo&amp;page=first'); void(0);" >
								<option value="%">Tutte le filiali</option>
								<?php foreach ($Filiali as $key => $field) { ?>
									<option value="<?php echo $field['id_filiale']; ?>"
										<?php if ($_POST['id_filiale'] == $field['id_filiale'])
											{ echo "selected=\"selected\""; }?> >
										<?php echo $field['filiale']; ?>
									</option>
								<?php } ?>
							</select>
						</td>
					</tr>

					<tr>
						<td>
							<label>Server:</label>
							<input name="server" type="text" style="width: 4em;"
								value="<?php if (isset($_POST['server'])) { echo $_POST['server']; } ?>" />

							&nbsp;
							<label>Sped:</label>
							<input name="spedizioni" type="text" style="width: 4em;"
								value="<?php if (isset($_POST['spedizioni'])) { echo $_POST['spedizioni']; } ?>" />

							&nbsp;
							<label>Stag:</label>
							<input name="staging" type="text" style="width: 4em;"
								value="<?php if (isset($_POST['staging'])) { echo $_POST['staging']; } ?>" />

							&nbsp;
							<label>Sost:</label>
							<input name="sostituzioni" type="text" style="width: 4em;"
								value="<?php if (isset($_POST['sostituzioni'])) { echo $_POST['sostituzioni']; } ?>" />

							&nbsp;
							<label>Riconf:</label>
							<input name="riconfigurazioni" type="text" style="width: 4em;"
								value="<?php if (isset($_POST['riconfigurazioni'])) { echo $_POST['riconfigurazioni']; } ?>" />

							&nbsp;
							<label>Disinst:</label>
							<input name="disinstallazioni" type="text" style="width: 4em;"
								value="<?php if (isset($_POST['disinstallazioni'])) { echo $_POST['disinstallazioni']; } ?>" />

							&nbsp;
							<label>Ritiri:</label>
							<input name="ritiri" type="text" style="width: 4em;"
								value="<?php if (isset($_POST['ritiri'])) { echo $_POST['ritiri']; } ?>" />

							&nbsp;
							<label>Inc:</label>
							<input name="pincident" type="text" style="width: 4em;"
								value="<?php if (isset($_POST['pincident'])) { echo $_POST['pincident']; } ?>" />

						</td>
					</tr>

					<?php $d->ShowResultBar($tot_res, "home.php?act=consuntivo"); ?>

				</table>
    </form>
</div>

<div id="search-result-5r">
	<form method="post" action="?act=consuntivo">
		<?php if ($tot_res > 0) {
			PrintCheckRequest($ListFil, "consuntivo");
		} ?>
	</form>
</div>


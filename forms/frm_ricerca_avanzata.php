<?php
	$tot_res = "-1";

	if (isset($_GET['del']))
	{
		$d->DeleteRows ("tab_anagrafica", "id_anag = '" . $_GET['del'] . "'", $db);
		$Result = "<label class=\"ok\">Anagrafica eliminata.</label>";
	}

	if (!isset($_GET['subact']))
	{
		$ResetPath = "home.php?act=ricerca_av";
	}else{
		$ResetPath = "frm_ricerca_avanzata.php?subact=" . $_GET['subact'];

		switch ($_GET['subact'])
		{
			case "select":
				$subact = "select";
				break;

			case "offerta":
				$subact = "offerta";
				break;

			case "piano_conti":
				$subact = "piano_conti";
				break;

			default:
				$subact = "search";
		}
	}



	if (isset($_GET['select']))
	{
		if ($subact == "select")
		{
			$_SESSION['documento']['id_anagrafica'] = $_GET['select']; ?>
			<script type="text/javascript">
				parent.location.href="home.php?act=documenti";
			</script><?php
		}

		if ($subact == "piano_conti")
		{
			if (isset($_GET['key']))
			{
				// Id anagrafica per la singola voce
				$_SESSION['piano']['dettaglio'][$_GET['key']]['id_anagrafica'] = $_GET['select'];
			}else{
				// Id anagrafica per il documento
				$_SESSION['piano']['id_anagrafica'] = $_GET['select'];
			}
		?>
			<script type="text/javascript">
				parent.location.href="home.php?act=piano_conti<?php
					if (isset($_SESSION['piano']['tomod']))
					{
						echo "&tomod=" . $_SESSION['piano']['tomod'];
					} ?>	";
			</script>
		<?php
		}

		if ($subact == "offerta")
		{
			$_SESSION['offerta']['id_anagrafica'] = $_GET['select']; ?>
			<script type="text/javascript">
				alert('<?php echo $_GET['select']; ?>');
				<?php if ($_SESSION['offerta']['modifica'] != 0)
					{
						$_SESSION['offerta']['modificata'] = "y"; ?>
							parent.location.href="home.php?act=assembla&id_off=<?php
								echo $_SESSION['offerta']['modifica']; ?>";
					<?php }else{ ?>
						parent.location.href="home.php?act=assembla"
					<?php } ?>
			</script><?php
		}
	}

	if (!empty($_POST))
	{
		$Where = "1";

		if ($_POST['tipo'] != "%")
		{
			$Where .= " AND tipo = '" . $_POST['tipo'] . "'";
		}

		if (trim($_POST['ragione_sociale']) != "")
		{
			$tmp_var = $d->ExpandSearch($_POST['ragione_sociale']);
			$Where .= " AND ragione_sociale LIKE '%$tmp_var%'";
		}

		if (trim($_POST['nome']) != "")
		{
			$tmp_var = $d->ExpandSearch($_POST['nome']);
			$Where .= " AND nome LIKE '%$tmp_var%'";
		}

		if (trim($_POST['cognome']) != "")
		{
			$tmp_var = $d->ExpandSearch($_POST['cognome']);
			$Where .= " AND cognome LIKE '%$tmp_var%'";
		}

		if (trim($_POST['piva']) != "")
		{
			$tmp_var = $d->ExpandSearch($_POST['piva']);
			$Where .= " AND piva LIKE '%$tmp_var%'";
		}

		if (trim($_POST['tel']) != "")
		{
			$tmp_var = $d->ExpandSearch($_POST['tel']);
			$Where .= " AND (tel LIKE '%$tmp_var%' OR cell LIKE '%$tmp_var%')";
		}

		$tot_res = count($d->GetRows ("id_anag", "view_anagrafica", $Where));
		$order = $d->PageSplit($_GET['page'], $tot_res);

		$Risultati = $d->GetRows ("*", "view_anagrafica", $Where, "",
			"tipo, ragione_sociale $order", 1);
	}
?>

<section id="search-form">
	<form method="post" action="<?php echo $_SERVER['REQUEST_URI']; ?>">
		<table class="form" style="width: 99%;">
			<?php if (!isset($_GET['act'])) { ?>
				<tr>
					<td style="text-align: right;" colspan="2">
							<a onclick="Javascript: ClosePopup(); void(0);">
								[ <label class="err" style="cursor: pointer;">Chiudi</label> ]
							</a>
					</td>
				</tr>
			<?php } ?>

			<tr>
				<th colspan="2">
					Seleziona i campi fra i quali ricercare:

					&nbsp;
					<a href="<?php if (isset($_GET['act']))
						{
							echo "home.php?act=anagrafica";
						}else{
							echo "frm_anagrafica.php";
						} ?>">
						<img src="/Images/Links/new.png" alt="Nuova Anagrafica" />
						Nuova Anagrafica
					</a>
				</th>
			</tr>

			<tr>
				<td colspan="2">
					<select name="tipo">
						<option value="%"
							<?php if (isset($_POST['tipo']) && ($_POST['tipo'] == "%"))
							{
								echo "selected=\"selected\"";
							} ?>>Tutte le anagrafiche</option>
						<option value="cliente"
							<?php if (isset($_POST['tipo']) && ($_POST['tipo'] == "cliente"))
							{
								echo "selected=\"selected\"";
							} ?>>Clienti</option>
						<option value="fornitore"
							<?php if (isset($_POST['tipo']) && ($_POST['tipo'] == "fornitore"))
							{
								echo "selected=\"selected\"";
							} ?>>Fornitori</option>
					</select>

					&nbsp;
					<input name="ragione_sociale" type="text" style="width: 16em;"
						placeholder="Ragione Sociale" title="Ragione Sociale"
						value="<?php if (isset($_POST['ragione_sociale'])) { echo $_POST['ragione_sociale']; } ?>"
						onKeyPress="return SubmitEnter(this,event);" />

					&nbsp;
					<input name="nome" type="text" style="width: 15em;"
						placeholder="Nome" title="Nome"
						value="<?php if (isset($_POST['nome'])) { echo $_POST['nome']; } ?>"
						onKeyPress="return SubmitEnter(this,event);" />

					&nbsp;
					<input name="cognome" type="text" style="width: 15em;"
						placeholder="Cognome" title="Cognome"
						value="<?php if (isset($_POST['cognome'])) { echo $_POST['cognome']; } ?>"
						onKeyPress="return SubmitEnter(this,event);" />

					&nbsp;
					<input name="piva" type="text" style="width: 16em;"
						placeholder="P.Iva o C.F." title="P.Iva o C.F."
						value="<?php if (isset($_POST['piva'])) { echo $_POST['piva']; } ?>"
						onKeyPress="return SubmitEnter(this,event);" />

					&nbsp;
					<input name="tel" type="text" style="width: 15em;"
						placeholder="Telefono o Cellulare" title="Telefono o Cellulare"
						value="<?php if (isset($_POST['tel'])) { echo $_POST['tel']; } ?>"
						onKeyPress="return SubmitEnter(this,event);" />
				</td>
			</tr>

			<?php
				if (!isset($_GET['subact']))
				{
					$d->ShowResultBar($tot_res, "home.php?act=ricerca_av");
				}else{
					$d->ShowResultBar($tot_res, "frm_ricerca_avanzata.php?subact="
						. $_GET['subact']);
				}
			?>

		</table>
	</form>
</section>

<?php if ($tot_res > 0) { ?>
	<section id="search-result">
		<form method="post" action="<?php echo $_SERVER['REQUEST_URI']; ?>">
			<table class="dettaglio" style="width: 100%;">
				<tr>
					<th><label>Tipo:</label></th>
					<th><label>Rag. sociale:</label></th>
					<th><label>Cognome:</label></th>
					<th><label>Nome:</label></th>
					<th><label>P. Iva/Cod. Fisc:</label></th>
					<th><label>Telefono:</label></th>
					<th><label>Cellulare:</label></th>
					<th><!-- links !--></th>
				</tr>
				<tr><th colspan="9"><hr /></th></tr>
				<?php
					foreach ($Risultati as $key => $field)
					{
						$id_anag = $field['id_anag'];
						$ddelconferma = "Eliminare " . $field['ragione_sociale'] . "?";
						$delurl = "home.php?act=ricerca_av&amp;del=$id_anag";
					?>
					<tr>
						<td><?php echo $field['tipo']; ?></td>
						<td>
							<?php if (isset($subact)) { ?>
								<a onclick="javascript: conferma_salva('1', 'Confermi la selezione dell\'anagrafica?',
										'<?php echo $_SERVER['REQUEST_URI']; ?>&amp;select=<?php echo $field['id_anag']; ?>'); void(0);"
										title="Seleziona anagrafica">
										<?php echo $field['ragione_sociale']; ?>
									</a>
								<?php }else{
									echo $field['ragione_sociale'];
							} ?>
						</td>
						<td><?php echo $field['cognome']; ?></td>
						<td><?php echo $field['nome']; ?></td>
						<td><?php echo $field['piva']; ?></td>
						<td><?php echo $field['tel']; ?></td>
						<td><?php echo $field['cell']; ?></td>

						<?php if (!isset($subact)) { ?>
							<td style="text-align: left;">
								<a href="home.php?act=anagrafica&amp;tomod=<?php
									echo $id_anag; ?>" title="Modifica">
										<img src="/Images/Links/edit.png" alt="edit" />
								</a>

								<a onclick="Javascript: go_conf2('<?php echo $ddelconferma; ?>' ,
									'<?php echo $delurl; ?>'); void(0);"
									title="Elimina" style="cursor: pointer;">
										<img src="/Images/Links/del.png" alt="del" />
								</a>
							</td>
						<?php }else{ ?>
							<th style="text-align: right;" colspan="2">
								<a href="home.php?act=ricerca_av&amp;subact=<?php echo $subact; ?>&amp;select=<?php
									echo $id_anag; if (isset($_GET['key'])) { echo "&amp;key=" . $_GET['key']; } ?>" title="Seleziona" style="cursor: pointer;">
									<img src="/Images/Links/select.png" alt="select" />
								</a>
							</th>
						<?php } ?>
				<?php } ?>
				<tr><th colspan="9"><hr /></th></tr>
			</table>
		</form>
	</section>
<?php } ?>

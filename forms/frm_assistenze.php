<?php
/*
 *  frm_assistenze.php
 *
 *  Copyright 2009 - 2014 Claudio Giordano <claudio.giordano@autistici.org>
 *
 */

 $msg = "";

	if (isset($_POST['inizia']))
	{
		if ($_POST['id_cliente'] != "-")
		{
			//~ $uid = GetIdUser($_SESSION['rm_user'], $db);
			$_POST['inizio'] = date("Y-m-d H:i:s");
			//~ $_POST['id_user'] = $uid;
			$Status = $d->SaveRow ($_POST, array("Avvia"), "tab_assistenze");
			$msg = $Status[0];
		}else{
			$msg = "<label class=\"err\">Errore, selezionare un cliente.</label>";
		}
	}

	if (isset($_GET['term']))
	{
		$Valori = array('fine' => date("Y-m-d H:i:s"));
		$Status = $d->UpdateRow ($Valori, array("Avvia"), "tab_assistenze",
			"id_assistenza = '" . $_GET['term'] . "'");
	}

	$Clienti = $d->GetRows ("*", "tab_anagrafica", "id_tipo = '2'", "", "ragione_sociale");
	$InCorso = $d->GetRows ("*", "tab_assistenze, tab_anagrafica, tab_utenti",
		"fine = '0000-00-00 00:00:00'
		AND tab_anagrafica.id_anag = tab_assistenze.id_cliente
		AND tab_utenti.id_user = tab_assistenze.id_user", "", "ragione_sociale, inizio");

?>

<form method="post" action="<?php echo $_SERVER['REQUEST_URI']; ?>">
	<table class="form" style="width: 100%;">
		<tr>
			<th colspan="8">Nuova Assistenza</th>
		</tr>
		<tr>
			<td style="text-align: right;">
				<label>Seleziona Cliente:</label>
			</td>
			<td>
				<select name="id_cliente" style="width: 280px;">
					<option value="-">-</option>
					<?php foreach ($Clienti as $key => $field) { ?>
						<option value="<?php echo $field['id_anag']; ?>"
							<?php if ($field['id_anag'] == $_POST['id_cliente']) {
								echo "selected=\"selected\"";
							} ?> >
							<?php echo $field['ragione_sociale']; ?>
						</option>
					<?php } ?>
				</select>

				&nbsp;
				<input name="inizia" type="submit" value="Avvia" />
			</td>
		</tr>
		<tr>
			<th colspan="5" style="text-align: right;">
				<?php echo $msg; ?>
			</th>
		</tr>
	</table>
</form>


<form method="post" action="<?php echo $_SERVER['REQUEST_URI']; ?>">
	<table class="form" style="width: 100%;">
		<tr>
			<th colspan="5">Assistenze in corso</th>
		</tr>
		<tr><th colspan="5"><br /></th></tr>
		<?php if (count($InCorso) != 0) { ?>
			<tr>
				<th colspan="5" style="text-align: right;">
					<?php echo count($InCorso) ?>
					<label class="err"> assistenze attualmente in corso.</label>
				</th>
			</tr>
			<tr><th colspan="5"><br /></th></tr>
			<tr>
				<th>Rag. Sociale:</th>
				<th>Inizio:</th>
				<th>Trascorsi:</th>
				<th>Utente:</th>
				<th></th>
			</tr>
			<tr><th colspan="5"><hr /></th></tr>
			<?php foreach ($InCorso as $key => $field) {
				$inizio = Inverti_DataTime($field['inizio']);
				$diff = DateTimeDiff($field['inizio'], date("Y-m-d H:i:s")); ?>
				<tr>
					<td><?php echo $field['ragione_sociale']; ?></td>
					<td><?php echo $inizio; ?></td>
					<td><label class="err"><?php echo $diff, " ore"; ?></label></td>
					<td><?php echo $field['user']; ?></td>
					<td>
						<input name="ck<?php echo $field['id_assistenza']; ?>" type="checkbox"
							onchange="Javascript: go_conf('Terminare l\'assistenza?' ,
								'home.php?act=assistenze&amp;term=<?php
									echo $field['id_assistenza']; ?>',
								'home.php?act=assistenze'); "/>
					</td>
				</tr>
			<?php } ?>
			<tr><th colspan="5"><hr /></th></tr>
		<?php }else{ ?>
			<tr>
				<th style="text-align: right;">
					<label class="ok">Nessun'assistenza in corso.</label>
				</th>
			</tr>
		<?php } ?>
	</table>
</form>

<?php ob_end_clean();
	  $bullet = chr(149);
	  	 $deg = chr(186);
	 $TotDare = 0;
	$TotAvere = 0;

global $Titolo;
$Sottoconti = array();

if (isset($_GET['tipo']) && $_GET['tipo'] != "")
{
	switch ($_GET['tipo'])
	{
		case "sp":
			$Titolo = "Stato Patrimoniale";
			$SubTitolo1 = "Attivo (Dare):";
			$SubTitolo2 = "Passivo (Avere):";
			$SpDare = array(1, 2, 3, 9, 7, 6, 4, 5, 10, 8);
			$SpAvere = array(14, 16, 20, 17, 18, 15, 42, 43, 19);
			break;

		case "ce":
			$Titolo = "Conto Economico";
			$SubTitolo1 = "Costi:";
			$SubTitolo2 = "Ricavi:";
			$SpDare = array(27, 25, 24, 22, 23, 29, 31, 28, 30, 32, 26, 33, 34, 35);
			$SpAvere = array(37, 38, 36, 39, 40);
			break;
	}
}else{
	$Titolo = "Stato Patrimoniale";
	$SubTitolo1 = "Attivo (Dare):";
	$SubTitolo2 = "Passivo (Avere):";
	$SpDare = array(1, 2, 3, 9, 7, 6, 4, 5, 10, 8);
	$SpAvere = array(14, 16, 20, 17, 18, 15, 42, 43, 19);
}

if (count($SpDare) > count($SpAvere))
{
	$MaxConto = count($SpDare);
}else{
	$MaxConto = count($SpAvere);
}


/*
 * 22/08/2009 01:04:49 CEST Claudio Giordano
 *
 * AddPage([string orientation ,[ mixed format]])
 * Cell(float w [, float h [, string txt [, mixed border [, int ln [, string align [, boolean fill [, mixed link]]]]]]])
 * Line(float x1, float y1, float x2, float y2)
 */

class PDF extends FPDF
{
	function Header()
	{
		global $Titolo;
		global $SubTitolo1;
		global $SubTitolo2;
		$this->SetFont('Arial', 'BI', 20);
		$this->Cell(200, 10, $Titolo, 0, 0, 'C');
		$this->Ln(15);
		$this->SetFont('Arial', 'B', 9);
		/*
		 * 22/08/2009 00:23:24 CEST Claudio Giordano
		 *
		 * Intestazione tabella:
		 */
		$this->Cell(80, 4, $SubTitolo1, 'B', '', 'C');
		$this->Cell(20, 4, 'Importo:', 'BR', '', 'C');
		$this->Cell(80, 4, $SubTitolo2, 'B', '', 'C');
		$this->Cell(20, 4, 'Importo:', 'B', '', 'C');
		$this->Ln();
	}

	function Footer()
	{
		//Position at 1.5 cm from bottom
		$this->SetY(-15);
		$this->SetFont('Arial', '', 9);
		$this->Cell(40, 4, 'Data: ' . date('d/m/Y'), 0, '', 'L');
		$this->Cell(0, 4, 'Pagina ' . $this->PageNo() . "/{nb}", 0, 0 ,'C');
	}
}

$pdf=new PDF();
$pdf->AliasNbPages();
$pdf->SetMargins(5, 5, 5);
$pdf->AddPage();

for ($i = 0; $i < $MaxConto; $i++)
{
	$pdf->SetFont('Arial', 'B', 9);

	if ($SpDare[$i] != "")
	{
		$desc_conto = GetRows ("tab_conti", "id_conto = '" . $SpDare[$i] . "'", "", $db);
		$pdf->Cell('13', '4',  "(" . $desc_conto[0]['codice_conto'] . ")", 0, '', 'R');
		$pdf->Cell('87', '4', ucfirst(strtolower($desc_conto[0]['descr_conto'])), "R");
	}else{
		$pdf->Cell('100', '4', '', 0);
	}

	if ($SpAvere[$i] != "")
	{
		$desc_conto = GetRows ("tab_conti", "id_conto = '" . $SpAvere[$i]. "'", "", $db);
		$pdf->Cell('13', '4', "(" . $desc_conto[0]['codice_conto'] . ")", 0,'', 'R');
		$pdf->Cell('87', '4', ucfirst(strtolower($desc_conto[0]['descr_conto'])));
	}else{
		$pdf->Cell('100', '4', '');
	}
	$pdf->Ln();

	$ScDare = GetRows ("tab_sottoconti", "id_conto = '" . $SpDare[$i] . "'", "", $db);
	$ScAvere = GetRows ("tab_sottoconti", "id_conto = '" . $SpAvere[$i] . "'", "", $db);


	if (count($ScDare) > count($ScAvere))
	{
		$MaxSott = count($ScDare);
	}else{
		$MaxSott = count($ScAvere);
	}

	$TotContoD = 0;
	$TotContoA = 0;
	$pdf->SetFont('Arial', '', 9);

	for ($a = 0; $a < $MaxConto; $a++)
	{
		if ($ScDare[$a]['id_sottoconto'] != "")
		{
			$SubTotD = GetRows ("tab_dett_piano_conti", "id_sottoconto = '"
				. $ScDare[$a]['id_sottoconto'] . "'", "", $db, 1,
				"sum(importo_d) as tot_dare, sum(importo_a) as tot_avere");
			$SubTot = sprintf("%.2f", ($SubTotD[0]['tot_dare']-$SubTotD[0]['tot_avere']));
			$TotContoD += ($SubTotD[0]['tot_dare']-$SubTotD[0]['tot_avere']);
			$TotDare += $SubTot;

			$pdf->Cell('13', '4', "(" . $ScDare[$a]['codice_sottoconto'] . ")", 0);
			$pdf->Cell('70', '4', $ScDare[$a]['descr_sottoconto'], 0);
			$pdf->Cell('17', '4', $SubTot, 'R', '', 'R');
		}else{
			$pdf->Cell('100', '4', '', 'R');
			//$pdf->Cell('100', '4', 'Totale Conto D: ' , 1, '', 'R');
		}

		if ($ScAvere[$a]['id_sottoconto'] != "")
		{
			$SubTotA = GetRows ("tab_dett_piano_conti", "id_sottoconto = '"
				. $ScAvere[$a]['id_sottoconto'] . "'", "", $db, 1,
				"sum(importo_d) as tot_dare, sum(importo_a) as tot_avere");

			$SubTot = sprintf("%.2f", ($SubTotA[0]['tot_avere']-$SubTotA[0]['tot_dare']));
			$TotContoA += ($SubTotA[0]['tot_avere']-$SubTotA[0]['tot_dare']);
			$TotAvere += $SubTot;

			$pdf->Cell('13', '4', "(" . $ScAvere[$a]['codice_sottoconto'] . ")", 0);
			$pdf->Cell('70', '4', $ScAvere[$a]['descr_sottoconto'], 0);
			$pdf->Cell('17', '4', $SubTot, '', '', 'R');
		}else{
			$pdf->Cell('100', '4', '', 0);
		}
		$pdf->Ln();
	}

	$pdf->SetFont('Arial', 'B', 9);

	if ($SpDare[$i] != "")
	{
		$TotContoD = sprintf("%.2f", $TotContoD);
		$pdf->Cell('83', '4', 'Totale Conto:', 'T', '', 'R');
		$pdf->Cell('17', '4', $TotContoD, 'T', '', 'R');
	}

	if ($SpAvere[$i] != "")
	{
		$TotContoA = sprintf("%.2f", $TotContoA);
		$pdf->Cell('83', '4', 'Totale Conto:', 'T', '', 'R');
		$pdf->Cell('17', '4', $TotContoA , 'T', '', 'R');
	}

	$pdf->Ln();
	$pdf->Ln();
}

$pdf->Cell('83', '4', 'Totale Dare:', 'T', '', 'R');
$pdf->Cell('17', '4', $TotDare, 'T', '', 'R');
$pdf->Cell('83', '4', 'Totale Avere:', 'T', '', 'R');
$pdf->Cell('17', '4', $TotAvere , 'T', '', 'R');

$pdf->Output('Bilancio.pdf', 'I');

?>

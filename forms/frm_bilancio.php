<?php

if (isset($_GET['tipo']) && $_GET['tipo'] != "")
{
	switch ($_GET['tipo'])
	{
		case "sp":
			$Titolo = "Stato Patrimoniale";
			$SubTitolo1 = "Attivo (Dare):";
			$SubTitolo2 = "Passivo (Avere):";
			$SpDare = array(1, 2, 3, 9, 7, 6, 4, 5, 10, 8);
			$SpAvere = array(14, 16, 20, 17, 18, 15, 42, 43, 19);
			break;

		case "ce":
			$Titolo = "Conto Economico";
			$SubTitolo1 = "Costi:";
			$SubTitolo2 = "Ricavi:";
			$SpDare = array(27, 25, 24, 22, 23, 29, 31, 28, 30, 32, 26, 33, 34, 35);
			$SpAvere = array(37, 38, 36, 39, 40);
			break;
	}
}else{
	$Titolo = "Stato Patrimoniale";
	$SubTitolo1 = "Attivo (Dare):";
	$SubTitolo2 = "Passivo (Avere):";
	$SpDare = array(1, 2, 3, 9, 7, 6, 4, 5, 10, 8);
	$SpAvere = array(14, 16, 20, 17, 18, 15, 42, 43, 19);
}
$Sottoconti = array();

if (count($SpDare) > count($SpAvere))
{
	$MaxConto = count($SpDare);
}else{
	$MaxConto = count($SpAvere);
}

//~ border-left: 1px solid black; border-right: 1px solid black;
?>

<div id="search-form">
	<form method="post" action="<?php echo $_SERVER['REQUEST_URI']; ?>">
		<table class="form" style="width: 100%;">
			<tr>
				<th>Seleziona il tipo di bilancio da stampare:</th>
			</tr>
			<tr>
				<th>
					<a href="home.php?act=bilancio&amp;tipo=sp">
						<img src="img/refresh.png" alt="refresh"
							style="border: 0px; width: 14px; height: 14px; vertical-align: middle;" />
						Stato Patrimoniale
					</a>

					&nbsp;
					<a href="home.php?act=bilancio&amp;tipo=ce">
						<img src="img/refresh.png" alt="refresh"
							style="border: 0px; width: 14px; height: 14px; vertical-align: middle;" />
						Conto Economico
					</a>

					&nbsp;
					<a onclick="javascript: window.open('frm_pdf_bilancio.php?tipo=sp', '_blank'); void(0);"
						title="Esporta in formato pdf">
						<img src="img/pdf.png" alt="pdf"
							style="border: 0px; width: 14px; height: 14px; vertical-align: middle;" />
						Stato Patrimoniale
					</a>

					&nbsp;
					<a onclick="javascript: window.open('frm_pdf_bilancio.php?tipo=ce', '_blank'); void(0);"
						title="Esporta in formato pdf">
						<img src="img/pdf.png" alt="pdf"
							style="border: 0px; width: 14px; height: 14px; vertical-align: middle;" />
						Conto Economico
					</a>
				</th>
			</tr>
		</table>
	</form>
</div>

<div id="search-result-2r">
	<table style="width: 100%;" class="form">
		<tr>
			<th colspan="4" style="text-align: center;">
				<?php echo $Titolo; ?>
				<a onclick="javascript: window.open('frm_pdf_bilancio.php?tipo=<?php
					echo $_GET['tipo']; ?>', '_blank'); void(0);" title="Esporta in formato pdf">
					<img src="img/pdf.png" alt="pdf"
						style="border: 0px; width: 18px; height: 18px; vertical-align: middle;" />
				</a>
			</th>
		</tr>
		<tr><th colspan="3"><br /></th></tr>

		<tr>
			<td style="vertical-align: top;">
				<!-- Tabella Dare !-->
				<table style="width: 100%;" class="dettaglio">
					<tr>
						<th colspan="2" style="text-align: center; border-bottom: 1px solid black;">
							<label><?php echo $SubTitolo1; ?></label>
						</th>
						<th style="text-align: center; border-bottom: 1px solid black;">
							<label>Importo:</label>
						</th>
					</tr>
					<tr><th colspan="3"><br /></th></tr>
					<?php foreach ($SpDare as $id) {
						$TotConto = 0;
						$Sottoconti = GetRows ("tab_conti, tab_sottoconti",
							"tab_conti.id_conto = '$id' AND tab_conti.id_conto = tab_sottoconti.id_conto",
							"", $db);
						foreach ($Sottoconti as $key => $field)
						{
							if ($key == 0)
							{ ?>
								<tr>
									<th style="text-align: right;">
										<label>
											<?php echo "(". $field['codice_conto'] . ")" ?>
										</label>
									</th>
									<th colspan="2">
										<label>
											<?php echo ucfirst(strtolower($field['descr_conto'])); ?>
										</label>
									</th>
								</tr>
								<tr>
									<td style="text-align: right;">
										<?php echo "(". $field['codice_sottoconto'] . ")" ?>
									</td>
									<td>
										<a href="home.php?act=gest_conti&amp;tomod=<?php
											echo $field['id_sottoconto']; ?>&amp;tipo=sc&amp;id_conto=<?php
												echo $field['id_conto']; ?>" title="Click per modificare il sottoconto">
											<?php echo ucfirst(strtolower($field['descr_sottoconto'])); ?>
										</a>
									</td>
									<td style="text-align: right;">
										<label class="err">
											<?php
												$Campi = GetRows ("tab_dett_piano_conti",
													"id_sottoconto = '" . $field['id_sottoconto'] . "'",
													"", $db, 1, "sum(importo_d) as tot_dare, sum(importo_a) as tot_avere");

												if ($Campi[0]['tot_dare'] != "")
												{
													printf("%.2f", $Campi[0]['tot_dare']-$Campi[0]['tot_avere']);
													$TotConto += ($Campi[0]['tot_dare']-$Campi[0]['tot_avere']);
													$TotDare += ($Campi[0]['tot_dare']-$Campi[0]['tot_avere']);
												}else{
													echo "0.00";
												}
											?>
										</label>
									</td>
								</tr>
							<?php }else{ ?>
								<tr>
									<td style="text-align: right;">
										<?php echo "(". $field['codice_sottoconto'] . ")" ?>
									</td>
									<td>
										<a href="home.php?act=gest_conti&amp;tomod=<?php
											echo $field['id_sottoconto']; ?>&amp;tipo=sc&amp;id_conto=<?php
												echo $field['id_conto']; ?>" title="Click per modificare il sottoconto">
											<?php echo ucfirst(strtolower($field['descr_sottoconto'])); ?>
										</a>
									</td>
									<td style="text-align: right;">
										<label class="err">
											<?php
												$Campi = GetRows ("tab_dett_piano_conti",
													"id_sottoconto = '" . $field['id_sottoconto'] . "'",
													"", $db, 1, "sum(importo_d) as tot_dare, sum(importo_a) as tot_avere");

												if ($Campi[0]['tot_dare'] != "")
												{
													printf("%.2f", $Campi[0]['tot_dare']-$Campi[0]['tot_avere']);
													$TotConto += ($Campi[0]['tot_dare']-$Campi[0]['tot_avere']);
													$TotDare += ($Campi[0]['tot_dare']-$Campi[0]['tot_avere']);
												}else{
													echo "0.00";
												}
											?>
										</label>
									</td>
								</tr>
							<?php }
							} ?>
							<tr>
								<th colspan="2" style="text-align: right; border-top: 1px solid black;">
									<label>Totale Conto:</label>
								</th>
								<th style="text-align: right; border-top: 1px solid black;">
									<label>
										<?php printf("%.2f", $TotConto); ?>
									</label>
								</th>
							</tr>
							<tr><th colspan="3"><br /></th></tr>
						<?php } ?>
				</table>
			</td>

			<td style="border-left: 1px solid black; width: 0em;"></td>

			<td style="vertical-align: top;">
				<!-- Tabella Avere !-->
				<table style="width: 100%;" class="dettaglio">
					<tr>
						<th colspan="2" style="text-align: center; border-bottom: 1px solid black;">
							<label><?php echo $SubTitolo2; ?></label>
						</th>
						<th style="text-align: center; border-bottom: 1px solid black;">
							<label>Importo:</label>
						</th>
					</tr>
					<tr><th colspan="3"><br /></th></tr>
					<?php foreach ($SpAvere as $id) {
						$TotConto = 0;
						$Sottoconti = GetRows ("tab_conti, tab_sottoconti",
							"tab_conti.id_conto = '$id' AND tab_conti.id_conto = tab_sottoconti.id_conto",
							"", $db);

						foreach ($Sottoconti as $key => $field)
						{
							if ($key == 0)
							{ ?>
								<tr>
									<th style="text-align: right;">
										<label>
											<?php echo "(". $field['codice_conto'] . ")" ?>
										</label>
									</th>
									<th colspan="2">
										<label>
											<?php echo ucfirst(strtolower($field['descr_conto'])); ?>
										</label>
									</th>
								</tr>
								<tr>
									<td style="text-align: right;">
										<?php echo "(". $field['codice_sottoconto'] . ")" ?>
									</td>
									<td>
										<a href="home.php?act=gest_conti&amp;tomod=<?php
											echo $field['id_sottoconto']; ?>&amp;tipo=sc&amp;id_conto=<?php
												echo $field['id_conto']; ?>" title="Click per modificare il sottoconto">
											<?php echo ucfirst(strtolower($field['descr_sottoconto'])); ?>
										</a>
									</td>
									<td style="text-align: right;">
										<label class="ok">
											<?php
												$Campi = GetRows ("tab_dett_piano_conti",
													"id_sottoconto = '" . $field['id_sottoconto'] . "'",
													"", $db, 1, "sum(importo_d) as tot_dare, sum(importo_a) as tot_avere");

												if ($Campi[0]['tot_avere'] != "") {
													printf("%.2f", $Campi[0]['tot_avere']-$Campi[0]['tot_dare']);
													$TotConto += ($Campi[0]['tot_avere']-$Campi[0]['tot_dare']);
													$TotAvere += ($Campi[0]['tot_avere']-$Campi[0]['tot_dare']);
												}else{
													echo "0.00";
												}
											?>
										</label>
									</td>
								</tr>
							<?php }else{ ?>
								<tr>
									<td style="text-align: right;">
										<?php echo "(". $field['codice_sottoconto'] . ")" ?>
									</td>
									<td>
										<a href="home.php?act=gest_conti&amp;tomod=<?php
											echo $field['id_sottoconto']; ?>&amp;tipo=sc&amp;id_conto=<?php
												echo $field['id_conto']; ?>" title="Click per modificare il sottoconto">
											<?php echo ucfirst(strtolower($field['descr_sottoconto'])); ?>
										</a>
									</td>
									<td style="text-align: right;">
										<label class="ok">
											<?php
												$Campi = GetRows ("tab_dett_piano_conti",
													"id_sottoconto = '" . $field['id_sottoconto'] . "'",
													"", $db, 1, "sum(importo_d) as tot_dare, sum(importo_a) as tot_avere");

												if ($Campi[0]['tot_avere'] != "") {
													printf("%.2f", $Campi[0]['tot_avere']-$Campi[0]['tot_dare']);
													$TotConto += ($Campi[0]['tot_avere']-$Campi[0]['tot_dare']);
													$TotAvere += ($Campi[0]['tot_avere']-$Campi[0]['tot_dare']);
												}else{
													echo "0.00";
												}
											?>
										</label>
									</td>
								</tr>
							<?php }
							} ?>
							<tr>
								<th colspan="2" style="text-align: right; border-top: 1px solid black;">
									<label>Totale Conto:</label>
								</th>
								<th style="text-align: right; border-top: 1px solid black;">
									<label>
										<?php printf("%.2f", $TotConto); ?>
									</label>
								</th>
							</tr>
							<tr><th colspan="3"><br /></th></tr>
						<?php } ?>
				</table>
			</td>
			<td style="border-left: 1px solid black; width: 0em;"></td>
		</tr>

		<tr>
			<td style="text-align: right;">
				<label>Totale Dare:</label>
				<label class="err">
					<?php printf("%.2f", $TotDare); ?>
				</label>
			</td>
			<td></td>
			<td style="text-align: right;">
				<label>Totale Avere:</label>
				<label class="ok">
					<?php printf("%.2f", $TotAvere); ?>
				</label>
			</td>
			<td></td>
		</tr>
	</table>
</div>

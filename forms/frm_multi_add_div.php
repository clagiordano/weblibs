<?php
    $root = substr($_SERVER['DOCUMENT_ROOT'], 0, -1);
//    require_once("connection.php");
	require_once("$root/Function/Db.php");

    echo "<pre>post: ";
    print_r($_POST);
    echo "</pre>";

    echo "<pre>get: ";
    print_r($_GET);
    echo "</pre>";

	if (isset($_POST['desc']))
	{
		$errori = "";
		if (trim($_POST['desc']) == "")
		{
			$errori .= "Errore, il campo valore non può essere vuoto.<br />";
		}

		switch ($_GET['subact'])
		{
			case "documenti":
				if (isset($_POST['prz']) && (trim($_POST['prz']) == "") || !is_numeric($_POST['prz']))
				{
					$errori .= "Errore, il campo prezzo immesso non è valido.<br />";
				}

				if (isset($_POST['qta']) && trim($_POST['qta']) == "")
				{
					$_POST['qta'] = 1;
				}

				if (isset($_POST['qta']) && !is_numeric($_POST['qta']))
				{
					$errori .= "Errore, il campo quantità immesso non è valido.<br />";
				}

				if (isset($_POST['sco']) && ((!is_numeric($_POST['sco']))
					|| ($_POST['sco'] < 0) || ($_POST['sco'] > 100)))
				{
					if (trim($_POST['sco']) == "")
					{
						$_POST['sco'] = 0;
					}else{
						$errori .= "Errore, il campo sconto immesso non è valido.<br />";
					}
				}
				break;
		}
	}

    echo "Debug => post <br />";

	if (isset($_POST['desc']) && ($errori == ""))
	{
        echo "Debug => post int <br />";
        $_POST['desc'] = trim($_POST['desc']);
        
		//~ Aggiungo in sessione l'array con le info:
		switch ($_POST['subact'])
        {
			case "documenti":
				if (!isset($_SESSION['documento']['dettaglio']))
				{
					$_SESSION['documento']['dettaglio'] = array();
				}

				array_push($_SESSION['documento']['dettaglio'],
					array(    'id' => '-',
							'desc' => $_POST['desc'],
                             'prz' => sprintf("%.2f", $_POST['prz']),
							'disp' => '-',
					    'cod_forn' => '-',
                         'cod_int' => '-',
                             'sco' => sprintf("%.2f", $_POST['sco']),
							 'qta' => sprintf("%.2f", $_POST['qta'])));
				break;

			case "piano_conti":
				if (!isset($_SESSION['piano']['dettaglio']))
				{
					$_SESSION['piano']['dettaglio'] = array();
				}

				array_push($_SESSION['piano']['dettaglio'],
					array(				'id' => '',
										'da' => '',		//~ dare/avere (d o a)
							 'id_anagrafica' => '',
									  'desc' => $_POST['desc'],
								 'importo_d' => '',
								 'importo_a' => '',
								    'id_iva' => '',
								   'id_causale' => $_POST['causale'])
				);
				break;

            case "cat_merce":
                $unic = GetRows ("tab_cat_merce", "categoria = '".$_POST['desc']."'", "", $db, 0);
                   $Campi = GetRows ($Tab, $Where, $Order, $db);
                if (count($unic) != 0)
                {
                    $errori .= "Errore, la categoria è già presente.<br />";
                }else{
                    $Valori = array('categoria' => $_POST['desc']);
                    $Status = SaveRow ($Valori, "", "tab_cat_merce", $db, 0);
                    // salva -> ricarica lista cat -> aggiorna combo -> nascondi div
                }
                break;

		}
        
		//~ Riaggiorno la pagina genitore e chiudo il popup:
		?>
			<!--<script type="text/javascript">
				window.opener.location.href="home.php?act=<?php //echo $_GET['subact']; ?>"
				window.close();
			</script> !-->

	<?php }else{ ?>
		<label class="err"><?php echo $errori; ?></label>
	<?php } ?>

<form method="post" action="<?php echo $_SERVER['REQUEST_URI']; ?>">
	<table class="form" style="width: 100%;">
		<tr>
			<th colspan="2">
				<?php if (isset($_GET['titolo']))
					{
						echo $_GET['titolo'] . ":";
					} ?>
			</th>
		</tr>

        <tr><td><br /></td></tr>
        
		<tr>
			<td style="text-align: right;">
				<label>Descrizione:</label>
			</td>
			<td>
				<input name="desc" type="text" style="width: 35em;"
					value="<?php if (isset($_POST['desc'])) { echo $_POST['desc']; } ?>" />
			</td>
		</tr>
		<?php switch ($_GET['subact'])
        {
				case "documenti": ?>
					<tr>
						<td style="text-align: right;">
							<label>Prezzo:</label>
						</td>
						<td>
							<input name="prz" type="text" style="width: 5em;"
								value="<?php if (isset($_POST['prz'])) { echo $_POST['prz']; }; ?>" />

							&nbsp;
							<label>Quantità:</label>
							<input name="qta" type="text" style="width: 5em;"
								value="<?php if (isset($_POST['qta'])) { echo $_POST['qta']; }; ?>" />

							&nbsp;
							<label>Sconto:</label>
							<input name="sco" type="text" style="width: 5em;"
								value="<?php if (isset($_POST['sco'])) { echo $_POST['sco']; }; ?>" />
						</td>
					</tr>
					<?php break;

				case "piano_conti": ?>
					<!--<tr>
						<td style="text-align: right;">
							<label>Causale:</label>
						</td>
						<td>
							<input name="causale" type="text" style="width: 35em;"
								value="<?php //if (isset($_POST['causale'])) { echo $_POST['causale']; }; ?>" />
						</td>
					</tr>
					<tr>
						<td style="text-align: right;">
							<label>Dare:</label>
						</td>
						<td>
							<input name="importo_d" type="text" style="width: 7em;"
								value="<?php //if (isset($_POST['importo_d'])) { echo $_POST['importo_d']; }; ?>" />

							&nbsp;
							<label>Avere:</label>
							<input name="importo_a" type="text" style="width: 7em;"
								value="<?php //if (isset($_POST['importo_a'])) { echo $_POST['importo_a']; }; ?>" />

						</td>
					</tr>!-->
					<?php break;
        } ?>
                    <tr>
                        <th>
                            <a onclick="javascript: ShowPopupDiv('../Forms/frm_multi_add_div.php' ,
                                '200px', '200px', '550px', '300px', 'popup', 
                                'titolo=<?php echo $_GET['titolo']; ?>&amp;subact=<?php echo $_GET['subact']; ?>&amp;desc=' + document.forms[1].desc.value);"
                                title="Aggiungi">
                                <img src="img/add.png" alt="Aggiungi" />Aggiungi
                            </a>
                        </th>
                        <th style="text-align: right;">
                            <a onclick="javascript: document.getElementById('popup').style.display = 'none';">
                                <img src="img/exit.png" alt="exit" />
                                Chiudi
                            </a>
                        </th>
                    </tr>
	</table>
</form>

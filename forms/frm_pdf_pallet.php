<?php ob_end_clean();
	require_once("fpdf/fpdf.php");
	if (isset($_GET['pallet']) && (trim($_GET['pallet']) != ""))
	{
		//echo "[Debug]: esiste pallet <br />";
		$_GET['pallet'] = stripslashes($_GET['pallet']);
		if (strlen($_GET['pallet']) > 50)
		{
			//echo "[Debug]: la lunghezza supera i 50 caratteri <br />";
			$_GET['pallet'] = substr($_GET['pallet'], 0, 49);
		}

		$dett_pallet = GetRows ("view_macchine", "pallet = '"
			. $_GET['pallet'] . "'", "tipo_mch", $db, 1);

		class PDF extends FPDF
		{
			//Page footer
			function Footer()
			{
				//Position at 1.5 cm from bottom
				$this->SetY(-15);
				$this->SetFont('Arial', '', 9);
				$this->Cell(40, 4, 'Data: ' . date('d/m/Y'), 0, '', 'L');
				$this->Cell(0, 4, 'Pagina ' . $this->PageNo() . "/{nb}", 0, 0 ,'C');
			}
		}

		$pdf=new PDF();
		$pdf->AliasNbPages();
		$pdf->SetMargins(6, 15, 6);
		$pdf->AddPage();

		// Header
		$pdf->SetFont('Helvetica', 'B', 14);
		$pdf->Cell(200, 7, 'Dettaglio Pallet: ' . $_GET['pallet'], '', '', '');
		$pdf->ln(10);

		$pdf->SetFont('Helvetica', 'B', 10);
		$pdf->Cell(10, 5, '', 'B', '', 'R');
		$pdf->Cell(30, 5, 'Tipo Macchina:', 'B', '', 'L');
		$pdf->Cell(30, 5, 'Marca:', 'B', '', 'L');
		$pdf->Cell(30, 5, 'Modello:', 'B', '', 'L');
		$pdf->Cell(50, 5, 'Serial:', 'B', '', 'L');
		$pdf->Cell(50, 5, 'Asset:', 'B', '', 'L');
		$pdf->ln();

		foreach ($dett_pallet as $key => $field)
		{
			$pdf->SetFont('Helvetica', 'B', 10);
			$pdf->Cell(10, 5, $key+1 . ")", '0', '', 'R');

			$pdf->SetFont('Helvetica', '', 10);
			$pdf->Cell(30, 5, $field['tipo_mch'], '0', '', 'L');
			$pdf->Cell(30, 5, $field['marca'], '0', '', 'L');
			$pdf->Cell(30, 5, $field['modello'], '0', '', 'L');
			$pdf->Cell(50, 5, $field['serial'], '0', '', 'L');
			$pdf->Cell(50, 5, $field['asset'], '0', '', 'L');
			$pdf->ln();
		}

		$pdf->Output('Pallet.pdf', 'I');
	}else{
		echo "Errore, impossibile selezionare il pallet desiderato.";
	}
?>

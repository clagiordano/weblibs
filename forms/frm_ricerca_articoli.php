<?php
	/*
	 * frm_ricerca_articoli.php
	 *
	 * Copyright 2008 - 2014 Claudio Giordano <claudio.giordano@autistici.org>
	 *
	 */

	//~ echo "<pre>";
		//~ print_r($_POST);
	//~ echo "</pre>";

	require_once PHP_PATH . "Gestione.php";
    
	$tot_res		= -1;
	$doc_key		= "";
	$Depositi       = $d->GetRows("*", "tab_depositi", "", "", "dep");
	$TipoArt 		= $d->GetRows("*", "tab_tipo_art", "", "", "desc_art");
	$Categorie      = $d->GetRows("*", "tab_cat_merce", "", "", "cat");
	$SelfPage       = "frm_ricerca_articoli.php";

	if (isset($_GET['subact']))
	{
		$SelfPage .= "?subact=" . $_GET['subact'];
	}else{
		$_GET['subact'] = "";
	}

	if (isset($_GET['tomod']))
	{
		$SelfPage .= "&tomod=" . $_GET['tomod'];
	}

	if (!isset($_GET['page']))
	{
		$_GET['page'] = "first";
	}
    
	if (isset($_GET['prod']))
	{
		// echo "[Debug]: è stato selezionato un prodotto <br />";
		$prod 	= $_GET['prod'];

		// Se la quantità scelta è nulla o < 1 allora impostala ad 1:
		//~ if (!is_numeric($_POST[$prod]) || ($_POST[$prod] < 0))
		//~ {
			//~ $_POST[$prod] = "1";
		//~ }

		//~ Estraggo le informazioni riguardo il prodotto secondo l'id prodotto:
		$dett_prodotto = $d->GetRows("*", "view_prodotti",
			"id_prodotto = '" . $_GET['prod'] . "'", "", "", 1);
        
		switch ($_GET['subact'])
		{
			case "articolo_com":
				if (isset($_SESSION['composto']['tomod']))
				{
					$_SESSION['composto']['modificato'] = "y";
					$OpenerUrl = "home.php?act=articolo_com&tomod="
						. $_SESSION['composto']['tomod'];
				}else{
					$OpenerUrl = "home.php?act=articolo_com";
				}

				if (!isset($_SESSION['composto']))
				{
					$_SESSION['composto'] = array();
				}

				if (!isset($_SESSION['composto']['comp']))
				{
					$_SESSION['composto']['comp'] = array();
				}

				$Campi = $d->GetRows ("*", "view_prodotti",
					"id_prodotto = '" . $_GET['prod'] . "'");

				if ($_GET['pezzo'] == "gen")
				{
					$_SESSION['composto']['genitore']['id'] 			= $_GET['prod'];
					$_SESSION['composto']['genitore']['cod'] 			= $Campi[0]['cod_forn'];
					$_SESSION['composto']['genitore']['marca'] 		= $Campi[0]['marca'];
					$_SESSION['composto']['genitore']['modello'] 	= $Campi[0]['modello'];
					$_SESSION['composto']['genitore']['prz'] 			= $Campi[0]['prz_acq'];
					$_SESSION['composto']['genitore']['qta'] 			= $_POST[$prod];
				}

				if ($_GET['pezzo'] == "comp")
				{
					$dett_prodotto[0]['qta'] = $_POST[$prod];
					array_push($_SESSION['composto']['dettaglio'], $dett_prodotto[0]);
				}
			break;

			case "documenti":
				//~ echo "Debug => caso documenti <br />";
				if (isset($_SESSION['documento']['tomod']))
				{
					$OpenerUrl = "home.php?act=documenti&amp;tomod=" 
						. $_SESSION['documento']['tomod'];
				}else{
					$OpenerUrl = "home.php?act=documenti";
				}

				// Se il tipo di documento è 5 ovvero fattura vendita
				// verifica che la giacenza sia sufficiente:
				switch ($_SESSION['documento']['tipo_doc'])
				{
					case "5":
						$Giacenza = VerificaGiacenza($prod, $_POST[$prod], 1);
						//~ echo "[Debug][frm_ricerca_articoli]: GIACENZA: $Giacenza <br />";
						
						if ($Giacenza > 0)
						{
							AddDocumentRow("prodotto", $prod, "", "", $_POST[$prod]);
						}else{ ?>
							<script type="text/javascript">
								alert("L'articolo non è disponibile, impossibile selezionarlo.")
								document.location.href="<?php echo $SelfPage; ?>";
							</script>
						<?php }

						break;

					default:
						AddDocumentRow("prodotto", $prod, "", "", $_POST[$prod]);
				}
				break;
		} ?>
		<script type="text/javascript">
			//~ alert("STOP");
			<?php if (isset($_GET['prod'])) { ?>
				parent.location.href="<?php echo $OpenerUrl; ?>";
			<?php } ?>
		</script>
		<?php
	}

	// Se è stata fatta una ricerca:
	if (!empty($_POST) && isset($_POST['modello']))
	{
		// Compone la query per la ricerca dei prodotti:
		$Where = "1";

		if (trim($_POST['articolo']) != "")
		{
			$val = $d->ModoRicerca($_POST['articolo'], "tutto");
			$val = $d->ExpandSearch($val);
			$Where = " (cod_forn LIKE '%$val%' OR cod_int LIKE '%$val%')";
		}

		if (trim($_POST['modello']) != "")
		{
			$val = $d->ModoRicerca($_POST['modello'], "tutto");
			$val = $d->ExpandSearch($val);
			$Where = " (marca LIKE '%$val%' OR modello LIKE '%$val%')";
		}

		if ($_POST['id_cat'] != "%")
		{
			$Where .= " AND id_cat = '" . $_POST['id_cat'] . "' ";
		}

		if ($_POST['id_tipo'] != "%")
		{
			$Where .= " AND id_tipo = '" . $_POST['id_tipo'] . "' ";
		}

		$tot_res 	= count($d->GetRows ("id_prodotto", "view_prodotti", $Where));
		$order 		= $d->PageSplit($_GET['page'], $tot_res);
		$Campi 		= $d->GetRows ("*", "view_prodotti", $Where, "", "marca, modello $order");
	}
?>

<div id="search-form">
	<form method="post" action="<?php echo $_SERVER['REQUEST_URI']; ?>">
		<table class="form" style="width: 100%;">

			<?php if (!isset($_GET['act'])) { ?>
				<tr>
					<td style="text-align: right;" colspan="2">
						<a onclick="Javascript: ClosePopup(); void(0);">
							[ <label class="err" style="cursor: pointer;">Chiudi</label> ]
						</a>
					</td>
				</tr>
			<?php } ?>

			<tr>
				<th colspan="2">
					Ricerca Articoli:

					&nbsp;
					<a href="<?php if (isset($_GET['act'])) {
						echo "home.php?act=articolo"; }else{ echo "frm_anag_articolo.php"; } ?>">
						<img src="<?php echo IMG_PATH; ?>Links/new.png" alt="Nuovo Articolo" />
						Nuovo
					</a>
				</th>

			</tr>
			<tr>
				<td colspan="2">
						<select name="id_cat" style="max-width: 20em;"
							onchange="Javascript: posta('0', '<?php echo $_SERVER['REQUEST_URI']; ?>');" >
							<option value="%">Tutte le categorie</option>
							<option value="%"></option>
							<?php foreach ($Categorie as $key => $field) { ?>
								<option value="<?php echo $field['id_cat']; ?>"
									<?php if (isset($_POST['id_cat'])
										&& ($_POST['id_cat'] == $field['id_cat'])) {
											echo "selected=\"selected\""; } ?> >
									<?php echo $field['cat']; ?>
								</option>
							<?php } ?>
						</select>

						<select name="id_tipo" style="max-width: 20em;"
							onchange="Javascript: posta('0', '<?php echo $_SERVER['REQUEST_URI']; ?>');" >
							<option value="%">Tutti i tipi</option>
							<option value="%"></option>
							<?php foreach ($TipoArt as $key => $field) { ?>
								<option value="<?php echo $field['id_art']; ?>"
									<?php if (isset($_POST['id_tipo'])
										&& ($_POST['id_tipo'] == $field['id_art'])) {
											echo "selected=\"selected\""; } ?> >
									<?php echo $field['desc_art']; ?>
								</option>
							<?php } ?>
						</select>

						<input name="articolo" type="text" style="width: 10em;"
							onKeyPress="return SubmitEnter(this,event);"
							placeholder="Codice articolo"
							value="<?php if (isset($_POST['articolo'])) { echo $_POST['articolo']; } ?>" />

						<input name="modello" type="text" style="width: 30em;"
							onKeyPress="return SubmitEnter(this,event);"
							placeholder="Marca o Modello"
							value="<?php if (isset($_POST['modello'])) { echo $_POST['modello']; } ?>" />
				</td>
			</tr>

			<?php
				if (!isset($_GET['act']))
				{
					$d->ShowResultBar($tot_res, "frm_ricerca_articoli.php?page=first&amp;subact="
						. $_GET['subact']);
				}else{
					$d->ShowResultBar($tot_res, "home.php?act=" . $_GET['act']
						. "&amp;page=first&amp;subact=" . $_GET['subact']);
				}
			?>

		</table>
	</form>
</div>

<?php if(isset($_POST['modello']) && (count($Campi) != 0)) { ?>
	<div id="search-result-2r">
		<form method="post" action="<?php echo $_SERVER['REQUEST_URI']; ?>">
			<table style="width: 100%;" class="dettaglio">
				<tr>
					<th><label>Cod. Fornitore:</label></th>
					<th><label>Cod. Interno:</label></th>
                    <th><label>Categoria:</label></th>
					<th><label>Marca / Modello:</label></th>
					<th><label>Tipo:</label></th>
					<th><label>Prz. Acquisto:</label></th>
					<th><label>Prz. Veendita:</label></th>
					<th><label>Disp:</label></th>
					<?php if (!isset($_GET['act'])) { ?>
						<th colspan="2">
							<label>Q.ta:</label>
						</th>
					<?php } ?>
                    <th style="text-align: right;">
                        <a onclick="Javascript: if ( CheckSelected('multisel') == true ) 
                            {
                                ShowFormDiv('frm_anag_articolo.php?tomod=multi',
                                '200px', '200px', '770px', '320px', 'popup'); void(0); 
                            }else{ 
                                alert('Nessuna voce selezionata.'); 
                            }"
							 title="Modifica Selezionati">
							<img src="<?php echo IMG_PATH; ?>Links/edit.png" alt="edit" />
						</a>
                       
                        <input type="checkbox" title="Commuta Selezione" 
                            onclick="Javascript: SelectAll('multisel', 'comm'); void(0);" />
                        
                        &nbsp;
                        &nbsp;
                    </th>
				</tr>
                
				<tr><th colspan="10"><hr /></th></tr>
            
				<?php foreach ($Campi as $key => $field)
					{
						$Image = $field['cod_forn'] . ".jpg"; 
						
						$DisponibilitaProdotto = getGiacenza($field['cod_int'], 1);
						if ($field['um'] == "Pz")
						{
							$DisponibilitaProdotto = sprintf("%." . PRECISIONE_DECIMALI . "f", $DisponibilitaProdotto);
						}
					?>

                    <tr>
                        <td>
                            <?php echo $field['cod_forn']; ?>
                        </td>
                        <td>
                            <?php echo $field['cod_int']; ?>
                        </td>
                        <td>
                            <?php echo $field['cat']; ?>
                        </td>
                        <td>
                            <?php echo $field['marca'] . " " . $field['modello']; ?>
                        </td>
                        <td>
                            <?php echo $field['desc_art']; //tipo ?>
                        </td>

                        <td style="text-align: right;">
                            <?php printf("%." . PRECISIONE_DECIMALI . "f € ",  $field['prz_acq']); ?>
                        </td>

                        <td style="text-align: right;">
                            <?php printf("%." . PRECISIONE_DECIMALI . "f € ", $field['prz_ve']); ?>
                        </td>

                        <td style="text-align: right;">
                            <?php echo $DisponibilitaProdotto . " " . $field['um']; ?>
                        </td>

                        <?php if (!isset($_GET['act'])) { ?>
                            <td>
                                <input name="<?php echo $field['id_prodotto']; ?>" type="text"
                                    maxlength="5" style="text-align: right; width: 3em;" />
                            </td>

                            <td style="text-align: center;">
                                <a onclick="javascript: posta('1', 'frm_ricerca_articoli.php?prod=<?php
                                    echo $field['id_prodotto'];
                                    if (isset($_GET['subact'])) { echo "&amp;subact=" . $_GET['subact']; }
                                    if (isset($_GET['pezzo'])) { echo "&amp;pezzo=" . $_GET['pezzo']; }?>');"
                                    title="Seleziona prodotto" style="cursor: pointer;">
                                    <img src="<?php echo IMG_PATH; ?>Links/select.png" alt="Seleziona prodotto" />
                                </a>
                            </td>
                        <?php }else{ ?>
                            <td style="text-align: right;">
                                <?php if ($field['ck_com'] == "0") { ?>
                                    <a href="home.php?act=articolo_com&amp;tomod=<?php
                                        echo $field['id_prodotto']; ?>" title="Modifica Composto">
                                        <img src="<?php echo IMG_PATH; ?>Links/edit2.png" alt="edit2" />
                                    </a>
                                <?php } ?>

                                <a href="home.php?act=articolo&amp;tomod=<?php
                                    echo $field['id_prodotto']; ?>" title="Modifica Articolo">
                                    <img src="<?php echo IMG_PATH; ?>Links/edit.png" alt="edit" />
                                </a>

                                <input name="multisel" type="checkbox" style="vertical-align: middle;"
                                    value="<?php echo $field['id_prodotto']; ?>" />

                                <a href="javascript: go_conf2('Eliminare l\'articolo?', 'home.php?act=ricerca_art&amp;del=<?php
                                    echo $field['id_prodotto']; ?>'); void(0);" title="Elimina Articolo">
                                    <img src="<?php echo IMG_PATH; ?>Links/del.png" alt="del"/>
                                </a>
                            </td>
                        <?php } ?>
                    </tr>
                <?php } ?>
				<tr><th colspan="10"><hr /></th></tr>
			</table>
		</form>
	</div>
<?php } ?>


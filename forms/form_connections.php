<?php
	$users 		= $d->GetRows("*", "tab_utenti", "", "", "user");
	$tot_res 	= -1;
	$Where 		= "";
	
	if (!isset($_GET['page']))
	{
		$_GET['page'] = "first";
	}
	
	if (!empty($_POST))
	{
		$Where = "user LIKE '" . $_POST['user'] . "'";

		if (trim($_POST['ip']) != "")
		{			
			$Where .= " AND ip LIKE '%" 
				. $d->ExpandSearch(str_replace("'", "\'", preg_quote($_POST['ip'])))	
				. "%'";
		}

		if (trim($_POST['action']) != "")
		{			
			$Where .= " AND action LIKE '%" 
				. $d->ExpandSearch(str_replace("'", "\'", preg_quote($_POST['action'])))	
				. "%'";
		}

		if (isset($_POST['ck_data']))
		{
			/*
			 * Se una delle date è vuota ma il campo e' stato flaggato
			 * le date mancanti diventano la data odierna
			 */

			if (trim($_POST['data_i']) == "")
			{
				$_POST['data_i'] = date('d/m/Y');
			}

			if (trim($_POST['data_f']) == "")
			{
				$_POST['data_f'] = date('d/m/Y');
			}

			$inizio = $d->Inverti_Data($_POST['data_i']);
			$fine = $d->Inverti_Data($_POST['data_f']);
			$Where .= " AND (data >= '$inizio 00:00:00' AND data <= '$fine 23:59:59')";
		}

// 		Fix SQL Injection vedi sopra		
// 		$Where .= " AND ip LIKE '%" . $d->ExpandSearch($_POST['ip']) . "%'";
// 		$Where .= " AND action LIKE '%" . $d->ExpandSearch($_POST['action']) . "%'";

		$tot_res = $d->GetRows("count(user) as tot", "view_connections", $Where, "", "", 1);
		$tot_res = $tot_res[0]['tot'];
		$order = $d->PageSplit($_GET['page'], $tot_res);
		$Connections = $d->GetRows("*", "view_connections", $Where, "", "data desc $order", 0);
	}
?>

<section id="search-form">
	<form method="post" action="<?php echo $_SERVER['REQUEST_URI']; ?>">
		<table class="form" style="width: 100%;">
			<tr>
				<th colspan="2">Seleziona i campi fra i quali ricercare:</th>
			</tr>

			<tr>
				<td>
					<?php $d->DrawControl("selectall", "user", "Seleziona utente", 
						"\d+", $_POST, null, "auto", "", 
						"onchange=\"Javascript: posta('0', '" . $_SERVER['REQUEST_URI'] . "'); void(0);\"", 
						$users, "user", "user"); ?>
						
					&nbsp;
					<label>Data:</label>
					<?php 
						$d->DrawControl("checkbox", "ck_data", "", "", $_POST, "interval_att");
					
						$d->DrawControl("text", "data_i", "Data iniziale", "", 
							$_POST, null, "7em", "10", "onfocus=\"showCalendarControl(this);\"");
							
						$d->DrawControl("text", "data_f", "Data finale", "", 
							$_POST, null, "7em", "10", "onfocus=\"showCalendarControl(this);\""); 
					?>

					&nbsp;
					<label>IP:</label>
					<?php $d->DrawControl("text", "ip", "Indirizzo IP", "", 
							$_POST, null, "12em", "30"); ?>

					&nbsp;
					<label>Action:</label>
					<?php $d->DrawControl("text", "action", "Azione", "", 
							$_POST, null, "15em", "200"); ?>
				</td>
			</tr>
			<?php $d->ShowResultBar($tot_res, "?act=connections"); ?>
		</table>
	</form>
</section>

<section id="search-result">
	<form method="post" action="<?php echo $_SERVER['REQUEST_URI']; ?>">
		<?php if(!empty($_POST)) { ?>
			<?php if (count($Connections) != 0) { ?>
				<table class="dettaglio" style="width: 100%;">
					<tr>
						<th>DataTime:</th>
						<th>IP:</th>
						<th>User (Group):</th>
						<th>Platform:</th>
						<th>Arch:</th>
						<th>Browser:</th>
						<th>Version:</th>
						<th>Action:</th>
					</tr>
					<tr><th colspan="8"><hr /></th></tr>
						<?php foreach ($Connections as $key => $field) { ?>
							<tr>
								<td style="vertical-align: top;">
									<?php echo $d->Inverti_DataTime($field['data']); ?>
								</td>

								<td style="vertical-align: top;">
									<label class="err"><?php echo $field['ip']; ?></label>
								</td>

								<td style="vertical-align: top;">
									<label class="ok">
										<?php echo $field['user']
											. " (" . $field['descr_tipo'] . ")"; ?></label>
								</td>

								<td style="vertical-align: top;">
									<?php echo $field['platform']; ?>
								</td>

								<td style="vertical-align: top;">
									<?php echo $field['arch'] . "bit"; ?>
								</td>

								<td style="vertical-align: top;">
									<?php echo $field['browser']; ?>
								</td>

								<td style="vertical-align: top;">
									<?php echo $field['version']; ?>
								</td>

								<td style="vertical-align: top;">
									<?php echo wordwrap($field['action'], 72, "<br />"); ?>
								</td>
							</tr>

						<?php } ?>
					<tr><th colspan="8"><hr /></th></tr>
				</table>
			<?php } ?>
		<?php } ?>
	</form>
</section>
